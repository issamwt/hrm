-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  ven. 04 jan. 2019 à 20:38
-- Version du serveur :  10.1.37-MariaDB-cll-lve
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `smarthrt_demo`
--

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE `countries` (
  `idCountry` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT '',
  `countryName_ar` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `countries`
--

INSERT INTO `countries` (`idCountry`, `countryCode`, `countryName`, `countryName_ar`) VALUES
(1, 'AD', 'Andorra', 'أندورا'),
(2, 'AE', 'United Arab Emirates', 'الإمارات'),
(3, 'AF', 'Afghanistan', 'أفغانستان'),
(4, 'AG', 'Antigua and Barbuda', 'بربودا و أنتيقا'),
(5, 'AI', 'Anguilla', 'أنغويلا'),
(6, 'AL', 'Albania', 'ألبانيا'),
(7, 'AM', 'Armenia', 'أرمينيا'),
(8, 'AO', 'Angola', 'أنغولا'),
(9, 'AQ', 'Antarctica', 'أنترانتيكا'),
(10, 'AR', 'Argentina', 'الأرجنتين'),
(11, 'AS', 'American Samoa', 'ساموا'),
(12, 'AT', 'Austria', 'النمسا'),
(13, 'AU', 'Australia', 'أستراليا'),
(14, 'AW', 'Aruba', 'أروبا'),
(16, 'AZ', 'Azerbaijan', 'أذربيجان'),
(17, 'BA', 'Bosnia and Herzegovina', 'البوسنة والهرسك'),
(18, 'BB', 'Barbados', 'بربادوسا'),
(19, 'BD', 'Bangladesh', 'بنغلاديش'),
(20, 'BE', 'Belgium', 'بلجيكيا'),
(21, 'BF', 'Burkina Faso', 'بوركينا فاسو'),
(22, 'BG', 'Bulgaria', 'بلغاريا'),
(23, 'BH', 'Bahrain', 'البحرين'),
(24, 'BI', 'Burundi', 'بوراندي'),
(25, 'BJ', 'Benin', 'البينين'),
(27, 'BM', 'Bermuda', 'برمودا'),
(28, 'BN', 'Brunei', 'بروناي'),
(29, 'BO', 'Bolivia', 'بوليفيا'),
(30, 'BQ', 'Bonaire', 'بونار'),
(31, 'BR', 'Brazil', 'البرازيل'),
(32, 'BS', 'Bahamas', 'الباهاما'),
(33, 'BT', 'Bhutan', 'بوتان'),
(34, 'BV', 'Bouvet Island', 'جزيرة بوفيت'),
(35, 'BW', 'Botswana', 'بوتسوانا'),
(36, 'BY', 'Belarus', 'بيلاروسيا'),
(37, 'BZ', 'Belize', 'بيليز'),
(38, 'CA', 'Canada', 'كندا'),
(39, 'CC', 'Cocos [Keeling] Islands', 'جزر كوكوس'),
(40, 'CD', 'Democratic Republic of the Congo', 'الكونغو الديمقراطية'),
(41, 'CF', 'Central African Republic', 'جمهورية إفريقيا الوسطى'),
(42, 'CG', 'Republic of the Congo', 'الكنغو'),
(43, 'CH', 'Switzerland', 'سويسرا'),
(46, 'CL', 'Chile', 'شيلي'),
(47, 'CM', 'Cameroon', 'كاميرون'),
(48, 'CN', 'China', 'الصين'),
(49, 'CO', 'Colombia', 'كولومبيا'),
(50, 'CR', 'Costa Rica', 'كوستاريكا'),
(51, 'CU', 'Cuba', 'كوبا'),
(52, 'CV', 'Cape Verde', 'الرأس الأخضر'),
(53, 'CW', 'Curacao', 'كوراكاو'),
(55, 'CY', 'Cyprus', 'قبرص'),
(56, 'CZ', 'Czech Republic', 'جمهورية التشيك'),
(57, 'DE', 'Germany', 'ألمانيا'),
(58, 'DJ', 'Djibouti', 'جيبوتي'),
(59, 'DK', 'Denmark', 'دنمارك'),
(60, 'DM', 'Dominica', 'دومينيكا'),
(61, 'DO', 'Dominican Republic', 'جمهورية الدومنيكان'),
(62, 'DZ', 'Algeria', 'الجزائر'),
(63, 'EC', 'Ecuador', 'إكوادور'),
(64, 'EE', 'Estonia', 'إستونيا'),
(65, 'EG', 'Egypt', 'مصر'),
(66, 'EH', 'Western Sahara', 'الصحراء الغربية'),
(67, 'ER', 'Eritrea', 'إيريتريا'),
(68, 'ES', 'Spain', 'إسبانيا'),
(69, 'ET', 'Ethiopia', 'أثيوبيا'),
(70, 'FI', 'Finland', 'فنلاندا'),
(71, 'FJ', 'Fiji', 'فوجي'),
(72, 'FK', 'Falkland Islands', 'جزر فوكلاند'),
(73, 'FM', 'Micronesia', 'ميكرونيزيا'),
(75, 'FR', 'France', 'فرنسا'),
(76, 'GA', 'Gabon', 'الغابون'),
(77, 'GB', 'United Kingdom', 'المملكة المتحدة'),
(78, 'GD', 'Grenada', 'غرينادا'),
(79, 'GE', 'Georgia', 'جيورجيا'),
(80, 'GF', 'French Guiana', 'فارو جويانا الفرنسية'),
(82, 'GH', 'Ghana', 'غانا'),
(84, 'GL', 'Greenland', 'غرينلاند'),
(85, 'GM', 'Gambia', 'غامبيا'),
(86, 'GN', 'Guinea', 'غينيا'),
(87, 'GP', 'Guadeloupe', 'قوادالوبي'),
(88, 'GQ', 'Equatorial Guinea', 'غينيا الإستوائية'),
(89, 'GR', 'Greece', 'اليونان'),
(90, 'GS', 'South Georgia and the South Sandwich Islands', 'جورجيا الجنوبية وجزر ساندويتش الجنوبية'),
(91, 'GT', 'Guatemala', 'غواتيمالا'),
(92, 'GU', 'Guam', 'غوام'),
(93, 'GW', 'Guinea-Bissau', 'غينيا بيساو'),
(94, 'GY', 'Guyana', 'غينيا'),
(95, 'HK', 'Hong Kong', 'هونج كونج'),
(96, 'HM', 'Heard Island and McDonald Islands', 'جزيرة هيرد وجزر ماكدونالد'),
(97, 'HN', 'Honduras', 'هندوراس'),
(98, 'HR', 'Croatia', 'كرواتيا'),
(99, 'HT', 'Haiti', 'هايتي'),
(100, 'HU', 'Hungary', 'هنغاريا'),
(101, 'ID', 'Indonesia', 'أندونيسيا'),
(102, 'IE', 'Ireland', 'إيرلندا'),
(104, 'IM', 'Isle of Man', 'جزيرة آيل أوف مان'),
(105, 'IN', 'India', 'الهند'),
(106, 'IO', 'British Indian Ocean Territory', 'إقليم المحيط البريطاني الهندي'),
(107, 'IQ', 'Iraq', 'العراق'),
(108, 'IR', 'Iran', 'إيران'),
(109, 'IS', 'Iceland', 'إيزلاندا'),
(110, 'IT', 'Italy', 'إيطاليا'),
(111, 'JE', 'Jersey', 'جيريسي'),
(112, 'JM', 'Jamaica', 'جامايكا'),
(113, 'JO', 'Jordan', 'الأردن'),
(114, 'JP', 'Japan', 'اليابان'),
(115, 'KE', 'Kenya', 'كينيا'),
(116, 'KG', 'Kyrgyzstan', 'قيرغستان'),
(117, 'KH', 'Cambodia', 'كمبوديا'),
(118, 'KI', 'Kiribati', 'كيريباتي'),
(119, 'KM', 'Comoros', 'جزر القمر'),
(120, 'KN', 'Saint Kitts and Nevis', 'سانت كيتس ونيفيس'),
(121, 'KP', 'North Korea', 'كوريا الشمالية'),
(122, 'KR', 'South Korea', 'كوريا الجنوبية'),
(123, 'KW', 'Kuwait', 'الكويت'),
(124, 'KY', 'Cayman Islands', 'جزر كايمان'),
(125, 'KZ', 'Kazakhstan', 'كزخستان'),
(126, 'LA', 'Laos', 'لاووس'),
(127, 'LB', 'Lebanon', 'لبنان'),
(128, 'LC', 'Saint Lucia', 'سانت لوسيا'),
(129, 'LI', 'Liechtenstein', 'ليختنشتاين'),
(130, 'LK', 'Sri Lanka', 'سيريلنكا'),
(131, 'LR', 'Liberia', 'ليبيريا'),
(132, 'LS', 'Lesotho', 'ليسوتو'),
(133, 'LT', 'Lithuania', 'ليتوانيا'),
(134, 'LU', 'Luxembourg', 'لوكسيمبورغ'),
(135, 'LV', 'Latvia', 'لاتفيا'),
(136, 'LY', 'Libya', 'ليبيا'),
(137, 'MA', 'Morocco', 'المغرب'),
(138, 'MC', 'Monaco', 'موناكو'),
(139, 'MD', 'Moldova', 'مولدوفا'),
(140, 'ME', 'Montenegro', 'مونتينيغرو'),
(141, 'MF', 'Saint Martin', 'سانت مارتين'),
(142, 'MG', 'Madagascar', 'مدغشقر'),
(143, 'MH', 'Marshall Islands', 'جزر مارشال'),
(144, 'MK', 'Macedonia', 'مقدونيا'),
(145, 'ML', 'Mali', 'مالي'),
(146, 'MM', 'Myanmar [Burma]', 'ميامار بورما'),
(147, 'MN', 'Mongolia', 'منغوليا'),
(148, 'MO', 'Macao', 'ماكاو'),
(149, 'MP', 'Northern Mariana Islands', 'جزر مريانا الشمالية'),
(150, 'MQ', 'Martinique', 'مارتينيك'),
(151, 'MR', 'Mauritania', 'موريطانيا'),
(152, 'MS', 'Montserrat', 'مونتسيرات'),
(153, 'MT', 'Malta', 'مالطا'),
(154, 'MU', 'Mauritius', 'موريتيوس'),
(155, 'MV', 'Maldives', 'المالديف'),
(156, 'MW', 'Malawi', 'ملاوي'),
(157, 'MX', 'Mexico', 'المكسيك'),
(158, 'MY', 'Malaysia', 'ماليزيا'),
(159, 'MZ', 'Mozambique', 'الموزمبيق'),
(160, 'NA', 'Namibia', 'نمبيا'),
(161, 'NC', 'New Caledonia', 'كلدونيا الجديدة'),
(162, 'NE', 'Niger', 'النيجر'),
(163, 'NF', 'Norfolk Island', 'جزيرة نورفولك'),
(164, 'NG', 'Nigeria', 'نيجيريا'),
(165, 'NI', 'Nicaragua', 'نيكراقواي'),
(166, 'NL', 'Netherlands', 'هولندا'),
(167, 'NO', 'Norway', 'النرويج'),
(168, 'NP', 'Nepal', 'نيبال'),
(169, 'NR', 'Nauru', 'ناورو'),
(170, 'NU', 'Niue', 'نيوي'),
(171, 'NZ', 'New Zealand', 'نيوزيلندا'),
(172, 'OM', 'Oman', 'عمان'),
(173, 'PA', 'Panama', 'بنما'),
(174, 'PE', 'Peru', 'البيرو'),
(175, 'PF', 'French Polynesia', 'بولينيزيا الفرنسية'),
(176, 'PG', 'Papua New Guinea', 'بابوا غينيا الجديدة'),
(177, 'PH', 'Philippines', 'فيليبين'),
(178, 'PK', 'Pakistan', 'باكستان'),
(179, 'PL', 'Poland', 'بولندا'),
(180, 'PM', 'Saint Pierre and Miquelon', 'سان بيار وميكلون'),
(181, 'PN', 'Pitcairn Islands', 'جزر بيتكيرن'),
(182, 'PR', 'Puerto Rico', 'بويرتو ريكتو'),
(183, 'PS', 'Palestine', 'فلسطين'),
(184, 'PT', 'Portugal', 'البرتغال'),
(185, 'PW', 'Palau', 'بالاو'),
(186, 'PY', 'Paraguay', 'بوروغواي'),
(187, 'QA', 'Qatar', 'قطر'),
(189, 'RO', 'Romania', 'رومانيا'),
(190, 'RS', 'Serbia', 'صربيا'),
(191, 'RU', 'Russia', 'روسيا'),
(192, 'RW', 'Rwanda', 'رواندا'),
(193, 'SA', 'Saudi Arabia', 'المملكة العربية السعودية'),
(194, 'SB', 'Solomon Islands', 'جزر سليمان'),
(195, 'SC', 'Seychelles', 'سيشل'),
(196, 'SD', 'Sudan', 'السودان'),
(197, 'SE', 'Sweden', 'السويد'),
(198, 'SG', 'Singapore', 'سينغافورة'),
(199, 'SH', 'Saint Helena', 'سانت هيلينا'),
(200, 'SI', 'Slovenia', 'سلوفينيا'),
(201, 'SJ', 'Svalbard and Jan Mayen', 'سفالبارد وجان مايان'),
(202, 'SK', 'Slovakia', 'سلوفاكيا'),
(203, 'SL', 'Sierra Leone', 'سييرا ليون'),
(204, 'SM', 'San Marino', 'سان مارينو'),
(205, 'SN', 'Senegal', 'سينيغال'),
(206, 'SO', 'Somalia', 'الصومال'),
(207, 'SR', 'Suriname', 'سورينام'),
(208, 'SS', 'South Sudan', 'السودان الجنوبية'),
(210, 'SV', 'El Salvador', 'السلفادور'),
(211, 'SX', 'Sint Maarten', 'سينت مارتن'),
(212, 'SY', 'Syria', 'سوريا'),
(213, 'SZ', 'Swaziland', 'سوازيلاند'),
(214, 'TC', 'Turks and Caicos Islands', 'جزر تركس وكايكوس'),
(215, 'TD', 'Chad', 'تشاد'),
(216, 'TF', 'French Southern Territories', 'المناطق الجنوبية لفرنسا'),
(217, 'TG', 'Togo', 'طوغو'),
(218, 'TH', 'Thailand', 'تايلندا'),
(219, 'TJ', 'Tajikistan', 'طاجيكستان'),
(220, 'TK', 'Tokelau', 'توكيلو'),
(221, 'TL', 'East Timor', 'تيمور الشرقية'),
(222, 'TM', 'Turkmenistan', 'تركمنستان'),
(223, 'TN', 'Tunisia', 'تونس'),
(224, 'TO', 'Tonga', 'تونغا'),
(225, 'TR', 'Turkey', 'تركيا'),
(226, 'TT', 'Trinidad and Tobago', 'ترينداد وتوباغو'),
(227, 'TV', 'Tuvalu', 'توفالو'),
(228, 'TW', 'Taiwan', 'تايوان'),
(229, 'TZ', 'Tanzania', 'تنزانيا'),
(230, 'UA', 'Ukraine', 'أكرانيا'),
(231, 'UG', 'Uganda', 'أوغندا'),
(232, 'UM', 'U.S. Minor Outlying Islands', 'جزر الولايات المتحدة البعيدة الصغيرة'),
(233, 'US', 'United States', 'الولايات المتحدة الأمريكية'),
(234, 'UY', 'Uruguay', 'أوروغواي'),
(235, 'UZ', 'Uzbekistan', 'أزبكستان'),
(236, 'VA', 'Vatican City', 'الفاتيكان'),
(237, 'VC', 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين'),
(238, 'VE', 'Venezuela', 'فنزويلا'),
(239, 'VG', 'British Virgin Islands', 'جزر فيرجن البريطانية'),
(240, 'VI', 'U.S. Virgin Islands', 'جزر العذراء الأمريكية'),
(241, 'VN', 'Vietnam', 'فيتنام'),
(242, 'VU', 'Vanuatu', 'فانواتو'),
(243, 'WF', 'Wallis and Futuna', 'واليس وفوتونا'),
(244, 'WS', 'Samoa', 'ساموا'),
(245, 'XK', 'Kosovo', 'كوسوفو'),
(246, 'YE', 'Yemen', 'اليمن'),
(247, 'YT', 'Mayotte', 'مايوت'),
(248, 'ZA', 'South Africa', 'جنوب إفريقيا'),
(249, 'ZM', 'Zambia', 'زمبيا'),
(250, 'ZW', 'Zimbabwe', 'زمبابوي');

-- --------------------------------------------------------

--
-- Structure de la table `sms_templates`
--

CREATE TABLE `sms_templates` (
  `sms_template_id` int(2) NOT NULL,
  `arabic_new_app` text CHARACTER SET utf8,
  `english_new_app` text CHARACTER SET utf8,
  `arabic_accepted_app_from` text CHARACTER SET utf8,
  `english_accepted_app_from` text CHARACTER SET utf8,
  `arabic_final_acception` text CHARACTER SET utf8,
  `english_final_acception` text CHARACTER SET utf8,
  `arabic_final_acception_of` text CHARACTER SET utf8,
  `english_final_acception_of` text CHARACTER SET utf8,
  `arabic_refuse_app_from` text CHARACTER SET utf8,
  `english_refuse_app_from` text CHARACTER SET utf8,
  `arabic_refuse_of_from` text CHARACTER SET utf8,
  `english_refuse_of_from` text CHARACTER SET utf8,
  `arabic_delegate_of_from` text CHARACTER SET utf8,
  `english_delegate_of_from` text CHARACTER SET utf8,
  `arabic_consultation_of_from` text CHARACTER SET utf8,
  `english_consultation_of_from` text CHARACTER SET utf8,
  `arabic_pass_from` text CHARACTER SET utf8,
  `english_pass_from` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sms_templates`
--

INSERT INTO `sms_templates` (`sms_template_id`, `arabic_new_app`, `english_new_app`, `arabic_accepted_app_from`, `english_accepted_app_from`, `arabic_final_acception`, `english_final_acception`, `arabic_final_acception_of`, `english_final_acception_of`, `arabic_refuse_app_from`, `english_refuse_app_from`, `arabic_refuse_of_from`, `english_refuse_of_from`, `arabic_delegate_of_from`, `english_delegate_of_from`, `arabic_consultation_of_from`, `english_consultation_of_from`, `arabic_pass_from`, `english_pass_from`) VALUES
(1, 'لديك مطلب وارد جديد, تفقد صفحة الطلبات الواردة الخاصة بك.', 'you have a new received application, see your notification page', 'لقد تمت الموافقة على الطلب من قبل الموظف %s, و بإنتظار الموافقة النهائية.', 'An Application was Accepted from the employee %s, and waiting for the final acceptance', 'السلام عليكم ورحمة الله وبركاته. لقد تمت الموافقة على طلبك.', 'Your Application was Accepted', 'السلام عليكم ورحمة الله وبركاته. لقد تمت الموافقة على طلب الموظف %s, راجع صفحة أرشيف الطلبات.', 'The Application of %s was Accepted', 'السلام عليكم ورحمة الله وبركاته. لقد تم رفض طلبك من قبل %s, راجع صفحة الطلبات المرسلة الخاصة بك.', 'Your Application was refused from %s, see your sended application page', 'السلام عليكم ورحمة الله وبركاته. لقد تم رفض طلب الموظف %s من قبل %s راجع صفحة أرشيف الطلبات', 'The Application of %s was Refused from %s, see your applications archive page', 'لقد تمت تفويض الموافقة على الطلب  من %s إلى %s, و بإنتظار الموافقة النهائية.', 'Your Application was Delegated from %s to %s, waiting for the final acceptance', 'لقد تم إرسال إستشارة بخصوص الطلب  من الموظف %s إلى الموظف %s, و بإنتظار الرد.', 'Your Application had a consultation from %s to %s, waiting for the final acceptance', 'الموظف %s قام بالإطلاع على الطلب و تمريره إلى الموظف التالي.', 'the employee %s has seen the application and he passed it to the next employee'),
(2, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لديك مطلب وارد جديد, تفقد صفحة الطلبات الواردة الخاصة بك.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"font-size:18px\"><span style=\"color:#2980b9\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تمت الموافقة على الطلب من قبل الموظف &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot;, و بإنتظار الموافقة النهائية.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تمت الموافقة على طلبك.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تمت الموافقة على طلب الموظف&quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot;, راجع صفحة أرشيف الطلبات.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تم رفض طلبك من قبل &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot;, راجع صفحة الطلبات المرسلة الخاصة بك.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تم رفض طلب الموظف &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot; من قبل &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot; راجع صفحة أرشيف الطلبات.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تمت تفويض الموافقة على الطلب من &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot; إلى &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot;, و بإنتظار الموافقة النهائية</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">لقد تم إرسال إستشارة بخصوص الطلب من الموظف &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot; إلى الموظف &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot;, و بإنتظار الرد</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '<p>&nbsp;</p>\n\n<div style=\"background:#337ab7; color:#ffffff; padding:20px; text-align:center\">\n<h2>إدارة شؤون الموارد البشرية</h2>\n</div>\n\n<h1><img alt=\"\" height=\"140\" src=\"http://demo.smart-hr.top/img/logo.jpg\" style=\"float:left\" width=\"140\" /></h1>\n\n<h1><span style=\"color:#2980b9\"><span style=\"font-size:26px\"><strong><span style=\"font-family:Times New Roman,Times,serif\">السلام عليكم ورحمة الله وبركاته</span></strong></span></span></h1>\n\n<h2>&nbsp;</h2>\n\n<h3><br />\n<span style=\"font-family:Times New Roman,Times,serif\">الموظف &quot;<span style=\"color:#8e44ad\"><u>%s</u></span>&quot; قام بالإطلاع على الطلب و تمريره إلى الموظف التالي.</span></h3>\n\n<h3><span style=\"font-family:Times New Roman,Times,serif\">مع تحيات شركة ... &nbsp;شكرا</span></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_accountings`
--

CREATE TABLE `tbl_accountings` (
  `accounting_id` int(12) NOT NULL,
  `from_employee` int(10) NOT NULL,
  `to_employee` int(10) NOT NULL,
  `accounting_title` varchar(500) CHARACTER SET utf8 NOT NULL,
  `accounting_text` text CHARACTER SET utf8 NOT NULL,
  `employee_reply` text CHARACTER SET utf8 NOT NULL,
  `final_decision` text CHARACTER SET utf8 NOT NULL,
  `created_date` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_accountings`
--

INSERT INTO `tbl_accountings` (`accounting_id`, `from_employee`, `to_employee`, `accounting_title`, `accounting_text`, `employee_reply`, `final_decision`, `created_date`) VALUES
(1, 2, 2, 'fghjgfh', 'fghfhfgh', 'fghtfgh', 'rtju-r-ur-u', '2018-09-04'),
(2, 2, 2, 'erte', 'ertert', 'sdrgdrg', 'rfujrt', '2018-12-15');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_advance`
--

CREATE TABLE `tbl_advance` (
  `advance_id` int(11) NOT NULL,
  `title_ar` varchar(100) CHARACTER SET utf8 NOT NULL,
  `title_en` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_advance`
--

INSERT INTO `tbl_advance` (`advance_id`, `title_ar`, `title_en`) VALUES
(1, 'قرض سنوي', 'Annual loan'),
(2, '1', '1');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_advances`
--

CREATE TABLE `tbl_advances` (
  `advances_id` int(11) NOT NULL,
  `advance_type_id` int(11) NOT NULL,
  `advance_value` varchar(300) NOT NULL,
  `rest` varchar(300) NOT NULL DEFAULT '0',
  `advance_voucher` varchar(300) DEFAULT NULL,
  `employea_id` int(11) NOT NULL,
  `advance_date` varchar(200) NOT NULL,
  `last_date` varchar(300) DEFAULT NULL,
  `payement_method` tinyint(2) NOT NULL DEFAULT '1',
  `monthly_installement` varchar(200) NOT NULL DEFAULT '0',
  `payement_months` varchar(200) NOT NULL DEFAULT '0',
  `file` varchar(500) DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_advances`
--

INSERT INTO `tbl_advances` (`advances_id`, `advance_type_id`, `advance_value`, `rest`, `advance_voucher`, `employea_id`, `advance_date`, `last_date`, `payement_method`, `monthly_installement`, `payement_months`, `file`, `app_id`) VALUES
(2, 1, '444', '334', '58', 2, '2017-06-21', '2018-12-22', 2, '55', '203', NULL, 0),
(7, 1, '800', '800', '58', 2, '2018-09-04', NULL, 3, '200', '4', '121017.jpg', 0),
(5, 1, '333', '333', '58', 2, '2017-06-04', NULL, 3, '200', '2', 'bank.sql', 2),
(6, 1, '14', '14', '58', 2, '2017-06-13', NULL, 1, '1.17', '12', 'login.css', 0),
(8, 1, '200', '183.33', '', 2, '2018-12-13', '2018-12-22', 1, '16.67', '11', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_allowance`
--

CREATE TABLE `tbl_allowance` (
  `allowance_type_id` int(11) NOT NULL,
  `employe_id` int(11) NOT NULL,
  `allowance_id` int(11) NOT NULL,
  `allowance_value` varchar(20) NOT NULL,
  `allowance_type` varchar(200) NOT NULL,
  `allowance_date_from` date NOT NULL,
  `allowance_date_to` varchar(20) CHARACTER SET utf8 NOT NULL,
  `reservation` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_allowance`
--

INSERT INTO `tbl_allowance` (`allowance_type_id`, `employe_id`, `allowance_id`, `allowance_value`, `allowance_type`, `allowance_date_from`, `allowance_date_to`, `reservation`) VALUES
(3, 2, 1, '25', 'percent', '2018-12-10', '2018-12-31', '0'),
(4, 2, 2, '500', 'value', '2018-09-04', '2018-09-11', '10');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_allowance_category`
--

CREATE TABLE `tbl_allowance_category` (
  `allowance_id` int(11) NOT NULL,
  `allowance_title_ar` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `allowance_title_en` varchar(100) CHARACTER SET utf8 NOT NULL,
  `allowance_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `allowance_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_allowance_category`
--

INSERT INTO `tbl_allowance_category` (`allowance_id`, `allowance_title_ar`, `allowance_title_en`, `allowance_type`, `allowance_value`) VALUES
(1, 'بدل سكن', 'House allowance', 'percent', 25),
(2, 'بدل مواصلات', 'Transportation allowance', 'value', 500);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_applications`
--

CREATE TABLE `tbl_applications` (
  `application_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `accept` varchar(300) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `refuse` varchar(300) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `delegate` varchar(300) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `consultation` varchar(300) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `date` varchar(200) NOT NULL,
  `type_app` varchar(200) NOT NULL,
  `type` varchar(200) DEFAULT '0',
  `start_date` varchar(200) DEFAULT '0',
  `end_date` varchar(200) DEFAULT '0',
  `leave_duration` varchar(200) DEFAULT '0',
  `address_in_leave` varchar(300) DEFAULT '0',
  `tel_in_leave` varchar(200) DEFAULT '0',
  `replacement` varchar(300) DEFAULT '0',
  `tikets` varchar(200) DEFAULT '0',
  `going_date` varchar(200) DEFAULT '0',
  `coming_date` varchar(200) DEFAULT '0',
  `going_date_2` varchar(200) DEFAULT '0',
  `coming_date_2` varchar(200) DEFAULT '0',
  `note` text,
  `approval_cat_id` int(11) NOT NULL,
  `value` varchar(300) DEFAULT '0',
  `other_employee_id` int(11) DEFAULT '0',
  `name` varchar(300) DEFAULT '0',
  `new_jt` int(11) DEFAULT '0',
  `new_dep` int(11) DEFAULT '0',
  `new_sect` int(11) DEFAULT '0',
  `new_emp_cat` int(11) DEFAULT '0',
  `new_jtme` varchar(200) DEFAULT '0',
  `course_institute` varchar(300) DEFAULT '0',
  `prod_name` text,
  `prod_desc` text,
  `prod_num` varchar(300) DEFAULT '0',
  `prod_note` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `ids` varchar(300) NOT NULL,
  `results` varchar(300) NOT NULL,
  `current` int(10) NOT NULL DEFAULT '0',
  `id_no` varchar(300) NOT NULL DEFAULT '0',
  `id_no_end` varchar(300) NOT NULL DEFAULT '0',
  `nationality` int(11) NOT NULL DEFAULT '0',
  `sex` varchar(30) NOT NULL DEFAULT '0',
  `birth_date` varchar(300) NOT NULL DEFAULT '0',
  `phone` varchar(300) NOT NULL DEFAULT '0',
  `maratial_status` varchar(300) NOT NULL DEFAULT '0',
  `job_time` varchar(300) NOT NULL DEFAULT '0',
  `photo` varchar(300) NOT NULL DEFAULT '0',
  `file` text,
  `payement_method` tinyint(2) NOT NULL DEFAULT '1',
  `monthly_installement` varchar(200) NOT NULL DEFAULT '0',
  `payement_months` varchar(200) NOT NULL DEFAULT '0',
  `branch_name` varchar(300) DEFAULT NULL,
  `account_name` varchar(300) DEFAULT NULL,
  `account_number` varchar(300) DEFAULT NULL,
  `bank_name` varchar(300) DEFAULT NULL,
  `caching_reason` text,
  `full_name_ar` varchar(300) CHARACTER SET ucs2 DEFAULT NULL,
  `full_name_en` varchar(300) DEFAULT NULL,
  `bill_number` varchar(300) DEFAULT NULL,
  `item_no` varchar(300) DEFAULT NULL,
  `country` varchar(300) DEFAULT NULL,
  `swift_code` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `account_holder_name` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_applications`
--

INSERT INTO `tbl_applications` (`application_id`, `employee_id`, `accept`, `refuse`, `delegate`, `consultation`, `date`, `type_app`, `type`, `start_date`, `end_date`, `leave_duration`, `address_in_leave`, `tel_in_leave`, `replacement`, `tikets`, `going_date`, `coming_date`, `going_date_2`, `coming_date_2`, `note`, `approval_cat_id`, `value`, `other_employee_id`, `name`, `new_jt`, `new_dep`, `new_sect`, `new_emp_cat`, `new_jtme`, `course_institute`, `prod_name`, `prod_desc`, `prod_num`, `prod_note`, `status`, `ids`, `results`, `current`, `id_no`, `id_no_end`, `nationality`, `sex`, `birth_date`, `phone`, `maratial_status`, `job_time`, `photo`, `file`, `payement_method`, `monthly_installement`, `payement_months`, `branch_name`, `account_name`, `account_number`, `bank_name`, `caching_reason`, `full_name_ar`, `full_name_en`, `bill_number`, `item_no`, `country`, `swift_code`, `city`, `account_holder_name`) VALUES
(1, 2, '1;1', '1;1', '1;1', '1;1', '2017-07-23', 'la_custody', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'السبب', 1, '0', 0, 'الإسم', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 0, '58;2;61', '1;1;0', 2, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, '1;1', '1;1', '1;1', '1;1', '2017-07-24', 'la_def_salary', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'السبب', 1, '0', 0, 'عنوان الطلب', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 0, '58;2;61', '0;0;0', 0, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 58, '1;1;1;1', '1;1;1;1', '2;1;1;1', '2;1;1;1', '2018-01-23', 'la_vacations', '1', '2018-01-30', '2018-01-31', '2', 'hugyhtgrfe', '8754', '2', '1', '', '', '', '', 'uijyhtg', 2, '0', 0, '0', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 0, '2;58;2;63', '1;1;0;0', 2, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 58, '1;1', '1;1', '1;1', '1;1', '2017-09-14', 'la_maintenance', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'نص الرسالة', 1, '0', 0, 'عنوان الطلب', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 1, '58;2;61', '1;1;1', 3, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 61, '1;1;1;1', '1;1;1;1', '2;1;1;1', '2;1;1;1', '2017-12-25', 'la_vacations', '1', '2017-12-26', '2017-12-28', '3', 'عنوان الموظف أثناء الإجازة *', '45656456', '58', '3', '', '', '', '', 'السبب', 2, '0', 0, '0', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 1, '58;58;2;63', '1;1;1;1', 4, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 2, '1;1;1;1', '1;1;1;1', '2;1;1;1', '2;1;1;1', '2018-12-23', 'la_vacations', '1', '2018-12-12', '2018-12-20', '9', 'test', 'test', '2', '3', '', '', '', '', 'test test', 2, '0', 0, '0', 0, 0, 0, 0, '0', '0', NULL, NULL, '0', NULL, 0, '2;2;58;63', '0;0;0;0', 0, '0', '0', 0, '0', '0', '0', '0', '0', '0', NULL, 1, '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_application_notes`
--

CREATE TABLE `tbl_application_notes` (
  `note_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `action` tinyint(2) NOT NULL,
  `note_reason` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf32;

--
-- Déchargement des données de la table `tbl_application_notes`
--

INSERT INTO `tbl_application_notes` (`note_id`, `application_id`, `employee_id`, `action`, `note_reason`) VALUES
(1, 1, 58, 1, 'السبب'),
(2, 1, 2, 4, 'السبب'),
(3, 4, 58, 1, 'ملاحظة أولى'),
(4, 5, 58, 1, ''),
(5, 5, 2, 1, ''),
(6, 5, 61, 5, ''),
(7, 7, 58, 1, ''),
(8, 7, 58, 1, 'السبب'),
(9, 7, 2, 1, 'الأمرالأمرالأمر'),
(10, 7, 63, 1, 'السببالسببالسبب'),
(11, 8, 2, 1, ''),
(12, 1, 2, 1, ''),
(13, 8, 58, 1, 'qsdqsd');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_approvals_cat`
--

CREATE TABLE `tbl_approvals_cat` (
  `approval_cat_id` int(11) NOT NULL,
  `approval_cat_en` varchar(200) NOT NULL,
  `approval_cat_ar` varchar(200) NOT NULL,
  `approval_cat_status` tinyint(2) NOT NULL COMMENT '0=inactive, 1=active',
  `jplc` int(11) NOT NULL DEFAULT '0',
  `dep` int(11) NOT NULL DEFAULT '0',
  `sect` int(11) NOT NULL DEFAULT '0',
  `unit` int(11) NOT NULL DEFAULT '0',
  `divi` int(11) NOT NULL DEFAULT '0',
  `hr_admin` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `admins` varchar(200) DEFAULT NULL,
  `employees` varchar(200) NOT NULL DEFAULT 'dm',
  `accept` varchar(200) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `refuse` varchar(200) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `delegate` varchar(200) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `consultation` varchar(200) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0',
  `order_list` varchar(200) DEFAULT NULL,
  `all_list` varchar(200) DEFAULT NULL,
  `automatic` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `approval_cat_delay` int(11) NOT NULL DEFAULT '5',
  `approval_cat_times` int(2) NOT NULL DEFAULT '1',
  `approval_cat_action` int(2) NOT NULL DEFAULT '1',
  `linked_apps` varchar(600) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_approvals_cat`
--

INSERT INTO `tbl_approvals_cat` (`approval_cat_id`, `approval_cat_en`, `approval_cat_ar`, `approval_cat_status`, `jplc`, `dep`, `sect`, `unit`, `divi`, `hr_admin`, `admins`, `employees`, `accept`, `refuse`, `delegate`, `consultation`, `order_list`, `all_list`, `automatic`, `approval_cat_delay`, `approval_cat_times`, `approval_cat_action`, `linked_apps`) VALUES
(1, 'all applications', 'كل الطلبات', 1, 0, 0, 0, 0, 0, 1, '2;58', 'dm;61', '1;1;1', '1;1;1', '1;1;1', '1;1;1', '1;2;3;4', '2;58;dm;61', 1, 5, 1, 1, 'la_advance/0,la_caching,la_filter_dues,la_custody,la_resignation,la_transfer,la_training,la_overtime,la_purchase,la_maintenance,la_annual_assessment,la_permission,la_def_salary,la_recrutement,la_job_application,la_embarkation'),
(2, 'طلبات إجازات وغيرها', 'طلبات إجازات وغيرها', 1, 0, 0, 0, 0, 0, 1, '58', 'dm;63', '1;1;1', '1;1;1', '1;1;1', '1;1;1', '1;2;3', 'dm;58;63', 1, 5, 1, 1, 'la_vacations/0'),
(3, 'talab ijaza', 'طلب إجازة', 1, 0, 1, 0, 0, 0, 1, NULL, 'dm', '0;0;0;0;0;0;0;0;0;0;0', '0;0;0;0;0;0;0;0;0;0;0', '0;0;0;0;0;0;0;0;0;0;0', '0;0;0;0;0;0;0;0;0;0;0', NULL, NULL, 1, 5, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendance_id` int(11) NOT NULL,
  `employee_att_id` int(11) NOT NULL,
  `employee_name` varchar(500) NOT NULL,
  `employee_status` int(10) NOT NULL COMMENT '1=active, 2=blocked',
  `Action` int(10) NOT NULL,
  `job_code` varchar(300) NOT NULL,
  `att_date` varchar(300) DEFAULT NULL,
  `att_time` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`attendance_id`, `employee_att_id`, `employee_name`, `employee_status`, `Action`, `job_code`, `att_date`, `att_time`) VALUES
(1, 2, 'Issam', 1, 1, 'hr_admin', '2017/04/28', '12:12:55'),
(2, 58, 'Mohamed', 1, 1, 'super_admin', '2017/04/29', '10:53:46'),
(3, 2, 'Içam', 1, 1, 'hr_admin', '2017/06/27', '00:00:00'),
(4, 2, 'Içam', 1, 1, 'hr_admin', '2018/09/04', '14:40:11'),
(5, 2, 'Içam', 1, 2, 'hr_admin', '2018/09/04', '14:42:25'),
(6, 2, 'Içam', 1, 1, 'hr_admin', '2018/12/14', '17:25:03'),
(7, 2, 'Içam', 1, 2, 'hr_admin', '2018/12/14', '20:25:44');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_attendance_xh`
--

CREATE TABLE `tbl_attendance_xh` (
  `attendance_xh_id` int(11) NOT NULL,
  `employee_xh_id` int(11) NOT NULL,
  `employee_name` varchar(300) CHARACTER SET utf8 NOT NULL,
  `employee_status` int(10) NOT NULL,
  `Action` int(11) NOT NULL,
  `job_code` varchar(300) CHARACTER SET utf8 NOT NULL,
  `att_date` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `att_time` varchar(300) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_attendance_xh`
--

INSERT INTO `tbl_attendance_xh` (`attendance_xh_id`, `employee_xh_id`, `employee_name`, `employee_status`, `Action`, `job_code`, `att_date`, `att_time`) VALUES
(1, 2, 'Içam', 1, 1, 'hr_admin', '2017/07/10', '10:28:04'),
(2, 2, 'Içam', 1, 1, 'hr_admin', '2018/09/04', '14:21:47'),
(3, 2, 'Içam', 1, 2, 'hr_admin', '2018/09/04', '14:21:52'),
(4, 2, 'Içam', 1, 1, 'hr_admin', '2018/09/04', '14:44:02'),
(5, 2, 'Içam', 1, 1, 'hr_admin', '2018/12/14', '17:31:10'),
(6, 2, 'Içam', 1, 1, 'hr_admin', '2018/12/14', '17:32:02'),
(7, 2, 'Içam', 1, 2, 'hr_admin', '2018/12/14', '17:32:06');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_bonuses`
--

CREATE TABLE `tbl_bonuses` (
  `bonuse_id` int(11) NOT NULL,
  `title_ar` varchar(10) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `assurance` varchar(11) NOT NULL,
  `guarantee` varchar(11) NOT NULL,
  `departure` varchar(11) NOT NULL,
  `brief_ar` text NOT NULL,
  `brief_en` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_branches`
--

CREATE TABLE `tbl_branches` (
  `branche_id` int(11) NOT NULL,
  `branche_ar` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `branche_en` varchar(200) NOT NULL,
  `branche_desc_ar` text NOT NULL,
  `branche_desc_en` text NOT NULL,
  `lat` varchar(300) NOT NULL,
  `lng` varchar(300) NOT NULL,
  `zoom` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_branches`
--

INSERT INTO `tbl_branches` (`branche_id`, `branche_ar`, `branche_en`, `branche_desc_ar`, `branche_desc_en`, `lat`, `lng`, `zoom`) VALUES
(1, 'الفرع الرئيسي', 'Main Branche', 'الوصف', 'description', '-34.016241889667015', '129.254150390625', 2),
(2, 'الاعلام والتسويق الالكتروني', 'markting', 'ادارة الإعلام والتسويق الالكتروني ', 'test', '', '', 2),
(3, 'الموارد البشرية ', 'HR', 'ادارة الموارد البشرية تختص بالموظفين واجازاتهم والحضور والانصراف وكل شي', '', '', '', 2),
(4, 'تنمية الموارد', 'Resource Development', 'تهتم بجمع وتنمية موارد الوقف', '', '', '', 2);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_caching`
--

CREATE TABLE `tbl_caching` (
  `caching_id` int(11) NOT NULL,
  `employec_id` int(11) NOT NULL,
  `caching_date` varchar(200) NOT NULL,
  `cahing_type` int(2) NOT NULL DEFAULT '0',
  `name` varchar(300) NOT NULL,
  `bill_number` varchar(1000) NOT NULL,
  `item_no` varchar(300) NOT NULL,
  `bank_name` varchar(500) NOT NULL,
  `account_holder_name` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL,
  `account_number` varchar(500) NOT NULL,
  `address_in_leave` varchar(500) NOT NULL,
  `swift_code` varchar(500) NOT NULL,
  `city` varchar(500) NOT NULL,
  `caching_reason` text NOT NULL,
  `value` varchar(300) NOT NULL,
  `file` varchar(500) DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_caching`
--

INSERT INTO `tbl_caching` (`caching_id`, `employec_id`, `caching_date`, `cahing_type`, `name`, `bill_number`, `item_no`, `bank_name`, `account_holder_name`, `phone`, `country`, `account_number`, `address_in_leave`, `swift_code`, `city`, `caching_reason`, `value`, `file`, `app_id`) VALUES
(1, 2, '2017-06-04', 1, 'sdgsdgf', '64543489', 'sdfsdf', 'sgsg', 'sdgsdg', '232424', 'sdgsdsd', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdfsdf', '2442', 'حفظ_الطلاب_بالأجزاء2.docx', 0),
(2, 2, '2017-06-04', 0, 'عصام', 'رقم الفاتورة', 'رقم البند', 'اسم البنك', 'اسم صاحب الحساب', '0021699961102', 'United Kingdom', 'سيلسل', 'dththd', 'sgfsdfsdf', 'dfgdfg', 'الغرض', '111', 'حفظ_الطلاب_بالأجزاء10.docx', 1),
(3, 2, '2018-12-15', 0, 'aze', 'aze', 'aze', 'zer', 'zer', '65465465465', 'zerzer', 'zer', 'zer', 'zer', 'zer', 'zer', '55', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_center_documentations`
--

CREATE TABLE `tbl_center_documentations` (
  `doc_id` int(11) NOT NULL,
  `doc_name_ar` varchar(250) NOT NULL,
  `doc_name_en` varchar(250) NOT NULL,
  `link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_center_documentations`
--

INSERT INTO `tbl_center_documentations` (`doc_id`, `doc_name_ar`, `doc_name_en`, `link`) VALUES
(1, 'وثيقة', 'document', 'a.doc'),
(2, 'test', 'test', '');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `course_id` int(11) NOT NULL,
  `employeetr_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `course_institute_name` varchar(1000) NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  `course_price` int(30) NOT NULL,
  `course_note` text NOT NULL,
  `file` varchar(500) DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_courses`
--

INSERT INTO `tbl_courses` (`course_id`, `employeetr_id`, `course_name`, `course_institute_name`, `start_date`, `end_date`, `course_price`, `course_note`, `file`, `app_id`) VALUES
(1, 2, 'إسم الدورة', 'مؤسسة التدريب', '2017-06-12', '2017-06-13', 42, 'ملاحظات', 'حفظ_الطلاب_بالأجزاء3.docx', 0),
(2, 2, 'rthyreyt', 'zertert', '2018-12-21', '2018-12-28', 44, 'ert', '3.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_deduction`
--

CREATE TABLE `tbl_deduction` (
  `deduction_type_id` int(11) NOT NULL,
  `employd_id` int(11) NOT NULL,
  `deduction_id` int(11) NOT NULL,
  `deduction_type` varchar(20) NOT NULL,
  `deduction_value` varchar(300) NOT NULL,
  `start_date` varchar(300) NOT NULL,
  `end_date` varchar(300) DEFAULT NULL,
  `note_deduction` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_deduction`
--

INSERT INTO `tbl_deduction` (`deduction_type_id`, `employd_id`, `deduction_id`, `deduction_type`, `deduction_value`, `start_date`, `end_date`, `note_deduction`) VALUES
(1, 2, 12, 'percent', '10', '2018-08-13', '2018-09-11', ''),
(3, 2, 12, 'value', '100', '2018-12-01', '2019-01-31', 'sdefgsdfsdf');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_deduction_category`
--

CREATE TABLE `tbl_deduction_category` (
  `deduction_id` int(11) NOT NULL,
  `deduction_title_en` varchar(100) NOT NULL,
  `deduction_title_ar` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `deduction_type` varchar(11) NOT NULL,
  `deduction_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_deduction_category`
--

INSERT INTO `tbl_deduction_category` (`deduction_id`, `deduction_title_en`, `deduction_title_ar`, `deduction_type`, `deduction_value`) VALUES
(9, '????', 'سلفة', 'percent', 25),
(11, 'For transportation allowance', 'عن بدل المواصلات', 'value', 180),
(12, 'khatiyya', 'خطية', 'percent', 10);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_department`
--

CREATE TABLE `tbl_department` (
  `department_id` int(5) NOT NULL,
  `branche_id` int(11) NOT NULL,
  `employee_id` varchar(11) DEFAULT NULL,
  `employee_rep_id` varchar(11) DEFAULT NULL,
  `department_name` varchar(100) NOT NULL,
  `department_name_ar` varchar(100) NOT NULL,
  `department_desc_ar` text,
  `department_desc_en` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_department`
--

INSERT INTO `tbl_department` (`department_id`, `branche_id`, `employee_id`, `employee_rep_id`, `department_name`, `department_name_ar`, `department_desc_ar`, `department_desc_en`) VALUES
(1, 1, '2', '', 'default Department', 'إدارة إفتراضية', 'الوصف', 'Description'),
(2, 1, '61', '', 'secondary department', 'إدارة فرعية', 'الوصف', ''),
(3, 1, '', '', 'test', 'test', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_designations`
--

CREATE TABLE `tbl_designations` (
  `designations_id` int(5) NOT NULL,
  `employee_id` varchar(100) DEFAULT '0',
  `department_id` int(11) NOT NULL,
  `designations` varchar(100) NOT NULL,
  `designations_ar` varchar(100) NOT NULL,
  `designations_desc_en` text,
  `designations_desc_ar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_designations`
--

INSERT INTO `tbl_designations` (`designations_id`, `employee_id`, `department_id`, `designations`, `designations_ar`, `designations_desc_en`, `designations_desc_ar`) VALUES
(1, '2', 1, 'Default Section', 'القسم الإفتراضي', 'Description', 'الوصف'),
(3, '', 1, 'test', 'test', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_discount_category`
--

CREATE TABLE `tbl_discount_category` (
  `discount_id` int(11) NOT NULL,
  `discount_title_ar` varchar(100) NOT NULL,
  `discount_title_en` varchar(100) NOT NULL,
  `discount_type` varchar(20) NOT NULL,
  `discount_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_divisions`
--

CREATE TABLE `tbl_divisions` (
  `division_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `division_ar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `division_en` varchar(200) CHARACTER SET utf8 NOT NULL,
  `div_employee_id` varchar(200) CHARACTER SET utf8 DEFAULT '0',
  `div_desc_ar` text CHARACTER SET utf8,
  `div_desc_en` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tbl_divisions`
--

INSERT INTO `tbl_divisions` (`division_id`, `unit_id`, `division_ar`, `division_en`, `div_employee_id`, `div_desc_ar`, `div_desc_en`) VALUES
(1, 1, 'الشعبة الإفتراضية', 'Default Division', '2', 'الوصف', 'Description');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_draft`
--

CREATE TABLE `tbl_draft` (
  `draft_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `to` text NOT NULL,
  `subject` varchar(300) NOT NULL,
  `message_body` text NOT NULL,
  `attach_file` varchar(200) DEFAULT NULL,
  `attach_file_path` text,
  `attach_filename` text,
  `message_time` varchar(200) DEFAULT NULL,
  `deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_draft`
--

INSERT INTO `tbl_draft` (`draft_id`, `user_id`, `employee_id`, `to`, `subject`, `message_body`, `attach_file`, `attach_file_path`, `attach_filename`, `message_time`, `deleted`) VALUES
(1, 1, NULL, 'a:1:{i:0;s:20:\"hamedissam@gmail.com\";}', 'test', '<p>tsetskjsbgksbg sg sdg</p>\n', NULL, NULL, NULL, '2017-05-25 05:00:04', 'Yes'),
(2, 1, NULL, 'a:1:{i:0;s:20:\"hamedissam@gmail.com\";}', '0', '<p>test</p>\n\n<p>&quot;&gt;</p>\n\n<p><!--?php  echo \"hello\";?--></p>\n', NULL, NULL, NULL, '2018-11-14 04:27:03', 'No');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_embarkation`
--

CREATE TABLE `tbl_embarkation` (
  `embarkation_id` int(10) NOT NULL,
  `employermb_id` int(10) NOT NULL,
  `date1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `date2` varchar(100) CHARACTER SET utf8 NOT NULL,
  `rest` int(10) NOT NULL,
  `note` text CHARACTER SET utf8 NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_embarkation`
--

INSERT INTO `tbl_embarkation` (`embarkation_id`, `employermb_id`, `date1`, `date2`, `rest`, `note`, `file`, `app_id`) VALUES
(1, 2, '2017-06-13', '2017-06-21', 3, 'ملاحظات', 'حفظ_الطلاب_بالأجزاء9.docx', 0),
(2, 2, '2018-09-04', '2018-09-05', 6, 'drg', NULL, 0),
(3, 2, '2018-12-28', '2018-12-25', 0, 'qsdqsd', '2.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `employee_id` int(5) NOT NULL,
  `reference` int(11) NOT NULL,
  `employment_id` varchar(200) NOT NULL,
  `full_name_ar` varchar(100) NOT NULL,
  `full_name_en` varchar(100) NOT NULL,
  `date_of_birth` varchar(11) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `maratial_status` varchar(20) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `passport_number` varchar(100) DEFAULT NULL,
  `passport_end` varchar(11) NOT NULL DEFAULT '0',
  `identity_no` varchar(100) DEFAULT NULL,
  `identity_end` varchar(11) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `photo_a_path` varchar(150) DEFAULT NULL,
  `present_address` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `designations_id` varchar(11) NOT NULL DEFAULT '0',
  `departement_id` int(11) NOT NULL,
  `direct_manager_id` int(11) NOT NULL DEFAULT '0',
  `joining_date` varchar(200) DEFAULT NULL,
  `retirement_date` varchar(200) DEFAULT NULL,
  `job_time` varchar(20) NOT NULL,
  `job_time_hours` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 2=blocked',
  `job_title` varchar(100) NOT NULL,
  `holiday_no` int(11) NOT NULL,
  `med_insur` varchar(10) NOT NULL DEFAULT '0',
  `med_insur_type` varchar(10) NOT NULL DEFAULT '0',
  `social_insurance` varchar(10) NOT NULL DEFAULT '0',
  `social_insurance_type` varchar(10) NOT NULL DEFAULT '0',
  `wife_name` varchar(100) NOT NULL,
  `wife_name_ar` varchar(100) NOT NULL,
  `wife_birth` varchar(11) NOT NULL,
  `fils_name` varchar(1000) DEFAULT NULL,
  `fils_name_ar` varchar(1000) DEFAULT NULL,
  `fils_birth` varchar(1000) DEFAULT NULL,
  `education` varchar(100) NOT NULL,
  `job_place_id` int(11) NOT NULL,
  `employee_category_id` int(11) DEFAULT NULL,
  `employee_salary` double NOT NULL,
  `bank_name` varchar(600) DEFAULT NULL,
  `branch_name` varchar(600) DEFAULT NULL,
  `account_name` varchar(200) DEFAULT NULL,
  `account_number` varchar(200) DEFAULT NULL,
  `id_proff` varchar(1000) DEFAULT NULL,
  `cin_photo_path` varchar(1000) DEFAULT NULL,
  `passport_photo_path` varchar(1000) DEFAULT NULL,
  `resume_path` varchar(1000) DEFAULT NULL,
  `contract_paper_path` varchar(1000) DEFAULT NULL,
  `other_document_path` varchar(1000) DEFAULT NULL,
  `houcing_status` tinyint(2) DEFAULT '0',
  `houcing_type` tinyint(2) DEFAULT '0',
  `houcing_value` int(200) DEFAULT '0',
  `old_balance` varchar(200) NOT NULL DEFAULT '0',
  `new_balance` varchar(200) NOT NULL DEFAULT '0',
  `old_rest` varchar(300) NOT NULL DEFAULT '0',
  `reset_old_balance` varchar(200) DEFAULT '0',
  `reset_new_balance` varchar(200) NOT NULL DEFAULT '0',
  `reset_old_rest` varchar(300) NOT NULL DEFAULT '0',
  `test_period` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_employee`
--

INSERT INTO `tbl_employee` (`employee_id`, `reference`, `employment_id`, `full_name_ar`, `full_name_en`, `date_of_birth`, `gender`, `maratial_status`, `nationality`, `passport_number`, `passport_end`, `identity_no`, `identity_end`, `photo`, `photo_a_path`, `present_address`, `phone`, `email`, `designations_id`, `departement_id`, `direct_manager_id`, `joining_date`, `retirement_date`, `job_time`, `job_time_hours`, `status`, `job_title`, `holiday_no`, `med_insur`, `med_insur_type`, `social_insurance`, `social_insurance_type`, `wife_name`, `wife_name_ar`, `wife_birth`, `fils_name`, `fils_name_ar`, `fils_birth`, `education`, `job_place_id`, `employee_category_id`, `employee_salary`, `bank_name`, `branch_name`, `account_name`, `account_number`, `id_proff`, `cin_photo_path`, `passport_photo_path`, `resume_path`, `contract_paper_path`, `other_document_path`, `houcing_status`, `houcing_type`, `houcing_value`, `old_balance`, `new_balance`, `old_rest`, `reset_old_balance`, `reset_new_balance`, `reset_old_rest`, `test_period`) VALUES
(2, 3207, 'hr_admin', 'عصام', 'Içam', '1986-01-13', 'Male', 'Married', '2', '', '', '2271740074', '2027-03-9', '', NULL, 'العنوان', '0021699961102', 'hamedissam@gmail.com', '1', 1, 2, '2015-02-25', '2018-03-11', 'Full', 0, 1, '4', 30, '1', '2', '1', 'non-saudi', 'wife', 'الزوجة', '2007-08-1', 'son;;;;', 'الإبن;;;;', '2017-04-9;', 'إسم الشهادة', 1, 1, 3400, 'اسم البنك', 'اسم الفرع', 'إسم الحساب', '531608010017554', '%D9%88%D8%AB%D9%8A%D9%82%D8%A9.doc', '', '', '', '', '', 1, 1, 10, '40', '22', '48', '0', '40', '58', 0),
(58, 1198, 'super_admin', 'محمد', 'Mohamed', '2017-04-27', 'Male', 'Married', '12', '0000000000000', '2027-01-6', '', '', 'img/uploads/arton30389110.jpg', NULL, 'العنوان', '0021699961102', 'hamedissam217@gmail.com', '1', 1, 2, '2017-04-22', '', 'Full', 0, 1, '1', 44, '0', '0', '0', '0', '', '', '', ';;;;', ';;;;', '', 'إجازة', 1, 1, 4400, '', '', '', '', '%D9%88%D8%AB%D9%8A%D9%82%D8%A91.doc', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '0', '0', '0', '0', 0),
(61, 6404, 'employee1', 'موظف أول', 'first employee', '2017-05-15', 'Male', 'Married', '23', '123456', '2030-01-15', '', '', 'img/uploads/images.jpg', NULL, 'لبنان', '0021699961102', 'hamedissam@gmail.com', '1', 1, 2, '2017-05-14', '', 'Full', 0, 1, '6', 30, '0', '0', '0', '0', '', '', '', ';;;;', ';;;;', '', 'شهادة في الهندسة المعمارية', 1, 1, 5000, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '30', '0', '0', '0', 0),
(62, 9092, '6342', 'محمد قاسم ثابت النهاري', 'mohammed al nahari', '6-06-05', 'Male', 'Married', '246', '', '', '10666655541', '2022-05-12', '', NULL, 'istanbul', '055555555555', 'm.alnahary1@gmail.com', '0', 2, 58, '2017-07-20', '2018-07-9', 'Full', 0, 1, '5', 30, '1', '1', '2', 'saudi1', '', '', '', ';;;;', ';;;;', ';;;;', 'بكالويروس', 1, 2, 5000, '', '', 'admin', '', 'admin.png', '', '', '', '', '', 0, 0, 0, '0', '10', '90', '0', '0', '60', 23),
(63, 1679, 'employee2', 'موظف ثان', 'second employee', '1998-05-29', 'Male', 'Married', '62', '778787767', '2031-09-1', '', '', 'img/uploads/category-leadership-management-3.jpg', NULL, 'الجزائر', '0021699961102', 'devweb218@gmail.com', '0', 2, 61, '2017-06-29', '', 'Full', 0, 1, '5', 33, '1', '1', '1', 'non-saudi', '', '', '', ';;;;', ';;;;', '', 'شهادة محاسبة', 1, 1, 2300, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '0', '0', '0', '0', 0),
(64, 7373, '٣٠٠٥', 'saleh', 'almoaish', '2018-05-3', 'Male', 'Married', '65', '4554566514', '2021-04-20', '', '', NULL, NULL, 'السعودية', '0559353708', 'almoaish@gmail.com', '1', 1, 2, '2017-04-28', '', 'Full', 0, 1, '5', 30, '0', '0', '0', '0', '', '', '', ';;;;', ';;;;', '', 'بكالريوس', 1, 1, 4000, '', '', 'admin', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '0', '0', '0', '0', 0),
(65, 5894, 'WhrnsZlU', 'test', 'test', '2018-11-20', 'Male', 'Married', '10', '541521521052', '2018-12-5', '52585258525852', '2018-11-20', NULL, NULL, '', '', '', '0', 2, 2, '2018-12-4', '2018-11-26', 'Full', 0, 1, '4', 52, '1', '1', '1', 'saudi1', '', '', '', ';;;;', ';;;;', '', 'test', 1, 1, 5252, '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '0', '0', '0', '0', 0),
(66, 9862, 'WDXRHOyz', 'صثقصثق', 'صثقصثق', '2018-12-06', 'Male', 'Married', '10', '6846545432', '2018-12-26', '7885456454', '2018-12-19', 'img/uploads/Nature23.jpg', NULL, 'sdfsdf', '654564564', 'sdjkfsdf@sdf.Sdfg', '1', 1, 2, '2018-12-21', '2018-12-11', 'Full', 0, 1, '1', 22, '2', '0', '2', '0', 'sdf', 'dfs', '2018-12-27', 'sdf;sdf;sdf;;', 'sdf;sdf;sdf;;', '2018-12-20;2018-12-14;2018-12-06;;', 'sdfsdf', 1, 2, 2217, 'sdf', 'sdf', 'sdf', 'sdf', 'Water8.jpg', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0', '0', '0', '0', '0', '0', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_employee_award`
--

CREATE TABLE `tbl_employee_award` (
  `employee_award_id` int(11) NOT NULL,
  `award_name` varchar(100) NOT NULL,
  `employee_id` int(2) NOT NULL,
  `gift_item` varchar(300) NOT NULL,
  `award_amount` int(5) NOT NULL,
  `award_date` varchar(10) NOT NULL,
  `view_status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=Read 2=Unread',
  `notify_me` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=on 0=off'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_employee_custody`
--

CREATE TABLE `tbl_employee_custody` (
  `custody_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `name_ar` varchar(100) CHARACTER SET utf8 NOT NULL,
  `name_en` varchar(100) CHARACTER SET utf8 NOT NULL,
  `delivery_date` date NOT NULL,
  `nombre` int(11) NOT NULL,
  `custody_reference` int(11) NOT NULL,
  `description_ar` text CHARACTER SET utf8 NOT NULL,
  `description_en` text CHARACTER SET utf8 NOT NULL,
  `document` varchar(350) CHARACTER SET utf8 DEFAULT NULL,
  `received` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_employee_custody`
--

INSERT INTO `tbl_employee_custody` (`custody_id`, `employee_id`, `name_ar`, `name_en`, `delivery_date`, `nombre`, `custody_reference`, `description_ar`, `description_en`, `document`, `received`) VALUES
(1, 2, 'gdfty', 'Saudi Kitchen', '2018-09-04', 99, 612809, '', '', NULL, 1),
(2, 2, 'zerzer', 'zerzer', '2018-12-19', 99, 405415, 'kjhk', '', NULL, 1),
(3, 2, 'qsd', 'qsd', '2018-12-28', 212, 2781562, 'qsd', '', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_employee_document`
--

CREATE TABLE `tbl_employee_document` (
  `document_id` int(5) NOT NULL,
  `employee_id` int(2) NOT NULL,
  `resume` varchar(300) DEFAULT NULL,
  `resume_path` varchar(300) DEFAULT NULL,
  `resume_filename` varchar(100) DEFAULT NULL,
  `offer_letter` varchar(300) DEFAULT NULL,
  `offer_letter_filename` varchar(200) DEFAULT NULL,
  `offer_letter_path` varchar(300) DEFAULT NULL,
  `joining_letter` varchar(300) DEFAULT NULL,
  `joining_letter_filename` varchar(200) DEFAULT NULL,
  `joining_letter_path` varchar(300) DEFAULT NULL,
  `contract_paper` varchar(300) DEFAULT NULL,
  `contract_paper_filename` varchar(200) DEFAULT NULL,
  `contract_paper_path` varchar(300) DEFAULT NULL,
  `id_proff` varchar(300) DEFAULT NULL,
  `id_proff_filename` varchar(200) DEFAULT NULL,
  `id_proff_path` varchar(300) DEFAULT NULL,
  `other_document` varchar(300) DEFAULT NULL,
  `other_document_filename` varchar(200) DEFAULT NULL,
  `other_document_path` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_employee_login`
--

CREATE TABLE `tbl_employee_login` (
  `employee_login_id` int(5) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `activate` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_employee_login`
--

INSERT INTO `tbl_employee_login` (`employee_login_id`, `employee_id`, `user_name`, `password`, `activate`) VALUES
(1, 2, 'hr_admin', 'hr_admin', 1),
(2, 58, 'super_admin', 'super_admin', 1),
(5, 61, 'employee1', 'employee1', 1),
(6, 62, '6342', 'admin', 1),
(7, 63, 'employee2', 'employee2', 1),
(8, 64, '٣٠٠٥', 'admin', 1),
(9, 65, 'WhrnsZlU', '1Fb7IAzw', 1),
(10, 66, 'WDXRHOyz', '8lceBc3b', 1);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_evaluations`
--

CREATE TABLE `tbl_evaluations` (
  `evaluation_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `valuess` varchar(300) NOT NULL,
  `ids` varchar(300) NOT NULL,
  `total` int(20) NOT NULL,
  `created_date` varchar(300) NOT NULL,
  `departmnt_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_evaluations`
--

INSERT INTO `tbl_evaluations` (`evaluation_id`, `emp_id`, `valuess`, `ids`, `total`, `created_date`, `departmnt_id`) VALUES
(1, 2, '50;100', '1;2', 150, '2017-07-05', 1),
(4, 2, '68;80', '1;2', 148, '2017-07-19', 1),
(5, 2, '80;60', '2;3', 140, '2018-09-04', 1),
(6, 2, '100;0100', '2;3', 200, '2018-12-16', 1);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_extra_hours`
--

CREATE TABLE `tbl_extra_hours` (
  `extra_hours_id` int(11) NOT NULL,
  `extra_work_id` int(11) NOT NULL,
  `num_hours` int(11) NOT NULL,
  `start_time` varchar(300) CHARACTER SET utf8 NOT NULL,
  `employexh_id` int(11) NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_extra_hours`
--

INSERT INTO `tbl_extra_hours` (`extra_hours_id`, `extra_work_id`, `num_hours`, `start_time`, `employexh_id`, `file`, `app_id`) VALUES
(1, 4, 3, '4 : 55 م', 2, 'حفظ_الطلاب_بالأجزاء4.docx', 0),
(2, 4, 10, '4 : 21 م', 2, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_extra_work`
--

CREATE TABLE `tbl_extra_work` (
  `extra_work_id` int(11) NOT NULL,
  `title_ar` varchar(100) CHARACTER SET utf8 NOT NULL,
  `title_en` varchar(100) CHARACTER SET utf8 NOT NULL,
  `work_value` double(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_extra_work`
--

INSERT INTO `tbl_extra_work` (`extra_work_id`, `title_ar`, `title_en`, `work_value`) VALUES
(4, 'الساعات الإضافية', 'extra hours', 150);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_finance_info`
--

CREATE TABLE `tbl_finance_info` (
  `finance_info_id` int(11) NOT NULL,
  `employef_id` int(11) NOT NULL,
  `payment_method` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=bank, 2=checks, 3 = cash money',
  `med_insur_start_date` varchar(200) DEFAULT NULL,
  `med_insur_end_date` varchar(200) DEFAULT NULL,
  `social_salary` varchar(200) DEFAULT NULL,
  `social_start_date` varchar(200) DEFAULT NULL,
  `option_end_service` tinyint(2) NOT NULL DEFAULT '1',
  `note` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_finance_info`
--

INSERT INTO `tbl_finance_info` (`finance_info_id`, `employef_id`, `payment_method`, `med_insur_start_date`, `med_insur_end_date`, `social_salary`, `social_start_date`, `option_end_service`, `note`) VALUES
(1, 2, 3, '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_gsettings`
--

CREATE TABLE `tbl_gsettings` (
  `id_gsettings` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `logo` varchar(150) DEFAULT NULL,
  `full_path` varchar(150) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_id` int(3) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `hotline` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `currency` varchar(200) NOT NULL,
  `timezone_name` varchar(35) NOT NULL,
  `num_days_off` int(10) NOT NULL DEFAULT '4',
  `job_time_hours` int(10) NOT NULL DEFAULT '8'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_gsettings`
--

INSERT INTO `tbl_gsettings` (`id_gsettings`, `name`, `logo`, `full_path`, `email`, `address`, `city`, `country_id`, `phone`, `mobile`, `hotline`, `fax`, `website`, `currency`, `timezone_name`, `num_days_off`, `job_time_hours`) VALUES
(1, 'Smart Life', 'img/uploads/smartlife.png', '/home/smartli1/public_html/hrm_lite/img/uploads/smartlife.png', 'example@email.com', 'address', 'city', 193, '0000000000', '0000000000', '', '', 'http://www.mywebsite.com', 'SAR', 'Asia/Anadyr', 6, 8);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_holiday`
--

CREATE TABLE `tbl_holiday` (
  `holiday_id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_houcing`
--

CREATE TABLE `tbl_houcing` (
  `houcing_id` int(11) NOT NULL,
  `houcing_status` int(11) NOT NULL,
  `start_day` date NOT NULL,
  `houcing_type` varchar(20) NOT NULL,
  `houcing_value` varchar(100) NOT NULL,
  `houcing_repetition` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_inbox`
--

CREATE TABLE `tbl_inbox` (
  `inbox_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `to` varchar(100) NOT NULL,
  `from` varchar(100) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `message_body` text NOT NULL,
  `attach_file` varchar(200) DEFAULT NULL,
  `attach_file_path` text,
  `attach_filename` text,
  `message_time` varchar(200) DEFAULT NULL,
  `view_status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=Read 2=Unread',
  `notify_me` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=on 0=off',
  `deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_inbox`
--

INSERT INTO `tbl_inbox` (`inbox_id`, `employee_id`, `user_id`, `to`, `from`, `subject`, `message_body`, `attach_file`, `attach_file_path`, `attach_filename`, `message_time`, `view_status`, `notify_me`, `deleted`) VALUES
(1, 2, NULL, 'hamedissam@gmail.com', 'hamedissam@gmail.com', 'kjbkjbjk', '<p>jkbjkbjkbnkj</p>\n', NULL, NULL, NULL, '2017-07-07 07:02:05', 1, 1, 'No'),
(2, NULL, 1, 'hamedissam217@gmail.com', '0', 'wdddddddddd', '<p>0000000</p>\n', NULL, NULL, NULL, '2018-08-06 18:57:54', 1, 1, 'No'),
(3, NULL, 1, 'hamedissam217@gmail.com', '0', 'wdddddddddd', '<p>eeeeeeeeeeeeeeeeee</p>\n', NULL, NULL, NULL, '2018-08-06 19:11:27', 1, 1, 'No'),
(4, NULL, 1, 'hamedissam217@gmail.com', '0', 'wsws', '<p>dcffwe</p>\n', NULL, NULL, NULL, '2018-08-06 19:15:29', 1, 1, 'No'),
(5, NULL, 1, 'hamedissam@gmail.com', '0', 'wdddddddddd', '<p>000</p>\n', 'img/uploads/0.phtml', '/home/smarthrt/public_html/apps/demo/img/uploads/0.phtml', '0.phtml', '2018-08-06 19:16:55', 1, 1, 'No'),
(6, 2, NULL, 'hamedissam@gmail.com', 'hamedissam@gmail.com', 'سرفر لا يشتغل', '<p>zet</p>\n', NULL, NULL, NULL, '2018-12-13 18:34:22', 1, 1, 'No'),
(7, 2, NULL, 'hamedissam@gmail.com', 'hamedissam@gmail.com', 'zet', '<p>zet</p>\n', NULL, NULL, NULL, '2018-12-13 18:34:56', 1, 1, 'No');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_irrigularities`
--

CREATE TABLE `tbl_irrigularities` (
  `irrigularity_id` int(11) NOT NULL,
  `irrigularity_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_irrigularity_category`
--

CREATE TABLE `tbl_irrigularity_category` (
  `irrigularity_category_id` int(11) NOT NULL,
  `name_en` varchar(500) NOT NULL,
  `name_ar` varchar(500) NOT NULL,
  `first_en` varchar(500) DEFAULT NULL,
  `first_ar` varchar(500) DEFAULT NULL,
  `second_en` varchar(500) DEFAULT NULL,
  `second_ar` varchar(500) DEFAULT NULL,
  `third_en` varchar(500) DEFAULT NULL,
  `third_ar` varchar(500) DEFAULT NULL,
  `fourth_en` varchar(500) DEFAULT NULL,
  `fourth_ar` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_irrigularity_category`
--

INSERT INTO `tbl_irrigularity_category` (`irrigularity_category_id`, `name_en`, `name_ar`, `first_en`, `first_ar`, `second_en`, `second_ar`, `third_en`, `third_ar`, `fourth_en`, `fourth_ar`) VALUES
(1, 'Name of the violation', 'إسم المخالفة', 'Punishment', 'عقوبة', 'Punishment', 'عقوبة', 'Punishment', 'عقوبة', 'Punishment', 'عقوبة'),
(2, 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_job_appliactions`
--

CREATE TABLE `tbl_job_appliactions` (
  `job_appliactions_id` int(11) NOT NULL,
  `job_circular_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `cover_letter` text NOT NULL,
  `resume` text NOT NULL,
  `application_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=pending 1=accept 2 = reject',
  `apply_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_job_places`
--

CREATE TABLE `tbl_job_places` (
  `job_place_id` int(11) NOT NULL,
  `place_name_ar` varchar(300) CHARACTER SET utf8 NOT NULL,
  `place_name_en` varchar(300) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_job_places`
--

INSERT INTO `tbl_job_places` (`job_place_id`, `place_name_ar`, `place_name_en`) VALUES
(1, 'المقر الرئيسي', 'Main Headquarters'),
(2, 'test', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_job_titles`
--

CREATE TABLE `tbl_job_titles` (
  `job_titles_id` int(10) NOT NULL,
  `job_titles_name_ar` varchar(255) NOT NULL,
  `job_titles_name_en` varchar(200) NOT NULL,
  `job_titles_desc_ar` text,
  `job_titles_desc_en` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_job_titles`
--

INSERT INTO `tbl_job_titles` (`job_titles_id`, `job_titles_name_ar`, `job_titles_name_en`, `job_titles_desc_ar`, `job_titles_desc_en`) VALUES
(1, 'المدير التنفيذي', 'المدير التنفيذي', 'القيام بأداء جميع المهمات و المسئوليات المنوطة بالمدير التنفيذي ، و المشاركة الفعالة فى تحديد و صياغة الأهداف، و كذلك تخطيط و تنظيم سير العمل بالمنشأة بما يضمن تحقيق الأهداف المحددة', 'To perform all the tasks and responsibilities of the Executive manager , and active participation in the identification and formulation of objectives, as well as planning and organizing workflow facility to ensure the achievement of the targets set'),
(4, 'مسؤول الموارد البشرية', 'Human Resources', 'يتولى مسؤولية جميع الأنشطة المرتبطة بكافة الإجراءات النظامية لشؤون الموظفين بداية من التعين حتى التقاعد أو إنهاء الخدمة', 'Is responsible for all costs associated with all the statutory procedures for staff from the beginning of activities of appointment until retirement or termination of service'),
(5, 'محاسب', 'Accountant', 'التاكد من تقييد العمليات المحاسبية وترحيلها واستخراج موازين المراجعة اللازمة واجراء قيود التسويات واعداد التقارير المالية والمحاسبية وإتمام الحسابات الختامية والميزانية السنوية .', 'Make sure to restrict the accounting processes and migration and extraction of trial balances and necessary adjustments restrictions and the preparation of financial and accounting reports and the completion of the final accounts and the annual budget.'),
(6, 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_languages`
--

CREATE TABLE `tbl_languages` (
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_ar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tbl_languages`
--

INSERT INTO `tbl_languages` (`code`, `name`, `name_ar`, `icon`, `active`) VALUES
('ar', 'arabic', 'العربية ', 'sa', 1),
('en', 'english', 'الإنجليزية', 'us', 1);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_leaves`
--

CREATE TABLE `tbl_leaves` (
  `leave_id` int(11) NOT NULL,
  `leave_category_id` int(11) NOT NULL,
  `employel_id` int(11) NOT NULL,
  `duration` int(3) NOT NULL,
  `quota` int(3) DEFAULT '0' COMMENT '1=yes, 2=no',
  `affect_stock` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=contractual year, 2=normal year',
  `calc_type` tinyint(2) NOT NULL DEFAULT '1',
  `replacement` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `leave_tel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `address_in_leave` text CHARACTER SET utf8 NOT NULL,
  `going_date` varchar(200) CHARACTER SET utf8 NOT NULL,
  `coming_date` varchar(200) CHARACTER SET utf8 NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_leaves`
--

INSERT INTO `tbl_leaves` (`leave_id`, `leave_category_id`, `employel_id`, `duration`, `quota`, `affect_stock`, `calc_type`, `replacement`, `leave_tel`, `address_in_leave`, `going_date`, `coming_date`, `file`, `app_id`) VALUES
(1, 2, 2, 10, 100, 2, 1, '61', '2425642', 'عنوان الموظف أثناء الإجازة', '2017-07-11', '2017-07-20', NULL, 15),
(2, 2, 2, 100, 2, 1, 1, '58', '25', 'عنوان الموظف أثناء الإجازة', '2017-06-26', '2017-10-03', NULL, 0),
(3, 1, 61, 3, 0, 2, 1, '58', '45656456', 'عنوان الموظف أثناء الإجازة *', '2017-12-26', '2017-12-28', NULL, 7),
(4, 3, 2, 17, 0, 1, 1, '2', '864756465465', 'sdkfjhskjdf', '2018-12-13', '2018-12-29', 'Mountains12.jpg', 0),
(6, 1, 2, 5, 0, 1, 1, '2', '857456465', 'rder', '2018-12-01', '2018-12-05', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_leave_category`
--

CREATE TABLE `tbl_leave_category` (
  `leave_category_id` int(2) NOT NULL,
  `category_en` varchar(100) NOT NULL,
  `category_ar` varchar(100) NOT NULL,
  `leave_duration` int(3) NOT NULL,
  `leave_quota` int(2) NOT NULL,
  `paid_leave` tinyint(4) NOT NULL COMMENT '1=yes, 2=no',
  `affect_balance` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=yes, 2=no',
  `calc_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=contractual year, 2=normal year',
  `set_replacement` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=yes, 2=no',
  `leave_tel` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1=yes, 2=no',
  `allowance_cat_ids` varchar(200) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_leave_category`
--

INSERT INTO `tbl_leave_category` (`leave_category_id`, `category_en`, `category_ar`, `leave_duration`, `leave_quota`, `paid_leave`, `affect_balance`, `calc_type`, `set_replacement`, `leave_tel`, `allowance_cat_ids`) VALUES
(1, 'annual vacation', 'إجازة سنوية', 30, 0, 1, 2, 1, 1, 1, '1;2'),
(2, 'ُEmergency Leave', 'اجازة استثنائية -بدون راتب', 11, 100, 2, 2, 1, 1, 1, '1'),
(3, 'test', 'test', 1, 0, 1, 1, 1, 2, 1, '1');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_loan_list`
--

CREATE TABLE `tbl_loan_list` (
  `loan_list_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `advance_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `month_number` int(11) NOT NULL,
  `amount_part` double NOT NULL,
  `loan_date` date NOT NULL,
  `loan_status` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_maintenaces`
--

CREATE TABLE `tbl_maintenaces` (
  `maintenance_id` int(11) NOT NULL,
  `employeemn_id` int(11) NOT NULL,
  `maintenance_title` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `maintenance_date` varchar(30) CHARACTER SET utf8 NOT NULL,
  `maintenance_notes` text CHARACTER SET utf8 NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_maintenaces`
--

INSERT INTO `tbl_maintenaces` (`maintenance_id`, `employeemn_id`, `maintenance_title`, `maintenance_date`, `maintenance_notes`, `file`, `app_id`) VALUES
(1, 2, 'عنوان الصيانة', '2017-06-04', 'ملاحظات', 'حفظ_الطلاب_بالأجزاء6.docx', 0),
(2, 58, 'عنوان الطلب', '2017-09-14', 'نص الرسالة', NULL, 5),
(3, 2, 'sfzef', '2018-12-15', 'zer', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_medical_insurane`
--

CREATE TABLE `tbl_medical_insurane` (
  `medical_id` int(11) NOT NULL,
  `title_ar` varchar(100) CHARACTER SET utf8 NOT NULL,
  `title_en` varchar(100) CHARACTER SET utf8 NOT NULL,
  `m_insurance_percent` varchar(111) NOT NULL DEFAULT '0',
  `m_societe_percent` varchar(111) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_medical_insurane`
--

INSERT INTO `tbl_medical_insurane` (`medical_id`, `title_ar`, `title_en`, `m_insurance_percent`, `m_societe_percent`) VALUES
(1, 'كامل المبلغ', 'Total amount', '0', '100'),
(2, 'جزء من المبلغ', 'Part of the amount', '4', '4.5');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `label`, `link`, `icon`, `parent`, `sort`) VALUES
(1, 'dashboard', 'admin/dashboard', 'fa fa-dashboard', 0, 1),
(2, 'settings', '#', 'fa fa-cogs', 0, 2),
(3, 'general_settings', 'admin/settings/general_settings', 'fa fa-gears', 2, 1),
(4, 'job_titles', 'admin/settings/job_titles', 'fa fa-user', 2, 2),
(5, 'leave_category', 'admin/settings/leave_category', 'fa fa-child', 2, 3),
(6, 'irrigularity_category', 'admin/settings/irrigularity_category', 'fa fa-bolt', 2, 4),
(7, 'job_places', 'admin/settings2/job_places', 'fa fa-map-marker', 2, 5),
(8, 'employee_category', 'admin/settings2/employee_category', 'fa fa-users', 2, 6),
(9, 'evaluation_items', 'admin/settings2/evaluation_items', 'fa fa-magic', 2, 7),
(10, 'center_documentations', 'admin/settings2/center_documentations', 'fa fa-files-o', 2, 8),
(57, 'mailbox', '#', 'fa fa-credit-card', 0, 9),
(58, 'inbox', 'admin/mailbox/inbox', 'fa fa-inbox', 57, 1),
(59, 'draft', 'admin/mailbox/draft', 'fa fa-files-o', 57, 2),
(60, 'sent', 'admin/mailbox/sent', 'fa fa-paper-plane', 57, 3),
(83, 'manage_employee', '#', 'fa fa-user', 0, 5),
(84, 'manage_employee', 'admin/employee/employees', 'entypo-users', 83, 1),
(90, 'leave_category', 'admin/settings/leave_category', 'fa fa-dedent', 86, 4),
(91, 'notification_settings', 'admin/settings/notification_settings', 'fa fa-bell-o', 86, 5),
(92, 'language_settings', 'admin/settings/language_settings', 'fa fa-language', 86, 6),
(93, 'database_backup', 'admin/settings/database_backup', 'fa fa-database', 0, 18),
(97, 'hourly_rate', 'admin/payroll/hourly_rate', 'fa fa-files-o', 74, 1),
(98, 'trash', 'admin/mailbox/trash', 'fa fa-trash-o', 57, 4),
(99, 'financial_information', '#', 'fa fa-eur', 0, 6),
(101, 'extra_work', 'admin/finances/extra_work_list', 'fa fa-clock-o', 99, 2),
(102, 'deduction_type', 'admin/finances/deduction_list', 'fa fa-arrow-down', 99, 3),
(103, 'other_provisions', 'admin/finances/provision_list', 'fa fa-credit-card', 99, 4),
(104, 'advances', 'admin/finances/advance_list', 'fa fa-money', 99, 5),
(105, 'insurance_information', '#', 'fa fa-h-square', 0, 6),
(106, 'medical_insurance', 'admin/insurances/medical_list', 'fa fa-ambulance', 105, 1),
(107, 'social_insurance', 'admin/insurances/social_list', 'fa fa-user-md', 105, 2),
(108, 'bonuses_type', 'admin/finances/allowance_list', 'fa fa-money', 99, 1),
(110, 'users_permissions_system', '#', 'fa fa-unlock-alt', 0, 2),
(111, 'users_list', 'admin/users/users_list', 'fa fa-list-alt', 110, 1),
(112, 'add_user', 'admin/users/add_user', 'fa fa-plus', 110, 2),
(113, 'approvals', '#', 'fa fa-thumbs-up', 0, 7),
(114, 'approvals_list', 'admin/approvals/approvals_list', 'fa fa-list-alt', 113, 1),
(115, 'add_approval', 'admin/approvals/add_approval', 'fa fa-plus', 113, 2),
(116, 'link_approvals', 'admin/approvals/link_approvals', 'fa fa-gears', 113, 3),
(118, 'organizational_structure', '#', 'fa fa-cubes', 0, 4),
(119, 'branches', 'admin/organizational_structure/branches', 'fa fa-globe', 118, 1),
(120, 'departements', 'admin/organizational_structure/department_list', 'fa fa-university', 118, 2),
(121, 'designations', 'admin/organizational_structure/designation_list', 'fa fa-building', 118, 3),
(122, 'units', 'admin/organizational_structure/units_list', 'fa fa-th-large', 118, 4),
(123, 'divisions', 'admin/organizational_structure/divisions_list', 'fa fa-th', 118, 5),
(124, 'overview', 'admin/organizational_structure/overview', 'fa fa-eye', 118, 6),
(125, 'automatic_reminder', 'admin/settings/automatic_reminder', 'fa fa-bell-o', 2, 8),
(126, 'applications', 'admin/approvals/applications', 'fa fa-list-alt', 113, 4),
(127, 'sms_config', 'admin/settings2/sms_config', 'fa fa-mobile', 2, 9);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `subject_ar` varchar(150) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `subject_en` varchar(150) NOT NULL,
  `message_ar` text CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `message_en` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `subject_ar`, `subject_en`, `message_ar`, `message_en`) VALUES
(1, 'إنتهاء فترة تجربة موظف', 'End of an employee test period', 'يوم على إنتهاء فترة تجربة الموظف', 'Days for the end of a test period for the employee '),
(2, 'إنتهاء فترة تجربة موظف', 'End of an employee test period', 'يوم على إنتهاء فترة تجربة الموظف', 'Days for the end of a test period for the employee '),
(3, 'إنتهاء صلاحية بطاقة الهوية', 'expiration of identity card', 'يوم على إنتهاء صلاحية بطاقة هوية الموظف', 'Days for the expiration of identity card of the employee '),
(4, 'إنتهاء صلاحية جواز السفر', 'expiration of passport', 'يوم على إنتهاء صلاحية جواز السفر الموظف', 'Days for the expiration of passport of the employee '),
(5, 'إنتهاء صلاحية بطاقة التأمين', 'Insurance card expiration', 'يوم على إنتهاء صلاحية بطاقة التأمين للموظف', 'Days before the expiration of the\n insurance card of the employee'),
(6, 'إنتهاء عقد موظف', 'End of contract of an employee', 'يوم على إنتهاء عقد الموظف', 'days for the end of the contract of the employee');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_notice`
--

CREATE TABLE `tbl_notice` (
  `notice_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `short_description` text,
  `long_description` text,
  `employee_id` int(11) NOT NULL,
  `created_date` varchar(50) DEFAULT NULL,
  `flag` tinyint(1) NOT NULL COMMENT '0 = unpublished, 1 = published',
  `view_status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1=Read 2=Unread',
  `notify_me` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=on 0=off',
  `send_to` varchar(20) NOT NULL DEFAULT '0' COMMENT 'dm=direct_manager,hrm=hr_manager',
  `sugg_or_compl` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=suggestion, 2complaint',
  `to_all` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_notice`
--

INSERT INTO `tbl_notice` (`notice_id`, `title`, `short_description`, `long_description`, `employee_id`, `created_date`, `flag`, `view_status`, `notify_me`, `send_to`, `sugg_or_compl`, `to_all`) VALUES
(1, 'sdf', NULL, '<p>sdfsdf</p>\n', 2, '2018-12-13', 1, 1, 1, '2', 2, 0),
(2, 'طلب مساءلة من قبل مسؤول الموارد البشرية عصام', NULL, 'لقد إستلمت طلب مساءلة. الرجاء إتبع رابط الطلب التالي للإطلاع على تفاصيل المساءلة :<br><a href=\"http://demo.smart-hr.top/employee/accounting/accounting_detail1/2\" class=\"btn btn-primary\" target=\"_blank\">رابط المساءلة</a>', 2, '2018-12-15', 1, 1, 1, '2', 5, 0),
(3, 'رد موظف على مساءلة', NULL, 'لقد تم الرد على مسائلتك للموظف : عصام', 2, '2018-12-15', 1, 1, 1, '2', 3, 0),
(4, 'طلب مساءلة من قبل عصام', NULL, '<b>القرار النهائي : </b><br>ru-r(uy', 2, '2018-12-15', 1, 1, 1, '2', 3, 0),
(5, 'تقييم جديد', NULL, 'لديك تقييم جديد من طرف المدير :<br><br><a href=\"http://demo.smart-hr.top/employee/evaluations/evaluation_detail/6\" target=\"_blank\" style=\" width: 200px;background: #337AB7;padding: 6px 20px;text-decoration: none;font-weight: bold;color: #fff;border-radius: 2px;\">رابط التقييم</a>', 2, '2018-12-16', 1, 1, 1, '2', 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_other_document_path`
--

CREATE TABLE `tbl_other_document_path` (
  `odp_id` int(12) NOT NULL,
  `filename` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `employee_idodp` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_permissions`
--

CREATE TABLE `tbl_permissions` (
  `permission_id` int(11) NOT NULL,
  `employeeprm_id` int(11) NOT NULL,
  `permission_type` tinyint(2) NOT NULL,
  `permission_date` varchar(300) CHARACTER SET utf8 NOT NULL,
  `permission_star` varchar(300) CHARACTER SET utf8 NOT NULL,
  `permission_end` varchar(300) CHARACTER SET utf8 NOT NULL,
  `reason` text CHARACTER SET utf8 NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`permission_id`, `employeeprm_id`, `permission_type`, `permission_date`, `permission_star`, `permission_end`, `reason`, `file`, `app_id`) VALUES
(1, 2, 1, '2017-06-04', '5 : 07 م', '5 : 07 م', 'sdfsdf', 'حفظ_الطلاب_بالأجزاء7.docx', 0),
(2, 2, 2, '2018-12-15', '4 : 28 م', '4 : 28 م', 'zer', 'Doc-P-112426-636373514621895222.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_permission_list`
--

CREATE TABLE `tbl_permission_list` (
  `permission_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `permission_title` int(11) NOT NULL,
  `permission_date` date NOT NULL,
  `permission_status` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `permission_save_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_provision`
--

CREATE TABLE `tbl_provision` (
  `provision_id` int(11) NOT NULL,
  `employp_id` int(11) NOT NULL,
  `provision_category_id` int(11) NOT NULL,
  `provision_value` varchar(100) NOT NULL,
  `start_date` varchar(300) NOT NULL,
  `end_date` varchar(300) DEFAULT NULL,
  `note_provision` text CHARACTER SET utf8 COLLATE utf8_persian_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_provision`
--

INSERT INTO `tbl_provision` (`provision_id`, `employp_id`, `provision_category_id`, `provision_value`, `start_date`, `end_date`, `note_provision`) VALUES
(1, 2, 1, '20', '2017-05-10', NULL, ''),
(2, 2, 1, '45', '2018-12-14', '2018-12-25', 'sdf');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_provision_category`
--

CREATE TABLE `tbl_provision_category` (
  `provision_category_id` int(11) NOT NULL,
  `provision_title_en` varchar(100) NOT NULL,
  `provision_title_ar` varchar(100) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_provision_category`
--

INSERT INTO `tbl_provision_category` (`provision_category_id`, `provision_title_en`, `provision_title_ar`) VALUES
(1, 'Gift', 'مكافئة');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_purchases`
--

CREATE TABLE `tbl_purchases` (
  `purchase_id` int(11) NOT NULL,
  `employeepr_id` int(11) NOT NULL,
  `prod_name` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `prod_desc` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `prod_num` int(220) NOT NULL,
  `purchase_date` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `prod_note` text CHARACTER SET utf8 NOT NULL,
  `file` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_purchases`
--

INSERT INTO `tbl_purchases` (`purchase_id`, `employeepr_id`, `prod_name`, `prod_desc`, `prod_num`, `purchase_date`, `prod_note`, `file`, `app_id`) VALUES
(1, 2, 'fhtfrth', 'rthyrty', 55, '2018-09-05', 'zsergtzert', '511273916.jpg', 0),
(2, 2, 'drgerg', 'ertert', 4, '2018-12-15', 'ertert', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_recrutements`
--

CREATE TABLE `tbl_recrutements` (
  `recrutement_id` int(11) NOT NULL,
  `employerc_id` int(11) NOT NULL,
  `course_institute` varchar(500) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `address_in_leave` varchar(1000) NOT NULL,
  `going_date` varchar(300) NOT NULL,
  `coming_date` varchar(300) NOT NULL,
  `leave_duration` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `file` varchar(500) DEFAULT NULL,
  `app_id` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_recrutements`
--

INSERT INTO `tbl_recrutements` (`recrutement_id`, `employerc_id`, `course_institute`, `name`, `type`, `address_in_leave`, `going_date`, `coming_date`, `leave_duration`, `note`, `file`, `app_id`) VALUES
(1, 2, 'الجهة الطالبة', 'مهمة الإنتداب', 1, 'موقع الإنتداب ', '2017-06-13', '2017-06-21', '9', 'تفاصيل', 'حفظ_الطلاب_بالأجزاء8.docx', 0),
(2, 2, 'qsd', 'qsdqsd', 2, 'qsdqsd', '2018-12-21', '2018-12-27', '7', 'qsdsqd', NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_reminder`
--

CREATE TABLE `tbl_reminder` (
  `reminder_id` int(11) NOT NULL,
  `reminder_lang` varchar(11) NOT NULL DEFAULT 'ar',
  `reminder_test_period_hrm` int(11) NOT NULL DEFAULT '0',
  `reminder_test_period_dm` int(11) NOT NULL DEFAULT '0',
  `reminder_identity_end` int(11) NOT NULL DEFAULT '0',
  `reminder_passport_end` int(11) NOT NULL DEFAULT '0',
  `reminder_med_insurance_end` int(11) NOT NULL DEFAULT '0',
  `reminder_contract_end` int(11) NOT NULL DEFAULT '0',
  `reminder_mail` tinyint(2) NOT NULL DEFAULT '0',
  `reminder_employee_fixed` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_reminder`
--

INSERT INTO `tbl_reminder` (`reminder_id`, `reminder_lang`, `reminder_test_period_hrm`, `reminder_test_period_dm`, `reminder_identity_end`, `reminder_passport_end`, `reminder_med_insurance_end`, `reminder_contract_end`, `reminder_mail`, `reminder_employee_fixed`) VALUES
(1, 'ar', 30, 30, 30, 30, 30, 30, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_sent`
--

CREATE TABLE `tbl_sent` (
  `sent_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `to` varchar(100) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `message_body` text NOT NULL,
  `attach_file` varchar(200) DEFAULT NULL,
  `attach_file_path` text,
  `attach_filename` text,
  `message_time` varchar(200) DEFAULT NULL,
  `deleted` enum('Yes','No') NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_sent`
--

INSERT INTO `tbl_sent` (`sent_id`, `user_id`, `employee_id`, `to`, `subject`, `message_body`, `attach_file`, `attach_file_path`, `attach_filename`, `message_time`, `deleted`) VALUES
(1, 1, NULL, 'hamedissam@gmail.com', 'test', '<p>tsetskjsbgksbg sg sdg</p>\n', NULL, NULL, NULL, '2017-05-25 05:00:14', 'Yes'),
(2, 1, NULL, 'hamedissam@gmail.com', 'test', '<p>tsetskjsbgksbg sg sdg</p>\n', NULL, NULL, NULL, '2017-05-25 05:00:49', 'Yes'),
(3, 1, NULL, 'hamedissam@gmail.com', 'test', '<p>tsetskjsbgksbg sg sdg</p>\n', NULL, NULL, NULL, '2017-05-25 05:02:44', 'Yes'),
(4, 1, NULL, 'hamedissam@gmail.com', 'test', '<p>tsetskjsbgksbg sg sdg</p>\n', NULL, NULL, NULL, '2017-05-25 05:06:13', 'Yes'),
(5, 1, NULL, 'hamedissam@gmail.com', 'matjar', '<p>سيقغيل</p>\n', NULL, NULL, NULL, '2017-05-25 05:07:36', 'Yes'),
(6, 1, NULL, 'hamedissam@gmail.com', 'matjar', '<p>سيقغيل</p>\n', NULL, NULL, NULL, '2017-05-25 05:08:14', 'No'),
(7, NULL, 2, 'hamedissam@gmail.com', 'kjbkjbjk', '<p>jkbjkbjkbnkj</p>\n', NULL, NULL, NULL, '1438-10-12', 'No'),
(8, 1, NULL, 'hamedissam217@gmail.com', 'wdddddddddd', '<p>0000000</p>\n', NULL, NULL, NULL, '2018-08-06 18:57:54', 'No'),
(9, 1, NULL, 'devweb218@gmail.com', '', '<p>0000</p>\n', NULL, NULL, NULL, '2018-08-06 18:59:24', 'Yes'),
(10, 1, NULL, 'hamedissam217@gmail.com', 'wdddddddddd', '<p>eeeeeeeeeeeeeeeeee</p>\n', NULL, NULL, NULL, '2018-08-06 19:11:27', 'No'),
(11, 1, NULL, 'hamedissam217@gmail.com', 'wsws', '<p>dcffwe</p>\n', NULL, NULL, NULL, '2018-08-06 19:15:29', 'No'),
(12, 1, NULL, 'hamedissam@gmail.com', 'wdddddddddd', '<p>000</p>\n', 'img/uploads/0.phtml', '/home/smarthrt/public_html/apps/demo/img/uploads/0.phtml', '0.phtml', '2018-08-06 19:16:55', 'No'),
(13, NULL, 2, 'hamedissam@gmail.com', 'سرفر لا يشتغل', '<p>zet</p>\n', NULL, NULL, NULL, '2018-12-13', 'Yes'),
(14, NULL, 2, 'hamedissam@gmail.com', 'zet', '<p>zet</p>\n', NULL, NULL, NULL, '2018-12-13', 'No');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_sms_config`
--

CREATE TABLE `tbl_sms_config` (
  `sms_config_id` tinyint(2) NOT NULL,
  `default` tinyint(2) NOT NULL DEFAULT '1',
  `sms_login1` varchar(300) CHARACTER SET utf8 NOT NULL,
  `sms_password1` varchar(300) CHARACTER SET utf8 NOT NULL,
  `sender_name1` varchar(300) NOT NULL,
  `sms_login2` varchar(300) CHARACTER SET utf8 NOT NULL,
  `sms_password2` varchar(300) CHARACTER SET utf8 NOT NULL,
  `sender_name2` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_sms_config`
--

INSERT INTO `tbl_sms_config` (`sms_config_id`, `default`, `sms_login1`, `sms_password1`, `sender_name1`, `sms_login2`, `sms_password2`, `sender_name2`) VALUES
(1, 2, '966549344855', '14091409', 'demohrm', 'smart02', '123123smx', 'demohrm');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_social_insurane`
--

CREATE TABLE `tbl_social_insurane` (
  `social_id` int(11) NOT NULL,
  `social_flag` varchar(200) NOT NULL,
  `title_ar` varchar(300) CHARACTER SET utf8 NOT NULL,
  `title_en` varchar(300) CHARACTER SET utf8 NOT NULL,
  `insurance_percent` int(100) NOT NULL DEFAULT '0',
  `societe_percent` int(100) NOT NULL DEFAULT '0',
  `habit_insurance` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=yes, 0=no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tbl_social_insurane`
--

INSERT INTO `tbl_social_insurane` (`social_id`, `social_flag`, `title_ar`, `title_en`, `insurance_percent`, `societe_percent`, `habit_insurance`) VALUES
(1, 'saudi1', 'سعودي (قابل للخصم)', 'Saudi (deductible)', 10, 12, 1),
(2, 'saudi2', 'سعودي (غير قابل للخصم)', 'Saudi (non deductible)', 0, 22, 1),
(3, 'non-saudi', 'غير سعودي', 'non saudi', 0, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_timezone`
--

CREATE TABLE `tbl_timezone` (
  `timezone_id` int(10) NOT NULL,
  `country_code` char(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `timezone_name` varchar(35) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_timezone`
--

INSERT INTO `tbl_timezone` (`timezone_id`, `country_code`, `timezone_name`) VALUES
(1, 'AD', 'Europe/Andorra'),
(2, 'AE', 'Asia/Dubai'),
(3, 'AF', 'Asia/Kabul'),
(4, 'AG', 'America/Antigua'),
(5, 'AI', 'America/Anguilla'),
(6, 'AL', 'Europe/Tirane'),
(7, 'AM', 'Asia/Yerevan'),
(8, 'AO', 'Africa/Luanda'),
(9, 'AQ', 'Antarctica/McMurdo'),
(10, 'AQ', 'Antarctica/Rothera'),
(11, 'AQ', 'Antarctica/Palmer'),
(12, 'AQ', 'Antarctica/Mawson'),
(13, 'AQ', 'Antarctica/Davis'),
(14, 'AQ', 'Antarctica/Casey'),
(15, 'AQ', 'Antarctica/Vostok'),
(16, 'AQ', 'Antarctica/DumontDUrville'),
(17, 'AQ', 'Antarctica/Syowa'),
(18, 'AQ', 'Antarctica/Troll'),
(19, 'AR', 'America/Argentina/Buenos_Aires'),
(20, 'AR', 'America/Argentina/Cordoba'),
(21, 'AR', 'America/Argentina/Salta'),
(22, 'AR', 'America/Argentina/Jujuy'),
(23, 'AR', 'America/Argentina/Tucuman'),
(24, 'AR', 'America/Argentina/Catamarca'),
(25, 'AR', 'America/Argentina/La_Rioja'),
(26, 'AR', 'America/Argentina/San_Juan'),
(27, 'AR', 'America/Argentina/Mendoza'),
(28, 'AR', 'America/Argentina/San_Luis'),
(29, 'AR', 'America/Argentina/Rio_Gallegos'),
(30, 'AR', 'America/Argentina/Ushuaia'),
(31, 'AS', 'Pacific/Pago_Pago'),
(32, 'AT', 'Europe/Vienna'),
(33, 'AU', 'Australia/Lord_Howe'),
(34, 'AU', 'Antarctica/Macquarie'),
(35, 'AU', 'Australia/Hobart'),
(36, 'AU', 'Australia/Currie'),
(37, 'AU', 'Australia/Melbourne'),
(38, 'AU', 'Australia/Sydney'),
(39, 'AU', 'Australia/Broken_Hill'),
(40, 'AU', 'Australia/Brisbane'),
(41, 'AU', 'Australia/Lindeman'),
(42, 'AU', 'Australia/Adelaide'),
(43, 'AU', 'Australia/Darwin'),
(44, 'AU', 'Australia/Perth'),
(45, 'AU', 'Australia/Eucla'),
(46, 'AW', 'America/Aruba'),
(47, 'AX', 'Europe/Mariehamn'),
(48, 'AZ', 'Asia/Baku'),
(49, 'BA', 'Europe/Sarajevo'),
(50, 'BB', 'America/Barbados'),
(51, 'BD', 'Asia/Dhaka'),
(52, 'BE', 'Europe/Brussels'),
(53, 'BF', 'Africa/Ouagadougou'),
(54, 'BG', 'Europe/Sofia'),
(55, 'BH', 'Asia/Bahrain'),
(56, 'BI', 'Africa/Bujumbura'),
(57, 'BJ', 'Africa/Porto-Novo'),
(58, 'BL', 'America/St_Barthelemy'),
(59, 'BM', 'Atlantic/Bermuda'),
(60, 'BN', 'Asia/Brunei'),
(61, 'BO', 'America/La_Paz'),
(62, 'BQ', 'America/Kralendijk'),
(63, 'BR', 'America/Noronha'),
(64, 'BR', 'America/Belem'),
(65, 'BR', 'America/Fortaleza'),
(66, 'BR', 'America/Recife'),
(67, 'BR', 'America/Araguaina'),
(68, 'BR', 'America/Maceio'),
(69, 'BR', 'America/Bahia'),
(70, 'BR', 'America/Sao_Paulo'),
(71, 'BR', 'America/Campo_Grande'),
(72, 'BR', 'America/Cuiaba'),
(73, 'BR', 'America/Santarem'),
(74, 'BR', 'America/Porto_Velho'),
(75, 'BR', 'America/Boa_Vista'),
(76, 'BR', 'America/Manaus'),
(77, 'BR', 'America/Eirunepe'),
(78, 'BR', 'America/Rio_Branco'),
(79, 'BS', 'America/Nassau'),
(80, 'BT', 'Asia/Thimphu'),
(81, 'BW', 'Africa/Gaborone'),
(82, 'BY', 'Europe/Minsk'),
(83, 'BZ', 'America/Belize'),
(84, 'CA', 'America/St_Johns'),
(85, 'CA', 'America/Halifax'),
(86, 'CA', 'America/Glace_Bay'),
(87, 'CA', 'America/Moncton'),
(88, 'CA', 'America/Goose_Bay'),
(89, 'CA', 'America/Blanc-Sablon'),
(90, 'CA', 'America/Toronto'),
(91, 'CA', 'America/Nipigon'),
(92, 'CA', 'America/Thunder_Bay'),
(93, 'CA', 'America/Iqaluit'),
(94, 'CA', 'America/Pangnirtung'),
(95, 'CA', 'America/Resolute'),
(96, 'CA', 'America/Atikokan'),
(97, 'CA', 'America/Rankin_Inlet'),
(98, 'CA', 'America/Winnipeg'),
(99, 'CA', 'America/Rainy_River'),
(100, 'CA', 'America/Regina'),
(101, 'CA', 'America/Swift_Current'),
(102, 'CA', 'America/Edmonton'),
(103, 'CA', 'America/Cambridge_Bay'),
(104, 'CA', 'America/Yellowknife'),
(105, 'CA', 'America/Inuvik'),
(106, 'CA', 'America/Creston'),
(107, 'CA', 'America/Dawson_Creek'),
(108, 'CA', 'America/Vancouver'),
(109, 'CA', 'America/Whitehorse'),
(110, 'CA', 'America/Dawson'),
(111, 'CC', 'Indian/Cocos'),
(112, 'CD', 'Africa/Kinshasa'),
(113, 'CD', 'Africa/Lubumbashi'),
(114, 'CF', 'Africa/Bangui'),
(115, 'CG', 'Africa/Brazzaville'),
(116, 'CH', 'Europe/Zurich'),
(117, 'CI', 'Africa/Abidjan'),
(118, 'CK', 'Pacific/Rarotonga'),
(119, 'CL', 'America/Santiago'),
(120, 'CL', 'Pacific/Easter'),
(121, 'CM', 'Africa/Douala'),
(122, 'CN', 'Asia/Shanghai'),
(123, 'CN', 'Asia/Urumqi'),
(124, 'CO', 'America/Bogota'),
(125, 'CR', 'America/Costa_Rica'),
(126, 'CU', 'America/Havana'),
(127, 'CV', 'Atlantic/Cape_Verde'),
(128, 'CW', 'America/Curacao'),
(129, 'CX', 'Indian/Christmas'),
(130, 'CY', 'Asia/Nicosia'),
(131, 'CZ', 'Europe/Prague'),
(132, 'DE', 'Europe/Berlin'),
(133, 'DE', 'Europe/Busingen'),
(134, 'DJ', 'Africa/Djibouti'),
(135, 'DK', 'Europe/Copenhagen'),
(136, 'DM', 'America/Dominica'),
(137, 'DO', 'America/Santo_Domingo'),
(138, 'DZ', 'Africa/Algiers'),
(139, 'EC', 'America/Guayaquil'),
(140, 'EC', 'Pacific/Galapagos'),
(141, 'EE', 'Europe/Tallinn'),
(142, 'EG', 'Africa/Cairo'),
(143, 'EH', 'Africa/El_Aaiun'),
(144, 'ER', 'Africa/Asmara'),
(145, 'ES', 'Europe/Madrid'),
(146, 'ES', 'Africa/Ceuta'),
(147, 'ES', 'Atlantic/Canary'),
(148, 'ET', 'Africa/Addis_Ababa'),
(149, 'FI', 'Europe/Helsinki'),
(150, 'FJ', 'Pacific/Fiji'),
(151, 'FK', 'Atlantic/Stanley'),
(152, 'FM', 'Pacific/Chuuk'),
(153, 'FM', 'Pacific/Pohnpei'),
(154, 'FM', 'Pacific/Kosrae'),
(155, 'FO', 'Atlantic/Faroe'),
(156, 'FR', 'Europe/Paris'),
(157, 'GA', 'Africa/Libreville'),
(158, 'GB', 'Europe/London'),
(159, 'GD', 'America/Grenada'),
(160, 'GE', 'Asia/Tbilisi'),
(161, 'GF', 'America/Cayenne'),
(162, 'GG', 'Europe/Guernsey'),
(163, 'GH', 'Africa/Accra'),
(164, 'GI', 'Europe/Gibraltar'),
(165, 'GL', 'America/Godthab'),
(166, 'GL', 'America/Danmarkshavn'),
(167, 'GL', 'America/Scoresbysund'),
(168, 'GL', 'America/Thule'),
(169, 'GM', 'Africa/Banjul'),
(170, 'GN', 'Africa/Conakry'),
(171, 'GP', 'America/Guadeloupe'),
(172, 'GQ', 'Africa/Malabo'),
(173, 'GR', 'Europe/Athens'),
(174, 'GS', 'Atlantic/South_Georgia'),
(175, 'GT', 'America/Guatemala'),
(176, 'GU', 'Pacific/Guam'),
(177, 'GW', 'Africa/Bissau'),
(178, 'GY', 'America/Guyana'),
(179, 'HK', 'Asia/Hong_Kong'),
(180, 'HN', 'America/Tegucigalpa'),
(181, 'HR', 'Europe/Zagreb'),
(182, 'HT', 'America/Port-au-Prince'),
(183, 'HU', 'Europe/Budapest'),
(184, 'ID', 'Asia/Jakarta'),
(185, 'ID', 'Asia/Pontianak'),
(186, 'ID', 'Asia/Makassar'),
(187, 'ID', 'Asia/Jayapura'),
(188, 'IE', 'Europe/Dublin'),
(189, 'IL', 'Asia/Jerusalem'),
(190, 'IM', 'Europe/Isle_of_Man'),
(191, 'IN', 'Asia/Kolkata'),
(192, 'IO', 'Indian/Chagos'),
(193, 'IQ', 'Asia/Baghdad'),
(194, 'IR', 'Asia/Tehran'),
(195, 'IS', 'Atlantic/Reykjavik'),
(196, 'IT', 'Europe/Rome'),
(197, 'JE', 'Europe/Jersey'),
(198, 'JM', 'America/Jamaica'),
(199, 'JO', 'Asia/Amman'),
(200, 'JP', 'Asia/Tokyo'),
(201, 'KE', 'Africa/Nairobi'),
(202, 'KG', 'Asia/Bishkek'),
(203, 'KH', 'Asia/Phnom_Penh'),
(204, 'KI', 'Pacific/Tarawa'),
(205, 'KI', 'Pacific/Enderbury'),
(206, 'KI', 'Pacific/Kiritimati'),
(207, 'KM', 'Indian/Comoro'),
(208, 'KN', 'America/St_Kitts'),
(209, 'KP', 'Asia/Pyongyang'),
(210, 'KR', 'Asia/Seoul'),
(211, 'KW', 'Asia/Kuwait'),
(212, 'KY', 'America/Cayman'),
(213, 'KZ', 'Asia/Almaty'),
(214, 'KZ', 'Asia/Qyzylorda'),
(215, 'KZ', 'Asia/Aqtobe'),
(216, 'KZ', 'Asia/Aqtau'),
(217, 'KZ', 'Asia/Oral'),
(218, 'LA', 'Asia/Vientiane'),
(219, 'LB', 'Asia/Beirut'),
(220, 'LC', 'America/St_Lucia'),
(221, 'LI', 'Europe/Vaduz'),
(222, 'LK', 'Asia/Colombo'),
(223, 'LR', 'Africa/Monrovia'),
(224, 'LS', 'Africa/Maseru'),
(225, 'LT', 'Europe/Vilnius'),
(226, 'LU', 'Europe/Luxembourg'),
(227, 'LV', 'Europe/Riga'),
(228, 'LY', 'Africa/Tripoli'),
(229, 'MA', 'Africa/Casablanca'),
(230, 'MC', 'Europe/Monaco'),
(231, 'MD', 'Europe/Chisinau'),
(232, 'ME', 'Europe/Podgorica'),
(233, 'MF', 'America/Marigot'),
(234, 'MG', 'Indian/Antananarivo'),
(235, 'MH', 'Pacific/Majuro'),
(236, 'MH', 'Pacific/Kwajalein'),
(237, 'MK', 'Europe/Skopje'),
(238, 'ML', 'Africa/Bamako'),
(239, 'MM', 'Asia/Rangoon'),
(240, 'MN', 'Asia/Ulaanbaatar'),
(241, 'MN', 'Asia/Hovd'),
(242, 'MN', 'Asia/Choibalsan'),
(243, 'MO', 'Asia/Macau'),
(244, 'MP', 'Pacific/Saipan'),
(245, 'MQ', 'America/Martinique'),
(246, 'MR', 'Africa/Nouakchott'),
(247, 'MS', 'America/Montserrat'),
(248, 'MT', 'Europe/Malta'),
(249, 'MU', 'Indian/Mauritius'),
(250, 'MV', 'Indian/Maldives'),
(251, 'MW', 'Africa/Blantyre'),
(252, 'MX', 'America/Mexico_City'),
(253, 'MX', 'America/Cancun'),
(254, 'MX', 'America/Merida'),
(255, 'MX', 'America/Monterrey'),
(256, 'MX', 'America/Matamoros'),
(257, 'MX', 'America/Mazatlan'),
(258, 'MX', 'America/Chihuahua'),
(259, 'MX', 'America/Ojinaga'),
(260, 'MX', 'America/Hermosillo'),
(261, 'MX', 'America/Tijuana'),
(262, 'MX', 'America/Santa_Isabel'),
(263, 'MX', 'America/Bahia_Banderas'),
(264, 'MY', 'Asia/Kuala_Lumpur'),
(265, 'MY', 'Asia/Kuching'),
(266, 'MZ', 'Africa/Maputo'),
(267, 'NA', 'Africa/Windhoek'),
(268, 'NC', 'Pacific/Noumea'),
(269, 'NE', 'Africa/Niamey'),
(270, 'NF', 'Pacific/Norfolk'),
(271, 'NG', 'Africa/Lagos'),
(272, 'NI', 'America/Managua'),
(273, 'NL', 'Europe/Amsterdam'),
(274, 'NO', 'Europe/Oslo'),
(275, 'NP', 'Asia/Kathmandu'),
(276, 'NR', 'Pacific/Nauru'),
(277, 'NU', 'Pacific/Niue'),
(278, 'NZ', 'Pacific/Auckland'),
(279, 'NZ', 'Pacific/Chatham'),
(280, 'OM', 'Asia/Muscat'),
(281, 'PA', 'America/Panama'),
(282, 'PE', 'America/Lima'),
(283, 'PF', 'Pacific/Tahiti'),
(284, 'PF', 'Pacific/Marquesas'),
(285, 'PF', 'Pacific/Gambier'),
(286, 'PG', 'Pacific/Port_Moresby'),
(287, 'PG', 'Pacific/Bougainville'),
(288, 'PH', 'Asia/Manila'),
(289, 'PK', 'Asia/Karachi'),
(290, 'PL', 'Europe/Warsaw'),
(291, 'PM', 'America/Miquelon'),
(292, 'PN', 'Pacific/Pitcairn'),
(293, 'PR', 'America/Puerto_Rico'),
(294, 'PS', 'Asia/Gaza'),
(295, 'PS', 'Asia/Hebron'),
(296, 'PT', 'Europe/Lisbon'),
(297, 'PT', 'Atlantic/Madeira'),
(298, 'PT', 'Atlantic/Azores'),
(299, 'PW', 'Pacific/Palau'),
(300, 'PY', 'America/Asuncion'),
(301, 'QA', 'Asia/Qatar'),
(302, 'RE', 'Indian/Reunion'),
(303, 'RO', 'Europe/Bucharest'),
(304, 'RS', 'Europe/Belgrade'),
(305, 'RU', 'Europe/Kaliningrad'),
(306, 'RU', 'Europe/Moscow'),
(307, 'RU', 'Europe/Simferopol'),
(308, 'RU', 'Europe/Volgograd'),
(309, 'RU', 'Europe/Samara'),
(310, 'RU', 'Asia/Yekaterinburg'),
(311, 'RU', 'Asia/Omsk'),
(312, 'RU', 'Asia/Novosibirsk'),
(313, 'RU', 'Asia/Novokuznetsk'),
(314, 'RU', 'Asia/Krasnoyarsk'),
(315, 'RU', 'Asia/Irkutsk'),
(316, 'RU', 'Asia/Chita'),
(317, 'RU', 'Asia/Yakutsk'),
(318, 'RU', 'Asia/Khandyga'),
(319, 'RU', 'Asia/Vladivostok'),
(320, 'RU', 'Asia/Sakhalin'),
(321, 'RU', 'Asia/Ust-Nera'),
(322, 'RU', 'Asia/Magadan'),
(323, 'RU', 'Asia/Srednekolymsk'),
(324, 'RU', 'Asia/Kamchatka'),
(325, 'RU', 'Asia/Anadyr'),
(326, 'RW', 'Africa/Kigali'),
(327, 'SA', 'Asia/Riyadh'),
(328, 'SB', 'Pacific/Guadalcanal'),
(329, 'SC', 'Indian/Mahe'),
(330, 'SD', 'Africa/Khartoum'),
(331, 'SE', 'Europe/Stockholm'),
(332, 'SG', 'Asia/Singapore'),
(333, 'SH', 'Atlantic/St_Helena'),
(334, 'SI', 'Europe/Ljubljana'),
(335, 'SJ', 'Arctic/Longyearbyen'),
(336, 'SK', 'Europe/Bratislava'),
(337, 'SL', 'Africa/Freetown'),
(338, 'SM', 'Europe/San_Marino'),
(339, 'SN', 'Africa/Dakar'),
(340, 'SO', 'Africa/Mogadishu'),
(341, 'SR', 'America/Paramaribo'),
(342, 'SS', 'Africa/Juba'),
(343, 'ST', 'Africa/Sao_Tome'),
(344, 'SV', 'America/El_Salvador'),
(345, 'SX', 'America/Lower_Princes'),
(346, 'SY', 'Asia/Damascus'),
(347, 'SZ', 'Africa/Mbabane'),
(348, 'TC', 'America/Grand_Turk'),
(349, 'TD', 'Africa/Ndjamena'),
(350, 'TF', 'Indian/Kerguelen'),
(351, 'TG', 'Africa/Lome'),
(352, 'TH', 'Asia/Bangkok'),
(353, 'TJ', 'Asia/Dushanbe'),
(354, 'TK', 'Pacific/Fakaofo'),
(355, 'TL', 'Asia/Dili'),
(356, 'TM', 'Asia/Ashgabat'),
(357, 'TN', 'Africa/Tunis'),
(358, 'TO', 'Pacific/Tongatapu'),
(359, 'TR', 'Europe/Istanbul'),
(360, 'TT', 'America/Port_of_Spain'),
(361, 'TV', 'Pacific/Funafuti'),
(362, 'TW', 'Asia/Taipei'),
(363, 'TZ', 'Africa/Dar_es_Salaam'),
(364, 'UA', 'Europe/Kiev'),
(365, 'UA', 'Europe/Uzhgorod'),
(366, 'UA', 'Europe/Zaporozhye'),
(367, 'UG', 'Africa/Kampala'),
(368, 'UM', 'Pacific/Johnston'),
(369, 'UM', 'Pacific/Midway'),
(370, 'UM', 'Pacific/Wake'),
(371, 'US', 'America/New_York'),
(372, 'US', 'America/Detroit'),
(373, 'US', 'America/Kentucky/Louisville'),
(374, 'US', 'America/Kentucky/Monticello'),
(375, 'US', 'America/Indiana/Indianapolis'),
(376, 'US', 'America/Indiana/Vincennes'),
(377, 'US', 'America/Indiana/Winamac'),
(378, 'US', 'America/Indiana/Marengo'),
(379, 'US', 'America/Indiana/Petersburg'),
(380, 'US', 'America/Indiana/Vevay'),
(381, 'US', 'America/Chicago'),
(382, 'US', 'America/Indiana/Tell_City'),
(383, 'US', 'America/Indiana/Knox'),
(384, 'US', 'America/Menominee'),
(385, 'US', 'America/North_Dakota/Center'),
(386, 'US', 'America/North_Dakota/New_Salem'),
(387, 'US', 'America/North_Dakota/Beulah'),
(388, 'US', 'America/Denver'),
(389, 'US', 'America/Boise'),
(390, 'US', 'America/Phoenix'),
(391, 'US', 'America/Los_Angeles'),
(392, 'US', 'America/Metlakatla'),
(393, 'US', 'America/Anchorage'),
(394, 'US', 'America/Juneau'),
(395, 'US', 'America/Sitka'),
(396, 'US', 'America/Yakutat'),
(397, 'US', 'America/Nome'),
(398, 'US', 'America/Adak'),
(399, 'US', 'Pacific/Honolulu'),
(400, 'UY', 'America/Montevideo'),
(401, 'UZ', 'Asia/Samarkand'),
(402, 'UZ', 'Asia/Tashkent'),
(403, 'VA', 'Europe/Vatican'),
(404, 'VC', 'America/St_Vincent'),
(405, 'VE', 'America/Caracas'),
(406, 'VG', 'America/Tortola'),
(407, 'VI', 'America/St_Thomas'),
(408, 'VN', 'Asia/Ho_Chi_Minh'),
(409, 'VU', 'Pacific/Efate'),
(410, 'WF', 'Pacific/Wallis'),
(411, 'WS', 'Pacific/Apia'),
(412, 'YE', 'Asia/Aden'),
(413, 'YT', 'Indian/Mayotte'),
(414, 'ZA', 'Africa/Johannesburg'),
(415, 'ZM', 'Africa/Lusaka'),
(416, 'ZW', 'Africa/Harare');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_training`
--

CREATE TABLE `tbl_training` (
  `training_id` int(5) NOT NULL,
  `employee_id` int(5) NOT NULL,
  `training_name` varchar(100) NOT NULL,
  `vendor_name` varchar(100) NOT NULL,
  `training_start_date` date NOT NULL,
  `training_finish_date` date NOT NULL,
  `training_cost` int(11) NOT NULL,
  `training_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = pending, 1 = started, 2 = completed, 3 = terminated',
  `training_performance` tinyint(1) DEFAULT '0' COMMENT '0 = not concluded, 1 = satisfactory, 2 = average, 3 = poor, 4 = excellent',
  `training_remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_units`
--

CREATE TABLE `tbl_units` (
  `unit_id` int(11) NOT NULL,
  `unit_ar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `unit_en` varchar(200) CHARACTER SET utf8 NOT NULL,
  `unit_desc_ar` text,
  `unit_desc_en` text,
  `designations_id` int(11) NOT NULL,
  `unit_employee_id` varchar(100) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tbl_units`
--

INSERT INTO `tbl_units` (`unit_id`, `unit_ar`, `unit_en`, `unit_desc_ar`, `unit_desc_en`, `designations_id`, `unit_employee_id`) VALUES
(1, 'الوحدة الإفتراضية', 'Default Unit', 'الوصف', 'Description', 1, '2');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(5) NOT NULL,
  `first_name` varchar(100) DEFAULT 'admin',
  `last_name` varchar(100) DEFAULT 'admin',
  `user_name_ar` varchar(200) NOT NULL,
  `user_name_en` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0 =block',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `owner` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `first_name`, `last_name`, `user_name_ar`, `user_name_en`, `email`, `user_name`, `password`, `user_status`, `flag`, `owner`) VALUES
(1, 'admin', 'admin', 'مدير النظام', 'System administrator', 'admin@admin.org', 'admin', '27b0d575bd76125a167ccaee72a873336123e2e4c869a4468f6468160d2e7d69258b2a07a4d7ff882afcc6136f889984b415058f99d8605347f44c4c3479d819', 1, 1, 1),
(28, 'admin', 'admin', 'حساب إستثنائي', 'vice manager', 'vadmin@vadmin.com', 'vadmin', '90225fd7d0a03d547390ffd37698269f64b57d4888bffb089cd68515077ed3610dd2bdfb0d0dae6c5ce70dd346ad01de0abab1ba047ad84d642397ba3f538b32', 1, 1, 2),
(29, 'admin', 'admin', 'صالح المعيش', 'saleh almoaish', 'almoaish@gmail.com', 'almoaish', 'aa5b3b0a6041fd242ee9743b304d590de0b51e78ae4410db7fdda0887a23ec6ff89a4a3433fcdb754a64f6523e06aead0bdb252839689e3a139581a52d77aa30', 1, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_user_role`
--

CREATE TABLE `tbl_user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tbl_user_role`
--

INSERT INTO `tbl_user_role` (`user_role_id`, `user_id`, `menu_id`) VALUES
(962, 28, 2),
(963, 28, 3),
(964, 28, 4),
(965, 28, 5),
(966, 28, 6),
(967, 28, 7),
(968, 28, 8),
(969, 28, 9),
(970, 28, 10),
(971, 28, 125),
(972, 28, 127),
(973, 28, 57),
(974, 28, 58),
(975, 28, 59),
(976, 28, 60),
(977, 28, 98),
(978, 28, 83),
(979, 28, 84),
(980, 1, 2),
(981, 1, 3),
(982, 1, 4),
(983, 1, 5),
(984, 1, 6),
(985, 1, 7),
(986, 1, 8),
(987, 1, 9),
(988, 1, 10),
(989, 1, 125),
(990, 1, 127),
(991, 1, 57),
(992, 1, 58),
(993, 1, 59),
(994, 1, 60),
(995, 1, 98),
(996, 1, 83),
(997, 1, 84),
(998, 1, 93),
(999, 1, 99),
(1000, 1, 101),
(1001, 1, 102),
(1002, 1, 103),
(1003, 1, 104),
(1004, 1, 108),
(1005, 1, 105),
(1006, 1, 106),
(1007, 1, 107),
(1008, 1, 110),
(1009, 1, 111),
(1010, 1, 112),
(1011, 1, 113),
(1012, 1, 114),
(1013, 1, 115),
(1014, 1, 116),
(1015, 1, 126),
(1016, 1, 118),
(1017, 1, 119),
(1018, 1, 120),
(1019, 1, 121),
(1020, 1, 122),
(1021, 1, 123),
(1022, 1, 124);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_vacations`
--

CREATE TABLE `tbl_vacations` (
  `id` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `employee_id` int(5) NOT NULL,
  `affect_stock` tinyint(4) NOT NULL,
  `discount_perc` int(3) NOT NULL,
  `calc_annually` tinyint(4) NOT NULL COMMENT 'if =1 : calculate by normal year / i f=0 : calculate from begin',
  `assign_rep` tinyint(4) NOT NULL,
  `rep_employee_id` int(11) DEFAULT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `date_assign` datetime DEFAULT NULL,
  `notified` tinyint(4) NOT NULL DEFAULT '0',
  `ended` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_working_days`
--

CREATE TABLE `tbl_working_days` (
  `working_days_id` int(11) NOT NULL,
  `day_id` int(5) NOT NULL,
  `flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tbl_working_hours`
--

CREATE TABLE `tbl_working_hours` (
  `working_hours_id` int(11) NOT NULL,
  `start_hours` time NOT NULL,
  `end_hours` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tpl_employee_category`
--

CREATE TABLE `tpl_employee_category` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name_en` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tpl_employee_category`
--

INSERT INTO `tpl_employee_category` (`id`, `name_ar`, `name_en`) VALUES
(1, 'لا يوجد', 'N/A'),
(2, 'فئة جديدة', 'new cat'),
(3, 'test', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `tpl_evaluation_items`
--

CREATE TABLE `tpl_evaluation_items` (
  `evaluation_items_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `name_ar` varchar(200) CHARACTER SET utf8 NOT NULL,
  `name_en` varchar(200) CHARACTER SET utf8 NOT NULL,
  `type` tinyint(2) NOT NULL COMMENT '1=percentage,2=rating',
  `ei_desc_ar` text CHARACTER SET utf8,
  `ei_desc_en` text CHARACTER SET utf8,
  `job_titles_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tpl_evaluation_items`
--

INSERT INTO `tpl_evaluation_items` (`evaluation_items_id`, `department_id`, `name_ar`, `name_en`, `type`, `ei_desc_ar`, `ei_desc_en`, `job_titles_id`) VALUES
(2, 1, 'بند تقييم للإدارة المالية', 'evaluation item', 2, 'الوصف', 'Description', 0),
(3, 0, 'eryery', 'erytert', 1, 'dfhgdfg', 'dfgdfgdfg', 4),
(4, 0, 'test', 'test', 2, 'test', 'test', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`idCountry`);

--
-- Index pour la table `sms_templates`
--
ALTER TABLE `sms_templates`
  ADD PRIMARY KEY (`sms_template_id`);

--
-- Index pour la table `tbl_accountings`
--
ALTER TABLE `tbl_accountings`
  ADD PRIMARY KEY (`accounting_id`);

--
-- Index pour la table `tbl_advance`
--
ALTER TABLE `tbl_advance`
  ADD PRIMARY KEY (`advance_id`);

--
-- Index pour la table `tbl_advances`
--
ALTER TABLE `tbl_advances`
  ADD PRIMARY KEY (`advances_id`);

--
-- Index pour la table `tbl_allowance`
--
ALTER TABLE `tbl_allowance`
  ADD PRIMARY KEY (`allowance_type_id`);

--
-- Index pour la table `tbl_allowance_category`
--
ALTER TABLE `tbl_allowance_category`
  ADD PRIMARY KEY (`allowance_id`);

--
-- Index pour la table `tbl_applications`
--
ALTER TABLE `tbl_applications`
  ADD PRIMARY KEY (`application_id`);

--
-- Index pour la table `tbl_application_notes`
--
ALTER TABLE `tbl_application_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Index pour la table `tbl_approvals_cat`
--
ALTER TABLE `tbl_approvals_cat`
  ADD PRIMARY KEY (`approval_cat_id`);

--
-- Index pour la table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Index pour la table `tbl_attendance_xh`
--
ALTER TABLE `tbl_attendance_xh`
  ADD PRIMARY KEY (`attendance_xh_id`);

--
-- Index pour la table `tbl_bonuses`
--
ALTER TABLE `tbl_bonuses`
  ADD PRIMARY KEY (`bonuse_id`);

--
-- Index pour la table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  ADD PRIMARY KEY (`branche_id`);

--
-- Index pour la table `tbl_caching`
--
ALTER TABLE `tbl_caching`
  ADD PRIMARY KEY (`caching_id`);

--
-- Index pour la table `tbl_center_documentations`
--
ALTER TABLE `tbl_center_documentations`
  ADD PRIMARY KEY (`doc_id`);

--
-- Index pour la table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Index pour la table `tbl_deduction`
--
ALTER TABLE `tbl_deduction`
  ADD PRIMARY KEY (`deduction_type_id`);

--
-- Index pour la table `tbl_deduction_category`
--
ALTER TABLE `tbl_deduction_category`
  ADD PRIMARY KEY (`deduction_id`);

--
-- Index pour la table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Index pour la table `tbl_designations`
--
ALTER TABLE `tbl_designations`
  ADD PRIMARY KEY (`designations_id`);

--
-- Index pour la table `tbl_discount_category`
--
ALTER TABLE `tbl_discount_category`
  ADD PRIMARY KEY (`discount_id`);

--
-- Index pour la table `tbl_divisions`
--
ALTER TABLE `tbl_divisions`
  ADD PRIMARY KEY (`division_id`);

--
-- Index pour la table `tbl_draft`
--
ALTER TABLE `tbl_draft`
  ADD PRIMARY KEY (`draft_id`);

--
-- Index pour la table `tbl_embarkation`
--
ALTER TABLE `tbl_embarkation`
  ADD PRIMARY KEY (`embarkation_id`);

--
-- Index pour la table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Index pour la table `tbl_employee_award`
--
ALTER TABLE `tbl_employee_award`
  ADD PRIMARY KEY (`employee_award_id`);

--
-- Index pour la table `tbl_employee_custody`
--
ALTER TABLE `tbl_employee_custody`
  ADD PRIMARY KEY (`custody_id`);

--
-- Index pour la table `tbl_employee_document`
--
ALTER TABLE `tbl_employee_document`
  ADD PRIMARY KEY (`document_id`);

--
-- Index pour la table `tbl_employee_login`
--
ALTER TABLE `tbl_employee_login`
  ADD PRIMARY KEY (`employee_login_id`);

--
-- Index pour la table `tbl_evaluations`
--
ALTER TABLE `tbl_evaluations`
  ADD PRIMARY KEY (`evaluation_id`);

--
-- Index pour la table `tbl_extra_hours`
--
ALTER TABLE `tbl_extra_hours`
  ADD PRIMARY KEY (`extra_hours_id`);

--
-- Index pour la table `tbl_extra_work`
--
ALTER TABLE `tbl_extra_work`
  ADD PRIMARY KEY (`extra_work_id`);

--
-- Index pour la table `tbl_finance_info`
--
ALTER TABLE `tbl_finance_info`
  ADD PRIMARY KEY (`finance_info_id`);

--
-- Index pour la table `tbl_gsettings`
--
ALTER TABLE `tbl_gsettings`
  ADD PRIMARY KEY (`id_gsettings`);

--
-- Index pour la table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Index pour la table `tbl_houcing`
--
ALTER TABLE `tbl_houcing`
  ADD PRIMARY KEY (`houcing_id`);

--
-- Index pour la table `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  ADD PRIMARY KEY (`inbox_id`);

--
-- Index pour la table `tbl_irrigularities`
--
ALTER TABLE `tbl_irrigularities`
  ADD PRIMARY KEY (`irrigularity_id`);

--
-- Index pour la table `tbl_irrigularity_category`
--
ALTER TABLE `tbl_irrigularity_category`
  ADD PRIMARY KEY (`irrigularity_category_id`);

--
-- Index pour la table `tbl_job_appliactions`
--
ALTER TABLE `tbl_job_appliactions`
  ADD PRIMARY KEY (`job_appliactions_id`);

--
-- Index pour la table `tbl_job_places`
--
ALTER TABLE `tbl_job_places`
  ADD PRIMARY KEY (`job_place_id`);

--
-- Index pour la table `tbl_job_titles`
--
ALTER TABLE `tbl_job_titles`
  ADD PRIMARY KEY (`job_titles_id`);

--
-- Index pour la table `tbl_languages`
--
ALTER TABLE `tbl_languages`
  ADD PRIMARY KEY (`code`);

--
-- Index pour la table `tbl_leaves`
--
ALTER TABLE `tbl_leaves`
  ADD PRIMARY KEY (`leave_id`);

--
-- Index pour la table `tbl_leave_category`
--
ALTER TABLE `tbl_leave_category`
  ADD PRIMARY KEY (`leave_category_id`);

--
-- Index pour la table `tbl_loan_list`
--
ALTER TABLE `tbl_loan_list`
  ADD PRIMARY KEY (`loan_list_id`);

--
-- Index pour la table `tbl_maintenaces`
--
ALTER TABLE `tbl_maintenaces`
  ADD PRIMARY KEY (`maintenance_id`);

--
-- Index pour la table `tbl_medical_insurane`
--
ALTER TABLE `tbl_medical_insurane`
  ADD PRIMARY KEY (`medical_id`);

--
-- Index pour la table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Index pour la table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Index pour la table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Index pour la table `tbl_other_document_path`
--
ALTER TABLE `tbl_other_document_path`
  ADD PRIMARY KEY (`odp_id`);

--
-- Index pour la table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`permission_id`);

--
-- Index pour la table `tbl_permission_list`
--
ALTER TABLE `tbl_permission_list`
  ADD PRIMARY KEY (`permission_id`);

--
-- Index pour la table `tbl_provision`
--
ALTER TABLE `tbl_provision`
  ADD PRIMARY KEY (`provision_id`);

--
-- Index pour la table `tbl_provision_category`
--
ALTER TABLE `tbl_provision_category`
  ADD PRIMARY KEY (`provision_category_id`);

--
-- Index pour la table `tbl_purchases`
--
ALTER TABLE `tbl_purchases`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Index pour la table `tbl_recrutements`
--
ALTER TABLE `tbl_recrutements`
  ADD PRIMARY KEY (`recrutement_id`);

--
-- Index pour la table `tbl_reminder`
--
ALTER TABLE `tbl_reminder`
  ADD PRIMARY KEY (`reminder_id`);

--
-- Index pour la table `tbl_sent`
--
ALTER TABLE `tbl_sent`
  ADD PRIMARY KEY (`sent_id`);

--
-- Index pour la table `tbl_sms_config`
--
ALTER TABLE `tbl_sms_config`
  ADD PRIMARY KEY (`sms_config_id`);

--
-- Index pour la table `tbl_social_insurane`
--
ALTER TABLE `tbl_social_insurane`
  ADD PRIMARY KEY (`social_id`);

--
-- Index pour la table `tbl_timezone`
--
ALTER TABLE `tbl_timezone`
  ADD PRIMARY KEY (`timezone_id`),
  ADD KEY `idx_zone_name` (`timezone_name`);

--
-- Index pour la table `tbl_training`
--
ALTER TABLE `tbl_training`
  ADD PRIMARY KEY (`training_id`);

--
-- Index pour la table `tbl_units`
--
ALTER TABLE `tbl_units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Index pour la table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  ADD PRIMARY KEY (`user_role_id`);

--
-- Index pour la table `tbl_vacations`
--
ALTER TABLE `tbl_vacations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tbl_working_days`
--
ALTER TABLE `tbl_working_days`
  ADD PRIMARY KEY (`working_days_id`);

--
-- Index pour la table `tbl_working_hours`
--
ALTER TABLE `tbl_working_hours`
  ADD PRIMARY KEY (`working_hours_id`);

--
-- Index pour la table `tpl_employee_category`
--
ALTER TABLE `tpl_employee_category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tpl_evaluation_items`
--
ALTER TABLE `tpl_evaluation_items`
  ADD PRIMARY KEY (`evaluation_items_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `countries`
--
ALTER TABLE `countries`
  MODIFY `idCountry` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT pour la table `sms_templates`
--
ALTER TABLE `sms_templates`
  MODIFY `sms_template_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_accountings`
--
ALTER TABLE `tbl_accountings`
  MODIFY `accounting_id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_advance`
--
ALTER TABLE `tbl_advance`
  MODIFY `advance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_advances`
--
ALTER TABLE `tbl_advances`
  MODIFY `advances_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `tbl_allowance`
--
ALTER TABLE `tbl_allowance`
  MODIFY `allowance_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_allowance_category`
--
ALTER TABLE `tbl_allowance_category`
  MODIFY `allowance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_applications`
--
ALTER TABLE `tbl_applications`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `tbl_application_notes`
--
ALTER TABLE `tbl_application_notes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `tbl_approvals_cat`
--
ALTER TABLE `tbl_approvals_cat`
  MODIFY `approval_cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `tbl_attendance_xh`
--
ALTER TABLE `tbl_attendance_xh`
  MODIFY `attendance_xh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `tbl_bonuses`
--
ALTER TABLE `tbl_bonuses`
  MODIFY `bonuse_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  MODIFY `branche_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_caching`
--
ALTER TABLE `tbl_caching`
  MODIFY `caching_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_center_documentations`
--
ALTER TABLE `tbl_center_documentations`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_deduction`
--
ALTER TABLE `tbl_deduction`
  MODIFY `deduction_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_deduction_category`
--
ALTER TABLE `tbl_deduction_category`
  MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `department_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_designations`
--
ALTER TABLE `tbl_designations`
  MODIFY `designations_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_discount_category`
--
ALTER TABLE `tbl_discount_category`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_divisions`
--
ALTER TABLE `tbl_divisions`
  MODIFY `division_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_draft`
--
ALTER TABLE `tbl_draft`
  MODIFY `draft_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_embarkation`
--
ALTER TABLE `tbl_embarkation`
  MODIFY `embarkation_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `employee_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT pour la table `tbl_employee_award`
--
ALTER TABLE `tbl_employee_award`
  MODIFY `employee_award_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_employee_custody`
--
ALTER TABLE `tbl_employee_custody`
  MODIFY `custody_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_employee_document`
--
ALTER TABLE `tbl_employee_document`
  MODIFY `document_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_employee_login`
--
ALTER TABLE `tbl_employee_login`
  MODIFY `employee_login_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `tbl_evaluations`
--
ALTER TABLE `tbl_evaluations`
  MODIFY `evaluation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tbl_extra_hours`
--
ALTER TABLE `tbl_extra_hours`
  MODIFY `extra_hours_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_extra_work`
--
ALTER TABLE `tbl_extra_work`
  MODIFY `extra_work_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_finance_info`
--
ALTER TABLE `tbl_finance_info`
  MODIFY `finance_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_gsettings`
--
ALTER TABLE `tbl_gsettings`
  MODIFY `id_gsettings` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_holiday`
--
ALTER TABLE `tbl_holiday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_houcing`
--
ALTER TABLE `tbl_houcing`
  MODIFY `houcing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  MODIFY `inbox_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `tbl_irrigularities`
--
ALTER TABLE `tbl_irrigularities`
  MODIFY `irrigularity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_irrigularity_category`
--
ALTER TABLE `tbl_irrigularity_category`
  MODIFY `irrigularity_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_job_appliactions`
--
ALTER TABLE `tbl_job_appliactions`
  MODIFY `job_appliactions_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_job_places`
--
ALTER TABLE `tbl_job_places`
  MODIFY `job_place_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_job_titles`
--
ALTER TABLE `tbl_job_titles`
  MODIFY `job_titles_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tbl_leaves`
--
ALTER TABLE `tbl_leaves`
  MODIFY `leave_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tbl_leave_category`
--
ALTER TABLE `tbl_leave_category`
  MODIFY `leave_category_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_loan_list`
--
ALTER TABLE `tbl_loan_list`
  MODIFY `loan_list_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_maintenaces`
--
ALTER TABLE `tbl_maintenaces`
  MODIFY `maintenance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_medical_insurane`
--
ALTER TABLE `tbl_medical_insurane`
  MODIFY `medical_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT pour la table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `tbl_other_document_path`
--
ALTER TABLE `tbl_other_document_path`
  MODIFY `odp_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_permission_list`
--
ALTER TABLE `tbl_permission_list`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_provision`
--
ALTER TABLE `tbl_provision`
  MODIFY `provision_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_provision_category`
--
ALTER TABLE `tbl_provision_category`
  MODIFY `provision_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_purchases`
--
ALTER TABLE `tbl_purchases`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_recrutements`
--
ALTER TABLE `tbl_recrutements`
  MODIFY `recrutement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tbl_reminder`
--
ALTER TABLE `tbl_reminder`
  MODIFY `reminder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_sent`
--
ALTER TABLE `tbl_sent`
  MODIFY `sent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `tbl_sms_config`
--
ALTER TABLE `tbl_sms_config`
  MODIFY `sms_config_id` tinyint(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_social_insurane`
--
ALTER TABLE `tbl_social_insurane`
  MODIFY `social_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_timezone`
--
ALTER TABLE `tbl_timezone`
  MODIFY `timezone_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=417;

--
-- AUTO_INCREMENT pour la table `tbl_training`
--
ALTER TABLE `tbl_training`
  MODIFY `training_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_units`
--
ALTER TABLE `tbl_units`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `tbl_user_role`
--
ALTER TABLE `tbl_user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1023;

--
-- AUTO_INCREMENT pour la table `tbl_vacations`
--
ALTER TABLE `tbl_vacations`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_working_days`
--
ALTER TABLE `tbl_working_days`
  MODIFY `working_days_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tbl_working_hours`
--
ALTER TABLE `tbl_working_hours`
  MODIFY `working_hours_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tpl_employee_category`
--
ALTER TABLE `tpl_employee_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tpl_evaluation_items`
--
ALTER TABLE `tpl_evaluation_items`
  MODIFY `evaluation_items_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
