<?php

// menu
$lang['dashboard'] = 'Dashboard';
$lang['job_titles'] = 'Job Titles';
$lang['departements'] = 'Departements';
$lang['designations'] = 'Sections';
$lang['employee_category'] = 'Employee Category';
$lang['evaluation_items'] = 'Evaluation Items';
$lang['organizational_chart'] = 'Organizational Chart';
$lang['mailbox'] = 'Mailbox';
$lang['inbox'] = 'Inbox';
$lang['draft'] = 'Draft';
$lang['sent'] = 'Sent';
$lang['trash'] = 'Trash';
$lang['task'] = 'Task';
$lang['notice'] = 'Notice';
$lang['events'] = 'Events';
$lang['attendance'] = 'Attendance';
$lang['timechange_request'] = 'Timechange Request';
$lang['attendance_report'] = 'Attendance Report';
$lang['overtime'] = 'Overtime';
$lang['application_list'] = 'Application List';
$lang['stock'] = 'Stock';
$lang['expenses'] = 'Expenses';
$lang['award'] = 'Award';
$lang['payroll'] = 'Payroll';
$lang['set_salary_details'] = 'Set Salary Details';
$lang['employee_salary_list'] = 'Employee Salary List';
$lang['make_payment'] = 'Make Payment';
$lang['generate_payslip'] = 'Generate Payslip';
$lang['training'] = 'Training';
$lang['department'] = 'Department';
$lang['employee'] = 'Employee';
$lang['manage_employee'] = 'Manage Employee';
$lang['set_access_role'] = 'Set Access Role';
$lang['settings'] = 'Settings';
$lang['general_settings'] = 'General Settings';
$lang['set_working_days'] = 'Set Working Days';
$lang['personal_event'] = 'Personal Event';
$lang['leave_category'] = 'Leave Category';
$lang['notification_settings'] = 'Notification Settings';
$lang['language_settings'] = 'Language Settings';
$lang['database_backup'] = 'Database Backup';
$lang['stock_category'] = 'Stock Category';
$lang['add_stock'] = 'Add Stock';
$lang['stock_list'] = 'Stock List';
$lang['hourly_rate'] = 'Payroll Template';

// dashboard
$lang['hr_title'] = 'HR Lite';
$lang['total_department'] = 'Total Department';
$lang['more_info'] = 'More Info';
$lang['total_employee'] = 'Total Employee';
$lang['total_award'] = 'Total Award';
$lang['total_stock'] = 'Total Stock';
$lang['total_expense'] = 'Total Expense';
$lang['expense_report'] = 'Expense Report';
$lang['select_year'] = 'Select Year';
$lang['recent_application'] = 'Recent Applications';
$lang['view_all'] = 'View All';
$lang['upcomming_birthday'] = 'Upcoming Birthday';
$lang['today'] = 'Today';
$lang['recent_notice'] = 'Recent Notice';
$lang['view_details'] = 'Details';

$lang['recent_email'] = 'Recent Email';
$lang['compose'] = 'Compose';
$lang['minute'] = 'Minutes';
$lang['ago'] = 'ago';
$lang['hour'] = 'hour';
$lang['hours'] = 'hours';
$lang['day'] = 'day';
$lang['days'] = 'days';
$lang['nothing_to_display'] = 'Nothing to display';
$lang['personal_event'] = 'Personal Event';
$lang['event_name'] = 'Event Name';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['close'] = 'Close';
$lang['update'] = 'Update';
$lang['inbox_details'] = 'Inbox Details';
$lang['sent_details'] = 'Sent Details';
$lang['compose_new'] = 'Compose New Message';
$lang['attachment'] = 'Attachment';
$lang['save_to_draft'] = 'Save to Draft';
$lang['send'] = 'Send';
$lang['discard'] = 'Discard';
$lang['discard_alert'] = 'Are you sure to discard this email ? ';
$lang['message_permanent_deleted'] = 'Meassage Information Permanently Deleted !';
$lang['message_deleted'] = 'Meassage Information Successfully Deleted !';
$lang['message_sent'] = 'Meassage Information Successfully Sent !';
$lang['please_select_message'] = "Please Select a message !";
$lang['delete_alert'] = 'You are about to delete a record. This cannot be undone. Are you sure?';


// task
$lang['task_list'] = 'Task List';
$lang['assign_task'] = 'Assign Task';
$lang['task_name'] = 'Task Name';
$lang['created_date'] = 'Created Date';
$lang['due_date'] = 'Due Date';
$lang['status'] = 'Status';
$lang['changes/view'] = 'Change / View';
$lang['assined_to'] = 'Assigned To';
$lang['select_employee'] = 'Select From Employees';
$lang['select_admin'] = 'Select From Administrators';
$lang['estimated_hour'] = 'Estimated Hour ';
$lang['input_value'] = 'Input Whole Value';
$lang['task_description'] = 'Task Description';
$lang['progress'] = 'Progress';
$lang['task_status'] = 'Task Status';
$lang['pending'] = 'Pending';
$lang['started'] = 'Started';
$lang['completed'] = 'Completed';
$lang['save'] = 'Save';
$lang['task_management'] = "Task Management";
$lang['all_task'] = "All Task";
$lang['save_task'] = "Task Information Successfully Updated!";
$lang['task_updated'] = "Task Successfully Updated!";
$lang['task_comment_save'] = "Task Comment Successfully Updated!";
$lang['task_file_updated'] = 'Task file information successfully updated';
$lang['task_file_added'] = 'Task file information successfully Added';
$lang['task_details'] = "Task Details";
$lang['operation_failed'] = 'Operation Failed !';
$lang['task_initials'] = 'Task Initials';
$lang['update'] = 'Update';
$lang['details'] = 'Details';
$lang['comments'] = 'Comments';
$lang['post_comment'] = 'Post Comments';
$lang['change'] = 'Change';
$lang['select_file'] = 'Select file';
$lang['add_more'] = 'Add More';
$lang['upload_file'] = 'Upload Files';
$lang['file_title'] = 'File Title';
$lang['description'] = 'Description';
$lang['attach_file_list'] = 'Attachment File List ';
$lang['files'] = 'Files';
$lang['size'] = 'Size';
$lang['date'] = 'Date';
$lang['uploaded_by'] = 'Uploaded By';
$lang['action'] = 'Action';

// notice
$lang['notice'] = 'Notice';
$lang['all_notice'] = 'All Notice';
$lang['new_notice'] = 'New Notice';
$lang['short_description'] = 'Short Descriptions';
$lang['long_description'] = 'Long Descriptions';
$lang['sl'] = 'Num';
$lang['title'] = 'Title';
$lang['publication_status'] = 'Publication Status';
$lang['published'] = 'Published';
$lang['unpublished'] = 'Unpublished';
$lang['notice_details'] = 'Notice Details';
$lang['published_date'] = 'Published Date';
$lang['no_record_found'] = 'No Record Found';
$lang['notice_saved'] = "Notice Successfully Saved!";
$lang['deleted_notice'] = "Notice Successfully Deleted !";

// event
$lang['year'] = 'year';
$lang['event_list'] = 'Event List';
$lang['new_event'] = 'New Event';
$lang['event_name'] = 'Event Name';
$lang['event_management'] = "Event Management";
$lang['event_already_exist'] = "Holiday Information Already Exist!";
$lang['event_saved'] = "Holiday Information Successfully Saved!";
$lang['event_deleted'] = "Holiday Information Successfully Delete!";

// attendance
$lang['overtime_details'] = "Overtime Details";
$lang['overtime_list'] = "Overtime List";
$lang['new_overtime'] = "New Overtime";
$lang['overtime_management'] = "Overtime Management";
$lang['overtime_already_exist'] = "Overtime Information Already Exist !";
$lang['overtime_saved'] = "Overtime Information Successfully Saved !";
$lang['overtime_deleted'] = "Overtime Information Successfully Delete !";
$lang['time_changes_request'] = "Time Change Request";
$lang['attendance_management'] = "Attendance Management";
$lang['view_time_change_request'] = "View Time Change Request";
$lang['time_change_accept'] = "Time Change Request Accepted !";
$lang['time_change_decline'] = "Time Change Request Decline !";
$lang['attendance_report'] = "Attendance Report";
$lang['department_name'] = "Department Name ";
$lang['department'] = "Department";
$lang['month_year'] = "Month & Year";
$lang['search'] = "Search";
$lang['name'] = "Name";
$lang['holiday'] = "Holiday";
$lang['on_leave'] = "On Leave";
$lang['absent'] = "Absent";
$lang['total_working_hour'] = "Total Hour Work This Week";
$lang['employee_attendance'] = "Employee Attendance";
$lang['month'] = "Month";
$lang['week'] = "Week";
$lang['list_of_all_time_change_request'] = "List of All Time Change Request";
$lang['time_in'] = "Time In";
$lang['time_out'] = "Time Out";
$lang['accepted'] = "Accepted";
$lang['rejected'] = "Rejected";
$lang['employee_name'] = "Employee Name";
$lang['overtime_date'] = "Overtime Date";
$lang['overtime_hour'] = "Overtime Hour";
$lang['total_overtime_hour'] = "Total Overtime Hours";
$lang['select_designation'] = "Select Designation";
$lang['employee'] = "Employee";
$lang['overtime_report'] = "Overtime Report";
$lang['company_info'] = "Company Info";
$lang['time_change_request_details'] = "Time Change Request Details";
$lang['old_time_in'] = "Old Time in";
$lang['old_time_out'] = "Old Time out";
$lang['new_time_out'] = "New Time out";
$lang['new_time_in'] = "New Time in";
$lang['reason'] = "Reason";

// application list
$lang['application_list'] = 'Application List';
$lang['application_management'] = 'Application Management';
$lang['changes_application_status'] = '"Application Status Successfully Changed!"';
$lang['application_details'] = '"Application  Details"';
$lang['leave_date'] = 'Leave Date';
$lang['from'] = 'From';
$lang['to'] = 'From';
$lang['leave_type'] = 'Leave Type';
$lang['applied_on'] = 'Applied On';
$lang['leave_reason'] = 'Leave Reason';
$lang['current_status'] = 'Current Status';
$lang['approved_by'] = 'Approved By';
$lang['give_comment'] = 'Give Comment';
$lang['leave_date_of'] = 'Leave Details of';
$lang['total'] = 'Total';

// stock
$lang['stock_category'] = 'Stock Category';
$lang['stock_management'] = 'Stock Management';
$lang['category_saved'] = 'Category Information Successfully Saved!';
$lang['stock_category_exist'] = 'Stock Category Information Already Used !';
$lang['stock_category_delete'] = 'Stock Category Information Successfully Delete!';
$lang['sub_category_used'] = 'Stock Sub Category Information Already Used !';
$lang['sub_category_deleted'] = 'Stock Sub Category Information Successfully Delete!';
$lang['manage_stock'] = 'Manage Stock';
$lang['stock_saved'] = 'Stock Information Successfully Added!';
$lang['stock_history'] = 'Stock History';
$lang['history'] = 'History';
$lang['stock_deleted'] = 'Stock Information Successfully Deleted !';
$lang['new_category'] = 'New Category';
$lang['sub_category'] = 'Sub Category';
$lang['select_category'] = 'Select Category';
$lang['item_name'] = 'Item Name';
$lang['inventory'] = 'Inventory';
$lang['buying_date'] = 'Buying Date';

// expense
$lang['expense_saved'] = 'Expense Information Successfully Saved!';
$lang['expense_details'] = 'Expense Details';
$lang['expense_management'] = 'Expense Management';
$lang['expense_status_update'] = 'Expense Information Successfully Updated!';
$lang['expense_status_update'] = 'Expense Information Successfully Updated!';
$lang['all_expense'] = 'All Expense';
$lang['new_expense'] = 'New Expense';
$lang['purchase_from'] = 'Purchase From';
$lang['purchase_date'] = 'Purchase Date';
$lang['amount'] = 'Amount';
$lang['purchase_by'] = 'Purchased By';
$lang['bill_copy'] = 'Bill Copy';
$lang['cancel'] = 'Cancel';
$lang['approved'] = 'Approved';

// award
$lang['employee_award'] = 'Employee Award';
$lang['award_management'] = 'Award Management';
$lang['award_information_saved'] = 'Award Information Successfully Saved!';
$lang['award_information_delete'] = 'Award Information Successfully Delete !';
$lang['award_list'] = 'Award List';
$lang['give_award'] = 'Give Award';
$lang['give_award'] = 'Give Award';
$lang['award_name'] = 'Award Name';
$lang['gift'] = 'Gift';
$lang['gift_item'] = 'Gift Item';
$lang['select_month'] = 'Select Month';
$lang['employee_award_list'] = 'Employee Award List';

//payroll
$lang['hourly_rate_details'] = "Hourly Rate Details";
$lang['payroll_page_header'] = "Payroll Management";
$lang['payroll_template_save'] = 'Payroll Template Information Successfully Saved !';
$lang['hourly_rate_save'] = 'Hourly Rate Information Successfully Delete';
$lang['manage_salary_details'] = "Manage Salery Details";
$lang['salary_detail_save'] = 'Salary Details Information Successfully Save';
$lang['employee_salary_details'] = "Employee Salery Details";
$lang['view_salary_details'] = "View Salery Details";
$lang['working_hour_empty'] = "Working hours is empty !";
$lang['payment_salary_details'] = 'Payment Salary Details';
$lang['payment_info_updated'] = 'Payment Information Successfully Updated !';
$lang['generate_payslip'] = "Generate Payslip";
$lang['employee_id'] = "Employee ID";
$lang['designation'] = "Department & Section";
$lang['joining_date'] = "Joining Date";
$lang['salary_grade'] = "Salary Grade";
$lang['overtime_rate'] = "Overtime Rate";
$lang['list_of_all_employees'] = "List of Employees and Their Salaries";
$lang['full_name'] = "Full Name";
$lang['salary_type'] = "Salary Type";
$lang['basic_salary'] = "Basic Salary";
$lang['per_hour'] = "Per Hour";
$lang['salary_details'] = "Salary Details";
$lang['allowance_details'] = "Allowance Details";
$lang['deduction_details'] = "Deduction Details";
$lang['gross_salary'] = "Gross Salary";
$lang['total_salary_details'] = "Total Salary Details";
$lang['total_deduction'] = "Total Deduction";
$lang['net_salary'] = "Net Salary";
$lang['company_info'] = "Company Info";
$lang['select_department'] = "Select Department";
$lang['select_month'] = "Select Month";
$lang['go'] = "GO";
$lang['generate_payslip_for'] = "Generate Payslip For";
$lang['payroll_template_list'] = "Payroll Template List";
$lang['add_new_template'] = "Add New Template";
$lang['template_name'] = "Template Name";
$lang['payment_for'] = "Payment For";
$lang['overtime_salary'] = "Overtime Salary";
$lang['payment_amount'] = "Payment Amount";
$lang['payment_type'] = "Payment Type";
$lang['payment_month'] = "Payment Month";
$lang['payment_date'] = "Payment Date";
$lang['paid_amount'] = "Paid Amount";
$lang['payroll_template'] = "Payroll Template";
$lang['fine_deduction'] = "Fine Deduction";
$lang['paylsip'] = "Payslip";
$lang['salary_month'] = "Salary Month";
$lang['payslip_no'] = "Payslip NO";
$lang['mobile'] = "Mobile";
$lang['payment_method'] = "Payment Method";
$lang['payment_details'] = "Payment Details";
$lang['earning'] = "Earning";
$lang['gross_income'] = "Gross Income";
$lang['overtime_amount'] = "Overtime Amount";
$lang['award_amount'] = "Award Amount";
$lang['total'] = "Total";
$lang['working_hour_amount'] = "Working Hour Amount";
$lang['salary_template_list'] = "Salary Template List";
$lang['salary_template'] = "Salary Template";
$lang['allowance'] = "Allowance";
$lang['deduction'] = "Deduction";
$lang['salary_template_details'] = "Salary Template Details";
$lang['overtime_hour_amount'] = "Overtime Hour Amount";

// Job Circular
$lang['job_circular'] = 'Job Circular';
$lang['jobs_posted'] = 'Job Posted';
$lang['jobs_applications'] = 'Job Applications';

//training
$lang['all_training'] = "All Training";
$lang['training_page_header'] = "Training Management";
$lang['view_training'] = "View Training";
$lang['training_saved_message'] = "Training Information Successfully Saved!";
$lang['course_training'] = "Course / Training";
$lang['vendor'] = "Vendor";
$lang['finish_date'] = "Finish Date";
$lang['training_cost'] = "Training Cost";
$lang['terminated'] = "Terminated";
$lang['not_concluded'] = "Not Concluded";
$lang['satisfactory'] = "Satisfactory";
$lang['average'] = "Average";
$lang['poor'] = "Poor";
$lang['excellent'] = "Excellent";
$lang['remarks'] = "Remarks";
$lang['training_details'] = "Training Details";
$lang['training_list'] = "Training List";
$lang['add_training'] = "Add Training";
$lang['cost'] = "Cost";
$lang['performance'] = "Performance";

//department
$lang['department_list'] = "Department List";
$lang['department_page_header'] = "Department Management";
$lang['department_info_saved'] = "Department Information Successfully Saved!";
$lang['department_info_deleted'] = "Department Information Successfully Delete!";
$lang['designation_info_used'] = "Designation Information Already Used !";
$lang['designation_info_deleted'] = "Designation Information Successfully Delete!";
$lang['add_department'] = "Add Department";
$lang['add_desingation'] = "Add Designation";


//employee
$lang['employee_list'] = "Employee List";
$lang['employee_page_header'] = "Employee Management";
$lang['view_employee'] = "View Employee";
$lang['employee_info_saved'] = "Employee Information Successfully Saved!";
$lang['employee_info_deleted'] = "Employee Information Successfully Deleted !";
$lang['add_employee'] = "Add Employee";
$lang['dept_desingation'] = "Dept > Designation";
$lang['personal_details'] = "Personal Details";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['date_of_birth'] = "Date Of Birth";
$lang['gender'] = "Gender";
$lang['maratial_status'] = "Maratial Status";
$lang['male'] = "Male";
$lang['female'] = "Female";
$lang['married'] = "Married";
$lang['un-married'] = "Un-Married";
$lang['widowed'] = "Widowed";
$lang['divorced'] = "Divorced";
$lang['fathers_name'] = "Fathers Name";
$lang['nationality'] = "Nationality";
$lang['passport_no'] = "Passport NO";
$lang['photo'] = "Photo";
$lang['contact_details'] = "Contact Details";
$lang['present_address'] = "Present Address";
$lang['employee_document'] = "Employee Document";
$lang['resume'] = "Resume";
$lang['offer_letter'] = "Offer Letter";
$lang['joining_letter'] = "Joining Letter";
$lang['contract_paper'] = "Contract Paper";
$lang['id_proff'] = "ID Proff";
$lang['other_documents'] = "Other Documents";
$lang['bank_information'] = "Bank Information";
$lang['bank_name'] = "Bank Name";
$lang['branch_name'] = "Branch Name";
$lang['account_name'] = "Account Name";
$lang['account_number'] = "Account Number";
$lang['official_status'] = "Official Status";
$lang['inactive'] = "Inactive";
$lang['in_test'] = "In Test";
$lang['test_period'] = "Test Period";


//settings
$lang['general_settings'] = 'General Settings';
$lang['settings_management'] = 'Settings Management';
$lang['general_settings_saved'] = 'Company Information Successfully Update!';
$lang['set_working_days'] = 'Set Working Days';
$lang['working_dasy_saved'] = 'Working Days Successfully Saved!';
$lang['working_hour_saved'] = 'Working Hours Successfully Saved !';
$lang['holiday_list'] = 'Holiday List';
$lang['new_holiday'] = 'New Holiday';
$lang['holiday_information_exist'] = 'Holiday Information Already Exist!';
$lang['holiday_information_saved'] = 'Holiday Information Successfully Saved!';
$lang['holoday_information_delete'] = 'Holiday Information Successfully Delete!';
$lang['leave_application'] = 'Leave Application';
$lang['notification_error_mailbox'] = 'Notification status can not be changed yet - no mail received in inbox !';
$lang['notification_error_mailbox'] = 'Notification status can not be changed yet - no notice created !';
$lang['notification_error_notice'] = 'Notification status can not be changed yet - no notice created !';
$lang['notification_error_application'] = 'Notification status can not be changed yet - no application list !';
$lang['notification_error_clock'] = 'Notification status can not be changed yet - no change requset !';
$lang['notification_saved'] = 'Notification Status Successfully Changed!';
$lang['profile_information_updated'] = 'Information Successfully Updated! Please login again to see new updates.';
$lang['password_updated'] = 'Password Successfully Updated! Please logout to use new password.';
$lang['view_personal_event'] = 'View Personal Event';
$lang['personal_event_saved'] = 'Save Event Successfully!';
$lang['personal_event_deleted'] = 'Delete Event Successfully!';
$lang['language_added_successfully'] = 'Language Information Successfully Added';
$lang['language_active_successfully'] = 'Language Information Successfully Active';
$lang['language_deactive_successfully'] = 'Language Information Successfully Deactive';
$lang['update_information'] = 'Information Successfully Update';
$lang['company_name'] = 'Company Name';
$lang['company_logo'] = 'Company Logo';
$lang['remove'] = 'Remove';
$lang['email_address'] = 'Email Address';
$lang['company_address'] = 'Company Address';
$lang['city'] = 'City';
$lang['country'] = 'Country';
$lang['select_country'] = 'Select Country';
$lang['phone'] = 'Phone';
$lang['hoteline'] = 'Hotline';
$lang['web_address'] = 'Website Address';
$lang['currency'] = 'Currency';
$lang['select_currency'] = 'Select Currency';
$lang['set_timezone'] = 'Set Timezone';
$lang['select_timezone'] = 'Select Timezone';
$lang['start_hours'] = 'Start Hours';
$lang['end_hours'] = 'End Hours';
$lang['all_categary'] = 'All Categories';
$lang['category_name'] = 'Category Name';
$lang['email'] = 'Email';
$lang['time_changes_request'] = 'Time Changes Request';
$lang['add_translation'] = 'Add Translation';
$lang['translations'] = 'Translations';
$lang['icon'] = 'Icon';
$lang['language'] = 'Language';
$lang['languages'] = 'Languages';
$lang['done'] = 'Done';
$lang['edit'] = 'Edit';
$lang['activate'] = 'Active';
$lang['deactivate'] = 'Deactive';
$lang['save_translation'] = 'Save Translation';
$lang['update_profile'] = 'Update Profile';
$lang['changes_password'] = 'Change Password';
$lang['old_password'] = 'Old Password ';
$lang['new_password'] = 'New Password ';
$lang['confirm_password'] = 'Confirm Password';

// user profile

$lang['you_have'] = 'You have';
$lang['messages'] = 'Messages';
$lang['view_all'] = 'View All';
$lang['application'] = 'Application';
$lang['request'] = 'Request';
$lang['username'] = 'User Name';
$lang['profile'] = 'Profile';
$lang['sign_out'] = 'Sign Out';
$lang['mark_completed'] = 'Mark Completed';
$lang['mark_incomplete'] = 'Mark incompleted';
$lang['move_up'] = 'Move up';
$lang['move_down'] = 'Move Down';
$lang['delete'] = 'Delete';
$lang['add'] = 'Add';
$lang['view'] = 'View';


// employee panel
$lang['leave_application_submitted'] = "Leave Application Successfully Submitted !";
$lang['fill_up_all_fields'] = "Please Fill up all required fields to apply ";
$lang['password_do_not_match'] = "Your Entered Password Do Not Match !";
$lang['home'] = "Home";
$lang['my_time'] = "My Time";
$lang['logout'] = "Logout";
$lang['my_expense'] = "My Expense";
$lang['leave'] = "Leave";
$lang['dob'] = "DOB";
$lang['address'] = "Address";
$lang['notice_board'] = "Notice Board";
$lang['clock_in'] = "Clock In";
$lang['clock_out'] = "Clock Out";
$lang['set_reason'] = "Set Reason";
$lang['clock_out_reason'] = "Clock Out Reason";
$lang['birthdays'] = "Birthdays";
$lang['recent_mails'] = "Recent Mails";
$lang['upcoming_events'] = "Upcoming Events";
$lang['award_date'] = "Award Date";
$lang['list_of_all_awrds'] = "List of All Awards Received";
$lang['received'] = "Received";
$lang['list_of_all_notice'] = "List of All Notices";
$lang['award_details'] = "Award Details";
$lang['edit_my_time_logs'] = "Edit My Time Logs";
$lang['reason_for_edit'] = "Reason for Edit";
$lang['request_update'] = "Request Update";
$lang['all_leave'] = "All Leave";
$lang['new_leave'] = "New Leave";
$lang['leave_application_you_applied'] = "Leave Applications You Applied";
$lang['submit'] = "Submit";
$lang['all_leave_details'] = "All Leave Details";
$lang['event_details'] = "Event Details";
$lang['list_of_all_event'] = "List of All Events";
$lang['amount_spent'] = "Amount Spent";
$lang['my_time_log'] = "My Time logs";
$lang['payment_history'] = "Payment History";
$lang['your_personal_profile'] = "Your Personal Profile";
$lang['print_payslip'] = "Print Payslip";
$lang['director'] = "Director";
$lang['accountant'] = "Accountant";
$lang['go_back'] = "Go Back";

//new
$lang['yes'] = "YES";
$lang['no'] = "NO";
$lang['employee_name'] = "Employee Name";
$lang['administrator'] = "Administrator";
$lang['administrator'] = "Administrator";
$lang['notified'] = "Notified";
$lang['js_confirm_message'] = "Do you really want to delete this record ?";
$lang['name_ar'] = "Name in Arabic";
$lang['name_en'] = "Name in English";
$lang['description_ar'] = "Description in Arabic";
$lang['description_en'] = "Description in English";
$lang['saved_successfully'] = "Data Saved Successfully";
$lang['deleted_successfully'] = "Data Deleted Successfully";

// Departements
$lang['departement'] = "Administration";
$lang['department_id'] = "Departement ID";
$lang['department_name_ar'] = "Departement Name in Arabic";
$lang['department_name_en'] = "Departement Name in English";
$lang['add_department_admin'] = "Administrator Name";
$lang['add_department_admin_rep'] = "Administrator Replacement Name";
$lang['rep'] = "The Replacement";

// Designations
$lang['designations_admin'] = "Section Admin";
$lang['select_departement'] = "Select From Departement";
$lang['designations_list'] = "Sections List";
$lang['add_designation'] = "Add Section";
$lang['designation_name'] = "Section Name";
$lang['designation_emp'] = "Employee";

// Job Titles
$lang['job_title'] = 'Job Title';
$lang['job_titles_list'] = "Job Titles List";
$lang['add_job_title'] = "Add a Job Title";
$lang['job_title_ar'] = 'Job Title (Arabic)';
$lang['job_title_en'] = 'Job Title (English)';
$lang['job_title_used'] = 'The Job Title is used !';

//employee
$lang['employee_list'] = "Employee List";
$lang['employee_page_header'] = "Employee Management";
$lang['view_employee'] = "View Employee";
$lang['employee_info_saved'] = "Employee Information Successfully Saved!";
$lang['employee_info_deleted'] = "Employee Information Successfully Deleted !";
$lang['add_employee'] = "Add Employee";
$lang['dept_desingation'] = "Dept > Designation";
$lang['personal_details'] = "Personal Details";
$lang['full_name_ar'] = "Full Name (Arabic)";
$lang['full_name_en'] = "Full Name (English)";
$lang['date_of_birth'] = "Date Of Birth";
$lang['gender'] = "Gender";
$lang['maratial_status'] = "Maratial Status";
$lang['male'] = "Male";
$lang['female'] = "Female";
$lang['married'] = "Married";
$lang['un-married'] = "Un-Married";
$lang['widowed'] = "Widowed";
$lang['divorced'] = "Divorced";
$lang['fathers_name'] = "Fathers Name";
$lang['nationality'] = "Nationality";
$lang['passport_no'] = "Passport NO";
$lang['passport_end'] = "End Passport Date";
$lang['identity_no'] = "Identity NO";
$lang['identity_end'] = "Identity End";
$lang['photo'] = "Photo";
$lang['contact_details'] = "Details of the contract";
$lang['present_address'] = "Present Address";
$lang['employee_document'] = "Employee Document";
$lang['resume'] = "Resume";
$lang['offer_letter'] = "Offer Letter";
$lang['joining_letter'] = "Joining Letter";
$lang['contract_paper'] = "Contract Paper";
$lang['id_proff'] = "ID Proff";
$lang['other_documents'] = "Other Documents";
$lang['bank_information'] = "Bank Information";
$lang['bank_name'] = "Bank Name";
$lang['branch_name'] = "Branch Name";
$lang['account_name'] = "Account Name";
$lang['account_number'] = "Account Number";
$lang['official_status'] = "Official Status";
$lang['inactive'] = "Inactive";
$lang['dependent'] = "Dependent";
$lang['wife_name'] = "Wife Name(English)";
$lang['wife_name_ar'] = "Wife Name(Arabic)";
$lang['wife_birth'] = "Wife Birthday";
$lang['fils_name'] = "Son name (English)";
$lang['fils_name_ar'] = "Son name (Arabic)";
$lang['fils_birth'] = "Son Birthday";
$lang['employee_type'] = "Employee Type";
$lang['select_type'] = "Choose a category";
$lang['job_time'] = "Job Time";
$lang['job_full'] = "Full Time";
$lang['job_part'] = "Part Time";
$lang['job_time_select'] = "Choose Job Time";
$lang['insurance'] = "Insurance Information";
$lang['medical_insur'] = "Medical Insurance";
$lang['medical_insur_yes'] = "YES";
$lang['medical_insur_no'] = "NO";
$lang['medical_insur_select'] = "Choose";
$lang['medical_insur_type'] = "Medical Insurance Type";
$lang['medical_insur_type_select'] = "Select Medical Insurance Type";
$lang['medical_insur_type_all'] = "Total amount";
$lang['medical_insur_type_part'] = "Part of the amount";
$lang['social_insur'] = "Social Insurance";
$lang['social_insur_yes'] = "YES";
$lang['social_insur_no'] = "NO";
$lang['social_insur_select'] = "Choose";
$lang['social_insur_type'] = "Social insurance type";
$lang['social_insur_type_select'] = "Select the type of social insurance";
$lang['saudi_social_insur'] = "Saudi deductible";
$lang['no_saudi_social_insur'] = "Saudi non-deductible";
$lang['no_social_insur'] = "Non-Saudi";
$lang['education'] = "Educational Qualification";
$lang['diplome'] = "Diploma";
$lang['job_place'] = "Job Place";
$lang['holiday_no'] = "Number of days of annual leave";
$lang['reference'] = "Reference";
$lang['employee_salary'] = "Salary";
$lang['view_finance_details'] = "Finance data";
$lang['direct_boss'] = "Direct manager";

// Leaves in settings
$lang['leave_category_quota'] = 'Leave Quota (%)';
$lang['leave_category_duration'] = 'Leave Duration (days)';
$lang['leave_category_exist'] = 'Leave Category Already Exist!';
$lang['leave_category_saved'] = 'Leave Category Successfully Saved!';
$lang['leave_category_used'] = 'Leave Category Information Already Used!';
$lang['leave_category_deleted'] = 'Leave Category Information Successfully Delete!';
$lang['leave_affect_stock'] = 'Affect Leave Balance';
$lang['leave_calculation_type'] = 'Leave Calculated By';
$lang['normal_year'] = 'Normal Year';
$lang['contractual_year'] = 'Contractual Year';
$lang['leave_tel'] = 'Set a Phone Number';
$lang['set_replacement'] = 'Set a Replacement Employee';
$lang['paid_leave'] = 'Paid Leave';
$lang['allowance_cat_ids'] = 'Allowances Included';

// Irrigularity_category
$lang['irrigularity_category'] = 'Irrigularity Category';
$lang['first'] = 'First Time';
$lang['second'] = 'Second Time';
$lang['third'] = 'Third Time';
$lang['fourth'] = 'Fourth Time';
$lang['first_ar'] = 'First Time (Arabic)';
$lang['second_ar'] = 'Second Time (Arabic)';
$lang['third_ar'] = 'Third Time (Arabic)';
$lang['fourth_ar'] = 'Fourth Time (Arabic)';
$lang['first_en'] = 'First Time (English)';
$lang['second_en'] = 'Second Time (English)';
$lang['third_en'] = 'Third Time (English)';
$lang['fourth_en'] = 'Fourth Time (English)';
$lang['penalty'] = 'Penalties';
$lang['irrigularity_category_used'] = 'Irrigularity Category Information Already Used!';
$lang['irrigularity_category_deleted'] = 'Irrigularity Category Information Successfully Delete!';
$lang['job_places'] = 'Job Places';


//Bonuses
$lang['bonuses_page_header'] = "Types of bonuses";
$lang['bonuses_list'] = "Creating financial information";
$lang['financial_information'] = "Creating financial information";
$lang['bonuses_type'] = "Types of bonuses";
$lang['bonuse_list'] = "Bonuses list";
$lang['add_bonuse'] = "Add Bonuse";
$lang['bonuse_title_ar'] = "Bonuse Title (Arabic)";
$lang['bonuse_title_en'] = "Bonuse Title (English)";
$lang['bonuse_assurance'] = "Assurance";
$lang['bonuse_guarantee'] = "Guarantee";
$lang['bonuse_departure'] = "Departure";
$lang['bonuse_yes'] = "YES";
$lang['bonuse_no'] = "NO";
$lang['brief_en'] = "English Brief";
$lang['brief_ar'] = "Arabic Brief";
$lang['select_bonus'] = "Choose";

//extra work
$lang['extra_page_header'] = "Extra Work";
$lang['extra_work'] = "Extra Work";
$lang['extra_list'] = "Extra work list";
$lang['add_extra'] = "Add Extra work";
$lang['extra_title_ar'] = "Extra work Title (Arabic)";
$lang['extra_title_en'] = "Extra work Title (English)";
$lang['work_value'] = "Work Value";
$lang['extra_title'] = "Extra Work title";


//Financial deduction
$lang['deduction_page_header'] = "Financial Deduction";
$lang['deduction_title'] = "Financial Deduction";
$lang['deduction_type'] = "Deduction Type";
$lang['deduction_list'] = "Financial Deduction list";
$lang['add_deduction'] = "Add Financial Deduction";
$lang['deduction_title_en'] = "Description(English)";
$lang['deduction_title_ar'] = "Description(Arabic)";
$lang['deduction_type'] = "Deduction type";
$lang['deduction_type_percent'] = "Percent";
$lang['deduction_type_value'] = "Value";
$lang['deduction_value'] = "Deduction value";

//Others provisions
$lang['provision_page_header'] = "Other provisions";
$lang['other_provisions'] = "Other provisions";
$lang['provision_title'] = "Other provisions";
$lang['provision_list'] = "Other provisions list";
$lang['add_provision'] = "Add Other provisions";
$lang['provision_title_en'] = "Description(English)";
$lang['provision_title_ar'] = "Description(Arabic)";

//Types of advances
$lang['advance_page_header'] = "Types of advances";
$lang['advances'] = "Types of advances";
$lang['advance_title'] = "Types of advances";
$lang['advance_list'] = "Types of advances List";
$lang['add_advance'] = "Add Type of advance";
$lang['advance_title_en'] = "Description(English)";
$lang['advance_title_ar'] = "Description(Arabic)";


//Financial allowance
$lang['allowance_page_header'] = "Allowance";
$lang['allowance_title'] = "Allowance";
$lang['allowances'] = "Allowance";
$lang['allowance_type'] = "Allowance";
$lang['allowance_list'] = "Allowance list";
$lang['add_allowance'] = "Add Allowance";
$lang['allowance_title_en'] = "Description(English)";
$lang['allowance_title_ar'] = "Description(Arabic)";
$lang['allowance_type'] = "Allowance type";
$lang['allowance_type_percent'] = "Percent";
$lang['allowance_type_value'] = "Value";
$lang['allowance_value'] = "Allowance value";

//Discount 
$lang['discount_page_header'] = "Discount";
$lang['discount_title'] = "Discount";
$lang['discounts'] = "Discount";
$lang['discount_type'] = "Allowance";
$lang['discount_list'] = "Allowance list";
$lang['add_discount'] = "Add Discount";
$lang['discount_title_en'] = "Description(English)";
$lang['discount_title_ar'] = "Description(Arabic)";
$lang['discount_type'] = "Discount type";
$lang['discount_type_percent'] = "Percent";
$lang['discount_type_value'] = "Value";
$lang['discount_value'] = "Discount value";

//insurance information
$lang['insurance_information'] = "Create different insurance";
//medical insurance
$lang['medical_page_header'] = "Medical insurance";
$lang['medical_insurance'] = "Medical insurance";
$lang['medical_title'] = "Medical insurance";
$lang['medical_brief'] = "Brief";
$lang['medical_list'] = "Medical insurance List";
$lang['add_medical'] = "Add Medical Insurance";
$lang['medical_title_en'] = "Description(English)";
$lang['medical_title_ar'] = "Description(Arabic)";
$lang['medical_brief_en'] = "Brief(English)";
$lang['medical_brief_ar'] = "Brief(Arabic)";

//Social insurance
$lang['social_page_header'] = "Social insurance";
$lang['social_insurance'] = "Social insurance";
$lang['social_title'] = "Social insurance";
$lang['social_brief'] = "Brief";
$lang['social_list'] = "Social insurance List";
$lang['add_social'] = "Add Social insurance";
$lang['social_title_en'] = "Description(English)";
$lang['social_title_ar'] = "Description(Arabic)";
$lang['social_brief_en'] = "Brief(English)";
$lang['social_brief_ar'] = "Brief(Arabic)";

// Social details
$lang['insurance_percent'] = "Percentage Of guarantee";
$lang['societe_percent'] = "Percentage Of enterprise";
$lang['nb_days'] = "Days Guarantee calculate";
$lang['age_male'] = "Lifetime Warranty for males";
$lang['age_female'] = "Lifetime Warranty for females";
$lang['salary_insurance'] = "Increase the salary of the Social Security automatically";
$lang['habit_insurance'] = "The inclusion of housing benefits when the account";
$lang['insurance_value'] = "Social security value percentage of of the number of days";
$lang['max_salary'] = "The upper limit of the salary of Social Security";
$lang['min_salary'] = "The minimum salary for Social Security";
$lang['social_insurance_yes'] = "YES";
$lang['social_insurance_no'] = "NO";
$lang['active'] = "Active";
$lang['description'] = "Description";


// Custody details
$lang['custody_title'] = "Custody";
$lang['custody_name_ar'] = "Name(Arabic)";
$lang['custody_name_en'] = "Name(English)";
$lang['custody_description_ar'] = "Description(Arabic)";
$lang['custody_description_en'] = "Description(English)";
$lang['custody_nombre'] = "Nombre";
$lang['custody_delivery_date'] = "Delivery Date";
$lang['custody_reference'] = "Reference";
$lang['custody_document'] = "Add document";

// employee category
$lang['all_categary_emp'] = "All Categories";
$lang['add_emp'] = "New Category";

// leave date
$lang['start_leave'] = "Your next leave starts in";
$lang['no_leave'] = "You do not have holidays at the moment";

//loan employee panel
$lang['all_loan'] = "All loan";
$lang['new_loan']="New loan";
$lang['loan_application'] = "Loan Application";
$lang['loan_application_you_applied'] = "Loan Applications you applied";
$lang['loan_category'] = "Loan Category";
$lang['loan_amount'] = "Loan Amount";
$lang['loan_month'] = "Number of Months";
$lang['loan_amount_month'] = "Part per Month";
$lang['loan_application_submitted'] = "Loan Application Successfully Submitted !";
//status
$lang['pending'] = "Pending";
$lang['approved'] = "Approved";
$lang['rejcted'] = "Rejected";

$lang['applications'] = "Applications";

// Cashing application
$lang['all_cashing'] = "All Cashing application";
$lang['new_cashing'] = "New Cashing application";
$lang['cashing_application'] = "Cashing application";
$lang['cashing_category'] = "Cashing category";
$lang['cashing_full_name'] = "Full Name";
$lang['cashing_phone'] = "Phone";
$lang['cashing_address'] = "Address";
$lang['cashing_bank_name'] = "Bank name";
$lang['cashing_branch_name'] = "Branch name";
$lang['cashing_rib'] = "Account Number";
$lang['cashing_amount'] = "Amount";
$lang['cashing_purpose'] = "Purpose";
$lang['cashing_purpose_ar'] = "Purpose(Arabic)";
$lang['cashing_purpose_en'] = "Purpose(English)";
$lang['cashing_description_ar'] = "Description(Arabic)";
$lang['cashing_description_en'] = "Description(English)";
$lang['cashing_application_submitted'] = "Cashing Application Successfully Submitted !";
$lang['cashing_invoiced'] = "Invoiced";
$lang['cashing_non_invoiced'] = "Non-invoiced";

//device
$lang['device'] = "SR";

//Permission application
$lang['all_permission'] = "All Permission application";
$lang['new_permission'] = "New Permission application";
$lang['permission_application'] = "Permission application";
$lang['permission_category'] = "Permission category";
$lang['permission_type_retard'] = "Delay for attendance";
$lang['permission_type_leaving'] = "Leaving early";
$lang['permission_type_exit'] = "Out of working time";
$lang['permission_date'] = "Date";

//Extra Hours application
$lang['all_extra_hours'] = "All Extra hours application";
$lang['new_extra_hours'] = "New Extra hours application";
$lang['extra_hours_application'] = "Extra hours application";

//Finance data
$lang['finance_employee'] = "Financial datas";
$lang['finance_bonus'] = "Allowance name";
$lang['finance_value'] = "Allowance value";
$lang['finance_from'] = "From";
$lang['finance_to'] = "To";
$lang['finance_select'] = "Select allowance type...";
$lang['finance_type'] = "Allowance type";
$lang['finance_add_allowance'] = "Add Allowance";
$lang['finance_basic_salary'] = "Basic salary";
$lang['finance_all_salary'] = "Total salary";
$lang['finance_reservation'] = "Reservation";

//Finance other provision data
$lang['finance_provision'] = "Privileges";
$lang['finance_provision_bonus'] = "Provision Bonus";
$lang['finance_provision_value'] = "Provision value";
$lang['finance_provision_description'] = "Description";
$lang['finance_provision_type'] = "Provision type";
$lang['finance_provision_description_ar'] = "Description(Arabic)";
$lang['finance_provision_description_en'] = "Description(English)";
$lang['finance_provision_select'] = "Select provision type...";

//Finance deduction data
$lang['finance_permanent_deduction'] = "Deductions";
$lang['finance_deduction'] = "Deductions";
$lang['finance_deduction_bonus'] = "Deduction name";
$lang['finance_deduction_value'] = "Deduction value";
$lang['finance_deduction_description'] = "Description";
$lang['finance_deduction_type'] = "Deduction type";
$lang['finance_deduction_description_ar'] = "Description(Arabic)";
$lang['finance_deduction_description_en'] = "Description(English)";
$lang['finance_deduction_select'] = "Select provision type...";


//Finance houcing
$lang['finance_houcing'] = "Housing benefits";
$lang['finance_status'] = "The employee is subject to benefits housing";
$lang['finance_date'] = "Starting Date";
$lang['finance_count'] = "The movement is calculated";
$lang['finance_value'] = "Allowance value";
$lang['finance_repetition'] = "Repeat the bonus of housing automatically every";
$lang['finance_fixed'] = "Fixed";
$lang['finance_percent'] = "As a percentage of salary";

//Financial Fieldset
$lang['fieldset_bank'] = "Payment method";
$lang['fieldset_med'] = "Medical insurance";
$lang['fieldset_social'] = "Social insurance";
$lang['fieldset_other'] = "Other";

//Financial payement
$lang['payment_method'] = "Payment method";
$lang['payment_account'] = "Acccount number";
$lang['payment_bank'] = "Bank name";
$lang['payment_branch'] = "Branch name";

//Financial medical
$lang['medical_yes'] = "Subject to Insurance";
$lang['medical_type'] = "Types of insurance";
$lang['medical_date'] = "Insurance start date";
$lang['medical_end_date'] = "Insurance card expiration date";

//Financial social
$lang['social_yes'] = "Subject to ensure";
$lang['social_type'] = "Social insurance Type";
$lang['social_date'] = "Start date of social insurance";
$lang['social_salary'] = "Salary guarantee";

// Financial Options
$lang['finance_option'] = "Financial Options";
$lang['option_end_service'] = "End of service benefits";
$lang['option_date'] = "Is dated";
$lang['option_fix'] = "Fixed";
$lang['option_suspend'] = "Suspended";
$lang['retrait_date'] = "Retirement date";
$lang['option_stop_salary'] = "Date of stopping salary";
$lang['option_accounting_1'] = "Figure accounting 1";
$lang['option_accounting_2'] = "Figure accounting 2";
$lang['option_description_ar'] = "Description(Arabic)";
$lang['option_description_en'] = "Description(English)";

//Automatic reminders
$lang['automatic_reminder'] = "Configure automatic reminders";
$lang['reminder_hrm_employee'] = "Remind human Resources responsible before";
$lang['reminder_dm_employee'] = "Remind the direct manager before";
$lang['reminder_language'] = "Reminder language";
$lang['reminder_language_select'] = "Select language";
$lang['reminder_number_select'] = "Select number";
$lang['reminder_test_period'] = "Days for the end of test period";
$lang['reminder_identity_end'] = "Days for the expiration of identity card";
$lang['reminder_passport_end'] = "Days for the expiration of passport";
$lang['reminder_med_insurance_end'] = "Days for the expiration of the insurance card";
$lang['reminder_contract_end'] = "Days for the end of the contract";
$lang['reminder_mail'] = "Write also a reminder to employees when  remind the human resources responsible about the ended of their documentation";
$lang['reminder_employee_fixed'] = "Transfer the employee automatically to fixed after the expiration of the test period";

$lang['general_items'] = "General Items";