<?php

// menu
$lang['Full'] = 'Full';
$lang['Part'] = 'Part';
$lang['employee_panel'] = 'Employee Panel';
$lang['view_profile'] = 'View Profile';
$lang['Un-Married'] = 'Un-Married';
$lang['Married'] = 'Married';
$lang['Widowed'] = 'Widowed';
$lang['Divorced'] = 'Divorced';
$lang['job_details'] = 'Job Details';
$lang['no-exist'] = 'Does not exist';
$lang['hr_manager'] = 'Human Resource Manager';
$lang['job_title_desc'] = 'Description of Job';
$lang['rial'] = 'Rials';
$lang['usefull_links'] = 'Usefull Links';

// administrative_affairs
$lang['administrative_affairs'] = 'Administrative Affairs';
$lang['leaves_types'] = "Leaves Types";
$lang['irrigularity_types'] = "Irrigularity Types";
$lang['center_documentations'] = "Center Documentations";

// contact_admin
$lang['contact_admin'] = "Contact The Administration";
$lang['box_sugg_compl'] = "Box for suggestions and complaints";
$lang['sugg_or_compl'] = "Suggestion or Complaint";
$lang['sugg'] = "Suggestion";
$lang['compl'] = "Complaint";
$lang['msg_body'] = "Message Body";
$lang['contact_admin_error'] = "Sorry ! can\'t send empty message... The field Message Body is required";
$lang['contact_admin_success'] = "Message sended";
$lang['direction_msg'] = "Send To";
$lang['msg_title'] = "Message Title";

//notices
$lang['notices'] = "Notifications";
$lang['notices_list'] = "Notifications List";
$lang['sender'] = "Sender";
$lang['notif_title'] = "Title";
$lang['notif_type'] = "Type";
$lang['anotice'] = "Notification";
$lang['readed'] = "Readed";
$lang['unreaded'] = "Un-Readed";

// send_application
$lang['app_sended'] = "Application was sended successfully";
$lang['send_applications'] = "Send Applications";
$lang['application_templ'] = "Application Forms";
$lang['applications_list'] = "Applications List";
$lang['your_approvals_cat'] = "List of Approvals associated with this account";
$lang['commission'] = "Commission";
$lang['all_leaves'] = "All Leaves";
$lang['all_advances'] = "All Advances";
$lang['allowed'] = "Allowed";
$lang['not_allowed'] = "Not Allowed";

// send_applications TabPanes
$lang['leaves'] = "Vacations";
$lang['leave_type'] = "Vacation Type";
$lang['address_in_leave'] = "Address during the vacation";
$lang['telephone'] = "Telephone";
$lang['leave_note'] = "You have to send the application 30 days before the vacation";
$lang['leave_max_duration'] = "Allowed days from the administration";
$lang['leave_duration'] = "Leave duration";
$lang['replacement'] = "Replacement employee";
$lang['tikets'] = "Tikets";
$lang['tikets_opt1'] = "Booking and Tikets";
$lang['tikets_opt2'] = "Getting Tikets prices";
$lang['tikets_opt3'] = "No Tikets";
$lang['booking_opts'] = "Booking options";
$lang['going_date'] = "Going date";
$lang['coming_date'] = "Coming date";
$lang['going_date_2'] = "Going date 2";
$lang['coming_date_2'] = "Coming dte 2";


// dependent
$lang['family'] = "Family";
$lang['dependent'] = "Depandants";
$lang['show_hide'] = "Show / Hide";
$lang['generate_auto'] = "Auto Generation";
$lang['employee_account'] = "Employee Account";
$lang['retirement_date'] = "End of contract / rerirement date";

//advances_app
$lang['advances_app'] = "Advances";
$lang['advance_type'] = "Advance type";
$lang['advance_value'] = "Value of Advance (in Rial)";
$lang['advance_note'] = "I pledge to accept the recovery of the loan at a rate of no more than 30% of the total monthly salary and not to the loan for more than 12 monthly premium. And that the resolution of the monthly salary of the first installment following the receipt of the loan begins.";
$lang['voucher'] = "The Voucher";
$lang['advance_note_2'] = "The voucher must promise to pay the premiums due on the above guaranteed in the event of default on any installment or leave work for any reason whatsoever.";

// caching
$lang['caching'] = "Caching";
$lang['cahing_value'] = "Value";

//filter_dues
$lang['allowances'] = "Allowances";
$lang['other_dues'] = "Other dues";
$lang['other_dues_placeholder'] = "Exemple: voyage tickets, payed vacation... etc";

//custody
$lang['custody'] = "Custody";
$lang['my_custodies'] = "My Custodies";
$lang['download'] = "Download";
$lang['new_custody'] = "Application for new Custody";

// transfer
$lang['transfer'] = "Transfer";
$lang['current_data'] = "Current Data";
$lang['current_job_title'] = "Current job title";
$lang['current_deaptment'] = "Current department";
$lang['current_designation'] = "Current section";
$lang['current_emp_category'] = "Current employee category";
$lang['current_job_time'] = "Current job time";
$lang['new_data'] = "New Data";
$lang['employee_cat'] = "Employee Class";

// training
$lang['la_trainings'] = "Training Courses";
$lang['course_name'] = "Course ame";
$lang['course_institute'] = "Course institute";
$lang['recrutement_task'] = "Recrutement task";
$lang['recrutement_type'] = "Recrutement type";
$lang['internal'] = "Internal";
$lang['external'] = "External";
$lang['course_date'] = "Suggested Date";
$lang['course_price'] = "Estimated Cost (in Rial)";
$lang['course_note'] = "Reason for applying for the course";

//extra_work
$lang['extra_work_type'] = "Type of extra work";

// la_purchase
$lang['prod_name'] = "Product";
$lang['prod_desc'] = "Description";
$lang['prod_num'] = "Quantity";
$lang['prod_note'] = "Notes";

// la_permission
$lang['permission_type'] = "Permission Type";
$lang['permission_opt1'] = "Parting early";
$lang['permission_opt2'] = "late in attendance";
$lang['permission_opt3'] = "Out and back during work time";
$lang['permission_time'] = "Permission time";
$lang['permission_star'] = "From";
$lang['permission_end'] = "To";

// resignation
$lang['resignation'] = "Resignation";

// list_applications
$lang['list_applications'] = "Sended Applications";
$lang['app_type'] = "Application";
$lang['date_app'] = "Date";
$lang['id_app'] = "ID";
$lang['view_application'] = "Application Details";
$lang['hijri'] = "Hijri";

// received_applications
$lang['received_applications'] = "Received Applications";

// mouvements
$lang['mouvements'] = "Mouvements";
$lang['required'] = "required";
$lang['non_required'] = "non required";
$lang['the_leave_tel'] = "Telphone";
$lang['leave_category_duration2'] = "Duration";
$lang['leave_category_quota2'] = "Discount";
$lang['leave_affect_stock2'] = "Affect Balance";

// evaluations_section
$lang['evaluations_section'] = "Evaluations Section";
$lang['all_evaluations'] = "Evaluations List";
$lang['put_an_evaluation'] = "Put an Evaluation for an employee";
$lang['continue'] = "Continue";
$lang['description_ev_item'] = "Description of Evaluation Item";
$lang['final_result'] = "Final Result";
$lang['cutodies'] = "Custodies";
$lang['percent'] = "Percentage from the basic salary";
$lang['value'] = "Fixed value";
$lang['finance_value2'] = "The value";
$lang['saudi1'] = "Saudi deductible";
$lang['saudi2'] = "Saudi non-deductible";
$lang['non-saudi'] = "Non-Saudi";
$lang['edit_medical'] = "Edit medical insurance";
$lang['social_salary'] = "Social salary";
$lang['medical_salary'] = "Medical salary";
$lang['insurance_percent'] = "Percentage Of employee";

// reports_section
$lang['image'] = "Photo";
$lang['reports_section'] = "Report Section";
$lang['report_type'] = "Report Type";
$lang['print'] = "print";
$lang['report_error'] = "You have not choose any employee from the list";
$lang['report_error_year'] = "You have not choose the year";
$lang['finance_details'] = "Financial Details";
$lang['evaluations'] = "Evaluations";
$lang['vacations'] = "Vacations";
$lang['results'] = "Results";
$lang['salary'] = "Salary";
$lang['social_insurance_type'] = "Social Insurance Type";
$lang['without_section'] = "Without Section";
$lang['the_report'] = "Report";

// app_handling
$lang['app_handling'] = "Application Handling";
$lang['appro_pass'] = "Pass";
$lang['employee_accepted'] = "Accept";
$lang['employee_refused'] = "Refuse";
$lang['current_employee'] = "Waiting";
$lang['sended_successfully'] = "Consultation request sended successfully";


$lang['super_manager'] = "General Manager";
$lang['dep_manager'] = "Department Manager";
$lang['sec_manager'] = "Section manager";


$lang['administration'] = "Administration";
$lang['employees'] = "Employees";
$lang['subject'] = "Subject";

// email
$lang['trash_sent_item'] = "Trash Sent Item";
$lang['trash_draft_item'] = "Trash Draft Item";
$lang['trash_inbox_item'] = "Trash inbox Item";
$lang['please_select_message'] = "Please Select a message";

//
$lang['clock_in_message'] = "Your Clock In is saved :";
$lang['clock_out_message'] = "Your Clock Out is saved :";
$lang['clock_in_message2'] = "Your Extra hours In is saved :";
$lang['clock_out_message2'] = "Your Extra hours Out is saved :";
$lang['clock_error_message'] = "An error has occured in the proccess. Please try again";
$lang['attendances'] = "Attendances";

//
$lang['vacation_balances'] = "Vacation Balances";
$lang['old_balance'] = "Last Year Balance";
$lang['new_balance'] = "Current Year Balance";
$lang['the_rest'] = "The Rest";
$lang['reset'] = "Reset";

// Calculating salaries and attendance
$lang['calculating_salaries_attendance'] = "Calculating salaries and attendance";
$lang['calculating_attendance'] = "Calculating Attendance";
$lang['calculating_salary'] = "Calculating Salary";

$lang['attendance_hours'] = "Attendance hours";
$lang['presence'] = "Presence";
$lang['total_presence_in month'] = "Total Presence Hours In This Month";
$lang['num_days_off'] = "Number Of  Weekends and Aîds";
$lang['job_time_hours'] = "Job hours";
$lang['calculate_abscence_hours'] = "Calculate Abscence Hours In This Month";
$lang['all_hours_available_in_month'] = "All Hours Available In This Month";
$lang['total_abscence_in_month'] = "Total Abscence Hours In This Month";
$lang['non_paid_leaves'] = "Non paid leaves";
$lang['discounts'] = "Discounts";
$lang['extras'] = "Extras";

// Fixactions
$lang['name_exist'] = "This name already exist in database... choose another name";
$lang['la_job_application'] = "Job Application";
$lang['la_embarkation'] = "Embarkation";
$lang['duration'] = "Duration";
$lang['from_institute'] = "Recrutement Address";
$lang['coming_date_after_vacation'] = "Date of starting after vacation";
$lang['embarkation_note1'] = "I admit that I took all my annual vacation days which: I do not have any entitled vacation untill :";
$lang['embarkation_note2'] = "I still have a only";
$lang['embarkation_note3'] = "Number of discounted days of salary (the days of deserved leave)";
$lang['notes'] = "Notes";
$lang['recrutements'] = "Recrutements";
$lang['presence_status'] = "Presence";
$lang['in_leave'] = "In Vacation";
$lang['present'] = "Present";
$lang['employee_id2'] = "UID";
$lang['datetime'] = "Date & Time";


$lang['start_contractual_year'] = "Start date of the contractual year";
$lang['days_of_work_this_yeas'] = "Working days in this contractual year";
$lang['yearly_holiday_no'] = "Allowed days for annual leave";
$lang['notes_emp'] = "Employee notes";
$lang['app_title'] = "Application title";
$lang['payement_method'] = "Payement method";
$lang['monthly_installement'] = "Monthly installement";
$lang['payement_method1'] = "In one year";
$lang['payement_method2'] = "Across the contract duration";
$lang['payement_method3'] = "By Specefic amount";
$lang['no_contract_duration'] = "No fixed duration for the contract";
$lang['payement_months'] = "number of monthly installments";
$lang['months'] = "month(s)";
$lang['the_advance_type'] = "Advance type";
$lang['advance_update'] = "Advance update";
$lang['rest_advance'] = "The rest";
$lang['last_advance_date'] = "Last payment date";
$lang['proccess_advance'] = "Paying";
$lang['edit_employee'] = "Edit employee infos";


/**************************************************/
$lang['report_all'] = "َAll employees";
$lang['report_by_employee'] = "Choose employees";
$lang['vacations_detailed'] = "Vacations (detailed report)";
$lang['in_work'] = "In Work";
$lang['custody_details'] = "Custody Details";
$lang['finance_employee_page'] = "Finance Details Page";
$lang['employee_signature'] = "Employee Signature";
$lang['manager_signature'] = "Manager Signature";
$lang['cin_photo'] = "ID Card Photo";
$lang['passport_photo'] = "passport Photo";
$lang['calculate_salary_attendance1'] = "Calculate salary and attendance (each employee individually)";
$lang['calculate_salary_attendance2'] = "Calculate attendance for all employees";

/**************************************************/
$lang['extra_hours'] = "Extra Hours";
$lang['num_hours'] = "extra hours number";
$lang['according_to_job_titles'] = "According to Job Titles";
$lang['according_to_departments'] = "According to Departments";
$lang['course_institute_name'] = "Course Institute";
$lang['maintenance_title'] = "Maintenance Title";

$lang['start_pr'] = "Start permission";
$lang['end_pr'] = "End permission";

$lang['total_extra_hors'] = 'Total of extra hours';
$lang['total_attendant_hours'] = 'Total present hours';
$lang['total_permissions'] = 'Total permissions hours';
$lang['with_permissions'] = 'with permissions';
$lang['without_permissions'] = 'without permissions';

$lang['value_per_hour'] = "Value per hour";
$lang['value_extra_hour'] = "Value of one extra hour";
$lang['value_extra_hours'] = "Value of all extra hours";
$lang['value_abscences'] = "value of abscences";

$lang['missions_list']="Missions List :";
$lang['add_mission']="Add Mission";
$lang['edit_ev']="Edit evaluation item";

$lang['bill_number']="Bill Number";
$lang['item_no']="Item No";
$lang['account_holder_name']="Account Holder Name";
$lang['country']="Country";
$lang['swift_code']="Swift Code";
$lang['city']="City";
$lang['additional_bank_information']="Additional bank information";

$lang['abscence_days']="Abscence Hours";
$lang['merits']="Merits";
$lang['total_merits']="All Merits";
$lang['net_merits']="Net Due";
$lang['deductions']="Deductions";
$lang['total_deductions']="All Deductions";
$lang['abscence']="Abscence";

$lang['import_attendance']="Import attendance data";
$lang['upload']="Upload";
$lang['upload_condition']="The file must only have (.xlsx) extension !";
$lang['total_extra_hours']="Total extra hours";

$lang['accounting']="Accounting";
$lang['send_accounting']="Send Accounting";
$lang['accounting_archive']="Accounting Archive";
$lang['accounting_title']="Accounting Title";
$lang['accounting_text']="Accounting Text";
$lang['accounting_from']="Accounting from ";
$lang['accounting_long_description_1']="you have received an accounting. Please follow the link below to see the accounting detail :";
$lang['accounting_long_description_2'] = "Accounting Link";
$lang['employee_reply'] = "Employee response";
$lang['final_decision'] = "Final Decision";
$lang['accounting_long_description_3'] = "employee response on an accounting";
$lang['accounting_long_description_4'] = "the employee has respond on your accounting : ";
$lang['acounting_deleted'] = "This accounting has been deleted !";
$lang['manual_adjustment'] = "Manual adjustment";

$lang['house_allowance'] = "House Allowance";
$lang['transportation_allowance'] = "Transportation Allowance";

$lang['application'] = " application";
$lang['note_accept'] = 'Acceptance Note';
$lang['note_refuse'] = 'Rejection Note';
$lang['note_delegate'] = 'Delegate Note';
$lang['note_consultation'] = 'Consultation Note';

$lang['sms_config'] = "SMS settings";
$lang['sender_name'] = "Sender Name";
$lang['sms_login'] = "Username";
$lang['sms_password'] = "Password";
$lang['sms_config_note1'] = "For receiving short messages when you send requests to the Manager, you must create an account (username and password) at the site:";
$lang['sms_config_note2'] = "The name must be in Latin characters only";
$lang['sms_test'] = "Sms Test";
$lang['sms_default'] = "Default Account";

$lang['counted'] = "added in Attendance $ Salary";
