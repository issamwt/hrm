<?php

// job_places
$lang['job_places'] = 'Job Places';
$lang['add_job_places'] = 'Add job Place';
$lang['update_job_places'] = 'Update a Job Place';
$lang['address_ar'] = 'Address (in Arabic)';
$lang['address_en'] = 'Address (in English)';
$lang['lat'] = 'Latitude';
$lang['lng'] = 'Longitude';
$lang['zoom'] = 'Zoom';
$lang['map'] = 'The Map';
$lang['job_place_used'] = 'Job Place informations are used !';
$lang['employee_category_used'] = 'Employee Category informations are used !';

// Evaluation items
$lang['all_items'] = 'All Items';
$lang['new_item'] = 'New Item';
$lang['name_ev_item'] = 'Evaluation Item';
$lang['ev_type'] = 'Item Type';
$lang['percentage'] = 'Percentage';
$lang['rating'] = 'Rating';
$lang['fromto'] = 'From 0%.........................To 100%';

//organizational_chart
$lang['organizational_chart'] = 'Organizational Chart';
$lang['acompany'] = 'The Company';
$lang['abranche'] = 'Branche';
$lang['adeprtment'] = 'Administration';
$lang['adseignation'] = 'Section';
$lang['aunit'] = 'Unit';
$lang['units'] = 'Units';
$lang['adivision'] = 'Division';
$lang['divisions'] = 'Divisions';
$lang['add_unit'] = 'Add Unit';
$lang['add_division'] = 'Add Division';
$lang['alert'] = 'Please Fill All The Fields !';
$lang['alert_error'] = 'An error has occured in the database connection. Please reload the page and try again.';

// users_permissions_system
$lang['users_permissions_system'] = 'Users & Permissions System';
$lang['users_list'] = 'Users List';
$lang['add_user'] = 'Add User';
$lang['edit_user'] = 'Edit User';
$lang['user_type'] = 'User Type';
$lang['choose_user_type'] = 'Choose User Type';
$lang['user_note'] = 'Notes';
$lang['super_admin'] = 'Super Admin';
$lang['hr_admin'] = 'Human Resources Responsable';
$lang['compt_admin'] = 'Accountant';
$lang['guest_admin'] = 'Guest Account';
$lang['user_permissions'] = 'User Permissions';
$lang['options'] = 'Options';
$lang['user_data'] = 'User Data';
$lang['password'] = 'Password';
$lang['user_department'] = 'department';
$lang['user_designation'] = 'Section';
$lang['user_option1'] = 'The admin have the possibility to send application for an employee';
$lang['user_updated'] = 'User Login Information Update Successfully!';
$lang['user_created'] = 'New User Create Successfully!';

// Organizational Structure
$lang['organizational_structure'] = 'Organizational Structure';
$lang['overview'] = 'Overview';
$lang['branches'] = 'Branches';
$lang['branche'] = 'Branche';
$lang['branches_list'] = 'Branches List';
$lang['add_branche'] = 'Add Branche';
$lang['select_branche'] = 'Select From Branche';
$lang['units_list'] = 'Units List';
$lang['unit_name'] = 'Unit';
$lang['select_designation'] = 'Select From Sections';
$lang['divisions_list'] = 'Divisions List';
$lang['division_name'] = 'Division';
$lang['select_unit'] = 'Select From Units';

// Approvals
$lang['designation'] = 'Section';
$lang['new_approval_chain'] = 'New Approval Chain';
$lang['approvals'] = 'Approvals';
$lang['approvals_list'] = 'Approvals Chains List';
$lang['add_approval'] = 'Add Approval Chain';
$lang['edit_approval'] = 'Edit Approval Chain';
$lang['link_approvals'] = 'Link Approvals Chain';
$lang['follow_up_approvals'] = 'Follow Up Approvals';
$lang['configure_comm'] = 'Configure commision By';
$lang['configure_scope'] = 'Configure Scope By';
$lang['opt_employee'] = 'Employee';
$lang['opt_structure'] = 'Structure';
$lang['config'] = 'Config';
$lang['follow_up'] = 'FollowUp';
$lang['configure_approval'] = 'Configure Approval';
$lang['approvals_data'] = 'Approval Chain Data';
$lang['approvals_scope'] = 'Approval Chain Scope';
$lang['approvals_comm_adm'] = 'Determine approvals Commission  from managers';
$lang['approvals_comm_emp'] = 'Determine approvals Commission from employees';
$lang['approvals_comm_squence'] = 'Determine the sequence of the Commission';
$lang['all'] = 'All';
$lang['approval_notify_emp'] = 'Notify the employee with the final result of The Approval';
$lang['admin_must_participate'] = 'Direct Manager must participate in Approval';
$lang['can_add_replacement'] = 'Can Add Replacement';
$lang['can_consult'] = 'The possibility of consulting on demand';
$lang['choose_admin'] = 'Choose Admins';
$lang['responsability'] = 'Responsability';
$lang['manager'] = 'Manager of';
$lang['the_list'] = 'The List';
$lang['choose_employees'] = 'Choose Employees';
$lang['direct_manager'] = 'Direct Manager';
$lang['order'] = 'Order';
$lang['comm_members'] = 'Commission Members';
$lang['member_type'] = 'Member Type';
$lang['appro_accept'] = 'Acceptance';
$lang['appro_refuse'] = 'Rejection';
$lang['appro_view'] = 'Viewing';
$lang['appro_delegate'] = 'Delegate';
$lang['appro_consultation'] = 'Consultation';
$lang['approvals_comm_squence_note'] = "If you've added or removed members to the committee, please save the page in order to show the NEW list of members";
$lang['follow_up1'] = 'Follow up the application Automatically by the System';
$lang['follow_up2'] = 'In';
$lang['follow_up3'] = 'munites from receiving the manager in question the application, the sysytem will send a reminder email';
$lang['follow_up4'] = 'time (s)';
$lang['follow_up5'] = 'and when the delay of reminder is out the system will :';
$lang['follow_up6'] = 'Delegate the replacement of manager in case he exsit, other wise the application stay with the manager';
$lang['follow_up7'] = 'send the application directly th the next manager to commission of approvals';
$lang['exit'] = 'Exit';

// Link Approvals
$lang['link_approval_list'] = 'List of Possible Applications';
$lang['link_approval_expression'] = 'Approvals linked to the application';
$lang['la_advance'] = 'Advance';
$lang['la_vacations'] = 'Vacations';
$lang['la_caching'] = 'Caching';
$lang['la_filter_dues'] = 'Filter Dues';
$lang['la_custody'] = 'Custody';
$lang['la_resignation'] = 'Resignation';
$lang['la_transfer'] = 'Transfer to other Department';
$lang['la_training'] = 'Training course';
$lang['la_overtime'] = 'Overtime';
$lang['la_purchase'] = 'Purchase';
$lang['la_maintenance'] = 'Maintenance';
$lang['la_annual_assessment'] = 'Other';
$lang['app_name'] = 'Application name';
$lang['la_permission'] = 'Permission';
$lang['la_def_salary'] = 'Definition of Salary';
$lang['la_recrutement'] = 'Recrutement';
$lang['configure_approvals'] = 'Approvals List';
$lang['help'] = 'Help';
$lang['help1'] = 'Choose the type(s) of Approvals(s) from the list in the Left';
$lang['help2'] = 'Add the approval(s) to the list in the Rigth';
$lang['help3'] = 'Choose the Apllication(s) That you want to Link from the list in the Left';
$lang['help4'] = 'Add the Application to the list in the Right';
$lang['help5'] = 'Save';

// New
$lang['ideal_employee'] = 'The ideal employee for this month';

// last
$lang['total_branches'] = 'Total Branches';
$lang['total_sections'] = 'Total Sections';
$lang['last_application'] = 'Last applications';
$lang['all_documents'] = 'All documents';
$lang['add_document'] = 'New document';
$lang['link'] = 'Link';
$lang['link_note'] = 'Filename must be in english';
$lang['get_dep_admins'] = "Departments Managers";
$lang['get_des_admins'] = "Sections Managers";
$lang['waiting_apps'] = "waiting Applications";
$lang['received_apps_archive'] = "Received Applications Archive";
$lang['skip'] = "Skip";
$lang['cahing_type'] = "Cahing type";
$lang['cahing_type0'] = "Invoiced";
$lang['cahing_type1'] = "Not Invoiced";
$lang['beneficiary_name'] = "Beneficiary Name";
$lang['beneficiary_address'] = "Bank Address";
$lang['caching_reason'] = "Reason";
$lang['end_contractual_year'] = "date of the end of the contractual year";
$lang['old_rest'] = "The Rest from last year";
$lang['modify_app'] = "Modify Application";
$lang['received'] = "Received";
$lang['receipt_confirmation'] = "Receipt Confirmation";
$lang['receipt_confirmed_successfully'] = "Receipt Confirmed Successfully";





