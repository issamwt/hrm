<?php

$lang['date_year'] = "سنة";

$lang['date_years'] = "سنوات";

$lang['date_month'] = "شهر";

$lang['date_months'] = "أشهر";

$lang['date_week'] = "أسبوع";

$lang['date_weeks'] = "أسابيع";

$lang['date_day'] = "يوم";

$lang['date_days'] = "أيام";

$lang['date_hour'] = "ساعة";

$lang['date_hours'] = "ساعات";

$lang['date_minute'] = "دقيقة";

$lang['date_minutes'] = "دقائق";

$lang['date_second'] = "ثانية";

$lang['date_seconds'] = "ثواني";

$lang['UM12'] = '(UTC -12:00) جزر هزلاند';

$lang['UM11'] = '(UTC -11:00) صاموا, نيوو';

$lang['UM10'] = '(UTC -10:00) هواواي, تاهيتي';

$lang['UM95'] = '(UTC -9:30) جزر ماركيساس';

$lang['UM9'] = '(UTC -9:00) ألاسكا';

$lang['UM8'] = '(UTC -8:00) المحيط الهادئ';

$lang['UM7'] = '(UTC -7:00) جبل طارق';

$lang['UM6'] = '(UTC -6:00) التوقيت المتوسط';

$lang['UM5'] = '(UTC -5:00) شرق الكاريبي';

$lang['UM45'] = '(UTC -4:30) فنزويلا';

$lang['UM4'] = '(UTC -4:00) غرب الكاريبي';

$lang['UM35'] = '(UTC -3:30) نيوفاوندلاند';

$lang['UM3'] = '(UTC -3:00) الأرجنتين, البرازيل, الأوروغواي';

$lang['UM2'] = '(UTC -2:00) جنوب جورجيا';

$lang['UM1'] = '(UTC -1:00) الرأس الأخضر';

$lang['UTC'] = '(UTC) غرينيتش';

$lang['UP1'] = '(UTC +1:00) منتصف أوروبا';

$lang['UP2'] = '(UTC +2:00) منتصف أفريقيا, شرق أوروبا';

$lang['UP3'] = '(UTC +3:00) موسكو';

$lang['UP35'] = '(UTC +3:30) إيران';

$lang['UP4'] = '(UTC +4:00) أذربيدجان';

$lang['UP45'] = '(UTC +4:30) أفغانستان';

$lang['UP5'] = '(UTC +5:00) باكستان';

$lang['UP55'] = '(UTC +5:30) الهند';

$lang['UP575'] = '(UTC +5:45) النيبال';

$lang['UP6'] = '(UTC +6:00) بنغلاديش';

$lang['UP65'] = '(UTC +6:30) ميهمار و جزر كوكوس';

$lang['UP7'] = '(UTC +7:00) كمبوديا, لاووس, تايلاند, فيتنام';

$lang['UP8'] = '(UTC +8:00) غرب أستراليا';

$lang['UP875'] = '(UTC +8:45) وسط غرب أستراليا';

$lang['UP9'] = '(UTC +9:00) اليابان, كوريا';

$lang['UP95'] = '(UTC +9:30) وسط أستراليا';

$lang['UP10'] = '(UTC +10:00) شرق أستراليا';

$lang['UP105'] = '(UTC +10:30) جزيرة لورد هاوي';

$lang['UP11'] = '(UTC +11:00) فانواتو, ماغادان';

$lang['UP115'] = '(UTC +11:30) جزيرة نورفولك';

$lang['UP12'] = '(UTC +12:00) نيوزيلندا';

$lang['UP1275'] = '(UTC +12:45) جزر شاذام';

$lang['UP13'] = '(UTC +13:00) جزر فونيكس';

$lang['UP14'] = '(UTC +14:00) جزر لاين';

/* End of file date_lang.php */

/* Location: ./system/language/english/date_lang.php */