<?php

// Employee
$lang['Full'] = 'كامل';
$lang['Part'] = 'جزئي';
$lang['employee_panel'] = 'لوحة تحكم الموظف';
$lang['view_profile'] = 'الملف الشخصي';
$lang['Un-Married'] = 'غير متزوج';
$lang['Married'] = 'متزوج';
$lang['Widowed'] = 'أرمل';
$lang['Divorced'] = 'مطلق';
$lang['country'] = 'الدولة';
$lang['job_details'] = 'تفاصيل الوظيفة';
$lang['department'] = 'الإدارة';
$lang['no-exist'] = 'لا يوجد';
$lang['hr_manager'] = 'مسؤول الموارد البشرية';
$lang['job_title_desc'] = 'وصف الوظيفة';
$lang['rial'] = 'ريال';
$lang['usefull_links'] = 'روابط مهمة';

// administrative_affairs
$lang['administrative_affairs'] = 'الشؤون الإدارية';
$lang['leaves_types'] = "أنواع الإجازات";
$lang['irrigularity_types'] = "أنواع المخالفات";
$lang['center_documentations'] = "وثائق المركز";

// contact_admin
$lang['contact_admin'] = "تواصل مع الإدارة";
$lang['box_sugg_compl'] = "خانة المقترحات و الشكاوى";
$lang['sugg_or_compl'] = "مقترح أم شكوى";
$lang['sugg'] = "مقترح";
$lang['compl'] = "شكوى";
$lang['compl'] = "شكوى";
$lang['msg_body'] = "نص الرسالة";
$lang['contact_admin_error'] = "عفوا لا يمكن إرسال رسالة فارغة... حقل نص الرسالة مطلوب";
$lang['contact_admin_success'] = "تم الإرسال";
$lang['direction_msg'] = "أرسل إلى";
$lang['msg_title'] = "عنوان الرسالة";

//notices
$lang['notices'] = "الإشعارات";
$lang['notices_list'] = "قائمة الإشعارات";
$lang['date'] = "التاريخ";
$lang['sender'] = "المرسل";
$lang['notif_title'] = "العنوان";
$lang['notif_type'] = "نوع الإشعار";
$lang['anotice'] = "إشعار";
$lang['readed'] = "مقروء";
$lang['unreaded'] = "غير مقروء";
$lang['notice_details'] = "تفاصيل الإشعار";

// send_application
$lang['app_sended'] = "تم إرسال الطلب";
$lang['reason'] = "السبب";
$lang['send_applications'] = "إرسال الطلبات";
$lang['application_templ'] = "نماذج إرسال الطلبات";
$lang['applications_list'] = "قائمة الطلبات";
$lang['your_approvals_cat'] = "قائمة سلسلة الموافقات الخاصة بهذا الحساب";
$lang['commission'] = "اللجنة";
$lang['applications'] = "الطلبات";
$lang['all_leaves'] = "كل الإجازات";
$lang['all_advances'] = "كل السلف";
$lang['allowed'] = "مسموح به";
$lang['not_allowed'] = "غير مسموح به";

// send_applications TabPanes
$lang['leaves'] = "الإجازات";
$lang['leave_type'] = "نوع الإجازة";
$lang['address_in_leave'] = "عنوان الموظف أثناء الإجازة";
$lang['telephone'] = "الهاتف";
$lang['leave_note'] = "يجب إرسال الطلب قبل موعد الإجازة ب30 يوما";
$lang['leave_max_duration'] = "الأيام المحددة من قبل الإدارة";
$lang['leave_duration'] = "مدة الإجازة";
$lang['replacement'] = "الموظف البديل";
$lang['tikets'] = "التذاكر";
$lang['tikets_opt1'] = "الحجز وإصدار التذاكر";
$lang['tikets_opt2'] = "استلام قيمة التذاكر";
$lang['tikets_opt3'] = "لا يوجد تذاكر";
$lang['booking_opts'] = "خيارات الحجز";
$lang['going_date'] = "تاريخ الذهاب";
$lang['coming_date'] = "تاريخ العودة";
$lang['going_date_2'] = "تاريخ الذهاب 2";
$lang['coming_date_2'] = "تاريخ العودة 2";

// dependent
$lang['family'] = "العائلة";
$lang['dependent'] = "التابعين";
$lang['show_hide'] = "إظهار/ إخفاء ";
$lang['generate_auto'] = "توليد تلقائي";
$lang['employee_account'] = "معلومات الحساب";
$lang['retirement_date'] = "تاريخ إنتهاء العقد / التقاعد";



//advances_app
$lang['advances_app'] = "السلف";
$lang['advance_type'] = "أنواع السلف";
$lang['advance_value'] = "قيمة القرض";
$lang['advance_note'] = "أتعهد بقبول استرداد القرض بمعدل لا يزيد عن 30% من مجمل راتبي الشهري وعلى أن لا تزيد مدة القرض على (12) قسط شهري  . وأن يبدأ حسم الأقساط الشهرية من أول راتب يلي استلام القرض .";
$lang['voucher'] = "الكفيل";
$lang['advance_note_2'] = "الكفيل يجب أن يتعهد بسداد الاقساط المستحقة على المكفول أعلاه في حال تخلفه عن سداد أي قسط او تركه العمل لأي سبب كان .";

// caching
$lang['caching'] = "الصرف";
$lang['cahing_value'] = "المبلغ";

//filter_dues
$lang['allowances'] = "بدلات";
$lang['other_dues'] = "مستحقات أخرى";
$lang['other_dues_placeholder'] = "مثلا تذكرة سفر, إجازة مدفوعة... إلخ";

//custody
$lang['custody'] = "عهدة";
$lang['my_custodies'] = "العهد السابقة التي إستلمتها";
$lang['download'] = "تحميل";
$lang['new_custody'] = "طلب عهدة جديدة";

// transfer
$lang['transfer'] = "نقل";
$lang['current_data'] = "البيانات الحالية";
$lang['current_job_title'] = "المسمى الوظيفي الحالي";
$lang['current_job_title'] = "المسمى الوظيفي الحالي";
$lang['current_deaptment'] = "الإدارة الحالية";
$lang['current_designation'] = "القسم الحالي";
$lang['current_emp_category'] = "صنف الموظف الحالي";
$lang['current_job_time'] = "نوع الدوام الحالي";
$lang['new_data'] = "البيانات الجديدة";
$lang['employee_cat'] = "الصنف";

// training
$lang['la_trainings'] = "دورات تدريبية";
$lang['course_name'] = "إسم الدورة";
$lang['course_institute'] ="الجهة الطالبة";
$lang['recrutement_task'] = "مهمة الإنتداب";
$lang['recrutement_type'] = "نوع الإنتداب";
$lang['internal'] = "داخلي";
$lang['external'] = "خارجي";
$lang['course_date'] = "الموعد المقترح";
$lang['course_price'] = "التكلفة التقديرية (بالريال)";
$lang['course_note'] = "سبب طلب الدورة";

// extra_work
$lang['extra_work_type'] = "نوع الساعات الإضافية";

// la_purchase
$lang['prod_name'] = "الصنف";
$lang['prod_desc'] = "وصف الصنف";
$lang['prod_num'] = "الكمية";
$lang['prod_note'] = "ملاحظات";

// la_permission
$lang['permission_type'] = "نوع الإستئذان";
$lang['permission_opt1'] = "إنصراف مبكر";
$lang['permission_opt2'] = "تأخر في الحضور";
$lang['permission_opt3'] = "الخروج و العودة أثناء الدوام";
$lang['permission_time'] = "وقت الإستئذان";
$lang['permission_star'] = "من الساعة";
$lang['permission_end'] = "إلى الساعة";

// resignation
$lang['resignation'] = "إستقالة";

// list_applications
$lang['list_applications'] = "الطلبات المرسلة";
$lang['app_type'] = "نوع الحركة";
$lang['date_app'] = "تاريخ الطلب";
$lang['id_app'] = "معرف";
$lang['view_application'] = "تفاصيل الطلب";
$lang['hijri'] = "هجري";

// received_applications
$lang['received_applications'] = "الطلبات الواردة";

// mouvements
$lang['mouvements'] = "حركات";
$lang['required'] = "مطلوب";
$lang['non_required'] = "غير مطلوب";
$lang['the_leave_tel'] = "رقم الهاتف";
$lang['leave_category_duration2'] = "المدة";
$lang['leave_category_quota2'] = "نسبة الخصم";
$lang['leave_affect_stock2'] = "تأثر على الرصيد";

// evaluations_section
$lang['evaluations_section'] = "قسم التقييمات";
$lang['all_evaluations'] = "قائمة التقييمات";
$lang['put_an_evaluation'] = "ضع تقييم لموظف";
$lang['continue'] = "مواصلة";
$lang['description_ev_item'] = "صفة بند التقييم";
$lang['final_result'] = "النتيجة النهائية";
$lang['cutodies'] = "عهدات";
$lang['percent'] = "نسبة من الراتب الأساسي";
$lang['value'] = "قيمة ثابتة";
$lang['finance_value2'] = "قيمة العلاوة";
$lang['saudi1'] = "سعودي قابل للخصم";
$lang['saudi2'] = "سعودي غير قابل للخصم";
$lang['non-saudi'] = "غير سعودي";
$lang['edit_medical'] = "تعديل التأمين الطبي";
$lang['social_salary'] = "راتب الضمان";
$lang['medical_salary'] = "راتب التأمين";
$lang['insurance_percent'] = "نسبة الموظف";

// reports_section
$lang['image'] = "الصورة";
$lang['reports_section'] = "قسم التقارير";
$lang['report_type'] = "نوع التقرير";
$lang['print'] = "طباعة";
$lang['report_error'] = "لم تختر أي موظف من القائمة";
$lang['report_error_year'] = "لم تختر السنة";
$lang['finance_details'] = "بيانات مالية";
$lang['evaluations'] = "تقييمات";
$lang['vacations'] = "إجازات";
$lang['results'] = "النتائج";
$lang['salary'] = "الراتب";
$lang['social_insurance_type'] = "نوع التأمينات الاجتماعية";
$lang['without_section'] = "لا يوجد قسم";
$lang['the_report'] = "التقرير";
$lang['month'] = "الشهر";
$lang['year'] = "السنة";

// app_handling
$lang['app_handling'] = "معالجة الطلب";
$lang['appro_pass'] = "تمرير";
$lang['employee_accepted'] = "موافقة";
$lang['employee_refused'] = "رفض";
$lang['current_employee'] = "قيد المعالجة";
$lang['sended_successfully'] = "أرسل طلب الإستشارة بنجاح";



$lang['administration'] = "الإدارة";
$lang['employees'] = "الموظفين";
$lang['subject'] = "الموضوع";

// email
$lang['trash_sent_item'] = "رسائل مرسلة محذوفة";
$lang['trash_draft_item'] = "رسائل مسودة محذوفة";
$lang['trash_inbox_item'] = "رسائل واردة محذوفة";
$lang['please_select_message'] = "الرجاء حدد الرسالة";

//
$lang['clock_in_message'] = "تم تسجيل دخولك للدوام :";
$lang['clock_out_message'] = "تم تسجيل خروجك من الدوام :";
$lang['clock_in_message2'] = "تم تسجيل بداية الساعات الإضافية :";
$lang['clock_out_message2'] = "تم تسجيل نهاية الساعات الإضافية :";
$lang['clock_error_message'] = "لقد حصل خطأ في العملية الرجاء المحاولة ثانية";
$lang['attendances'] = "الحضور و الإنصراف";

//
$lang['vacation_balances'] = "أرصدة الإجازات";
$lang['old_balance'] = "رصيد مأخوذ خلال السنة السابقة";
$lang['new_balance'] = "رصيد مأخوذ خلال السنة الحالية";
$lang['the_rest'] = "الرصيد المستحق حتى هذه اللحظة";
$lang['reset'] = "إعادة تعيين";

// Calculating salaries and attendance
$lang['calculating_salaries_attendance'] = "حساب الرواتب و الدوام";
$lang['calculating_attendance'] = "حساب الدوام";
$lang['calculating_salary'] = "حساب الراتب";

$lang['attendance_hours'] = "ساعات الحضور";
$lang['presence'] = "مجموع ساعات الحضور";
$lang['total_presence_in month'] = "مجموع ساعات الحضور لهذا الشهر";
$lang['num_days_off'] = "أيام عطلة آخر الأسبوع و الأعياد";
$lang['job_time_hours'] = "عدد ساعات الدوام";
$lang['calculate_abscence_hours'] = "إحتساب الغيابات لهذا الشهر";
$lang['all_hours_available_in_month'] = "مجموع الساعات الواجبة لهذا الشهر";
$lang['total_abscence_in_month'] = "مجموع ساعات الغياب لهذا الشهر";
$lang['finance_count'] = "كيف تحسب الحركة";
$lang['non_paid_leaves'] = "الإجازات غير مدفوعة الأجر";
$lang['discounts'] = "خصومات";
$lang['extras'] = "إضافات";

// Fixactions
$lang['name_exist'] = "هذا الإسم موجود في قاعدة البيانات... إختر إسم جديد للعنصر";
$lang['la_job_application'] = "طلب وظيفة";
$lang['la_embarkation'] = "مباشرة العمل";
$lang['duration'] = "المدة";
$lang['from_institute'] = "موقع الإنتداب";
$lang['coming_date_after_vacation'] = "تاريخ المباشرة بعد العودة من الإجازة";
$lang['embarkation_note1'] = "أقر بأنني تمتعت بجميع إجازاتي السنوية ولم تتبق لى إجازات مستحقة حتى :";
$lang['embarkation_note2'] = "فقط تبقت لي عدد";
$lang['embarkation_note3'] = "عدد الأيام المخصومة من الراتب (على أيام الإجازة المستحقة)";
$lang['notes'] = "ملاحظات";
$lang['recrutements'] = "الإنتدابات";
$lang['presence_status'] = "الحضور";
$lang['in_leave'] = "في إجازة";
$lang['present'] = "حاضر";
$lang['employee_id2'] = "معرف الموظف";
$lang['datetime'] = "التاريخ و الوقت";

$lang['start_contractual_year'] = "تاريخ بداية السنة العقدية";
$lang['days_of_work_this_yeas'] = "عدد أيام العمل في هذه السنة العقدية";
$lang['yearly_holiday_no'] = "عدد الأيام المسموح بها للإجازة السنوية";
$lang['notes_emp'] = "ملاحظات الموظف";
$lang['app_title'] = "عنوان الطلب";
$lang['payement_method'] = "طريقة السداد";
$lang['monthly_installement'] = "قيمة القسط الشهري";
$lang['payement_method1'] = "على سنة";
$lang['payement_method2'] = "على بقية العقد";
$lang['payement_method3'] = "على مبلغ محدد";
$lang['no_contract_duration'] = "لا توجد مدة محددة للعقد";
$lang['payement_months'] = "عدد الأقساط الشهرية";
$lang['months'] = "شهر(أشهر)";
$lang['the_advance_type'] = "نوع السلف";
$lang['advance_update'] = "تعديل معلومات السلفة";
$lang['rest_advance'] = "متخلد بالذمة";
$lang['last_advance_date'] = "تاريخ آخر عملية دفع";
$lang['proccess_advance'] = "خلاص القسط";
$lang['edit_employee'] = "تعديل معلومات الموظف";
$lang['in_work'] = "في الدوام";




















































// employee panel
$lang['leave_application_submitted'] = "إضافة  طلب إجازة بنجاح";
$lang['fill_up_all_fields'] = "Please Fill up all required fields to apply ";
$lang['password_do_not_match'] = "Your Entered Password Do Not Match !";
$lang['home'] = "الرئيسية";
$lang['my_time'] = "My Time";
$lang['logout'] = "خروج";
$lang['my_expense'] = "My Expense";
$lang['leave'] = "إجازة";
$lang['dob'] = "DOB";
$lang['address'] = "العنوان";
$lang['notice_board'] = "Notice Board";
$lang['clock_in'] = "دخول";
$lang['clock_out'] = "خروج";
$lang['set_reason'] = "Set Reason";
$lang['clock_out_reason'] = "Clock Out Reason";
$lang['birthdays'] = "Birthdays";
$lang['recent_mails'] = "الرسائل الواردة";
$lang['upcoming_events'] = "Upcoming Events";
$lang['award_date'] = "Award Date";
$lang['list_of_all_awrds'] = "List of All Awards Received";
$lang['received'] = "Received";
$lang['list_of_all_notice'] = "List of All Notices";
$lang['award_details'] = "Award Details";
$lang['edit_my_time_logs'] = "Edit My Time Logs";
$lang['reason_for_edit'] = "Reason for Edit";
$lang['request_update'] = "Request Update";
$lang['all_leave'] = "All Leave";
$lang['new_leave'] = "New Leave";
$lang['leave_application_you_applied'] = "Leave Applications You Applied";
$lang['submit'] = "Submit";
$lang['all_leave_details'] = "All Leave Details";
$lang['event_details'] = "Event Details";
$lang['list_of_all_event'] = "List of All Events";
$lang['amount_spent'] = "Amount Spent";
$lang['my_time_log'] = "My Time logs";
$lang['payment_history'] = "Payment History";
$lang['your_personal_profile'] = "الملف الشخصي الخاص بك";
$lang['print_payslip'] = "Print Payslip";
$lang['director'] = "Director";
$lang['accountant'] = "محاسب";
$lang['super_manager'] = "مدير عام";
$lang['dep_manager'] = "مدير إدارة";
$lang['sec_manager'] = "مدير قسم";
$lang['go_back'] = "عودة";


/**************************************************/
$lang['report_all'] = "جميع الموظفين";
$lang['report_by_employee'] = "إختيار الموظفين";
$lang['vacations_detailed'] = "إجازات (تقرير مفصل)";
$lang['custody_details'] = "تفاصيل عهدة";
$lang['finance_employee_page'] = "صفحة البيانات المالية";
$lang['employee_signature'] = "إمضاء الموظف";
$lang['manager_signature'] = "إمضاء المدير";
$lang['cin_photo'] = "صورة بطاقة الهوية";
$lang['passport_photo'] = "صورة جواز السفر";
$lang['calculate_salary_attendance1'] = "حساب الدوام و الراتب (كل موظف على حدة)";
$lang['calculate_salary_attendance2'] = "حساب الدوام لجميع الموظفين";

/**************************************************/
$lang['extra_hours'] = "الساعات الإضافية";
$lang['num_hours'] = "عدد الساعات الإضافية";
$lang['according_to_job_titles'] = "حسب المسميات الوظيفية";
$lang['according_to_departments'] = "حسب الإدارات";
$lang['course_institute_name'] = "مؤسسة التدريب";
$lang['maintenance_title'] = "عنوان الصيانة";

$lang['start_pr'] = 'بداية إستئذان';
$lang['end_pr'] = 'نهاية إستئذان';

$lang['total_extra_hours'] = 'الساعات الإضافية';
$lang['total_attendant_hours'] = 'مجموع ساعات الحضور';
$lang['total_permissions'] = 'مجموع الإستئذانات';
$lang['with_permissions'] = 'مع الإستئذانات';
$lang['without_permissions'] = 'بدون الإستئذانات';


$lang['value_per_hour'] = "قيمة الساعة العادية";
$lang['value_extra_hour'] = "قيمة الساعة الإضافية";
$lang['value_extra_hours'] = "الساعات الإضافية";
$lang['value_abscences'] = "قيمة ساعات الغياب";

$lang['missions_list']="قائمة المهام :";
$lang['add_mission']="إضافة مهمة";
$lang['edit_ev']="تعديل بند التقييم";

$lang['bill_number']="رقم الفاتورة";
$lang['item_no']="رقم البند";
$lang['account_holder_name']="اسم صاحب الحساب";
$lang['country']="الدولة";
$lang['swift_code']="سويفت كود";
$lang['city']="المدينة";
$lang['additional_bank_information']="معلومات بنكية إضافية";

$lang['abscence_days']="ساعات الغياب";
$lang['merits']="الإستحقاقات";
$lang['total_merits']="إجمالي الإستحقاقات";
$lang['net_merits']="الصافي المستحق";
$lang['deductions']="الإقتطاعات";
$lang['total_deductions']="إجمالي الإقتطاعات";
$lang['abscence']="غياب";

$lang['import_attendance']="إستيراد معلومات الحضور و الإنصراف";
$lang['upload']="رفع";
$lang['upload_condition']="يجب أن يكون الملف بإمتداد (xlsx) فقط !";

$lang['accounting']="مساءلة";
$lang['send_accounting']="إرسال مساءلة";
$lang['accounting_title']="عنوان المساءلة";
$lang['accounting_text']="نص المساءلة";
$lang['accounting_from']="طلب مساءلة من قبل ";
$lang['accounting_long_description_1']="لقد إستلمت طلب مساءلة. الرجاء إتبع رابط الطلب التالي للإطلاع على تفاصيل المساءلة :";
$lang['accounting_long_description_2'] = "رابط المساءلة";
$lang['employee_reply'] = "رد الموظف";
$lang['final_decision'] = "القرار النهائي";
$lang['accounting_archive'] = "أرشيف المساءلات";
$lang['accounting_long_description_3'] = "رد موظف على مساءلة";
$lang['accounting_long_description_4'] = "لقد تم الرد على مسائلتك للموظف : ";
$lang['acounting_deleted'] = "لقد تم حذف هذه المساءلة !";
$lang['manual_adjustment'] = "تعديل يدوي";

$lang['house_allowance'] = "بدل السكن";
$lang['transportation_allowance'] = "بدل مواصلات";

$lang['application'] = "طلب ";
$lang['note_accept'] = 'إعلام عند الموافقة';
$lang['note_refuse'] = 'إعلام عند الرفض';
$lang['note_delegate'] = 'إعلام عند التفويض';
$lang['note_consultation'] = 'إعلام عند الإستشارة';

$lang['sms_config'] = "إعدادات الرسائل القصيرة";
$lang['sender_name'] = "إسم المرسل";
$lang['sms_login'] = "إسم المستخدم";
$lang['sms_password'] = "كلمة المرور";
$lang['sms_config_note1'] = "لإستقبال رسائل قصيرة عند إرسال الطلبات للإدارة, يجب إنشاء حساب  (إسم المستخدم و كلمة المرور) في موقع التالي :";
$lang['sms_config_note2'] = "إسم المرسل يجب أن يكون بالأحرف اللاتينية فقط";
$lang['sms_test'] = "تجربة رسالة قصيرة";
$lang['sms_default'] = "الحساب الإفتراضي";

$lang['counted'] = "يحتسب في الدوام و الراتب";

