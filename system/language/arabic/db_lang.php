<?php

$lang['db_invalid_connection_str'] = 'لا يمكن الإتصال بقاعدة البيانات بناءا على إعلالات الإتصال.';

$lang['db_unable_to_connect'] = 'لا يمكن الإتصال بقاعدة البيانات بناءا على إعلالات الإتصال.';

$lang['db_unable_to_select'] = 'لا يمكن تحديد قاعدة البيانات : %s';

$lang['db_unable_to_create'] = 'لا يمكن إضافة قاعدة البيانات: %s';

$lang['db_invalid_query'] = 'الكويري غير صالحة';

$lang['db_must_set_table'] = 'يجب إضافة إسم التايبل إلى الكويري';

$lang['db_must_use_set'] = 'إستعمل هذه الدالة set.';

$lang['db_must_use_index'] = 'يجب إصافة أنديكس من أجل عمليات التحديث';

$lang['db_batch_missing_index'] = 'بعض الصفوف في قاعدة البيانات لا تناسب الأنديكس الموصوع';

$lang['db_must_use_where'] = 'التحديثات يجب أن تحتوي على where';

$lang['db_del_must_use_where'] = 'الحذف يجب أن يجتوي على where او like في الكويري';

$lang['db_field_param_missing'] = 'يجب إضافة إسم التايبل لإظهار النتائج';

$lang['db_unsupported_function'] = 'هذه الخاصية لا يمكن إستعمالها مع قاعدة البيانات هذه';

$lang['db_transaction_failure'] = 'Transaction failure: Rollback performed.';

$lang['db_unable_to_drop'] = 'لا يمكن حذف قاعة البيانات';

$lang['db_unsuported_feature'] = 'هذه الخاصية لا يمكن إستعمالها مع منصة قاعدة البيانات هذه';

$lang['db_unsuported_compression'] = 'صيغة الظغط هذه غير مدعومة في هذا السيرفر';

$lang['db_filepath_error'] = 'لا يمكن إضافة البيانات إلى الملف الحامل للمسار المذكور';

$lang['db_invalid_cache_path'] = 'مسار الكاش غير صحيح';

$lang['db_table_name_required'] = 'إسم التايبل مطلوب للعملية';

$lang['db_column_name_required'] = 'إسم العمود مطلوب للعملية';

$lang['db_column_definition_required'] = 'تعريف العمود مطلوب للعملية';

$lang['db_unable_to_set_charset'] = 'Unable to set client connection character set: %s';

$lang['db_error_heading'] = 'خطأ في قاعة البيانات';

/* End of file db_lang.php */

/* Location: ./system/language/english/db_lang.php */