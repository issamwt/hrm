<?php

$lang['upload_userfile_not_set'] = "لا يمكن إيجاد متغير post إسمه userfile";

$lang['upload_file_exceeds_limit'] = "الملف تجاوز الحد المسموح به في إعدادات البي آتش بي";

$lang['upload_file_exceeds_form_limit'] = "الملف تجاوز الحد المسموح به في الإستمارة";

$lang['upload_file_partial'] = "تحميل الملف صار بشكل جزئي";

$lang['upload_no_temp_directory'] = "الملف المؤقت غير موجود";

$lang['upload_unable_to_write_file'] = "لم يكن بالإمكان كتابة الملف على القرص الصلب";

$lang['upload_stopped_by_extension'] = "تحميل الملف توقف بسبب extension";

$lang['upload_no_file_selected'] = "لم تقم بإختيار ملف للتحميل";

$lang['upload_invalid_filetype'] = "نوع الملف الذي تريد تحميله غير مقبول";

$lang['upload_invalid_filesize'] = "حجم الملف تجاوز الحد المسموح به";

$lang['upload_invalid_dimensions'] = "أبعاد الصورة تجاوزت الحد المسموح به";

$lang['upload_destination_error'] = "حذثت مشكلة أثناء محاولة نقل الملف الذي تم تحميله إلى المجلد النهائي";

$lang['upload_no_filepath'] = "مسار التحميل غير صحيح";

$lang['upload_no_file_types'] = "لم تحدد نوعية الملفات المسموح بتحميلها";

$lang['upload_bad_filename'] = "الملف موجود أصلا على الخادم";

$lang['upload_not_writable'] = "المجلد الذي تريد أن تحمل إليه الملف غير قابل للكتابة";

/* End of file upload_lang.php */

/* Location: ./system/language/english/upload_lang.php */