<?php

$lang['terabyte_abbr'] = "تيرابايت";

$lang['gigabyte_abbr'] = "جيغابايت";

$lang['megabyte_abbr'] = "ميغابايت";

$lang['kilobyte_abbr'] = "كيلوبايت";

$lang['bytes'] = "بايت";

/* End of file number_lang.php */

/* Location: ./system/language/english/number_lang.php */