<?php

$lang['imglib_source_image_required'] = "يجب إصافة مسار للصورة في الإعدادات";

$lang['imglib_gd_required'] = "GD image library لازمة لأجل هذه الخاصية";

$lang['imglib_gd_required_for_props'] = "السرفر يجب أن يدعم  GD image library";

$lang['imglib_unsupported_imagecreate'] = "الخادم لا يدعم GD function الازمة لهذه العملية";

$lang['imglib_gif_not_supported'] = "gif مرفوضة, إستعمل صيغة أخرى";

$lang['imglib_jpg_not_supported'] = "غير مدعومة jpg";

$lang['imglib_png_not_supported'] = "غير مدعومة png";

$lang['imglib_jpg_or_png_required'] = "بروتوكول تعديل أبعاد الصورة يدعم فقط صيغة jpg و png";

$lang['imglib_copy_error'] = "نسخ الملف غير ممكن, تأكد من ان الملف قابل للقراءة";

$lang['imglib_rotate_unsupported'] = "Image rotation غير مدعومة على الخادم";

$lang['imglib_libpath_invalid'] = "المسار غير صحيح";

$lang['imglib_image_process_failed'] = "معالجة الصور فشلت. الرجاء التحقق من أن الخادم الخاص بك يدعم بروتوكول المختار وأن الطريق إلى مكتبة الصور الخاصة بك هو الصحيح.";

$lang['imglib_rotation_angle_required'] = "زاوية تدوير لازمة لتدوير الصورة";

$lang['imglib_writing_failed_gif'] = "GIF صورة";

$lang['imglib_invalid_path'] = "المسار غير صحيح";

$lang['imglib_copy_failed'] = "فشل نسخ الصورة";

$lang['imglib_missing_font'] = "لا يمكن إيجاد خط للإستعمال";

$lang['imglib_save_failed'] = "لا يمكن حفظ الصورة, تأكد من أن الصورة و المجلد قابلان للكتابة";

/* End of file imglib_lang.php */

/* Location: ./system/language/english/imglib_lang.php */