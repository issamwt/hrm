<?php

$lang['cal_su'] = "أح";

$lang['cal_mo'] = "إث";

$lang['cal_tu'] = "ثل";

$lang['cal_we'] = "أر";

$lang['cal_th'] = "خم";

$lang['cal_fr'] = "جم";

$lang['cal_sa'] = "سب";

$lang['cal_sun'] = "أحد";

$lang['cal_mon'] = "إثن";

$lang['cal_tue'] = "ثلا";

$lang['cal_wed'] = "أرب";

$lang['cal_thu'] = "خمي";

$lang['cal_fri'] = "جمع";

$lang['cal_sat'] = "سبت";

$lang['cal_sunday'] = "الأحد";

$lang['cal_monday'] = "الإثنين";

$lang['cal_tuesday'] = "الثلاثاء";

$lang['cal_wednesday'] = "الأربعاء";

$lang['cal_thursday'] = "الخميس";

$lang['cal_friday'] = "الجمعة";

$lang['cal_saturday'] = "السبت";

$lang['cal_jan'] = "جان";

$lang['cal_feb'] = "فيف";

$lang['cal_mar'] = "مار";

$lang['cal_apr'] = "أبر";

$lang['cal_may'] = "ماي";

$lang['cal_jun'] = "جوا";

$lang['cal_jul'] = "جوي";

$lang['cal_aug'] = "أوت";

$lang['cal_sep'] = "سبت";

$lang['cal_oct'] = "أوكت";

$lang['cal_nov'] = "نوف";

$lang['cal_dec'] = "ديس";

$lang['cal_january'] = "جانفي";

$lang['cal_february'] = "فيفري";

$lang['cal_march'] = "مارس";

$lang['cal_april'] = "أفريل";

$lang['cal_mayl'] = "ماي";

$lang['cal_june'] = "جوان";

$lang['cal_july'] = "جويلية";

$lang['cal_august'] = "أوت";

$lang['cal_september'] = "سبتمبر";

$lang['cal_october'] = "أكتوبر";

$lang['cal_november'] = "نوفمبر";

$lang['cal_december'] = "ديسمبر";

/* End of file calendar_lang.php */

/* Location: ./system/language/english/calendar_lang.php */