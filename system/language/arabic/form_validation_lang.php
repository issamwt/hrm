<?php

$lang['required'] = "الحقل %s مطلوب";

$lang['isset'] = "الحقل %s يجب أن يحتوي على قيمة";

$lang['valid_email'] = "الحقل %s يجب أن يحتوي على عنوان بريد إلكتروني صحيح";

$lang['valid_emails'] = "الحقل %s يجب أن يحتوي على عناوين بريد إلكترونية صحيحة";

$lang['valid_url'] = "الحقل %s يجب أن يحتوي على رابط صحيح";

$lang['valid_ip'] = "الحقل %s يجب أن يحتوي على آيبي صحيح";

$lang['min_length'] = "الحقل %s يجب أن يحتوي على %s حرف على الأقل";

$lang['max_length'] = "الحقل %s يجب أن يحتوي على %s حرف على الأكثر";

$lang['exact_length'] = "الحقل %s يجب أن يحتوي على %s حرف بالظبط";

$lang['alpha'] = "الحقل %s يجب أن يحتوي على حروف أبجدية فقط";

$lang['alpha_numeric'] = "الحقل %s يجب أن يحتوي على حروف أبجدية أو أرقام فقط";

$lang['alpha_dash'] = "الحقل %s يجب أن يحتوي على حروف أبجدية أو _ أو \" فقط";

$lang['numeric'] = "الحقل %s يجب أن يحتوي على أرقام فقط";

$lang['is_numeric'] = "الحقل %s يجب أن يحتوي على أرقام فقط";

$lang['integer'] = "الحقل %s يجب أن يحتوي على عدد صحيح فقط";

$lang['regex_match'] = "الحقل %s ليس في الشكل المطلوب";

$lang['matches'] = "الحقل %s  ليس مماثل للحقل %s";

$lang['is_unique'] = "الحقل %s يجب أن يحتوي على أيدي خاص";

$lang['is_natural'] = "الحقل %s يجب أن يحتوي على أرقام سلبية فقط";

$lang['is_natural_no_zero'] = "الحقل %s يجب أن يحتوي على أرقام أكبر من الصفر فقط";

$lang['decimal'] = "الحقل %s يجب أن يحتوي على أرقام عشرية فقط";

$lang['less_than'] = "الحقل %s يجب أن يحتوي على عدد أقل من %s";

$lang['greater_than'] = "الحقل %s يجب أن يحتوي على عدد أكثر من %s";

/* End of file form_validation_lang.php */

/* Location: ./system/language/english/form_validation_lang.php */