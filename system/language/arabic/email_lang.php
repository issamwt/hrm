<?php

$lang['email_must_be_array'] = "The email validation method must be passed an array.";

$lang['email_invalid_address'] = "عنوان إلكتروني غير صالح: %s";

$lang['email_attachment_missing'] = "غير ممكن إيجاد ملف مرفق مع الرسالة : %s";

$lang['email_attachment_unreadable'] = "لا يمكن فتح الملف المرفق: %s";

$lang['email_no_recipients'] = "يجب إضافة العلامات: To, Cc, or Bcc";

$lang['email_send_failure_phpmail'] = "لا يمكن إستعمال دالة PHP mail(). السيرفر لا يدعمها";

$lang['email_send_failure_sendmail'] = "لا يمكن إستعمال دالة PHP Sendmail. السيرفر لا يدعمها";

$lang['email_send_failure_smtp'] = "لا يمكن إستعمال  PHP SMTP.  السيرفر لا يدعمها";

$lang['email_sent'] = "تم الإرسال بنجاح بإستعمال البروتوكول: %s";

$lang['email_no_socket'] = "غير ممكن فتح socket إلى Sendmail. راجع الإعدادات.";

$lang['email_no_hostname'] = "لم تحدد SMTP hostname.";

$lang['email_smtp_error'] = "خطأ SMTP : %s";

$lang['email_no_smtp_unpw'] = "يجب إضافة SMTP كلمة مرور و إسم دخول";

$lang['email_failed_smtp_login'] = "لا يمكن إرسال AUTH LOGIN command. خطأ: %s";

$lang['email_smtp_auth_un'] = "لا يمكن التحقق من إسم الدخول : %s";

$lang['email_smtp_auth_pw'] = "لا يمكن التحقق من كلمة المرور : %s";

$lang['email_smtp_data_failure'] = "لا يمكن إرسال البيانات: %s";

$lang['email_exit_status'] = "Exit status code: %s";

/* End of file email_lang.php */

/* Location: ./system/language/english/email_lang.php */