<?php

// job_places
$lang['job_places'] = 'أماكن العمل';
$lang['add_job_places'] = 'إضافة مكان عمل';
$lang['update_job_places'] = 'تعديل مكان عمل';
$lang['address_ar'] = 'العنوان (بالعربية)';
$lang['address_en'] = 'العنوان (بالإنجليزية)';
$lang['lat'] = 'خط العرض';
$lang['lng'] = 'خط الطول';
$lang['zoom'] = 'زوم';
$lang['map'] = 'الخريطة';
$lang['job_place_used'] = 'معلومات مكان العمل مستخدمة';
$lang['employee_category_used'] = 'معلومات تصنيف الموظفين مستعملة';

// Evaluation items
$lang['all_items'] = 'كل البنود';
$lang['new_item'] = 'بند جديد';
$lang['name_ev_item'] = 'بند التقييم';
$lang['ev_type'] = 'نوع البند';
$lang['percentage'] = 'نسبة';
$lang['rating'] = 'تقدير';
$lang['fromto'] = 'من 0%........................إلى  100%';

//organizational_chart
$lang['organizational_chart'] = 'الهيكل التنظيمي';
$lang['acompany'] = 'الشركة';
$lang['abranche'] = 'الفرع';
$lang['adeprtment'] = 'الإدارة';
$lang['adseignation'] = 'القسم';
$lang['aunit'] = 'الوحدة';
$lang['units'] = 'الوحدات';
$lang['adivision'] = 'الشعبة';
$lang['divisions'] = 'الشعب';
$lang['add_unit'] = 'أضف وحدة';
$lang['add_division'] = 'أضف شعبة';
$lang['alert'] = 'الرجاء تعمير كل الحقول';
$lang['alert_error'] = 'لقد وقع خطأ ما في الإتصال بقاعدة البيانات. الرجاء إعادة تحميل الصفحة و المحاولة ثانية';

// users_permissions_system
$lang['users_permissions_system'] = 'نظام مستخدمين وصلاحيات';
$lang['users_list'] = 'قائمة المستخدمين';
$lang['add_user'] = 'إضافة مستخدم';
$lang['edit_user'] = 'تعديل مستخدم';
$lang['user_type'] = 'فئة المستخدم';
$lang['choose_user_type'] = 'إختر فئة للمستخدم';
$lang['user_note'] = 'ملاحظات';
$lang['super_admin'] = 'مدير عام';
$lang['hr_admin'] = 'مسؤول الموارد البشرية';
$lang['compt_admin'] = 'محاسب';
$lang['guest_admin'] = 'حساب إستثنائي';
$lang['user_permissions'] = 'صلاحيات المستخدم';
$lang['options'] = 'خيارات';
$lang['user_data'] = 'بيانات المستخدم';
$lang['password'] = 'كلمة المرور';
$lang['user_department'] = 'الإدارة';
$lang['user_designation'] = 'القسم';
$lang['user_option1'] = 'للمدير إمكانية تقديم طلب بدلا من موظف :';
$lang['user_updated'] = 'تم تحديث معلومات المستخدم بنجاح';
$lang['user_created'] = 'تم إضافة مستخدم بنجاح';

// Organizational Structure
$lang['organizational_structure'] = 'الهيكل التنظيمي';
$lang['overview'] = 'نظرة عامة';
$lang['branches'] = 'الفروع';
$lang['branche'] = 'الفرع';
$lang['branches_list'] = 'قائمة الفروع';
$lang['add_branche'] = 'إضافة فرع';
$lang['select_branche'] = 'إختر الفرع';
$lang['units_list'] = 'قائمة الوحدات';
$lang['unit_name'] = 'الوحدة';
$lang['select_designation'] = 'إختر القسم';
$lang['divisions_list'] = 'قائمة الشعب';
$lang['division_name'] = 'الشعبة';
$lang['select_unit'] = 'إختر الوحدة';

// Approvals
$lang['designation'] = 'القسم';
$lang['new_approval_chain'] = 'سلسلة موافقات جديدة';
$lang['approvals'] = 'الموافقات';
$lang['approvals_list'] = 'قائمة سلسلة الموافقات';
$lang['add_approval'] = 'إضافة سلسلة موافقات';
$lang['edit_approval'] = 'تعديل سلسلة الموافقات';
$lang['link_approvals'] = 'ربط  سلسلة الموافقات';
$lang['follow_up_approvals'] = 'متابعة الموافقات';
$lang['configure_comm'] = 'تهيئة اللجنة حسب';
$lang['configure_scope'] = 'تهيئة المدى حسب';
$lang['opt_employee'] = 'الموظف';
$lang['opt_structure'] = 'الهيكلية';
$lang['config'] = 'التهيئة';
$lang['follow_up'] = 'المتابعة';
$lang['configure_approval'] = 'تهيئة الموافقة';
$lang['approvals_data'] = 'بيانات سلسلة الموافقات';
$lang['approvals_scope'] = 'مجال سلسلة الموافقات';
$lang['approvals_comm_adm'] = 'تحديد لجنة الموافقات من المدراء';
$lang['approvals_comm_emp'] = 'تحديد لجنة الموافقات من الموظفين';
$lang['approvals_comm_squence'] = 'تحديد تسلسل اللجنة';
$lang['all'] = 'الكل';
$lang['approval_notify_emp'] = 'إعلام الموظف بنتيجة الموافقة النهائية';
$lang['admin_must_participate'] = 'ضرورة مشاركة المدير المباشر في الموافقة';
$lang['can_add_replacement'] = 'القدرة على إضافة البديل من قبل مدير مباشر';
$lang['can_consult'] = 'إمكانية الإستشارة على الطلب';
$lang['choose_admin'] = 'إختر المدراء';
$lang['responsability'] = 'المسؤولية';
$lang['manager'] = 'مدير';
$lang['the_list'] = 'القائمة';
$lang['choose_employees'] = 'إختر الموظفين';
$lang['direct_manager'] = 'المدير المباشر';
$lang['order'] = 'الترتيب';
$lang['comm_members'] = 'أعضاء اللجنة';
$lang['member_type'] = 'نوع الأعضاء';
$lang['appro_accept'] = 'موافقة';
$lang['appro_refuse'] = 'رفض';
$lang['appro_view'] = 'إطلاع';
$lang['appro_delegate'] = 'تفويض';
$lang['appro_consultation'] = 'إستشارة';
$lang['approvals_comm_squence_note'] = 'إذا كنت قد أضفت أو حذفت أعضاء من الجنة فالرجاء حفظ الصفحة كي تظهر قائمة الأعضاء الجديدة';
$lang['follow_up1'] = 'تفعيل خيار متابعة الطلب تلقائيا من قبل النظام';
$lang['follow_up2'] = 'خلال';
$lang['follow_up3'] = 'دقيقة من إستلام المدير المخول بالموافقة للطلب سيقوم النظام بإرسال بريد تذكيري';
$lang['follow_up4'] = 'مرة / مرات';
$lang['follow_up5'] = 'و عند إنقضاءفترة التذكير مجددا سيقوم النظام تلقائيا ب :';
$lang['follow_up6'] = 'تفويض البديل في حال وجوده و في حال عدم وجود بديل يبقى الطلب عند المدير.';
$lang['follow_up7'] = 'إرسال الطلب مباشرة إلى المدير التالي في سلسلة الموافقات';
$lang['exit'] = 'خروج';

// Link Approvals
$lang['link_approval_list'] = 'قائمة حركات الموافقات';
$lang['link_approval_expression'] = 'تهيئة الموافقات المربوطة بنوع الحركة';
$lang['la_advance'] = 'السلف';
$lang['la_vacations'] = 'الإجازات';
$lang['la_caching'] = 'صرف';
$lang['la_filter_dues'] = 'تصفية مستحقات';
$lang['la_custody'] = 'عهدة';
$lang['la_resignation'] = 'إستقالة';
$lang['la_transfer'] = 'نقل لإدارة أخرى';
$lang['la_training'] = 'دورة تدريبية';
$lang['la_overtime'] = 'ساعات إضافية';
$lang['la_purchase'] = 'شراء';
$lang['la_maintenance'] = 'صيانة';
$lang['la_annual_assessment'] = 'أخرى';
$lang['app_name'] = 'إسم الطلب';
$lang['la_permission'] = 'إستئذان';
$lang['la_def_salary'] = 'التعريف بالراتب';
$lang['la_recrutement'] = 'إنتداب';
$lang['configure_approvals'] = 'تهيئة الموافقات';
$lang['help'] = 'مساعدة';
$lang['help1'] = 'إختر نوع (أنواع) الموافقة من القائمة التي على اليمين.';
$lang['help2'] = 'أضف نوع الموافقة إلى القائمة التي على اليسار';
$lang['help3'] = 'إختر الحركة (أو الحركات) التي تود ربطها من القائمة التي على اليمين.';
$lang['help4'] = 'أضف الحركة إلى القائمة لتي على اليسار.';
$lang['help5'] = 'حفظ';

// New
$lang['ideal_employee'] = 'الموظف المثالي لهذا الشهر';


// last
$lang['total_branches'] = 'مجموع الفروع';
$lang['total_sections'] = 'مجموع الأقسام';
$lang['last_application'] = 'آخر المطالب';
$lang['all_documents'] = 'جميع الوثائق';
$lang['add_document'] = 'وثيقة جديدة';
$lang['link'] = 'الرابط';
$lang['link_note'] = 'إسم الملف يجب ان يكون بالأحرف اللاتينية فقط';
$lang['get_dep_admins'] = "مديرو الإدارات";
$lang['get_des_admins'] = "مديرو الأقسام";
$lang['waiting_apps'] = "مطالب بإنتظار الرد";
$lang['received_apps_archive'] = "أرشيف المطالب الواردة";
$lang['skip'] = "تخطي";
$lang['cahing_type'] = "نوع الصرف";
$lang['cahing_type0'] = "مفوتر";
$lang['cahing_type1'] = "غير مفوتر";
$lang['beneficiary_name'] = "اسم الجهة المستفيدة";
$lang['beneficiary_address'] = "عنوان البنك";
$lang['caching_reason'] = "الغرض";
$lang['end_contractual_year'] = "تاريخ نهاية السنة العقدية";
$lang['old_rest'] = "الرصيد المتبقي للموظف من السنة السابقة";
$lang['modify_app'] = "تعديل الطلب";
$lang['received'] = "تم الإستلام";
$lang['receipt_confirmation'] = "تأكيد الإستلام";
$lang['receipt_confirmed_successfully'] = "تم تأكيد الإستلام";



















