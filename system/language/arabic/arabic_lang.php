<?php

// menu
$lang['dashboard'] = 'لوحة التحكم';
$lang['job_titles'] = 'المسميات الوظيفية';
$lang['departements'] = 'الإدارات';
$lang['designations'] = 'الأقسام';
$lang['employee_category'] = 'فئات الموظفين';
$lang['evaluation_items'] = 'بنود التقييم';
$lang['organizational_chart'] = 'الهيكل التنظيمي';
$lang['mailbox'] = 'صندوق البريد';
$lang['inbox'] = 'صندوق الوارد';
$lang['draft'] = 'مسودة';
$lang['sent'] = 'أرسلت';
$lang['trash'] = 'قمامة';
$lang['task'] = 'مهمة';
$lang['notice'] = 'إشعار';
$lang['events'] = 'أحداث';
$lang['attendance'] = 'الحضور';
$lang['timechange_request'] = 'طلب تغيير الوقت';
$lang['attendance_report'] = 'تقرير الحضور';
$lang['overtime'] = 'تأخير';
$lang['application_list'] = 'قائمة المطالب';
$lang['stock'] = 'المخزون';
$lang['expenses'] = 'مصاريف';
$lang['award'] = 'مكافآت';
$lang['payroll'] = 'كشف الرواتب';
$lang['set_salary_details'] = ' تفاصيل الرواتب';
$lang['employee_salary_list'] = ' قائمة رواتب الموظفين';
$lang['make_payment'] = 'قم بالدفع';
$lang['generate_payslip'] = 'إنشاء قسيمة دفع المرتب';
$lang['training'] = 'تدريب';
$lang['department'] = 'إدارة';
$lang['employee'] = 'موظف';
$lang['manage_employee'] = 'إدارة الموظفين';
$lang['set_access_role'] = 'تحديد إمكانية الدخول';
$lang['settings'] = 'إعدادات';
$lang['general_settings'] = 'إعدادات عامة';
$lang['set_working_days'] = 'تحديد ايام العمل';
$lang['personal_event'] = 'حدث شخصي';
$lang['leave_category'] = 'تصنيف الإجازات';
$lang['notification_settings'] = 'إعدادات الإشعار';
$lang['language_settings'] = 'اعدادات اللغة';
$lang['database_backup'] = 'قاعدة البيانات الاحتياطية';
$lang['stock_category'] = 'تصنيف المخزون ';
$lang['add_stock'] = 'إضافة المخزون';
$lang['stock_list'] = 'قائمة المخزون ';
$lang['hourly_rate'] = 'نماذج كشف رواتب';

// dashboard
$lang['hr_title'] = 'HR Lite';
$lang['total_department'] = 'مجموع الإدارات';
$lang['more_info'] = 'المزيد';
$lang['total_employee'] = 'مجموع الموظفين';
$lang['total_award'] = 'مجموع المكافآت';
$lang['total_stock'] = 'إجمالي المخزون';
$lang['total_expense'] = 'مجموع النفقات';
$lang['expense_report'] = 'تقرير المصاريف';
$lang['select_year'] = 'اختر السنة';
$lang['recent_application'] = 'التطبيقات الأخيرة';
$lang['view_all'] = 'عرض الكل';
$lang['upcomming_birthday'] = 'عيد الميلاد المقبل';
$lang['today'] = 'اليوم';
$lang['recent_notice'] = 'ملاحظة الاخيرة';
$lang['view_details'] = 'التفاصيل';
$lang['recent_email'] = 'البريد الإلكتروني الاخير';
$lang['compose'] = 'إنشاء';
$lang['minute'] = 'دقائق';
$lang['ago'] = 'منذ';
$lang['hour'] = 'ساعة';
$lang['hours'] = 'ساعات';
$lang['day'] = 'يوم';
$lang['days'] = 'أيام';
$lang['nothing_to_display'] = 'لا يوجد شيء للعرض';
$lang['personal_event'] = 'حدث شخصي';
$lang['event_name'] = 'اسم الحدث';
$lang['start_date'] = 'تاريخ البداية';
$lang['end_date'] = 'تاريخ الانتهاء';
$lang['close'] = 'غلق';
$lang['update'] = 'تحديث';
$lang['inbox_details'] = 'تفاصيل البريد الوارد';
$lang['sent_details'] = 'إرسال تفاصيل';
$lang['compose_new'] = 'إنشاء رسالة جديدة';
$lang['attachment'] = 'المرفق';
$lang['save_to_draft'] = 'حفظ كمسودة';
$lang['send'] = 'إرسال';
$lang['discard'] = 'تجاهل';
$lang['discard_alert'] = 'هل أنت متأكد من تجاهل هذا البريد الإلكتروني ? ';
$lang['message_permanent_deleted'] = '!+رسالة معلومات حذفت نهائيا';
$lang['message_deleted'] = 'رسالة معلومات حذفت بنجاح !';
$lang['message_sent'] = '!رسالة معلومات أرسلت بنجاح';
$lang['please_select_message'] = "الرجاء إختيار رسالة";
$lang['delete_alert'] = '?انت بصدد حذف سجل.خطوة لا يمكن التراجع عنها. هل أنت متأكد';


// task
$lang['task_list'] = 'قائمة المهام';
$lang['assign_task'] = 'تعيين المهام';
$lang['task_name'] = 'اسم المهمة';
$lang['created_date'] = 'تاريخ الإنشاء';
$lang['due_date'] = 'التاريخ المقرر';
$lang['status'] = 'الحالة';
$lang['changes/view'] = 'تغير / عرض';
$lang['assined_to'] = 'مخصص ل';
$lang['select_employee'] = 'حدد من الموظفين';
$lang['select_admin'] = 'حدد من المدراء';
$lang['estimated_hour'] = 'تقدير الساعة ';
$lang['input_value'] = 'إدخال كامل القيمة';
$lang['task_description'] = 'وصف المهمة';
$lang['progress'] = 'التقدم';
$lang['task_status'] = 'حالة المهمة';
$lang['pending'] = 'في انتظار';
$lang['started'] = 'بدأت';
$lang['completed'] = 'منجز';
$lang['save'] = 'حفظ';
$lang['task_management'] = "ادارة المهام";
$lang['all_task'] = "جميع المهام";
$lang['save_task'] = "تحديث معلومات المهمة بنجاح";
$lang['task_updated'] = "تحديث المهمة بنجاح!";
$lang['task_comment_save'] = "تحديث التعليق بنجاح";
$lang['task_file_updated'] = 'تحديث الملف بنجاح';
$lang['task_file_added'] = 'تحديث معلومات الملف بنجاح';
$lang['task_details'] = "تفاصيل المهمة";
$lang['operation_failed'] = 'العملية فشلت!';
$lang['task_initials'] = 'Task Initials';
$lang['update'] = 'تحديث';
$lang['details'] = 'تفاصيل';
$lang['comments'] = 'تعليقات';
$lang['post_comment'] = 'آخر التعليقات';
$lang['change'] = 'تغيير';
$lang['select_file'] = 'اختر الملف';
$lang['add_more'] = 'أضف المزيد';
$lang['upload_file'] = 'تحميل الملفات';
$lang['file_title'] = 'عنوان الملف';
$lang['description'] = 'الوصف';
$lang['attach_file_list'] = 'مرفق قائمة الملفات ';
$lang['files'] = 'ملفات';
$lang['size'] = 'حجم';
$lang['date'] = 'تاريخ';
$lang['uploaded_by'] = 'تم الرفع بواسطة';
$lang['action'] = 'الأمر';

// notice
$lang['notice'] = 'ملاحظة';
$lang['all_notice'] = 'كل الملاحظات';
$lang['new_notice'] = 'ملاحظة جديدة';
$lang['short_description'] = 'وصف قصير';
$lang['long_description'] = 'وصف مطول';
$lang['sl'] = 'الرتبة';
$lang['title'] = 'عنوان';
$lang['publication_status'] = 'حالة المنشور';
$lang['published'] = 'نشرت';
$lang['unpublished'] = 'غير منشورة';
$lang['notice_details'] = 'تفاصيل الملاحظة';
$lang['published_date'] = 'تاريخ النشر';
$lang['no_record_found'] = 'لا يوجد سجلات';
$lang['notice_saved'] = "تم حفظ الملاحظة بنجاح ";
$lang['deleted_notice'] = "تم حذف الملاحظة بنجاح";

// event
$lang['year'] = 'سنة';
$lang['event_list'] = 'قائمة الأحداث';
$lang['new_event'] = 'حدث جديد';
$lang['event_name'] = 'اسم الحدث';
$lang['event_management'] = "إدارة الأحداث";
$lang['event_already_exist'] = "معلومات الإجازة موجودة";
$lang['event_saved'] = "تم حفظ معلومات الإجازة بنجاح!";
$lang['event_deleted'] = "تم حذف معلومات الإجازة بنجاح!";

// attendance
$lang['overtime_details'] = "تفاصيل العمل الإضافي";
$lang['overtime_list'] = "قائمة العمل الإضافي";
$lang['new_overtime'] = "عمل اضافي جديد";
$lang['overtime_management'] = "إدارة الساعات الإضافية";
$lang['overtime_already_exist'] = "Overtime Information Already Exist !";
$lang['overtime_saved'] = "Overtime Information Successfully Saved !";
$lang['overtime_deleted'] = "Overtime Information Successfully Delete !";
$lang['time_changes_request'] = "طلب تغييرالوقت";
$lang['attendance_management'] = "إدارة الحضور";
$lang['view_time_change_request'] = "عرض طلب تغييرالوقت";
$lang['time_change_accept'] = "تم قبول طلب تغييرالوقت !";
$lang['time_change_decline'] = "تم رفض طلب تغييرالوقت";
$lang['attendance_report'] = "تقرير الحضور";
$lang['department_name'] = "إسم الإدارة ";
$lang['department'] = "الإدارات";
$lang['month_year'] = "شهر و سنة";
$lang['search'] = "بحث";
$lang['name'] = "الإسم";
$lang['holiday'] = "إجازة ";
$lang['on_leave'] = "في إجازة";
$lang['absent'] = "غائب";
$lang['total_working_hour'] = "مجموع ساعات العمل هذا الأسبوع";
$lang['employee_attendance'] = "حضور الموظف";
$lang['month'] = "شهر";
$lang['week'] = "أسبوع";
$lang['list_of_all_time_change_request'] = "طلب تغييرقائمة كل الأوقات";
$lang['time_in'] = "الوقت في";
$lang['time_out'] = "نفذ الوقت";
$lang['accepted'] = "قبلت";
$lang['rejected'] = "مرفوض";
$lang['employee_name'] = "اسم الموظف";
$lang['overtime_date'] = "تاريخ العمل الإضافي";
$lang['overtime_hour'] = "ساعات العمل الإضافي";
$lang['total_overtime_hour'] = "مجموع ساعات العمل الإضافي";
$lang['select_designation'] = "إختر تسمية";
$lang['employee'] = "موظف";
$lang['overtime_report'] = "تقرير العمل الاضافي";
$lang['company_info'] = "معلومات عن الشركة";
$lang['time_change_request_details'] = " تفاصيل طلب تغييرالوقت";
$lang['old_time_in'] = "الزمن القديم في";
$lang['old_time_out'] = "الزمن القديم من";
$lang['new_time_out'] = "الزمن الجديد من";
$lang['new_time_in'] = "الزمن الجديد في";
$lang['reason'] = "سبب";

// application list
$lang['application_list'] = 'قائمة المطالب';
$lang['application_management'] = 'إدارة المطالب';
$lang['changes_application_status'] = '"تم تغيير حالة المطلب بنجاح!"';
$lang['application_details'] = '"تفاصيل المطلب"';
$lang['leave_date'] = 'تاريخ الإجازة';
$lang['from'] = 'من';
$lang['to'] = 'إلى';
$lang['leave_type'] = 'نوع الإجازة';
$lang['applied_on'] = 'المطبقة في';
$lang['leave_reason'] = 'أسباب الإجازة';
$lang['current_status'] = 'الوضع الحالي';
$lang['approved_by'] = 'المعتمدة بواسطة';
$lang['give_comment'] = 'أعط تعليق';
$lang['leave_date_of'] = 'تفاصيل الإجازة';
$lang['total'] = 'المجموع';

// stock
$lang['stock_category'] = 'فئة المخزون';
$lang['stock_management'] = 'إدارة المخزون';
$lang['category_saved'] = 'تم حفظ المعلومات بنجاح!';
$lang['stock_category_exist'] = 'المعلومات موجودة حاليا!';
$lang['stock_category_delete'] = 'تم حذف المعلومات بنجاح!';
$lang['sub_category_used'] = 'المعلومات موجودة حاليا!';
$lang['sub_category_deleted'] = 'تم حذف المعلومات بنجاح!';
$lang['manage_stock'] = 'إدارة المخزون';
$lang['stock_saved'] = 'تم إضافة المعلومات بنجاح!';
$lang['stock_history'] = ' تاريخ المخزون';
$lang['history'] = 'السجل';
$lang['stock_deleted'] = 'تم حذف المعلومات بنجاح!';
$lang['new_category'] = 'تصنيف جديد';
$lang['sub_category'] = 'الفئة الفرعية';
$lang['select_category'] = 'اختر الفئة';
$lang['item_name'] = 'اسم العنصر';
$lang['inventory'] = 'المخزون';
$lang['buying_date'] = 'تاريخ الشراء ';

// expense
$lang['expense_saved'] = 'تم إضافة معلومات المصاريف بنجاح!';
$lang['expense_details'] = 'تفاصيل النفقات';
$lang['expense_management'] = 'إدارة النفقات';
$lang['expense_status_update'] = 'تم تحديث معلومات المصاريف بنجاح!';
$lang['expense_status_update'] = 'تم تحديث معلومات المصاريف بنجاح!';
$lang['all_expense'] = 'جميع المصاريف';
$lang['new_expense'] = 'نفقات جديدة';
$lang['purchase_from'] = 'شراء من';
$lang['purchase_date'] = 'تاريخ الشراء';
$lang['amount'] = 'كمية';
$lang['purchase_by'] = 'تم الشراء بواسطة';
$lang['bill_copy'] = 'نسخة الفاتورة';
$lang['cancel'] = 'إلغاء';
$lang['approved'] = 'معتمدة';

// award
$lang['employee_award'] = 'مكافئة الموظفين';
$lang['award_management'] = 'إدارة المكافآت';
$lang['award_information_saved'] = 'تم إضافة مكافئة بنجاح!';
$lang['award_information_delete'] = 'تم حذف مكافئة بنجاح!';
$lang['award_list'] = 'قائمة المكافآت';
$lang['give_award'] = 'أعط المكافئة';
$lang['give_award'] = 'أعط المكافئة';
$lang['award_name'] = 'اسم المكافئة';
$lang['gift'] = 'هدية';
$lang['gift_item'] = 'بند الهدية';
$lang['select_month'] = 'اختر الشهر';
$lang['employee_award_list'] = 'قائمة مكافآت الموظف';

//payroll
$lang['hourly_rate_details'] = "تفاصيل الأجر بالساعة";
$lang['payroll_page_header'] = "إدارة المرتبات";
$lang['payroll_template_save'] = 'تم حفظ نماذج المرتبات بنجاح!';
$lang['hourly_rate_save'] = 'تم حذف معدل الساعات بنجاح!';
$lang['manage_salary_details'] = "إدارة تفاصيل الراتب";
$lang['salary_detail_save'] = 'تم حفظ معلومات الرواتب بنجاح!';
$lang['employee_salary_details'] = "تفاصيل رواتب الموظفين";
$lang['view_salary_details'] = "عرض تفاصيل الرواتب";
$lang['working_hour_empty'] = "ساعات العمل فارغة!";
$lang['payment_salary_details'] = 'تفاصيل دفع الراتب';
$lang['payment_info_updated'] = 'تم تحديث معلومات الدفع بنجاح!';
$lang['generate_payslip'] = "إنشاء قسيمة الدفع";
$lang['employee_id'] = "الرقم الوظيفي";
$lang['designation'] = "الإدارة و القسم";
$lang['joining_date'] = "تاريخ التعيين";
$lang['salary_grade'] = "درجة الراتب";
$lang['overtime_rate'] = "تقييم العمل الاضافي";
$lang['list_of_all_employees'] = "قائمة الموظفين ورواتبهم";
$lang['full_name'] = "الاسم الكامل";
$lang['salary_type'] = "نوع الراتب";
$lang['basic_salary'] = "الراتب الاساسي";
$lang['per_hour'] = "في الساعة";
$lang['salary_details'] = "تفاصيل الراتب";
$lang['allowance_details'] = "تفاصيل التعويضات";
$lang['deduction_details'] = "تفاصيل الخصم";
$lang['gross_salary'] = "الراتب الإجمالي";
$lang['total_salary_details'] = "مجموع تفاصيل الراتب";
$lang['total_deduction'] = "إجمالي الخصم";
$lang['net_salary'] = "صافي الراتب";
$lang['company_info'] = "معلومات عن الشركة";
$lang['select_department'] = "اختر الإدارة";
$lang['select_month'] = "اختر الشهر";
$lang['go'] = "انطلق";
$lang['generate_payslip_for'] = "إنشاء قسيمة الدفع ل";
$lang['payroll_template_list'] = " قائمة نماذج المرتبات";
$lang['add_new_template'] = "إضافة نموذج جديد";
$lang['template_name'] = "اسم النموذج";
$lang['payment_for'] = "دفع ل";
$lang['overtime_salary'] = "راتب العمل الاضافي";
$lang['payment_amount'] = "دفع المبلغ";
$lang['payment_type'] = "نوع الدفع";
$lang['payment_month'] = "شهر الدفع";
$lang['payment_date'] = "تاريخ الدفع";
$lang['paid_amount'] = "المبلغ المدفوع";
$lang['payroll_template'] = "نماذج المرتبات";
$lang['fine_deduction'] = "خصم غرامة";
$lang['paylsip'] = "قسيمة الدفع";
$lang['salary_month'] = "راتب شهر";
$lang['payslip_no'] = "قسيمة الدفع رقم";
$lang['mobile'] = "المحمول";
$lang['payment_method'] = "طريقة الدفع";
$lang['payment_details'] = "بيانات الدفع";
$lang['earning'] = "كسب";
$lang['gross_income'] = "الدخل الإجمالي";
$lang['overtime_amount'] = "مبلغ العمل الاضافي";
$lang['award_amount'] = "مبلغ المكافآت";
$lang['total'] = "مجموع";
$lang['working_hour_amount'] = "مبلغ ساعات العمل";
$lang['salary_template_list'] = "قائمة نماذج الرواتب";
$lang['salary_template'] = "النماذج الراتب";
$lang['allowance'] = "التعويضات";
$lang['deduction'] = "اقتطاع";
$lang['salary_template_details'] = "تفاصيل نماذج الرواتب";
$lang['overtime_hour_amount'] = "مبلغ ساعات العمل الاضافي";

// Job Circular
$lang['job_circular'] = 'وظيفة دائرية';
$lang['jobs_posted'] = 'نشر وظيفة';
$lang['jobs_applications'] = 'طلبات عمل';

//training
$lang['all_training'] = "كل التدريبات";
$lang['training_page_header'] = "إدارة التدريب";
$lang['view_training'] = "عرض التدريب";
$lang['training_saved_message'] = "تم حفظ معلومات التدريب بنجاح!";
$lang['course_training'] = "درس / تدريب";
$lang['vendor'] = "بائع";
$lang['finish_date'] = "تاريخ الانتهاء";
$lang['training_cost'] = "كلفة التدريب";
$lang['terminated'] = "إنهاء";
$lang['not_concluded'] = "لم تنته";
$lang['satisfactory'] = "مرضي";
$lang['average'] = "متوسط";
$lang['poor'] = "ضعيف";
$lang['excellent'] = "ممتاز";
$lang['remarks'] = "ملاحظات";
$lang['training_details'] = "تفاصيل التدريب";
$lang['training_list'] = "قائمة التدريبات";
$lang['add_training'] = "إضافة التدريب";
$lang['cost'] = "كلفة";
$lang['performance'] = "الأداء";

//department
$lang['department_list'] = "قائمة الإدارات";
$lang['department_page_header'] = "تحرير الإدارات";
$lang['department_info_saved'] = "تم إضافة معلومات الإدارة بنجاح!";
$lang['department_info_deleted'] = "تم حذف معلومات الإدارة بنجاح!";
$lang['designation_info_used'] = "معلومات التعيين موجودة حاليا!";
$lang['designation_info_deleted'] = "تم إضافة معلومات التعيين بنجاح!";
$lang['add_department'] = "إضافة إدارة";
$lang['add_desingation'] = "إضافة قسم";


//employee
$lang['employee_list'] = "قائمة الموظفين";
$lang['employee_page_header'] = "إدارة الموظفين";
$lang['view_employee'] = "عرض معلومات الموظف";
$lang['employee_info_saved'] = "تم إضافة موظف بنجاح!";
$lang['employee_info_deleted'] = "تم حذف موظف بنجاح!";
$lang['add_employee'] = "إضافة موظف";
$lang['dept_desingation'] = "التعيين> قسم";
$lang['personal_details'] = "تفاصيل شخصية";
$lang['first_name'] = "الاسم";
$lang['last_name'] = "اللقب";
$lang['date_of_birth'] = "تاريخ الميلاد";
$lang['gender'] = "الجنس";
$lang['maratial_status'] = "الحالة الاجتماعية";
$lang['male'] = "ذكر";
$lang['female'] = "أنثى";
$lang['married'] = "متزوج";
$lang['un-married'] = "اعزب";
$lang['widowed'] = "ارمل";
$lang['divorced'] = "مطلق";
$lang['fathers_name'] = "اسم الاب";
$lang['nationality'] = "الجنسية";
$lang['passport_no'] = "رقم جواز السفر";
$lang['photo'] = "صورة";
$lang['contact_details'] = "تفاصيل العقد";
$lang['present_address'] = "العنوان الحالي";
$lang['employee_document'] = "وثائق الموظفين";
$lang['resume'] = "سيرة ذاتية";
$lang['offer_letter'] = "رسالة العرض";
$lang['joining_letter'] = "رسالة الانضمام";
$lang['contract_paper'] = "ورقة العقد";
$lang['id_proff'] = "ID Proff";
$lang['other_documents'] = "وثائق أخرى";
$lang['bank_information'] = "معلومات عن البنك ";
$lang['bank_name'] = "اسم البنك";
$lang['branch_name'] = "اسم الفرع";
$lang['account_name'] = "إسم الحساب";
$lang['account_number'] = "رقم الحساب";
$lang['official_status'] = "الصفة الرسمية";
$lang['inactive'] = "غير فعال";
$lang['active'] = "فعال";
$lang['in_test'] = "في تجربة";
$lang['test_period'] = "فترة التجربة";



//settings
$lang['general_settings'] = 'الاعدادات العامة';
$lang['settings_management'] = 'تحرير الإعدادات';
$lang['general_settings_saved'] = 'تم تحديث معلومات الشركة بنجاح!';
$lang['set_working_days'] = 'تحديد أيام العمل';
$lang['working_dasy_saved'] = 'تم تحديث  أيام العمل بنجاح!';
$lang['working_hour_saved'] = 'تم تحديث ساعات العمل بنجاح!';
$lang['holiday_list'] = 'قائمة الإجازات';
$lang['new_holiday'] = 'إجازة جديدة';
$lang['holiday_information_exist'] = ' معلومات الإجازة موجودة';
$lang['holiday_information_saved'] = 'تم تحديث معلومات الإجازة بنجاح!';
$lang['holoday_information_delete'] = 'تم حذف معلومات الإجازة بنجاح!';
$lang['leave_application'] = 'مطلب إجازة';
$lang['notification_error_mailbox'] = 'لا يمكن تغيير وضع الإشعار. لم يصل اي بريد إلكتروني !';
$lang['notification_error_mailbox'] = 'لا يمكن تغيير وضع الإشعار. الرجاء إضافة إشعار!';
$lang['notification_error_notice'] = 'لا يمكن تغيير وضع الإشعار. لا توجد إشعارات!';
$lang['notification_error_application'] = 'لا يمكن تغيير وضع الإشعار. لا توجد قائمة التطبيقات!';
$lang['notification_error_clock'] = 'لا يمكن تغيير وضع الإشعار. الطلب دون تغيير!';
$lang['notification_saved'] = 'تم تحديث حالة الإشعار بنجاح!';
$lang['profile_information_updated'] = '.تم تحديث المعلومات بنجاح.الرجاء الخروج لمعاينة التحديثات';
$lang['password_updated'] = 'تم تحديث كلمة المروربنجاح.الرجاء الخروج لإستعمال كلمة المرور الجديدة';
$lang['view_personal_event'] = 'عرض الحدث الشخصي';
$lang['personal_event_saved'] = '!تم حفظ الحدث بنجاح';
$lang['personal_event_deleted'] = '!تم حذف الحدث بنجاح';
$lang['language_added_successfully'] = '!تم  إضافة معلومات اللغة بنجاح';
$lang['language_active_successfully'] = '!تم تفعيل معلومات اللغة بنجاح';
$lang['language_deactive_successfully'] = '!تم تعطيل معلومات اللغة بنجاح';
$lang['update_information'] = '!تم تحديث المعلومات بنجاح';
$lang['company_name'] = 'اسم الشركة';
$lang['company_logo'] = 'شعار الشركة';
$lang['remove'] = 'إزالة';
$lang['email_address'] = 'عنوان البريد الإلكتروني';
$lang['company_address'] = 'عنوان الشركة';
$lang['city'] = 'مدينة';
$lang['country'] = 'دولة';
$lang['select_country'] = 'حدد الدولة';
$lang['phone'] = 'رقم الهاتف ';
$lang['hoteline'] = 'الخط الساخن';
$lang['web_address'] = 'عنوان موقع الويب';
$lang['currency'] = 'عملة';
$lang['select_currency'] = 'اختر العملة';
$lang['set_timezone'] = 'تعيين المنطقة الزمنية';
$lang['select_timezone'] = 'اختر المنطقة الزمنية';
$lang['start_hours'] = 'بدء الساعات';
$lang['end_hours'] = 'نهاية الساعات';
$lang['all_categary'] = 'جميع التصنيفات';
$lang['category_name'] = 'اسم التصنيف';
$lang['email'] = 'البريد الإلكتروني';
$lang['time_changes_request'] = 'طلب تغيير الوقت';
$lang['add_translation'] = 'إضافة الترجمة';
$lang['translations'] = 'الترجمات';
$lang['icon'] = 'أيقونة';
$lang['language'] = 'لغة';
$lang['languages'] = 'اللغات';
$lang['done'] = 'منجز';
$lang['edit'] = 'تعديل';
$lang['activate'] = 'نشط';
$lang['deactivate'] = 'إلغاء';
$lang['save_translation'] = 'حفظ الترجمة';
$lang['update_profile'] = 'تحديث الملف';
$lang['changes_password'] = 'تغيير كلمة السر';
$lang['old_password'] = 'كلمة المرور القديمة';
$lang['new_password'] = 'كلمة السر الجديدة';
$lang['confirm_password'] = 'تأكيد كلمة المرور';

// user profile

$lang['you_have'] = 'عندك';
$lang['messages'] = 'رسائل';
$lang['view_all'] = 'عرض الكل';
$lang['application'] = 'تطبيق';
$lang['request'] = 'طلب';
$lang['username'] = 'اسم المستخدم';
$lang['profile'] = 'الملف الشخصي';
$lang['sign_out'] = 'خروج';
$lang['mark_completed'] = 'علامة كاملة';
$lang['mark_incomplete'] = 'علامة غير كاملة';
$lang['move_up'] = 'انتقال لأعلى';
$lang['move_down'] = 'انتقال لأسفل';
$lang['delete'] = 'حذف';
$lang['add'] = 'إضافة';
$lang['view'] = 'عرض';

// employee panel
$lang['leave_application_submitted'] = "إضافة  طلب إجازة بنجاح";
$lang['fill_up_all_fields'] = "Please Fill up all required fields to apply ";
$lang['password_do_not_match'] = "Your Entered Password Do Not Match !";
$lang['home'] = "Home";
$lang['my_time'] = "My Time";
$lang['logout'] = "Logout";
$lang['my_expense'] = "My Expense";
$lang['leave'] = "Leave";
$lang['dob'] = "DOB";
$lang['address'] = "Address";
$lang['notice_board'] = "Notice Board";
$lang['clock_in'] = "Clock In";
$lang['clock_out'] = "Clock Out";
$lang['set_reason'] = "Set Reason";
$lang['clock_out_reason'] = "Clock Out Reason";
$lang['birthdays'] = "Birthdays";
$lang['recent_mails'] = "Recent Mails";
$lang['upcoming_events'] = "Upcoming Events";
$lang['award_date'] = "Award Date";
$lang['list_of_all_awrds'] = "List of All Awards Received";
$lang['received'] = "Received";
$lang['list_of_all_notice'] = "List of All Notices";
$lang['award_details'] = "Award Details";
$lang['edit_my_time_logs'] = "Edit My Time Logs";
$lang['reason_for_edit'] = "Reason for Edit";
$lang['request_update'] = "Request Update";
$lang['all_leave'] = "All Leave";
$lang['new_leave'] = "New Leave";
$lang['leave_application_you_applied'] = "Leave Applications You Applied";
$lang['submit'] = "Submit";
$lang['all_leave_details'] = "All Leave Details";
$lang['event_details'] = "Event Details";
$lang['list_of_all_event'] = "List of All Events";
$lang['amount_spent'] = "Amount Spent";
$lang['my_time_log'] = "My Time logs";
$lang['payment_history'] = "Payment History";
$lang['your_personal_profile'] = "الملف الشخصي الخاص بك";
$lang['print_payslip'] = "Print Payslip";
$lang['director'] = "Director";
$lang['accountant'] = "Accountant";
$lang['go_back'] = "عودة";

//new
$lang['yes'] = "نعم";
$lang['no'] = "لا";
$lang['employee_name'] = "إسم الموظف";
$lang['administrator'] = "المدير";
$lang['notified'] = "إخطار";
$lang['js_confirm_message'] = "هل تريد حقا محو هذه البيانات ؟";
$lang['name_ar'] = "الإسم بالعربية";
$lang['name_en'] = "الإسم بالإنجليزية";
$lang['description_ar'] = "الوصف بالعربية";
$lang['description_en'] = "الوصف بالإنجليزية";
$lang['saved_successfully'] = "تم حفظ البيانات بنجاح";
$lang['deleted_successfully'] = "تم محو البيانات بنجاح";

// Departements
$lang['departement'] = "الإدارة";
$lang['department_id'] = "معرف الإدارة";
$lang['department_name_ar'] = "إسم الإدارة بالعربية";
$lang['department_name_en'] = "إسم الإدارة بالإنجليزية";
$lang['add_department_admin'] = "إسم المدير";
$lang['add_department_admin_rep'] = "إسم بديل المدير";
$lang['rep'] = "البديل";

// Designations
$lang['designations_admin'] = "مدير القسم";
$lang['select_departement'] = "حدد الإدارة";
$lang['designations_list'] = "قائمة الأقسام";
$lang['add_designation'] = "إضافة قسم";
$lang['designation_name'] = "إسم القسم";
$lang['designation_emp'] = "الموظف";


// Job Titles
$lang['job_title'] = 'المسمى الوظيفي';
$lang['job_titles_list'] = "قائمة المسميات الوظيفية";
$lang['add_job_title'] = "إضافة مسمى وظيفي";
$lang['job_title_ar'] = 'المسمى (بالعربية)';
$lang['job_title_en'] = 'المسمى (بالإنجليزية)';
$lang['job_title_used'] = 'الإسم الوظيفي مستعمل';



//employee
$lang['employee_list'] = "قائمة الموظفين";
$lang['employee_page_header'] = "إدارة الموظفين";
$lang['view_employee'] = "عرض معلومات الموظف";
$lang['employee_info_saved'] = "تم إضافة موظف بنجاح!";
$lang['employee_info_updated'] = "تم تعديل معلومات الموظف بنجاح!";
$lang['employee_info_deleted'] = "تم حذف موظف بنجاح!";
$lang['add_employee'] = "إضافة موظف";
$lang['dept_desingation'] = "التعيين> قسم";
$lang['personal_details'] = "تفاصيل شخصية";
$lang['full_name_ar'] = "الإسم الكامل-عربي";
$lang['full_name_en'] = "الإسم الكامل-إنجليزي";
$lang['date_of_birth'] = "تاريخ الميلاد";
$lang['gender'] = "الجنس";
$lang['maratial_status'] = "الحالة الاجتماعية";
$lang['male'] = "ذكر";
$lang['female'] = "أنثى";
$lang['married'] = "متزوج";
$lang['un-married'] = "اعزب";
$lang['widowed'] = "ارمل";
$lang['divorced'] = "مطلق";
$lang['fathers_name'] = "اسم الاب";
$lang['nationality'] = "الجنسية";
$lang['passport_no'] = "رقم جواز السفر";
$lang['passport_end'] = "تاريخ نهاية جواز السفر";
$lang['identity_no'] = "رقم الهوية";
$lang['identity_end'] = "تاريخ نهاية الهوية";
$lang['photo'] = "صورة";
$lang['contact_details'] = "تفاصيل العقد";
$lang['present_address'] = "العنوان الحالي";
$lang['employee_document'] = "وثائق الموظفين";
$lang['resume'] = "سيرة ذاتية";
$lang['offer_letter'] = "رسالة العرض";
$lang['joining_letter'] = "رسالة الانضمام";
$lang['contract_paper'] = "ورقة العقد";
$lang['id_proff'] = "ID Proff";
$lang['other_documents'] = "وثائق أخرى";
$lang['bank_information'] = "معلومات عن البنك ";
$lang['bank_name'] = "اسم البنك";
$lang['branch_name'] = "اسم الفرع";
$lang['account_name'] = "إسم الحساب";
$lang['account_number'] = "رقم الحساب";
$lang['official_status'] = "الصفة الرسمية";
$lang['inactive'] = "غير فعال";
$lang['dependent'] = "تابع";
$lang['wife_name'] = "اسم الزوجة (إنجليزي)";
$lang['wife_name_ar'] = "اسم الزوجة (عربي)";
$lang['wife_birth'] = "تاريخ ميلاد الزوجة";
$lang['fils_name'] = "اسم االإبن(إنجليزي)";
$lang['fils_name_ar'] = "اسم االإبن(عربي)";
$lang['fils_birth'] = "تاريخ ميلاد الابن";
$lang['employee_type'] = "فئة الموظف";
$lang['select_type'] = "إختر فئة";
$lang['job_time'] = "نوع الدوام";
$lang['job_full'] = "كامل";
$lang['job_part'] = "جزئي";
$lang['job_time_select'] = "إختر نوع  الدوام";
$lang['insurance'] = "معلومات التأمين";
$lang['medical_insur'] = "التأمين الطبي";
$lang['medical_insur_yes'] = "نعم";
$lang['medical_insur_no'] = "لا";
$lang['medical_insur_select'] = " إختر";
$lang['medical_insur_type'] = "نوع التأمين الطبي";
$lang['medical_insur_type_select'] = "إختر نوع التأمين الطبي";
$lang['medical_insur_type_all'] = "كامل المبلغ";
$lang['medical_insur_type_part'] = "جزء من المبلغ";
$lang['social_insur'] = "التأمينات الإجتماعية";
$lang['social_insur_yes'] = "نعم";
$lang['social_insur_no'] = "لا";
$lang['social_insur_select'] = "إختر";
$lang['social_insur_type'] = "نوع التأمين الاجتماعي";
$lang['social_insur_type_select'] = "إختر نوع التأمين الاجتماعي";
$lang['saudi_social_insur'] = "سعودي قابل للخصم";
$lang['no_saudi_social_insur'] = "سعودي غير قابل للخصم";
$lang['no_social_insur'] = "غير سعودي";
$lang['education'] = "المؤهل العلمي";
$lang['diplome'] = "الشهادة";
$lang['job_place'] = "موقع العمل";
$lang['holiday_no'] = "عدد أيام الإجازة المسموح بها خلال السنة التعاقدية";
$lang['reference'] = "الرقم الوظيفي";
$lang['employee_salary'] = "الراتب";
$lang['view_finance_details'] = "بيانات لمالية";
$lang['direct_boss'] = "المدير المباشر";

// Leaves in settings
$lang['leave_category_quota'] = 'نسبة الخصم من الراتب (%)';
$lang['leave_category_duration'] = 'مدة الإجازة (باليوم)';
$lang['leave_category_exist'] = 'معلومات صنف الإجازة موجودة!';
$lang['leave_category_saved'] = 'تم حفظ صنف الإجازة بنجاح';
$lang['leave_category_used'] = 'معلومات صنف الإجازة مستخدمة!';
$lang['leave_category_deleted'] = 'تم حذف صنف الإجازة بنجاح!';
$lang['leave_affect_stock'] = 'تؤثر على رصيد الإجازات';
$lang['leave_calculation_type'] = 'كيف تحسب الإجازة';
$lang['normal_year'] = 'السنة العادية';
$lang['contractual_year'] = 'السنة العقدية';
$lang['leave_tel'] = 'تحديد رقم هاتف';
$lang['set_replacement'] = 'تحديد موظف بديل';
$lang['paid_leave'] = 'إجازة مدفوعة الأجر';
$lang['allowance_cat_ids'] = 'العلاوات الداخلة في بدل الإجازة';

// Irrigularity_category
$lang['irrigularity_category'] = 'تصنيف المخالفات';
$lang['first'] = 'المرة الأولى';
$lang['second'] = 'المرة الثانية';
$lang['third'] = 'المرة الثالثة';
$lang['fourth'] = 'المرة الرابعة';
$lang['first_ar'] = 'المرة الأولى (بالعربية)';
$lang['second_ar'] = 'المرة الثانية (بالعربية)';
$lang['third_ar'] = 'المرة الثالثة (بالعربية)';
$lang['fourth_ar'] = 'المرة الرابعة (بالعربية)';
$lang['first_en'] = 'المرة الأولى (بالإنجليزية)';
$lang['second_en'] = 'المرة الثانية (بالإنجليزية)';
$lang['third_en'] = 'المرة الثالثة (بالإنجليزية)';
$lang['fourth_en'] = 'الرة الرابعة (بالإنجليزية)';
$lang['penalty'] = 'الجزاءات';
$lang['irrigularity_category_used'] = 'معلومات صنف المخالفة مستخدمة';
$lang['irrigularity_category_deleted'] = 'تم حذف صنف المخالفة بنجاح';
$lang['job_places'] = 'أماكن العمل';



//Bonuses
$lang['bonuses_page_header'] = "أنواع البدلات";
$lang['bonuses_list'] = "تهيئة المعلومات المالية";
$lang['financial_information'] = "تهيئة المعلومات المالية";
$lang['bonuses_type'] = "أنواع البدلات";
$lang['bonuse_list'] = "قائمة البدلات";
$lang['add_bonuse'] = "إضافة بدل";
$lang['bonuse_title_ar'] = "وصف (عربي)";
$lang['bonuse_title_en'] = "وصف (إنجليزي)";
$lang['bonuse_assurance'] = "تأمين";
$lang['bonuse_guarantee'] = "ضمان";
$lang['bonuse_departure'] = "مغادرة";
$lang['bonuse_yes'] = "نعم";
$lang['bonuse_no'] = "لا";
$lang['brief_en'] = "مختصر(إنجليزية)";
$lang['brief_ar'] = "مختصر(عربي)";
$lang['select_bonus'] = "أختر";

//Extra work

$lang['extra_page_header'] = "العمل الإضافي ";
$lang['extra_work'] = "العمل الإضافي ";
$lang['extra_list'] = "قائمة الأعمال الإضافية";
$lang['add_extra'] = "إضافة عمل إضافي";
$lang['extra_title_ar'] = "وصف (عربي)";
$lang['extra_title_en'] = "وصف (إنجليزي)";
$lang['work_value'] = "قيمة الساعة";
$lang['extra_title'] = "عمل إضافي";

//Financial deduction
$lang['deduction_page_header'] = "اقتطاعات مالية";
$lang['deduction_title'] = "اقتطاعات مالية";
$lang['deduction_type'] = "نوع الإقتطاع";
$lang['deduction_list'] = "قائمة الاقتطاع المالي";
$lang['add_deduction'] = "إضافة إقتطاع المالي";
$lang['deduction_title_en'] = "وصف (إنجليزي)";
$lang['deduction_title_ar'] = "وصف (عربي)";
$lang['deduction_type'] = "نوع الإقتطاع";
$lang['deduction_type_percent'] = "نسبة مئوية ";
$lang['deduction_type_value'] = "مبلغ ";
$lang['deduction_value'] = "قيمة الإقتطاع";

//Others provisions
$lang['provision_page_header'] = "مخصصات أخرى";
$lang['other_provisions'] = "مخصصات أخرى";
$lang['provision_title'] = "مخصصات أخرى";
$lang['provision_list'] = "قائمة المخصصات أخرى";
$lang['add_provision'] = "إضافة مخصصات أخرى";
$lang['provision_title_en'] = "وصف (إنجليزي)";
$lang['provision_title_ar'] = "وصف (عربي)";

//Types of advances
$lang['advance_page_header'] = "أنواع السلف";
$lang['advances'] = "أنواع السلف";
$lang['advance_title'] = "أنواع السلف";
$lang['advance_list'] = "قائمة أنواع السلف";
$lang['add_advance'] = "إضافة أنواع السلف";
$lang['advance_title_en'] = "وصف (إنجليزي)";
$lang['advance_title_ar'] = "وصف (عربي)";

//Financial allowance
$lang['allowance_page_header'] = "البدلات";
$lang['allowance_title'] = "البدلات";
$lang['allowances'] = "البدلات";
$lang['allowance_type'] = "البدلات";
$lang['allowance_list'] = "قائمة البدلات";
$lang['add_allowance'] = "إضافة بدل";
$lang['allowance_title_en'] = "وصف (إنجليزي)";
$lang['allowance_title_ar'] = "وصف (عربي)";
$lang['allowance_type'] = "نوع البدل";
$lang['allowance_type_percent'] = "نسبة مئوية";
$lang['allowance_type_value'] = "مبلغ";
$lang['allowance_value'] = "قيمة البدل";


//Discount 
$lang['discount_page_header'] = "الخصومات ";
$lang['discount_title'] = "الخصومات ";
$lang['discounts'] = "الخصومات ";
$lang['discount_type'] = "الخصومات ";
$lang['discount_list'] = "قائمة الخصومات ";
$lang['add_discount'] = "إضافة خصومات ";
$lang['discount_title_en'] = "وصف (إنجليزي)";
$lang['discount_title_ar'] = "وصف (عربي)";
$lang['discount_type'] = "نوع الخصومات";
$lang['discount_type_percent'] = "نسبة مئوية";
$lang['discount_type_value'] = "مبلغ";
$lang['discount_value'] = "قيمة الخصم";

//insurance information
$lang['insurance_information'] = "تهيئة التأمينات المختلفة";
//medical insurance
$lang['medical_page_header'] = "التأمين الطبي";
$lang['medical_insurance'] = "التأمين الطبي";
$lang['medical_title'] = "التأمين الطبي";
$lang['medical_list'] = "قائمة التأمينات الطبية";
$lang['add_medical'] = "إضافة التأمين الطبي";
$lang['medical_title_en'] = "وصف(إنجليزي)";
$lang['medical_title_ar'] = "وصف(عربي)";
$lang['medical_brief_en'] = "مختصر(إنجليزي)";
$lang['medical_brief_ar'] = "مختصر(عربي)";

//Social insurance
$lang['social_page_header'] = "الضمان الإجتماعي";
$lang['social_insurance'] = "الضمان الإجتماعي";
$lang['social_title'] = "الضمان الإجتماعي";
$lang['social_list'] = "قائمة التأمينات الاجتماعية";
$lang['add_social'] = "إضافة التأمين الاجتماعي";
$lang['social_title_en'] = "وصف(إنجليزي)";
$lang['social_title_ar'] = "وصف(عربي)";
$lang['social_brief_en'] = "مختصر(إنجليزي)";
$lang['social_brief_ar'] = "مختصر(عربي)";

// Social details
$lang['insurance_percent'] = "نسبة الضمان";
$lang['societe_percent'] = "نسبة المنشأة";
$lang['nb_days'] = "أيام حساب الضمان";
$lang['age_male'] = "عمر الضمان للذكور";
$lang['age_female'] = "عمر الضمان للإناث";
$lang['salary_insurance'] = "زيادة  راتب الضمان الإجتماعي تلقائيا";
$lang['habit_insurance'] = "شمول مستحقات السكن عند الحساب";
$lang['insurance_value'] = "قيمة الضمان الإجتماعي نسبة من عدد الايام";
$lang['max_salary'] = "الحد الاعلى لراتب الضمان الإجتماعي";
$lang['min_salary'] = "الحد الإدنى لراتب الضمان الإجتماعي";
$lang['social_insurance_yes'] = "نعم";
$lang['social_insurance_no'] = "لا";


// Custody details
$lang['custody_title'] = "العهدة";
$lang['custody_name_ar'] = "الاسم(عربي)";
$lang['custody_name_en'] = "الاسم(انجليزي)";
$lang['custody_description_ar'] = "وصف(عربي)";
$lang['custody_description_en'] = "وصف(إنجليزي)";
$lang['custody_nombre'] = "العدد";
$lang['custody_delivery_date'] = "تاريخ التسليم";
$lang['custody_reference'] = "رقم العهدة";
$lang['custody_document'] = "إضافة ملف";

// employee category
$lang['all_categary_emp'] = "جميع الفئات";
$lang['add_emp'] = "فئة جديدة";

// leave date
$lang['start_leave'] = "إجازتك المقبلة تبدأ خلال";
$lang['no_leave'] = "ليست لديك إجازات في الوقت الحالي";
$lang['loan_application_you_applied'] = "كل الإجازات التي طلبتها";

//loan employee panel
$lang['laon_categories'] = "تصنيف السلفات";
$lang['all_loan'] = "السلفات";
$lang['new_loan']="سلفة جديدة";
$lang['loan_application'] = "مطلب سلفة ";
$lang['loan_application_you_applied'] = "كل السلفات التي طلبتها";
$lang['loan_category'] = "نوع السلفة";
$lang['loan_amount'] = "قيمة السلفة";
$lang['loan_month'] = "عدد الاشهر";
$lang['loan_amount_month'] = "عدد الاشهر";
$lang['loan_application_submitted'] = "إضافة  طلب سلفة بنجاح";

//status
$lang['pending'] = "في الإنتظار";
$lang['approved'] = "موافق عليه";
$lang['rejcted'] = "مرفوض";


$lang['applications'] = "طلبات";

// Cashing application
$lang['all_cashing'] = "صرف";
$lang['new_cashing'] = "صرف جديد";
$lang['cashing_application'] = "صرف";
$lang['cashing_category'] = "فئة الصرف";
$lang['cashing_full_name'] = "الاسم الكامل";
$lang['cashing_phone'] = "المحمول";
$lang['cashing_address'] = "العنوان";
$lang['cashing_bank_name'] = "اسم البنك";
$lang['cashing_branch_name'] = "اسم الفرع";
$lang['cashing_rib'] = "رقم الحساب";
$lang['cashing_amount'] = "المبلغ";
$lang['cashing_purpose'] = "الغرض";
$lang['cashing_purpose_ar'] = "الغرض (عربي)";
$lang['cashing_purpose_en'] = "الغرض (إنجليزي)";
$lang['cashing_description_ar'] = "وصف(عربي)";
$lang['cashing_description_en'] = "وصف(إنجليزي)";
$lang['cashing_application_submitted'] = "إضافة  طلب صرف بنجاح";
$lang['cashing_invoiced'] = "مفوتر";
$lang['cashing_non_invoiced'] = "غير مفوتر";

//device 
$lang['device'] = "ريال";

//Permission application
$lang['all_permission'] = "كل طلبات الإستئذان ";
$lang['new_permission'] = "طلب إستئذان جديد";
$lang['permission_application'] = "إستئذان";
$lang['permission_category'] = "فئة الإستئذان";
$lang['permission_type_retard'] = "التأخر عن الحضور";
$lang['permission_type_leaving'] = "الإنصراف باكرا";
$lang['permission_type_exit'] = "الخروج وقت الدوام";
$lang['permission_date'] = "التاريخ";


//Finance data
$lang['finance_employee'] = "بيانات مالية";
$lang['finance_bonus'] = "رمز البدل";
$lang['finance_value'] = "قيمة البدل";
$lang['finance_from'] = "من";
$lang['finance_to'] = "الى";
$lang['finance_select'] = "اختر نوع بدل...";
$lang['finance_type'] = "نوع البدل";
$lang['finance_add_allowance'] = "أضف البدل";
$lang['finance_basic_salary'] = "الراتب الاساسي";
$lang['finance_all_salary'] = "الراتب الإجمالي";
$lang['finance_reservation'] = "حجز";

//Finance other provision data
$lang['finance_provision'] = "إمتيازات";
$lang['finance_provision_bonus'] = "نوع الامتياز";
$lang['finance_provision_value'] = "قيمة الامتياز";
$lang['finance_provision_description'] = "وصف";
$lang['finance_provision_type'] = "نوع الامتياز";
$lang['finance_provision_description_ar'] = "وصف(عربي)";
$lang['finance_provision_description_en'] = "وصف(إنجليزي)";
$lang['finance_provision_select'] = "اختر نوع الامتياز...";

//Finance deduction data
$lang['finance_permanent_deduction'] = "إقتطاعات";
$lang['finance_deduction'] = "إقتطاعات";
$lang['finance_deduction_bonus'] = "اسم الاقتطاع";
$lang['finance_deduction_value'] = "قيمة الاقتطاع";
$lang['finance_deduction_description'] = "وصف";
$lang['finance_deduction_type'] = "نوع الاقتطاع";
$lang['finance_deduction_description_ar'] = "وصف(عربي)";
$lang['finance_deduction_description_en'] = "وصف(إنجليزي)";
$lang['finance_deduction_select'] = "اختر نوع الاقتطاع...";

$lang['finance_houcing'] = "مستحقات السكن";
$lang['finance_status'] = "الموظف خاضع لمستحقات السكن";
$lang['finance_date'] = "تاريخ البدء";
$lang['finance_count'] = "تحسب الحركة";
$lang['finance_value'] = "قيمة البدل";
$lang['finance_repetition'] = "تكرار علاوة السكن تلقائيا كل";
$lang['finance_fixed'] = "ثابتة";
$lang['finance_percent'] = "كنسبة من الراتب";

//Financial Fieldset
$lang['fieldset_bank'] = "طريقة الدفع";
$lang['fieldset_med'] = "التأمين الطبي";
$lang['fieldset_social'] = "التأمينات الاجتماعية";
$lang['fieldset_other'] = "أخرى";

//Financial payement
$lang['payment_method'] = "طريقة الدفع";
$lang['payment_account'] = "رقم الحساب";
$lang['payment_bank'] = "اسم البنك";
$lang['payment_branch'] = "اسم الفرع";

//Financial medical
$lang['medical_yes'] = "خاضع للتامين";
$lang['medical_type'] = "نوع التامين";
$lang['medical_date'] = "تاريخ بدء التامين";
$lang['medical_end_date'] = "تاريخ انتهاء بطاقة التامين";

//Financial social
$lang['social_yes'] = "نوع التأمينات الاجتماعية";
$lang['social_type'] = "نوع التأمينات الاجتماعية";
$lang['social_date'] = "تاريخ بدء التأمينات الاجتماعية";
$lang['social_salary'] = "راتب الضمان";

// Financial Options
$lang['finance_option'] = "خيارات مالية";
$lang['option_end_service'] = "مكافأة نهاية الخدمة";
$lang['option_date'] = "بتاريخ";
$lang['option_fix'] = "مثبت";
$lang['option_suspend'] = "موقوف";
$lang['retrait_date'] = "تاريخ التقاعد ";
$lang['option_stop_salary'] = "ثاريخ ايقاف الراتب";
$lang['option_accounting_1'] = "الرقم المحاسبي 1";
$lang['option_accounting_2'] = "الرقم المحاسبي 2";
$lang['option_description_ar'] = "وصف(عربي)";
$lang['option_description_en'] = "وصف(إنجليزي)";

//Automatic reminders
$lang['automatic_reminder'] = "تهيئة التذكير التلقائي";
$lang['reminder_hrm_employee'] = "تذكير مسؤول الموارد البشرية قبل";
$lang['reminder_dm_employee'] = "تذكير المدير المباشر قبل";
$lang['reminder_language'] = "لغة التذكير";
$lang['reminder_language_select'] = "اختر اللغة";
$lang['reminder_number_select'] = "اختر عدد";
$lang['reminder_test_period'] = "أيام على إنتهاء فترة تجربة الموظف";
$lang['reminder_identity_end'] = "أيام على إنتهاء صلاحية بطاقة الهوية";
$lang['reminder_passport_end'] = "أيام على إنتهاء صلاحية جواز السفر";
$lang['reminder_med_insurance_end'] = "أيام على انتهاء صلاحية بطاقات التامين";
$lang['reminder_contract_end'] = "أيام على إنتهاء العقد";
$lang['reminder_mail'] = "إرسال بريد تذكيري أيضا للموظفين عند تذكير مسؤول الموارد البشرية بوثائقهم المنتهية";
$lang['reminder_employee_fixed'] = "تحويل الموظف تلقائيا إلى مثبت بعد إنتهاء فترة التجربة";

$lang['general_items'] = "البنود العامة";