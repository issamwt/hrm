<?php

class Employee_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');

        $lang = $this->session->userdata("lang") == null ? "english" : $this->session->userdata("lang");
        $this->session->set_userdata('lang', $lang);
        $this->lang->load('employee', $lang);
        $this->lang->load('app', $lang);

        $this->get_notices();
        $this->get_aps();
        $this->get_inbox();

        $user_type = $this->session->userdata('user_type');
        if ($user_type != 2) {
            $url = $this->session->userdata('url');
            redirect($url);
        }
    }

    public function get_aps(){
        $result = $this->db->select("application_id, current, ids")->from('tbl_applications')->where('status','0')->get()->result();

        $array = [];
        foreach ($result as $r){
            if(explode(';',$r->ids)[$r->current]==$this->session->userdata('employee_id'))
                array_push($array,explode(';',$r->ids)[$r->current]);
        }
        $this->session->set_userdata('my_app_count', count($array));
    }
public function get_inbox(){
        $result = $this->db->select("to")->from('tbl_inbox')->where('view_status','2')->get()->result();
$email = $this->db->select("email")->from('tbl_employee')->where('employee_id',$this->session->userdata('employee_id'))->get()->row();
//echo '<pre>';print_r($result); print_r($email->email);echo'</pre>';

        $array = [];
        foreach ($result as $r){
            if($r->to==$email->email){
 array_push($array,$r->to);
        }}

        $this->session->set_userdata('my_inbox_count', count($array));
    }

    public function get_notices(){
        $result1 = $this->db->select("notice_id")->from("tbl_notice")->where('send_to', $this->session->userdata('employee_id'))->where('view_status', '2')->where('to_all', '0')->get()->result();
        $result2 = $this->db->select("notice_id")->from("tbl_notice")->where('send_to', $this->session->userdata('employee_id'))->where('view_status', '2')->where('to_all', '1')->get()->result();

        $this->session->set_userdata('my_notices_count', count($result1));
        $this->session->set_userdata('all_notices_count', count($result2));
    }

}
