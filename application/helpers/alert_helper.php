<?php
/**
 * -------------------------------------------------------------------
 * Developed and maintained by Zaman
 * -------------------------------------------------------------------
 */



if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('message_box')) {
    function message_box($message_type, $close_button = TRUE)
    {
        $CI =& get_instance();
        $message = $CI->session->flashdata($message_type);
        $retval = '';

        if($message){
            switch($message_type){
                case 'success':
                    $retval .= '<div class="alert alert-success">';
                    break;
                case 'error':
                    $retval .= '<div class="alert alert-danger">';
                    break;
                case 'info':
                    $retval .= '<div class="alert alert-info">';
                    break;
                case 'warning':
                    $retval .= '<div class="alert alert-warning">';
                    break;
            }

            if($close_button)
                $retval .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';

            $retval .= $message;
            $retval .= '</div>';
            return $retval;
        }
    }
}

if (!function_exists('set_message')){
    function set_message($type, $message)
    {
        $CI =& get_instance();
        $CI->session->set_flashdata($type, $message);
    }
}


if (!function_exists('hijjri')){
    function hijjri($date){
        if($date){
            echo '<div class="hijjri">'.Greg2Hijjri($date, true)."</div>";
        }
    }
}



function intparrt($float) {
    if ($float < -0.0000001)
        return ceil($float - 0.0000001);
    else
        return floor($float + 0.0000001);
}

function Greg2Hijjri($date, $string = false) {
    $date = explode(' ', $date);
    $dt = $date[0];
    $d = explode('-', $dt);

    $day = (int) $d[2];
    $month = (int) $d[1];
    $year = (int) $d[0];

    if (($year > 1582) or ( ($year == 1582) and ( $month > 10)) or ( ($year == 1582) and ( $month == 10) and ( $day > 14))) {
        $jd = intparrt((1461 * ($year + 4800 + intparrt(($month - 14) / 12))) / 4) + intparrt((367 * ($month - 2 - 12 * (intparrt(($month - 14) / 12)))) / 12) -
            intparrt((3 * (intparrt(($year + 4900 + intparrt(($month - 14) / 12) ) / 100) ) ) / 4) + $day - 32075;
    } else {
        $jd = 367 * $year - intparrt((7 * ($year + 5001 + intparrt(($month - 9) / 7))) / 4) + intparrt((275 * $month) / 9) + $day + 1729777;
    }

    $l = $jd - 1948440 + 10632;
    $n = intparrt(($l - 1) / 10631);
    $l = $l - 10631 * $n + 354;
    $j = (intparrt((10985 - $l) / 5316)) * (intparrt((50 * $l) / 17719)) + (intparrt($l / 5670)) * (intparrt((43 * $l) / 15238));
    $l = $l - (intparrt((30 - $j) / 15)) * (intparrt((17719 * $j) / 50)) - (intparrt($j / 16)) * (intparrt((15238 * $j) / 43)) + 29;

    $month = intparrt((24 * $l) / 709);
    $day = $l - intparrt((709 * $month) / 24);
    $year = 30 * $n + $j - 30;

    $date = array();
    $date['year'] = $year;
    $date['month'] = $month - 1;
    $date['day'] = $day + 1;
    $hijriMonth = array("محرم", "صفر", "ربيع الاول", "ربيع الثاني", "جمادى الاولى", "جمادى الثانية", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

    if (!$string)
        return $date;
    else {
        if (strlen($month) == 1)
            $month = '0' . $month;
        if (strlen($day) == 1)
            $day = '0' . $day;
        return $year . '-' . $month . '-' . $day;
    }
}
