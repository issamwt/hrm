<?php

/* sms templates */
$lang['sms_templates'] = 'SMS Templates';
$lang['new_app'] = 'New received application';
$lang['accepted_app_from'] = 'Application accepted from employee %s';
$lang['final_acception'] = 'Your Application was Accepted';
$lang['final_acception_of'] = 'The Application of %s was Accepted';
$lang['refuse_app_from'] = 'Your Application was refused from %s';
$lang['refuse_of_from'] = 'The Application of %s was Refused from %s';
$lang['delegate_of_from'] = 'Your Application was Delegated from %s to %s';
$lang['consultation_of_from'] = 'Your Application had a consultation from %s to %s';
$lang['pass_from'] = 'passing the Application from employee %s';

/* email templates */

$lang['email_templates'] = 'Email Templates';
$lang['choose_email'] = 'Choose Email';
