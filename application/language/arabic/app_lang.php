<?php

/* sms templates */
$lang['sms_templates'] = 'نماذج رسائل الجوال';
$lang['new_app'] = 'طلب وارد جديد';
$lang['accepted_app_from'] = 'موافقة على الطلب من الموظف %s';
$lang['final_acception'] = 'موافقة نهائية على طلبك';
$lang['final_acception_of'] = 'موافقة نهائية على طلب الموظف %s';
$lang['refuse_app_from'] = 'تم رفض طلبك من %s';
$lang['refuse_of_from'] = 'تم رفض طلب الموظف %s من قبل الموظف %s';
$lang['delegate_of_from'] = 'تم التفويض من الموظف %s إلى الموظف %s';
$lang['consultation_of_from'] = 'تم إرسال إستشارة من الموظف %s إلى الموظف %s';
$lang['pass_from'] = 'تمرير الطلب من الموظف %s';

/* email templates */

$lang['email_templates'] = 'نماذج رسائل البريد الإاكتروني';
$lang['choose_email'] = 'إختر نموذج الرسالة';