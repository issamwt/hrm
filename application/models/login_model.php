<?php

class Login_Model extends MY_Model
{

    protected $_table_name;
    protected $_order_by;
    public $rules = array(
        'user_name' => array(
            'field' => 'user_name',
            'label' => 'User Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    );

    public function login()
    {

        //check user type
        $this->_table_name = 'tbl_user';
        $this->_order_by = 'user_id';

        $admin = $this->get_by(array(
            'user_name' => $this->input->post('user_name'),
            'password' => $this->hash($this->input->post('password')),
        ), TRUE);
        if ($admin) {
            $data = array(
                'user_name' => $admin->user_name,
                'employee_id' => $admin->user_id,
                'employee_login_id' => $admin->user_id,
                'loggedin' => TRUE,
                // 'user_type' => 1,
                'user_flag' => $admin->flag,
                'url' => 'admin/dashboard',
                'user_name_en' => $admin->user_name_en,
                'user_name_ar' => $admin->user_name_ar,
            );
            $data['user_type'] = $admin->owner;
            // echo '<pre>'; print_r($data); exit();
            $this->session->set_userdata($data);
        } else {
            $this->_table_name = 'tbl_employee_login';
            $this->_order_by = 'employee_login_id';
            $employee = $this->get_by(array(
                'user_name' => $this->input->post('user_name'),
                'password' => $this->input->post('password')
            ), TRUE);





            if (count($employee)) {
                // Log in user
                $employee_id = $employee->employee_id;
                $this->_table_name = "tbl_employee"; //table name
                $this->_order_by = "employee_id";
                $user_info = $this->get_by(array('employee_id' => $employee_id), TRUE);

                if (empty($user_info) or $user_info->status!=1)
                    return;
                $data = $this->get_employee_type($user_info);
                $data['name'] = $employee->user_name;
                $data['email'] = $user_info->email;
                $data['employee_id'] = $employee->employee_id;
                //$data['user_name'] = $user_info->first_name . '  ' . $user_info->last_name;
                $data['user_name'] = 'employ';
                $data['photo'] = ($user_info->photo) ? $user_info->photo : 'img/admin.png';
                $data['employee_login_id'] = $employee->employee_login_id;
                $data['loggedin'] = TRUE;
                $data['user_type'] = 2;
                $data['url'] = 'employee/dashboard';

                $this->session->set_userdata($data);
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
    }

    public function loggedin()
    {
        return (bool)$this->session->userdata('loggedin');
    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function get_employee_type($user_info)
    {
        $this->_table_name = 'tbl_department';
        $this->_order_by = 'department_id';
        $deps = $this->get();

        $this->_table_name = 'tbl_designations';
        $this->_order_by = 'designations_id';
        $desig = $this->get();

        $data['emp_type'] = 'employee';

        foreach ($desig as $des) {
            if ($des->employee_id == $user_info->employee_id) {
                $data['emp_type'] = 'sec_manager';
                $data['my_designation_id'] = $des->designations_id;
                $data['my_designation_name_ar'] = $des->designations_ar;
                $data['my_designation_name_en'] = $des->designations;
            }
        }

        foreach ($deps as $dep) {
            if ($dep->employee_id == $user_info->employee_id) {
                $data['emp_type'] = 'dep_manager';
                $data['my_department_id'] = $dep->department_id;
                $data['my_department_name_ar'] = $dep->department_name_ar;
                $data['my_department_name_en'] = $dep->department_name;
            }
        }

        switch ($user_info->job_title) {
            case 1:
                $data['emp_type'] = 'super_manager';
                break;
            case 4:
                $data['emp_type'] = 'hr_manager';
                break;
            case 5:
                $data['emp_type'] = 'accountant';
                break;
        }

        return $data;
    }

}
