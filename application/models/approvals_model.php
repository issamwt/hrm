<?php

class Approvals_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function getdep() {
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->from('tbl_department');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getsec() {
        $this->db->select('tbl_designations.designations_id, tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->from('tbl_designations');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_dep_admins() {
        $this->db->select('dep.employee_id,  emp.full_name_ar, emp.full_name_en,jt.job_titles_name_ar,jt.job_titles_name_en');
        $this->db->from('tbl_department as dep');
        $this->db->join('tbl_employee as emp', 'emp.employee_id=dep.employee_id', 'left');
        $this->db->join('tbl_job_titles as jt', 'emp.job_title=jt.job_titles_id', 'left');
        $this->db->where('dep.employee_id !=', '');
        $this->db->where('emp.full_name_ar !=', '');
        $this->db->where('emp.full_name_en !=', '');
        $this->db->distinct();
        $this->db->group_by('dep.employee_id');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_des_admins() {
        $this->db->select('des.employee_id, emp.full_name_ar, emp.full_name_en,jt.job_titles_name_ar,jt.job_titles_name_en');
        $this->db->from('tbl_designations as des');
        $this->db->join('tbl_employee as emp', 'emp.employee_id=des.employee_id', 'left');
        $this->db->join('tbl_job_titles as jt', 'emp.job_title=jt.job_titles_id', 'left');
        $this->db->where('des.employee_id !=', '');
        $this->db->where('emp.full_name_ar !=', '');
        $this->db->where('emp.full_name_en !=', '');
        $this->db->distinct();
        $this->db->group_by('des.employee_id');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_emp_with_jib_titles() {
        $this->db->select('emp.employee_id,emp.full_name_en,emp.full_name_ar,jt.job_titles_name_en,jt.job_titles_name_ar');
        $this->db->from('tbl_employee as emp');
        $this->db->join('tbl_job_titles as jt', 'emp.job_title=jt.job_titles_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function employees_non_admins() {
        $this->db->select('emp.employee_id,emp.full_name_en,emp.full_name_ar,jt.job_titles_name_en,jt.job_titles_name_ar');
        $this->db->from('tbl_employee as emp');
        $this->db->join('tbl_job_titles as jt', 'emp.job_title=jt.job_titles_id', 'left');
        $this->db->where('emp.employee_id not in (select employee_id from tbl_department) AND emp.employee_id not in (select employee_id from tbl_designations) and emp.employee_id!=4');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_applications_by_id($id) {
        $this->db->select('tbl_employee.full_name_ar, tbl_employee.full_name_en, tbl_employee.employment_id');
        $this->db->select('tbl_applications.*');
        $this->db->select('tbl_approvals_cat.*');
        $this->db->from('tbl_applications');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id=tbl_applications.employee_id', "LEFT");
        $this->db->join('tbl_approvals_cat', 'tbl_approvals_cat.approval_cat_id=tbl_applications.approval_cat_id', "LEFT");
        $this->db->order_by('tbl_applications.application_id');
        $this->db->where('tbl_applications.application_id', $id);
        $result = $this->db->get()->row();
        return $result;
    }

    function intPart($float) {
        if ($float < -0.0000001)
            return ceil($float - 0.0000001);
        else
            return floor($float + 0.0000001);
    }

    function Greg2Hijri($date, $string = false) {
        $date = explode(' ', $date);
        $dt = $date[0];
        $d = explode('-', $dt);

        $day = (int) $d[2];
        $month = (int) $d[1];
        $year = (int) $d[0];

        if (($year > 1582) or ( ($year == 1582) and ( $month > 10)) or ( ($year == 1582) and ( $month == 10) and ( $day > 14))) {
            $jd = $this->intPart((1461 * ($year + 4800 + $this->intPart(($month - 14) / 12))) / 4) + $this->intPart((367 * ($month - 2 - 12 * ($this->intPart(($month - 14) / 12)))) / 12) -
                $this->intPart((3 * ($this->intPart(($year + 4900 + $this->intPart(($month - 14) / 12) ) / 100) ) ) / 4) + $day - 32075;
        } else {
            $jd = 367 * $year - $this->intPart((7 * ($year + 5001 + $this->intPart(($month - 9) / 7))) / 4) + $this->intPart((275 * $month) / 9) + $day + 1729777;
        }

        $l = $jd - 1948440 + 10632;
        $n = $this->intPart(($l - 1) / 10631);
        $l = $l - 10631 * $n + 354;
        $j = ($this->intPart((10985 - $l) / 5316)) * ($this->intPart((50 * $l) / 17719)) + ($this->intPart($l / 5670)) * ($this->intPart((43 * $l) / 15238));
        $l = $l - ($this->intPart((30 - $j) / 15)) * ($this->intPart((17719 * $j) / 50)) - ($this->intPart($j / 16)) * ($this->intPart((15238 * $j) / 43)) + 29;

        $month = $this->intPart((24 * $l) / 709);
        $day = $l - $this->intPart((709 * $month) / 24);
        $year = 30 * $n + $j - 30;

        $date = array();
        $date['year'] = $year;
        $date['month'] = $month - 1;
        $date['day'] = $day + 1;
        $hijriMonth = array("محرم", "صفر", "ربيع الاول", "ربيع الثاني", "جمادى الاولى", "جمادى الثانية", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

        if (!$string)
            return $date;
        else {
            if (strlen($month) == 1)
                $month = '0' . $month;
            return $year . '-' . $month . '-' . $day;
        }
    }

}
