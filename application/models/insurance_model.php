<?php

class Insurance_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;
    
     public function all_social_info($id = NULL) {
        $this->db->select('tbl_social_insurane.*', FALSE);
        $this->db->select('tbl_social_details.*', FALSE);
        $this->db->from('tbl_social_insurane');
        $this->db->join('tbl_social_details', 'tbl_social_details.social_id = tbl_social_insurane.social_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_social_insurane.social_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
       
        return $result;
    }

    
}