<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emp_model
 *
 * @author nayem
 */
class Emp_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;
    public function emplyee_name($id){
        if($this->session->userdata('lang')=='english')
            $this->db->select('full_name_en', FALSE);
        else
            $this->db->select('full_name_ar', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('employee_id', $id);
        $query_result = $this->db->get();

        if($this->session->userdata('lang')=='english')
            $result = $query_result->row()->full_name_en;
        else
            $result = $query_result->row()->full_name_ar;
        return $result;
    }

    public function all_emplyee_info($id = NULL, $all=true) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_social_insurane.social_id, tbl_social_insurane.social_flag, tbl_social_insurane.insurance_percent, tbl_social_insurane.societe_percent', FALSE);
        $this->db->select('tbl_medical_insurane.m_insurance_percent, tbl_medical_insurane.m_societe_percent', FALSE);
        $this->db->select('tpl_employee_category.*', FALSE);
        $this->db->select('tbl_employee_login.password', FALSE);
        $this->db->select('tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->select('countries.countryName, countries.countryName_ar', FALSE);
        $this->db->select('tbl_job_titles.*', FALSE);
        $this->db->select('tbl_job_places.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);

        $this->db->from('tbl_employee');

        $this->db->join('tbl_social_insurane', 'tbl_social_insurane.social_flag = tbl_employee.social_insurance_type', 'left');
        $this->db->join('tbl_medical_insurane', 'tbl_medical_insurane.medical_id = tbl_employee.med_insur_type', 'left');
        $this->db->join('tbl_employee_login', 'tbl_employee_login.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tpl_employee_category', 'tpl_employee_category.id  = tbl_employee.employee_category_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_employee.departement_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.nationality', 'left');
        $this->db->join('tbl_job_titles', 'tbl_job_titles.job_titles_id  = tbl_employee.job_title', 'left');
        $this->db->join('tbl_job_places', 'tbl_job_places.job_place_id  = tbl_employee.job_place_id', 'left');
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employef_id  = tbl_employee.employee_id', 'left');
        if(!$all)
            $this->db->where('tbl_employee.status !=',2);
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function all_emplyee_info_by_dep($id = NULL,$dep_id, $all=true) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_social_insurane.social_id, tbl_social_insurane.social_flag, tbl_social_insurane.insurance_percent, tbl_social_insurane.societe_percent', FALSE);
        $this->db->select('tbl_medical_insurane.m_insurance_percent, tbl_medical_insurane.m_societe_percent', FALSE);
        $this->db->select('tpl_employee_category.*', FALSE);
        $this->db->select('tbl_employee_login.password', FALSE);
        $this->db->select('tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->select('countries.countryName, countries.countryName_ar', FALSE);
        $this->db->select('tbl_job_titles.*', FALSE);
        $this->db->select('tbl_job_places.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);

        $this->db->from('tbl_employee');

        $this->db->join('tbl_social_insurane', 'tbl_social_insurane.social_flag = tbl_employee.social_insurance_type', 'left');
        $this->db->join('tbl_medical_insurane', 'tbl_medical_insurane.medical_id = tbl_employee.med_insur_type', 'left');
        $this->db->join('tbl_employee_login', 'tbl_employee_login.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tpl_employee_category', 'tpl_employee_category.id  = tbl_employee.employee_category_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_employee.departement_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.nationality', 'left');
        $this->db->join('tbl_job_titles', 'tbl_job_titles.job_titles_id  = tbl_employee.job_title', 'left');
        $this->db->join('tbl_job_places', 'tbl_job_places.job_place_id  = tbl_employee.job_place_id', 'left');
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employef_id  = tbl_employee.employee_id', 'left');
        if(!$all)
            $this->db->where('tbl_employee.status !=',2);
        $this->db->where('(tbl_employee.departement_id='.$dep_id.' OR tbl_employee.direct_manager_id='.$this->session->userdata("employee_id").')');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function all_emplyee_info_by_sec($id = NULL, $des_id, $all=true) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_social_insurane.social_id, tbl_social_insurane.social_flag, tbl_social_insurane.insurance_percent, tbl_social_insurane.societe_percent', FALSE);
        $this->db->select('tbl_medical_insurane.m_insurance_percent, tbl_medical_insurane.m_societe_percent', FALSE);
        $this->db->select('tpl_employee_category.*', FALSE);
        $this->db->select('tbl_employee_login.password', FALSE);
        $this->db->select('tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->select('countries.countryName, countries.countryName_ar', FALSE);
        $this->db->select('tbl_job_titles.*', FALSE);
        $this->db->select('tbl_job_places.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);

        $this->db->from('tbl_employee');

        $this->db->join('tbl_social_insurane', 'tbl_social_insurane.social_flag = tbl_employee.social_insurance_type', 'left');
        $this->db->join('tbl_medical_insurane', 'tbl_medical_insurane.medical_id = tbl_employee.med_insur_type', 'left');
        $this->db->join('tbl_employee_login', 'tbl_employee_login.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tpl_employee_category', 'tpl_employee_category.id  = tbl_employee.employee_category_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_employee.departement_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.nationality', 'left');
        $this->db->join('tbl_job_titles', 'tbl_job_titles.job_titles_id  = tbl_employee.job_title', 'left');
        $this->db->join('tbl_job_places', 'tbl_job_places.job_place_id  = tbl_employee.job_place_id', 'left');
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employef_id  = tbl_employee.employee_id', 'left');
        if(!$all)
            $this->db->where('tbl_employee.status !=',2);
        $this->db->where('(tbl_employee.designations_id='.$des_id.' OR tbl_employee.direct_manager_id='.$this->session->userdata("employee_id").')');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_all_notice($limit = NULL) {
        $this->db->select('tbl_notice.*', FALSE);
        $this->db->from('tbl_notice');
        if (!empty($limit)) {
            $this->db->limit('5');
        }
        $this->db->where('flag', 1);
        $this->db->order_by("notice_id", "desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }




    public function get_total_attendace_by_date($start_date, $end_date, $employee_id) {

        $this->db->select('tbl_attendance.*', FALSE);
        $this->db->from('tbl_attendance');
        $this->db->where('employee_id', $employee_id);
        $this->db->where('date_in >=', $start_date);
        $this->db->where('date_in <=', $end_date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function get_inbox_message($email, $flag = Null, $del_info = NULL, $limit = NULL) {

        $this->db->select('*');
        $this->db->from('tbl_inbox');
        if (!empty($del_info)) {
            $this->db->where('deleted', 'Yes');
        } else {
            $this->db->where('deleted', 'No');
        }
        if (!empty($flag)) {
            $this->db->where('view_status', '2');
        }
        if (!empty($limit)) {
            $this->db->limit('10');
        }
        $this->db->where('to', $email);
        $this->db->order_by('message_time', 'DESC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_sent_message($employee_id, $del_info = NULL) {
        $this->db->select('*');
        $this->db->from('tbl_sent');
        $this->db->where('employee_id', $employee_id);
        if (!empty($del_info)) {
            $this->db->where('deleted', 'Yes');
        } else {
            $this->db->where('deleted', 'No');
        }
        $this->db->order_by('message_time', 'DESC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_draft_message($employee_id, $del_info = NULL) {
        $this->db->select('*');
        $this->db->from('tbl_draft');
        if (!empty($del_info)) {
            $this->db->where('deleted', 'Yes');
        } else {
            $this->db->where('deleted', 'No');
        }
        $this->db->where('employee_id', $employee_id);

        $this->db->order_by('message_time', 'DESC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }





    public function designations($id = NULL) {
        $this->db->select('*');
        $this->db->from(' tbl_designations');
        if ($id) {
            $this->db->where('designations_id', $id);
            $result = $this->db->get()->row();
            return $result;
        }
        $result = $this->db->get()->result();
        return $result;
    }

    public function departments($id = NULL) {
        $this->db->select('*');
        $this->db->from(' tbl_department');
        if ($id) {
            $this->db->where('department_id', $id);
            $result = $this->db->get()->row();
            return $result;
        }
        $result = $this->db->get()->result();
        return $result;
    }

    public function ideal_employee($id) {
        $this->db->select('tbl_department.*');
        $this->db->select('tbl_designations.*');
        $this->db->select('tbl_job_titles.*');
        $this->db->select('tbl_employee.*');
        $this->db->from('tbl_employee');
        $this->db->join("tbl_department", "tbl_employee.departement_id=tbl_department.department_id", "left");
        $this->db->join("tbl_designations", "tbl_employee.designations_id=tbl_designations.designations_id", "left");
        $this->db->join("tbl_job_titles", "tbl_employee.job_title=tbl_job_titles.job_titles_id", "left");
        $this->db->where('tbl_employee.employee_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    function intPart($float) {
        if ($float < -0.0000001)
            return ceil($float - 0.0000001);
        else
            return floor($float + 0.0000001);
    }

    function Greg2Hijri($date, $string = false) {
        $date = explode(' ', $date);
        $dt = $date[0];
        $d = explode('-', $dt);

        $day = (int) $d[2];
        $month = (int) $d[1];
        $year = (int) $d[0];

        if (($year > 1582) or ( ($year == 1582) and ( $month > 10)) or ( ($year == 1582) and ( $month == 10) and ( $day > 14))) {
            $jd = $this->intPart((1461 * ($year + 4800 + $this->intPart(($month - 14) / 12))) / 4) + $this->intPart((367 * ($month - 2 - 12 * ($this->intPart(($month - 14) / 12)))) / 12) -
                $this->intPart((3 * ($this->intPart(($year + 4900 + $this->intPart(($month - 14) / 12) ) / 100) ) ) / 4) + $day - 32075;
        } else {
            $jd = 367 * $year - $this->intPart((7 * ($year + 5001 + $this->intPart(($month - 9) / 7))) / 4) + $this->intPart((275 * $month) / 9) + $day + 1729777;
        }

        $l = $jd - 1948440 + 10632;
        $n = $this->intPart(($l - 1) / 10631);
        $l = $l - 10631 * $n + 354;
        $j = ($this->intPart((10985 - $l) / 5316)) * ($this->intPart((50 * $l) / 17719)) + ($this->intPart($l / 5670)) * ($this->intPart((43 * $l) / 15238));
        $l = $l - ($this->intPart((30 - $j) / 15)) * ($this->intPart((17719 * $j) / 50)) - ($this->intPart($j / 16)) * ($this->intPart((15238 * $j) / 43)) + 29;

        $month = $this->intPart((24 * $l) / 709);
        $day = $l - $this->intPart((709 * $month) / 24);
        $year = 30 * $n + $j - 30;

        $date = array();
        $date['year'] = $year;
        $date['month'] = $month - 1;
        $date['day'] = $day + 1;
        $hijriMonth = array("محرم", "صفر", "ربيع الاول", "ربيع الثاني", "جمادى الاولى", "جمادى الثانية", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

        if (!$string)
            return $date;
        else {
            if (strlen($month) == 1)
                $month = '0' . $month;
            if (strlen($day) == 1)
                $day = '0' . $day;
            return $year . '-' . $month . '-' . $day;
        }
    }

    function Hijri2Greg($date, $string = false)
    {
        $date = explode(' ', $date);
        $dt = $date[0];
        $d = explode('-', $dt);

        $day = (int) $d[2];
        $month = (int) $d[1];
        $year = (int) $d[0];

        $jd = $this->intPart((11*$year+3) / 30) + 354 * $year + 30 * $month - $this->intPart(($month-1)/2) + $day + 1948440 - 385;

        if ($jd > 2299160)
        {
            $l = $jd+68569;
            $n = $this->intPart((4*$l)/146097);
            $l = $l-$this->intPart((146097*$n+3)/4);
            $i = $this->intPart((4000*($l+1))/1461001);
            $l = $l-$this->intPart((1461*$i)/4)+31;
            $j = $this->intPart((80*$l)/2447);
            $day = $l-$this->intPart((2447*$j)/80);
            $l = $this->intPart($j/11);
            $month = $j+2-12*$l;
            $year  = 100*($n-49)+$i+$l;
        }
        else
        {
            $j = $jd+1402;
            $k = $this->intPart(($j-1)/1461);
            $l = $j-1461*$k;
            $n = $this->intPart(($l-1)/365)-$this->intPart($l/1461);
            $i = $l-365*$n+30;
            $j = $this->intPart((80*$i)/2447);
            $day = $i-$this->intPart((2447*$j)/80);
            $i = $this->intPart($j/11);
            $month = $j+2-12*$i;
            $year = 4*$k+$n+$i-4716;
        }

        $data = array();
        $date['year']  = $year;
        $date['month'] = $month;
        $date['day']   = $day;

        if (!$string)
            return $date;
        else {
            if (strlen($month) == 1)
                $month = '0' . $month;
            if (strlen($day) == 1)
                $day = '0' . $day;
            return $year . '-' . $month . '-' . $day;
        }
    }

    public function get_all_approvals() {
        $this->db->select("appr_cat.*");
        $this->db->select("jb_plc.*");
        $this->db->select("deps.department_id, deps.department_name, deps.department_name_ar");
        $this->db->select("desg.designations_id, desg.designations, desg.designations_ar");
        $this->db->from("tbl_approvals_cat as appr_cat");
        $this->db->join("tbl_job_places as jb_plc", "appr_cat.jplc=jb_plc.job_place_id", "LEFT");
        $this->db->join("tbl_department as deps", "appr_cat.dep=deps.department_id", "LEFT");
        $this->db->join("tbl_designations as desg", "appr_cat.sect=desg.designations_id", "LEFT");
        $this->db->order_by("appr_cat.approval_cat_id");
        $result = $this->db->get()->result();
        return $result;
    }

    public function get_applications_by_emp($id) {
        $this->db->select('tbl_employee.full_name_ar, tbl_employee.full_name_en, tbl_employee.employment_id');
        $this->db->select('tbl_applications.*');
        $this->db->select('tbl_approvals_cat.*');
        $this->db->from('tbl_applications');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id=tbl_applications.employee_id', "LEFT");
        $this->db->join('tbl_approvals_cat', 'tbl_approvals_cat.approval_cat_id=tbl_applications.approval_cat_id', "LEFT");
        $this->db->order_by('tbl_applications.application_id');
        $this->db->where('tbl_employee.employee_id', $id);
        $result = $this->db->get()->result();
        return $result;
    }

    public function get_applications_by_id($id) {
        $this->db->select('tbl_employee.full_name_ar, tbl_employee.full_name_en, tbl_employee.employment_id');
        $this->db->select('tbl_applications.*');
        $this->db->from('tbl_applications');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id=tbl_applications.employee_id', "LEFT");
        $this->db->order_by('tbl_applications.application_id');
        $this->db->where('tbl_applications.application_id', $id);
        $result = $this->db->get()->row();
        return $result;
    }

    /*
      public function employee_info($id) {
      $this->db->select('tbl_employee.*');
      //$this->db->select('tbl_employee_login.*');
      //$this->db->join('tbl_employee_login', 'tbl_employee_login.employee_id=tbl_employee.employee_id', "LEFT");
      $this->db->from('tbl_employee');
      $this->db->where('tbl_employee.employee_id', $id);
      $result = $this->db->get()->row();
      echo $this->db->last_query();
      return $result;
      }
     */
}
