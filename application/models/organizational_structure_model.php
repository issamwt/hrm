<?php

class Organizational_Structure_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_add_department_by_id($department_id) {
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->from('tbl_department');
        $this->db->join('tbl_designations', 'tbl_department.department_id = tbl_designations.department_id', 'left');
        $this->db->where('tbl_department.department_id', $department_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function reset_designation($data) {
        $this->db->update_batch('tbl_designations', $data, 'designations_id');
    }

    public function get_designation_with_departments($id = NULL) {
        $this->db->select('ds.*, d.department_name, d.department_name_ar, e.full_name_en, e.full_name_ar');
        $this->db->from('tbl_designations ds');
        $this->db->order_by('ds.designations_id', 'ASC');
        $this->db->join('tbl_department d', 'd.department_id = ds.department_id', 'left');
        $this->db->join('tbl_employee e', 'e.employee_id = ds.employee_id', 'left');
        $this->db->order_by('designations_id');
        if ($id) {
            $this->db->where('ds.designations_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
            return $result;
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_unit_with_designations($id = NULL) {
        $this->db->select('unit.*, ds.designations, ds.designations_ar, e.full_name_en, e.full_name_ar');
        $this->db->from('tbl_units unit');
        $this->db->order_by('unit.unit_id', 'ASC');
        $this->db->join('tbl_designations ds', 'ds.designations_id = unit.designations_id', 'left');
        $this->db->join('tbl_employee e', 'e.employee_id = unit.unit_employee_id', 'left');
        $this->db->order_by('unit_id');
        if ($id) {
            $this->db->where('unit.unit_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
            return $result;
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_division_with_units($id = NULL) {

        $this->db->select('tbl_divisions.*, unit.unit_en, unit.unit_ar, e.full_name_en, e.full_name_ar');
        $this->db->from('tbl_divisions');
        $this->db->order_by('tbl_divisions.division_id', 'ASC');
        $this->db->join('tbl_units unit', 'unit.unit_id = tbl_divisions.unit_id', 'left');
        $this->db->join('tbl_employee e', 'e.employee_id = tbl_divisions.div_employee_id', 'left');
        $this->db->order_by('division_id');
        if ($id) {
            $this->db->where('tbl_divisions.division_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
            return $result;
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_branches() {
        $this->db->select('branche_id, branche_en, branche_ar');
        $get = $this->db->get('tbl_branches');
        return $get->result_array();
    }

    public function get_deps($jpid) {
        $this->db->select('department_id, department_name, department_name_ar');
        $this->db->where('branche_id', $jpid);
        $get = $this->db->get('tbl_department');
        return $get->result_array();
    }

    public function get_desg($dep_id) {
        $this->db->select('designations_id, designations, designations_ar');
        $this->db->where('department_id', $dep_id);
        $get = $this->db->get('tbl_designations');
        return $get->result_array();
    }

    public function get_units($des_id) {
        $this->db->select('unit_id, unit_en, unit_ar');
        $this->db->where('designations_id', $des_id);
        $get = $this->db->get('tbl_units');
        return $get->result_array();
    }

    public function get_divis($unit_id) {
        $this->db->select('unit_id,division_id, division_en, division_ar');
        $this->db->where('unit_id', $unit_id);
        $get = $this->db->get('tbl_divisions');
        return $get->result_array();
    }

}
