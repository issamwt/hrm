<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Designation_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_designation_with_departments($id = NULL) {
        $this->db->select('ds.*, d.department_name, d.department_name_ar, e.full_name_en, e.full_name_ar');
        $this->db->from('tbl_designations ds');
        $this->db->order_by('ds.designations_id', 'ASC');
        $this->db->join('tbl_department d', 'd.department_id = ds.department_id', 'left');
        $this->db->join('tbl_employee e', 'e.employee_id = ds.employee_id', 'left');
        $this->db->order_by('designations_id');
        if ($id) {
            $this->db->where('ds.designations_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
            return $result;
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

}
