<?php

class Settings_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_working_days() {
        $this->db->select('tbl_working_days.*', FALSE);
        $this->db->select('tbl_days.day', FALSE);
        $this->db->from('tbl_working_days');
        $this->db->join('tbl_days', 'tbl_working_days.day_id = tbl_days.day_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_holiday_list_by_date($start_date, $end_date) {
        $this->db->select('tbl_holiday.*', FALSE);
        $this->db->from('tbl_holiday');
        $this->db->where('start_date >=', $start_date);
        $this->db->where('end_date <=', $end_date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function delete_all($table) {
        $this->db->empty_table($table);
    }

    public function select_general_info() {
        $this->db->select('*');
        $this->db->from('tbl_gsettings');

        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    function translations() {

        $companies = $this->db->select('language')->group_by('language')->order_by('language', 'ASC')->get('tbl_client')->result();
        $users = $this->db->select('language')->group_by('language')->order_by('language', 'ASC')->get('tbl_account_details')->result();
        if (!empty($companies)) {
            foreach ($companies as $lang) {
                $tran[$lang->language] = $lang->language;
            }
        }
        if (!empty($users)) {
            foreach ($users as $lan) {
                $tran[$lan->language] = $lan->language;
            }
        }
        if (!empty($tran)) {
            unset($tran['english']);
            return $tran;
        }
    }

    function get_active_languages($lang = FALSE) {

        if (!$lang) {
            return $this->db->order_by('name', 'ASC')->get('tbl_languages')->result();
        }
        $result = $this->db->where('name', $lang)->get('tbl_languages')->result();
        return $result;
    }


    public function evaluation_items_by_departements_id($department_id) {
        $this->db->select('tbl_department.*', FALSE);
        $this->db->select('tpl_evaluation_items.*', FALSE);
        $this->db->from('tbl_department');
        $this->db->join('tpl_evaluation_items', 'tbl_department.department_id = tpl_evaluation_items.department_id', 'left');
        $this->db->where('tbl_department.department_id', $department_id);
        $this->db->where('tpl_evaluation_items.job_titles_id',0);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
 public function get_deps() {
        $this->db->select('department_id, department_name, department_name_ar');
        $get = $this->db->get('tbl_department');
        return $get->result_array();
    }

    public function get_desg($dep_id) {
        $this->db->select('designations_id, designations, designations_ar');
        $this->db->where('department_id', $dep_id);
        $get = $this->db->get('tbl_designations');
        return $get->result_array();
    }

    public function get_units($des_id) {
        $this->db->select('unit_id, unit_en, unit_ar');
        $this->db->where('designations_id', $des_id);
        $get = $this->db->get('tbl_units');
        return $get->result_array();
    }

    public function get_divis($unit_id) {
        $this->db->select('unit_id,division_id, division_en, division_ar');
        $this->db->where('unit_id', $unit_id);
        $get = $this->db->get('tbl_divisions');
        return $get->result_array();
    }
}
