<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emp_model
 *
 * @author nayem
 */
class Reports_Model extends MY_Model
{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function custodies_by_employee($id = null){
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->select('countries.countryName, countries.countryName_ar', FALSE);
        $this->db->select('tbl_employee_custody.name_ar, tbl_employee_custody.name_en, tbl_employee_custody.delivery_date, tbl_employee_custody.nombre, tbl_employee_custody.custody_reference, tbl_employee_custody.description_ar, tbl_employee_custody.description_en document,', FALSE);
        $this->db->select('tbl_job_titles.*', FALSE);

        $this->db->from('tbl_employee');

        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_employee.departement_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.nationality', 'left');
        $this->db->join('tbl_employee_custody', 'tbl_employee_custody.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_job_titles', 'tbl_job_titles.job_titles_id  = tbl_employee.job_title', 'left');

        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }
}