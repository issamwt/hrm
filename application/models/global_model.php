<?php

class Global_Model extends MY_Model {

    protected $_table_name;
    protected $_order_by;



    public function select_user_roll($employee_login_id) {
        $this->db->select('tbl_user_roll.menu_id', FALSE);
        $this->db->select('tbl_menu.slug, tbl_menu.menu_name', FALSE);
        $this->db->from('tbl_user_roll');
        $this->db->join('tbl_menu', 'tbl_user_roll.menu_id = tbl_menu.menu_id', 'left');
        $this->db->where('employee_login_id', $employee_login_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function check_user_name($user_name, $user_id) {
        $this->db->select('tbl_user.*', false);
        $this->db->from('tbl_user');
        if ($user_id) {
            $this->db->where('user_id !=', $user_id);
        }
        $this->db->where('user_name', $user_name);
        $query_result = $this->db->get();
        $result = $query_result->row();

        return $result;
    }



}
