<?php

/**
 * Description of employee_model
 *
 * @author NaYeM
 */
class Employee_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_add_department_by_id($department_id) {
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->from('tbl_department');
        $this->db->join('tbl_designations', 'tbl_department.department_id = tbl_designations.department_id', 'left');
        $this->db->where('tbl_department.department_id', $department_id);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function all_emplyee_info($id = NULL) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_social_insurane.social_id, tbl_social_insurane.social_flag, tbl_social_insurane.insurance_percent, tbl_social_insurane.societe_percent', FALSE);
        $this->db->select('tbl_medical_insurane.m_insurance_percent, tbl_medical_insurane.m_societe_percent', FALSE);
        $this->db->select('tpl_employee_category.*', FALSE);
        $this->db->select('tbl_employee_login.password', FALSE);
        $this->db->select('tbl_designations.designations, tbl_designations.designations_ar', FALSE);
        $this->db->select('tbl_department.department_id, tbl_department.department_name, tbl_department.department_name_ar', FALSE);
        $this->db->select('countries.countryName, countries.countryName_ar', FALSE);
        $this->db->select('tbl_job_titles.*', FALSE);
        $this->db->select('tbl_job_places.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);
        //$this->db->select('tbl_allowance.*', FALSE);

        $this->db->from('tbl_employee');

        $this->db->join('tbl_social_insurane', 'tbl_social_insurane.social_flag = tbl_employee.social_insurance_type', 'left');
        $this->db->join('tbl_medical_insurane', 'tbl_medical_insurane.medical_id = tbl_employee.med_insur_type', 'left');
        $this->db->join('tbl_employee_login', 'tbl_employee_login.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tpl_employee_category', 'tpl_employee_category.id  = tbl_employee.employee_category_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_employee.departement_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.nationality', 'left');
        $this->db->join('tbl_job_titles', 'tbl_job_titles.job_titles_id  = tbl_employee.job_title', 'left');
        $this->db->join('tbl_job_places', 'tbl_job_places.job_place_id  = tbl_employee.job_place_id', 'left');
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employef_id  = tbl_employee.employee_id', 'left');
        //$this->db->join('tbl_allowance', 'tbl_allowance.employe_id  = tbl_employee.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function all_designations_by_department($id = NULL) {
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_designations');
        $this->db->join('tbl_employee', 'tbl_designations.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->where('tbl_designations.department_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_department_byId($id = NULL) {
        $this->db->select('tbl_department.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_department');
        $this->db->join('tbl_employee', 'tbl_department.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->where('tbl_department.department_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_designation_byId($id = NULL) {
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_designations');
        $this->db->join('tbl_employee', 'tbl_designations.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->where('tbl_designations.designations_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_allowances() {
        $this->db->select('tbl_allowance_category.*', FALSE);
        $this->db->from('tbl_allowance_category');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_allowances_by_employee($id = NULL) {
        $this->db->select('tbl_allowance.*', FALSE);
        $this->db->select('tbl_allowance_category.*', FALSE);
        $this->db->where('tbl_allowance.employe_id', $id);
        $this->db->join('tbl_allowance_category', 'tbl_allowance_category.allowance_id  = tbl_allowance.allowance_id', 'left');
        $this->db->from('tbl_allowance');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_allowances_bytype($type) {
        $this->db->select('tbl_allowance_category.*', FALSE);
        $this->db->where('tbl_allowance_category.allowance_id', $type);
        $this->db->from('tbl_allowance_category');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_deduction_bytype($type) {
        $this->db->select('tbl_deduction_category.*', FALSE);
        $this->db->where('tbl_deduction_category.deduction_id', $type);
        $this->db->from('tbl_deduction_category');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_provisions() {
        $this->db->select('tbl_provision_category.*', FALSE);
        $this->db->from('tbl_provision_category');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_provisions_by_employee($id = NULL) {
        $this->db->select('tbl_provision.*', FALSE);
        $this->db->select('tbl_provision_category.*', FALSE);
        $this->db->where('tbl_provision.employee_id', $id);
        $this->db->join('tbl_provision_category', 'tbl_provision_category.provision_category_id  = tbl_provision.provision_category_id', 'left');
        $this->db->from('tbl_provision');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function all_deductions() {
        $this->db->select('tbl_deduction_category.*', FALSE);
        $this->db->from('tbl_deduction_category');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getAllEmployees() {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->from('tbl_employee');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getEmployeesRetrait($date) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_finance_info.*', FALSE);
        $this->db->join('tbl_finance_info', 'tbl_finance_info.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->from('tbl_employee');
        $this->db->where('tbl_finance_info.retrait_date', $date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getHr() {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('tbl_employee.job_title', 4);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    function intPart($float) {
        if ($float < -0.0000001)
            return ceil($float - 0.0000001);
        else
            return floor($float + 0.0000001);
    }

    function Greg2Hijri($date, $string = false) {
        $date = explode(' ', $date);
        $dt = $date[0];
        $d = explode('-', $dt);

        $day = (int) $d[2];
        $month = (int) $d[1];
        $year = (int) $d[0];

        if (($year > 1582) or ( ($year == 1582) and ( $month > 10)) or ( ($year == 1582) and ( $month == 10) and ( $day > 14))) {
            $jd = $this->intPart((1461 * ($year + 4800 + $this->intPart(($month - 14) / 12))) / 4) + $this->intPart((367 * ($month - 2 - 12 * ($this->intPart(($month - 14) / 12)))) / 12) -
                $this->intPart((3 * ($this->intPart(($year + 4900 + $this->intPart(($month - 14) / 12) ) / 100) ) ) / 4) + $day - 32075;
        } else {
            $jd = 367 * $year - $this->intPart((7 * ($year + 5001 + $this->intPart(($month - 9) / 7))) / 4) + $this->intPart((275 * $month) / 9) + $day + 1729777;
        }

        $l = $jd - 1948440 + 10632;
        $n = $this->intPart(($l - 1) / 10631);
        $l = $l - 10631 * $n + 354;
        $j = ($this->intPart((10985 - $l) / 5316)) * ($this->intPart((50 * $l) / 17719)) + ($this->intPart($l / 5670)) * ($this->intPart((43 * $l) / 15238));
        $l = $l - ($this->intPart((30 - $j) / 15)) * ($this->intPart((17719 * $j) / 50)) - ($this->intPart($j / 16)) * ($this->intPart((15238 * $j) / 43)) + 29;

        $month = $this->intPart((24 * $l) / 709);
        $day = $l - $this->intPart((709 * $month) / 24);
        $year = 30 * $n + $j - 30;

        $date = array();
        $date['year'] = $year;
        $date['month'] = $month - 1;
        $date['day'] = $day + 1;
        $hijriMonth = array("محرم", "صفر", "ربيع الاول", "ربيع الثاني", "جمادى الاولى", "جمادى الثانية", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

        if (!$string)
            return $date;
        else {
            if (strlen($month) == 1)
                $month = '0' . $month;
            return $year . '-' . $month . '-' . $day;
        }
    }

}
