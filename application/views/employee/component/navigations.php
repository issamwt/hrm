<?php $lang = $this->session->userdata('lang'); ?>

<style type="text/css">
    .notices_count {
        position: absolute;
        left: 3px;
        top: 3px;
        font-size: 0.9em;
    }

    .my_notices_count {
        background: red;
        border-radius: 2px;
        color: #fff;
        padding: 1px 2px;
        box-shadow: 0px 0px 1px 1px #fff;
    }

    .all_notices_count {
        background: #00CC00;
        border-radius: 2px;
        color: #fff;
        padding: 1px 2px;
        box-shadow: 0px 0px 1px 1px #fff;
    }
    #main-header{
        border: 0px !important;
    }
</style>

<div id="main-header" class="clearfix">
    <header id="header" class="clearfix">
        <div class="row main">
            <nav class="navbar navbar-custom" id="header_menu" role="navigation">

                <div class="menu-bg">
                    <nav class="main-menu navbar navbar-collapse menu-bg" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header menu-bg">
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="main-menu collapse navbar-collapse menu-bg" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="<?= @$menu['index'] == 1 ? 'active' : ''; ?>">
                                        <a href="<?php echo base_url() ?>employee/dashboard"><?= lang('home') ?></a>
                                    </li>
                                    <?php if ($this->session->userdata('emp_type') != 'employee'): ?>
                                        <li class="<?= @$menu['employee_list'] == 1 ? 'active' : ''; ?>">
                                            <a href="#" class="dropdown-toggle"
                                               data-toggle="dropdown"><?= lang('manage_employee') ?><b
                                                    class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/dashboard/employee_list"><?= lang('employee_list') ?></a>
                                                </li>
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'  or $this->session->userdata('emp_type') == 'dep_manager'): ?>
                                                    <li>
                                                        <a href="<?php echo base_url() ?>employee/evaluations/evaluations_section"><?= lang('evaluations_section') ?></a>
                                                    </li>
                                                <?php endif; ?>
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/reports/reports_section"><?= lang('reports_section') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/dashboard/vacation_balances"><?= lang('vacation_balances') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/calculations/calculating_attendance"><?= lang('calculating_attendance') ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/calculations/calculating_salary"><?= lang('calculating_salary') ?></a>
                                                </li>
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'):?>
                                                <li>
                                                    <a href="<?php echo base_url() ?>employee/settings"><?= lang('settings') ?></a>
                                                </li>
                                                <?php endif;?>
                                            </ul>
                                        </li>
                                    <?php endif; ?>
                                    <li class="<?= @$menu['applications'] == 1 ? 'active' : ''; ?>"
                                        style="position: relative">
                                        <a href="#" class="dropdown-toggle"
                                           data-toggle="dropdown"><?= lang('applications') ?><b class="caret"></b></a>
                                        <div class="notices_count">
                                            <?php if ($this->session->userdata('my_app_count') != 0): ?>
                                                <small
                                                    class="my_notices_count"><?= $this->session->userdata('my_app_count') ?></small>
                                            <?php endif; ?>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <li class="<?= @$menu['send_applications'] == 1 ? 'active' : ''; ?>">
                                                <a href="<?php echo base_url() ?>employee/applications/send_applications"><?= lang('send_applications') ?></a>
                                            </li>
                                            <li class="<?= @$menu['list_applications'] == 1 ? 'active' : ''; ?>">
                                                <a href="<?php echo base_url() ?>employee/applications/list_applications"><?= lang('list_applications') ?></a>
                                            </li>
                                            <li class="<?= @$menu['received_applications'] == 1 ? 'active' : ''; ?>" <?=($this->session->userdata('my_app_count') != 0)?'style="background-color:red"':'';?>>
                                                <a href="<?php echo base_url() ?>employee/applications/received_applications"><?= lang('received_applications') ?></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="<?= @$menu['notices'] == 1 ? 'active' : ''; ?>"
                                        style="position: relative">
                                        <a href="<?php echo base_url() ?>employee/dashboard/all_notice"><?= lang('notices') ?></a>
                                        <div class="notices_count">
                                            <? if ($this->session->userdata('my_notices_count') != 0): ?>
                                                <small
                                                    class="my_notices_count"><?= $this->session->userdata('my_notices_count') ?></small>
                                            <? endif; ?>
                                            <? if($this->session->userdata('all_notices_count') != 0):?>
                                            <small class="all_notices_count"><?= $this->session->userdata('all_notices_count') ?></small>
                                            <? endif; ?>
                                        </div>
                                    </li>
                                    <li class="<?= @$menu['contact_admin'] == 1 ? 'active' : ''; ?>">
                                        <a href="<?php echo base_url() ?>employee/dashboard/contact_admin"><?= lang('contact_admin') ?></a>
                                    </li>


                                    <li class="dropdown <?php
                                    if (!empty($menu['mailbox'])) {
                                        echo $menu['mailbox'] == 1 ? 'active' : '';
                                    }
                                    ?>">
                                        <a href="#" class="dropdown-toggle"
                                           data-toggle="dropdown"><?= lang('mailbox') ?><b class="caret"></b></a>
<div class="notices_count">
                                            <?php if ($this->session->userdata('my_inbox_count') != 0): ?>
                                                <small
                                                    class="my_notices_count"><?= $this->session->userdata('my_inbox_count') ?></small>
                                            <?php endif; ?>
                                        </div>
                                        <ul class="dropdown-menu">
                                            <li class="<?php
                                            if (!empty($menu['inbox'])) {
                                                echo $menu['inbox'] == 1 ? 'active' : '';
                                            }
                                            ?>">
                                                <a href="<?php echo base_url() ?>employee/dashboard/inbox"><?= lang('inbox') ?></a>
                                            </li>
                                            <li class="<?php
                                            if (!empty($menu['sent'])) {
                                                echo $menu['sent'] == 1 ? 'active' : '';
                                            }
                                            ?>">
                                                <a href="<?php echo base_url() ?>employee/dashboard/sent"><?= lang('sent') ?></a>
                                            </li>
                                            <li class="<?php
                                            if (!empty($menu['draft'])) {
                                                echo $menu['draft'] == 1 ? 'active' : '';
                                            }
                                            ?>">
                                                <a href="<?php echo base_url() ?>employee/dashboard/draft"><?= lang('draft') ?></a>
                                            </li>
                                            <li class="<?php
                                            if (!empty($menu['trash'])) {
                                                echo $menu['trash'] == 1 ? 'active' : '';
                                            }
                                            ?>">
                                                <a href="<?php echo base_url() ?>employee/dashboard/trash"><?= lang('trash') ?></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>

                                <ul class="main-menu nav navbar-nav navbar-right">
                                    <li class="dropdown <?php
                                    if (!empty($menu['language'])) {
                                        echo $menu['language'] == 1 ? 'active' : '';
                                    }
                                    ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                class="fa fa-language">&nbsp;</i><?= lang('languages') ?><b
                                                class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <?php $languages = $this->db->order_by('name', 'ASC')->get('tbl_languages')->result(); ?>
                                            <?php foreach ($languages as $lang) : ?>
                                                <?php if ($lang->active == 1) : ?>
                                                    <li>
                                                        <a href="<?= base_url() ?>employee/dashboard/set_language/<?= $lang->name ?>"/>
                                                        <img
                                                            src="<?= base_url() ?>asset/images/flags/<?= $lang->icon ?>.gif"/>
                                                        <?= ($this->session->userdata('lang') == 'english') ? ucwords(str_replace("_", " ", $lang->name)) : ucwords(str_replace("_", " ", $lang->name_ar)); ?>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li class="dropdown <?php
                                    if (!empty($menu['profile'])) {
                                        echo $menu['profile'] == 1 ? 'active' : '';
                                    }
                                    ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                                class="fa fa-user">&nbsp;</i><?= lang('profile') ?><b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <style type="text/css">
                                                .profile-img {
                                                    width: 70% !important;
                                                    height: 70% !important;
                                                    display: block;
                                                    margin: 20px auto;
                                                    margin-bottom: 20px;
                                                    border-radius: 3px;
                                                    border: 1px solid darkslategray;
                                                }

                                                .some-btns {
                                                    padding: 5px;
                                                }

                                                .logout-btn {
                                                    display: inline-block;
                                                    float: left;
                                                    background: #F00E0E;
                                                    color: #fff;
                                                    border-radius: 4px;
                                                    padding: 9px !important;
                                                }

                                                .profile-btn {
                                                    display: inline-block;
                                                    float: right;
                                                    background: #00CC00;
                                                    color: #fff;
                                                    border-radius: 4px;
                                                    padding: 9px !important;
                                                }
                                            </style>
                                            <li>
                                                <?php if ($this->employee_details->photo): ?>
                                                    <img
                                                        src="<?php echo base_url() . $this->employee_details->photo; ?>"
                                                        style="height: 200px; width: 210px;"
                                                        class="img-responsive center-block profile-img"/>
                                                <?php else: ?>
                                                    <img src="<?php echo base_url() ?>asset/img/administrator.png"
                                                         style="height: 200px; width: 210px; "
                                                         class="img-responsive center-block profile-img"
                                                         alt="Employee_Image"/>
                                                <?php endif; ?>
                                            </li>
                                            <li>
                                                <div class="text-center"
                                                     style="padding:10px 5px 5px 5px;"><?= ($this->session->userdata('lang') == 'english') ? $this->employee_details->full_name_en : $this->employee_details->full_name_ar; ?></div>
                                                <div class="text-center"
                                                     style="padding:0px 5px 10px 5px;"><?= lang($this->session->userdata('emp_type')) ?>
                                                    <?php if ($this->session->userdata('emp_type') == 'dep_manager'): ?>
                                                        <?php
                                                        echo '(';
                                                        echo ($lang == "english") ? $this->session->userdata("my_department_name_en") : $this->session->userdata("my_department_name_ar");
                                                        echo ')';
                                                        ?>
                                                    <?php elseif ($this->session->userdata('emp_type') == 'sec_manager'): ?>
                                                        <?php
                                                        echo '(';
                                                        echo ($lang == "english") ? $this->session->userdata("my_designation_name_en") : $this->session->userdata("my_designation_name_ar");
                                                        echo ')';
                                                        ?>
                                                    <?php endif; ?>
                                                </div>
                                            </li>
                                            <li class="some-btns">
                                                <div>
                                                    <a class="logout-btn"
                                                       href="<?php echo base_url() ?>login/logout"><?= lang('logout') ?></a>
                                                    <a class="profile-btn"
                                                       href="<?php echo base_url() ?>employee/dashboard/profile"><?= lang('profile') ?></a>
                                                    <div style="clear: both"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div>
                    </nav>
                </div>
            </nav>
        </div>
    </header>
</div>


