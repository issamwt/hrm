<style type="text/css">
    .table-container {
        padding: 20px;
        padding-top: 10px;
    }

    h4 {
        color: #fff;
        background: #337AB7;
        padding: 5px 0px;
    }
</style>
<div class="col-md-12">
    <div class="box box-primary">
        <!-- Default panel contents -->

        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only"><?= lang('close') ?></span></button>
            <div class="panel-title">
                <strong><?= lang('advance_update') ?></strong>
            </div>
        </div>
        <div class="table-container">
            <form action="<?= base_url() ?>employee/employee/update_advance/<?= $adv_id ?>"
                  method="post">
                <table class="table table-non-bordered">
                    <tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?= ($lang == 'english') ? $employee_detail->full_name_en : $employee_detail->full_name_ar; ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('date') ?></td>
                        <td><?= @$advance->advance_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('advance_value') ?></td>
                        <td><?= @$advance->advance_value ?> <?= lang('rial') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('rest_advance') ?></td>
                        <td><input type="rest" readonly class="restx" value="<?= @$advance->rest ?>"> <?= lang('rial') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('payement_method') ?></td>
                        <td><?= lang('payement_method' . $advance->payement_method) ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('monthly_installement') ?></td>
                        <td><input type="text" name="monthly_installement" class="monthly-installementx"
                                   value="<?= @$advance->monthly_installement ?>"> <?= lang('rial') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('payement_months') ?></td>
                        <td><input type="text" name="payement_months" readonly class="payement-monthsx" value="<?= @$advance->payement_months ?>"></td>
                    </tr>
                    <td width="30%"></td>
                    <td>
                        <input type="submit" class="btn btn-primary" style="padding: 0px 15px;"
                               value="<?= lang('update') ?>"></td>
                </table>
                <input type="hidden" class="pmethodx" value="<?= $advance->payement_method ?>">
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.monthly-installementx').on('change textInput input', function (){
            var restx = $('.restx').val();
            var mix = $('.monthly-installementx').val();
            var pmx = $('.payement-monthsx').val();
            console.log(restx);
            console.log(mix);
            var res = Math.ceil(restx/mix);
            $('.payement-monthsx').val(res);
        });
    });
</script>