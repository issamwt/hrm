<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang('results') ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php @$today = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                        <?php foreach (@$results as $result): ?>
                            <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                        class="fa fa-print"></i> <?= lang('print') ?></a>

                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == 'arabic'): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 300px;
                                        /*height: 100px;*/
                                        padding: 10px 15px;
                                    <?php if($lang == 'english'):?> float: left;
                                    <?php else: ?> float: right;
                                    <?php endif; ?>
                                    }

                                    table {
                                        width: 100%;
                                    }

                                    table td, table th {
                                        padding: 2px;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <b><?= lang('name') ?>
                                                    : </b><?= ($lang == 'english') ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang('designation') ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == 'english') ? $result->designations : $result->designations_ar) : lang('no-exist'); ?>
                                                <br>
                                                <b><?= lang('department') ?>
                                                    : </b><?= ($lang == 'english') ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang('direct_manager') ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == 'english') ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                                -----------------------
                                                <br>
                                                <?php if ($lang == "english"): ?>
                                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                                <?php else: ?>
                                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                                <?php endif; ?>
                                                <b><?= lang("the_report") ?> : </b> <?= lang("vacations") ?><br>
                                                <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                                <b><?= lang("year") ?>
                                                    : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 10px;"></div>
                                    <h2 style="margin:0px;" class="text-center"><?= lang('vacations') ?></h2><br>

                                </div>
                                <div class="content">
                                    <table border="1">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('leave_type') ?></th>
                                            <th class="text-center"><?= lang('leave_category_duration2') ?></th>
                                            <th class="text-center"><?= lang('going_date') ?></th>
                                            <th class="text-center"><?= lang('coming_date') ?></th>
                                            <th class="text-center"><?= lang('paid_leave') ?></th>
                                            <th class="text-center"><?= lang('leave_category_quota2') ?></th>
                                            <th class="text-center"><?= lang('leave_affect_stock2') ?></th>
                                            <th class="text-center"><?= lang('replacement') ?></th>
                                            <th class="text-center"><?= lang('the_leave_tel') ?></th>
                                            <th class="text-center"><?= lang('address_in_leave') ?></th>
                                            <th class="text-center"><?= lang('allowance_cat_ids') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? $i = 0; ?>
                                        <?php foreach (@$leaves_list as $leave): ?>
                                            <?php if ($leave->employel_id == $result->employee_id): ?>
                                                <?php if (strpos($leave->going_date, $filter) !== false) : $i++; ?>
                                                    <tr>
                                                        <td>
                                                            <?php foreach (@$leaves_cat as $lc): ?>
                                                                <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                                    <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td><?= $leave->duration ?></td>
                                                        <td><?= $leave->going_date ?></td>
                                                        <td><?= $leave->coming_date ?></td>
                                                        <td>
                                                            <?php foreach (@$leaves_cat as $lc): ?>
                                                                <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                                    <?= ($lc->paid_leave == 1) ? lang('yes') : lang('no') ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td><?= $leave->quota ?> %</td>
                                                        <td><?= ($leave->affect_stock == 1) ? lang('yes') : lang('no'); ?></td>
                                                        <td>
                                                            <?php if (empty($leave->replacement)): ?>
                                                                <?= lang('no-exist') ?>
                                                            <?php else: ?>
                                                                <?php foreach (@$all as $emp): ?>
                                                                    <?php if ($emp->employee_id == $leave->replacement): ?>
                                                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?= ($leave->leave_tel) ? $leave->leave_tel : lang('no-exist'); ?></td>
                                                        <td><?= $leave->address_in_leave; ?></td>
                                                        <td>
                                                            <?php foreach (@$leaves_cat as $lc): ?>
                                                                <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                                    <?php $allowance_cat_ids = explode(';', @$lc->allowance_cat_ids); ?>
                                                                    <?php foreach (@$all_allowance_categories as $ac): ?>
                                                                        <?php if (in_array($ac->allowance_id, $allowance_cat_ids)): ?>
                                                                            <li><?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?></li>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <?if($i==0):?>
                                            <tr>
                                                <td colspan="11" class="text-center"><?=lang("no-exist")?></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

