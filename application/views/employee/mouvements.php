<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    .panel-body {
        display: none;
    }

    .modal .panel-body {
        display: block;
    }

    .boxino {
        border: 1px solid gray;
        background: #337AB7;
        color: #fff;
        padding: 15px 15px 15px 50px;
        display: inline-block;
        border-radius: 8px;
    }

    .print {
        display: none;
    }

    @media print {
        .print {
            display: block;
            -webkit-print-color-adjust: exact !important;
        }
    }

    .spinner_sects {
        display: none;
        font-size: 7px;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">

            <div class="boxino">
                <table>
                    <tr>
                        <td width="170"><b><?= lang('name') ?> : </b></td>
                        <td><?= ($lang == 'english') ? $employee_info->full_name_en : $employee_info->full_name_ar; ?></td>
                    </tr>
                    <tr>
                        <td width="170"><b><?= lang('employee_id') ?> : </b></td>
                        <td>
                            <small><?= $employee_info->employment_id ?></small>
                        </td>
                    </tr>
                    <tr>
                        <td><b><?= lang('job_time') ?> :</b></td>
                        <td><?= lang($employee_info->job_time) ?></td>
                    </tr>
                    <tr>
                        <td>-------------------------</td>
                        <td>---------------</td>
                    </tr>
                    <tr>
                        <td width="170"><b><?= lang('department') ?> : </b></td>
                        <td><?= ($lang == 'english') ? $employee_info->department_name : $employee_info->department_name_ar; ?></td>
                    </tr>
                    <tr>
                        <td><b><?= lang('direct_manager') ?> :</b></td>
                        <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar']; ?></td>
                    </tr>
                    <tr>
                        <td><b><?= lang('hr_manager') ?> :</b></td>
                        <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar']; ?></td>
                    </tr>
                </table>
            </div>

            <br><br><br><br>


            <!-- leaves -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()" style="cursor: pointer;">
                    <strong><?= lang('leaves') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <?php if ($lang == 'english')
                        $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                    else
                        $months = array("جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
                    $d = explode('-', @$employee_info->joining_date);
                    $d2 = explode('-', @$today);
                    // employee join the work in this year (or employee join the work before this year and joining date has passed)
                    if (($d2[0] == $d[0]) or (($d2[1] * 30 + $d2[2]) > ($d[1] * 30 + $d[2]))) {
                        $days = abs(($d2[1] * 30 + $d2[2]) - ($d[1] * 30 + $d[2]));
                    } // employee join the work before this year
                    else {
                        $date1 = new DateTime(($d2[0] - 1) . '-' . $d[1] . '-' . $d[2]);
                        $date2 = new DateTime($today);
                        $days = $date2->diff($date1)->format("%a");
                    }
                    $currentyearm = date("Y");
                    $dxx = $currentyearm."-01-01";
                    $dxx2 = $dxx;
                    $currentyearh = explode("-", $dxx2)[0];
                    $datenowh =  date("Y-m-d");
                    if(str_replace("-","",$datenowh)>str_replace("-","",($currentyearh."-".$d[1]."-".$d[2]))){
                        $starth = ($currentyearh)."-".$d[1]."-".$d[2];
                        $endh = ($currentyearh+1)."-".$d[1]."-".$d[2];
                    }else{
                        $starth = ($currentyearh-1)."-".$d[1]."-".$d[2];
                        $endh = ($currentyearh)."-".$d[1]."-".$d[2];
                    }
                    echo "<strong>"."السنة العقدية الحالية : "."</strong><br>";
                    echo "<strong>";
                    echo $starth. " --> ".$endh."<br>";
                    echo "</strong>";
                    $new_balance_sanawiya = 0;
                    ?>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('leave_type') ?></th>
                            <th class="text-center"><?= lang('leave_category_duration2') ?></th>
                            <th class="text-center"><?= lang('going_date') ?></th>
                            <th class="text-center"><?= lang('coming_date') ?></th>
                            <th class="text-center"><?= lang('paid_leave') ?></th>
                            <th class="text-center"><?= lang('leave_category_quota2') ?></th>
                            <th class="text-center"><?= lang('leave_affect_stock2') ?></th>
                            <th class="text-center"><?= lang('replacement') ?></th>
                            <th class="text-center"><?= lang('the_leave_tel') ?></th>
                            <th class="text-center"><?= lang('address_in_leave') ?></th>
                            <th class="text-center"><?= lang('allowance_cat_ids') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty(@$leaves_list)): ?>
                            <?php foreach (@$leaves_list as $leave): ?>
                                <?php
                                if(str_replace("-","",$leave->going_date)>= str_replace("-","",$starth) and str_replace("-","",$leave->going_date)<=str_replace("-","",$endh)){
                                    if($leave->leave_category_id==1)
                                        $new_balance_sanawiya+=$leave->duration;
                                    $current = true;
                                }else{
                                    $current = false;
                                }
                                ?>
                                <tr style="background: <?=($current)?'#d7f4ff':'#ececec';?>">
                                    <td>
                                        <?php foreach (@$leaves_cat as $lc): ?>
                                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= $leave->duration ?></td>
                                    <td style="min-width: 110px; vertical-align: middle;" class="relative">
                                        <?= hijjri($leave->going_date); ?>
                                        <?= $leave->going_date ?>
                                    </td>
                                    <td style="min-width: 110px; vertical-align: middle;" class="relative">
                                        <?= hijjri($leave->coming_date); ?>
                                        <?= $leave->coming_date ?>
                                    </td>
                                    <td>
                                        <?php foreach (@$leaves_cat as $lc): ?>
                                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                <?= ($lc->paid_leave == 1) ? "<strong style='color: #2fa360; display: block; text-align: center'>".lang('yes')."</strong>" : "<strong style='color: crimson; display: block; text-align: center'>".lang('no')."</strong>" ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= $leave->quota ?> %</td>
                                    <td><?= ($leave->affect_stock == 1) ? "<strong style='color: #2fa360; display: block; text-align: center'>".lang('yes')."</strong>" : "<strong style='color: crimson; display: block; text-align: center'>".lang('no')."</strong>"; ?></td>
                                    <td>
                                        <?php if (empty($leave->replacement)): ?>
                                            <?= lang('no-exist') ?>
                                        <?php else: ?>
                                            <?php foreach (@$employee_list as $emp): ?>
                                                <?php if ($emp->employee_id == $leave->replacement): ?>
                                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= ($leave->leave_tel) ? $leave->leave_tel : lang('no-exist'); ?></td>
                                    <td><?= $leave->address_in_leave; ?></td>
                                    <td>
                                        <?php foreach (@$leaves_cat as $lc): ?>
                                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                <?php $allowance_cat_ids = explode(';', @$lc->allowance_cat_ids); ?>
                                                <?php foreach (@$all_allowance_categories as $ac): ?>
                                                    <?php if (in_array($ac->allowance_id, $allowance_cat_ids)): ?>
                                                        <li><?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php if($leave->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$leave->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($leave->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$leave->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>employee/employee/delete_leave/<?= $leave->leave_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-block btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                        <a href="<?= base_url() ?>employee/employee/leave_detail/<?= $leave->leave_id ?>"
                                           data-toggle="modal" data-target="#myModal_lg" class="btn btn-block btn-success btn-xs">
                                            <span class="fa fa-eye"></span> <?= lang('view') ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="14"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br>
                    <div style="display:inline-block;background: #d7f4ff; width: 15px; height: 15px"></div> <span>السنة الحالية</span>
                    <br>
                    <div style="display:inline-block;background: #ececec; width: 15px; height: 15px"></div> <span>السنوات الماضية</span>
                    <br><br><br><br>

                    <form enctype="multipart/form-data"  action="<?= base_url() ?>employee/employee/save_leave/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form"><br>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-7">

                                <div class="note2" disabled=""
                                     style="padding:12px; display:none;">
                                    <?=($lang == 'english')?"Start Year : ":"بداية السنة العقدية : ";?><span><?php echo $starth;?></span><br>
                                    <?=($lang == 'english')?"End Year : ":"نهاية السنة العقدية : ";?><span><?=$endh;?></span><br>
                                    <?php $rest = (ceil(((@$employee_info->holiday_no) * $days) / 365  - @$new_balance_sanawiya) <= (@$employee_info->holiday_no - @$new_balance_sanawiya))
                                        ? ceil(((@$employee_info->holiday_no) * $days) / 365 - @$new_balance_sanawiya) + @$employee_info->old_rest
                                        : (@$employee_info->holiday_no - @$new_balance_sanawiya) + @$employee_info->old_rest; ?>
                                    <?= lang('holiday_no') ?> : <?= (@$employee_info->holiday_no) ?><br>
                                    <?= lang('old_rest') ?> : <?= @$employee_info->old_rest ?><br>
                                    <?= lang('new_balance') ?> (<?=($lang == 'english')?"from annual vacations":"من الإجازات السنوية";?>) : <?= @$new_balance_sanawiya ?><br>
                                    <?= lang('days_of_work_this_yeas') ?> : <?= $days ?><br>
                                    <?= lang('yearly_holiday_no') ?> : <?= $rest ?><br>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="leave_category_id" class="form-control leaves_cat" required=""
                                        onchange="set_data($(this))">
                                    <option></option>
                                    <?php if (!empty(@$leaves_cat)): ?>
                                        <?php foreach (@$leaves_cat as $lc): ?>
                                            <option value="<?= $lc->leave_category_id ?>"
                                                    data-duration="<?= $lc->leave_duration ?>"
                                                    data-duration="<?= $lc->leave_duration ?>"
                                                    data-quota="<?= $lc->leave_quota ?>"
                                                    data-affect-stock="<?= $lc->affect_balance ?>"
                                                    data-calc-type="<?= $lc->calc_type ?>"
                                                    data-replacement="<?= $lc->set_replacement ?>"
                                                    data-leave-tel="<?= $lc->leave_tel ?>">
                                                <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_category_duration') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input type="number" name="duration" id="data-duration" step="any" min="0" max="240"
                                       class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('going_date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="going_date" type="text" class="form-control hijri_datepickerx"
                                       style="width:200px;" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('coming_date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="coming_date" type="text" class="form-control hijri_datepickery"
                                       style="width:200px;" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_category_quota') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input type="number" id="data-quota" step="any" min="0" max="100" name="quota"
                                       class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_affect_stock') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <select name="affect_stock" id="data-affect-stock" class="form-control" required="">
                                    <option></option>
                                    <option value="1"><?= lang('yes') ?></option>
                                    <option value="2"><?= lang('no') ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_calculation_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <select name="calc_type" id="data-calc-type" class="form-control" required="">
                                    <option></option>
                                    <option value="1"><?= lang('contractual_year') ?></option>
                                    <option value="2" disabled=""><?= lang('normal_year') ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('set_replacement') ?> <span class="required1"
                                                                                                       style="display: none; color: red"> *</span></label>
                            <div class="col-md-3">
                                <select name="replacement" class="form-control replacement">
                                    <option></option>
                                    <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                        <?php foreach (@$employee_list as $emp): ?>
                                            <option value="<?= $emp->employee_id ?>">
                                                <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php foreach (@$employee_list as $emp): ?>
                                            <?php if ($emp->departement_id == $employee_info->departement_id): ?>
                                                <option value="<?= $emp->employee_id ?>">
                                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div id="data-replacement"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('leave_tel') ?> <span class="required2"
                                                                                                 style="display: none; color: red"> *</span></label>
                            <div class="col-md-3">
                                <input type="number" id="data-leave-tel" name="leave_tel"
                                       class="form-control leave-tel">
                            </div>
                            <div id="leave-tel"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('address_in_leave') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="address_in_leave" type="text" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- leaves -->


            <!-- advances_app -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()" style="cursor: pointer;">
                    <strong><?= lang('advances_app') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('the_advance_type') ?></th>
                            <th class="text-center"><?= lang('voucher') ?></th>
                            <th class="text-center"><?= lang('advance_value') ?></th>
                            <th class="text-center"><?= lang('rest_advance') ?></th>
                            <th class="text-center"><?= lang('payement_method') ?></th>
                            <th class="text-center"><?= lang('monthly_installement') ?></th>
                            <th class="text-center"><?= lang('payement_months') ?></th>
                            <th class="text-center"><?= lang('last_advance_date') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty(@$advances)): ?>
                            <?php foreach (@$advances as $adv): ?>
                                <tr>
                                    <td class="relative" style="vertical-align: bottom;padding-bottom: 3px;"><?= @$adv->advance_date ?><?= hijjri(@$adv->advance_date); ?></td>
                                    <td>
                                        <?php if (!empty(@$advances_list)): ?>
                                            <?php foreach (@$advances_list as $al): ?>
                                                <?php if ($adv->advance_type_id == $al->advance_id): ?>
                                                    <?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (empty(@$adv->advance_voucher)): ?>
                                            <?= lang('no-exist') ?>
                                        <?php else: ?>
                                            <?php foreach (@$employee_list as $emp): ?>
                                                <?php if ($emp->employee_id == $adv->advance_voucher): ?>
                                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= $adv->advance_value ?> <?= lang('rial') ?></td>
                                    <td><?= $adv->rest ?> <?= lang('rial') ?></td>
                                    <td><?= lang('payement_method' . $adv->payement_method) ?></td>
                                    <td><?= $adv->monthly_installement ?>  <?= lang('rial') ?></td>
                                    <td><?= $adv->payement_months ?></td>
                                    <td class="relative" style="vertical-align: bottom;padding-bottom: 3px;"><?=hijjri($adv->last_date);?><?= (!empty($adv->last_date)) ? $adv->last_date : lang('no-exist'); ?></td>
                                    <td>
                                        <?php if($adv->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$adv->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($adv->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$adv->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td width="12%">
                                        <?= anchor('employee/employee/update_advance_page/' . $adv->advances_id . '/' . $employee_info->employee_id, '<i class="fa fa-edit"></i> ' . lang('update'), array('class' => "btn btn-success btn-xs", 'title' => 'View', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')) ?>
                                        <a href="<?= base_url() ?>employee/employee/delete_advance/<?= $adv->advances_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="10"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br><br><br><br>


                    <form enctype="multipart/form-data"  action="<?= base_url() ?>employee/employee/save_advance/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form2"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('advance_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="advance_type_id" class="form-control advances_list" required="">
                                    <?php if (!empty(@$advances_list)): ?>
                                        <?php foreach (@$advances_list as $al): ?>
                                            <option
                                                    value="<?= $al->advance_id ?>"><?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('advance_value') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="advance_value" type="number" class="form-control advance-value"
                                       required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('payement_method') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-sm-3">
                                <select name="payement_method" onchange="payementmethod($(this))"
                                        class="form-control payement-method" required="" disabled>
                                    <option></option>
                                    <option value="1"><?= lang('payement_method1') ?></option>
                                    <?php if (!empty(@$employee_info->retirement_date)): ?>
                                        <option value="2"><?= lang('payement_method2') ?></option>
                                    <?php else: ?>
                                        <option disabled><?= lang('payement_method2') ?>
                                            (<?= lang('no_contract_duration') ?>)
                                        </option>
                                    <?php endif; ?>
                                    <option value="3"><?= lang('payement_method3') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('monthly_installement') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-sm-3">
                                <input type="text" value="0" name="monthly_installement"
                                       class="form-control monthly-installement" disabled/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('payement_months') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-sm-3">
                                <input type="text" value="0" name="payement_months" class="form-control payement-months"
                                       readonly/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input type="text" name="advance_date" class="form-control hijri_datepicker"
                                       required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('voucher') ?></label>
                            <div class="col-md-7">
                                <select name="advance_voucher" class="form-control">
                                    <option value=""></option>
                                    <?php if (!empty(@$employee_list)): ?>
                                        <?php foreach (@$employee_list as $emp): ?>
                                            <option
                                                    value="<?= $emp->employee_id ?>"><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- advances_app -->


            <!-- caching -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()" style="cursor: pointer">
                    <strong><?= lang('caching') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('cahing_type') ?></th>
                            <th class="text-center"><?= lang('beneficiary_name') ?></th>
                            <th class="text-center"><?= lang('bill_number') ?></th>
                            <th class="text-center"><?= lang('item_no') ?></th>
                            <th class="text-center"><?= lang('cahing_value') ?></th>
                            <th class="text-center"><?= lang('bank_name') ?></th>
                            <th class="text-center"><?= lang('account_holder_name') ?></th>
                            <th class="text-center"><?= lang('phone') ?></th>
                            <th class="text-center"><?= lang('country') ?></th>
                            <th class="text-center"><?= lang('account_number') ?></th>
                            <th class="text-center"><?= lang('beneficiary_address') ?></th>
                            <th class="text-center"><?= lang('swift_code') ?></th>
                            <th class="text-center"><?= lang('city') ?></th>
                            <th class="text-center"><?= lang('caching_reason') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty(@$cachings)): ?>
                            <?php foreach (@$cachings as $c): ?>
                                <tr>
                                    <td class="relative" style="min-width: 110px;vertical-align: bottom"><?= $c->caching_date ?><?= hijjri($c->caching_date); ?></td>
                                    <td><?= lang('cahing_type' . $c->cahing_type) ?></td>
                                    <td><?= $c->name ?></td>
                                    <td><?= $c->bill_number ?></td>
                                    <td><?= $c->item_no ?></td>
                                    <td><?= $c->value ?> <?= lang('rial') ?></td>
                                    <td><?= $c->bank_name ?></td>
                                    <td><?= $c->account_holder_name ?></td>
                                    <td><?= $c->phone ?></td>
                                    <td><?= $c->country ?></td>
                                    <td><?= $c->account_number ?></td>
                                    <td><?= $c->address_in_leave ?></td>
                                    <td><?= $c->swift_code ?></td>
                                    <td><?= $c->city ?></td>
                                    <td><?= $c->caching_reason ?></td>
                                    <td>
                                        <?php if($c->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$c->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($c->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$c->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>employee/employee/delete_caching/<?= $c->caching_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td class="text-center" colspan="15"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br><br><br><br>


                    <form enctype="multipart/form-data"  action="<?= base_url() ?>employee/employee/save_caching/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form2"><br>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('cahing_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="cahing_type" class="form-control" required="">
                                    <option value="0"><?= lang('cahing_type0') ?></option>
                                    <option value="1"><?= lang('cahing_type1') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('beneficiary_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="name" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('bill_number') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="bill_number" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('item_no') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="item_no" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('cahing_value') ?> (<?= lang('rial') ?>)
                                <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="value" type="number" min="1" class="form-control"
                                       required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('bank_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="bank_name" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('account_holder_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="account_holder_name" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('phone') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="number" name="phone" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('country') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="country" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('additional_bank_information') ?></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('account_number') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="account_number" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('beneficiary_address') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="address_in_leave" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('swift_code') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="swift_code" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('city') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input type="text" name="city" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('caching_reason') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <textarea name="caching_reason" class="form-control" rows="6"
                                          required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- caching -->


            <!-- cutodies -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block">
                    <strong><?= lang('cutodies') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('custody_reference') ?></th>
                                    <th class="text-center"><?= lang('custody_delivery_date') ?></th>
                                    <th class="text-center"><?= lang('custody_nombre') ?></th>
                                    <th class="text-center"><?= lang('name') ?></th>
                                    <th class="text-center"><?= lang('description') ?></th>
                                    <th class="text-center"><?= lang('files') ?></th>
                                    <th class="text-center"><?= lang('receipt_confirmation') ?></th>
                                    <th class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$custodies_list)): ?>
                                    <?php foreach (@$custodies_list as $cl): ?>
                                        <tr>
                                            <td><?= $cl->custody_reference; ?></td>
                                            <td class="relative" style="vertical-align: bottom;padding-bottom: 3px;"><?= $cl->delivery_date; ?><?= hijjri($cl->delivery_date); ?></td>
                                            <td><?= $cl->nombre; ?></td>
                                            <td><?= ($lang == 'english') ? $cl->name_en : $cl->name_ar; ?></td>
                                            <td width="25%"><?= ($lang == 'english') ? $cl->description_en : $cl->description_ar; ?></td>
                                            <td>
                                                <?php if (!empty($cl->document)): ?>
                                                    <a target="_blank"
                                                       href="<?= base_url() ?>img/uploads/<?= $cl->document; ?>"><b><?= lang('download') ?></b></a>
                                                <?php else: ?>
                                                    <?= lang('no-exist') ?>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($cl->received == 1): ?>
                                                    <i class="fa fa-check" style="color:#4cae4c"></i>
                                                <?php else: ?>
                                                    <i class="fa fa-check" style="color:#d43f3a"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <?php if ($cl->received == 1): ?>
                                                        <a class="btn btn-warning btn-xs" disabled=""><i
                                                                    class="fa fa-star"></i> <?= lang('receipt_confirmation') ?>
                                                        </a>
                                                    <?php else: ?>
                                                        <a href="<?= base_url() ?>employee/employee/receipt_confirmation2/<?= $cl->custody_id ?>/<?= $employee_info->employee_id ?>"
                                                           class="btn btn-warning btn-xs"><i
                                                                    class="fa fa-star"></i> <?= lang('receipt_confirmation') ?>
                                                        </a>
                                                    <?php endif; ?>
                                                    <a href="#" class="btn btn-success btn-xs" data-toggle="modal"
                                                       data-target="#custodyModal<?= $cl->custody_id ?>"><i
                                                                class="fa fa-edit"></i> <?= lang('edit') ?></a>
                                                <?php endif; ?>
                                                <a class="btn btn-primary btn-xs"
                                                   onclick="print_custody('custody_<?= $cl->custody_id ?>');"><i
                                                            class="fa fa-print"></i> <?= lang('print') ?></a>
                                                <a href="<?= base_url() ?>employee/employee/delete_custody2/<?= $cl->custody_id ?>/<?= $employee_info->employee_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7" class="text-center">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <br><br><br><br>
                            <form enctype="multipart/form-data"  action="<?= base_url() ?>employee/employee/save_custody2/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_reference') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="number" min="0" name="custody_reference" id="custody_reference"
                                               class="form-control" required="">
                                    </div>
                                    <div class="col-sm-2"><a class="btn btn-primary btn-block"
                                                             onclick="generate()"><?= lang('generate_auto') ?></a></div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_name_ar') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_ar" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_name_en') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_en" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_delivery_date') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="delivery_date" class="form-control hijri_datepicker"
                                               required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_nombre') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" name="nombre" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('upload_document') ?></label>
                                    <div class="col-sm-3">
                                        <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                            <div class="btn btn-primary btn-file btn-block"><span
                                                        class="fileinput-new"><?= lang('select_file') ?></span>
                                                <span class="fileinput-exists"><?= lang('change') ?></span>
                                                <input type="file" name="document" class="form-control">
                                            </div>
                                            <div class="fileinput-filename"></div>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                               style="float: none; color:red;">&times;</a>
                                        </div>
                                        <div id="msg_pdf" style="color: #e11221"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_description_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="description_ar" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_description_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="description_en" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group margin no-attachment">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Start Modal -->
                    <? if (!empty(@$custodies_list)): ?>
                        <? foreach (@$custodies_list as $cl): ?>
                            <div class="modal fade" id="custodyModal<?= $cl->custody_id ?>" tabindex="-1"
                                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="font-size: 1.2em;">
                                        <div class="modal-body">
                                            <form enctype="multipart/form-data"  action="<?= base_url() ?>employee/employee/save_custody2/<?= $employee_info->employee_id ?>/<?= $cl->custody_id ?>"
                                                  method="post" class="form form-horizontal" id="formi"
                                                  enctype="multipart/form-data">
                                                <style>
                                                    .modal-body {
                                                        padding: 40px 15px !important;
                                                    }

                                                    .modal-body labels {
                                                        font-weight: normal !important;
                                                    }
                                                </style>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_reference') ?>
                                                        <span class="required">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="number" class="form-control" disabled="disabled"
                                                               value="<?= $cl->custody_reference; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_name_ar') ?>
                                                        <span class="required">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="name_ar" class="form-control"
                                                               required="" value="<?= $cl->name_ar; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_name_en') ?>
                                                        <span class="required">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="name_en" class="form-control"
                                                               required="" value="<?= $cl->name_en; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_delivery_date') ?>
                                                        <span
                                                                class="required">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="delivery_date"
                                                               class="form-control hijri_datepicker"
                                                               required="" value="<?= $cl->delivery_date; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_nombre') ?>
                                                        <span class="required">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="nombre" class="form-control"
                                                               required="" value="<?= $cl->nombre; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?= lang('upload_document') ?></label>
                                                    <div class="col-sm-4">
                                                        <div class="fileinput fileinput-new col-sm-9"
                                                             data-provides="fileinput">
                                                            <div class="btn btn-primary btn-file btn-block"><span
                                                                        class="fileinput-new"><?= lang('select_file') ?></span>
                                                                <span class="fileinput-exists"><?= lang('change') ?></span>
                                                                <input type="file" name="document" class="form-control">
                                                            </div>
                                                            <div class="fileinput-filename"></div>
                                                            <a href="#" class="close fileinput-exists"
                                                               data-dismiss="fileinput"
                                                               style="float: none; color:red;">&times;</a>
                                                        </div>
                                                        <div id="msg_pdf" style="color: #e11221"></div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php if (!empty($cl->document)): ?>
                                                            <a target="_blank"
                                                               href="<?= base_url() ?>img/uploads/<?= $cl->document; ?>"><b><?= lang('download') ?></b></a>
                                                        <?php else: ?>
                                                            <?= lang('no-exist') ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_description_ar') ?></label>
                                                    <div class="col-sm-7">
                                                        <textarea name="description_ar" rows="5"
                                                                  class="form-control"><?= $cl->description_ar; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-1"
                                                           class="col-sm-3 control-label"><?= lang('custody_description_en') ?></label>
                                                    <div class="col-sm-7">
                                                        <textarea name="description_en" rows="5"
                                                                  class="form-control"><?= $cl->description_en; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group margin no-attachment">
                                                    <div class="col-sm-offset-3 col-sm-7">
                                                        <button type="submit" id="sbtn"
                                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    <? endif; ?>
                    <!-- End Modal -->
                </div>
            </div>
            <!-- cutodies -->


            <!-- transfer -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block !important;">
                    <strong><?= lang('transfer') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <p><b><?= lang('current_data') ?> : </b></p>
                    <table>
                        <tr>
                            <td width="200px"><?= lang('current_job_title') ?> :</td>
                            <td><?= ($lang == 'english') ? $employee_info->job_titles_name_en : $employee_info->job_titles_name_ar; ?></td>
                        </tr>
                        <tr>
                            <td width="200px"><?= lang('current_deaptment') ?> :</td>
                            <td><?= ($lang == 'english') ? $employee_info->department_name : $employee_info->department_name_ar; ?></td>
                        </tr>
                        <tr>
                            <td width="200px"><?= lang('current_designation') ?> :</td>
                            <td><?= ($employee_info->designations_id) ? (($lang == 'english') ? $employee_info->designations : $employee_info->designations_ar) : lang('no-exist'); ?></td>
                        </tr>
                        <tr>
                            <td width="200px"><?= lang('current_emp_category') ?> :</td>
                            <td><?= ($lang == 'english') ? $employee_info->name_en : $employee_info->name_ar; ?></td>
                        </tr>
                        <tr>
                            <td width="200px"><?= lang('current_job_time') ?> :</td>
                            <td><?= lang($employee_info->job_time) ?></td>
                        </tr>
                    </table>
                    <br><br>
                    <p><b><?= lang('new_data') ?> : </b></p>
                    <form enctype="multipart/form-data"  enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/transfer/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('job_title') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="job_title" class="form-control" required="">
                                    <option value=""></option>
                                    <?php if (!empty(@$job_titles_list)): ?>
                                        <?php foreach (@$job_titles_list as $jt): ?>
                                            <option <?= ($employee_info->job_titles_id == $jt->job_titles_id) ? 'selected' : ''; ?>
                                                    value="<?= $jt->job_titles_id ?>"><?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('department') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="departement_id" class="form-control" required=""
                                        onchange="print_sects(this.value)">
                                    <option value=""></option>
                                    <?php if (!empty(@$department_list)): ?>
                                        <?php foreach (@$department_list as $dep): ?>
                                            <option <?= ($employee_info->departement_id == $dep->department_id) ? 'selected' : ''; ?>
                                                    value="<?= $dep->department_id ?>"><?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('designation') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select id="x3" name="designations_id" class="form-control" required="">
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="col-sm-1 spinner_sects"><i
                                        class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('employee_cat') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="employee_category_id" class="form-control" required="">
                                    <option value=""></option>
                                    <?php if (!empty(@$emp_cat_list)): ?>
                                        <?php foreach (@$emp_cat_list as $emp_cat): ?>
                                            <option <?= ($employee_info->employee_category_id == $emp_cat->id) ? 'selected' : ''; ?>
                                                    value="<?= $emp_cat->id ?>"><?= ($lang == 'english') ? $emp_cat->name_en : $emp_cat->name_ar; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('job_time') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="job_time" class="form-control" required="">
                                    <option value=""></option>
                                    <option value="Full" <?= ($employee_info->job_time == 'Full') ? 'selected' : ''; ?>><?= lang('Full') ?></option>
                                    <option value="Part" <?= ($employee_info->job_time == 'Part') ? 'selected' : ''; ?>><?= lang('Part') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group margin no-attachment">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- transfer -->


            <!-- la_training -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block !important;">
                    <strong><?= lang('la_training') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('course_name') ?></th>
                            <th class="text-center"><?= lang('course_institute_name') ?></th>
                            <th class="text-center"><?= lang('start_date') ?></th>
                            <th class="text-center"><?= lang('end_date') ?></th>
                            <th class="text-center"><?= lang('course_price') ?></th>
                            <th class="text-center"><?= lang('notes') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty(@$courses_list)): ?>
                            <?php foreach (@$courses_list as $course): ?>
                                <tr>
                                    <td width="9%"><?= $course->course_name ?></td>
                                    <td><?= $course->course_institute_name ?></td>
                                    <td class="relative" style="padding-bottom: 3px; vertical-align: bottom;"><?= $course->start_date ?><?= hijjri($course->start_date); ?></td>
                                    <td class="relative" style="padding-bottom: 3px; vertical-align: bottom;"><?= $course->end_date ?><?= hijjri($course->end_date); ?></td>
                                    <td width="9%" class="text-center"><?= $course->course_price ?></td>
                                    <td width="30%"><?= $course->course_note ?></td>
                                    <td>
                                        <?php if($course->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$course->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($course->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$course->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>employee/employee/delete_training/<?= $course->course_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <? endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="9" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <form enctype="multipart/form-data"  enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/savetraining/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form7">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('course_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="course_name" type="text" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('course_institute_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="course_institute_name" type="text" class="form-control"
                                       required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('start_date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input name="start_date" type="text"
                                       class="form-control hijri_datepicker5" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('end_date') ?></label>
                            <div class="col-md-3">
                                <input name="end_date" type="text"
                                       class="form-control hijri_datepicker6"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('course_price') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input name="course_price" type="number" min="1" class="form-control"
                                       required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('notes') ?></label>
                            <div class="col-md-7">
                                <textarea name="course_note" class="form-control" rows="6"></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- la_training -->


            <!-- extra_hours -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block">
                    <strong><?= lang('extra_hours') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('extra_work_type') ?></th>
                            <th class="text-center"><?= lang('work_value') ?></th>
                            <th class="text-center"><?= lang('num_hours') ?></th>
                            <th class="text-center"><?= lang('permission_star') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($extra_hours_list)): ?>
                            <?php foreach ($extra_hours_list as $xh): ?>
                                <tr>
                                    <td class="text-center">
                                        <? foreach (@$extra_work_list as $ew): ?>
                                            <? if ($ew->extra_work_id == $xh->extra_work_id): ?>
                                                <?= ($lang == 'english') ? $ew->title_en : $ew->title_ar; ?>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </td>
                                    <td class="text-center">
                                        <? foreach (@$extra_work_list as $ew): ?>
                                            <? if ($ew->extra_work_id == $xh->extra_work_id): ?>
                                                <?= $ew->work_value; ?> %
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </td>
                                    <td class="text-center"><?= $xh->num_hours ?></td>
                                    <td style="direction: ltr !important;text-align: center;"><?= $xh->start_time ?></td>
                                    <td>
                                        <?php if($xh->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$xh->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($xh->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$xh->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>employee/employee/delete_extra_hours/<?= $xh->extra_hours_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_extra_hours/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form2"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('extra_work_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="extra_work_id" class="form-control extra_work_id" required="">
                                    <option value=""></option>
                                    <?php if (!empty(@$extra_work_list)): ?>
                                        <?php foreach (@$extra_work_list as $ew): ?>
                                            <option value="<?= $ew->extra_work_id ?>"
                                                    data-value="<?= $ew->work_value ?>">
                                                <?= ($lang == 'english') ? $ew->title_en : $ew->title_ar; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('work_value') ?></label>
                            <div class="col-md-3">
                                <input name="work_value" type="text" class="form-control work_value" disabled/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('num_hours') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input name="num_hours" type="number" min="0" max="12" class="form-control num_hours"
                                       required/>
                            </div>
                        </div>

                        <link rel="stylesheet" href="<?= base_url() ?>asset/css/plugins/wickedpicker.min.css">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('permission_star') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-5">
                                <input name="start_time" type="text" class="form-control" id="wickedpicker1" required=""
                                       style="width: 110px"/>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- extra_hours -->


            <!-- la_purchase -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block">
                    <strong><?= lang('la_purchase') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('prod_name') ?></th>
                            <th class="text-center"><?= lang('prod_desc') ?></th>
                            <th class="text-center"><?= lang('prod_num') ?></th>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('prod_note') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($purchase_list)): ?>
                            <?php foreach ($purchase_list as $pr): ?>
                                <tr>
                                    <td><?= $pr->prod_name ?></td>
                                    <td><?= $pr->prod_desc ?></td>
                                    <td width="7%" class="text-center"><?= $pr->prod_num ?></td>
                                    <td width="10%" class="text-center relative" style="vertical-align: bottom; padding-bottom: 3px;">
                                        <?= hijjri($pr->purchase_date); ?>
                                        <?= $pr->purchase_date ?>
                                    </td>
                                    <td width="30%"><?= $pr->prod_note ?></td>
                                    <td>
                                        <?php if($pr->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$pr->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($pr->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$pr->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td width="7%" class="text-center">
                                        <a href="<?= base_url() ?>employee/employee/delete_purchase/<?= $pr->purchase_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_purchase/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form4"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('prod_name') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="prod_name" type="text" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('prod_desc') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="prod_desc" type="text" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('prod_num') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input name="prod_num" type="number" min="1" class="form-control" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('prod_note') ?> </label>
                            <div class="col-md-7">
                                <textarea name="prod_note" rows="6" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- la_purchase -->


            <!-- la_maintenance -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block">
                    <strong><?= lang('la_maintenance') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('maintenance_title') ?></th>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('notes') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($maintenace_list)): ?>
                            <?php foreach ($maintenace_list as $mn): ?>
                                <tr>
                                    <td><?= $mn->maintenance_title ?></td>
                                    <td width="10%" class="text-center relative" style="vertical-align: bottom; padding-bottom: 3px;">
                                        <?= hijjri($mn->maintenance_date); ?>
                                        <?= $mn->maintenance_date ?>
                                    </td>
                                    <td width="40%"><?= $mn->maintenance_notes ?></td>
                                    <td>
                                        <?php if($mn->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$mn->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($mn->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$mn->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td width="7%" class="text-center">
                                        <a href="<?= base_url() ?>employee/employee/delete_maintenance/<?= $mn->maintenance_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_maintenance/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form4"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('maintenance_title') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="maintenance_title" type="text" class="form-control" required=""/>
                            </div>

                            <label class="col-sm-3 control-label"><?= lang('notes') ?> </label>
                            <div class="col-md-7">
                                <textarea name="maintenance_notes" class="form-control" rows="6"></textarea>
                            </div>

                            <div class="form-group margin">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- la_maintenance -->


            <!-- la_permission -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                     style="cursor: pointer; display: block">
                    <strong><?= lang('la_permission') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('permission_type') ?></th>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('permission_star') ?></th>
                            <th class="text-center"><?= lang('permission_end') ?></th>
                            <th class="text-center"><?= lang('reason') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($permissions_list)): ?>
                            <?php foreach ($permissions_list as $prm): ?>
                                <tr>
                                    <td><?= lang("permission_opt" . $prm->permission_type); ?></td>
                                    <td class="text-center relative" style="vertical-align: bottom; padding-bottom: 3px;">
                                        <?= hijjri($prm->permission_date); ?>
                                        <?= $prm->permission_date ?>
                                    </td>
                                    <td style="direction: ltr !important; text-align: center"><?= $prm->permission_star ?></td>
                                    <td style="direction: ltr !important; text-align: center"><?= $prm->permission_end ?></td>
                                    <td width="30%"><?= $prm->reason ?></td>
                                    <td>
                                        <?php if($prm->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$prm->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($prm->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$prm->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>employee/employee/delete_permission/<?= $prm->permission_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_permission/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form4"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('permission_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select name="permission_type" class="form-control" required="">
                                    <option value=""></option>
                                    <option value="1"><?= lang('permission_opt1') ?></option>
                                    <option value="2"><?= lang('permission_opt2') ?></option>
                                    <option value="3"><?= lang('permission_opt3') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('permission_time') ?>
                                :</label>
                            <div class="col-md-7">
                            </div>
                        </div>
                        <link rel="stylesheet" href="<?= base_url() ?>asset/css/plugins/wickedpicker.min.css">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('permission_star') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="permission_star" type="text" class="form-control" id="wickedpicker3"
                                       required=""
                                       style="width: 110px; direction: ltr !important;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('permission_end') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="permission_end" type="text" class="form-control" id="wickedpicker4"
                                       required=""
                                       style="width: 110px; direction: ltr !important;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                                <textarea name="reason" class="form-control" rows="6"
                                                          required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- la_permission -->


            <!-- recrutements -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()" style="cursor: pointer;">
                    <strong><?= lang('recrutements') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('course_institute') ?></th>
                            <th class="text-center"><?= lang('recrutement_task') ?></th>
                            <th class="text-center"><?= lang('recrutement_type') ?></th>
                            <th class="text-center"><?= lang('from_institute') ?></th>
                            <th class="text-center"><?= lang('going_date') ?></th>
                            <th class="text-center"><?= lang('coming_date') ?></th>
                            <th class="text-center"><?= lang('duration') ?> (<?= lang('days') ?>)</th>
                            <th class="text-center"><?= lang('details') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($recrutements)): ?>
                            <?php foreach ($recrutements as $rc): ?>
                                <tr>
                                    <td><?= $rc->course_institute ?></td>
                                    <td><?= $rc->name ?></td>
                                    <td><?= ($rc->type == 1) ? lang('internal') : lang('external'); ?></td>
                                    <td><?= $rc->address_in_leave ?></td>
                                    <td class="text-center relative" style="vertical-align: bottom; padding-bottom: 3px;">
                                        <?= hijjri($rc->going_date); ?>
                                        <?= $rc->going_date ?>
                                    </td>
                                    <td class="text-center relative" style="vertical-align: bottom; padding-bottom: 3px;">
                                        <?= hijjri($rc->coming_date); ?>
                                        <?= $rc->coming_date ?>
                                    </td>
                                    <td>
                                        <?= $rc->leave_duration ?>
                                    </td>
                                    <td width="20%"><?= $rc->note ?></td>
                                    <td>
                                        <?php if($rc->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$rc->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($rc->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$rc->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() ?>employee/employee/delete_recrutement/<?= $rc->recrutement_id ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="9" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_recrutement/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form2"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('course_institute') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input tye="" text name="course_institute" class="form-control"
                                       required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('recrutement_task') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input tye="" text name="name" class="form-control"
                                       required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('recrutement_type') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <select text name="type" class="form-control" required="">
                                    <option value="1"><?= lang('internal') ?></option>
                                    <option value="2"><?= lang('external') ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('from_institute') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input tye="" text name="address_in_leave" class="form-control"
                                       required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('going_date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input tye="" text name="going_date"
                                       class="form-control hijri_datepicker3" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('coming_date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input tye="" text name="coming_date"
                                       class="form-control hijri_datepicker4" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('duration') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input name="leave_duration" id="data-duration2" type="number" min="0"
                                       class="form-control"
                                       style="width:100px" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('details') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- recrutements -->


            <!-- la_embarkation -->
            <div class="panel panel-primary">
                <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()" style="cursor: pointer;">
                    <strong><?= lang('la_embarkation') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th><?= lang('coming_date_after_vacation') ?></th>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('the_rest') ?> (<?= lang('days') ?>)</th>
                            <th><?= lang('notes') ?></th>
                            <th class="text-center"><?=($lang=='arabic')?'رقم الطلب':'Application';?></th>
                            <th class="text-center"><?= lang("attachment") ?></th>
                            <th><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($embarkations_list)): ?>
                            <?php foreach ($embarkations_list as $emb): ?>
                                <tr>
                                    <td class="relative" style="vertical-align: bottom; padding-bottom: 3px;"><?= $emb->date1; ?><?= hijjri($emb->date1); ?></td>
                                    <td class="relative" style="vertical-align: bottom; padding-bottom: 3px;"><?= $emb->date2; ?><?= hijjri($emb->date2); ?></td>
                                    <td class="text-center"><?= $emb->rest; ?></td>
                                    <td><?= $emb->note; ?></td>
                                    <td>
                                        <?php if($emb->app_id==0): ?>
                                            <?=lang('no-exist');?>
                                        <?php else: ?>
                                            <?=$emb->app_id;?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($emb->file): ?>
                                            <a class="btn btn-xs btn-info" href="<?=base_url()?>img/uploads/<?=$emb->file;?>" target="_blank">
                                                <i class="fa fa-download"></i> <?=lang('download');?>
                                            </a>
                                        <?php else: ?>
                                            <?=lang('no-exist');?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>employee/employee/delete_embarkation/<?= $emb->embarkation_id; ?>/<?= $employee_info->employee_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/employee/save_embarkation/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form2"><br>
                        <div class="form-group">
                            <label
                                    class="col-sm-3 control-label"><?= lang('coming_date_after_vacation') ?>
                                <span class="required"> *</span></label>
                            <div class="col-md-3">
                                <input tye="text" name="date1"
                                       class="form-control hijri_datepicker" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-7">
                                <input type="text" class="form-control note" disabled=""
                                       value="<?= lang('embarkation_note1') ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('date') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-3">
                                <input tye="" text name="date2"
                                       class="form-control hijri_datepicker" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('embarkation_note2') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="rest" type="number" min="0" value="0" class="form-control"
                                       style="width:100px" required="">
                                <?= lang('day') ?> / <?= lang('days') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('notes') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <textarea name="note" class="form-control" rows="6" required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- la_embarkation -->
        </div>
    </div>
</div>

<? if (!empty(@$custodies_list)): ?>
    <? foreach (@$custodies_list as $cl): ?>
        <div class="print" id="custody_<?= $cl->custody_id ?>">
            <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
            <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
            <?php if ($lang == "arabic"): ?>
                <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css" rel="stylesheet"/>
            <?php endif; ?>
            <style type="text/css">
                .print {
                    border: 1px solid gray;
                    min-height: 200px;
                    margin-bottom: 100px;
                    padding: 100px;
                }

                table {
                    width: 100%
                }
            </style>
            <table>
                <tr>
                    <td width="10%"></td>
                    <td>
                        <h3><?= lang("custody_details"); ?></h3>
                    </td>
                    <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                    <td width="10%"></td>
                    <td>
                        <table class="table table-bordered" style="">
                            <tr>
                                <td><?= lang("employee_name") ?></td>
                                <td><?= ($lang == "arabic") ? $employee_info->full_name_ar : $employee_info->full_name_en; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_reference") ?></td>
                                <td><?= $cl->custody_reference; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_delivery_date") ?></td>
                                <td><?= $cl->delivery_date; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_nombre") ?></td>
                                <td><?= $cl->nombre; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("name") ?></td>
                                <td><?= ($lang == 'english') ? $cl->name_en : $cl->name_ar; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("description") ?></td>
                                <td><?= ($lang == 'english') ? $cl->description_en : $cl->description_ar; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("receipt_confirmation") ?></td>
                                <td>
                                    <?php if ($cl->received == 1): ?>
                                        <?= lang("yes"); ?>
                                    <?php else: ?>
                                        <?= lang("no"); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br><br>
            <table>
                <tr>
                    <td width="65%"></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-center"><?= lang("employee_signature") ?></td>
                            </tr>
                            <tr>
                                <td style="height: 70px"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td width="65%"></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-center"><?= lang("manager_signature") ?></td>
                            </tr>
                            <tr>
                                <td style="height: 140px"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
        </div>
    <? endforeach; ?>
<? endif ?>

<script type="text/javascript">

    function print_custody(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }

    function set_data(s) {
        if (s.val()) {

            $('#data-duration').val(s.find(':selected').attr('data-duration'));
            $('#data-quota').val(s.find(':selected').attr('data-quota'));
            $('#data-affect-stock').val(s.find(':selected').attr('data-affect-stock'));
            $('#data-calc-type').val(s.find(':selected').attr('data-calc-type'));
            if (s.find(':selected').attr('data-replacement') == 1) {
                $('#data-replacement').html("<?= lang('required') ?>");
                $('.required1').fadeIn();
                $(".replacement").prop('required', 'true');
            } else {
                $('#data-replacement').html("<?= lang('non_required') ?>");
                $('.required1').fadeOut();
                $(".replacement").prop('required', false);
            }
            if (s.find(':selected').attr('data-leave-tel') == 1) {
                $('#leave-tel').html("<?= lang('required') ?>");
                $('.required2').fadeIn();
                $(".leave-tel").prop('required', 'true');
            } else {
                $('#leave-tel').html("<?= lang('non_required') ?>");
                $('.required2').fadeOut();
                $(".leave-tel").prop('required', false);
            }

            /////////////
            if (s.val() == 1) {
                $('.note2').fadeIn();
                $('#data-duration').val(<?= @$rest; ?>);
            }
            else
                $('.note2').fadeOut();
            /////////////
        } else {
            $('.note2').fadeOut();
            $('#data-duration').val('');
            $('#data-quota').val();
            $('#data-affect-stock').val();
            $('#data-calc-type').val();
            /////////////////////////////////
            $('#data-replacement').html("<?= lang('non_required') ?>");
            $('.required1').fadeOut();
            $(".replacement").prop('required', false);
            /////////////////////////////////
            $('#leave-tel').html("<?= lang('non_required') ?>");
            $('.required2').fadeOut();
            $(".leave-tel").prop('required', false);
        }
        validatedate();
    }
</script>

<script type="text/javascript">
    $(function () {
        print_sects(<?=$employee_info->departement_id;?>);
        var calendar = $.calendars.instance('ummalqura');
        $('.hijri_datepicker').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
        $('.hijri_datepickerx').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate();
        });
        $('.hijri_datepickery').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate();
        });
        $('.hijri_datepicker3').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate2();
        });
        $('.hijri_datepicker4').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate2();
        });
        $('.hijri_datepicker5').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
        $('.hijri_datepicker6').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
    });

    $(function () {
        $('form').map(function () {
            $(this).validate({rules: {name: "required"}});
        });
        $('.extra_work_id').on('change', function () {
            if ($(this).val()) {
                $('.work_value').val($(':selected', this).attr('data-value') + " %");
            }
        });
        $('.advance-value').on('change textInput input', function () {
            if ($(this).val()) {
                $('.payement-method').attr('disabled', false);
                $('.monthly-installement').attr('disabled', false);
                payementmethod($('.payement-method'));
            }
            else {
                $('.payement-method').attr('disabled', true);
                $('.monthly-installement').attr('disabled', true);
            }
        });
        $('.monthly-installement').on('change textInput input', function () {
            pm = $('.payement-method').val();
            av = $('.advance-value').val();
            mi = $(this).val();

            if (!mi || mi == 0 || !$.isNumeric(mi)) {
                $('.payement-months').val(0);
                $('.monthly-installement').val(0);
                return;
            }
            if (pm == 3) {
                if (av < eval(mi)) {
                    $('.monthly-installement').val(0);
                }
                else {
                    var nummonths = Math.ceil(av / parseFloat(mi, 10));
                    $('.payement-months').val(nummonths);
                }
            }

        });

        $('#data-duration').on('change', function () {
            validatedate();
        });
        $('#data-duration2').on('change', function () {
            validatedate2();
        });
    });


</script>

<script type="text/javascript">

    function payementmethod(s) {
        pm = s.val();
        av = $('.advance-value').val();
        if (pm == 1) {
            var res = av / 12;
            $('.monthly-installement').val(res.toFixed(2));
            $('.payement-months').val(12);
        }
        else if (pm == 2) {
            <?php
            $date1 = new DateTime($today);
            $date2 = new DateTime(@$employee_info->retirement_date);
            $months = ceil($date2->diff($date1)->format("%a") / 30);
            ?>
            var nummonths = <?=$months?>;
            var res = av / nummonths;
            $('.monthly-installement').val(res.toFixed(2));
            $('.payement-months').val(nummonths);
        }
        else if (pm == 3) {
            if ($('.monthly-installement').val() != 0) {
                var res = av / $('.monthly-installement').val();
                $('.payement-months').val(Math.ceil(res));
            }
            else {
                $('.payement-months').val(0);
            }
        }
        else {
            $('.monthly-installement').val();
            $('.payement-months').val(0);
        }
    }
    function validatedate() {
        dategoing = $('.hijri_datepickerx').val();
        datecoming = $('.hijri_datepickery').val();
        duration = $('#data-duration2').val();

        if(dategoing && datecoming){
            dateDiffInDays(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    function dateDiffInDays(a, b) {
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        res = Math.floor((utc2 - utc1) / _MS_PER_DAY) + 1;

        if (res < 0) {
            $('.hijri_datepickery').val('');
            $('#data-duration').val('');
        }
        else {
            $('#data-duration').val(res);
        }
    }
    function validatedate2() {
        dategoing = $('.hijri_datepicker3').val();
        datecoming = $('.hijri_datepicker4').val();
        duration = $('#data-duration2').val();

        if(dategoing && datecoming){
            dateDiffInDays2(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }
    function dateDiffInDays2(a, b) {

        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        res = Math.floor((utc2 - utc1) / _MS_PER_DAY) + 1;

        if (res < 0) {
            $('.hijri_datepicker4').val('');
            $('#data-duration2').val('');
        }
        else {
            $('#data-duration2').val(res);
        }
    }
    function intPart(floatNum) {
        if (floatNum < -0.0000001) {
            return Math.ceil(floatNum - 0.0000001);
        }
        return Math.floor(floatNum + 0.0000001);
    }
    function HijriToGreg(arabicDate) {

        if (arabicDate == "") {
            return "";
        }
        //declare a date format year,month,day sequence
        var jd;
        var jd1;
        var l;
        var j;
        var n;
        var wd;
        var i;
        var k;
        arabicDate = arabicDate.split("-");
        var d = parseInt(arabicDate[2]);
        var m = parseInt(arabicDate[1]);
        var y = parseInt(arabicDate[0]);

        //var delta = 0;//its load from config.js
        var english_date = new Array(0, 0, 0);
        var delta = 1;
        //added delta=1 on jd to comply isna rulling for hajj 2007

        //delta = delta_array_hijri[arabicDate[1] - 1];


        jd = intPart((11 * y + 3) / 30) + 354 * y + 30 * m - intPart((m - 1) / 2) + d + 1948440 - 385 - delta;
        //arg.JD.value=jd
        //wd = weekDay(jd % 7); // no use of this line

        if (jd > 2299160) {
            l = jd + 68569;
            n = intPart((4 * l) / 146097);
            l = l - intPart((146097 * n + 3) / 4);
            i = intPart((4000 * (l + 1)) / 1461001);
            l = l - intPart((1461 * i) / 4) + 31;
            j = intPart((80 * l) / 2447);
            d = l - intPart((2447 * j) / 80);
            l = intPart(j / 11);
            m = j + 2 - 12 * l;
            y = 100 * (n - 49) + i + l;
        }
        else {
            j = jd + 1402;
            k = intPart((j - 1) / 1461);
            l = j - 1461 * k;
            n = intPart((l - 1) / 365) - intPart(l / 1461);
            i = l - 365 * n + 30;
            j = intPart((80 * i) / 2447);
            d = i - intPart((2447 * j) / 80);
            i = intPart(j / 11);
            m = j + 2 - 12 * i;
            y = 4 * k + n + i - 4716;
        }

        english_date[2] = d;
        english_date[1] = m;
        english_date[0] = y;

        return y + '-' + m + '-' + d;

    }
    function generate() {
        var val = makeid();
        $('#custody_reference').val(val);
    }
    function makeid() {
        var text = "";
        var possible = "0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    var lang = "<?= $this->session->userdata('lang'); ?>";
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>employee/dashboard/get_sec_with_dep/" + id;
        if (id) {
            $('.spinner_sects').fadeIn('fast');

            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x3').empty();
                    $('#x3').append('<option value=""></option>');
                    $('#x3').append('<option value="0"><?=lang("without_section")?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                    $('#x3').val('<?=$employee_info->designations_id?>');
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
            });
        } else {
            $('#x3').empty();
            $('#x3').append('<option value=""></option>');
        }

    }
</script>

<?php if ($lang == 'english'): ?>
    <script type="text/javascript" src="<?= base_url() ?>asset/js/plugins/wickedpicker.js"></script>
<?php else: ?>
    <script type="text/javascript" src="<?= base_url() ?>asset/js/plugins/wickedpicker-ar.js"></script>
<?php endif; ?>
<script type="text/javascript">
    $('#wickedpicker1').wickedpicker({title: <?=($lang == 'english') ? '"choose time"' : '"إختر الوقت"';?>});
    $('#wickedpicker2').wickedpicker({title: <?=($lang == 'english') ? '"choose time"' : '"إختر الوقت"';?>});
    $('#wickedpicker3').wickedpicker({title: <?=($lang == 'english') ? '"choose time"' : '"إختر الوقت"';?>});
    $('#wickedpicker4').wickedpicker({title: <?=($lang == 'english') ? '"choose time"' : '"إختر الوقت"';?>});
</script>

<script type="text/javascript">
    $(function () {
        var html = '<div class="form-group">' +
            '<label class="col-sm-3 control-label"><?= lang("attachment") ?></label>' +
            '<div class="col-sm-7">' +
            '<div class="fileinput fileinput-new" data-provides="fileinput">' +
            '<span class="btn btn-primary btn-file">' +
            '<span class="fileinput-new"><?= lang("select_file") ?></span>' +
            '<span class="fileinput-exists"><?= lang("change") ?></span>' +
            '<input type="file" name="file">' +
            '</span>' +
            '<span class="fileinput-filename"></span>' +
            '<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>' +
            '</div>' +
            '</div>' +
            '</div>';
        $(html).insertBefore('form .form-group.margin:not(.no-attachment)');


    });
</script>