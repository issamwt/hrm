<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang('results') ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php @$today = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                        <?php foreach (@$results as $result): ?>
                            <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                    class="fa fa-print"></i> <?= lang('print') ?></a>

                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == 'arabic'): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-primary.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 10px;
                                        padding-bottom: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 300px;
                                        /*height: 100px;*/
                                        padding: 10px 15px;
                                    <?php if($lang == 'english'):?> float: left;
                                    <?php else: ?> float: right;
                                    <?php endif; ?>
                                    }

                                    table {
                                        width: 100%;
                                    }

                                    table td, table th {
                                        padding: 2px;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <b><?= lang('name') ?>
                                                    : </b><?= ($lang == 'english') ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang('designation') ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == 'english') ? $result->designations : $result->designations_ar) : lang('no-exist'); ?>
                                                <br>
                                                <b><?= lang('department') ?>
                                                    : </b><?= ($lang == 'english') ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang('direct_manager') ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == 'english') ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                                -----------------------
                                                <br>
                                                <?php if ($lang == "english"): ?>
                                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                                <?php else: ?>
                                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                                <?php endif; ?>
                                                <b><?= lang("the_report") ?> : </b> <?= lang("caching") ?><br>
                                                <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                                <b><?= lang("year") ?>
                                                    : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 10px;"></div>
                                    <h2 style="margin:0px;" class="text-center"><?= lang('caching') ?></h2><br>
                                </div>
                                <div class="content">

                                    <div>
                                        <table border="1">
                                            <thead>
                                            <tr>
                                                <th class="text-center"><?= lang('date') ?></th>
                                                <th class="text-center"><?= lang('cahing_type') ?></th>
                                                <th class="text-center"><?= lang('beneficiary_name') ?></th>
                                                <th class="text-center"><?= lang('bill_number') ?></th>
                                                <th class="text-center"><?= lang('item_no') ?></th>
                                                <th class="text-center"><?= lang('cahing_value') ?></th>
                                                <th class="text-center"><?= lang('bank_name') ?></th>
                                                <th class="text-center"><?= lang('account_holder_name') ?></th>
                                                <th class="text-center"><?= lang('country') ?></th>
                                                <th class="text-center"><?= lang('account_number') ?></th>
                                                <th class="text-center"><?= lang('beneficiary_address') ?></th>
                                                <th class="text-center"><?= lang('swift_code') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach (@$cachings as $c): ?>
                                                <?php if ($c->employec_id == $result->employee_id): ?>
                                                    <?php if (strpos($c->caching_date, $filter) !== false) : ?>
                                                        <tr>
                                                            <td><?= $c->caching_date ?></td>
                                                            <td><?= lang('cahing_type' . $c->cahing_type) ?></td>
                                                            <td><?= $c->name ?></td>
                                                            <td><?= $c->bill_number ?></td>
                                                            <td><?= $c->item_no ?></td>
                                                            <td><?= $c->value ?> <?= lang('rial') ?></td>
                                                            <td><?= $c->bank_name ?></td>
                                                            <td><?= $c->account_holder_name ?></td>
                                                            <td><?= $c->country ?></td>
                                                            <td><?= $c->account_number ?></td>
                                                            <td><?= $c->address_in_leave ?></td>
                                                            <td><?= $c->swift_code ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


