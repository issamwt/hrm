<div class="col-md-12">
    <div class="box box-primary">
        <!-- Default panel contents -->


        <div class="table-container">

            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?= lang('close') ?></span></button>
            </div>
            <h3 class="text-center" style="margin-top: 0px"><?= lang('edit') ?></h3>
            <form class="form" method="post" id="the_form">
                <table class="table table-nonbordered  the-table">
                    <tr>
                        <td width="60%">اليوم</td>
                        <td>
                            <input name="att_date" type="text" value="<?= $attendace->att_date; ?>"
                                   class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td width="60%">الوقت</td>
                        <td>
                            <input name="att_time" type="text" value="<?= $attendace->att_time; ?>"
                                   class="form-control" required>
                        </td>
                    </tr>
                    <tr>
                        <td>دخول/خروج</td>
                        <td style="direction: ltr;text-align: right;">دخول
                            <input type="radio" name="Action"
                                   value="1" <?= ($attendace->Action == 1) ? 'checked="checked"' : ''; ?> required/>
                            <br>
                            خروج
                            <input type="radio" name="Action"
                                   value="2" <?= ($attendace->Action == 2) ? 'checked="checked"' : ''; ?> required/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="submit">
                                <button class="btn btn-block btn-primary"><?= lang('save') ?></button>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>


