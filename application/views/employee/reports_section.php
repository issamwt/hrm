<?php echo message_box('success'); ?>
<style type="text/css">
    .spinner_sects {
        display: none;
        font-size: 7px;
    }
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0"><br><br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><?= lang('options') ?> </strong>
            </div>
            <div class="panel-body"><br>
                <form action="" method="post" class="form form-horizontal" id="form" target="_blank"><br>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('report_type') ?> <span
                                    class="required"> *</span></label>
                        <div class="col-md-5">
                            <select name="report_type" class="form-control report_type" required="">
                                <option value=""></option>
                                <option value="accounting"><?= lang('accounting') ?></option>
                                <option value="la_training"><?= lang('la_trainings') ?></option>
                                <option value="cutodies"><?= lang('cutodies') ?></option>
                                <option value="finance_details"><?= lang('finance_details') ?></option>
                                <option value="caching"><?= lang('caching') ?></option>
                                <option value="personal_details"><?= lang('personal_details') ?></option>
                                <option value="evaluations"><?= lang('evaluations') ?></option>
                                <option value="recrutements"><?= lang('recrutements') ?></option>
                                <option value="attendances"><?= lang('attendances') ?></option>
                                <option value="vacations1"><?= lang('vacations') ?></option>
                                <option value="vacations"><?= lang('vacations_detailed') ?></option>
                                <option value="la_embarkation"><?= lang('la_embarkation') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('department') ?></label>
                        <div class="col-sm-5">
                            <select name="department" class="form-control department" required=""
                                    onchange="print_sects(this.value)">
                                <option value="0"><?= lang("all") ?></option>
                                <?php foreach ($department_list as $dep): ?>
                                    <option value="<?= $dep->department_id ?>">
                                        <?= ($lang == "arabic") ? $dep->department_name_ar : $dep->department_name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('designation') ?></label>
                        <div class="col-sm-5">
                            <select id="x3" name="designation" class="form-control designation" required="">
                                <option value="-1"><?= lang("all") ?></option>
                                <option value="0"><?= lang("without_section") ?></option>
                            </select>
                        </div>
                        <div class="col-sm-1 spinner_sects"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('month') ?> <span
                                    class="required"> *</span></label>
                        <div class="col-md-5">
                            <select name="month" class="form-control months" required="">
                                <option value="0"><?= lang('all') ?></option>
                            </select>
                        </div>
                    </div>
                    <script type="text/javascript">
                        <?php if($lang == 'english'):?>
                        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                        <?php else: ?>
                        var months = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
                        <?php endif; ?>

                        $(function () {
                            $.each(months, function (i, v) {
                                var num = pad(i + 1, 2);
                                $('.months').append('<option value="' + num + '">' + v + '</option>');
                            })
                        });
                    </script>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('year') ?> <span
                                    class="required"> *</span></label>
                        <div class="col-md-5">
                            <?php
                            $current_year = explode('-', $current_date)[0];
                            $start_year = $current_year - 5;
                            ?>
                            <select name="year" class="form-control years" required="">
                                <option value="0"><?= lang('all') ?></option>
                                <?php for ($i = $start_year; $i < ($start_year + 20); $i++): ?>
                                    <option value="<?= $i ?>" ><?= $i ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group margin">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="sbtn" name="submit" value="one"
                                    class="btn btn-success btn-block"><?= lang('report_by_employee') ?></button>
                            <button type="submit" id="sbtn2" name="submit" value="all"
                                    class="btn btn-success btn-block"><?= lang('report_all') ?></button>
                        </div>
                    </div>

                    <?php $emp_at_leave = array(); ?>
                    <?php foreach (@$leaves_list as $lv): ?>

                        <?php
                        $datex = date_create($current_date);
                        $dateg = date_create($lv->going_date);
                        $datec = date_create($lv->coming_date);

                        if ($datex > $dateg and $datex < $datec) {
                            array_push($emp_at_leave, $lv->employel_id);
                        }
                        ?>
                    <?php endforeach; ?>


                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th width="5%" class="text-center"><input type="checkbox" class="all ids" name="ids[]"
                                                                      value="0"
                                                                      style="cursor: pointer"/></th>
                            <th style="width:72px" class="text-center"><?= lang('image') ?></th>
                            <th class="text-center"><?= lang('name') ?></th>
                            <th class="text-center"><?= lang('job_title') ?></th>
                            <th class="text-center"><?= lang('designation') ?></th>
                            <th class="text-center"><?= lang('department') ?></th>
                            <th class="text-center"><?= lang('nationality') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($employee_list as $emp): ?>
                            <?php if (!in_array($emp->employee_id, $emp_at_leave)): ?>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle">
                                        <input type="checkbox" name="ids[]" class="ids" value="<?= $emp->employee_id ?>"
                                               style="cursor: pointer"/>
                                    </td>
                                    <td>
                                        <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $emp->gender == "Female"): ?>
                                            <img src="<?=base_url()?>img/adminf.png"  style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; " alt="Please Connect Your Internet"/>
                                        <?php else:?>
                                            <img src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>" style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; " alt="Please Connect Your Internet">
                                        <?php endif;?>
                                    </td>
                                    <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->designations : $emp->designations_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->department_name : $emp->department_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->countryName : $emp->countryName_ar; ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <br><br>


                    <h3><?= ($lang == 'english') ? 'Employees at Vacation' : 'موظفون في إجازة'; ?> :</h3>

                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th width="5%" class="text-center"><input type="checkbox" class="all0 ids0" name="ids[]"
                                                                      value="0"
                                                                      style="cursor: pointer"/></th>
                            <th style="width:72px" class="text-center"><?= lang('image') ?></th>
                            <th class="text-center"><?= lang('name') ?></th>
                            <th class="text-center"><?= lang('job_title') ?></th>
                            <th class="text-center"><?= lang('designation') ?></th>
                            <th class="text-center"><?= lang('department') ?></th>
                            <th class="text-center"><?= lang('nationality') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($employee_list as $emp): ?>
                            <?php if (in_array($emp->employee_id, $emp_at_leave)): ?>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle">
                                        <input type="checkbox" name="ids[]" class="ids0"
                                               value="<?= $emp->employee_id ?>"
                                               style="cursor: pointer"/>
                                    </td>
                                    <td>
                                        <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $emp->gender == "Female"): ?>
                                            <img src="<?=base_url()?>img/adminf.png"  style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; " alt="Please Connect Your Internet"/>
                                        <?php else:?>
                                            <img src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>" style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; " alt="Please Connect Your Internet">
                                        <?php endif;?>
                                    </td>
                                    <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->designations : $emp->designations_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->department_name : $emp->department_name_ar; ?></td>
                                    <td><?= ($lang == 'english') ? $emp->countryName : $emp->countryName_ar; ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>


                </form>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function () {
        $('#sbtn2').on('click', function (e) {
            $('.ids').map(function () {
                $(this).prop('checked', true);
            });
            $('.all.ids').prop('checked', false);
            $('.all0.ids').prop('checked', false);
        });
        /////////////////////////////////
        $('.all').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.ids').map(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                $('.ids').map(function () {
                    $(this).prop('checked', false);
                });
            }
        });
        /////////////////////////////////
        $('.all0').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.ids0').map(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                $('.ids0').map(function () {
                    $(this).prop('checked', false);
                });
            }
        });

        /////////////////////////////////
        $('.report_type').change(function () {
            /*
             var val = $(this).val();
             if(val=='vacations1'){
             $('.months').attr('disabled', true);
             $('select[name="year"]').attr('disabled', true);
             }
             else{
             $('.months').attr('disabled', false);
             $('select[name="year"]').attr('disabled', false);
             }
             */
        });

        ////////////////////////////////
        $('#form').on('submit', function (e) {
            var flag = false;
            if($('.months').val()!='0' && $('.years').val()=='0'){
                alert('<?=lang("report_error_year")?>')
                return false;
            }
            $('.ids').map(function () {
                if ($(this).is(':checked'))
                    flag = true;
            });
            $('.ids0').map(function () {
                if ($(this).is(':checked'))
                    flag = true;
            });
            if (!flag) {
                alert('<?=lang("report_error")?>')
                return false;
            }
            else {
                $('#form').attr('action', '<?=base_url()?>employee/reports/' + $('.report_type').val());
            }
        });
    });


    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    var lang = "<?= $this->session->userdata('lang'); ?>";
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>employee/dashboard/get_sec_with_dep/" + id;
        if (id) {
            $('.spinner_sects').fadeIn('fast');
            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x3').empty();
                    $('#x3').append('<option value="-1"><?=lang("all")?></option>');
                    $('#x3').append('<option value="0"><?= lang("without_section") ?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '" >' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
            });
        } else {
            $('#x3').empty();
            $('#x3').append('<option value="0"><?=lang("all")?></option>');
        }
    }
</script>