<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang('results') ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php @$today = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                        <?php foreach (@$results as $result): ?>
                            <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                    class="fa fa-print"></i> <?= lang('print') ?></a>

                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == 'arabic'): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-primary.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 300px;
                                        /*height: 100px;*/
                                        padding: 10px 15px;
                                    <?php if($lang == 'english'):?> float: left;
                                    <?php else: ?> float: right;
                                    <?php endif; ?>
                                    }

                                    table {
                                        width: 100%;
                                    }

                                    table td, table th {
                                        padding: 2px;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <b><?= lang('name') ?>
                                                    : </b><?= ($lang == 'english') ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang('designation') ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == 'english') ? $result->designations : $result->designations_ar) : lang('no-exist'); ?>
                                                <br>
                                                <b><?= lang('department') ?>
                                                    : </b><?= ($lang == 'english') ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang('direct_manager') ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == 'english') ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                                -----------------------
                                                <br>
                                                <?php if ($lang == "english"): ?>
                                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                                <?php else: ?>
                                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                                <?php endif; ?>
                                                <b><?= lang("the_report") ?> : </b> <?= lang("evaluations") ?><br>
                                                <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                                <b><?= lang("year") ?>
                                                    : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 10px;"></div>
                                    <h2 style="margin:0px;" class="text-center"><?= lang('evaluations') ?></h2><br>

                                </div>
                                <div class="content">
                                    <table border="1">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('date') ?></th>
                                            <th class="text-center"><?= lang('results') ?></th>
                                            <th class="text-center"><?= lang('total') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($evaluations as $ev): ?>
                                            <?php if ($ev->emp_id == $result->employee_id): ?>
                                                <?php if (strpos($ev->created_date, $filter) !== false) : ?>
                                                    <tr>
                                                        <td class="text-center" style="vertical-align: middle">
                                                            <strong><b><?= $ev->created_date ?></b></strong></td>
                                                        <td>
                                                            <table>
                                                                <?php $ids = explode(';', $ev->ids); ?>
                                                                <?php $valuess = explode(';', $ev->valuess); ?>
                                                                <?php for ($i = 0; $i < count($ids); $i++): ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php foreach ($evaluation_items as $ev_item): ?>
                                                                                <?php if ($ev_item->evaluation_items_id == $ids[$i]): ?>
                                                                                    <?= ($lang == 'english') ? $ev_item->name_en : $ev_item->name_ar; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <?= $valuess[$i] ?> %
                                                                        </td>
                                                                    </tr>
                                                                <?php endfor; ?>
                                                            </table>
                                                        </td>
                                                        <td class="text-center" style="vertical-align: middle">
                                                            <strong><b><?= ($ev->total / count($ids)) ?> %</b></strong>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    function print_this(id) {
        var divToPrint=$('#' + id);
        console.log(divToPrint.html());

        var newWin=window.open('','PRINT', 'height=400,width=600');

        newWin.document.write('<html><body onload="window.print()"><style type="text/css">table { page-break-inside:auto }tr{ page-break-inside:avoid; page-break-after:auto }thead{ display:table-header-group }tfoot { display:table-footer-group }</style>'+divToPrint.html()+'</body></html>');

        newWin.document.close();

        return true;

    }
</script>
