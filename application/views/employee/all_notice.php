<div class="col-md-12">
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-12">

                <!-- Ideal Employee -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('notices_list') ?> </strong>
                    </div>
                    <div class="panel-body">

                        <!-- Table -->
                        <table class="table table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th class="text-center" width="18%"><?= lang('sender') ?></th>
                                <th class="text-center" width="18%"><?= lang('date') ?></th>
                                <th class="text-center" width="18%"><?= lang('notif_title') ?></th>
                                <th class="text-center" width="18%"><?= lang('notif_type') ?></th>
                                <th class="text-center" width="14%"><?= lang('status') ?></th>
                                <th class="text-center" width="14%"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody style="font-size: 1.1em">
                            <?php if (!empty($notice_info)): ?>
                                <?php foreach ($notice_info as $v_notice) : ?>
                                    <tr>
                                        <td>
                                            <?php foreach ($emps as $emp): ?>
                                                <?php if ($emp->employee_id == $v_notice->employee_id): ?>
                                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </td>
                                        <td class="relative"><?= $v_notice->created_date ?><?= hijjri($v_notice->created_date); ?></td>
                                        <td><?php echo $v_notice->title ?></td>
                                        <td>
                                            <?php
                                            $type = $v_notice->sugg_or_compl;
                                            if ($type == 1)
                                                echo lang('sugg');
                                            elseif ($type == 2)
                                                echo lang('compl');
                                            elseif ($type == 3)
                                                echo lang('anotice');
                                            elseif ($type == 4)
                                                echo lang('appro_consultation');
                                            elseif ($type == 5)
                                                echo lang('accounting');
                                            else
                                                echo lang('anotice');
                                            ?>
                                        </td>
                                        <td class="text-center" id="<?= $v_notice->notice_id ?>">
                                            <?php if ($v_notice->to_all != 0): ?>
                                                <?php if ($v_notice->view_status == 1): ?>
                                                    <a class="btn btn-primary" style="cursor:auto"><?= lang('all') ?>
                                                        <small>(<?= lang('readed') ?>)</small>
                                                    </a>
                                                <?php else: ?>
                                                    <a class="btn btn-primary" style="cursor:auto"><?= lang('all') ?>
                                                        <small>(<?= lang('unreaded') ?>)</small>
                                                    </a>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if ($v_notice->view_status == 1): ?>
                                                    <button class="btn btn-success"><?= lang('readed') ?></button>
                                                <?php else: ?>
                                                    <a class="btn btn-warning"
                                                       style="cursor:auto"><?= lang('unreaded') ?></a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if ($v_notice->to_all == 0): ?>
                                                <a href="<?= base_url() ?>employee/dashboard/notice_detail/<?= $v_notice->notice_id ?>"
                                                   data-toggle="modal" data-target="#myModal_lg" class="btn btn-primary btn-xs"
                                                   onclick="readed(<?= $v_notice->notice_id ?>,0)">
                                                    <span class="fa fa-eye"> <?= lang('view') ?></span>
                                                </a>
                                            <?php else: ?>
                                                <a href="<?= base_url() ?>employee/dashboard/notice_detail/<?= $v_notice->notice_id ?>"
                                                   data-toggle="modal" data-target="#myModal_lg" class="btn btn-primary btn-xs"
                                                   onclick="readed(<?= $v_notice->notice_id ?>,1)">
                                                    <span class="fa fa-eye"> <?= lang('view') ?></span>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                <a href="<?= base_url() ?>employee/dashboard/delete_notice/<?= $v_notice->notice_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <td colspan="6" class="text-center">
                                    <strong><?= lang('no-exist') ?></strong>
                                </td>
                            <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function readed(id, to_all) {
        $url = '<?= base_url() ?>' + 'employee/dashboard/notice_readed/' + id;
        $.get($url,
            function (data) {
                if (to_all == 0)
                    $('#' + id).html("<button class='btn btn-success'><?= lang('readed') ?></button>");
                else
                    $('#' + id).html("<button class='btn btn-primary'><?= lang('all') ?> <small>(<?= lang('readed') ?>)</small></button>");
            });
        console.log($url);
    }

</script>