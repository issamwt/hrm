<!-- Here begin Main Content -->

<style type="text/css">
    .primary-link {
        font-weight: bold;
    }

    .emp_type {
        padding: 10px 0px;
        margin: 10px auto;
        background: #337AB7 !important;
        color: #fff !important;
        display: block;
        width: 90%;
    }
</style>
<div class="col-md-12">
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-user"></i> <strong><?= lang('personal_details') ?> </strong>
                        </div>
                        <div class="panel-body"><br>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td><span class="primary-link"><?= lang('name') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->full_name_en : $employee_details->full_name_ar; ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('date_of_birth') ?></span></td>
                                    <td><?= $employee_details->date_of_birth ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('gender') ?></span></td>
                                    <td><?= ($employee_details->gender == 'Male') ? lang('male') : lang('female'); ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('maratial_status') ?></span></td>
                                    <td><?= lang($employee_details->maratial_status); ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('email') ?></span></td>
                                    <td><?= $employee_details->email ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('phone') ?></span></td>
                                    <td><?= $employee_details->phone ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('passport_no') ?></span></td>
                                    <td><?= $employee_details->passport_number ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('identity_no') ?></span></td>
                                    <td><?= $employee_details->identity_no ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('country') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->countryName : $employee_details->countryName_ar; ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('education') ?></span></td>
                                    <td><?= $employee_details->education ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-building"></i> <strong><?= lang('bank_information') ?> </strong>
                        </div>
                        <div class="panel-body"><br>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('bank_name') ?></span></td>
                                    <td><?= (!empty($employee_details->bank_name)) ? $employee_details->bank_name : lang('no-exist'); ?></td>
                                </tr>
                                <tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('branch_name') ?></span></td>
                                    <td><?= (!empty($employee_details->branch_name)) ? $employee_details->branch_name : lang('no-exist'); ?></td>
                                </tr>
                                <tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('account_name') ?></span></td>
                                    <td><?= (!empty($employee_details->account_name)) ? $employee_details->account_name : lang('no-exist'); ?></td>
                                </tr>
                                <tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('account_number') ?></span></td>
                                    <td><?= (!empty($employee_details->account_number)) ? $employee_details->account_number : lang('no-exist'); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--
                    <div class="well-custom">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4" style="border-right: 1px solid #DAE4E6">
                                <div class="uppercase text-center">
                                    <strong>0 / <?php echo $total_days; ?>
                                    </strong>
                                </div>
                                <div class="uppercase text-center">
                    <?= lang('attendance') ?>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4" style="border-right: 1px solid #DAE4E6">
                                <a href="<?php echo base_url() ?>employee/dashboard/leave_application" style="color: white;">
                                    <div class="uppercase text-center">
                                        <strong>
                    <?php echo $total_leave_applied; ?>
                                        </strong>
                                    </div>
                                    <div class="uppercase text-center">
                    <?= lang('leave') ?>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <a href="<?php echo base_url() ?>employee/dashboard/all_award" style="color: white;">
                                    <div class="uppercase text-center">
                                        <strong>
                    <?php echo $total_award_received; ?>
                                        </strong>
                                    </div>
                                    <div class="uppercase text-center">
                    <?= lang('award') ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    -->
                </div>
                <div class="col-md-5">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-university"></i> <strong><?= lang('department') ?> </strong>
                        </div>
                        <div class="panel-body"><br>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td><span class="primary-link"><?= lang('department') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->department_name : $employee_details->department_name_ar ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('designation') ?></span></td>
                                    <?php if ($employee_details->designations_id != 0): ?>
                                        <td><?= ($lang == 'english') ? $employee_details->designations : $employee_details->designations_ar ?></td>
                                    <?php else: ?>
                                        <td><?= lang('no-exist') ?></td>
                                    <?php endif; ?>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('direct_manager') ?></span></td>
                                    <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar'] ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('hr_manager') ?></span></td>
                                    <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar'] ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-desktop"></i> <strong><?= lang('job_details') ?> </strong>
                        </div>
                        <div class="panel-body"><br>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td><span class="primary-link"><?= lang('job_title') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->job_titles_name_en : $employee_details->job_titles_name_ar ?></td>
                                </tr>
                                <tr>
                                    <td width="37%"><span class="primary-link"><?= lang('job_title_desc') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->job_titles_desc_en : $employee_details->job_titles_desc_ar ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('reference') ?></span></td>
                                    <td><?= $employee_details->employment_id    ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('finance_basic_salary') ?></span></td>
                                    <td><?= $employee_details->employee_salary ?> <?= lang('rial') ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('joining_date') ?></span></td>
                                    <td><?= $employee_details->joining_date ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('job_place') ?></span></td>
                                    <td><?= ($lang == 'english') ? $employee_details->place_name_en : $employee_details->place_name_ar ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('job_time') ?></span></td>
                                    <td><?= ($employee_details->job_time == 'Full') ? lang('job_full') : lang('job_part'); ?></td>
                                </tr>
                                <tr>
                                    <td><span class="primary-link"><?= lang('holiday_no') ?></span></td>
                                    <td><?= $employee_details->holiday_no ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
                <div class="col-md-3">
                    <div class="panel panel-info" style="border: 1px solid #004884 ">
                        <div class="panel-body">
                            <?php if ($employee_details->photo): ?>
                                <img src="<?php echo base_url() . $employee_details->photo; ?>"
                                     style="height: 200px; width: 210px;" class="img-responsive center-block"/>
                            <?php else: ?>
                                <img src="<?php echo base_url() ?>asset/img/administrator.png"
                                     style="height: 200px; width: 210px; " class="img-responsive center-block"
                                     alt="Employee_Image"/>
                            <?php endif; ?>
                        </div>
                        <div style="border-top: 1px solid #004884 ">
                            <h3 class="text-center"><?= ($lang == 'english') ? $employee_details->full_name_en : $employee_details->full_name_ar; ?></h3>
                            <h4 class="text-center emp_type">
                                <?php $type = $this->session->userdata('emp_type'); ?>
                                <?php
                                switch ($type) {
                                    case 'employee':
                                        echo lang($type);
                                        break;
                                    case 'super_manager':
                                        echo lang($type);
                                        break;
                                    case 'hr_manager':
                                        echo lang($type);
                                        break;
                                    case 'accountant':
                                        echo lang($type);
                                        break;
                                    case 'dep_manager':
                                        echo lang($type) . '<br><br>';
                                        echo '(';
                                        echo ($lang == "english") ? $this->session->userdata("my_department_name_en") : $this->session->userdata("my_department_name_ar");
                                        echo ')';
                                        break;
                                    case 'sec_manager':
                                        echo lang($type) . '<br><br>';
                                        echo '(';
                                        echo ($lang == "english") ? $this->session->userdata("my_designation_name_en") : $this->session->userdata("my_designation_name_ar");
                                        echo ')';
                                        break;
                                    default:
                                        break;
                                }
                                ?>
                            </h4>
                            <h5 class="text-center"><?= lang('employee_id') ?>
                                : <?php echo $employee_details->employment_id ?></h5>
                            <h5 class="text-center">
                                <?= ($lang == 'english') ? $employee_details->department_name : $employee_details->department_name_ar; ?>
                                <?php if ($employee_details->designations != '0'): ?>
                                    -
                                    <?= ($lang == 'english') ? $employee_details->designations : $employee_details->designations_ar ?>
                                <?php endif; ?>
                            </h5>
                            <p></p>
                        </div>
                    </div>

                    <form method="post" action="<?php echo base_url() ?>employee/dashboard/set_attendance/">
                        <div>
                            <button name="action" type="submit" value="1"
                                    class="btn btn-success btn-block clock_in_button">
                                <i class="fa fa-arrow-right"></i> <?= lang('clock_in') ?>
                            </button>
                            <button name="action" type="submit" value="2"
                                    class="btn btn-danger btn-block clock_in_button">
                                <i class="fa fa-arrow-left"></i> <?= lang('clock_out') ?>
                            </button>
                        </div>
                    </form>

                    <? if(!empty($extra_hours)): ?>
                        <br>
                        <form method="post" action="<?php echo base_url() ?>employee/dashboard/set_extra_hours/">
                            <div>
                                <button name="action" type="submit" value="1"
                                        class="btn btn-primary btn-block clock_in_button">
                                    <i class="fa fa-arrow-right"></i> <?= lang('clock_in') ?> <br><small><?= lang('extra_hours') ?></small>
                                </button>
                                <button name="action" type="submit" value="2"
                                        class="btn btn-primary btn-block clock_in_button">
                                    <i class="fa fa-arrow-left"></i> <?= lang('clock_out') ?> <br><small><?= lang('extra_hours') ?></small>
                                </button>
                            </div>
                        </form>
                    <?endif;?>

                    <div class="panel panel-primary" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-link"></i> <strong><?= lang('usefull_links') ?> </strong>
                        </div>
                        <div class="panel-body panel_links">
                            <ul>
                                <li>
                                    <a href="<?php echo base_url() ?>employee/dashboard/profile"
                                       class="view-all-front"><?= lang('view_profile') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>employee/dashboard/administrative_affairs"
                                       class="view-all-front"><?= lang('administrative_affairs') ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>employee/dashboard/contact_admin"
                                       class="view-all-front"><?= lang('contact_admin') ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>

        </div>
    </div> <!-- /.col-md-12 -->
</div> <!-- /.col-md-12 -->
