<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .add-button {
        margin-bottom: 10px;
        padding: 10px 20px;
        float: left;
    }

    .btn-default {
        background: #9b59b6 !important;
        color: #fff !important;
        border-color: #8e3faf !important;
    }

    .btn-default:hover {
        background: #8e44ad !important;
        color: #fff !important;
        border-color: #8e3faf !important;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">

            <a class="btn btn-primary add-button" style="margin:0px 10px 10px 0px" href="<?=base_url();?>employee/dashboard/employee_list"><i class="fa fa-reply"></i> <?=($lang=="arabic")?"الموظفين الفعالين":"Active Employees";?></a>
            <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                <a class="btn btn-primary add-button" href="<?= base_url() ?>employee/dashboard/add_employee"><i
                            class="fa fa-plus"></i> <?= lang('add_employee') ?></a>
            <?php endif; ?>
            <div class="clearfix"></div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?=($lang=="arabic")?"الموظفين غير الفعالين":"Inactive Employees";?> </strong>
                </div>
                <div class="panel-body"><br>

                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('sl') ?></th>
                            <th class="text-center"><?= lang('employee_id') ?></th>
                            <th style="width:72px" class="text-center"><?= lang('image') ?></th>
                            <th class="text-center"><?= lang('name') ?></th>
                            <th class="text-center"><?= lang('job_title') ?></th>
                            <th class="text-center"><?= lang('dept_desingation') ?></th>
                            <th class="text-center"><?= lang('job_time') ?></th>
                            <th class="text-center" width="7%"><?= lang('status') ?></th>
                            <th class="text-center" width="15%"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $ls = 1; ?>
                        <?php if (!empty(@$employee_list)): ?>
                            <?php foreach (@$employee_list as $emp): ?>
                                <?php if($emp->status!=1): ?>
                                    <tr>
                                        <td class="text-center"><?= $ls++ ?></td>
                                        <td><?= $emp->employment_id ?></td>
                                        <td>
                                            <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $emp->gender == "Female"): ?>
                                                <img src="<?= base_url() ?>img/adminf.png"
                                                     style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                     alt="Please Connect Your Internet"/>
                                            <?php else: ?>
                                                <img src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>"
                                                     style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                     alt="Please Connect Your Internet">
                                            <?php endif; ?>
                                        </td>
                                        <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                        <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                        <td>
                                            <?php if ($lang == 'english'): ?>
                                                <?= $emp->department_name . ' > '; ?>
                                                <?php if ($emp->designations_id != '0'): ?>
                                                    <?= $emp->designations ?>
                                                <?php else: ?>
                                                    <?= lang('no-exist') ?>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?= $emp->department_name_ar . ' > '; ?>
                                                <?php if ($emp->designations_id != '0'): ?>
                                                    <?= $emp->designations_ar ?>
                                                <?php else: ?>
                                                    <?= lang('no-exist') ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if ($emp->job_time == 'Part'): ?>
                                                <?= lang('job_part') ?>
                                            <?php else: ?>
                                                <?= lang('job_full') ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if ($emp->status == 1):
                                                echo '<span class="btn btn-success btn-xs btn-block disabled">' . lang("active") . '</span>';
                                            elseif ($emp->status == 2):
                                                echo '<span class="btn btn-danger btn-xs btn-block disabled">' . lang("inactive") . '</span>';
                                            else:
                                                echo '<span class="btn btn-primary btn-xs btn-block disabled">' . lang("in_test") . '</span>';
                                            endif; ?>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-x btn-xs btn-block" type="button"
                                               data-toggle="collapse"
                                               data-target="#collapseExample<?= $emp->employee_id; ?>"
                                               aria-expanded="false" aria-controls="collapseExample">
                                                <i class="fa fa-arrow-circle-down"></i>
                                            </a>
                                            <div class="collapse emp_actions" id="collapseExample<?= $emp->employee_id; ?>">
                                                <!-- view -->
                                                <a href="<?= base_url() ?>employee/dashboard/profile/<?= @$emp->employee_id ?>"
                                                   class="btn btn-success btn-xs btn-block"><i
                                                        class="fa fa-eye"></i> <?= lang('view') ?></a>
                                                <!-- view -->

                                                <!-- finance_details -->
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <a href="<?= base_url() ?>employee/employee/finance_employee/<?= @$emp->employee_id ?>"
                                                       class="btn btn-warning btn-xs btn-block"><i
                                                            class="fa fa-dollar"></i> <?= lang('view_finance_details') ?>
                                                    </a>
                                                <?php endif; ?>
                                                <!-- finance_details -->

                                                <!-- personal_details -->
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <a href="<?= base_url() ?>employee/dashboard/add_employee/<?= @$emp->employee_id ?>"
                                                       class="btn btn-default-default btn-xs btn-block"><i
                                                            class="fa fa-edit"></i> <?= lang('personal_details') ?></a>
                                                <?php endif; ?>
                                                <!-- personal_details -->

                                                <!-- mouvements -->
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <a href="<?= base_url() ?>employee/employee/mouvements/<?= @$emp->employee_id ?>"
                                                       class="btn btn-info btn-xs btn-block"><i
                                                            class="fa fa-bolt"></i> <?= lang('mouvements') ?></a>
                                                <?php endif; ?>
                                                <!-- mouvements -->

                                                <!-- accounting -->
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager' or $this->session->userdata('emp_type') == 'sec_manager' or $this->session->userdata('emp_type') == 'dep_manager'): ?>
                                                    <a href="<?= base_url() ?>employee/accounting/send_accounting/<?= @$emp->employee_id ?>"
                                                       class="btn btn-default btn-xs btn-block"><i
                                                            class="fa fa-question-circle"></i> <?= lang('accounting') ?>
                                                    </a>
                                                <?php endif; ?>
                                                <!-- accounting -->

                                                <!-- delete -->
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <?php if (@$emp->job_title != 4 and @$emp->job_title != 1): ?>
                                                        <a href="<?= base_url() ?>employee/employee/delete_employee/<?= @$emp->employee_id ?>"
                                                           class="btn btn-danger btn-xs btn-block"
                                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                                    <?php else: ?>
                                                        <a href="#" class="btn btn-danger btn-xs disabled btn-block"><i
                                                                class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <!-- delete -->
                                            </div>
                                        </td>
                                    </tr>
                                <?php endif;?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
            <br>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

