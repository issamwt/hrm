<?php echo message_box('success'); ?>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <!-- employee_reply -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('employee_reply') ?> </strong>
                </div>
                <div class="panel-body"><br>

                    <?php if (!empty($accounting_detail)): ?>
                        <form enctype="multipart/form-data"
                              action="<?= base_url() ?>employee/accounting/employee_reply/<?= $accounting_detail->accounting_id ?>"
                              method="post" class="form form-horizontal" id="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('accounting_title') ?> : </label>
                                <div class="col-md-7">
                                    <div style="padding-top: 6px;"><?= $accounting_detail->accounting_title ?></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('accounting_text') ?> : </label>
                                <div class="col-md-7">
                                    <div style="padding-top: 6px;"><?= $accounting_detail->accounting_text ?></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('employee_reply') ?> <span
                                            class="required"> *</span></label>
                                <div class="col-md-7">
                                    <textarea name="employee_reply" class="form-control" rows="6"
                                              required="" <?=($accounting_detail->final_decision=='')?'':'disabled';?>></textarea>
                                </div>
                            </div>

                            <div class="form-group margin">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" id="sbtn" <?=($accounting_detail->final_decision=='')?'':'disabled';?>
                                            class="btn btn-primary"><?= lang('send') ?></button>
                                </div>
                            </div>
                        </form>
                    <?php else: ?>
                        <br>
                        <div class="alert alert-danger">
                            <?= lang('acounting_deleted') ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>