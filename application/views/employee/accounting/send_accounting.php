<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    .boxino {
        border: 1px solid gray;
        background: #337AB7;
        color: #fff;
        padding: 15px 15px 15px 50px;
        display: inline-block;
        border-radius: 8px;
    }

    .btn-default {
        background: #9b59b6 !important;
        color: #fff !important;
        border-color: #8e3faf !important;
    }

    .btn-default:hover {
        background: #8e44ad !important;
        color: #fff !important;
        border-color: #8e3faf !important;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <!-- send_accounting -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('send_accounting') ?> </strong>
                </div>
                <div class="panel-body">
                    <div class="boxino">
                        <table>
                            <tr>
                                <td width="170"><b><?= lang('name') ?> : </b></td>
                                <td><?= ($lang == 'english') ? $employee_info->full_name_en : $employee_info->full_name_ar; ?></td>
                            </tr>
                            <tr>
                                <td width="170"><b><?= lang('employee_id') ?> : </b></td>
                                <td>
                                    <small><?= $employee_info->employment_id ?></small>
                                </td>
                            </tr>
                            <tr>
                                <td><b><?= lang('job_time') ?> :</b></td>
                                <td><?= lang($employee_info->job_time) ?></td>
                            </tr>
                            <tr>
                                <td>-------------------------</td>
                                <td>---------------</td>
                            </tr>
                            <tr>
                                <td width="170"><b><?= lang('department') ?> : </b></td>
                                <td><?= ($lang == 'english') ? $employee_info->department_name : $employee_info->department_name_ar; ?></td>
                            </tr>
                            <tr>
                                <td><b><?= lang('direct_manager') ?> :</b></td>
                                <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar']; ?></td>
                            </tr>
                            <tr>
                                <td><b><?= lang('hr_manager') ?> :</b></td>
                                <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar']; ?></td>
                            </tr>
                        </table>
                    </div>
                    <br><br><br>
                    <form enctype="multipart/form-data"
                          action="<?= base_url() ?>employee/accounting/save_accounting/<?= $employee_info->employee_id ?>"
                          method="post" class="form form-horizontal" id="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('accounting_title') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <input name="accounting_title" type="text" class="form-control" required=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('accounting_text') ?> <span
                                        class="required"> *</span></label>
                            <div class="col-md-7">
                                <textarea name="accounting_text" class="form-control" rows="6" required=""></textarea>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- send_accounting -->

            <!-- accounting_archive -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('accounting_archive') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('date') ?></th>
                            <th class="text-center"><?= lang('employee_name') ?></th>
                            <th class="text-center"><?= lang('accounting_title') ?></th>
                            <th class="text-center" width="18%"><?= lang('accounting_text') ?></th>
                            <th class="text-center" width="18%"><?= lang('employee_reply') ?></th>
                            <th class="text-center" width="18%"><?= lang('final_decision') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($accounting_list)): ?>
                            <?php foreach ($accounting_list as $acc): ?>
                                <tr>
                                    <td class="relative"><?= $acc->created_date ?><?= hijjri($acc->created_date); ?></td>
                                    <td>
                                        <?php foreach ($all as $emp): ?>
                                            <?php if ($emp->employee_id == $acc->to_employee): ?>
                                                <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= $acc->accounting_title ?></td>
                                    <td><?= $acc->accounting_text ?></td>
                                    <td><?= $acc->employee_reply ?></td>
                                    <td>
                                        <?= $acc->final_decision ?>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal<?= @$acc->accounting_id ?>">
                                            <i class="fa fa-question-circle"></i> <?= lang('final_decision') ?>
                                        </button>
                                        <a href="<?= base_url() ?>employee/accounting/delete_accounting/<?= $employee_info->employee_id ?>/<?= @$acc->accounting_id ?>"
                                           class="btn btn-danger btn-xs"
                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                    class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7" class="text-center"><?= lang('no-exist') ?></td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- accounting_archive -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.panel').closest('.container').attr('class', 'container-fluid');
    });
</script>


<?php foreach ($accounting_list as $acc): ?>
<!-- Modal -->
<div id="myModal<?= @$acc->accounting_id ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>employee/accounting/final_decision/<?= $employee_info->employee_id ?>/<?= @$acc->accounting_id ?>"
                      method="post" class="form form-horizontal">
                    <p><b><?= @$acc->accounting_title ?> </b><br> <?=lang('final_decision')?></p>
                    <textarea style="width: 100%" name="final_decision" class="form-control" rows="6" required></textarea>
                    <button type="submit" class="btn btn-primary btn-block"><?=lang('send')?></button>
                </form>
            </div>
        </div>

    </div>
</div>
<?php endforeach; ?>