<div class="col-md-12">

    <div class="box box-primary">
        <!-- Default panel contents -->

        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                    class="sr-only"><?= lang('close') ?></span></button>
            <div class="panel-title">
                <strong><?= lang('leave') ?></strong>
            </div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('leave_type') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?php foreach (@$leaves_cat as $lc): ?>
                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('leave_category_duration2') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= $leave->duration ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('going_date') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= $leave->going_date ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('coming_date') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= $leave->coming_date ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('paid_leave') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?php foreach (@$leaves_cat as $lc): ?>
                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                <?= ($lc->paid_leave == 1) ? lang('yes') : lang('no') ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('leave_category_quota2') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= $leave->quota ?> %
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('leave_affect_stock2') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= ($leave->affect_stock == 1) ? lang('yes') : lang('no'); ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('replacement') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?php if (empty($leave->replacement)): ?>
                            <?= lang('no-exist') ?>
                        <?php else: ?>
                            <?php foreach (@$employee_list as $emp): ?>
                                <?php if ($emp->employee_id == $leave->replacement): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('the_leave_tel') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= ($leave->leave_tel) ? $leave->leave_tel : lang('no-exist'); ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('address_in_leave') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?= $leave->address_in_leave; ?>
                    </p>
                </div>
            </div>

            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-4 text-right">
                    <label class="control-label"><strong><?= lang('allowance_cat_ids') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <ul class="form-control-static">
                        <?php foreach (@$leaves_cat as $lc): ?>
                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                <?php $allowance_cat_ids = explode(';', @$lc->allowance_cat_ids); ?>
                                <?php foreach (@$all_allowance_categories as $ac): ?>
                                    <?php if (in_array($ac->allowance_id, $allowance_cat_ids)): ?>
                                        <li><?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?></li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>






