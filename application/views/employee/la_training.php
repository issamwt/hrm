<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php foreach (@$results as $result): ?>
                            <button class="btn btn-primary btn-xs"
                                    onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                    class="fa fa-print"></i> <?= lang("print") ?></button>
                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == "arabic"): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-primary.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 300px;
                                        padding: 10px 15px;
                                    <?php if($lang == "english"):?> float: left;
                                    <?php else: ?> float: right;
                                    <?php endif; ?>
                                    }

                                    table {
                                        width: 100%;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <b><?= lang("name") ?>
                                                    : </b><?= ($lang == "english") ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang("designation") ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == "english") ? $result->designations : $result->designations_ar) : lang("no-exist"); ?>
                                                <br>
                                                <b><?= lang("department") ?>
                                                    : </b><?= ($lang == "english") ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang("direct_manager") ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == "english") ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                                -----------------------
                                                <br>
                                                <?php if ($lang == "english"): ?>
                                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                                <?php else: ?>
                                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                                <?php endif; ?>
                                                <b><?= lang("the_report") ?> : </b> <?= lang("la_trainings") ?><br>
                                                <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                                <b><?= lang("year") ?>
                                                    : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 60px;"></div>
                                    <h2 class="text-center"><?= lang("la_trainings") ?></h2><br>
                                </div>
                                <div class="content">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang("course_name") ?></th>
                                            <th class="text-center"><?= lang("course_institute_name") ?></th>
                                            <th class="text-center"><?= lang("start_date") ?></th>
                                            <th class="text-center"><?= lang("end_date") ?></th>
                                            <th class="text-center"><?= lang("course_price") ?></th>
                                            <th class="text-center"><?= lang("notes") ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? $i = 0; ?>
                                        <?php foreach ($training_list as $tr): ?>
                                            <?php if ($tr->employeetr_id == $result->employee_id): ?>
                                                <?php if (strpos($tr->start_date, $filter) !== false) : $i++; ?>
                                                    <tr>
                                                        <td><?= $tr->course_name ?></td>
                                                        <td><?= $tr->course_institute_name ?></td>
                                                        <td><?= $tr->start_date ?></td>
                                                        <td><?= $tr->end_date ?></td>
                                                        <td><?= $tr->course_price ?></td>
                                                        <td><?= $tr->course_note ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <? if ($i == 0): ?>
                                            <tr>
                                                <td class="text-center" colspan="6"><?= lang("no-exist") ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin: 30px auto; color: red"><?= lang("no-exist") ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


