<?php echo message_box('success'); ?>

<style type="text/css">
    .hi {
        padding: 0px 10px;
    }

    .hi .col-sm-7, .hi .col-sm-5 {
        padding: 10px;
    }

    .view-all-front.btn.btn-primary {
        border: none;
    }
</style>
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <!-- main content -->
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-12 panel-title">
                            <strong><?= lang('your_personal_profile') ?></strong><span class="pull-right"><a
                                        onclick="history.go(-1);"
                                        class="view-all-front btn btn-primary"><?= lang('go_back') ?></a></span>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 well-user-profile">
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <div class="fileinput-new thumbnail"
                                     style="width: 144px; height: 158px; margin-top: 14px; margin-left: 16px; background-color: #EBEBEB;">
                                    <a href="" data-toggle="modal" data-target="#myModal">
                                        <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                            <?php if ($employee_details->photo): ?>
                                                <img src="<?php echo base_url() . $employee_details->photo; ?>"
                                                     style="width: 142px; height: 148px; border-radius: 3px;">
                                            <?php else: ?>
                                                <img src="<?php echo base_url() ?>/img/admin.png" alt="Employee_Image">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?php if ($employee_details->gender == "Male"): ?>
                                                <?php if ($employee_details->photo): ?>
                                                    <img src="<?php echo base_url() . $employee_details->photo; ?>"
                                                         style="width: 142px; height: 148px; border-radius: 3px;">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url() ?>/img/admin.png"
                                                         alt="Employee_Image">
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <?php if ($employee_details->photo): ?>
                                                    <?php if ($employee_details->employee_id == $this->session->userdata('employee_id')): ?>
                                                        <img src="<?php echo base_url() . $employee_details->photo; ?>"
                                                             style="width: 142px; height: 148px; border-radius: 3px;">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url() ?>/img/adminf.png"
                                                             alt="Employee_Image">
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <img src="<?php echo base_url() ?>/img/admin.png"
                                                         alt="Employee_Image">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </a>
                                    <!-- Modal content-->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body"
                                                     style="color: #000000 !important; text-align: center">
                                                    <form action="<?= base_url() ?>employee/employee/changeimage/<?= @$employee_details->employee_id ?>"
                                                          method="post"
                                                          class="form form-horizontal form-groups-bordered"
                                                          enctype="multipart/form-data"
                                                          id="form">

                                                        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.8/css/fileinput.min.css"
                                                              media="all" rel="stylesheet" type="text/css"/>
                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.8/js/fileinput.min.js"></script>
                                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.8/js/locales/ar.min.js"></script>

                                                        <input name="photo" id="input-id" type="file" class="file"
                                                               multiple="true"
                                                               data-show-upload="false" data-show-caption="true">
                                                        <script type="text/javascript">
                                                            $("#input-id").fileinput({
                                                                'language': '<?=($lang == 'arabic') ? "ar" : "en";?>',
                                                                'showUpload': false,
                                                                'previewFileType': 'any'
                                                            });
                                                        </script>

                                                        <div class="form-group">
                                                            <div class="col-sm-offset-3 col-sm-6">
                                                                <button type="submit" id="sbtn"
                                                                        class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if($this->session->userdata('employee_id')==$employee_details->employee_id): ?>
                                        <!-- Modal content-->
                                        <a data-toggle="modal" data-target="#myModal2"
                                           style="    margin:15px 0px;color: #fff;display: block;text-align: center;"><b>
                                            <?= ($lang == 'arabic') ? 'تغيير كلمة السر' : 'change the password'; ?>
                                            </b>
                                        </a>
                                        <!-- Modal content-->
                                        <div id="myModal2" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-body"
                                                         style="color: #000000 !important; text-align: center">
                                                        <form action="<?= base_url() ?>employee/employee/changepassword/<?= @$employee_details->employee_id ?>"
                                                              method="post"
                                                              class="form form-horizontal form-groups-bordered"
                                                              enctype="multipart/form-data"
                                                              id="form">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label"><?= lang('password') ?>
                                                                    <span
                                                                            class="required"> *</span></label>
                                                                <div class="col-sm-6">
                                                                    <input name="password"
                                                                           value="<?= @$employee_details->password ?>"
                                                                           type="text"
                                                                           id="password" class="form-control" required="">
                                                                </div>
                                                                <div class="col-sm-3"><a class="btn btn-primary"
                                                                                         onclick="generate2()"><?= lang('generate_auto') ?></a>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-offset-3 col-sm-6">
                                                                    <button type="submit" id="sbtn"
                                                                            class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                                                </div>
                                                            </div>
                                                            <script type="text/javascript">
                                                                function generate2() {
                                                                    var val = makeid();
                                                                    $('#password').val(val);
                                                                }
                                                                function makeid() {
                                                                    var text = "";
                                                                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                                                                    for (var i = 0; i < 8; i++)
                                                                        text += possible.charAt(Math.floor(Math.random() * possible.length));

                                                                    return text;
                                                                }
                                                            </script>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Modal content-->
                                    <?php endif;?>

                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-1">
                            </div>
                            <div class="col-lg-8 col-sm-7 ">
                                <div style="margin-left: 20px;">
                                    <h3><?= ($lang == 'english') ? $employee_details->full_name_en : $employee_details->full_name_ar; ?></h3>
                                    <hr/>
                                    <table class="table table-non-bordered">
                                        <tr>
                                            <td width="20%"><strong><?= lang('employee_id') ?></strong></td>
                                            <td><?php echo $employee_details->employment_id ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong><?= lang('department') ?></strong></td>
                                            <td><?= ($lang == 'english') ? $employee_details->department_name : $employee_details->department_name_ar; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong><?= lang('designation') ?></strong></td>
                                            <?php if ($employee_details->designations_id != 0): ?>
                                                <td><?= ($lang == 'english') ? $employee_details->designations : $employee_details->designations_ar; ?></td>
                                            <?php else: ?>
                                                <td><?= lang('no-exist') ?></td>
                                            <?php endif; ?>
                                        </tr>
                                        <tr>
                                            <td><strong><?= lang('joining_date') ?></strong></td>
                                            <td class="relative"><?= $employee_details->joining_date ?><?= hijjri($employee_details->joining_date); ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <!-- ************************ Personal Information Panel Start ************************-->
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><?= lang('personal_details') ?></h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('full_name_ar') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->full_name_ar ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('full_name_en') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->full_name_en ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('date_of_birth') ?> : </b></div>
                                            <div class="col-sm-7 relative"><?= $employee_details->date_of_birth ?><?= hijjri($employee_details->date_of_birth) ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('gender') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->gender == "Male") ? lang('male') : lang('female'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('phone') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->phone ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('email') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->email ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('nationality') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($lang == 'english') ? $employee_details->countryName : $employee_details->countryName_ar; ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('present_address') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->present_address ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('education') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->education ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('passport_no') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->passport_number ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('passport_end') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->passport_end ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('identity_no') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->identity_no ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('identity_end') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->identity_end ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('passport_end') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->passport_end ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('maratial_status') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?= lang(strtolower($employee_details->maratial_status)) ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('medical_insur') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?php if ($employee_details->med_insur == 1) echo lang('yes'); else echo lang('no'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('medical_insur_type') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?php if ($employee_details->med_insur_type == 1) echo lang('medical_insur_type_all'); elseif ($employee_details->med_insur_type == 2) echo lang('medical_insur_type_part');
                                                else echo lang('no-exist') ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('social_insur') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?php if ($employee_details->social_insurance == 1) echo lang('yes'); else echo lang('no'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('social_insurance_type') ?>
                                                    : </b></div>
                                            <div
                                                    class="col-sm-7"><?= (!empty($employee_details->social_insurance_type) and $employee_details->social_insurance_type != 0) ? lang($employee_details->social_insurance_type) : lang('no-exist'); ?></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> <!-- ************************ Personal Information Panel End ************************-->
                        <div class="col-sm-6">
                            <!-- ************************ Contact Details Start******************************* -->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('contact_details') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('salary') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= $employee_details->employee_salary; ?> <?= lang('rial') ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('department') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($lang == 'english') ? $employee_details->department_name : $employee_details->department_name_ar; ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('designation') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->designations_id != 0) ? (($lang == 'english') ? $employee_details->designations : $employee_details->designations_ar) : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('status') ?> : </b></div>
                                            <div class="col-sm-7">
                                                <?php if ($employee_details->status == 1):
                                                    echo lang("active");
                                                elseif ($employee_details->status == 2):
                                                    echo lang("inactive");
                                                else:
                                                    echo lang("in_test");
                                                endif; ?>
                                            </div>
                                        </div>
                                        <?php if ($employee_details->status == 3): ?>
                                            <div class="col-sm-12">
                                                <div class="col-sm-5"><b><?= lang('test_period') ?> (<?= lang('days') ?>
                                                        )
                                                        : </b></div>
                                                <div class="col-sm-7"><?= $employee_details->test_period ?></div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('employee_type') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($lang == 'english') ? $employee_details->name_en : $employee_details->name_ar; ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('joining_date') ?> : </b></div>
                                            <div class="col-sm-7 relative"><?= $employee_details->joining_date ?><?= hijjri($employee_details->joining_date) ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('retirement_date') ?> : </b></div>
                                            <div class="col-sm-7 relative">
                                                <?= (!empty(@$employee_details->retirement_date)) ? @$employee_details->retirement_date : lang('no-exist'); ?>
                                                <?= hijjri(@$employee_details->retirement_date);?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('job_time') ?>: </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->job_time == "Full") ? lang('job_full') : lang('job_part'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('job_title') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($lang == 'english') ? $employee_details->job_titles_name_en : $employee_details->job_titles_name_ar; ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('job_place') ?>: </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($lang == 'english') ? $employee_details->place_name_en : $employee_details->place_name_ar; ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('holiday_no') ?>: </b></div>
                                            <div
                                                    class="col-sm-7"><?= $employee_details->holiday_no ?> <?= lang('days') ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('employee_id') ?> : </b></div>
                                            <div class="col-sm-7"><?= $employee_details->employment_id ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- ************************ Contact Details End ******************************* -->

                        <div class="col-sm-6 hidden-print">
                            <!-- ************************ Employee Documents Start ******************************* -->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('employee_document') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $employee_details->gender == "Female"): ?>
                                        <?php else: ?>
                                            <div class="col-sm-12">
                                                <div class="col-sm-5"><b><?= lang('cin_photo') ?> : </b></div>
                                                <div
                                                        class="col-sm-7"><?= ($employee_details->cin_photo_path) ? '<a target="_blank" href="' . base_url() . $employee_details->cin_photo_path . '">' . lang("download") . '</a>' : lang('no-exist'); ?></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-5"><b><?= lang('passport_photo') ?> : </b></div>
                                                <div
                                                        class="col-sm-7"><?= ($employee_details->passport_photo_path) ? '<a target="_blank" href="' . base_url() . $employee_details->passport_photo_path . '">' . lang("download") . '</a>' : lang('no-exist'); ?></div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('resume') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->resume_path) ? '<a target="_blank" href="' . base_url() . 'img/uploads/' . $employee_details->resume_path . '">' . lang("download") . '</a>' : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('contract_paper') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->contract_paper_path) ? '<a target="_blank" href="' . base_url() . 'img/uploads/' . $employee_details->contract_paper_path . '">' . lang("download") . '</a>' : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('diplome') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->id_proff) ? '<a target="_blank" href="' . base_url() . 'img/uploads/' . $employee_details->id_proff . '">' . lang("download") . '</a>' : lang('no-exist'); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ************************ Employee Documents Start ******************************* -->

                        <!-- ************************ other_documents ******************************* -->
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('other_documents') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $employee_details->gender == "Female"): ?>
                                        <?php else: ?>
                                            <?php if (!empty($other_documents)): ?>
                                                <?php foreach ($other_documents as $od): ?>
                                                    <div class="col-sm-12">
                                                        <a href="<?= base_url() ?>employee/employee/delete_other_doc/<?= $od->odp_id ?>">
                                                            <i class="fa fa-times" style="color:red"></i>
                                                        </a>
                                                        <a target="_blank"
                                                           href="<?= base_url() ?>img/uploads/<?= $od->filename ?>">
                                                            <?= $od->filename ?>
                                                        </a>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <?= lang('no-exist') ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ************************ other_documents ******************************* -->

                        <!-- ************************      Bank Details Start******************************* -->
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('bank_information') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('bank_name') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->bank_name) ? $employee_details->bank_name : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('branch_name') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->branch_name) ? $employee_details->branch_name : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('account_name') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->account_name) ? $employee_details->account_name : lang('no-exist'); ?></div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-5"><b><?= lang('account_number') ?> : </b></div>
                                            <div
                                                    class="col-sm-7"><?= ($employee_details->account_number) ? $employee_details->account_number : lang('no-exist'); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ************************ Bank Details End ******************************* -->

                        <!-- ************************  missions ******************************* -->
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('missions_list') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row hi">
                                        <?php if (!empty($evaluation_items)): ?>
                                            <?php foreach ($evaluation_items as $ev): ?>
                                                <div class="col-sm-12">
                                                    <b><?= ($lang == 'english') ? $ev->name_en : $ev->name_ar; ?></b>
                                                    <?php if ($lang == 'english'): ?>
                                                        <?= ($ev->ei_desc_en) ? ' <b>:</b> ' . $ev->ei_desc_en : ''; ?>
                                                    <?php else: ?>
                                                        <?= ($ev->ei_desc_ar) ? ' <b>:</b> ' . $ev->ei_desc_ar : ''; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <div class="col-sm-12"><?= lang('no-exist') ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ************************ missions ******************************* -->

                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('allowances') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('finance_bonus') ?></th>
                                            <th class="text-center"><?= lang('finance_value') ?></th>
                                            <th class="text-center"><?= lang('finance_from') ?></th>
                                            <th class="text-center"><?= lang('finance_to') ?></th>
                                            <th class="text-center"><?= lang('finance_reservation') ?> %</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$allowances_list)): $total = 0; ?>
                                            <?php foreach (@$allowances_list as $al): ?>
                                                <?php if ($al->employe_id == $employee_details->employee_id): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($al->allowance_id == 0): ?>
                                                                <?= lang('other_dues') ?>
                                                            <?php else: ?>
                                                                <?php foreach (@$allowances_cat_list as $acl): ?>
                                                                    <?php if ($acl->allowance_id == $al->allowance_id): ?>
                                                                        <?= ($lang == 'english') ? $acl->allowance_title_en : $acl->allowance_title_ar; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($al->allowance_type == 'value'): ?>
                                                                <?= $al->allowance_value ?>
                                                            <?php else: ?>
                                                                <?= $al->allowance_value ?> (%)
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="relative"><?= $al->allowance_date_from ?><?= hijjri($al->allowance_date_from) ?></td>
                                                        <td class="relative"><?= $al->allowance_date_to ?><?= hijjri($al->allowance_date_to) ?></td>
                                                        <td><?= $al->reservation ?> %</td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td class="text-center" colspan="7">
                                                    <strong><?= lang('nothing_to_display') ?></strong></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('finance_provision') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('finance_provision_bonus') ?></th>
                                            <th class="text-center"><?= lang('finance_provision_value') ?></th>
                                            <th class="text-center"><?= lang('start_date') ?></th>
                                            <th class="text-center"><?= lang('end_date') ?></th>
                                            <th width="40%" class="text-center"><?= lang('prod_note') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$provision_list)): ?>
                                            <?php foreach (@$provision_list as $p): ?>
                                                <?php if ($p->employp_id == $employee_details->employee_id): ?>
                                                    <tr>
                                                        <td>
                                                            <?php foreach (@$provision_cat_list as $pc): ?>
                                                                <?php if ($pc->provision_category_id == $p->provision_category_id): ?>
                                                                    <?= ($lang == 'english') ? $pc->provision_title_en : $pc->provision_title_ar; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td><?= $p->provision_value ?> <?= lang('rial') ?></td>
                                                        <td class="relative"><?= $p->start_date ?><?= hijjri($p->start_date) ?></td>
                                                        <td class="relative"><?= $p->end_date ?><?= hijjri($p->end_date) ?></td>
                                                        <td><?= $p->note_provision ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td class="text-center" colspan="4">
                                                    <strong><?= lang('nothing_to_display') ?></strong></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('finance_permanent_deduction') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('finance_deduction_bonus') ?></th>
                                            <th class="text-center"><?= lang('finance_deduction_type') ?></th>
                                            <th class="text-center"><?= lang('finance_deduction_value') ?></th>
                                            <th class="text-center"><?= lang('start_date') ?></th>
                                            <th class="text-center"><?= lang('end_date') ?></th>
                                            <th class="text-center"><?= lang('prod_note') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$deduction_list)): ?>
                                            <?php foreach (@$deduction_list as $d): ?>
                                                <?php if ($d->employd_id == $employee_details->employee_id): ?>
                                                    <tr>
                                                        <td>
                                                            <?php foreach (@$deduction_category_list as $dc): ?>
                                                                <?php if ($d->deduction_id == $dc->deduction_id): ?>
                                                                    <?= ($lang == 'english') ? $dc->deduction_title_en : $dc->deduction_title_ar; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td><?= ($d->deduction_type == "percent") ? lang('deduction_type_percent') : lang('deduction_type_value'); ?></td>
                                                        <td><?= $d->deduction_value; ?></td>
                                                        <td class="relative"><?= $d->start_date; ?><?= hijjri($d->start_date); ?></td>
                                                        <td class="relative"><?= $d->end_date; ?><?= hijjri($d->end_date); ?></td>
                                                        <td><?= $d->note_deduction ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td class="text-center" colspan="4">
                                                    <strong><?= lang('nothing_to_display') ?></strong></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('vacations') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('leave_type') ?></th>
                                            <th class="text-center"><?= lang('leave_category_duration2') ?></th>
                                            <th class="text-center"><?= lang('going_date') ?></th>
                                            <th class="text-center"><?= lang('coming_date') ?></th>
                                            <th class="text-center"><?= lang('leave_category_quota2') ?></th>
                                            <th class="text-center"><?= lang('leave_affect_stock2') ?></th>
                                            <th class="text-center"><?= lang('replacement') ?></th>
                                            <th class="text-center"><?= lang('the_leave_tel') ?></th>
                                            <th class="text-center"><?= lang('address_in_leave') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach (@$leaves_list as $leave): ?>
                                            <?php if ($leave->employel_id == $employee_details->employee_id): ?>
                                                <tr>
                                                    <td>
                                                        <?php foreach (@$leaves_cat as $lc): ?>
                                                            <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                                <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </td>
                                                    <td><?= $leave->duration ?></td>
                                                    <td class="relative"><?= $leave->going_date ?><?= hijjri($leave->going_date) ?></td>
                                                    <td class="relative"><?= $leave->coming_date ?><?= hijjri($leave->coming_date) ?></td>
                                                    <td><?= $leave->quota ?></td>
                                                    <td><?= ($leave->affect_stock == 1) ? lang('yes') : lang('no'); ?></td>
                                                    <td>
                                                        <?php if (empty($leave->replacement)): ?>
                                                            <?= lang('no-exist') ?>
                                                        <?php else: ?>
                                                            <?php foreach (@$all as $emp): ?>
                                                                <?php if ($emp->employee_id == $leave->replacement): ?>
                                                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?= ($leave->leave_tel) ? $leave->leave_tel : lang('no-exist'); ?></td>
                                                    <td><?= $leave->address_in_leave; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('cutodies') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('custody_reference') ?></th>
                                            <th class="text-center"><?= lang('custody_delivery_date') ?></th>
                                            <th class="text-center"><?= lang('custody_nombre') ?></th>
                                            <th class="text-center"><?= lang('name') ?></th>
                                            <th class="text-center"><?= lang('description') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($custodies_list as $cust): ?>
                                            <?php if ($cust->employee_id == $employee_details->employee_id): ?>
                                                <tr>
                                                    <td><?= $cust->custody_reference ?></td>
                                                    <td class="relative"><?= $cust->delivery_date ?><?= hijjri($cust->delivery_date) ?></td>
                                                    <td><?= $cust->nombre ?></td>
                                                    <td><?= ($lang == 'english') ? $cust->name_en : $cust->name_ar; ?></td>
                                                    <td width="40%"><?= ($lang == 'english') ? $cust->description_en : $cust->description_ar; ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('advances_app') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('date') ?></th>
                                            <th class="text-center"><?= lang('the_advance_type') ?></th>
                                            <th class="text-center"><?= lang('voucher') ?></th>
                                            <th class="text-center"><?= lang('advance_value') ?></th>
                                            <th class="text-center"><?= lang('rest_advance') ?></th>
                                            <th class="text-center"><?= lang('payement_method') ?></th>
                                            <th class="text-center"><?= lang('monthly_installement') ?></th>
                                            <th class="text-center"><?= lang('payement_months') ?></th>
                                            <th class="text-center"><?= lang('last_advance_date') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach (@$advances as $adv): ?>
                                            <tr>
                                                <td class="relative"><?= @$adv->advance_date ?><?= hijjri(@$adv->advance_date); ?></td>
                                                <td>
                                                    <?php if (!empty(@$advances_list)): ?>
                                                        <?php foreach (@$advances_list as $al): ?>
                                                            <?php if ($adv->advance_type_id == $al->advance_id): ?>
                                                                <?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if (empty(@$adv->advance_voucher)): ?>
                                                        <?= lang('no-exist') ?>
                                                    <?php else: ?>
                                                        <?php foreach (@$all as $emp): ?>
                                                            <?php if ($emp->employee_id == $adv->advance_voucher): ?>
                                                                <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?= $adv->advance_value ?> <?= lang('rial') ?></td>
                                                <td><?= $adv->rest ?> <?= lang('rial') ?></td>
                                                <td><?= lang('payement_method' . $adv->payement_method) ?></td>
                                                <td><?= $adv->monthly_installement ?>  <?= lang('rial') ?></td>
                                                <td><?= $adv->payement_months ?></td>
                                                <td class="relative">
                                                    <?= (!empty($adv->last_date)) ? $adv->last_date : lang('no-exist'); ?>
                                                    <?=hijjri($adv->last_date);?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4 class="panel-title"><?= lang('la_training') ?></h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('course_name') ?></th>
                                            <th class="text-center"><?= lang('course_institute_name') ?></th>
                                            <th class="text-center"><?= lang('start_date') ?></th>
                                            <th class="text-center"><?= lang('end_date') ?></th>
                                            <th class="text-center"><?= lang('course_price') ?></th>
                                            <th class="text-center"><?= lang('notes') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$courses_list)): ?>
                                            <?php foreach (@$courses_list as $course): ?>
                                                <tr>
                                                    <td width="9%"><?= $course->course_name ?></td>
                                                    <td><?= $course->course_institute_name ?></td>
                                                    <td class="relative"><?= $course->start_date ?><?= hijjri($course->start_date); ?></td>
                                                    <td class="relative"><?= $course->end_date ?><?= hijjri($course->end_date); ?></td>
                                                    <td width="9%" class="text-center"><?= $course->course_price ?></td>
                                                    <td width="30%"><?= $course->course_note ?></td>
                                                </tr>
                                            <? endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="9" class="text-center">
                                                    <strong><?= lang('nothing_to_display') ?></strong>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


