<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <!-- applications_list -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('settings') ?> </strong>
                </div>
                <div class="panel-body">
                    <!-------------------------------------------------->
                    <hr>
                    <h4><?= ($lang == 'arabic') ? 'إعدادات أوقات الدوام' : 'work time settings'; ?></h4>
                    <hr>
                    <form action="<?= base_url() ?>employee/settings/save_settings"
                          method="post" class="form form-horizontal" id="form"><br>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('num_days_off') ?></label>
                            <div class="col-md-3">
                                <input name="num_days_off" type="number" min="0" class="form-control"
                                       value="<?= @$settings->num_days_off ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('job_time_hours') ?></label>
                            <div class="col-md-3">
                                <input name="job_time_hours" type="number" min="0" class="form-control"
                                       value="<?= @$settings->job_time_hours ?>">
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary btn-block"><?= lang('save') ?></button>
                            </div>
                        </div>
                    </form>
                    <!-------------------------------------------------->
                    <hr>
                    <h4><?= lang('import_attendance') ?></h4>
                    <hr>
                    <form action="<?= base_url() ?>employee/settings/save_attendance"
                          method="post" class="form form-horizontal" id="form2" enctype="multipart/form-data"><br>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('upload_document') ?></label>
                            <div class="col-sm-3">
                                <input type="file" name="excel" class="form-control"
                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                       required>
                                <p style="color: #a94442;"><?= lang('upload_condition') ?></p>
                            </div>
                        </div>
                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" id="sbtn"
                                        class="btn btn-success btn-block"><i
                                            class="fa fa-file-excel-o"></i> <?= lang('upload') ?></button>
                            </div>
                        </div>
                    </form>
                    <!-------------------------------------------------->
                    <hr>
                    <h4><?= ($lang == 'arabic') ? 'إرسال معلومات الحساب لموظف' : 'Send account informations to an employee'; ?></h4>
                    <hr>
                    <form action="<?= base_url() ?>employee/settings/send_account_info"
                          method="post" class="form form-horizontal" id="form3"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('employee_list') ?></label>
                            <div class="col-sm-3">
                                <select class="form-control" name="ids[]" multiple style="min-height: 200px">
                                    <option value="0"><?= lang('all') ?></option>
                                    <?php foreach ($deps as $dep): ?>
                                        <optgroup
                                                label="<?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>">
                                            <?php foreach ($dep->employees as $emp): ?>
                                                <option value="<?= $emp->employee_id ?>">
                                                    <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary btn-block"><i
                                            class="fa fa-envelope-o"></i> <?= lang('send') ?></button>
                            </div>
                        </div>
                    </form>
                    <!-------------------------------------------------->
                    <hr>
                    <h4><?= lang('sms_templates'); ?></h4>
                    <br>
                    <form action="<?= base_url() ?>employee/settings/sms_templates"
                          method="post" class="form form-horizontal sms_form" id="sms_form1"><br>
                        <ul>
                            <li>
                                <div class="sms emp">EMP</div>
                                : رسالة موجهة للموظف صاحب الطلب
                            </li>
                            <li>
                                <div class="sms hrm">HRM</div>
                                : رسالة موجهة لمسؤول الموارد البشرية
                            </li>
                            <li>
                                <div class="sms acp">ACP</div>
                                : رسالة موجهة لأعضاء لجنة الموافقة
                            </li>
                        </ul>
                        <br><br>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td width="25%">
                                    <div class="sms acp">ACP</div>
                                    <?= lang('new_app') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_new_app"
                                                       value="<?= $sms->{$lang . '_new_app'} ?>" class="form-control"
                                                       placeholder="..."></td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('accepted_app_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_accepted_app_from"
                                                       value="<?= $sms->{$lang . '_accepted_app_from'} ?>"
                                                       class="form-control sprintf1" placeholder="..."></td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp">EMP</div>
                                    <?= lang('final_acception') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_final_acception"
                                                       value="<?= $sms->{$lang . '_final_acception'} ?>"
                                                       class="form-control" placeholder="...">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('final_acception_of') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_final_acception_of"
                                                       value="<?= $sms->{$lang . '_final_acception_of'} ?>"
                                                       class="form-control sprintf1" placeholder="..."></td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp">EMP</div>
                                    <?= lang('refuse_app_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_refuse_app_from"
                                                       value="<?= $sms->{$lang . '_refuse_app_from'} ?>"
                                                       class="form-control sprintf1" placeholder="...">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('refuse_of_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_refuse_of_from"
                                                       value="<?= $sms->{$lang . '_refuse_of_from'} ?>"
                                                       class="form-control sprintf2" placeholder="...">
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('delegate_of_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_delegate_of_from"
                                                       value="<?= $sms->{$lang . '_delegate_of_from'} ?>"
                                                       class="form-control sprintf2" placeholder="..."></td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('consultation_of_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_consultation_of_from"
                                                       value="<?= $sms->{$lang . '_consultation_of_from'} ?>"
                                                       class="form-control sprintf2" placeholder="..."></td>
                            </tr>
                            <tr>
                                <td width="25%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('pass_from') ?>
                                </td>
                                <td width="75%"><input type="text" name="<?= $lang; ?>_pass_from"
                                                       value="<?= $sms->{$lang . '_pass_from'} ?>"
                                                       class="form-control sprintf1"
                                                       placeholder="..."></td>
                            </tr>
                        </table>
                        <button type="submit"
                                class="btn btn-primary btn-block" style="width: 300px"><i
                                    class="fa fa-mobile"></i> <?= lang('save') ?></button>

                    </form>

                    <!-------------------------------------------------->
                    <hr>
                    <h4><?= lang('email_templates'); ?></h4>
                    <br>
                    <form action="<?= base_url() ?>employee/settings/email_templates"
                          method="post" class="form form-horizontal sms_form" id="sms_form2"><br>
                        <ul>
                            <li>
                                <div class="sms emp">EMP</div>
                                : رسالة موجهة للموظف صاحب الطلب
                            </li>
                            <li>
                                <div class="sms hrm">HRM</div>
                                : رسالة موجهة لمسؤول الموارد البشرية
                            </li>
                            <li>
                                <div class="sms acp">ACP</div>
                                : رسالة موجهة لأعضاء لجنة الموافقة
                            </li>
                        </ul>
                        <br><br>

                        <input type="hidden" name="type" class="message-type" data-sprintf="0" value="">

                        <table class="table table-bordered table-striped slidetoggle"
                               style="width: 300px; margin-bottom: 0px">
                            <tr>
                                <td width="99%"><i class="fa fa-caret-down"></i> <?= lang('choose_email') ?></td>
                                <td width="1%"></td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-striped list" style="width: 300px">

                            <tr data-type="<?= $lang; ?>_new_app" data-sprintf="0">
                                <td width="99%">
                                    <div class="sms acp">ACP</div>
                                    <?= lang('new_app') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_accepted_app_from" data-sprintf="1">
                                <td width="99%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('accepted_app_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_final_acception" data-sprintf="0">
                                <td width="99%">
                                    <div class="sms emp">EMP</div>
                                    <?= lang('final_acception') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_final_acception_of" data-sprintf="1">
                                <td width="99%">
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('final_acception_of') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_refuse_app_from" data-sprintf="1">
                                <td width="99%">
                                    <div class="sms emp">EMP</div>
                                    <?= lang('refuse_app_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_refuse_of_from" data-sprintf="2">
                                <td width="99%">
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('refuse_of_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_delegate_of_from" data-sprintf="2">
                                <td width="99%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('delegate_of_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_consultation_of_from" data-sprintf="2">
                                <td width="99%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('consultation_of_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>
                            <tr data-type="<?= $lang; ?>_pass_from" data-sprintf="1">
                                <td width="99%">
                                    <div class="sms emp sms2">EMP</div>
                                    <div class="sms hrm">HRM</div>
                                    <?= lang('pass_from') ?>
                                </td>
                                <td width="1%"></td>
                            </tr>

                        </table>
                        <div class="spinner_sects"><i class="fa fa-refresh fa-spin fa-5x fa-fw"></i></div>

                        <br>
                        <textarea id="ck_editor" name="text" value="" class="form-control"
                                  rows="6" required>
                                    </textarea>
                        <?php echo display_ckeditor($editor['ckeditor']); ?>

                        <br>

                        <button type="submit" class="btn btn-primary" style="width: 300px"><i
                                    class="fa fa-envelope-o"></i> <?= lang('save') ?></button>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var choose_email = "<?= lang('choose_email') ?>";
    $(function () {
        $('.slidetoggle').on('click', function () {
            $('.list').slideToggle();
        });
        $('.list tr').on('click', function () {
            $('.list').slideToggle("fast");
            var html = $(this, 'td:first-of-type').html();
            $('.slidetoggle tr:first-of-type').html(html);
            $('.slidetoggle tr:first-of-type td:first-of-type').prepend('<i class="fa fa-caret-down"></i>');
            $('.slidetoggle tr:first-of-type').css('background-color', '#f6f8fa');
            CKEDITOR.instances['ck_editor'].setData('');
            $('.spinner_sects').fadeIn("fast");
            $('.message-type').val($(this).attr('data-type'));
            $('.message-type').attr('data-sprintf',$(this).attr('data-sprintf'));
            ajax_me1($(this).attr('data-type'));
        });
    });

    function ajax_me1(type) {
        var url = "<?=base_url()?>employee/settings/ajax1/" + type;
        $.ajax({
            url: url,
            success: function (data) {
                CKEDITOR.instances['ck_editor'].setData(data);
            }
        }).fail(function () {
            alert('<?= lang('alert_error') ?>');
        }).always(function () {
            $('.spinner_sects').fadeOut("fast");
        });
    }

    $(function () {

        $(".cke_button__save").remove();
        setTimeout(function () {$(".cke_button__save").remove();},1000);
        $(".cke_button__save").on("click", function (e) {
            e.preventDefault();
            $("#sms_form2").submit();
        });

        $("#sms_form2").on("submit", function (e) {
            var sprintf = $('.message-type').attr('data-sprintf');
            var txt = CKEDITOR.instances['ck_editor'].getData();
            var sprintf_count = txt.split("%s").length - 1;
            var email_flag = true;
                if (sprintf==1 && sprintf_count != 1) {
                    email_flag = false;
                }
                else if (sprintf==2 && sprintf_count != 2) {
                    email_flag = false;
                }
                if(!$('.message-type').val().length){
                    email_flag = false;
                }

            if (email_flag) {
                $('#sms_form2').unbind('submit');
                $('#sms_form2').submit();
            }
            else {
                alert("<?=($lang == 'arabic') ? 'الحقل النصي يفتقر لرمز \"%s\" الخاص بإسم الموظف (الموظفين)' : 'The text field is missing the \"%s\" symbol of the employee (s)';?>")
                e.preventDefault();
            }
        });

    });
</script>

<style>

    .spinner_sects {
        display: none;
        width: 100%;
        text-align: center;
        position: relative;
        font-size: 2.1em;
    }

    .spinner_sects .fa {
        position: absolute;
        color: #337AB7;
        margin-top: 170px;
        right: 39%;
        z-index: 99;
    }

    .slidetoggle td, .list td {
        background: #fff;
    }

    .slidetoggle tr:hover td, .list tr:hover td {
        background: #ddd !important;
        cursor: pointer;
    }

    .slidetoggle tr td:last-of-type, .list tr td:last-of-type {
        display: none;
    }

    .list {
        display: none;
        position: absolute;
        z-index: 9999;
    }

    .list * {
        z-index: 9999;
    }

    /*********************/
    .sms_form .table {
        border: 0px !important;
    }

    .sms_form .table td {
        vertical-align: middle;
    }

    .sms_form .form-control {
        border-radius: 0px !important;
        margin-bottom: 0px;
        border: 0px;
        box-shadow: none;
        color: #666;
    }

    .sms_form .sms {
        width: 25px !important;
        display: inline-block;
        padding: 0px 2px;
        color: #fff;
        border-radius: 1px;
        font-size: 10px;
        font-family: cursive;
        font-weight: bold;
        text-align: center;
    }

    .sms_form .emp {
        background: deeppink;
    }

    .sms_form .hrm {
        background: chartreuse;
    }

    .sms_form .acp {
        background: deepskyblue;
    }

    .sms_form .table tr td:first-of-type {
        position: relative;
        padding: 10px 10px 14px 10px;
    }

    .sms_form .table tr td:last-of-type {
        padding: 0px;
        background: #fff !important;
    }

    .sms_form .btn {
        height: 40px !important;
    }

    .sms_form .table .sms {
        position: absolute;
        bottom: 2px;
        left: 3px;
    }

    .sms_form .table .sms.sms2 {
        left: 30px;
    }
</style>

<script>
    $(function () {
        $("#sms_form1 .form-control").on('change, keypress', function () {
            $(this).css('border', '0px');
            $(this).css('color', '#666');
        });
        $('#sms_form1').on('submit', function (e) {
            var sms_flag = true;
            $(".sms_form .form-control").each(function (index) {
                if ($(this).hasClass('sprintf1')) {
                    if ($(this).val().split("%s").length - 1 != 1) {
                        $(this).css('border', '1px solid red');
                        $(this).css('color', 'red');
                        sms_flag = false;
                    }
                }
                else if ($(this).hasClass('sprintf2')) {
                    if ($(this).val().split("%s").length - 1 != 2) {
                        $(this).css('border', '1px solid red');
                        $(this).css('color', 'red');
                        sms_flag = false;
                    }
                }
            });
            if (sms_flag) {
                $('#sms_form1').unbind('submit');
                $('#sms_form1').submit();
            }
            else {
                alert("<?=($lang == 'arabic') ? 'الحقول التالي تفتقر لرمز \"%s\" الخاص بإسم الموظف (الموظفين)' : 'The following fields are missing the \"%s\" symbol of the employee (s)';?>")
                e.preventDefault();
            }
        });
    });
</script>

<script type="text/javascript">
    function save_app_msg_ar(id) {
        var text = encodeURI($('#ar' + id).closest('.row').find('input').val());
        var vurl = "<?php echo base_url() ?>employee/settings/save_app_msg_ar/" + id + "/" + text;
        if (id) {
            $('#ar' + id).html('<i class="fa fa-refresh fa-spin fa-fw"></i>');

            $.ajax({
                url: vurl,
                success: function (data) {

                    if (data == 'true') {
                        alert("<?=lang('saved_successfully')?>");
                        $('#ar' + id).removeClass('btn-primary');
                        $('#ar' + id).addClass('btn-success');
                        $('#ar' + id).html('<i class="fa fa-check-circle"></i>');
                    }
                    else {
                        alert("<?=lang('clock_error_message')?>");
                        $('#ar' + id).html('<i class="fa fa-check-circle"></i>');
                    }

                },
                error: function () {
                    alert("<?=lang('clock_error_message')?>");
                    $('#ar' + id).html('<i class="fa fa-check-circle"></i>');
                }
            });

        }
    }
</script>