<div class="col-md-12">
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-12">

                <!-- Ideal Employee -->
<?php if(!empty(@$ideal_employee)):?>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('ideal_employee') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-2">
                            <div class="fileinput-new thumbnail" style="width: 144px; height: 158px; margin-top: 14px; margin-left: 16px; background-color: #EBEBEB;">
                                <?php if ($ideal_employee->photo): ?>
                                    <img src="<?php echo base_url() . $ideal_employee->photo; ?>" style="width: 142px; height: 148px; border-radius: 3px;" >
                                <?php else: ?>
                                    <img src="<?php echo base_url() ?>/img/admin.png" alt="Employee_Image">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-8 ">
                            <div style="margin-left: 20px;">
                                <h3><?= ($lang == 'english') ? $ideal_employee->full_name_en : $ideal_employee->full_name_ar; ?></h3>
                                <hr />
                                <table class="table table-non-bordered">
                                    <tr>
                                        <td><strong><?= lang('department') ?></strong></td>
                                        <td><?= ($lang == 'english') ? $ideal_employee->department_name : $ideal_employee->department_name_ar; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong><?= lang('designation') ?></strong></td>
                                        <?php if ($ideal_employee->designations_id != 0): ?>
                                            <td><?= ($lang == 'english') ? $ideal_employee->designations : $ideal_employee->designations_ar; ?></td>
                                        <?php else: ?>
                                            <td><?= lang('no-exist') ?></td>
                                        <?php endif; ?>
                                    </tr>
                                    <tr>
                                        <td><strong><?= lang('job_title') ?></strong></td>
                                        <td><?= ($lang == 'english') ? $ideal_employee->job_titles_name_en : $ideal_employee->job_titles_name_ar; ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong><?= lang('joining_date') ?></strong></td>
                                        <td><?= $ideal_employee->joining_date ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
<?php endif; ?>

                <!-- irrigularity_types -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('irrigularity_types') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="26%" class="text-center"><?= lang('name') ?></th>
                                    <th width="13%" class="text-center"><?= lang('first') ?></th>
                                    <th width="13%" class="text-center"><?= lang('second') ?></th>
                                    <th width="13%" class="text-center"><?= lang('third') ?></th>
                                    <th width="13%" class="text-center"><?= lang('fourth') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($irrigularity_categories as $ic): ?>
                                    <tr>
                                        <td><?= ($lang == 'english') ? $ic->name_en : $ic->name_ar; ?></td>
                                        <td><?= ($lang == 'english') ? $ic->first_en : $ic->first_ar; ?></td>
                                        <td><?= ($lang == 'english') ? $ic->second_en : $ic->second_ar; ?></td>
                                        <td><?= ($lang == 'english') ? $ic->third_en : $ic->third_ar; ?></td>
                                        <td><?= ($lang == 'english') ? $ic->fourth_en : $ic->fourth_ar; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- leaves_types -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('leaves_types') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center"><?= lang('name') ?></th>
                                    <th class="text-center"><?= lang('leave_category_duration') ?></th>
                                    <th class="text-center"><?= lang('leave_category_quota') ?></th>
                                    <th class="text-center"><?= lang('paid_leave') ?></th>
                                    <th class="text-center"><?= lang('leave_affect_stock') ?></th>
                                    <th class="text-center"><?= lang('leave_calculation_type') ?></th>
                                    <th class="text-center"><?= lang('set_replacement') ?></th>
                                    <th class="text-center"><?= lang('leave_tel') ?></th>
                                    <th class="text-center"><?= lang('allowance_cat_ids') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($leaves_categories as $lc): ?>
                                    <tr>
                                        <td><?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?></td>
                                        <td class="text-center"><?= $lc->leave_duration ?></td>
                                        <td class="text-center"><?= $lc->leave_quota ?> %</td>
                                        <td class="text-center"><?= ($lc->paid_leave == 1) ? lang('yes') : lang('no') ?></td>
                                        <td class="text-center"><?= ($lc->affect_balance == 1) ? lang('yes') : lang('no'); ?></td>
                                        <td class="text-center"><?= ($lc->calc_type == 1) ? lang('contractual_year') : lang('normal_year'); ?></td>
                                        <td class="text-center"><?= ($lc->set_replacement == 1) ? lang('yes') : lang('no'); ?></td>
                                        <td class="text-center"><?= ($lc->leave_tel == 1) ? lang('yes') : lang('no'); ?></td>
                                        <td class="text-center">
                                            <ul>
                                                <?php $allowance_cat_ids = explode(';', @$lc->allowance_cat_ids); ?>
                                                <?php foreach (@$all_allowance_categories as $ac): ?>
                                                    <?php if (in_array($ac->allowance_id, $allowance_cat_ids)): ?>
                                                        <li><?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    </div>
                </div>

                <!-- center_documentations -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('center_documentations') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <?php foreach ($document_list as $doc): ?>
                                <li><a href="<?= base_url() ?>img/center_documentations/<?= $doc->link ?>" target="_blanck"><?= ($lang=="english")?$doc->doc_name_en:$doc->doc_name_ar; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>