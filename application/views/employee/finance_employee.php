<?php echo message_box('success'); ?>

<style type="text/css">
    .boxino {
        border: 1px solid gray;
        background: #337AB7;
        color: #fff;
        padding: 15px 15px 15px 50px;
        display: inline-block;
        border-radius: 8px;
    }

    .nav-tabs-custom {
        box-shadow: none !important;
    }

    .tab-content {
        border: 1px solid #f4f4f4;
        border-top: none;
    }

    .print {
        display: none;
    }

    @media print {
        .print {
            display: block;
            -webkit-print-color-adjust: exact !important;
        }
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="boxino">
            <table>
                <tr>
                    <td width="170"><b><?= lang('name') ?> : </b></td>
                    <td><?= ($lang == 'english') ? $employee_info->full_name_en : $employee_info->full_name_ar; ?></td>
                </tr>
                <tr>
                    <td width="170"><b><?= lang('employee_id') ?> : </b></td>
                    <td>
                        <small><?= $employee_info->employment_id ?></small>
                    </td>
                </tr>
                <tr>
                    <td><b><?= lang('job_time') ?> :</b></td>
                    <td><?= lang($employee_info->job_time) ?></td>
                </tr>
                <tr>
                    <td>-------------------------</td>
                    <td>---------------</td>
                </tr>
                <tr>
                    <td width="170"><b><?= lang('department') ?> : </b></td>
                    <td><?= ($lang == 'english') ? $employee_info->department_name : $employee_info->department_name_ar; ?></td>
                </tr>
                <tr>
                    <td><b><?= lang('direct_manager') ?> :</b></td>
                    <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar']; ?></td>
                </tr>
                <tr>
                    <td><b><?= lang('hr_manager') ?> :</b></td>
                    <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar']; ?></td>
                </tr>
            </table>
        </div>
        <br><br><br><br>

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#finances_info" data-toggle="tab"><?= lang('finance_employee') ?></a></li>
                <li><a href="#finances_option" data-toggle="tab"><?= lang('finance_option') ?></a></li>
                <li><a href="#finances_deduction" data-toggle="tab"><?= lang('finance_permanent_deduction') ?></a></li>
                <li><a href="#finances_other" data-toggle="tab"><?= lang('finance_provision') ?></a></li>
                <li><a href="#finances_houcing" data-toggle="tab"><?= lang('finance_houcing') ?></a></li>
                <li><a href="#cutodies" data-toggle="tab"><?= lang('cutodies') ?></a></li>

            </ul>
            <div class="tab-content no-padding">

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- finances_info -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane active" id="finances_info" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <div class="col-sm-6">
                                <div class="col-sm-3"><label><?= lang('finance_basic_salary') ?></label></div>
                                <div class="col-sm-5"><?= @$employee_info->employee_salary ?></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-3"><label><?= lang('finance_all_salary') ?></label></div>
                                <div class="col-sm-5">
                                    <div class="final-salary"></div>
                                </div>
                            </div>

                            <br><br><br><br>

                            <?php $today = date_create_from_format('Y-m-d', $today)->format('Ymd'); ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('finance_bonus') ?></th>
                                    <th class="text-center"><?= lang('finance_value') ?></th>
                                    <th class="text-center"><?= lang('finance_from') ?></th>
                                    <th class="text-center"><?= lang('finance_to') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('finance_reservation') ?> %</th>
                                    <th class="text-center"><?= lang('total') ?></th>
                                    <th class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$allowances_list)): $total = 0; ?>
                                    <?php foreach (@$allowances_list as $al): ?>
                                        <tr>
                                            <td>
                                                <?php if ($al->allowance_id == 0): ?>
                                                    <?= lang('other_dues') ?>
                                                <?php else: ?>
                                                    <?php foreach (@$allowances_cat_list as $acl): ?>
                                                        <?php if ($acl->allowance_id == $al->allowance_id): ?>
                                                            <?= ($lang == 'english') ? $acl->allowance_title_en : $acl->allowance_title_ar; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($al->allowance_type == 'value'): ?>
                                                    <span class="pull-left"><?= $al->allowance_value ?></span>
                                                <?php else: ?>
                                                    <span class="pull-left"><?= $al->allowance_value ?> (%)</span>
                                                    <span class="pull-right"
                                                          style="border:1px solid gray; padding: 0px 10px"><?= ($employee_info->employee_salary / 100) * ($al->allowance_value) ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td class="relative"><?= $al->allowance_date_from ?><?= hijjri($al->allowance_date_from); ?></td>
                                            <td class="relative"><?= $al->allowance_date_to ?><?= hijjri($al->allowance_date_to); ?></td>
                                            <td>
                                                <?php $from = date_create_from_format('Y-m-d', $al->allowance_date_from)->format('Ymd') ?>
                                                <?php $to = ($al->allowance_date_to != '') ? date_create_from_format('Y-m-d', $al->allowance_date_to)->format('Ymd') : date_create_from_format('Y-m-d', "9999-12-30")->format('Ymd'); ?>

                                                <?php if ($from <= $today and $today <= $to): ?>
                                                    <span class="label label-success"><?= lang('active') ?></span>
                                                <?php else: ?>
                                                    <span class="label label-warning"><?= lang('inactive') ?></span>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($from <= $today and $today <= $to): ?>
                                                    <span class="label label-success"><?= $al->reservation ?> %</span>
                                                <?php else: ?>
                                                    <span class="label label-warning"><?= $al->reservation ?> %</span>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if ($al->allowance_type == 'value'): ?>
                                                    <?php $val = ($al->allowance_value / 100) * (100 - $al->reservation); ?>
                                                    <?php if ($from <= $today and $today <= $to): ?>
                                                        <span class="label label-success"><?= $val ?></span>
                                                        <?php $total += $val; ?>
                                                    <?php else: ?>
                                                        <span class="label label-warning"><?= $val ?></span>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <?php $val = (($employee_info->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation); ?>
                                                    <?php if ($from <= $today and $today <= $to): ?>
                                                        <span class="label label-success"><?= $val ?></span>
                                                        <?php $total += $val; ?>
                                                    <?php else: ?>
                                                        <span class="label label-warning"><?= $val ?></span>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-success btn-xs" data-toggle="modal"
                                                   data-target="#myModal<?= $al->allowance_type_id ?>"><i
                                                            class="fa fa-edit"></i> <?= lang('edit') ?></a>
                                                <a href="<?= base_url() ?>employee/employee/delete_allowance/<?= $al->allowance_type_id ?>/<?= $employee_info->employee_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="6"></td>
                                        <td><?= $total ?></td>
                                        <td></td>
                                    </tr>
                                <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="8">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>

                            <form action="<?= base_url() ?>employee/employee/save_allowance/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form"><br><br><br><br>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('allowances') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-7">
                                        <select name="allowance_id" class="form-control allowances" required=""
                                                onchange="allowancevalue($(this))">
                                            <option value=""></option>
                                            <?php if (!empty(@$allowances_cat_list)): ?>
                                                <?php foreach (@$allowances_cat_list as $al): ?>
                                                    <option value="<?= $al->allowance_id ?>"
                                                            data-value="<?= $al->allowance_value ?>"
                                                            data-type="<?= $al->allowance_type ?>">
                                                        <?= ($lang == 'english') ? $al->allowance_title_en : $al->allowance_title_ar; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                                <option value="0" data-type="new"><?= lang('other_dues') ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_value') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="number" name="allowance_value" id="allowance_value"
                                               class="form-control" required="" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('allowance_type') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="allowance_type" id="allowance_type"
                                               class="form-control" required="" readonly="">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:-20px;">
                                    <div class="col-md-offset-3 col-md-7">
                                        <small>
                                            value : <?= lang('value') ?><br>
                                            percent : <?= lang('percent') ?>
                                        </small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_from') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="allowance_date_from"
                                               class="form-control hijri_datepicker" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_to') ?> </label>
                                    <div class="col-md-3">
                                        <input type="text" name="allowance_date_to"
                                               class="form-control  hijri_datepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_reservation') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="number" min="0" max="100" name="reservation" class="form-control"
                                               required="">
                                    </div>
                                </div>

                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>

                            <!-- Start Modal -->
                            <?php foreach (@$allowances_list as $al): ?>

                                <div class="modal fade" id="myModal<?= $al->allowance_type_id ?>" tabindex="-1"
                                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content" style="font-size: 1.2em;">
                                            <div class="modal-body">
                                                <form action="<?= base_url() ?>employee/employee/save_allowance/<?= $employee_info->employee_id ?>/<?= $al->allowance_type_id ?>"
                                                      method="post" class="form form-horizontal" id="form">
                                                    <br><br><br><br>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('allowances') ?>
                                                            <span class="required"> *</span></label>
                                                        <div class="col-md-7">
                                                            <select name="allowance_id" class="form-control allowances"
                                                                    required=""
                                                                    onchange="allowancevalue<?= $al->allowance_type_id ?>($(this))">
                                                                <option value=""></option>
                                                                <?php if (!empty(@$allowances_cat_list)): ?>
                                                                    <?php foreach (@$allowances_cat_list as $acl): ?>
                                                                        <option value="<?= $acl->allowance_id ?>"
                                                                                data-value="<?= $acl->allowance_value ?>"
                                                                                data-type="<?= $acl->allowance_type ?>"
                                                                            <?= ($al->allowance_id == $acl->allowance_id) ? 'selected' : ''; ?>>
                                                                            <?= ($lang == 'english') ? $acl->allowance_title_en : $acl->allowance_title_ar; ?>
                                                                        </option>
                                                                    <?php endforeach; ?>
                                                                    <option value="0"
                                                                            data-type="new" <?= ($al->allowance_id == 0) ? 'selected' : ''; ?>><?= lang('other_dues') ?></option>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('finance_value') ?>
                                                            <span class="required"> *</span></label>
                                                        <div class="col-md-3">
                                                            <input type="number" name="allowance_value"
                                                                   value="<?= $al->allowance_value ?>"
                                                                   id="allowance_value<?= $al->allowance_type_id ?>"
                                                                   class="form-control" required="" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('allowance_type') ?>
                                                            <span class="required"> *</span></label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="allowance_type"
                                                                   value="<?= $al->allowance_type ?>"
                                                                   id="allowance_type<?= $al->allowance_type_id ?>"
                                                                   class="form-control" required="" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-top:-20px;">
                                                        <div class="col-md-offset-3 col-md-7">
                                                            <small>
                                                                value : <?= lang('value') ?><br>
                                                                percent : <?= lang('percent') ?>
                                                            </small>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('finance_from') ?>
                                                            <span class="required"> *</span></label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="allowance_date_from"
                                                                   value="<?= $al->allowance_date_from ?>"
                                                                   class="form-control hijri_datepicker" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('finance_to') ?></label>
                                                        <div class="col-md-3">
                                                            <input type="text" name="allowance_date_to"
                                                                   value="<?= $al->allowance_date_to ?>"
                                                                   class="form-control  hijri_datepicker">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= lang('finance_reservation') ?>
                                                            <span class="required"> *</span></label>
                                                        <div class="col-md-3">
                                                            <input type="number" min="0" max="100"
                                                                   value="<?= $al->reservation ?>" name="reservation"
                                                                   class="form-control" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group margin">
                                                        <div class="col-sm-offset-3 col-sm-5">
                                                            <button type="submit" id="sbtn"
                                                                    class="btn btn-primary"><?= lang('save') ?></button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <script type="text/javascript">
                                                    function allowancevalue <?= $al->allowance_type_id ?>(s) {
                                                        var type = s.find(":selected").attr('data-type');
                                                        if (type == "value") {
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').prop('readonly ', 'true');
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').val(s.find(":selected").attr('data-value'));
                                                            $('#allowance_type<?= $al->allowance_type_id ?>').val(s.find(":selected").attr('data-type'));
                                                        } else if (type == "percent") {
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').prop('readonly ', 'true');
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').val(s.find(":selected").attr('data-value'));
                                                            $('#allowance_type<?= $al->allowance_type_id ?>').val(s.find(":selected").attr('data-type'));
                                                        } else if (type == "new") {
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').val(0);
                                                            $('#allowance_value<?= $al->allowance_type_id ?>').prop('readonly', false);
                                                            $('#allowance_type<?= $al->allowance_type_id ?>').val('value');
                                                        }
                                                    }
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <!-- End Modal -->

                        </div>
                    </div>
                </div>

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- finances_option -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="finances_option" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form action="<?= base_url() ?>employee/employee/save_finance_option/<?= @$employee_info->employee_id ?>/<?= @$employee_info->finance_info_id ?>"
                                  method="post" class="form form-horizontal form-groups-bordered" id="form">
                                <fieldset>
                                    <legend><?= lang('fieldset_bank') ?></legend>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('payment_method'); ?></label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="payment_method" required="">
                                                <option value=""><?= ($lang == 'arabic') ? 'اختر طريقة دفع' : 'Select a payment method'; ?></option>
                                                <option value="1" <?= (@$employee_info->payment_method == 1) ? 'selected' : ''; ?>><?= ($lang == 'arabic') ? 'بنك' : 'Bank'; ?></option>
                                                <option value="2" <?= (@$employee_info->payment_method == 2) ? 'selected' : ''; ?>><?= ($lang == 'arabic') ? 'شيكات' : 'Checks'; ?></option>
                                                <option value="3"<?= (@$employee_info->payment_method == 3) ? 'selected' : ''; ?>><?= ($lang == 'arabic') ? 'نقدا' : 'Cash money'; ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('account_number'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="account_number" required=""
                                                   value="<?= $employee_info->account_number ?>" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('bank_name'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="bank_name" required=""
                                                   value="<?= $employee_info->bank_name ?>" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('branch_name'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="branch_name" required=""
                                                   value="<?= $employee_info->branch_name ?>" class="form-control"/>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend><?= lang('fieldset_med') ?></legend>
                                    <div class="form-group  col-sm-6">
                                        <label class="col-sm-3 control-label"><?= lang('medical_insur') ?></label>
                                        <div class="col-sm-5">
                                            <select name="med_insur" id="medical_insur" class="form-control">
                                                <option value="0" <?= (@$employee_info->med_insur == 0) ? 'selected' : ''; ?>><?= lang('medical_insur_select') ?></option>
                                                <option value="1" <?= (@$employee_info->med_insur == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                <option value="2"<?= (@$employee_info->med_insur == 2) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group  col-sm-6">
                                        <label class="col-sm-3 control-label"><?= lang('medical_insur_type') ?></label>
                                        <div class="col-sm-5">
                                            <select name="med_insur_type" id="med_insur_type" class="form-control"
                                                    disabled="">
                                                <option value="0" <?= (@$employee_info->med_insur_type == 0) ? 'selected' : ''; ?>><?= lang('medical_insur_type_select') ?></option>
                                                <option value="1" <?= (@$employee_info->med_insur_type == 1) ? 'selected' : ''; ?>><?= lang('medical_insur_type_all') ?></option>
                                                <option value="2" <?= (@$employee_info->med_insur_type == 2) ? 'selected' : ''; ?>><?= lang('medical_insur_type_part') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('insurance_percent'); ?>
                                            (%)</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" disabled=""
                                                   value="<?= @$employee_info->m_insurance_percent ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('societe_percent'); ?>
                                            (%)</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" disabled=""
                                                   value="<?= @$employee_info->m_societe_percent ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label col-sm-3"><?= lang('medical_date'); ?></label>
                                        <div class="input-group col-sm-5">
                                            <input type="text" name="med_insur_start_date"
                                                   value="<?= @$employee_info->med_insur_start_date ?>"
                                                   class="form-control hijri_datepicker">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label col-sm-3"><?= lang('medical_end_date'); ?></label>
                                        <div class="input-group col-sm-5">
                                            <input type="text" name="med_insur_end_date"
                                                   value="<?= @$employee_info->med_insur_end_date ?>"
                                                   class="form-control hijri_datepicker">
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#medical_insur').on('change', function () {
                                                if ($(this).val() == '1' || $(this).val() == '0')
                                                    $('#med_insur_type').prop('disabled', false);
                                                else
                                                    $('#med_insur_type').prop('disabled', 'true');
                                            });
                                        });
                                    </script>
                                </fieldset>
                                <fieldset>
                                    <legend><?= lang('fieldset_social') ?></legend>
                                    <div class="form-group  col-sm-6">
                                        <label class="col-sm-3 control-label"><?= lang('fieldset_social'); ?></label>
                                        <div class="col-sm-5">
                                            <select name="social_insurance" id="soc_insur" class="form-control"
                                                    required="">
                                                <option value="0" <?= (@$employee_info->social_insurance == 0) ? 'selected' : ''; ?>><?= lang('social_insur_select') ?></option>
                                                <option value="1" <?= (@$employee_info->social_insurance == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                <option value="2" <?= (@$employee_info->social_insurance == 2) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="col-sm-3 control-label"><?= lang('social_type'); ?></label>
                                        <div class="col-sm-5">
                                            <select name="social_insurance_type" id="social_insurance_type"
                                                    class="form-control" disabled="" required="">
                                                <option value="0" <?= (@$employee_info->social_insurance_type == "0") ? 'selected' : ''; ?>><?= lang('social_insur_select') ?></option>
                                                <option value="saudi1" <?= (@$employee_info->social_insurance_type == "saudi1") ? 'selected' : ''; ?>><?= lang('saudi1') ?></option>
                                                <option value="saudi2" <?= (@$employee_info->social_insurance_type == "saudi2") ? 'selected' : ''; ?>><?= lang('saudi2') ?></option>
                                                <option value="non-saudi" <?= (@$employee_info->social_insurance_type == "non-saudi") ? 'selected' : ''; ?>><?= lang('non-saudi') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('insurance_percent'); ?>
                                            (%)</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" disabled=""
                                                   value="<?= @$employee_info->insurance_percent ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('societe_percent'); ?>
                                            (%)</label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" disabled=""
                                                   value="<?= @$employee_info->societe_percent ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('social_salary'); ?></label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" name="social_salary"
                                                   value="<?= @$employee_info->social_salary ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label col-sm-3"><?= lang('social_date'); ?> </label>
                                        <div class="input-group col-sm-5">
                                            <input type="text" name="social_start_date"
                                                   value="<?= @$employee_info->social_start_date ?>"
                                                   class="form-control hijri_datepicker">
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                        $(function () {
                                            $('#soc_insur').on('change', function () {
                                                if ($(this).val() == '1' || $(this).val() == '0')
                                                    $('#social_insurance_type').prop('disabled', false);
                                                else
                                                    $('#social_insurance_type').prop('disabled', 'true');
                                            });
                                        });
                                    </script>
                                </fieldset>
                                <fieldset>
                                    <legend><?= lang('fieldset_other') ?></legend>
                                    <div class="form-group col-sm-6">
                                        <label for="field-1"
                                               class="col-sm-3 control-label"><?= lang('option_end_service') ?></label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="option_end_service" required="">
                                                <option value=""></option>
                                                <option value="1" <?= (@$employee_info->option_end_service == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                <option value="0" <?= (@$employee_info->option_end_service == 0) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label col-sm-3"><?= lang('option_date') ?> </label>
                                        <div class="input-group col-sm-5 relative">
                                            <?=hijjri(@$employee_info->retirement_date);?>
                                            <input type="text" name="retirement_date"
                                                   class="form-control hijri_datepicker"
                                                   value="<?= @$employee_info->retirement_date ?>">
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="field-1"
                                               class="col-sm-2 control-label"><?= lang('prod_note') ?> </label>
                                        <div class="col-sm-8">
                                            <textarea name="note" class="form-control"
                                                      rows="4"><?= @$employee_info->note ?></textarea>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- finances_deduction -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="finances_deduction" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('finance_deduction_bonus') ?></th>
                                    <th class="text-center"><?= lang('finance_deduction_type') ?></th>
                                    <th class="text-center"><?= lang('finance_deduction_value') ?></th>
                                    <th class="text-center"><?= lang('start_date') ?></th>
                                    <th class="text-center"><?= lang('end_date') ?></th>
                                    <th width="30%" class="text-center"><?= lang('prod_note') ?></th>
                                    <th width="6%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$deduction_list)): ?>
                                    <?php foreach (@$deduction_list as $d): ?>
                                        <tr>
                                            <td>
                                                <?php foreach (@$deduction_category_list as $dc): ?>
                                                    <?php if ($d->deduction_id == $dc->deduction_id): ?>
                                                        <?= ($lang == 'english') ? $dc->deduction_title_en : $dc->deduction_title_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td><?=($d->deduction_type=="percent")?lang('deduction_type_percent'):lang('deduction_type_value');?></td>
                                            <td><?=$d->deduction_value;?></td>
                                            <td class="relative"><?=$d->start_date;?><?=hijjri($d->start_date);?></td>
                                            <td class="relative"><?=$d->end_date;?><?=hijjri($d->end_date);?></td>
                                            <td><?= $d->note_deduction ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>employee/employee/delete_deduction/<?= $d->deduction_type_id ?>/<?= $employee_info->employee_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="7">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <br><br><br><br>

                            <form action="<?= base_url() ?>employee/employee/save_deduction/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('deduction') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-7">
                                        <select name="deduction_id" class="form-control allowances" required=""
                                                onchange="deduction($(this))">
                                            <option value=""></option>
                                            <?php if (!empty(@$deduction_category_list)): ?>
                                                <?php foreach (@$deduction_category_list as $dc): ?>
                                                    <option value="<?= $dc->deduction_id ?>"
                                                            data-value="<?= $dc->deduction_value ?>"
                                                            data-type="<?= $dc->deduction_type ?>">
                                                        <?= ($lang == 'englis') ? $dc->deduction_title_en : $dc->deduction_title_ar; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('deduction_type') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <select name="deduction_type" class="form-control deduction_type" required="">
                                            <option value=""></option>
                                            <option value="percent"><?= lang('deduction_type_percent') ?></option>
                                            <option value="value"><?= lang('deduction_type_value') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_deduction_value') ?><span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="deduction_value" class="form-control deduction_value"
                                               required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('start_date') ?><span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="start_date" class="form-control hijri_datepicker" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('end_date') ?></label>
                                    <div class="col-md-3">
                                        <input type="text" name="end_date" class="form-control hijri_datepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('prod_note') ?> </label>
                                    <div class="col-sm-7">
                                        <textarea name="note_deduction" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-3">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                            <script type="text/javascript">
                                function deduction(s) {
                                    var type = s.find(':selected').attr('data-type');
                                    var value = s.find(':selected').attr('data-value');
                                    $('.deduction_type').val(type);
                                    $('.deduction_value').val(value);
                                }
                            </script>
                        </div>
                    </div>
                </div>

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- finances_other -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="finances_other" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('finance_provision_bonus') ?></th>
                                    <th class="text-center"><?= lang('finance_provision_value') ?></th>
                                    <th class="text-center"><?= lang('start_date') ?></th>
                                    <th class="text-center"><?= lang('end_date') ?></th>
                                    <th width="40%" class="text-center"><?= lang('prod_note') ?></th>
                                    <th width="10%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$provision_list)): ?>
                                    <?php foreach (@$provision_list as $p): ?>
                                        <tr>
                                            <td>
                                                <?php foreach (@$provision_cat_list as $pc): ?>
                                                    <?php if ($pc->provision_category_id == $p->provision_category_id): ?>
                                                        <?= ($lang == 'english') ? $pc->provision_title_en : $pc->provision_title_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td><?= $p->provision_value ?> <?= lang('rial') ?></td>
                                            <td class="relative"><?= $p->start_date ?><?= hijjri($p->start_date); ?></td>
                                            <td class="relative"><?= $p->end_date ?><?= hijjri($p->end_date); ?></td>
                                            <td><?= $p->note_provision ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>employee/employee/delete_provision/<?= $p->provision_id ?>/<?= $employee_info->employee_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td class="text-center" colspan="6">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <br><br><br><br>
                            <form action="<?= base_url() ?>employee/employee/save_provisions/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('finance_provision_type') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-md-7">
                                        <select name="provision_category_id" class="form-control allowances" required=""
                                                onchange="deduction($(this))">
                                            <option value=""></option>
                                            <?php if (!empty(@$provision_cat_list)): ?>
                                                <?php foreach (@$provision_cat_list as $pc): ?>
                                                    <option value="<?= $pc->provision_category_id ?>">
                                                        <?= ($lang == 'englis') ? $pc->provision_title_en : $pc->provision_title_ar; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('finance_provision_value') ?> <span
                                                class="required"> *</span></label>
                                    <div class="col-sm-3">
                                        <input type="number" name="provision_value" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('start_date') ?><span
                                                class="required"> *</span></label>
                                    <div class="col-md-3">
                                        <input type="text" name="start_date" class="form-control hijri_datepicker" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('end_date') ?></label>
                                    <div class="col-md-3">
                                        <input type="text" name="end_date" class="form-control hijri_datepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('prod_note') ?> </label>
                                    <div class="col-sm-7">
                                        <textarea name="note_provision" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-3">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- finances_houcing -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="finances_houcing" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form action="<?= base_url() ?>employee/employee/save_houcing/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form">
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_status') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <select name="houcing_status" class="form-control" required=""
                                                onchange="houcing($(this))">
                                            <option></option>
                                            <option value="1" <?= ($employee_info->houcing_status == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                            <option value="0" <?= ($employee_info->houcing_status == 0) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_count') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="houcing_type" class="form-control houcing"
                                                required="" <?= ($employee_info->houcing_status != 1) ? 'disabled' : ''; ?>>
                                            <option <?= ($employee_info->houcing_type == 0) ? 'selected' : ''; ?>></option>
                                            <option value="1" <?= ($employee_info->houcing_type == 1) ? 'selected' : ''; ?>><?= lang('percent') ?></option>
                                            <option value="2" <?= ($employee_info->houcing_type == 2) ? 'selected' : ''; ?>><?= lang('value') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_value2') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" value="<?= $employee_info->houcing_value ?>"
                                               name="houcing_value" min="0"
                                               class="form-control houcing" <?= ($employee_info->houcing_status != 1) ? 'disabled' : ''; ?>
                                               required=""/>
                                    </div>
                                </div>
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                            <script type="text/javascript">
                                function houcing(s) {
                                    var val = s.find(':selected').val();
                                    console.log(val);
                                    if (val == '1') {
                                        console.log($('.houcing'));
                                        $('.houcing').prop('disabled', false);
                                    } else {
                                        $('.houcing').prop('disabled', true);
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </div>


                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- custodies -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="cutodies" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('custody_reference') ?></th>
                                    <th class="text-center"><?= lang('custody_delivery_date') ?></th>
                                    <th class="text-center"><?= lang('custody_nombre') ?></th>
                                    <th class="text-center"><?= lang('name') ?></th>
                                    <th class="text-center"><?= lang('description') ?></th>
                                    <th class="text-center"><?= lang('files') ?></th>
                                    <th class="text-center"><?= lang('receipt_confirmation') ?></th>
                                    <th class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$custodies_list)): ?>
                                    <?php foreach (@$custodies_list as $cl): ?>
                                        <tr>
                                            <td><?= $cl->custody_reference; ?></td>
                                            <td class="relative" width="15%"><?= $cl->delivery_date; ?><?= hijjri($cl->delivery_date); ?></td>
                                            <td><?= $cl->nombre; ?></td>
                                            <td><?= ($lang == 'english') ? $cl->name_en : $cl->name_ar; ?></td>
                                            <td width="20%"><?= ($lang == 'english') ? $cl->description_en : $cl->description_ar; ?></td>
                                            <td>
                                                <?php if (!empty($cl->document)): ?>
                                                    <a target="_blank"
                                                       href="<?= base_url() ?>img/uploads/<?= $cl->document; ?>"><b><?= lang('download') ?></b></a>
                                                <?php else: ?>
                                                    <?= lang('no-exist') ?>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center" width="10%">
                                                <?php if ($cl->received == 1): ?>
                                                    <i class="fa fa-check" style="color:#4cae4c"></i>
                                                <?php else: ?>
                                                    <i class="fa fa-check" style="color:#d43f3a"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center" width="26%">
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <?php if ($cl->received == 1): ?>
                                                        <a class="btn btn-warning btn-xs" disabled=""><i
                                                                    class="fa fa-star"></i> <?= lang('receipt_confirmation') ?>
                                                        </a>
                                                    <?php else: ?>
                                                        <a href="<?= base_url() ?>employee/employee/receipt_confirmation/<?= $cl->custody_id ?>/<?= $employee_info->employee_id ?>"
                                                           class="btn btn-warning btn-xs"><i
                                                                    class="fa fa-star"></i> <?= lang('receipt_confirmation') ?>
                                                        </a>
                                                    <?php endif; ?>
                                                    <a href="#" class="btn btn-success btn-xs" data-toggle="modal"
                                                       data-target="#custodyModal<?= $cl->custody_id ?>"><i
                                                                class="fa fa-edit"></i> <?= lang('edit') ?></a>
                                                <?php endif; ?>
                                                <a class="btn btn-primary btn-xs"
                                                   onclick="print_custody('custody_<?= $cl->custody_id ?>');"><i
                                                            class="fa fa-print"></i> <?= lang('print') ?></a>
                                                <a href="<?= base_url() ?>employee/employee/delete_custody/<?= $cl->custody_id ?>/<?= $employee_info->employee_id ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7" class="text-center">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                            <br><br><br><br>
                            <form action="<?= base_url() ?>employee/employee/save_custody/<?= $employee_info->employee_id ?>"
                                  method="post" class="form form-horizontal" id="form" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_reference') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-3">
                                        <input type="number" min="0" name="custody_reference" id="custody_reference"
                                               class="form-control" required="">
                                    </div>
                                    <div class="col-sm-2"><a class="btn btn-primary btn-block"
                                                             onclick="generate()"><?= lang('generate_auto') ?></a></div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_name_ar') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_ar" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_name_en') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_en" class="form-control" required="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_delivery_date') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="delivery_date" class="form-control hijri_datepicker"
                                               required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('custody_nombre') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" name="nombre" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('upload_document') ?></label>
                                    <div class="col-sm-3">
                                        <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                            <div class="btn btn-primary btn-file btn-block"><span
                                                        class="fileinput-new"><?= lang('select_file') ?></span>
                                                <span class="fileinput-exists"><?= lang('change') ?></span>
                                                <input type="file" name="document" class="form-control">
                                            </div>
                                            <div class="fileinput-filename"></div>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                               style="float: none; color:red;">&times;</a>
                                        </div>
                                        <div id="msg_pdf" style="color: #e11221"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_description_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="description_ar" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('custody_description_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="description_en" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Start Modal -->
                <? if (!empty(@$custodies_list)): ?>
                    <? foreach (@$custodies_list as $cl): ?>
                        <div class="modal fade" id="custodyModal<?= $cl->custody_id ?>" tabindex="-1"
                             role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style="font-size: 1.2em;">
                                    <div class="modal-body">
                                        <form action="<?= base_url() ?>employee/employee/save_custody/<?= $employee_info->employee_id ?>/<?= $cl->custody_id ?>"
                                              method="post" class="form form-horizontal" id="formi"
                                              enctype="multipart/form-data">
                                            <style>
                                                .modal-body {
                                                    padding: 40px 15px !important;
                                                }

                                                .modal-body labels {
                                                    font-weight: normal !important;
                                                }
                                            </style>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_reference') ?>
                                                    <span class="required">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="number" class="form-control" disabled="disabled"
                                                           value="<?= $cl->custody_reference; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_name_ar') ?>
                                                    <span class="required">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="name_ar" class="form-control" required=""
                                                           value="<?= $cl->name_ar; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_name_en') ?>
                                                    <span class="required">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="name_en" class="form-control" required=""
                                                           value="<?= $cl->name_en; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_delivery_date') ?>
                                                    <span
                                                            class="required">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="delivery_date"
                                                           class="form-control hijri_datepicker"
                                                           required="" value="<?= $cl->delivery_date; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_nombre') ?>
                                                    <span class="required">*</span></label>
                                                <div class="col-sm-7">
                                                    <input type="number" name="nombre" class="form-control" required=""
                                                           value="<?= $cl->nombre; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?= lang('upload_document') ?></label>
                                                <div class="col-sm-4">
                                                    <div class="fileinput fileinput-new col-sm-9"
                                                         data-provides="fileinput">
                                                        <div class="btn btn-primary btn-file btn-block"><span
                                                                    class="fileinput-new"><?= lang('select_file') ?></span>
                                                            <span class="fileinput-exists"><?= lang('change') ?></span>
                                                            <input type="file" name="document" class="form-control">
                                                        </div>
                                                        <div class="fileinput-filename"></div>
                                                        <a href="#" class="close fileinput-exists"
                                                           data-dismiss="fileinput"
                                                           style="float: none; color:red;">&times;</a>
                                                    </div>
                                                    <div id="msg_pdf" style="color: #e11221"></div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?php if (!empty($cl->document)): ?>
                                                        <a target="_blank"
                                                           href="<?= base_url() ?>img/uploads/<?= $cl->document; ?>"><b><?= lang('download') ?></b></a>
                                                    <?php else: ?>
                                                        <?= lang('no-exist') ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_description_ar') ?></label>
                                                <div class="col-sm-7">
                                                    <textarea name="description_ar" rows="5"
                                                              class="form-control"><?= $cl->description_ar; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="field-1"
                                                       class="col-sm-3 control-label"><?= lang('custody_description_en') ?></label>
                                                <div class="col-sm-7">
                                                    <textarea name="description_en" rows="5"
                                                              class="form-control"><?= $cl->description_en; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group margin">
                                                <div class="col-sm-offset-3 col-sm-7">
                                                    <button type="submit" id="sbtn"
                                                            class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
                <!-- End Modal -->

            </div>
        </div>

    </div>
</div>


<? if (!empty(@$custodies_list)): ?>
    <? foreach (@$custodies_list as $cl): ?>
        <div class="print" id="custody_<?= $cl->custody_id ?>">
            <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
            <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
            <?php if ($lang == "arabic"): ?>
                <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css" rel="stylesheet"/>
            <?php endif; ?>
            <style type="text/css">
                .print {
                    border: 1px solid gray;
                    min-height: 200px;
                    margin-bottom: 100px;
                    padding: 100px;
                }

                table {
                    width: 100%
                }
            </style>
            <table>
                <tr>
                    <td width="10%"></td>
                    <td>
                        <h3><?= lang("custody_details"); ?></h3>
                    </td>
                    <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br><br><br>
            <table>
                <tr>
                    <td width="10%"></td>
                    <td>
                        <table class="table table-bordered" style="">
                            <tr>
                                <td><?= lang("employee_name") ?></td>
                                <td><?= ($lang == "arabic") ? $employee_info->full_name_ar : $employee_info->full_name_en; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_reference") ?></td>
                                <td><?= $cl->custody_reference; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_delivery_date") ?></td>
                                <td><?= $cl->delivery_date; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("custody_nombre") ?></td>
                                <td><?= $cl->nombre; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("name") ?></td>
                                <td><?= ($lang == 'english') ? $cl->name_en : $cl->name_ar; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("description") ?></td>
                                <td><?= ($lang == 'english') ? $cl->description_en : $cl->description_ar; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("receipt_confirmation") ?></td>
                                <td>
                                    <?php if ($cl->received == 1): ?>
                                        <?= lang("yes"); ?>
                                    <?php else: ?>
                                        <?= lang("no"); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br><br>
            <table>
                <tr>
                    <td width="65%"></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-center"><?= lang("employee_signature") ?></td>
                            </tr>
                            <tr>
                                <td style="height: 70px"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td width="65%"></td>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-center"><?= lang("manager_signature") ?></td>
                            </tr>
                            <tr>
                                <td style="height: 140px"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="10%"></td>
                </tr>
            </table>
        </div>
    <? endforeach; ?>
<? endif ?>

<script type="text/javascript">

    function print_custody(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>


<script type="text/javascript">
    $(function () {
        var calendar = $.calendars.instance('ummalqura');
        $('.hijri_datepicker').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
        $('form').map(function () {
            $(this).validate({rules: {name: "required"}});
        });
        $('#finances_info').find('form').map(function () {
            $(this).find('input, select').prop('disabled', 'true');
        });
        <?php if ($employee_info->job_time == 'Full'): ?>
            $('.final-salary').html(<?= (!empty($total)) ? $total + $employee_info->employee_salary : $employee_info->employee_salary; ?>);
            $('#finances_info').find('form').map(function () {
                $(this).find('input, select').prop('disabled', false);
            });
        <?php endif; ?>
    });
    function allowancevalue(s) {
        var type = s.find(":selected").attr('data-type');
        if (type == "value") {
            $('#allowance_value').prop('readonly ', 'true');
            $('#allowance_value').val(s.find(":selected").attr('data-value'));
            $('#allowance_type').val(s.find(":selected").attr('data-type'));
        } else if (type == "percent") {
            $('#allowance_value').prop('readonly ', 'true');
            $('#allowance_value').val(s.find(":selected").attr('data-value'));
            $('#allowance_type').val(s.find(":selected").attr('data-type'));
        } else if (type == "new") {
            $('#allowance_value').val(0);
            $('#allowance_value').prop('readonly', false);
            $('#allowance_type').val('value');
        }
    }
    function generate() {
        var val = makeid();
        $('#custody_reference').val(val);
    }
    function makeid() {
        var text = "";
        var possible = "0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
</script>