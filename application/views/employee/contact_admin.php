<div class="col-md-12">
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-12">

                <!-- Ideal Employee -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('box_sugg_compl') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-non-bordered" style="font-size: 1.1em">
                            <tr>
                                <td width="20%"><b><?= lang('direct_manager') ?> :</b></td>
                                <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar'] ?></td>
                            </tr>
                            <tr>
                                <td><b><?= lang('hr_manager') ?> :</b></td>
                                <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar'] ?></td>
                            </tr>
                        </table>
                        <hr>

                        <form class="form-horizontal form1" role="form" id="form"
                              action="<?= base_url() ?>employee/dashboard/contact_admin/send" method="post">
                            <br>

                            <!-- send to -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('direction_msg') ?> <span
                                            class="required"> *</span></label>
                                <div class="col-sm-7">
                                    <select name="send_to" value="" class="form-control" required>
                                        <?php if (!empty(@$reply_employee)): ?>
                                            <option value="<?=@$reply_employee->employee_id?>"><?= ($lang == 'arabic') ? @$reply_employee->full_name_ar : @$reply_employee->full_name_en; ?></option>
                                        <?php else: ?>

                                        <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager' or $this->session->userdata('emp_type') == 'dep_manager' or $this->session->userdata('emp_type') == 'sec_manager'): ?>
                                            <optgroup label="<?= lang('all') ?>">
                                                <option value="0"><?= lang('all') ?></option>
                                            </optgroup>
                                        <?php endif; ?>


                                        <optgroup label="<?= lang('administration') ?>">
                                            <option
                                                    value="<?= $managers['direct_manager_id'] ?>"><?= lang('direct_manager') ?></option>
                                            <option
                                                    value="<?= $managers['human_resource_id'] ?>"><?= lang('hr_manager') ?></option>
                                            <?php if ($this->session->userdata('emp_type') != 'employee'): ?>
                                                <optgroup label="<?= lang('employees') ?>">
                                                    <?php foreach (@$employee_list as $emp): ?>
                                                        <option value="<?= $emp->employee_id ?>">
                                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </optgroup>
                                            <?php endif; ?>

                                            <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- type -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('notif_type') ?> <span class="required"> *</span></label>
                                <div class="col-sm-7">
                                    <select name="sugg_or_compl" class="form-control" required>
                                        <?php if (!empty(@$reply_employee)): ?>
                                            <option value="1"><?= lang('sugg') ?></option>
                                        <?php else: ?>
                                            <option></option>
                                            <option value="1"><?= lang('sugg') ?></option>
                                            <option value="2"><?= lang('compl') ?></option>
                                            <option value="3"><?= lang('anotice') ?></option>
                                            <option value="4"><?= lang('appro_consultation') ?></option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- type -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('msg_title') ?> <span
                                            class="required"> *</span></label>
                                <div class="col-sm-7">
                                    <?php if (!empty(@$reply_employee)): ?>
                                        <input type="text" name="title" value="<?=($lang=='arabic')?'رد على إستشارة':'Reply on a consultation';?>" class="form-control" required>
                                    <?php else: ?>
                                        <input type="text" name="title" value="" class="form-control" required>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <!-- text -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?= lang('msg_body') ?> <span
                                            class="required"> *</span></label>
                                <div class="col-sm-7">
                                    <textarea id="ck_editor" name="long_description" value="" class="form-control"
                                              rows="6" required>
                                    </textarea>
                                    <?php echo display_ckeditor($editor['ckeditor']); ?>
                                </div>
                            </div>

                            <!-- -->
                            <div class="form-group margin">
                                <div class="col-sm-offset-3 col-sm-5">
                                    <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('send') ?></button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

