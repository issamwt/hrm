<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang('results') ?></strong></div>
                <div class="panel-body">
                    <div>
                        <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang('print') ?></a>
                        <a href="<?= base_url() ?>excel/<?= $name ?>.xlsx" target="_blank"
                           class="btn btn-success btn-xs"><i
                                class="fa fa-file-excel-o"></i> Excel</a>
                    </div>
                    <div class="print" id="printed_content">
                        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                        <?php if ($lang == 'arabic'): ?>
                            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                  rel="stylesheet"/>

                        <?php endif; ?>
                        <style type="text/css">
                            .btn.btn-xs {
                                float: left;
                                margin-bottom: 20px;
                                margin-right: 5px;
                            }

                            .print {
                                border: 1px solid gray;
                                min-height: 200px;
                                margin-bottom: 100px;
                                padding: 100px;
                            }

                            .print .boxino {
                                border: 1px solid black;
                                border-radius: 6px;
                                float: right;
                                min-width: 300px;
                                /*height: 100px;*/
                                padding: 10px 15px;
                            <?php if($lang == 'english'):?> float: left;
                            <?php else: ?> float: right;
                            <?php endif; ?>
                            }

                            table {
                                width: 100%;
                            }
                        </style>
                        <table>
                            <tr>
                                <td>
                                    <div class="boxino">
                                        <?php if ($lang == "english"): ?>
                                            <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                        <?php else: ?>
                                            <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                        <?php endif; ?>
                                        <b><?= lang("the_report") ?> : </b> <?= lang("attendances") ?><br>
                                        <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                        <b><?= lang("year") ?>
                                            : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                        <br>
                                    </div>
                                </td>
                                <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                            </tr>
                        </table>


                        <div style="clear:both;">
                            <div style="height: 60px;"></div>
                            <h2 class="text-center"><?= lang('attendances') ?></h2><br>
                        </div>
                        <div class="content">
                            <table class="table table-bordered" style="direction: ltr">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('employee_id2') ?></th>
                                    <th class="text-center"><?= lang('name_en') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('action') ?></th>
                                    <th class="text-center"><?= lang('employee_id') ?></th>
                                    <th class="text-center"><?= lang('datetime') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($attendances as $att): ?>
                                        <?php if (in_array($att->employee_att_id, $ids)): ?>
                                            <?php if (strpos(implode('-', explode('/', $att->att_date)), $filter) !== false) : ?>
                                                <tr>
                                                    <td class="text-center"><?= $att->employee_att_id ?></td>
                                                    <? foreach ($all as $emp): ?>
                                                        <? if ($emp->employee_id == $att->employee_att_id): ?>
                                                            <td><?=$emp->full_name_en?></td>
                                                        <? endif; ?>
                                                    <? endforeach; ?>
                                                    <td class="text-center"><?= $att->employee_status ?></td>
                                                    <td class="text-center"><?= ($att->Action == 1) ? '<div class="btn btn-success btn-block btn-xs">' . lang('clock_in') . '</div>' : '<span class="btn btn-danger  btn-block btn-xs">' . lang('clock_out') . '</span>'; ?></td>
                                                    <td class="text-center">
                                                        <?php foreach (@$employee_list as $emp):?>
                                                            <?php if($emp->employee_id==$att->employee_att_id): ?>
                                                                <?=$emp->employment_id?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </td>
                                                    <td><?= $att->att_date ?> <?= $att->att_time ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


