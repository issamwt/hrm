<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang('results') ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php @$today = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                        <?php foreach (@$results as $result): ?>
                            <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                    class="fa fa-print"></i> <?= lang('print') ?></a>

                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == 'arabic'): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-primary.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 350px;
                                        height: 100px;
                                        padding: 10px;
                                        float: right;
                                    }

                                    table {
                                        width: 100%;
                                    }

                                    table td, table th {
                                        padding: 2px;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $result->gender == "Female"): ?>
                                                    <img src="<?=base_url()?>img/adminf.png" style="height: 77px;float: right;border: 2px solid gray;border-radius:3px;margin:0px 10px;" alt="Please Connect Your Internet"/>
                                                <?php else:?>
                                                    <img src="<?= (!empty(@$result->photo)) ? base_url() . @$result->photo : base_url() . 'img/admin.png'; ?>" style="height: 77px; width:77px !important; float: right;border: 2px solid gray;border-radius:3px;margin:0px 10px;" alt="Please Connect Your Internet">
                                                <?php endif;?>
                                                <b><?= lang('name') ?>
                                                    : </b><?= ($lang == 'english') ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang('designation') ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == 'english') ? $result->designations : $result->designations_ar) : lang('no-exist'); ?>
                                                <br>
                                                <b><?= lang('department') ?>
                                                    : </b><?= ($lang == 'english') ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang('direct_manager') ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == 'english') ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 10px;"></div>
                                    <h2 style="margin:0px;" class="text-center"><?= lang('personal_details') ?></h2><br><br><br>
                                </div>
                                <div class="content">
                                    <table>
                                        <tr>
                                            <td width="60%">
                                                <b><?= lang('full_name_ar') ?> : </b><?= $result->full_name_ar ?><br>
                                                <b><?= lang('full_name_en') ?> : </b><?= $result->full_name_en ?><br>
                                                <b><?= lang('date_of_birth') ?> : </b><?= $result->date_of_birth ?><br>
                                                <b><?= lang('gender') ?>
                                                    : </b><?= ($result->gender == "Male") ? lang('male') : lang('female'); ?>
                                                <br>
                                                <b><?= lang('phone') ?> : </b><?= $result->phone ?><br>
                                                <b><?= lang('email') ?> : </b><?= $result->email ?><br>
                                                <b><?= lang('nationality') ?>
                                                    : </b><?= ($lang == 'english') ? $result->countryName : $result->countryName_ar; ?>
                                                <br>
                                                <b><?= lang('present_address') ?> : </b><?= $result->present_address ?><br>
                                                <b><?= lang('education') ?> : </b><?= $result->education ?><br>
                                                <b><?= lang('passport_no') ?> : </b><?= $result->passport_number ?><br>
                                                <b><?= lang('passport_end') ?> : </b><?= $result->passport_end ?><br>
                                                <b><?= lang('identity_no') ?> : </b><?= $result->identity_no ?><br>
                                                <b><?= lang('identity_end') ?> : </b><?= $result->identity_end ?><br>
                                                <b><?= lang('passport_end') ?> : </b><?= $result->passport_end ?><br>
                                                <b><?= lang('maratial_status') ?>
                                                    : </b><?= lang(strtolower($result->maratial_status)) ?><br>
                                                <b><?= lang('medical_insur') ?>
                                                    : </b><?php if ($result->med_insur == 1) echo lang('yes'); else echo lang('no'); ?>
                                                <br>
                                                <b><?= lang('medical_insur_type') ?>
                                                    : </b><?php if ($result->med_insur_type == 1) echo lang('medical_insur_type_all'); elseif ($result->med_insur_type == 2) echo lang('medical_insur_type_part');
                                                else lang('no_exist') ?><br>
                                                <b><?= lang('social_insur') ?>
                                                    : </b><?php if ($result->social_insurance == 1) echo lang('yes'); else echo lang('no'); ?>
                                                <br>
                                                <b><?= lang('social_insurance_type') ?>
                                                    : </b><?= lang($result->social_insurance_type) ?><br>
                                                <b><?= lang('employee_type') ?>
                                                    : </b><?= ($lang == 'english') ? $result->name_en : $result->name_ar; ?><br>
                                                <b><?= lang('joining_date') ?> : </b><?= $result->joining_date ?><br>
                                                <b><?= lang('retirement_date') ?> : </b><?= $result->retirement_date ?><br>
                                                <b><?= lang('job_time') ?>
                                                    : </b><?= ($result->job_time == "Full") ? lang('job_full') : lang('job_part'); ?>
                                                <br>
                                                <b><?= lang('job_place') ?>
                                                    : </b><?= ($lang == 'english') ? $result->place_name_en : $result->place_name_ar; ?>
                                                <br>
                                                <b><?= lang('job_title') ?>
                                                    : </b><?= ($lang == 'english') ? $result->job_titles_name_en : $result->job_titles_name_ar; ?>
                                                <br>
                                                <b><?= lang('holiday_no') ?>
                                                    : </b><?= $result->holiday_no ?> <?= lang('days') ?>
                                                <br>
                                                <b><?= lang('employee_id') ?> : </b><?= $result->employment_id ?><br>
                                            </td>
                                            <td width="40%" style="vertical-align: text-top">
                                                <b><?= lang('wife_name') ?>: </b><?= $result->wife_name ?><br>
                                                <b><?= lang('wife_name_ar') ?>: </b><?= $result->wife_name_ar ?><br>
                                                <b><?= lang('wife_birth') ?> : </b><?= $result->wife_birth ?><br>
                                                <br>
                                                <?php $fils_names = explode(';', @$result->fils_name); ?>
                                                <?php $fils_names_ar = explode(';', @$result->fils_name_ar); ?>
                                                <?php $fils_births = explode(';', @$result->fils_birth); ?>
                                                <?php for ($x = 0; $x < count($fils_names_ar); $x++): ?>
                                                    <?php if (!empty($fils_names_ar[$x])): ?>
                                                        <b><?= lang('fils_name_ar')?>: </b><?= $fils_names_ar[$x]; ?><br>
                                                        <b><?= lang('fils_name')?>: </b><?= $fils_names[$x]; ?><br>
                                                        <b><?= lang('fils_birth') ?> : </b><?= $fils_births[$x] ?><br>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    function print_this(id) {
        var divToPrint=$('#' + id);
        console.log(divToPrint.html());

        var newWin=window.open('','PRINT', 'height=400,width=600');

        newWin.document.write('<html><body onload="window.print()"><style type="text/css">table { page-break-inside:auto }tr{ page-break-inside:avoid; page-break-after:auto }thead{ display:table-header-group }tfoot { display:table-footer-group }</style>'+divToPrint.html()+'</body></html>');

        newWin.document.close();

        return true;

    }
</script>
