<div class="col-md-12">

    <div class="box box-primary">
        <!-- Default panel contents -->

        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('close') ?></span></button>
            <div class="panel-title">
                <strong><?= lang('notice_details') ?></strong>
            </div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <label class="control-label"><strong><?= lang('sender') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static">
                        <?php foreach ($emps as $emp): ?>
                            <?php if ($emp->employee_id == $full_notice_details->employee_id): ?>
                                <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </p>
                </div>
            </div>
            <div class="col-md-12 notice-details-margin">
			<div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <label class="control-label"><strong><?= lang('notif_title') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static"><?php if (!empty($full_notice_details->notice_id)) echo $full_notice_details->title; ?></p>
                </div>
            </div>
            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <label class="control-label"><strong><?= lang('notif_type') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static text-justify">
                        <?php
                        $type = $full_notice_details->sugg_or_compl;
                        if ($type == 1)
                            echo lang('sugg');
                        elseif ($type == 2)
                            echo lang('compl');
                        elseif ($type == 3)
                            echo lang('anotice');
                        elseif ($type == 4)
                            echo lang('appro_consultation');
                        elseif ($type == 5)
                            echo lang('accounting');
                        else
                            echo lang('anotice');
                        ?>
                    </p>
                </div>
            </div>
            <div class = "col-md-12 notice-details-margin">
				<div class="col-sm-2"></div>
                <div class="col-sm-2" style = "margin-top: 8px;">
                    <label class = "control-label"><strong><?= lang('msg_body') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static text-justify"><?php if (!empty($full_notice_details->notice_id)) echo $full_notice_details->long_description; ?></p>
                </div>
            </div>
            <div class="col-md-12 notice-details-margin">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <label class="control-label"><strong><?= lang('date') ?>:</strong></label>
                </div>
                <div class="col-sm-8">
                    <p class="form-control-static"><span class="text-danger"><?= $full_notice_details->created_date ?></span></p>
                </div>
            </div>

            <?php if($full_notice_details->sugg_or_compl==4): ?>
                <div class="col-md-12 notice-details-margin">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8">
                        <form action="<?=base_url()?>employee/dashboard/contact_admin/" method="post">
                            <input type="hidden" name="employee_id" value="<?=$full_notice_details->employee_id?>">
                            <button class="btn btn-primary" style="padding: 3px 37px; border-radius: 2px !important; font-weight: bold;">
                                <?=($lang=='arabic')?'الرد':'Reply'; ?>
                            </button>
                        </form>

                    </div>
                </div>
            <?php endif;?>

        </div>
    </div>
</div>






