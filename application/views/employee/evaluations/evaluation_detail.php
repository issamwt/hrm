<table class="table table-bordered" style="background: #fff;">
                            <tr>
                                <td><strong><?= lang('employee_name') ?> : </strong></td>
                                <td>
                                    <?php foreach (@$employee_list as $emp): ?>
                                        <?php if ($emp->employee_id == $ev->emp_id): ?>
                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong><?= lang('department') ?> : </strong></td>
                                <td>
                                    <?php foreach (@$department_list as $dep): ?>
                                        <?php if ($dep->department_id == $ev->departmnt_id): ?>
                                            <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                            <tr>
                                <td><strong><?= lang('date') ?> : </strong></td>
                                <td><?= $ev->created_date ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong><?= lang('evaluation_items') ?> : </strong></td>
                                <td>
                                    <table class="table table-bordered">
                                        <?php $ids = explode(';', $ev->ids); ?>
                                        <?php $valuess = explode(';', $ev->valuess); ?>
                                        <?php for ($i = 0; $i < count($ids); $i++): ?>
                                            <tr>
                                                <td>
                                                    <?php foreach ($evaluation_items as $ev_item): ?>
                                                        <?php if ($ev_item->evaluation_items_id == $ids[$i]): ?>
                                                            <?= ($lang == 'english') ? $ev_item->name_en : $ev_item->name_ar; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= $valuess[$i] ?> %
                                                </td>
                                            </tr>
                                        <?php endfor; ?>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background: #89c9ff">
                                <td><strong><?= lang('total') ?> : </strong></td>
                                <td class="text-center">
                                    <div><?= $ev->total ?> %</div>
                                    <hr style="width:50%; margin:0px auto; border-color: black;">
                                    <div><?= count($ids) * 100 ?> %</div>
                                </td>
                            </tr>
                            <tr style="background: #FCC612">
                                <td><strong><?= lang('final_result') ?> : </strong></td>
                                <td class="text-center">
                                    <div><?= $ev->total / count($ids) ?> %</div>
                                </td>
                            </tr>
                        </table>