<?php echo message_box('success'); ?>

<style type="text/css">
    .comment {
        background: #eee;
        color: #7d7c7c;
        font-size: 0.8em;
        padding: 10px;
        width: 100%;
        border: 1px solid #d2d2d2;
        margin: 0px auto;
    }

    .comment td {
        width: 14.2%;
        text-align: center;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('put_an_evaluation') ?> </strong>
                </div>
                <div class="panel-body"><br>

                    <form action="<?= base_url() ?>employee/evaluations/save_evaluation" method="post"
                          class="form form-horizontal" id="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('employee_name') ?></label>
                            <div class="col-md-7">
                                <input type="text" class="form-control"
                                       value="<?= ($lang == 'english') ? $employee_list->full_name_en : $employee_list->full_name_ar; ?>"
                                       disabled=""/>
                                <input type="hidden" name="emp_id" value="<?= @$employee_list->employee_id ?>"/>
                                <input type="hidden" name="departmnt_id"
                                       value="<?= @$employee_list->departement_id ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label class="col-sm-1"></label>-->
                            <div class="col-md-12">
                                <table class="table table-bordered evaluation-table">
                                    <thead>
                                    <tr>
                                        <th style="vertical-align: inherit;" rowspan="2" width="24%"
                                            class="text-center"><?= lang('description_ev_item') ?></th>
                                        <th style="vertical-align: inherit;" rowspan="2" width="18%"
                                            class="text-center"><?= lang('name_ev_item') ?></th>
                                        <th style="vertical-align: inherit;" rowspan="2" width="10%"
                                            class="text-center"><?= lang('ev_type') ?></th>
                                        <th width="50%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                    <tr>
                                        <th style="padding: 0px;">
                                            <table class="comment" border="1" cellspacing="1">
                                                <tr>
                                                    <td>ضعيف جدا</td>
                                                    <td>ضعيف</td>
                                                    <td>متوسظ</td>
                                                    <td>جيد</td>
                                                    <td>جيد جدا</td>
                                                    <td>ممتاز</td>
                                                </tr>
                                                <tr>
                                                    <td>0%</td>
                                                    <td>20%</td>
                                                    <td>40%</td>
                                                    <td>60%</td>
                                                    <td>80%</td>
                                                    <td>100%</td>
                                                </tr>
                                            </table>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? if (!empty(@$evaluation_items)):$x = 0; ?>
                                        <? foreach (@$evaluation_items as $item): ?>
                                            <? if (
                                                ($item->department_id == 0 and $item->job_titles_id == 0/*alldep_alljt*/)
                                                or
                                                ($item->department_id == @$employee_list->departement_id and $item->job_titles_id == 0/*mydep_alljt*/)
                                                or
                                                ($item->department_id == 0 and $item->job_titles_id == @$employee_list->job_title/*alldep_myjt*/)
                                            ): ?>
                                                <tr>
                                                    <td><?= ($lang == 'english') ? $item->ei_desc_en : $item->ei_desc_ar; ?></td>
                                                    <td style="vertical-align: inherit;">
                                                        <strong><?= ($lang == 'english') ? $item->name_en : $item->name_ar; ?></strong>
                                                    </td>
                                                    <td class="text-center" style="vertical-align: inherit;">
                                                        <strong><?= ($item->type == 1) ? lang('percentage') : lang('rating'); ?></strong>
                                                    </td>
                                                    <td style="vertical-align: inherit; padding: 0px;">
                                                        <?php if ($item->type == 1): ?>
                                                            <input type="number" name="valuess[]<?= $x ?>" min="0"
                                                                   max="100" value="0" class="form-control">
                                                        <?php else: ?>
                                                            <table class="comment" border="1" cellspacing="1"
                                                                   style="height: 100%;">
                                                                <tr>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="0"
                                                                                                    checked=""/></td>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="20"/></td>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="40"/></td>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="60"/></td>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="80"/></td>
                                                                    <td style="height: 100%"><input type="radio"
                                                                                                    name="valuess[]<?= $x ?>"
                                                                                                    value="100"/></td>
                                                                </tr>
                                                            </table>
                                                        <?php endif; ?>
                                                        <input type="hidden" name="ids[]"
                                                               value="<?= $item->evaluation_items_id ?>"/>
                                                    </td>
                                                </tr>
                                                <?php $x++; ?>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    </tbody>
                                    <tr>
                                        <td colspan="3" style="vertical-align: inherit; text-align: center">
                                            <strong><?= lang('total') ?></strong>
                                        </td>
                                        <td class="text-center" style="vertical-align: inherit; background: #FCC612">
                                            <strong>
                                                <input type="hidden" name="total" class="total"/>
                                                <span class="show-total">0</span> %
                                                <hr style="width: 20%; display: block; margin: 4px auto; border-color: black">
                                                <div><?= ($x * 100) . ' %' ?></div>
                                            </strong>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        <div class="form-group margin">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" id="sbtn"
                                        class="btn btn-primary btn-block"><?= lang('save') ?></button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.evaluation-table').find('input').map(function () {
            $(this).on('change', function () {
                get_value($(this));
            });
        });
    });

    function get_value(s) {
        var val = 0;
        $('.evaluation-table').find('input').map(function () {
            if ($(this).attr('type') == 'radio' || $(this).attr('type') == 'number') {
                if ($(this).attr('type') == 'radio' && $(this).is(':checked')) {
                    val += parseInt($(this).val());
                } else if ($(this).attr('type') == 'number') {
                    val += parseInt($(this).val());
                }
            }
        });
        //console.log(val);
        var str = val;
        $('.show-total').html(str);
        $('.total').val(str);
    }
</script>