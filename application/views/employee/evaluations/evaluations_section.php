<?php echo message_box('success'); ?>

<style type="text/css">
    .nav-tabs-custom {
        box-shadow: none !important;
    }

    .tab-content {
        border: 1px solid #f4f4f4;
        border-top: none;
    }

    .spinner_sects {
        display: none;
    }

    .spinner_sects .fa {
        font-size: 10em;
        margin: 10px auto;
        display: block;
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <br><br><br><br>

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#all_evaluations" data-toggle="tab"><?= lang('all_evaluations') ?></a></li>
                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'dep_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                    <li><a href="#put_an_evaluation" data-toggle="tab"><?= lang('put_an_evaluation') ?></a></li>
                <?php endif; ?>
            </ul>
            <div class="tab-content no-padding">

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- all_evaluations -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane active" id="all_evaluations" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">


                            <table class="table table-bordered" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th class="text-center"><?= lang('employee') ?></th>
                                    <th class="text-center"><?= lang('department') ?></th>
                                    <th class="text-center"><?= lang('date') ?></th>
                                    <th class="text-center"><?= lang('total') ?></th>
                                    <th width="15%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$evaluations)): ?>
                                    <?php foreach (@$evaluations as $ev): ?>
                                        <tr data-id="<?= $ev->evaluation_id ?>">
                                            <td>
                                                <?php foreach (@$employee_list as $emp): ?>
                                                    <?php if ($emp->employee_id == $ev->emp_id): ?>
                                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td>
                                                <?php foreach (@$department_list as $dep): ?>
                                                    <?php if ($dep->department_id == $ev->departmnt_id): ?>
                                                        <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td><?= $ev->created_date ?></td>
                                            <td class="text-center">
                                                <?php $ids = explode(';', $ev->ids); ?>
                                                <?= number_format((float)($ev->total / count($ids)), 2, '.', ''); ?> %
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-success btn-xs" data-toggle="modal"
                                                   data-target="#myModal<?= $ev->evaluation_id ?>"><i
                                                            class="fa fa-eye"></i> <?= lang('details') ?></a>
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                                    <a href="<?= base_url() ?>employee/evaluations/delete_evaluation/<?= $ev->evaluation_id ?>"
                                                       class="btn btn-danger btn-xs"
                                                       onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <td colspan="5" class="text-center">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <!-- put_an_evaluation -->
                <!-- ///////////////////////////////////////////////////////////////////////////////// -->
                <div class="tab-pane" id="put_an_evaluation" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <form action="<?= base_url() ?>employee/evaluations/put_an_evaluation" method="post"
                                  class="form form-horizontal" id="form"><br>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-sm-3 control-label"><?= lang('employees') ?> <span
                                                    class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <select name="emp_id" class="form-control" required="">
                                                <option></option>
                                                <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager' or $this->session->userdata('emp_type') == 'dep_manager'): ?>
                                                    <? foreach (@$employee_list as $emp): ?>
                                                        <option value="<?= $emp->employee_id ?>">
                                                            <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                        </option>
                                                    <? endforeach; ?>
                                                <? endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn"
                                                class="btn btn-success btn-block"><?= lang('continue') ?></button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<?php if (!empty(@$evaluations)): ?>
    <?php foreach (@$evaluations as $ev): ?>
        <div class="modal fade" id="myModal<?= $ev->evaluation_id ?>" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="font-size: 1.2em;">
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <tr>
                                <td><strong><?= lang('employee_name') ?> : </strong></td>
                                <td>
                                    <?php foreach (@$employee_list as $emp): ?>
                                        <?php if ($emp->employee_id == $ev->emp_id): ?>
                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <tr>
                                <td><strong><?= lang('department') ?> : </strong></td>
                                <td>
                                    <?php foreach (@$department_list as $dep): ?>
                                        <?php if ($dep->department_id == $ev->departmnt_id): ?>
                                            <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                            <tr>
                                <td><strong><?= lang('date') ?> : </strong></td>
                                <td><?= $ev->created_date ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong><?= lang('evaluation_items') ?> : </strong></td>
                                <td>
                                    <table class="table table-bordered">
                                        <?php $ids = explode(';', $ev->ids); ?>
                                        <?php $valuess = explode(';', $ev->valuess); ?>
                                        <?php for ($i = 0; $i < count($ids); $i++): ?>
                                            <tr>
                                                <td>
                                                    <?php foreach ($evaluation_items as $ev_item): ?>
                                                        <?php if ($ev_item->evaluation_items_id == $ids[$i]): ?>
                                                            <?= ($lang == 'english') ? $ev_item->name_en : $ev_item->name_ar; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?= $valuess[$i] ?> %
                                                </td>
                                            </tr>
                                        <?php endfor; ?>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background: #89c9ff">
                                <td><strong><?= lang('total') ?> : </strong></td>
                                <td class="text-center">
                                    <div><?= $ev->total ?> %</div>
                                    <hr style="width:50%; margin:0px auto; border-color: black;">
                                    <div><?= count($ids) * 100 ?> %</div>
                                </td>
                            </tr>
                            <tr style="background: #FCC612">
                                <td><strong><?= lang('final_result') ?> : </strong></td>
                                <td class="text-center">
                                    <div><?= number_format((float)($ev->total / count($ids)), 2, '.', ''); ?> %</div>

                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<!-- Modal -->

