<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .action-wrapper {
        width: 100%;
        height: 100%;
        background-color: #F7CF07;
        position: absolute;
    }

    .fa-lock {
        color: rgb(255, 255, 255);
        margin: 0px auto;
        font-size: 2em;
        display: block;
        text-align: center;
        line-height: 50px;
    }

    .fa-lock:hover {
        color: coral;
        cursor: pointer;
    }
</style>
<div class="main_content">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('vacation_balances') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('employee_id') ?></th>
                            <th class="text-center"><?= lang('name') ?></th>
                            <th class="text-center"><?= lang('holiday_no') ?></th>
                            <th class="text-center"><?= lang('old_rest') ?></th>
                            <th class="text-center"><?= lang('old_balance') ?></th>
                            <th class="text-center"><?= lang('new_balance') ?></th>
                            <th class="text-center"><?= lang('the_rest') ?></th>
                            <th class="text-center"><?= lang('end_contractual_year') ?></th>
                            <? if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                <th class="text-center"><?= lang('action') ?></th>
                            <? endif; ?>
                        </tr>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($employee_list as $emp): ?>
                            <tr>
                                <td><?= $emp->employment_id ?></td>
                                <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                <td class="text-center"><?= $emp->holiday_no ?></td>
                                <td class="text-center"><?= $emp->old_rest ?></td>
                                <td class="text-center"><?= $emp->old_balance ?></td>
                                <td class="text-center"><?= $emp->new_balance ?></td>
                                <td class="text-center">
                                    <?php $rest = 0; ?>
                                    <?= $rest = ($emp->holiday_no + $emp->old_rest) - $emp->new_balance ?>
                                </td>
                                <?php $arr = explode('-', $emp->joining_date); ?>
                                <?php if ($lang == "english"): ?>
                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                <?php else: ?>
                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                <?php endif; ?>
                                <td class="text-center"><?= $arr[2] . ' ' . $months[$arr[1] - 1] ?></td>
                                <? if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-xs btn-block" data-toggle="modal"
                                           data-target="#myModal<?= $emp->employee_id ?>"><i
                                                    class="fa fa-edit"></i> <?= lang('manual_adjustment') ?></a>
                                    </td>
                                <? endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php foreach ($employee_list as $emp): ?>
    <!-- Modal -->
    <div id="myModal<?= $emp->employee_id ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h3 class="text-center"><?= lang('manual_adjustment') ?></h3>
                    <br>
                    <style type="text/css">
                        .the-table td {
                            border: 0px !important;
                        }

                        .the-table td:first-child {
                            font-weight: bold;
                        }

                        .form {
                            display: block;
                            width: 70%;
                            margin: 0 auto;
                        }

                        .form input {
                            border-radius: 0px !important;
                        }
                    </style>
                    <form class="form form<?= $emp->employee_id ?>"
                          action="<?= base_url() ?>employee/employee/edit_balance/<?= $emp->employee_id ?>"
                          method="post">

                        <table class="table table-nonbordered the-table">
                            <tr>
                                <td width="65%"><?= lang('employee_id') ?></td>
                                <td><?= $emp->employment_id ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('name') ?></td>
                                <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang('holiday_no') ?></td>
                                <td><input name="holiday_no" type="number" step="0.1" class="form-control holiday_no"
                                           value="<?= $emp->holiday_no; ?>"></td>
                            </tr>
                            <tr>
                                <td><?= lang('old_balance') ?></td>
                                <td><input name="old_balance" type="number" step="0.1" class="form-control old_balance"
                                           value="<?= $emp->old_balance; ?>"></td>
                            </tr>
                            <tr>
                                <td><?= lang('old_rest') ?></td>
                                <td><input name="old_rest" type="number" step="0.1" class="form-control old_rest"
                                           value="<?= $emp->old_rest; ?>"></td>
                            </tr>
                            <tr>
                                <td><?= lang('new_balance') ?></td>
                                <td><input name="new_balance" type="number" step="0.1" class="form-control new_balance"
                                           value="<?= $emp->new_balance; ?>"></td>
                            </tr>
                            <tr>
                                <td><?= lang('the_rest'); ?>
                                    <small style="display: block;font-size: 0.7em;color: #003d71;">(<?=lang('holiday_no')?> + <?=lang('old_rest')?> - <?=lang('new_balance')?>)</small>
                                </td>
                                <td>
                                    <?php $rest = 0; ?>
                                    <input disabled="" class="form-control the_rest"
                                           value="<?= $rest = ($emp->holiday_no + $emp->old_rest) - $emp->new_balance ?>">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button class="btn btn-block btn-primary"><?= lang('save') ?></button>
                                </td>
                            </tr>
                        </table>

                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
<?php endforeach; ?>
