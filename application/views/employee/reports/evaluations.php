<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">

                    <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <div class="print" id="printed_content">
                        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                        <?php if ($lang == "arabic"): ?>
                            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                  rel="stylesheet"/>
                        <?php endif; ?>
                        <style type="text/css">
                            .btn.btn-primary.btn-xs {
                                float: left;
                                margin-bottom: 20px;
                            }

                            .print {
                                border: 1px solid gray;
                                min-height: 200px;
                                margin-bottom: 100px;
                                padding: 100px;
                            }

                            table {
                                width: 100%;
                            }

                            .print .boxino {
                                border: 1px solid black;
                                border-radius: 6px;
                                float: right;
                                min-width: 300px;
                                /*height: 100px;*/
                                padding: 10px 15px;
                            <?php if($lang == "english"):?> float: left;
                            <?php else: ?> float: right;
                            <?php endif; ?>
                            }

                            table {
                                width: 100%;
                            }
                        </style>
                        <table>
                            <tr>
                                <td>
                                    <div class="boxino">
                                        <?php if ($lang == "english"): ?>
                                            <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                        <?php else: ?>
                                            <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                        <?php endif; ?>
                                        <b><?= lang("the_report") ?> : </b> <?= lang("evaluations") ?><br>
                                        <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                        <b><?= lang("year") ?>
                                            : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                        <br>
                                        <b><?= lang('department') ?> : </b>
                                        <?php if ($department == 0): ?>
                                            <?= lang("all") ?>
                                        <?php else: ?>
                                            <?php foreach ($departments as $dep): ?>
                                                <?php if ($dep->department_id == $department): ?>
                                                    <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                        <b><?= lang('designation') ?> : </b>
                                        <?php if ($designation == -1): ?>
                                            <?= lang("all") ?>
                                        <?php elseif ($designation == 0): ?>
                                            <?= lang("without_section") ?>
                                        <?php else: ?>
                                            <?php foreach ($designations as $des): ?>
                                                <?php if ($des->designations_id == $designation): ?>
                                                    <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                    </div>
                                </td>
                                <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center"><?= lang("employee_name") ?></th>
                                <th class="text-center"><?= lang("reference") ?></th>
                                <th class="text-center"><?= lang('date') ?></th>
                                <th class="text-center"><?= lang('results') ?></th>
                                <th class="text-center"><?= lang('total') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <? $i = 0; ?>
                            <?php foreach ($results as $result): ?>
                                <?php foreach ($evaluations as $ev): ?>
                                    <?php if ($ev->emp_id == $result->employee_id):$i++; ?>
                                        <?php if (strpos($ev->created_date, $filter) !== false) : ?>
                                            <tr>
                                                <td><?= ($lang == "english") ? $result->full_name_en : $result->full_name_ar; ?></td>
                                                <td><?= $result->employment_id ?></td>
                                                <td class="text-center" style="vertical-align: middle">
                                                    <strong><b><?= $ev->created_date ?></b></strong></td>
                                                <td>
                                                    <table>
                                                        <?php $ids = explode(';', $ev->ids); ?>
                                                        <?php $valuess = explode(';', $ev->valuess); ?>
                                                        <?php for ($i = 0; $i < count($ids); $i++): ?>
                                                            <tr>
                                                                <td>
                                                                    <?php foreach ($evaluation_items as $ev_item): ?>
                                                                        <?php if ($ev_item->evaluation_items_id == $ids[$i]): ?>
                                                                            <?= ($lang == 'english') ? $ev_item->name_en : $ev_item->name_ar; ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                </td>
                                                                <td class="text-center">
                                                                    <?= $valuess[$i] ?> %
                                                                </td>
                                                            </tr>
                                                        <?php endfor; ?>
                                                    </table>
                                                </td>
                                                <td class="text-center" style="vertical-align: middle">
                                                    <strong><b><?= ($ev->total / count($ids)) ?> %</b></strong>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                            <? if ($i == 0): ?>
                                <tr>
                                    <td class="text-center" colspan="5"><?= lang('no-exist') ?></td>
                                </tr>
                            <? endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>



<script type="text/javascript">

    function print_this() {
        var divToPrint=$('#printed_contenet');
        console.log(divToPrint.html());

        var newWin=window.open('','PRINT', 'height=400,width=600');

        newWin.document.write('<html><body onload="window.print()"><h3><?=lang("reports_section")?></h3><hr><style type="text/css">table { page-break-inside:auto }tr{ page-break-inside:avoid; page-break-after:auto }thead{ display:table-header-group }tfoot { display:table-footer-group }</style>'+divToPrint.html()+'</body></html>');

        newWin.document.close();

        return true;

    }
</script>