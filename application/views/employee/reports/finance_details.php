<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">

                    <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <div class="print" id="printed_content">
                        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                        <?php if ($lang == "arabic"): ?>
                            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                  rel="stylesheet"/>
                        <?php endif; ?>
                        <style type="text/css">
                            .btn.btn-primary.btn-xs {
                                float: left;
                                margin-bottom: 20px;
                            }

                            .print {
                                border: 1px solid gray;
                                min-height: 200px;
                                margin-bottom: 100px;
                                padding: 100px;
                            }

                            table {
                                width: 100%;
                            }

                            .print .boxino {
                                border: 1px solid black;
                                border-radius: 6px;
                                float: right;
                                min-width: 300px;
                                padding: 10px 15px;
                            <?php if($lang == "english"):?> float: left;
                            <?php else: ?> float: right;
                            <?php endif; ?>
                            }

                            table {
                                width: 100%;
                            }
                        </style>
                        <table>
                            <tr>
                                <td>
                                    <div class="boxino">
                                        <?php if ($lang == "english"): ?>
                                            <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                        <?php else: ?>
                                            <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                        <?php endif; ?>
                                        <b><?= lang("the_report") ?> : </b> <?= lang("finance_details") ?><br>
                                        <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                        <b><?= lang("year") ?>
                                            : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                        <br>
                                        <b><?= lang('department') ?> : </b>
                                        <?php if ($department == 0): ?>
                                            <?= lang("all") ?>
                                        <?php else: ?>
                                            <?php foreach ($departments as $dep): ?>
                                                <?php if ($dep->department_id == $department): ?>
                                                    <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                        <b><?= lang('designation') ?> : </b>
                                        <?php if ($designation == -1): ?>
                                            <?= lang("all") ?>
                                        <?php elseif ($designation == 0): ?>
                                            <?= lang("without_section") ?>
                                        <?php else: ?>
                                            <?php foreach ($designations as $des): ?>
                                                <?php if ($des->designations_id == $designation): ?>
                                                    <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                    </div>
                                </td>
                                <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center"><?= lang("employee_name") ?></th>
                                <th class="text-center"><?= lang("reference") ?></th>
                                <th class="text-center"><?= lang("finance_details") ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php @$today = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                            <? foreach ($results as $em): ?>
                                <tr>
                                    <td><?= ($lang == "english") ? $em->full_name_en : $em->full_name_ar; ?></td>
                                    <td><?= $em->employment_id; ?></td>
                                    <td>
                                        <b><?= lang("basic_salary") ?>
                                            : </b> <?= $em->employee_salary ?> <?= lang("rial") ?>
                                        <br>
                                        <br>
                                        <table border="1">
                                            <thead>
                                            <tr>
                                                <th class="text-center"><?= lang("finance_bonus") ?></th>
                                                <th class="text-center"><?= lang("finance_value") ?></th>
                                                <th class="text-center"><?= lang("finance_from") ?></th>
                                                <th class="text-center"><?= lang("finance_to") ?></th>
                                                <th class="text-center"><?= lang("status") ?></th>
                                                <th class="text-center"><?= lang("finance_reservation") ?> %</th>
                                                <th class="text-center"><?= lang("total") ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if (!empty(@$allowances_list)): $total = 0; ?>
                                                <?php foreach (@$allowances_list as $al): ?>
                                                    <?php if ($al->employe_id == $em->employee_id): ?>
                                                        <?php if (dates($month, $year, $al->allowance_date_from, $al->allowance_date_to)): ?>
                                                            <tr>
                                                                <td>
                                                                    <?php if ($al->allowance_id == 0): ?>
                                                                        <?= lang("other_dues") ?>
                                                                    <?php else: ?>
                                                                        <?php foreach (@$allowances_cat_list as $acl): ?>
                                                                            <?php if ($acl->allowance_id == $al->allowance_id): ?>
                                                                                <?= ($lang == "english") ? $acl->allowance_title_en : $acl->allowance_title_ar; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($al->allowance_type == "value"): ?>
                                                                        <span
                                                                                class="pull-left"><?= $al->allowance_value ?></span>
                                                                    <?php else: ?>
                                                                        <span class="pull-left"><?= $al->allowance_value ?>
                                                                            (%)</span>
                                                                        <span class="pull-right"
                                                                              style="border:1px solid gray; padding: 0px 10px"><?= ($em->employee_salary / 100) * ($al->allowance_value) ?></span>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td><?= $al->allowance_date_from ?></td>
                                                                <td><?= $al->allowance_date_to ?></td>
                                                                <td>
                                                                    <?php $from = date_create_from_format("Y-m-d", $al->allowance_date_from)->format("Ymd") ?>
                                                                    <?php $to = ($al->allowance_date_to != '') ? date_create_from_format("Y-m-d", $al->allowance_date_to)->format("Ymd") : date_create_from_format("Y-m-d", "9999-12-30")->format("Ymd"); ?>

                                                                    <?php if ($from <= $today and $today <= $to): ?>
                                                                        <span
                                                                                class="label label-success"><?= lang("active") ?></span>
                                                                    <?php else: ?>
                                                                        <span
                                                                                class="label label-warning"><?= lang("inactive") ?></span>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($from <= $today and $today <= $to): ?>
                                                                        <span
                                                                                class="label label-success"><?= $al->reservation ?>
                                                                            %</span>
                                                                    <?php else: ?>
                                                                        <span
                                                                                class="label label-warning"><?= $al->reservation ?>
                                                                            %</span>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td>
                                                                    <?php if ($al->allowance_type == "value"): ?>
                                                                        <?php $val = ($al->allowance_value / 100) * (100 - $al->reservation); ?>
                                                                        <?php if ($from <= $today and $today <= $to): ?>
                                                                            <span
                                                                                    class="label label-success"><?= $val ?></span>
                                                                            <?php $total += $val; ?>
                                                                        <?php else: ?>
                                                                            <span
                                                                                    class="label label-warning"><?= $val ?></span>
                                                                        <?php endif; ?>
                                                                    <?php else: ?>
                                                                        <?php $val = (($em->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation); ?>
                                                                        <?php if ($from <= $today and $today <= $to): ?>
                                                                            <span
                                                                                    class="label label-success"><?= $val ?></span>
                                                                            <?php $total += $val; ?>
                                                                        <?php else: ?>
                                                                            <span
                                                                                    class="label label-warning"><?= $val ?></span>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <tr>
                                                    <td colspan="6"></td>
                                                    <td><?= $total ?></td>
                                                </tr>

                                            <?php else: ?>
                                                <tr>
                                                    <td class="text-center" colspan="7">
                                                        <strong><?= lang("nothing_to_display") ?></strong></td>
                                                </tr>
                                            <?php endif; ?>

                                            </tbody>
                                        </table>
                                        <h4><b><?= lang("finance_permanent_deduction") ?></b> : </h4>
                                        <div>
                                            <table border="1">
                                                <thead>
                                                <tr>
                                                    <th class="text-center"><?= lang('finance_deduction_bonus') ?></th>
                                                    <th class="text-center"><?= lang('finance_deduction_type') ?></th>
                                                    <th class="text-center"><?= lang('finance_deduction_value') ?></th>
                                                    <th class="text-center"><?= lang('start_date') ?></th>
                                                    <th class="text-center"><?= lang('end_date') ?></th>
                                                    <th class="text-center"><?= lang('prod_note') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if (!empty(@$deduction_list)): ?>
                                                    <?php foreach (@$deduction_list as $d): ?>
                                                        <?php if ($d->employd_id == $em->employee_id): ?>
                                                            <?php if (dates($month, $year, $d->start_date, $d->end_date)): ?>
                                                                <tr>
                                                                    <td>
                                                                        <?php foreach (@$deduction_category_list as $dc): ?>
                                                                            <?php if ($d->deduction_id == $dc->deduction_id): ?>
                                                                                <?= ($lang == "english") ? $dc->deduction_title_en : $dc->deduction_title_ar; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; ?>
                                                                    </td>
                                                                    <td><?= ($d->deduction_type == "percent") ? lang('deduction_type_percent') : lang('deduction_type_value'); ?></td>
                                                                    <td><?= $d->deduction_value; ?></td>
                                                                    <td><?= $d->start_date; ?></td>
                                                                    <td><?= $d->end_date; ?></td>
                                                                    <td><?= $d->note_deduction ?></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <tr>
                                                        <td class="text-center" colspan="4">
                                                            <strong><?= lang("nothing_to_display") ?></strong></td>
                                                    </tr>
                                                <?php endif; ?>
                                                </tbody>
                                            </table>
                                            <h4><b><?= lang("finance_provision") ?></b> : </h4>
                                            <div>
                                                <table border="1">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('finance_provision_bonus') ?></th>
                                                        <th class="text-center"><?= lang('finance_provision_value') ?></th>
                                                        <th class="text-center"><?= lang('start_date') ?></th>
                                                        <th class="text-center"><?= lang('end_date') ?></th>
                                                        <th width="40%"
                                                            class="text-center"><?= lang('prod_note') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty(@$provision_list)): ?>
                                                        <?php foreach (@$provision_list as $p): ?>
                                                            <?php if ($p->employp_id == $em->employee_id): ?>
                                                                <?php if (dates($month, $year, $p->start_date, $p->end_date)): ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php foreach (@$provision_cat_list as $pc): ?>
                                                                                <?php if ($pc->provision_category_id == $p->provision_category_id): ?>
                                                                                    <?= ($lang == "english") ? $pc->provision_title_en : $pc->provision_title_ar; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        </td>
                                                                        <td><?= $p->provision_value ?> <?= lang('rial') ?></td>
                                                                        <td><?= $p->start_date ?></td>
                                                                        <td><?= $p->end_date ?></td>
                                                                        <td><?= $p->note_provision ?></td>
                                                                    </tr>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else: ?>
                                                        <tr>
                                                            <td class="text-center" colspan="4">
                                                                <strong><?= lang("nothing_to_display") ?></strong></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <b><?= lang("payment_method"); ?></b> :
                                            <?php if ($em->payment_method == 1) echo(($lang == "arabic") ? "بنك" : "Bank");
                                            elseif ($em->payment_method == 1) echo(($lang == "arabic") ? "شيكات" : "Checks");
                                            else echo(($lang == "arabic") ? "نقدا" : "Cash money");
                                            ?><br>
                                            <b><?= lang("account_number"); ?> : </b><?= $em->account_number ?><br>
                                            <b><?= lang("bank_name"); ?> : </b><?= $em->bank_name ?><br>
                                            <b><?= lang("branch_name"); ?> : </b><?= $em->branch_name ?><br>
                                            <?php if ($em->med_insur == 1): ?>
                                                <b><?= lang("medical_insur_type") ?></b>
                                                :<?php if ($em->med_insur_type == 1) echo lang("medical_insur_type_all"); elseif ($em->med_insur_type == 2) echo lang("medical_insur_type_part");
                                                else echo lang("no-exist"); ?> <br>
                                                <b><?= lang("insurance_percent"); ?>
                                                : </b><?= $em->m_insurance_percent ?> %
                                                <br>
                                                <b><?= lang("societe_percent"); ?> : </b><?= $em->m_societe_percent ?> %
                                                <br>
                                                <b><?= lang("medical_date"); ?> : </b><?= $em->med_insur_start_date ?>
                                                <br>
                                                <b><?= lang("medical_end_date"); ?> : </b><?= $em->med_insur_end_date ?>
                                                <br>
                                            <?php else: ?>
                                                <b><?= lang("medical_insur_type") ?> : </b><?= lang("no-exist") ?><br>
                                            <?php endif; ?>
                                            <?php if ($em->social_insurance == 1): ?>
                                                <b><?= lang("social_type"); ?>
                                                : </b><?php if ($em->social_insurance_type != "0") echo lang($em->social_insurance_type); else echo lang("no-exist"); ?>
                                                <br>
                                                <b><?= lang("insurance_percent"); ?> : <?= $em->insurance_percent ?>
                                                    %</b><br>
                                                <b><?= lang("societe_percent"); ?> : <?= $em->societe_percent ?> %</b>
                                                <br>
                                                <b><?= lang("social_salary"); ?>
                                                    : <?= $em->social_salary ?> <? lang("rial") ?></b><br>
                                                <b><?= lang("social_date"); ?> : </b><?= $em->social_start_date ?><br>
                                            <?php else: ?>
                                                <b><?= lang("social_type"); ?> : </b><?= lang("no-exist") ?><br>
                                            <?php endif; ?>
                                            <b><?= lang("finance_status") ?> : </b>
                                            <?php if ($em->houcing_status == 1): ?>
                                                <?= lang("yes") ?><br>
                                                <b><?= lang("finance_count") ?> : </b>
                                                <?php if ($em->houcing_type == 1)
                                                    echo lang("percent");
                                                elseif ($em->houcing_type == 2)
                                                    echo lang("value");
                                                else
                                                    echo lang("no-exist");
                                                ?><br>
                                                <b><?= lang("finance_value2") ?> : </b><?= $em->houcing_value ?>
                                            <?php else: ?>
                                                <?= lang("no") ?>
                                            <?php endif; ?>
                                            <h4><b><?= lang("advances_app") ?></b> : <?php $i = 0; ?></h4>
                                            <div>
                                                <table border="1">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang("date") ?></th>
                                                        <th class="text-center"><?= lang("the_advance_type") ?></th>
                                                        <th class="text-center"><?= lang("voucher") ?></th>
                                                        <th class="text-center"><?= lang("advance_value") ?></th>
                                                        <th class="text-center"><?= lang("rest_advance") ?></th>
                                                        <th class="text-center"><?= lang("payement_method") ?></th>
                                                        <th class="text-center"><?= lang("monthly_installement") ?></th>
                                                        <th class="text-center"><?= lang("payement_months") ?></th>
                                                        <th class="text-center"><?= lang("last_advance_date") ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach (@$advances as $adv): ?>
                                                        <?php if ($adv->employea_id == $em->employee_id): $i++; ?>
                                                            <?php if (strpos($adv->advance_date, $filter) !== false) : ?>
                                                                <tr>
                                                                    <td><?= @$adv->advance_date ?></td>
                                                                    <td>
                                                                        <?php if (!empty(@$advances_list)): ?>
                                                                            <?php foreach (@$advances_list as $al): ?>
                                                                                <?php if ($adv->advance_type_id == $al->advance_id): ?>
                                                                                    <?= ($lang == "english") ? $al->title_en : $al->title_ar; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td>
                                                                        <?php if (empty(@$adv->advance_voucher)): ?>
                                                                            <?= lang("no-exist") ?>
                                                                        <?php else: ?>
                                                                            <?php foreach (@$all as $emp): ?>
                                                                                <?php if ($emp->employee_id == $adv->advance_voucher): ?>
                                                                                    <?= ($lang == "english") ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                    <td><?= $adv->advance_value ?> <?= lang("rial") ?></td>
                                                                    <td><?= $adv->rest ?> <?= lang("rial") ?></td>
                                                                    <td><?= lang("payement_method" . $adv->payement_method) ?></td>
                                                                    <td><?= $adv->monthly_installement ?>  <?= lang("rial") ?></td>
                                                                    <td><?= $adv->payement_months ?></td>
                                                                    <td><?= (!empty($adv->last_date)) ? $adv->last_date : lang("no-exist"); ?></td>

                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                    <?php if ($i == 0): ?>
                                                        <tr>
                                                            <td colspan="9"
                                                                class="text-center"><?= lang("no-exist"); ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                            <h4><b><?= lang("caching") ?></b> : </h4>
                                            <div>
                                                <table border="1">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center"><?= lang('date') ?></th>
                                                        <th class="text-center"><?= lang('cahing_type') ?></th>
                                                        <th class="text-center"><?= lang('beneficiary_name') ?></th>
                                                        <th class="text-center"><?= lang('bill_number') ?></th>
                                                        <th class="text-center"><?= lang('item_no') ?></th>
                                                        <th class="text-center"><?= lang('cahing_value') ?></th>
                                                        <th class="text-center"><?= lang('bank_name') ?></th>
                                                        <th class="text-center"><?= lang('account_holder_name') ?></th>
                                                        <th class="text-center"><?= lang('country') ?></th>
                                                        <th class="text-center"><?= lang('account_number') ?></th>
                                                        <th class="text-center"><?= lang('beneficiary_address') ?></th>
                                                        <th class="text-center"><?= lang('swift_code') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach (@$cachings as $c): ?>
                                                        <?php if ($c->employec_id == $em->employee_id): ?>
                                                            <?php if (strpos($c->caching_date, $filter) !== false) : ?>
                                                                <tr>
                                                                    <td><?= $c->caching_date ?></td>
                                                                    <td><?= lang('cahing_type' . $c->cahing_type) ?></td>
                                                                    <td><?= $c->name ?></td>
                                                                    <td><?= $c->bill_number ?></td>
                                                                    <td><?= $c->item_no ?></td>
                                                                    <td><?= $c->value ?> <?= lang('rial') ?></td>
                                                                    <td><?= $c->bank_name ?></td>
                                                                    <td><?= $c->account_holder_name ?></td>
                                                                    <td><?= $c->country ?></td>
                                                                    <td><?= $c->account_number ?></td>
                                                                    <td><?= $c->address_in_leave ?></td>
                                                                    <td><?= $c->swift_code ?></td>
                                                                </tr>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                    </td>

                                </tr>
                            <? endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php
function dates($month, $year, $start, $end)
{
    if ($end == '')
        $end = date_create_from_format('Y-m-d', "9999-12-30")->format('Y-m-d');
    $flag = false;
    if ($month == '0' and $year == '0') {
        $flag = true;
    } elseif ($month == '0' and $year != '0') {
        $year_s = date('Y', strtotime($start));
        $year_e = date('Y', strtotime($end));
        if ($year <= $year_e and $year >= $year_s)
            $flag = true;
    } else {
        $start_s = date_create_from_format('Y-m-d', $start)->format('Ym');
        $end_e = date_create_from_format('Y-m-d', $end)->format('Ym');
        $date_x = date_create_from_format('Y-m-d', $year . '-' . $month . '-01')->format('Ym');
        if ($date_x <= $end_e and $date_x >= $start_s)
            $flag = true;
    }

    return $flag;
}

?>
