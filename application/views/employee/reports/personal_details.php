<script language="javascript">
    function printdiv(printpage)
    {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr+newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">

                    <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <div class="print" id="printed_content">
                        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                        <?php if ($lang == "arabic"): ?>
                            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                  rel="stylesheet"/>
                        <?php endif; ?>
                        <style type="text/css">
                            .btn.btn-primary.btn-xs {
                                float: left;
                                margin-bottom: 20px;
                            }

                            .print {
                                border: 1px solid gray;
                                min-height: 200px;
                                margin-bottom: 100px;
                                padding: 100px;
                            }

                            table {
                                width: 100%;
                            }

                            .print .boxino {
                                border: 1px solid black;
                                border-radius: 6px;
                                float: right;
                                min-width: 300px;
                                /*height: 100px;*/
                                padding: 10px 15px;
                            <?php if($lang == "english"):?> float: left;
                            <?php else: ?> float: right;
                            <?php endif; ?>
                            }

                            table {
                                width: 100%;
                            }
                        </style>
                        <table>
                            <tr>
                                <td>
                                    <div class="boxino">
                                        <?php if ($lang == "english"): ?>
                                            <?php $months = array("All", "Muharram", "Safar", "Rabi al-awwal", "Rabi al-thani", "Jumada al-awwal", "Jumada al-thani", "Rajab", "Shaaban", "Ramadan", "Shawwal", "Dhu al-Qidah", "Dhu al-Hijjah"); ?>
                                        <?php else: ?>
                                            <?php $months = array("الكل", "محرّم", "صفر", "ربيع الأول", "ربيع الآخر أو ربيع الثاني", "جمادى الاول", "جمادى الآخر أو جمادى الثاني", "رجب", "شعبان", "رمضان", "شوّال", "ذو القعدة", "ذو الحجة"); ?>
                                        <?php endif; ?>
                                        <b><?= lang("the_report") ?> : </b> <?= lang("personal_details") ?><br>
                                        <b><?= lang('department') ?> : </b>
                                        <?php if ($department == 0): ?>
                                            <?= lang("all") ?>
                                        <?php else: ?>
                                            <?php foreach ($departments as $dep): ?>
                                                <?php if ($dep->department_id == $department): ?>
                                                    <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                        <b><?= lang('designation') ?> : </b>
                                        <?php if ($designation == -1): ?>
                                            <?= lang("all") ?>
                                        <?php elseif ($designation == 0): ?>
                                            <?= lang("without_section") ?>
                                        <?php else: ?>
                                            <?php foreach ($designations as $des): ?>
                                                <?php if ($des->designations_id == $designation): ?>
                                                    <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                    </div>
                                </td>
                                <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center"><?= lang("employee_name") ?></th>
                                <th class="text-center"><?= lang("reference") ?></th>
                                <th class="text-center"><?= lang("personal_details") ?></th>
                                <th class="text-center"><?= lang("dependent") ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($results as $result): ?>
                                <tr>
                                    <td><?= ($lang == "english") ? $result->full_name_en : $result->full_name_ar; ?></td>
                                    <td><?= $result->employment_id ?></td>
                                    <td>
                                        <b><?= lang('full_name_ar') ?> : </b><?= $result->full_name_ar ?><br>
                                        <b><?= lang('full_name_en') ?> : </b><?= $result->full_name_en ?><br>
                                        <b><?= lang('date_of_birth') ?> : </b><?= $result->date_of_birth ?><br>
                                        <b><?= lang('gender') ?>
                                            : </b><?= ($result->gender == "Male") ? lang('male') : lang('female'); ?>
                                        <br>
                                        <b><?= lang('phone') ?> : </b><?= $result->phone ?><br>
                                        <b><?= lang('email') ?> : </b><?= $result->email ?><br>
                                        <b><?= lang('nationality') ?>
                                            : </b><?= ($lang == 'english') ? $result->countryName : $result->countryName_ar; ?>
                                        <br>
                                        <b><?= lang('present_address') ?> : </b><?= $result->present_address ?><br>
                                        <b><?= lang('education') ?> : </b><?= $result->education ?><br>
                                        <b><?= lang('passport_no') ?> : </b><?= $result->passport_number ?><br>
                                        <b><?= lang('passport_end') ?> : </b><?= $result->passport_end ?><br>
                                        <b><?= lang('identity_no') ?> : </b><?= $result->identity_no ?><br>
                                        <b><?= lang('identity_end') ?> : </b><?= $result->identity_end ?><br>
                                        <b><?= lang('passport_end') ?> : </b><?= $result->passport_end ?><br>
                                        <b><?= lang('maratial_status') ?>
                                            : </b><?= lang(strtolower($result->maratial_status)) ?><br>
                                        <b><?= lang('employee_type') ?>
                                            : </b><?= ($lang == 'english') ? $result->name_en : $result->name_ar; ?><br>
                                        <b><?= lang('joining_date') ?> : </b><?= $result->joining_date ?><br>
                                        <b><?= lang('retirement_date') ?> : </b><?= $result->retirement_date ?><br>
                                        <b><?= lang('job_time') ?>
                                            : </b><?= ($result->job_time == "Full") ? lang('job_full') : lang('job_part'); ?>
                                        <br>
                                        <b><?= lang('job_place') ?>
                                            : </b><?= ($lang == 'english') ? $result->place_name_en : $result->place_name_ar; ?>
                                        <br>
                                        <b><?= lang('job_title') ?>
                                            : </b><?= ($lang == 'english') ? $result->job_titles_name_en : $result->job_titles_name_ar; ?>
                                        <br>
                                        <b><?= lang('holiday_no') ?>
                                            : </b><?= $result->holiday_no ?> <?= lang('days') ?>
                                        <br>
                                    </td>
                                    <td style="vertical-align: text-top">
                                        <b><?= lang('wife_name') ?>: </b><?= $result->wife_name ?><br>
                                        <b><?= lang('wife_name_ar') ?>: </b><?= $result->wife_name_ar ?><br>
                                        <b><?= lang('wife_birth') ?> : </b><?= $result->wife_birth ?><br>
                                        <br>
                                        <?php $fils_names = explode(';', @$result->fils_name); ?>
                                        <?php $fils_names_ar = explode(';', @$result->fils_name_ar); ?>
                                        <?php $fils_births = explode(';', @$result->fils_birth); ?>
                                        <?php for ($x = 0; $x < count($fils_names_ar); $x++): ?>
                                            <?php if (!empty($fils_names_ar[$x])): ?>
                                                <b><?= lang('fils_name_ar')?>: </b><?= $fils_names_ar[$x]; ?><br>
                                                <b><?= lang('fils_name')?>: </b><?= $fils_names[$x]; ?><br>
                                                <b><?= lang('fils_birth') ?> : </b><?= $fils_births[$x] ?><br>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>



<script type="text/javascript">

    function print_this() {
        var divToPrint=$('#printed_contenet');
        console.log(divToPrint.html());

        var newWin=window.open('','PRINT', 'height=400,width=600');

        newWin.document.write('<html><body onload="window.print()"><h3><?=lang("reports_section")?></h3><hr><style type="text/css">table { page-break-inside:auto }tr{ page-break-inside:avoid; page-break-after:auto }thead{ display:table-header-group }tfoot { display:table-footer-group }</style>'+divToPrint.html()+'</body></html>');

        newWin.document.close();

        return true;

    }
</script>