<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">
                    <?php @$today2 = date_create_from_format('Y-m-d', @$today)->format('Ymd'); ?>
                    <a class="btn btn-primary btn-xs" onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <div class="print" id="printed_content">
                        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                        <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                        <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                        <?php if ($lang == "arabic"): ?>
                            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                  rel="stylesheet"/>
                        <?php endif; ?>
                        <style type="text/css">
                            .btn.btn-primary.btn-xs {
                                float: left;
                                margin-bottom: 20px;
                            }

                            .print {
                                border: 1px solid gray;
                                min-height: 200px;
                                margin-bottom: 100px;
                                padding: 100px;
                            }

                            table {
                                width: 100%;
                            }

                            .print .boxino {
                                border: 1px solid black;
                                border-radius: 6px;
                                float: right;
                                min-width: 300px;
                                /*height: 100px;*/
                                padding: 10px 15px;
                            <?php if($lang == "english"):?> float: left;
                            <?php else: ?> float: right;
                            <?php endif; ?>
                            }

                            table {
                                width: 100%;
                            }
                        </style>
                        <table>
                            <tr>
                                <td>
                                    <div class="boxino">
                                        <?php if ($lang == "english"): ?>
                                            <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                        <?php else: ?>
                                            <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                        <?php endif; ?>
                                        <b><?= lang("the_report") ?> : </b> <?= lang("vacations") ?><br>
                                        <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                        <b><?= lang("year") ?>
                                            : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                        <br>
                                        <b><?= lang('department') ?> : </b>
                                        <?php if ($department == 0): ?>
                                            <?= lang("all") ?>
                                        <?php else: ?>
                                            <?php foreach ($departments as $dep): ?>
                                                <?php if ($dep->department_id == $department): ?>
                                                    <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                        <b><?= lang('designation') ?> : </b>
                                        <?php if ($designation == -1): ?>
                                            <?= lang("all") ?>
                                        <?php elseif ($designation == 0): ?>
                                            <?= lang("without_section") ?>
                                        <?php else: ?>
                                            <?php foreach ($designations as $des): ?>
                                                <?php if ($des->designations_id == $designation): ?>
                                                    <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <br>
                                    </div>
                                </td>
                                <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center"><?= lang('name') ?></th>
                                <th class="text-center"><?= lang('reference') ?></th>
                                <th class="text-center"><?= lang('status') ?></th>
                                <th class="text-center"><?= lang('leave_type') ?></th>
                                <th class="text-center"><?= lang('going_date') ?></th>
                                <th class="text-center"><?= lang('coming_date') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach (@$results as $emp): ?>
                                <? $gdate = array();
                                $cdate = array();
                                $leave_type = array(); ?>
                                <tr>
                                    <td class="text-center"><?= ($lang == "english") ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                    <td class="text-center"><?= $emp->employment_id ?></td>
                                    <td class="text-center">
                                        <? $status = true; ?>
                                        <? foreach ($leaves_list as $lv): ?>
                                            <? if ($lv->employel_id == $emp->employee_id): ?>
                                                <?php if (strpos(@$lv->going_date, $filter) !== false or strpos(@$lv->coming_date, $filter) !== false) : ?>
                                                    <? $status = false;
                                                    array_push($gdate, @$lv->going_date);
                                                    array_push($cdate, @$lv->coming_date); ?>
                                                    <? foreach ($leaves_cat as $lt): ?>
                                                        <? if ($lt->leave_category_id == $lv->leave_category_id): ?>
                                                            <? array_push($leave_type, ($lang == "english") ? $lt->category_en : $lt->category_ar); ?>
                                                        <? endif; ?>
                                                    <?php endforeach; ?>
                                                <? endif; ?>
                                            <? endif ?>
                                        <?php endforeach; ?>
                                        <?php if ($month!=0): ?>
                                            <?= ($status == true) ? '<b style="color: lime">' . lang("in_work") . '</b>' : '<b style="color:crimson">' . lang("on_leave") . '</b>'; ?>
                                        <?php else: ?>
                                            <span class="text-center">------</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center"><?php foreach ($leave_type as $t) {
                                            echo $t . '<br>';
                                        } ?></td>
                                    <td class="text-center"><?php foreach ($gdate as $gt) {
                                            echo $gt . '<br>';
                                        } ?></td>
                                    <td class="text-center"><?php foreach ($cdate as $ct) {
                                            echo $ct . '<br>';
                                        } ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<script type="text/javascript">

    function print_this() {
        var divToPrint = $('#printed_contenet');
        console.log(divToPrint.html());

        var newWin = window.open('', 'PRINT', 'height=400,width=600');

        newWin.document.write('<html><body onload="window.print()"><h3><?=lang("reports_section")?></h3><hr><style type="text/css">table { page-break-inside:auto }tr{ page-break-inside:avoid; page-break-after:auto }thead{ display:table-header-group }tfoot { display:table-footer-group }</style>' + divToPrint.html() + '</body></html>');

        newWin.document.close();

        return true;

    }
</script>