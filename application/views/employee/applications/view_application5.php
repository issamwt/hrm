<div class="print" id="printed_contenet">
    <?php if ($lang == "arabic"): ?>
        <link href="<?=base_url() ?>asset/css/bootstrap.ar.min.css" rel="stylesheet"/>
    <?php else: ?>
        <link href="<?=base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
    <?php endif; ?>

    <style type="text/css">

        .box.box-primary{
            border: none !important;
        }

        @media print {

            .print {
                border: 1px solid gray;
                min-height: 200px;
                margin-bottom: 100px;
                padding: 100px;
                direction: rtl !important;
                text-align: right !important;
            }

            .no-print {
                display: none;
            }

            h4 {
                text-align: center;
                color: #000;
            }
            .label{
                border:none;
            }
        }

        .ids {
            border: 2px solid darkslategray;
        }

        .ids.current {
            border: 2px solid red;
            border-style: double;
        }

        .table-container {
            padding: 20px;
            padding-top: 10px;
        }

        h4 {
            color: #fff;
            background: #337AB7;
            padding: 5px 0px;
        }
    </style>

    <div class="col-md-12">

        <a class="btn btn-success btn-block no-print" onclick="printdiv('printed_contenet')"><i class="fa fa-print"></i> <?= lang('print') ?></a>
        <div class="box box-primary">
            <!-- Default panel contents -->

            <div class="table-container">
                <!--  /////////////////  la_vacations //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_vacations"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_vacations') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <!--////////////////////////////////-->
                        <?php if (@$view_application->type == 1): ?>
                            <?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?php
                                    @$employee_detail->joining_date = $emp->joining_date;
                                    @$employee_detail->holiday_no=$emp->holiday_no;
                                    @$employee_detail->old_rest = $emp->old_rest;
                                    @$employee_detail->new_balance = $emp->new_balance;
                                    ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <?php if ($lang == 'english')
                                $months = array("Muharram", "Safar", "Rabi' al-awwal", "Rabi' al-thani", "Jumada al-awwal", "Jumada al-thani", "Rajab", "Sha'aban", "Ramadan", "Shawwal", "Dhu al-Qi'dah", "Dhu al-Hijjah");
                            else
                                $months = array("محرّم", "صفر", "ربيع الأول", "ربيع الثاني", "جمادى الاول", "جمادى الآخر أو جمادى الثاني", "رجب", "شعبان", "رمضان", "شوّال", "ذو القعدة", "ذو الحجة");
                            $d = explode('-', @$employee_detail->joining_date);
                            $d2 = explode('-', @$today);
                            // employee join the work in this year (or employee join the work before this year and joining date has passed)
                            if (($d2[0] == $d[0]) or (($d2[1] * 30 + $d2[2]) > ($d[1] * 30 + $d[2]))) {
                                $days = abs(($d2[1] * 30 + $d2[2]) - ($d[1] * 30 + $d[2]));
                            } // employee join the work before this year
                            else {
                                $date1 = new DateTime(($d2[0]-1).'-'.$d[1].'-'.$d[2]);
                                $date2 = new DateTime($today);
                                $days = $date2->diff($date1)->format("%a");
                            }
                            // $d[2].' '.$months[ltrim($d[1],'0')]
                            ?>
                        <?php endif; ?>
                        <!--////////////////////////////////-->
                        <tr>
                            <td width="40%"><?= lang('leave_type') ?></td>
                            <td>
                                <?php foreach (@$all_leave_category as $lc): ?>
                                    <?php if ($lc->leave_category_id == @$view_application->type): ?>
                                        <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?= lang('start_date') ?></td>
                            <td><?= @$view_application->start_date ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('end_date') ?></td>
                            <td><?= @$view_application->end_date ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('leave_duration') ?></td>
                            <td><?= @$view_application->leave_duration ?> <?= lang('days') ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('address_in_leave') ?></td>
                            <td><?= @$view_application->address_in_leave ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('telephone') ?></td>
                            <td><?= @$view_application->tel_in_leave ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('replacement') ?></td>
                            <td>
                                <?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == @$view_application->replacement): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('tikets') ?></td>
                            <td>
                                <?php
                                switch (@$view_application->tikets) {
                                    case '1':
                                        echo lang('tikets_opt1');
                                        break;
                                    case '2':
                                        echo lang('tikets_opt2');
                                        break;
                                    case '3':
                                        echo lang('tikets_opt3');
                                        break;
                                    default:
                                        echo '?';
                                        break;
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('going_date') ?></td>
                            <td><?= @$view_application->going_date ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('coming_date') ?></td>
                            <td><?= @$view_application->coming_date ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('going_date_2') ?></td>
                            <td><?= @$view_application->going_date_2 ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('coming_date_2') ?></td>
                            <td><?= @$view_application->coming_date_2 ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_advance //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_advance"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_advance') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('advance_type') ?></td>
                            <td>
                                <?php foreach (@$advances_list as $al): ?>
                                    <?php if ($al->advance_id == @$view_application->type): ?>
                                        <?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('advance_value') ?></td>
                            <td><?= @$view_application->value ?> <?= lang('rial') ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('payement_method') ?></td>
                            <td><?= lang('payement_method' . @$view_application->payement_method) ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('monthly_installement') ?></td>
                            <td><?= @$view_application->monthly_installement ?> <?= lang('rial') ?></td>
                        </tr>

                        <tr>
                            <td><?= lang('payement_months') ?></td>
                            <td><?= @$view_application->payement_months ?> <?= lang('months') ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('voucher') ?></td>
                            <td>
                                <?php if (@$view_application->other_employee_id == 0): ?>
                                    <?= lang('no-exist') ?>
                                <?php else: ?>
                                    <?php foreach (@$employees_list as $emp): ?>
                                        <?php if ($emp->employee_id == @$view_application->other_employee_id): ?>
                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('phone') ?></td>
                            <td><?= @$view_application->phone ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_caching //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_caching"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_caching') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('cahing_type') ?></td>
                            <td><?= lang('cahing_type'.$view_application->type) ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('beneficiary_name') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('bill_number') ?></td>
                            <td><?= @$view_application->bill_number ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('item_no') ?></td>
                            <td><?= @$view_application->item_no ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('cahing_value') ?></td>
                            <td><?= @$view_application->value ?> <?=lang('rial')?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('bank_name') ?></td>
                            <td><?= @$view_application->bank_name ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('account_holder_name') ?></td>
                            <td><?= @$view_application->account_holder_name ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('phone') ?></td>
                            <td><?= @$view_application->phone ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('country') ?></td>
                            <td><?= @$view_application->country ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('additional_bank_information') ?> :</td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('account_number') ?></td>
                            <td><?= @$view_application->account_number ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('beneficiary_address') ?></td>
                            <td><?= @$view_application->address_in_leave ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('swift_code') ?></td>
                            <td><?= @$view_application->swift_code ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('city') ?></td>
                            <td><?= @$view_application->city ?></td>
                        </tr>
                        <tr>
                            <td width="30%"><?= lang('caching_reason') ?></td>
                            <td><?= @$view_application->caching_reason ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_custody //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_custody"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_custody') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('name') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_transfer //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_transfer"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_transfer') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('job_title') ?></td>
                            <td>
                                <?php foreach (@$job_titles_list as $jt): ?>
                                    <?php if ($jt->job_titles_id == @$view_application->new_jt): ?>
                                        <?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('department') ?></td>
                            <td>
                                <?php foreach (@$department_list as $dep): ?>
                                    <?php if ($dep->department_id == @$view_application->new_dep): ?>
                                        <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('designation') ?></td>
                            <td>
                                <?php foreach (@$designations_list as $des): ?>
                                    <?php if ($des->designations_id == @$view_application->new_sect): ?>
                                        <?= ($lang == 'english') ? $des->designations : $des->designations_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('employee_cat') ?></td>
                            <td>
                                <?php foreach (@$emp_cat_list as $ec): ?>
                                    <?php if ($ec->id == @$view_application->new_emp_cat): ?>
                                        <?= ($lang == 'english') ? $ec->name_en : $ec->name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('job_time') ?></td>
                            <td><?= lang(@$view_application->new_jtme) ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_training //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_training"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_training') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_name') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_institute') ?></td>
                            <td><?= @$view_application->course_institute ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_date') ?></td>
                            <td><?= @$view_application->start_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_price') ?></td>
                            <td><?= @$view_application->value ?> <?= lang('rial') ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_note') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_overtime //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_overtime"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_overtime') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('num_hours') ?></td>
                            <td><?= @$view_application->other_employee_id ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('permission_star') ?></td>
                            <td style="direction: ltr !important;text-align: right;"><?= @$view_application->start_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('extra_work_type') ?></td>
                            <td>
                                <?php foreach (@$extra_work_list as $exw): ?>
                                    <?php if ($exw->extra_work_id == @$view_application->type): ?>
                                        <?= ($lang == 'english') ? $exw->title_en : $exw->title_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('details') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_purchase //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_purchase"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_purchase') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                    </table>
                    <?php $prod_name = explode(';', @$view_application->prod_name); ?>
                    <?php $prod_desc = explode(';', @$view_application->prod_desc); ?>
                    <?php $prod_num = explode(';', @$view_application->prod_num); ?>
                    <?php $prod_note = explode(';', @$view_application->prod_note); ?>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="27%" class="text-center"><?= lang('prod_name') ?></th>
                            <th width="40%" class="text-center"><?= lang('prod_desc') ?></th>
                            <th width="10%" class="text-center"><?= lang('prod_num') ?></th>
                            <th width="28%" class="text-center"><?= lang('prod_note') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php for ($i = 0; $i < 5; $i++): ?>
                            <?php if (!empty($prod_name[$i])): ?>
                                <tr>
                                    <td class="text-center"><?= $i + 1 ?></td>
                                    <td><?= $prod_name[$i] ?></td>
                                    <td><?= $prod_desc[$i] ?></td>
                                    <td class="text-center"><?= $prod_num[$i] ?></td>
                                    <td><?= $prod_note[$i] ?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endfor; ?>
                        </tbody>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_maintenance //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_maintenance"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_maintenance') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('app_title') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('msg_body') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_annual_assessment //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_annual_assessment"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_annual_assessment') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('app_name') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_permission //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_permission"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_permission') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('permission_type') ?></td>
                            <td><?= lang('permission_opt' . @$view_application->type) ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('permission_star') ?></td>
                            <td><?= @$view_application->start_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('permission_end') ?></td>
                            <td><?= @$view_application->end_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_def_salary //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_def_salary"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_def_salary') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('app_title') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_filter_dues //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_filter_dues"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_filter_dues') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('allowances') ?></td>
                            <td>
                                <?php foreach (@$allowances_list as $al): ?>
                                    <?php if ($al->allowance_id == @$view_application->type): ?>
                                        <?= ($lang == 'english') ? $al->allowance_title_en : $al->allowance_title_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_resignation //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_resignation"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_resignation') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('reason') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_job_application //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_job_application"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_job_application') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('full_name_ar') ?></td>
                            <td><?= @$view_application->full_name_ar ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('full_name_en') ?></td>
                            <td><?= @$view_application->full_name_en ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('identity_no') ?></td>
                            <td><?= @$view_application->id_no ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('identity_end') ?></td>
                            <td><?= @$view_application->id_no_end ?></td>
                        </tr>
                        <tr>
                            <?php $countries = $this->db->select('*')->from('countries')->get()->result(); ?>
                            <td width="40%"><?= lang('nationality') ?></td>
                            <td>
                                <?php foreach ($countries as $country): ?>
                                    <?php if ($country->idCountry == @$view_application->nationality): ?>
                                        <?= ($lang == 'english') ? $country->countryName : $country->countryName_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('gender') ?></td>
                            <td><?= (@$view_application->sex == "male") ? lang('male') : lang('female'); ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('date_of_birth') ?></td>
                            <td><?= @$view_application->birth_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('phone') ?></td>
                            <td><?= @$view_application->phone ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('maratial_status') ?></td>
                            <td><?php
                                if (@$view_application->maratial_status == "married") echo lang('married');
                                elseif (@$view_application->maratial_status == "un_married") echo lang('un-married');
                                elseif (@$view_application->maratial_status == "widowed") echo lang('widowed');
                                elseif (@$view_application->maratial_status == "divorced") echo lang('married');
                                ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('job_time') ?></td>
                            <td><?php
                                if (@$view_application->job_time == "full") echo lang('job_full');
                                elseif (@$view_application->job_time == "part") echo lang('job_part');
                                ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('photo') ?></td>
                            <td>
                                <?php $src = ($view_application->photo) ? base_url() . @$view_application->photo : base_url() . 'img/admin.png'; ?>
                                <img src="<?= $src ?>" style="width:130px; border:1px solid black; border-radius: 1px;">
                            </td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_recrutement //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_recrutement"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_recrutement') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('course_institute') ?></td>
                            <td><?= @$view_application->course_institute ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('recrutement_task') ?></td>
                            <td><?= @$view_application->name ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('recrutement_type') ?></td>
                            <td><?= (@$view_application->type == 1) ? lang('internal') : lang('external'); ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('from_institute') ?></td>
                            <td><?= @$view_application->address_in_leave ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('going_date') ?></td>
                            <td><?= @$view_application->going_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('coming_date') ?></td>
                            <td><?= @$view_application->coming_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('duration') ?></td>
                            <td><?= @$view_application->leave_duration ?> <?= lang('day') ?></td>
                        </tr>

                        <tr>
                            <td width="40%"><?= lang('details') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <!--  /////////////////  la_embarkation //////////////////////////////////////////////////////////////////////// -->
                <?php if (@$view_application->type_app == "la_embarkation"): ?>
                    <table style="width: 100%; margin-bottom: 20px">
                        <tr>
                            <td><?= lang('la_embarkation') ?></td>
                            <td><td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;"></td></td>
                        </tr>
                    </table>
                    <table class="table  table-bordered">
                        <tr>
                            <td width="40%"><?= lang('employee_name') ?></td>
                            <td><?php foreach ($employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%"><?=lang("final_result")?></td>
                            <td><?=(@$view_application->status==1)?lang("appro_accept"):lang("appro_refuse");?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('coming_date_after_vacation') ?></td>
                            <td><?= @$view_application->coming_date ?></td>
                        </tr>
                        <tr>
                            <td width="40%"></td>
                            <td><?= lang('embarkation_note1') ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('date') ?></td>
                            <td><?= @$view_application->coming_date_2 ?> </td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('embarkation_note2') ?></td>
                            <td><?= @$view_application->leave_duration ?> <?= lang('day') ?> / <?= lang('days') ?></td>
                        </tr>
                        <tr>
                            <td width="40%"><?= lang('notes') ?></td>
                            <td><?= @$view_application->note ?></td>
                        </tr>

                    </table>
                <?php endif; ?>


                <!--  /////////////////  ids //////////////////////////////////////////////////////////////////////// -->

                <?php if (!empty(@$app_notes)): ?>
                    <h4 class="text-center"><?= lang('notes') ?></h4>
                    <table class="table table-bordered">
                        <tbody>
                        <?php foreach (@$app_notes as $an): ?>
                            <tr>
                                <td>
                                    <?php foreach ($employees_list as $emp): ?>
                                        <?php if ($emp->employee_id == $an->employee_id): ?>
                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php get_action($an->action) ?>
                                </td>
                                <td width="70%">
                                    <?= $an->note_reason; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>

<?php

function get_action($x)
{
    switch ($x) {
        case '1':
            echo lang('appro_accept');
            break;
        case '2':
            echo lang('appro_refuse');
            break;
        case '3':
            echo lang('appro_delegate');
            break;
        case '4':
            echo lang('appro_consultation');
            break;
        case '5':
            echo lang('appro_pass');
            break;
        default:
            echo lang('appro_accept');
            break;
    }
}

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.12.2/printThis.min.js"></script>
<script language="javascript">

    function printdiv()
    {
        $('#myModal').modal('hide');

        setTimeout(function () {
            $('#printed_contenet').printThis();

        },500);
    }
</script>



