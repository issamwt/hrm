<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    .iframe {
        width: 100%
    }

    .iframe-container {
        margin: 10px auto;
        /*display: none;*/
    }

    .iframe-container .iframe-buttons {
        margin: 10px auto;
        display: block;
        width: 568px;
        text-align: center;
    }

    .iframe iframe {
        margin: 10px auto;
        display: block;
        min-width: 568px;
        min-height: 400px
    }

    .appro_consultation, .appro_delegate {
        margin: 10px auto;
        display: none;
        width: 50%;
    }
    tr.active td{
        color: white;
        background-color: #08C !important;;
    }
    tr.active:hover td{
        color: #ffffff !important;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <!-- applications_list -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('waiting_apps') ?> </strong>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('id_app') ?></th>
                            <th class="text-center"><?= lang('date_app') ?></th>
                            <th class="text-center"><?= lang('employee_id') ?></th>
                            <th class="text-center"><?= lang('employee_name') ?></th>
                            <th class="text-center"><?= lang('app_type') ?></th>
                            <th class="text-center"><?= lang('configure_approval') ?></th>
                            <th class="text-center"><?= lang('status') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($applications_list)): ?>
                            <?php foreach ($applications_list as $app): if ($app->status == 0): ?>
                                <?php $ids = explode(';', $app->ids); $class=''; ?>
                                <?php $class= (@$ids[$app->current] == $this->session->userdata('employee_id') and $app->status == 0)?'active':''; ?>
                                <tr <?='class="'.$class.'"'?>>
                                    <td class="text-center"><?= $app->application_id ?></td>
                                    <td class="relative" style="vertical-align: bottom;padding-bottom: 3px;"><?= $app->date ?><?= hijjri($app->date); ?></td>
                                    <td>
                                        <?php foreach ($all as $emp): ?>
                                            <?php if ($emp->employee_id == $app->employee_id): ?>
                                                <?= $emp->employment_id ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach ($all as $emp): ?>
                                            <?php if ($emp->employee_id == $app->employee_id): ?>
                                                <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= lang($app->type_app) ?></td>
                                    <td>
                                        <?php foreach ($approval_cats as $ac): ?>
                                            <?php if ($ac->approval_cat_id == $app->approval_cat_id): ?>
                                                <?= ($lang == 'english') ? $ac->approval_cat_en : $ac->approval_cat_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="text-center"><?= status($app->status) ?></td>
                                    <td class="text-center">
                                        <?= anchor('employee/applications/view_application/' . $app->application_id, '<span class="fa fa-list-alt"></span> ' . lang('view'), array('class' => "btn btn-primary btn-xs", 'title' => lang('view'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                        <?php $ids = explode(';', $app->ids); ?>
                                        <?php if (@$ids[$app->current] == $this->session->userdata('employee_id') and $app->status == 0): ?>
                                            <?= anchor('employee/applications/view_application3/' . $app->application_id, '<span class="fa fa-eye"></span> ' . lang('app_handling'), array('class' => "btn btn-success btn-xs", 'title' => lang('app_handling'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                        <?php else: ?>
                                            <button class="btn btn-success btn-xs" disabled=""><i
                                                        class="fa fa-eye"></i> <?= lang('app_handling') ?></button>
                                        <?php endif; ?>
                                        <?php if($this->emp_type=='hr_manager' and $app->type_app=='la_advance' and $app->status==0):?>
                                            <?= anchor('employee/applications/view_application4/' . $app->application_id, '<span class="fa fa-eye"></span> ' . lang('modify_app'), array('class' => "btn btn-info btn-xs", 'title' => lang('modify_app'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php endif; endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <br><br>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <!-- applications_list -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('received_apps_archive') ?> </strong>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" id="dataTables-example">
                        <thead>
                        <tr>
                            <th class="text-center"><?= lang('id_app') ?></th>
                            <th class="text-center"><?= lang('date_app') ?></th>
                            <th class="text-center"><?= lang('employee_id') ?></th>
                            <th class="text-center"><?= lang('employee_name') ?></th>
                            <th class="text-center"><?= lang('app_type') ?></th>
                            <th class="text-center"><?= lang('configure_approval') ?></th>
                            <th class="text-center"><?= lang('status') ?></th>
                            <th class="text-center"><?= lang('action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($applications_list)): ?>
                            <?php foreach ($applications_list as $app): if ($app->status != 0): ?>
                                <tr>
                                    <td class="text-center"><?= $app->application_id ?></td>
                                    <td class="relative" style="vertical-align: bottom;padding-bottom: 3px;"><?= $app->date ?><?= hijjri($app->date); ?></td>
                                    <td>
                                        <?php foreach ($all as $emp): ?>
                                            <?php if ($emp->employee_id == $app->employee_id): ?>
                                                <?= $emp->employment_id ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php foreach ($all as $emp): ?>
                                            <?php if ($emp->employee_id == $app->employee_id): ?>
                                                <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td><?= lang($app->type_app) ?></td>
                                    <td>
                                        <?php foreach ($approval_cats as $ac): ?>
                                            <?php if ($ac->approval_cat_id == $app->approval_cat_id): ?>
                                                <?= ($lang == 'english') ? $ac->approval_cat_en : $ac->approval_cat_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="text-center"><?= status($app->status) ?></td>
                                    <td class="text-center">
                                        <?= anchor('employee/applications/view_application/' . $app->application_id, '<span class="fa fa-list-alt"></span> ' . lang('view'), array('class' => "btn btn-primary btn-xs", 'title' => lang('view'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                        <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                            <?= anchor('employee/applications/view_application5/' . $app->application_id, '<span class="fa fa-print"></span> ' . lang('print'), array('class' => "btn btn-success btn-xs print-me", 'title' => lang('print'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                            <a href="<?= base_url() ?>employee/applications/delete_application/<?= $app->application_id ?>"
                                               class="btn btn-danger btn-xs"
                                               onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                        class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                        <? endif;?>
                                    </td>
                                </tr>
                            <?php endif; endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<?php
function status($x)
{
    switch ($x) {
        case '0':
            echo '<span class="label label-warning">' . lang('pending') . '</span>';
            break;
        case '1':
            echo '<span class="label label-success">' . lang('accepted') . '</span>';
            break;
        case '2':
            echo '<span class="label label-danger">' . lang('rejected') . '</span>';
            break;
        default:
            echo '?';
            break;
    }
}

?>


<script type="text/javascript">
    function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }
</script>

<script type="text/javascript">
    function show_app(id) {
        console.log(id);
        console.log($('#' + id).attr('id'));
        $('#' + id).fadeToggle('medium', function () {
            if ($(this).is(':visible'))
                $(this).css('display', 'block');
        });
    }
    function show_delegators() {
        $('.appro_delegate').slideToggle();
        return false;
    }
    function show_consultants() {
        $('.appro_consultation').slideToggle();
        return false;
    }
</script>