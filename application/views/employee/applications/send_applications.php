<div class="col-md-12">

    <style type="text/css">
        .nav-tabs-custom {
            box-shadow: none !important;
        }

        .tab-content {
            border: 1px solid #f4f4f4;
            border-top: none;
        }

        .tab-pane .form {
            margin-top: 30px
        }

        .note {
            cursor: inherit;
            box-shadow: none;
            border-color: lightgray;
        }

        .spinner_sects {
            display: none;
            font-size: 7px;
        }

        .nav-tabs-custom > .nav-tabs > li > a {
            border-bottom: 0px;
        }

        a.not-allowed {
            background: #eee !important;
            cursor: not-allowed !important;
        }

        .boxino {
            padding: 15px 15px 15px 50px;
            display: inline-block;
            border-radius: 1px;
            background: #337AB7;
            color: #fff;
        }

        .nav-tabs-custom > .nav-tabs > li > a {
            padding: 9px;
        }

        .employees-list{
            text-align: center;
        }

        .employees-list .btn{
            text-align: center;
            margin: 5px;
            padding: 7px 20px;
            border-radius: 2px !important;
        }

        .employees-list .btn h4{
            margin: 0px;
            padding: 0px;
        }
        td.general_info{
            padding-left: 20px
        }
        td.general_info img{
            width:100px;
            height: 100px;
            border: 1px solid #fff;
        }
        .employees-list .panel-body{
            display: none;
        }
    </style>

    <?php echo message_box('success');  ?>
    <?php echo message_box('error'); ?>

    <div class="main_content">
        <div class="row">
            <div class="col-md-12">

                <div class="boxino">
                    <table>
                        <tr>
                            <td class="general_info" rowspan="4">
                                <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $employee_detail->gender == "Female"): ?>
                                    <img src="<?= base_url() ?>img/adminf.png"/>
                                <?php else: ?>
                                    <img src="<?= (!empty(@$employee_detail->photo)) ? base_url() . @$employee_detail->photo : base_url() . 'img/admin.png'; ?>"/>
                                <?php endif; ?>
                            </td>
                            <td width="160"><b><?= lang('name') ?> : </b></td>
                            <td><?= ($lang == 'english') ? $employee_detail->full_name_en : $employee_detail->full_name_ar; ?></td>
                        </tr>
                        <tr>
                            <td width="160"><b><?= lang('department') ?> : </b></td>
                            <td><?= ($lang == 'english') ? $employee_detail->department_name : $employee_detail->department_name_ar; ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('direct_manager') ?> :</b></td>
                            <td><?= ($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar']; ?></td>
                        </tr>
                        <tr>
                            <td><b><?= lang('hr_manager') ?> :</b></td>
                            <td><?= ($lang == 'english') ? $managers['human_resourcen_name_en'] : $managers['human_resource_name_ar']; ?></td>
                        </tr>
                    </table>
                </div>
                <br><br>

                <!-- send_applications -->
                <div class="panel panel-primary">
                    <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                         style="cursor: pointer">
                        <strong><?= lang('your_approvals_cat') ?> </strong>
                    </div>
                    <div class="panel-body" style="display: none"><br>
                        <div><a class="btn btn-primary" style="background: #fff;"></a> <?= lang('allowed') ?></div>
                        <div><a class="btn btn-primary" style="background: #EEEEEE"></a> <?= lang('not_allowed') ?>
                        </div>
                        <br>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="15%" class="text-center"><?= lang('name') ?></th>
                                <th width="14%" class="text-center"><?= lang('job_place') ?></th>
                                <th width="14%" class="text-center"><?= lang('department') ?></th>
                                <th width="14%" class="text-center"><?= lang('designation') ?></th>
                                <th width="19%" class="text-center"><?= lang('commission') ?></th>
                                <th width="19%" class="text-center"><?= lang('applications') ?></th>
                            </tr>
                            </thead>
                            <tbody style="font-weight:400">
                            <?php if (!empty(@$approvals_list)): ?>
                                <?php foreach ($approvals_list as $approval): ?>
                                    <tr>
                                        <td class='approval-name'>
                                            <?= ($lang == 'english') ? $approval->approval_cat_en : $approval->approval_cat_ar; ?>
                                            <input type="hidden" class="code" name="code"
                                                   value="<?= $approval->jplc ?>/<?= $approval->dep ?>/<?= $approval->sect ?>">
                                            <input type="hidden" class="data-id" name="approval_cat_id"
                                                   value="<?= $approval->approval_cat_id ?>">
                                        </td>
                                        <td class="text-center"><?= ($approval->jplc) ? ($lang == 'english') ? $approval->place_name_en : $approval->place_name_ar : lang('all'); ?></td>
                                        <td class="text-center"><?= ($approval->dep) ? ($lang == 'english') ? $approval->department_name : $approval->department_name_ar : lang('all'); ?></td>
                                        <td class="text-center"><?= ($approval->sect) ? ($lang == 'english') ? $approval->designations : $approval->designations_ar : lang('all'); ?></td>
                                        <td>

                                            <?php $sl = 1; ?>
                                            <?php foreach (explode(";", $approval->all_list) as $adm): ?>
                                                <?= ($adm == 'dm') ? '(' . $sl++ . ') ' . (($lang == 'english') ? $managers['direct_manager_name_en'] : $managers['direct_manager_name_ar']) : ''; ?>
                                                <?php foreach ($employees_list as $emp): ?>
                                                    <?= ($emp->employee_id == $adm) ? '(' . $sl++ . ') ' . (($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar) : ''; ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </td>
                                        <td class="linked-apps" data-apps="<?= $approval->linked_apps ?>">
                                            <?php foreach (explode(",", $approval->linked_apps) as $app): ?> -
                                                <?php if (strpos($app, 'la_vacations') !== FALSE): ?>
                                                    <?php if (explode('/', $app)[1] == '0'): ?>
                                                        <?= lang('all_leaves') ?>
                                                    <?php else: ?>
                                                        <?php foreach ($leaves_cat as $lc): ?>
                                                            <?php if ($lc->leave_category_id == explode('/', $app)[1]): ?>
                                                                <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                <?php elseif (strpos($app, 'la_advance') !== FALSE): ?>
                                                    <?php if (explode('/', $app)[1] == '0'): ?>
                                                        <?= lang('all_advances') ?>
                                                    <?php else: ?>
                                                        <?php foreach ($advances_list as $al): ?>
                                                            <?php if ($al->advance_id == explode('/', $app)[1]): ?>
                                                                <?= ($lang == 'english') ? $al->title_en : $al->title_ar ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <?= lang($app) ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- send_applications -->



                <!-- send_applications -->
                <div class="panel panel-primary">
                    <div class="panel-heading" onclick="$(this).next('.panel-body').slideToggle()"
                         style="cursor: pointer">
                        <strong><?= lang('application_templ') ?> </strong>
                    </div>
                    <div class="panel-body" style="display: none"><br>
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs apps">
                                <li class="active" data-help="la_vacations"><a href="#la_vacations" class=""
                                                                               data-toggle="tab"><?= lang('leaves') ?></a>
                                </li>
                                <li data-help="la_advance"><a href="#la_advance"
                                                              data-toggle="tab"><?= lang('advances_app') ?></a></li>
                                <li data-help="la_caching"><a href="#la_caching"
                                                              data-toggle="tab"><?= lang('caching') ?></a></li>
                                <li data-help="la_custody"><a href="#la_custody"
                                                              data-toggle="tab"><?= lang('custody') ?></a></li>
                                <li data-help="la_transfer"><a href="#la_transfer"
                                                               data-toggle="tab"><?= lang('transfer') ?></a></li>
                                <li data-help="la_training"><a href="#la_training"
                                                               data-toggle="tab"><?= lang('la_training') ?></a></li>
                                <li data-help="la_overtime"><a href="#la_overtime"
                                                               data-toggle="tab"><?= lang('la_overtime') ?></a></li>
                                <li data-help="la_purchase"><a href="#la_purchase"
                                                               data-toggle="tab"><?= lang('la_purchase') ?></a></li>
                                <li data-help="la_maintenance"><a href="#la_maintenance"
                                                                  data-toggle="tab"><?= lang('la_maintenance') ?></a>
                                </li>

                                <li data-help="la_permission"><a href="#la_permission"
                                                                 data-toggle="tab"><?= lang('la_permission') ?></a></li>
                                <li data-help="la_recrutement"><a href="#la_recrutement"
                                                                  data-toggle="tab"><?= lang('la_recrutement') ?></a>
                                </li>
                                <li data-help="la_embarkation"><a href="#la_embarkation"
                                                                  data-toggle="tab"><?= lang('la_embarkation') ?></a>
                                </li>
                                <?php if ($employee_detail->job_time == 'Full'): ?>
                                    <li data-help="la_def_salary"><a href="#la_def_salary"
                                                                     data-toggle="tab"><?= lang('la_def_salary') ?></a>
                                    </li>
                                    <li data-help="la_filter_dues"><a href="#la_filter_dues"
                                                                      data-toggle="tab"><?= lang('la_filter_dues') ?></a>
                                    </li>
                                <?php endif; ?>
                                <li data-help="la_resignation"><a href="#la_resignation"
                                                                  data-toggle="tab"><?= lang('la_resignation') ?></a>
                                </li>

                                <li data-help="la_annual_assessment"><a href="#la_annual_assessment"
                                                                        data-toggle="tab"><?= lang('la_annual_assessment') ?></a>
                                </li>

                            </ul>
                            <div class="tab-content no-padding">

                                <!--  /////////////////  la_vacations //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane active" id="la_vacations">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_vacations"
                                          method="post" class="form form-horizontal" id="form"><br>

                                        <?php if ($lang == 'english')
                                            $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                                        else
                                            $months = array("جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر");
                                        $d = explode('-', @$employee_detail->joining_date);
                                        $d2 = explode('-',@$today);
                                        // employee join the work in this year (or employee join the work before this year and joining date has passed)
                                        if(($d2[0]==$d[0]) or (($d2[1]*30+$d2[2])>($d[1]*30+$d[2]))){
                                            $days = abs(($d2[1]*30+$d2[2])-($d[1]*30+$d[2]));
                                        }
                                        // employee join the work before this year
                                        else{
                                            $date1 = new DateTime(($d2[0]-1).'-'.$d[1].'-'.$d[2]);
                                            $date2 = new DateTime($today);
                                            $days = $date2->diff($date1)->format("%a");
                                        }
                                        $currentyearm = date("Y");
                                        $dxx = $currentyearm."-01-01";
                                        $dxx2 = $dxx;
                                        $currentyearh = explode("-", $dxx2)[0];
                                        $datenowh =  date("Y-m-d");
                                        if(str_replace("-","",$datenowh)>str_replace("-","",($currentyearh."-".$d[1]."-".$d[2]))){
                                            $starth = ($currentyearh)."-".$d[1]."-".$d[2];
                                            $endh = ($currentyearh+1)."-".$d[1]."-".$d[2];
                                        }else{
                                            $starth = ($currentyearh-1)."-".$d[1]."-".$d[2];
                                            $endh = ($currentyearh)."-".$d[1]."-".$d[2];
                                        }
                                        $new_balance_sanawiya = 0;
                                        ?>
                                        <?php foreach (@$leaves_list as $leave): ?>
                                            <?php
                                            if(str_replace("-","",$leave->going_date)>= str_replace("-","",$starth) and str_replace("-","",$leave->going_date)<=str_replace("-","",$endh)){
                                                if($leave->leave_category_id==1)
                                                    $new_balance_sanawiya+=$leave->duration;
                                                $current = true;
                                            }else{
                                                $current = false;
                                            }
                                        ?>
                                        <?php endforeach;?>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control note1" style="display:none;"
                                                       disabled="" value="<?= lang('leave_note') ?>">
                                                <div class="note2" rows="5" disabled=""
                                                     style="padding:12px; display:none;">
                                                    <?=($lang == 'english')?"Start Year : ":"بداية السنة العقدية : ";?><span><?php echo $starth;?></span><br>
                                                    <?=($lang == 'english')?"End Year : ":"نهاية السنة العقدية : ";?><span><?=$endh;?></span><br>
                                                    <?php $rest = $xrest = (ceil(((@$employee_detail->holiday_no) * $days) / 365 - @$new_balance_sanawiya) <= (@$employee_detail->holiday_no - @$new_balance_sanawiya))
                                                        ? ceil(((@$employee_detail->holiday_no) * $days) / 365 - @$new_balance_sanawiya) + @$employee_detail->old_rest
                                                        : (@$employee_detail->holiday_no - @$new_balance_sanawiya) + @$employee_detail->old_rest; ?>
                                                    <?= lang('holiday_no') ?> : <?= (@$employee_detail->holiday_no) ?><br>
                                                    <?= lang('old_rest') ?> : <?= @$employee_detail->old_rest ?><br>
                                                    <?= lang('new_balance') ?> (<?=($lang == 'english')?"from annual vacations":"من الإجازات السنوية";?>) : <?= @$new_balance_sanawiya ?><br>
                                                    <?= lang('days_of_work_this_yeas') ?> : <?= $days ?><br>
                                                    <?= lang('yearly_holiday_no') ?> : <?= $rest ?><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('leave_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control leaves_cat" required=""
                                                        onchange="get_duration($(this))">
                                                    <?php if (!empty(@$leaves_cat)): ?>
                                                        <option></option>
                                                        <?php foreach (@$leaves_cat as $lc): ?>
                                                            <option value="<?= $lc->leave_category_id ?>"
                                                                    data-duration="<?= $lc->leave_duration ?>"><?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group duration"></div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('start_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input name="start_date" type="text"
                                                       class="form-control hijri_datepickerx" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('end_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input name="end_date" type="text" class="form-control hijri_datepickery"
                                                       required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('leave_duration') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input name="leave_duration" type="number" min="1"
                                                       class="form-control"  id="data-duration" style="width:100px" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('address_in_leave') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="address_in_leave" type="text" class="form-control"
                                                       required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('telephone') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="tel_in_leave" type="text" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('replacement') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="replacement" class="form-control" required="">
                                                    <optgroup label="<?=lang('direct_manager')?>">
                                                        <option value="<?=$managers['direct_manager_id'];?>">
                                                            <?=($lang=='arabic')?$managers['direct_manager_name_ar']:$managers['direct_manager_name_en'];?>
                                                        </option>
                                                    </optgroup>
                                                    <optgroup label="<?=lang('employees')?>">
                                                        <?php if($this->session->userdata('emp_type')=='hr_manager' or $this->session->userdata('emp_type')=='super_manager'): ?>
                                                            <?php foreach(@$employees_list as $emp): ?>
                                                                <?php if($emp->employee_id!=@$employee_detail->employee_id): ?>
                                                                    <option value="<?=$emp->employee_id;?>"><?=($lang=='english')?$emp->full_name_en:$emp->full_name_ar;?></option>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        <?php else:?>
                                                            <?php foreach(@$employees_list as $emp): ?>
                                                                <?php
                                                                $xarray = array($employee_detail->departement_id);
                                                                foreach ($department_list as $dep):
                                                                    if($dep->employee_id==$employee_detail->employee_id):
                                                                        array_push($xarray,$dep->department_id);
                                                                    endif;
                                                                endforeach;
                                                                ?>
                                                                <?php if(in_array($emp->departement_id, $xarray)): ?>
                                                                    <?php if($emp->employee_id!=@$employee_detail->employee_id): ?>
                                                                        <option value="<?=$emp->employee_id;?>"><?=($lang=='english')?$emp->full_name_en:$emp->full_name_ar;?></option>
                                                                    <?php endif;?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <?php if ($employee_detail->job_time == "Full"): ?>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?= lang('tikets') ?> <span
                                                            class="required"> *</span></label>
                                                <div class="col-md-7">
                                                    <select name="tikets" class="form-control" required="">
                                                        <option value=""></option>
                                                        <option value="1"><?= lang('tikets_opt1') ?></option>
                                                        <option value="2"><?= lang('tikets_opt2') ?></option>
                                                        <option value="3"><?= lang('tikets_opt3') ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?= lang('booking_opts') ?>
                                                    :</label>
                                                <div class="col-md-7">
                                                    <button type="button" class="btn btn-success btn-xs" onclick="toogleskip()"><?=lang('skip')?></button>
                                                </div>
                                            </div>
                                            <div class="skippable">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?= lang('going_date') ?> <span
                                                                class="required"> *</span></label>
                                                    <div class="col-md-7">
                                                        <input name="going_date" type="text"
                                                               class="form-control hijri_datepicker" style="width:200px;"
                                                               required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?= lang('coming_date') ?> <span
                                                                class="required"> *</span></label>
                                                    <div class="col-md-7">
                                                        <input name="coming_date" type="text"
                                                               class="form-control hijri_datepicker" style="width:200px;"
                                                               required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?= lang('going_date_2') ?> <span
                                                                class="required"> *</span></label>
                                                    <div class="col-md-7">
                                                        <input name="going_date_2" type="text"
                                                               class="form-control hijri_datepicker" style="width:200px;"
                                                               required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"><?= lang('coming_date_2') ?> <span
                                                                class="required"> *</span></label>
                                                    <div class="col-md-7">
                                                        <input name="coming_date_2" type="text"
                                                               class="form-control hijri_datepicker" style="width:200px;"
                                                               required="">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_advance //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_advance">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_advance"
                                          method="post" class="form form-horizontal" id="form2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('advance_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control advances_list" required="">
                                                    <?php if (!empty(@$advances_list)): ?>
                                                        <?php foreach (@$advances_list as $al): ?>
                                                            <option
                                                                    value="<?= $al->advance_id ?>"><?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('advance_value') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="value"  type="number" class="form-control advance-value" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?=lang('payement_method')?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-3">
                                                <select name="payement_method" onchange="payementmethod($(this))" class="form-control payement-method"  required="" disabled>
                                                    <option></option>
                                                    <option value="1"><?=lang('payement_method1')?></option>
                                                    <?php if(!empty(@$employee_detail->retirement_date)):?>
                                                        <option value="2"><?=lang('payement_method2')?></option>
                                                    <?php else: ?>
                                                        <option disabled><?=lang('payement_method2')?> (<?=lang('no_contract_duration') ?>)</option>
                                                    <?php endif; ?>
                                                    <option value="3"><?=lang('payement_method3')?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?=lang('monthly_installement')?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-3">
                                                <input type="text" value="0" name="monthly_installement" class="form-control monthly-installement" disabled/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?=lang('payement_months')?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-3">
                                                <input type="text" value="0" name="payement_months" class="form-control payement-months" readonly/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-7">
                                                <textarea class="form-control note" rows="3"
                                                          disabled=""><?= lang('advance_note') ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('voucher') ?></label>
                                            <div class="col-md-7">
                                                <select name="other_employee_id" class="form-control">
                                                    <option value="0"></option>
                                                    <?php if (!empty(@$employees_list)): ?>
                                                        <?php foreach (@$employees_list as $emp): ?>
                                                            <option
                                                                    value="<?= $emp->employee_id ?>"><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-7">
                                                <textarea class="form-control note" rows="2"
                                                          disabled=""><?= lang('advance_note_2') ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('phone') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input type="number" name="phone" class="form-control" required="">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_caching //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_caching">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_caching"
                                          method="post" class="form form-horizontal" id="form3">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('cahing_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control" required="">
                                                    <option value="0"><?=lang('cahing_type0')?></option>
                                                    <option value="1"><?=lang('cahing_type1')?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('beneficiary_name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="name" class="form-control" required=""
                                                       value="<?=($lang=="english")?$employee_detail->full_name_en:$employee_detail->full_name_ar;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('bill_number') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="bill_number" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('item_no') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="item_no" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('cahing_value') ?> (<?=lang('rial')?>) <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="value" type="number" min="1" class="form-control"
                                                       required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('bank_name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="bank_name" class="form-control" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('account_holder_name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="account_holder_name" class="form-control" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('phone') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="number" name="phone" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('country') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="country" class="form-control" required="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('additional_bank_information') ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('account_number') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="account_number" class="form-control"  required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('beneficiary_address') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="address_in_leave" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('swift_code') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="swift_code" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('city') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="city" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('caching_reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="caching_reason" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_custody //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_custody">
                                    <br>
                                    <p><b><?= lang('my_custodies') ?> : </b></p>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th><?= lang('custody_reference') ?></th>
                                            <th><?= lang('custody_delivery_date') ?></th>
                                            <th><?= lang('custody_nombre') ?></th>
                                            <th><?= lang('name') ?></th>
                                            <th><?= lang('description') ?></th>
                                            <th><?= lang('files') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$custodies_list)): ?>
                                            <?php foreach (@$custodies_list as $cl): ?>
                                                <tr>
                                                    <td><?= $cl->custody_reference; ?></td>
                                                    <td><?= $cl->delivery_date; ?></td>
                                                    <td><?= $cl->nombre; ?></td>
                                                    <td><?= ($lang == 'english') ? $cl->name_en : $cl->name_ar; ?></td>
                                                    <td><?= ($lang == 'english') ? $cl->description_en : $cl->description_ar; ?></td>
                                                    <td>
                                                        <?php if (!empty($cl->document)): ?>
                                                            <a target="_blank"
                                                               href="<?= base_url() ?><?= $cl->document; ?>"><b><?= lang('download') ?></b></a>
                                                        <?php else: ?>
                                                            <?= lang('no-exist') ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="6" class="text-center">
                                                    <strong><?= lang('nothing_to_display') ?></strong></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                    <br>
                                    <p><b><?= lang('new_custody') ?> : </b></p>
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_custody"
                                          method="post" class="form form-horizontal" id="form5">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="name" type="text" class="form-control" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_transfer //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_transfer">
                                    <br>
                                    <p><b><?= lang('current_data') ?> : </b></p>
                                    <table>
                                        <tr>
                                            <td width="200px"><?= lang('current_job_title') ?> :</td>
                                            <td><?= ($lang == 'english') ? $employee_detail->job_titles_name_en : $employee_detail->job_titles_name_ar; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="200px"><?= lang('current_deaptment') ?> :</td>
                                            <td><?= ($lang == 'english') ? $employee_detail->department_name : $employee_detail->department_name_ar; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="200px"><?= lang('current_designation') ?> :</td>
                                            <td><?= ($employee_detail->designations_id) ? (($lang == 'english') ? $employee_detail->designations : $employee_detail->designations_ar) : lang('no-exist'); ?></td>
                                        </tr>
                                        <tr>
                                            <td width="200px"><?= lang('current_emp_category') ?> :</td>
                                            <td><?= ($lang == 'english') ? $employee_detail->name_en : $employee_detail->name_ar; ?></td>
                                        </tr>
                                        <tr>
                                            <td width="200px"><?= lang('current_job_time') ?> :</td>
                                            <td><?= lang($employee_detail->job_time) ?></td>
                                        </tr>
                                    </table>
                                    <br><br>
                                    <p><b><?= lang('new_data') ?> : </b></p>
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_transfer"
                                          method="post" class="form form-horizontal" id="form6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('job_title') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="new_jt" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php if (!empty(@$job_titles_list)): ?>
                                                        <?php foreach (@$job_titles_list as $jt): ?>
                                                            <option
                                                                    value="<?= $jt->job_titles_id ?>"><?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('department') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="new_dep" class="form-control" required=""
                                                        onchange="print_sects(this.value)">
                                                    <option value=""></option>
                                                    <?php if (!empty(@$department_list)): ?>
                                                        <?php foreach (@$department_list as $dep): ?>
                                                            <option
                                                                    value="<?= $dep->department_id ?>"><?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('designation') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select id="x3" name="new_sect" class="form-control" required="">
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1 spinner_sects"><i
                                                        class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('employee_cat') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="new_emp_cat" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php if (!empty(@$emp_cat_list)): ?>
                                                        <?php foreach (@$emp_cat_list as $emp_cat): ?>
                                                            <option
                                                                    value="<?= $emp_cat->id ?>"><?= ($lang == 'english') ? $emp_cat->name_en : $emp_cat->name_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('job_time') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="new_jtme" class="form-control" required="">
                                                    <option value=""></option>
                                                    <option value="Full"><?= lang('Full') ?></option>
                                                    <option value="Part"><?= lang('Part') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_training //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_training">
                                    <br>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('course_name') ?></th>
                                            <th class="text-center"><?= lang('course_institute_name') ?></th>
                                            <th class="text-center"><?= lang('start_date') ?></th>
                                            <th class="text-center"><?= lang('end_date') ?></th>
                                            <th class="text-center"><?= lang('course_price') ?></th>
                                            <th class="text-center"><?= lang('notes') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (!empty(@$courses_list)): ?>
                                            <?php foreach (@$courses_list as $course): ?>
                                                <tr>
                                                    <td width="9%"><?=$course->course_name?></td>
                                                    <td><?=$course->course_institute_name?></td>
                                                    <td><?=$course->start_date?></td>
                                                    <td><?=$course->end_date?></td>
                                                    <td width="9%" class="text-center"><?=$course->course_price?></td>
                                                    <td width="30%"><?=$course->course_note?></td>
                                                </tr>
                                            <? endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td colspan="9" class="text-center"><strong><?= lang('nothing_to_display') ?></strong>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                    <br>
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_training"
                                          method="post" class="form form-horizontal" id="form7">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="name" type="text" class="form-control" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_institute') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="course_institute" type="text" class="form-control"
                                                       required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="start_date" type="text"
                                                       class="form-control hijri_datepicker" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_price') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="value" type="number" min="1" class="form-control"
                                                       required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_note') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_overtime //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_overtime">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_overtime"
                                          method="post" class="form form-horizontal" id="form8">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('extra_work_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php if (!empty(@$extra_work_list)): ?>
                                                        <?php foreach (@$extra_work_list as $ew): ?>
                                                            <option
                                                                    value="<?= $ew->extra_work_id ?>"><?= ($lang == 'english') ? $ew->title_en : $ew->title_ar; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('num_hours') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input name="other_employee_id" type="number" min="0" max="12" class="form-control" rows="6"
                                                       required=""/>
                                            </div>
                                        </div>
                                        <link rel="stylesheet" href="<?=base_url()?>asset/css/plugins/wickedpicker.min.css">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('permission_star') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="start_date" type="text" class="form-control"id="wickedpicker3" required=""
                                                       style="width: 100px; direction: ltr !important;"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('details') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_purchase //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_purchase">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_purchase"
                                          method="post" class="form form-horizontal" id="form9">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th width="5%"></th>
                                                <th width="27%" class="text-center"><?= lang('prod_name') ?></th>
                                                <th width="30%" class="text-center"><?= lang('prod_desc') ?></th>
                                                <th width="10%" class="text-center"><?= lang('prod_num') ?></th>
                                                <th width="28%" class="text-center"><?= lang('prod_note') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php for ($i = 1; $i < 6; $i++): ?>
                                                <tr>
                                                    <td class="text-center"><?= $i ?></td>
                                                    <td><input name="prod_name[]" type="text"
                                                               class="form-control" <?= ($i == 1) ? 'required' : ''; ?> />
                                                    </td>
                                                    <td><input name="prod_desc[]" type="text"
                                                               class="form-control" <?= ($i == 1) ? 'required' : ''; ?> />
                                                    </td>
                                                    <td><input name="prod_num[]" type="number" min="1"
                                                               class="form-control" <?= ($i == 1) ? 'required' : ''; ?> />
                                                    </td>
                                                    <td><input name="prod_note[]" type="text"
                                                               class="form-control" <?= ($i == 1) ? 'required' : ''; ?> />
                                                    </td>
                                                </tr>
                                            <?php endfor; ?>
                                            </tbody>
                                        </table>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-4 col-sm-4">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_maintenance //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_maintenance">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_maintenance"
                                          method="post" class="form form-horizontal" id="form10"><br>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('app_title') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="name" class="form-control" rows="6" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('msg_body') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <!--  /////////////////  la_permission //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_permission">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_permission"
                                          method="post" class="form form-horizontal" id="form12"><br>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('permission_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control" required="">
                                                    <option value=""></option>
                                                    <option value="1"><?= lang('permission_opt1') ?></option>
                                                    <option value="2"><?= lang('permission_opt2') ?></option>
                                                    <option value="3"><?= lang('permission_opt3') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('permission_time') ?>
                                                :</label>
                                            <div class="col-md-7">
                                            </div>
                                        </div>
                                        <link rel="stylesheet" href="<?=base_url()?>asset/css/plugins/wickedpicker.min.css">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('permission_star') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="start_date" type="text" class="form-control"id="wickedpicker1" required=""
                                                       style="width: 100px"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('permission_end') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="end_date" type="text" class="form-control"id="wickedpicker2" required=""
                                                       style="width: 100px"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_def_salary //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_def_salary">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_def_salary"
                                          method="post" class="form form-horizontal" id="form13"><br>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('app_title') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input name="name" class="form-control" rows="6" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_filter_dues //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_filter_dues">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_filter_dues"
                                          method="post" class="form form-horizontal" id="form4">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('allowances') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select name="type" class="form-control allowances" required=""
                                                        onchange="filter_due($(this))">
                                                    <option value=""></option>
                                                    <?php if (!empty(@$allowances_list)): ?>
                                                        <?php foreach (@$allowances_list as $al): ?>
                                                            <option
                                                                    value="<?= $al->allowance_id ?>"><?= ($lang == 'english') ? $al->allowance_title_en : $al->allowance_title_ar; ?></option>
                                                        <?php endforeach; ?>
                                                        <option value="0"><?= lang('other_dues') ?></option>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group other_dues">

                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_resignation //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_resignation">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_resignation"
                                          method="post" class="form form-horizontal" id="form14"><br>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!--  /////////////////  la_recrutement //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_recrutement">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_recrutement"
                                          method="post" class="form form-horizontal" id="form15"><br>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('course_institute') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input tye="" text name="course_institute" class="form-control"
                                                       required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('recrutement_task') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input tye="" text name="name" class="form-control"
                                                       required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('recrutement_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <select text name="type" class="form-control" required="">
                                                    <option value="1"><?= lang('internal') ?></option>
                                                    <option value="2"><?= lang('external') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('from_institute') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input tye="" text name="address_in_leave" class="form-control"
                                                       required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('going_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input tye="" text name="going_date"
                                                       class="form-control hijri_datepicker1" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('coming_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input tye="" text name="coming_date"
                                                       class="form-control hijri_datepicker2" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('duration') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-3">
                                                <input name="leave_duration" id="data-duration2" type="number" min="0" class="form-control"
                                                       style="width:100px" required="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('details') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <!--  /////////////////  la_embarkation //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_embarkation">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_embarkation"
                                          method="post" class="form form-horizontal" id="form16"><br>
                                        <div class="form-group">
                                            <label
                                                    class="col-sm-3 control-label"><?= lang('coming_date_after_vacation') ?>
                                                <span class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input tye="" text name="coming_date"
                                                       class="form-control hijri_datepicker" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control note" disabled=""
                                                       value="<?= lang('embarkation_note1') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input tye="" text name="coming_date_2"
                                                       class="form-control hijri_datepicker" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('embarkation_note2') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <?php $rest = 0; ?>
                                                <?php $rest = ($employee_detail->holiday_no + $employee_detail->old_rest) - $employee_detail->new_balance ?>
                                                <input name="leave_duration" type="number" min="0" class="form-control"
                                                       style="width:100px" required="" value="<?=$rest;?>">
                                                <?= lang('day') ?> / <?= lang('days') ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('notes') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <!--  /////////////////  la_annual_assessment //////////////////////////////////////////////////////////////////////// -->
                                <div class="tab-pane" id="la_annual_assessment">
                                    <form enctype="multipart/form-data"
                                          action="<?= base_url() ?>employee/applications/save_application/la_annual_assessment"
                                          method="post" class="form form-horizontal" id="form11"><br>
                                        <?php
                                        $current_year = explode('-', $current_date)[0];
                                        $start_year = $current_year - 10;
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('app_name') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <input type="text" name="name" class="form-control" required=""/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('reason') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-7">
                                                <textarea name="note" class="form-control" rows="6"
                                                          required=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('attachment') ?></label>

                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-primary btn-file"><span class="fileinput-new"><?= lang('select_file') ?></span><span class="fileinput-exists"><?= lang('change') ?></span><input type="file" name="file"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group margin">
                                            <div class="col-sm-offset-3 col-sm-5">
                                                <button type="submit" id="sbtn"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- send_applications -->



                <!--Send applications for an employee-->
                <?php if($this->session->userdata('emp_type')!='accountant' and $this->session->userdata('emp_type')!='employee' and $this->session->userdata('emp_type')!='sec_manager'):?>
                    <!-- send_applications for employee -->
                    <div class="panel panel-primary">
                        <div class="panel-heading"
                             style="cursor: pointer">
                            <strong><?= ($lang=='english')?'Send applications for an employee':'إرسال مطلب عوضا عن موظف'; ?> </strong>
                        </div>
                        <div class="panel-body employees-list">
                            <?php if(!empty($department_list)):?>
                                <?php $n=0; ?>
                                <?php foreach ($department_list as $dep): ?>
                                    <div class="col-sm-4">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading"  onclick="$(this).next('.panel-body').slideToggle()"
                                                 style="cursor: pointer">
                                                <?=($lang=='arabic')?$dep->department_name_ar:$dep->department_name;?></div>
                                            <div class="panel-body">
                                                <?php if($this->session->userdata('emp_type')=='hr_manager' or $this->session->userdata('emp_type')=='super_manager'):?>
                                                    <?php foreach(@$employees_list as $emp): ?>
                                                        <?php if($emp->departement_id==$dep->department_id): ?>
                                                            <a target="_blank" href="<?=base_url()?>employee/applications/send_applications/<?=$emp->employee_id?>"><h5 style="cursor:pointer"><?=($lang=='english')?$emp->full_name_en:$emp->full_name_ar;?></h5></a>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php else: ?>
                                                    <?php foreach(@$employees_list as $emp): ?>
                                                        <?php if($emp->departement_id==$dep->department_id and $dep->employee_id==$employee_detail->employee_id): ?>
                                                            <a target="_blank" href="<?=base_url()?>employee/applications/send_applications/<?=$emp->employee_id?>"><h5 style="cursor:pointer"><?=($lang=='english')?$emp->full_name_en:$emp->full_name_ar;?></h5></a>
                                                        <?php endif;?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $n++;?>
                                    <?php if($n%3==0) echo'<div class="clearfix"></div>';?>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endif; ?>
                <!--Send applications for an employee-->

            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(function () {
        var jplc = <?= $employee_detail->job_place_id ?>;
        var dep = <?= $employee_detail->department_id ?>;
        var sect =<?= $employee_detail->designations_id ?>;
        var codes = ['0/0/0', jplc + '/0/0', jplc + '/' + dep + '/0', '0/' + dep + '/' + sect,'0/' + dep + '/0'];
        var allowed_apps = [];

        $('.code').map(function () {
            if ($.inArray($(this).val().toString(), codes) != -1) {
                var arry1 = $(this).closest('tr').find('td.linked-apps').attr('data-apps').split(',');
                $.each(arry1, function (i, v) {
                    allowed_apps.push(v.split('/')[0]);
                });
                //alert('if');
            } else {
                $(this).closest('tr').css('background', '#EEEEEE');
                //alert('else');
            }
        });
        console.log(allowed_apps);
        $('.nav.nav-tabs.apps').find('li').map(function () {
            var data_help = $(this).attr('data-help');
            if ($.inArray(data_help, allowed_apps) == -1) {
                $(this).find('a').addClass('not-allowed');
                $('#' + data_help).find('input, select, textarea, button').prop('disabled', 'true');
            }
        });
        $('.tab-content').find('.tab-pane').map(function () {
            var id = $(this).attr('id');
            $('.approval-name').map(function () {
                var data_apps = $(this).closest('tr').find('td.linked-apps').attr('data-apps');
                if (data_apps.indexOf(id) >= 0) {
                    var clone = $(this).closest('tr').find('td.approval-name .data-id').clone();
                    $('.tab-content').find('.tab-pane#' + id).find('form').append(clone);
                    /* for vacations  */
                    if (id == 'la_vacations') {
                        var arr = data_apps.split(',');
                        if ($.inArray("la_vacations/0", arr) != -1) {
                            $('.leaves_cat').find('option').map(function () {
                                if ($.inArray($(this).val(), arr4) != -1)
                                    $(this).prop('disabled', false);
                            });
                            return false;
                        }
                        console.log(arr);
                        var arr2 = [], arr3 = [], arr4 = [];
                        $.each(arr, function (i, v) {
                            if (v.split('/')[0] == "la_vacations")
                                arr2.push(v.split('/')[1]);
                        });
                        $('.leaves_cat').find('option').map(function () {
                            arr3.push($(this).val());
                        });
                        $.each(arr3, function (i, e) {
                            if ($.inArray(e, arr2) == -1)
                                arr4.push(e);
                        });
                        $('.leaves_cat').find('option').map(function () {
                            if ($.inArray($(this).val(), arr4) != -1)
                                $(this).prop('disabled', true);
                        });
                        console.log('arr2 ' + arr2);
                        console.log('arr3 ' + arr3);
                        console.log('arr4 ' + arr4);
                    }
                    /* for advance */
                    if (id == 'la_advance') {
                        var arr = data_apps.split(',');
                        if ($.inArray("la_advance/0", arr) != -1) {
                            $('.advances_list').find('option').map(function () {
                                if ($.inArray($(this).val(), arr4) != -1)
                                    $(this).prop('disabled', false);
                            });
                            return false;
                        }
                        console.log(arr);
                        var arr2 = [], arr3 = [], arr4 = [];
                        $.each(arr, function (i, v) {
                            if (v.split('/')[0] == "la_advance")
                                arr2.push(v.split('/')[1]);
                        });
                        $('.advances_list').find('option').map(function () {
                            arr3.push($(this).val());
                        });
                        $.each(arr3, function (i, e) {
                            if ($.inArray(e, arr2) == -1)
                                arr4.push(e);
                        });
                        $('.advances_list').find('option').map(function () {
                            if ($.inArray($(this).val(), arr4) != -1)
                                $(this).prop('disabled', true);
                        });
                        console.log('arr2 ' + arr2);
                        console.log('arr3 ' + arr3);
                        console.log('arr4 ' + arr4);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">

    $(function () {
        $('.hijri_datepicker').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
        $('.hijri_datepickerx').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate2();
        });
        $('.hijri_datepickery').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate2();
        });
        $('.hijri_datepicker1').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate3();
        });
        $('.hijri_datepicker2').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true}).on("changeDate", function(e) {
            validatedate3();
        });
    });</script>

<script type="text/javascript">
    function get_duration(s) {
        if (s.val()) {
            if (s.val() == '1') {
                $('.note1').fadeIn();
                $('.note2').fadeIn();
            }
            else {
                $('.note1').fadeOut();
                $('.note2').fadeOut();
            }
            var duration = s.find(':selected').attr('data-duration');
            if(s.val()!=1)
                $('.duration').html('<label class="col-sm-3 control-label"><?= lang("leave_max_duration") ?></label><div class="col-md-7"><input disabled type="text" class="form-control"  style="width:100px" value="' + duration + '"></div></div>');
            else
                $('.duration').html('<label class="col-sm-3 control-label"><?= lang("leave_max_duration") ?></label><div clas="col-md-7"><input disabled type="text" class="form-control"  style="width:100px" value="<?=@$xrest;?>"></div></div>');
        } else {
            $('.duration').html('');
        }
    }
    function filter_due(s) {
        var val = s.val();
        if (val) {
            if (val == 0)
                $('.other_dues').html('<label class="col-sm-3 control-label "><?= lang('name') ?> <span class="required"> *</span></label>'
                    + '<div class="col-md-7">'
                    + '<input name="name" type="text" class="form-control " reuired=""  placeholder="<?= lang('other_dues_placeholder') ?>"/>'
                    + '</div>');
            else
                $('.other_dues').empty();
        } else {
            $('.other_dues').empty();
        }
    }
    var lang = "<?= $this->session->userdata('lang'); ?>";
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>employee/applications/get_sec_with_dep/" + id;
        if (id) {
            $('.spinner_sects').fadeIn('fast');

            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x3').empty();
                    $('#x3').append('<option value=""></option>');
                    $('#x3').append('<option value="0"><?=lang("without_section")?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
            });
        } else {
            $('#x3').empty();
            $('#x3').append('<option value=""></option>');
        }

    }
</script>

<script type="text/javascript">
    $(function () {
        $('form').map(function () {
            $(this).validate({rules: {name: "required"}});
            $(this).append('<input type="hidden" name="employee_id" value="<?= $employee_detail->employee_id ?>"/>')
        });
        $('.advance-value').on('change textInput input', function (){
            if($(this).val()){
                $('.payement-method').attr('disabled', false);
                $('.monthly-installement').attr('disabled', false);
                payementmethod($('.payement-method'));
            }
            else{
                $('.payement-method').attr('disabled', true);
                $('.monthly-installement').attr('disabled', true);
            }
        });
        $('.monthly-installement').on('change textInput input', function (){
            pm = $('.payement-method').val();
            av = $('.advance-value').val();
            mi = $(this).val();

            if(!mi || mi==0 || !$.isNumeric(mi)){
                $('.payement-months').val(0);
                $('.monthly-installement').val(0);
                return;
            }
            if(pm==3){
                if(av<eval(mi)) {
                    $('.monthly-installement').val(0);
                }
                else{
                    var nummonths = Math.ceil(av/parseFloat(mi, 10));
                    $('.payement-months').val(nummonths);
                }
            }

        });
        $('#data-duration').on('change', function (){
            validatedate();
        });
        $('#data-duration2').on('change', function (){
            validatedate4();
        });
    });
</script>

<script type="text/javascript">
    function payementmethod(s) {
        pm = s.val();
        av = $('.advance-value').val();
        if (pm==1) {
            var res =av/12;
            $('.monthly-installement').val(res.toFixed(2));
            $('.payement-months').val(12);
            $('.monthly-installement').attr('readonly', true);
        }
        else if (pm==2) {
            $('.monthly-installement').attr('readonly', false);
            <?php
            $date1 = new DateTime($today);
            $date2 = new DateTime(@$employee_detail->retirement_date);
            $months = ceil($date2->diff($date1)->format("%a")/30);
            ?>
            var nummonths = <?=$months?>;
            var res =av/nummonths;
            $('.monthly-installement').val(res.toFixed(2));
            $('.payement-months').val(nummonths);
        }
        else if (pm==3){
            if($('.monthly-installement').val()!=0){
                var res = av/$('.monthly-installement').val();
                $('.payement-months').val(Math.ceil(res));
            }
            else{
                $('.payement-months').val(0);
            }
            $('.monthly-installement').attr('readonly', false);
        }
        else{
            $('.monthly-installement').val();
            $('.payement-months').val(0);
            $('.monthly-installement').attr('readonly', false);
        }
    }


    function validatedate() {
        dategoing = $('.hijri_datepickerx').val();
        datecoming = $('.hijri_datepickery').val();
        duration = $('#data-duration').val();

        if(dategoing && datecoming && duration){
            dategoing = HijriToGreg(dategoing);
            datecoming = HijriToGreg(datecoming);
            dateDiffInDays(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }
    function validatedate2() {
        dategoing = $('.hijri_datepickerx').val();
        datecoming = $('.hijri_datepickery').val();
        duration = $('#data-duration').val();

        if(dategoing && datecoming){
            dategoing = HijriToGreg(dategoing);
            datecoming = HijriToGreg(datecoming);
            dateDiffInDays(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    function dateDiffInDays(a, b) {
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        res = Math.floor((utc2 - utc1) / _MS_PER_DAY)+1;

        if(res<0){
            $('.hijri_datepickery').val('');
            $('#data-duration').val('');
        }
        else{
            $('#data-duration').val(res);
        }
    }
    function intPart(floatNum){
        if (floatNum< -0.0000001){
            return Math.ceil(floatNum-0.0000001);
        }
        return Math.floor(floatNum+0.0000001);
    }
    function HijriToGreg(arabicDate) {
        return arabicDate;
        if(arabicDate == ""){
            return "";
        }
        //declare a date format year,month,day sequence
        var jd;
        var jd1;
        var l;
        var j;
        var n;
        var wd;
        var i;
        var k;
        arabicDate = arabicDate.split("-");
        var d = parseInt(arabicDate[2]);
        var m = parseInt(arabicDate[1]);
        var y = parseInt(arabicDate[0]);

        //var delta = 0;//its load from config.js
        var english_date = new Array(0,0,0);
        var delta = 1;
        //added delta=1 on jd to comply isna rulling for hajj 2007

        //delta = delta_array_hijri[arabicDate[1] - 1];


        jd = intPart((11*y+3)/30)+354*y+30*m-intPart((m-1)/2)+d+1948440-385-delta;
        //arg.JD.value=jd
        //wd = weekDay(jd % 7); // no use of this line

        if (jd > 2299160 )
        {
            l = jd + 68569;
            n = intPart((4*l)/146097);
            l = l - intPart((146097*n+3)/4);
            i = intPart((4000*(l+1))/1461001);
            l = l-intPart((1461*i)/4)+31;
            j = intPart((80*l)/2447);
            d = l-intPart((2447*j)/80);
            l = intPart(j/11);
            m = j+2-12*l;
            y = 100*(n-49)+i+l;
        }
        else
        {
            j = jd+1402;
            k = intPart((j-1)/1461);
            l = j-1461*k;
            n = intPart((l-1)/365)-intPart(l/1461);
            i = l-365*n+30;
            j = intPart((80*i)/2447);
            d = i-intPart((2447*j)/80);
            i = intPart(j/11);
            m = j+2-12*i;
            y= 4*k+n+i-4716;
        }

        english_date[2] = d;
        english_date[1] = m;
        english_date[0] = y;

        return y+'-'+m+'-'+d;

    }

    function validatedate3() {
        dategoing = $('.hijri_datepicker1').val();
        datecoming = $('.hijri_datepicker2').val();
        duration = $('#data-duration2').val();

        if(dategoing && datecoming){
            dategoing = HijriToGreg(dategoing);
            datecoming = HijriToGreg(datecoming);
            dateDiffInDays2(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }

    function validatedate4() {
        dategoing = $('.hijri_datepicker1').val();
        datecoming = $('.hijri_datepicker2').val();
        duration = $('#data-duration2').val();

        if(dategoing && datecoming && duration){
            dategoing = HijriToGreg(dategoing);
            datecoming = HijriToGreg(datecoming);
            dateDiffInDays2(new Date(dategoing.split('-')[0],dategoing.split('-')[1],dategoing.split('-')[2]),
                new Date(datecoming.split('-')[0],datecoming.split('-')[1],datecoming.split('-')[2]));
        }
    }

    function dateDiffInDays2(a, b) {
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        res = Math.floor((utc2 - utc1) / _MS_PER_DAY)+1;
        if(res<0){
            $('.hijri_datepicker2').val('');
            $('#data-duration2').val('');
        }
        else{
            $('#data-duration2').val(res);
        }
    }

    function toogleskip() {
        $('.skippable').slideToggle();
    }
</script>

<?php if($lang=='english'):?>
    <script type="text/javascript" src="<?=base_url()?>asset/js/plugins/wickedpicker.js"></script>
<?php else: ?>
    <script type="text/javascript" src="<?=base_url()?>asset/js/plugins/wickedpicker-ar.js"></script>
<?php endif; ?>
<script type="text/javascript">
    $('#wickedpicker1').wickedpicker({title: <?=($lang=='english')?'"choose time"':'"إختر الوقت"';?>});
    $('#wickedpicker2').wickedpicker({title: <?=($lang=='english')?'"choose time"':'"إختر الوقت"';?>});
    $('#wickedpicker3').wickedpicker({title: <?=($lang=='english')?'"choose time"':'"إختر الوقت"';?>});
</script>
