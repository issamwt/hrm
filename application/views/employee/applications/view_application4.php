<style type="text/css">
    .ids {
        border: 2px solid darkslategray;
    }

    .ids.current {
        border: 2px solid red;
        border-style: double;
    }

    .table-container {
        padding: 20px;
        padding-top: 10px;
    }

    h4 {
        color: #fff;
        background: #337AB7;
        padding: 5px 0px;
    }

    .iframe {
        width: 100%
    }

    .iframe-container {
        margin: 10px auto;
        /*display: none;*/
    }

    .iframe-container .iframe-buttons {
        margin: 10px auto;
        display: block;
        width: 568px;
        text-align: center;
    }

    .iframe iframe {
        margin: 10px auto;
        display: block;
        min-width: 568px;
        min-height: 400px
    }

    .appro_consultation, .appro_delegate {
        margin: 10px auto;
        display: none;
        width: 50%;
    }

    .app-reason {
        margin: 10px auto;
        width: 50%;
    }
</style>
<script src="http://localhost/hrm_lite/asset/js/custom-validation.js" type="text/javascript"></script>
<?php if ($this->session->userdata('lang') == 'arabic'): ?>
    <script src="<?php echo base_url(); ?>asset/js/jquery.validate.ar.js" type="text/javascript"></script>
<?php else: ?>
    <script src="<?php echo base_url(); ?>asset/js/jquery.validate.js" type="text/javascript"></script>
<?php endif; ?>
<div class="col-md-12">
    <div class="box box-primary">

        <div class="table-container">

            <!--  /////////////////  la_advance //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_advance"): ?>
            <h4 class="text-center"><?= lang('la_advance') ?></h4>
            <form action="<?= base_url() ?>employee/applications/update_advance_app/<?= @$view_application->application_id ?>"
                  method="post">
                <table class="table  table-non-bordered">
                    <tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('advance_type') ?></td>
                        <td>
                            <?php foreach (@$advances_list as $al): ?>
                                <?php if ($al->advance_id == @$view_application->type): ?>
                                    <?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('advance_value') ?></td>
                        <td><input  type="number" step="0.01" name="value" value="<?= @$view_application->value ?>"/></td>
                    </tr>
                    <tr>
                        <td><?= lang('payement_method') ?></td>
                        <td>
                            <select name="payement_method" onchange="payementmethod2($(this))"
                                    class="payement-method">
                                <option value="1" <?= (@$view_application->payement_method == 1) ? 'selected' : ''; ?>>
                                    <?= lang('payement_method1') ?>
                                </option>
                                <?php if (@$view_application->payement_method == 2): ?>
                                    <option value="2" selected>
                                        <?= lang('payement_method2') ?>
                                    </option>
                                <?php endif; ?>
                                <option value="3"<?= (@$view_application->payement_method == 3) ? 'selected' : ''; ?>>
                                    <?= lang('payement_method3') ?>
                                </option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('monthly_installement') ?></td>
                        <td><input type="number" step="0.01" name="monthly_installement"
                                   value="<?= @$view_application->monthly_installement ?>"/></td>
                    </tr>

                    <tr>
                        <td><?= lang('payement_months') ?></td>
                        <td><input type="number" name="payement_months"
                                   value="<?= @$view_application->payement_months ?>"/></td>
                    </tr>
                    <tr>
                        <td><?= lang('voucher') ?></td>
                        <td>
                            <?php if (@$view_application->other_employee_id == 0): ?>
                                <?= lang('no-exist') ?>
                            <?php else: ?>
                                <?php foreach (@$employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == @$view_application->other_employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('phone') ?></td>
                        <td><?= @$view_application->phone ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file != null) ? '<a target="_blank" href="' . base_url() . 'img/uploads/' . @$view_application->file . '">' . lang("download") . '</a>' : lang('no-exist'); ?></td>
                    </tr>
                    <tr>
                        <td width="30%"></td>
                        <td>
                            <button type="submit" class="btn btn-primary btn-xs"
                                    style="padding: 0px 15px;"><?= lang('update') ?></button>
                        </td>
                    </tr>
                </table>
                <?php endif; ?>


        </div>


    </div>
</div>





