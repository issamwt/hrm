<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
    <div class="main_content">
        <div class="row">
            <div class="col-md-12">
                <!-- applications_list -->
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong><?= lang('applications_list') ?> </strong>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="dataTables-example">
                            <thead>
                            <tr>
                                <th class="text-center"><?= lang('id_app') ?></th>
                                <th class="text-center"><?= lang('date_app') ?></th>
                                <th class="text-center"><?= lang('employee_id') ?></th>
                                <th class="text-center"><?= lang('app_type') ?></th>
                                <th class="text-center"><?= lang('configure_approval') ?></th>
                                <th class="text-center"><?= lang('status') ?></th>
                                <th class="text-center"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($applications_list)): ?>
                                <?php foreach ($applications_list as $app): ?>
                                    <tr>
                                        <td class="text-center"><?= $app->application_id ?></td>
                                        <td class="relative" style="vertical-align: bottom; padding-bottom: 3px;"><?= $app->date ?><?= hijjri($app->date); ?></td>
                                        <td><?= $app->employment_id ?></td>
                                        <td><?= lang($app->type_app) ?></td>
                                        <td><?= ($lang == 'english') ? $app->approval_cat_en : $app->approval_cat_ar; ?></td>
                                        <td class="text-center"><?= status($app->status) ?></td>
                                        <td class="text-center">
                                            <?= anchor('employee/applications/view_application/' . $app->application_id, '<span class="fa fa-list-alt"></span> ' . lang('view'), array('class' => "btn btn-primary btn-xs", 'title' => lang('view'), 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-toggle' => 'modal', 'data-target' => '#myModal')); ?>
                                            <?php if ($app->status == 0): ?>
                                                <?php
                                                $f = explode(";", $app->ids);
                                                $u = explode(";", $app->results);
                                                $c = array_search(@$this->direct_manager["direct_manager_id"], $f);
                                                ?>
                                                <?php if($u[@$c]==0):?>
                                                    <?php $cancel = ($lang=="arabic")?"تراجع":"Cancel"; ?>
                                                    <?php $confirm = ($lang=="arabic")?"هل أنت متأكد ؟":"Are you sure ?"; ?>
                                                    <?= anchor('employee/applications/delete_application2/' . $app->application_id, '<span class="fa fa-times"></span> ' .$cancel , array('class' => "btn btn-danger btn-xs", 'title' => $cancel, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'onclick'=>'return confirm(\''.$confirm.'\')')); ?>
                                                <?php endif;?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
<?php

function status($x)
{
    switch ($x) {
        case '0':
            echo '<span class="label label-warning">' . lang('pending') . '</span>';
            break;
        case '1':
            echo '<span class="label label-success">' . lang('accepted') . '</span>';
            break;
        case '2':
            echo '<span class="label label-danger">' . lang('rejected') . '</span>';
            break;
        default:
            echo '?';
            break;
    }
}

?>