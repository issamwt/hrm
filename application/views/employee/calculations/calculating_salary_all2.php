<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
    $(function () {
        $('.calculating-salary').closest('.container').attr('class', 'container-fluid');
    });
</script>

<style type="text/css">
    .btn {
        margin-bottom: 3px;
    }

    .print {
        border: 1px solid gray;
        min-height: 200px;
        margin-bottom: 100px;
        padding: 10px;
        padding-bottom: 100px;
        position: relative;
    }

    .btn-print {
        position: absolute;
        top: 0px;
        left: 0px;
    }

    .print .boxino {
        border: 1px solid black;
        border-radius: 6px;
        float: right;
        min-width: 300px;
        padding: 10px 15px;
    <?php if($lang == 'english'):?> float: left;
    <?php else: ?> float: right;
    <?php endif; ?>
    }

    .btn-pr1 {
        background: #999;
        borde: 1px solid #777;
        color: #fff;
    }

    .btn-pr2 {
        background: #777;
        borde: 1px solid #777;
        color: #fff;
    }

    .print h5 {
        font-weight: bold;
        margin: 0px;
        padding: 5px 0px;
    }

    .btn-excel {
        position: absolute;
        top: 0px;
        left: 70px;
    }

    .print .plus {
        color: #2e6da4;
        font-weight: bold;
    }

    .print .minus {
        color: red;
        font-weight: bold;
    }

    .spinner_sects {
        display: none;
        font-size: 7px;
    }

    .the_table td, th {
        text-align: center !important;
        display: table-cell !important;
        vertical-align: middle !important;
    }

    @media print {
        .print {
            font-size: 0.5em;
        }
    }

    .print {
        font-size: 0.8em;
    }

</style>

<div class="row calculating-salary">
    <div class="col-sm-12" data-spy="scroll" data-offset="0"><br><br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><?= lang('results') ?>
                    <small>(<?= lang('report_all') ?>)</small>
                </strong>
            </div>
            <div class="panel-body">
                <?php $today = strtotime($today) ?>
                <div class="print" id="printed_content">
                    <style type="text/css">@media print {
                            .no-print {
                                display: none;
                            }

                            .print {
                                font-size: 0.5em;
                                background: #000;
                            }
                        }</style>
                    <a class="btn btn-primary btn-xs btn-print no-print"
                       onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <a class="btn btn-success btn-xs btn-excel no-print"
                       href="<?= base_url() ?>employee/calculations/download_salary/<?= $month ?>/<?= $year ?>/<?= $branche ?>/<?= $department ?>/<?= $designation ?>"
                       target="_blank">
                        <i class=" fa fa-file-excel-o"></i> Excel</a>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div class="boxino">
                                    <?php if ($lang == 'english'): ?>
                                        <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                    <?php else: ?>
                                        <?php $months = array("الكل","جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                    <?php endif; ?>
                                    <b><?= lang('month') ?> : </b> <?= $months[intval($month)] ?><br>
                                    <b><?= lang('year') ?>
                                        : </b> <?= ($year == 0) ? lang('all') : $year; ?>
                                    <br>
                                    <br>
                                    <b><?= lang('branche') ?> : </b>
                                    <?php if ($branches == 0): ?>
                                        <?= lang("all") ?>
                                    <?php else: ?>
                                        <?php foreach ($branches as $brch): ?>
                                            <?php if ($brch->branche_id == $branche): ?>
                                                <?= ($lang == 'arabic') ? $brch->branche_ar : $brch->branche_en; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <br>
                                    <b><?= lang('department') ?> : </b>
                                    <?php if ($department == 0): ?>
                                        <?= lang("all") ?>
                                    <?php else: ?>
                                        <?php foreach ($departments as $dep): ?>
                                            <?php if ($dep->department_id == $department): ?>
                                                <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <br>
                                    <b><?= lang('designation') ?> : </b>
                                    <?php if ($designation == -1): ?>
                                        <?= lang("all") ?>
                                    <?php elseif ($designation == 0): ?>
                                        <?= lang("without_section") ?>
                                    <?php else: ?>
                                        <?php foreach ($designations as $des): ?>
                                            <?php if ($des->designations_id == $designation): ?>
                                                <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <br>
                                </div>
                            </td>
                            <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                            </td>
                        </tr>
                    </table>
                    <br><br>

                    <table class="table table-bordered the_table">
                        <thead>
                        <tr>
                            <th class="text-center no-print" width="2%" rowspan="2"><?= lang('sl') ?></th>
                            <th class="text-center" rowspan="2"><?= lang('name') ?></th>
                            <th class="text-center no-print" rowspan="2"><?= lang('job_title') ?></th>
                            <th class="text-center no-print" rowspan="2"><?= lang('job_time') ?></th>
                            <th class="text-center no-print" rowspan="2"><?= lang('abscence_days') ?></th>
                            <th class="text-center no-print" rowspan="2"><?= lang('total_extra_hours') ?></th>
                            <th class="text-center" colspan="5"><?= lang('merits') ?></th>
                            <th class="text-center" rowspan="2"><?= lang('total_merits') ?></th>
                            <th class="text-center" colspan="6"><?= lang('deductions') ?></th>
                            <th class="text-center" rowspan="2"><?= lang('total_deductions') ?></th>
                            <th class="text-center" rowspan="2"><?= lang('net_merits') ?></th>
                        </tr>
                        <tr>
                            <th class="text-center"><?= lang('finance_basic_salary') ?></th>
                            <th class="text-center"><?= lang('transportation_allowance') ?></th>
                            <th class="text-center"><?= lang('finance_provision') ?></th>
                            <th class="text-center"><?= lang('value_extra_hours') ?></th>
                            <th class="text-center"><?= lang('house_allowance') ?></th>
                            <th class="text-center"><?= lang('abscence') ?></th>
                            <th class="text-center"><?= lang('fieldset_med') ?></th>
                            <th class="text-center"><?= lang('fieldset_social') ?></th>
                            <th class="text-center"><?= lang('finance_permanent_deduction') ?></th>
                            <th class="text-center"><?= lang('leaves') ?></th>
                            <th class="text-center"><?= lang('advances_app') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $n = 0; ?>
                        <?php $total_salary = 0; ?>
                        <?php if (!empty($employee_list)): ?>
                            <?php foreach ($employee_list as $emp):$n++; ?>
                                <tr>
                                    <td class="text-center no-print"><?= $n; ?></td>
                                    <td class="text-center"><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                    <td class="no-print text-center">
                                        <?php foreach ($job_titles as $jt): ?>
                                            <?php if ($jt->job_titles_id == $emp->job_title): ?>
                                                <?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="text-center no-print"><?= ($emp->job_time == "Full") ? lang('job_full') : lang('job_part'); ?></td>
                                    <td class="no-print">
                                        <? $presence = 0; ?>
                                        <? $presence_xh = 0; ?>
                                        <? $presence_pr = 0; ?>
                                        <?php for ($i = 1; $i <= $num_days; $i++): ?>
                                            <? $day = $year . '/' . $month . '/' . sprintf('%02d', $i); ?>
                                            <!---------------------------------->
                                            <? $total = 0; ?>
                                            <? $in = ""; ?>
                                            <? $out = ""; ?>
                                            <? $blocks = array(); ?>
                                            <?php foreach ($attendances as $att): ?>
                                                <?php if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day): ?>
                                                    <?php if ($att->Action == "1"): ?>
                                                        <?php $in = $att->att_time; ?>
                                                    <?php else: ?>
                                                        <?php $out = $att->att_time; ?>
                                                        <?php if ($in != "" and $out != ""): ?>
                                                            <?php $in_time = strtotime($in);
                                                            $out_time = strtotime($out);
                                                            $diff = round(abs($in_time - $out_time) / 60); ?>
                                                            <?php array_push($blocks, [$in, $out, $diff]); ?>
                                                            <?php $in = "";
                                                            $out = ""; ?>
                                                        <? endif; ?>
                                                    <? endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <? if (!empty($blocks)): ?>
                                                <? foreach ($blocks as $block): ?>
                                                    <? $total += $block[2]; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <!---------------------------------->
                                            <? $total_xh = 0; ?>
                                            <? $in_xh = ""; ?>
                                            <? $out_xh = ""; ?>
                                            <? $blocks_xh = array(); ?>
                                            <? foreach ($attendances_xh as $att_xh): ?>
                                                <? if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day): ?>
                                                    <? if ($att_xh->Action == "1"): ?>
                                                        <? $in_xh = $att_xh->att_time; ?>
                                                    <? else: ?>
                                                        <? $out_xh = $att_xh->att_time; ?>
                                                        <? if ($in_xh != "" and $out_xh != ""): ?>
                                                            <? $in_time = strtotime($in_xh);
                                                            $out_time = strtotime($out_xh);
                                                            $diff_xh = round(abs($in_time - $out_time) / 60); ?>
                                                            <? array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]); ?>
                                                            <? $in_xh = "";
                                                            $out_xh = ""; ?>
                                                        <? endif; ?>
                                                    <? endif; ?>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                            <? if (!empty($blocks_xh)): ?>
                                                <? foreach ($blocks_xh as $block_xh): ?>
                                                    <? $total_xh += $block_xh[2]; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <!---------------------------------->
                                            <? $total_pr = 0; ?>
                                            <?php foreach ($tbl_permissions as $pr): ?>
                                                <?php if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day): ?>
                                                    <?php $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60); ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <!---------------------------------->
                                            <? if ($total != 0): ?>
                                                <? $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                            <? if ($total_xh != 0): ?>
                                                <? $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                            <? if ($total_pr != 0): ?>
                                                <? $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                        <?php endfor; ?>
                                        <?php $plus_1 = 0; ?>
                                        <?php $minus_1 = 0; ?>
                                        <?php if ($emp->job_time == 'Part' and $emp->job_time_hours != 0): ?>
                                            <?php $mn = ($num_days - $num_days_off) * $emp->job_time_hours; ?>
                                        <?php else: ?>
                                            <?php $mn = ($num_days - $num_days_off) * $job_time_hours; ?>
                                        <?php endif; ?>
                                        <?php $mn = ($mn * 3600); ?>
                                        <?php $abs = $mn - $presence - $presence_pr; ?>
                                        <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00' ?>
                                    </td>
                                    <td class="no-print">
                                        <?= ((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00' ?>
                                    </td>
                                    <td><span class="plus"><?= $emp->employee_salary ?></span></td>
                                    <td>
                                        <?php $plus_2 = 0; ?>
                                        <?php if ($emp->job_time == "Full"): ?>
                                            <?php if (!empty(@$allowances_list)): ?>
                                                <?php foreach (@$allowances_list as $al): ?>
                                                    <?php if ($al->employe_id == $emp->employee_id and $al->allowance_id == 2): ?>
                                                        <?php if (dates($month, $year, $al->allowance_date_from, $al->allowance_date_to)): ?>
                                                            <?php $from = strtotime($al->allowance_date_from); ?>
                                                            <?php $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30"); ?>

                                                            <?php if ($al->allowance_type == 'value'): ?>
                                                                <?php $val = ($al->allowance_value / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <?php $plus_2 += $val; ?>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <?php $plus_2 += $val; ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <span class="plus"><?= $plus_2 ?></span>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <span class="plus"><?= $plus_2 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php $plus_3 = 0; ?>
                                        <?php if (!empty(@$provision_list)): ?>
                                            <?php foreach (@$provision_list as $p): ?>
                                                <?php if ($p->employp_id == $emp->employee_id): ?>
                                                    <?php if (dates($month, $year, $p->start_date, $p->end_date)): ?>
                                                        <?php $plus_3 += $p->provision_value ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <span class="plus"><?= $plus_3 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php $mn_value = (int)($mn / 60); ?>
                                        <?php $vh = round($emp->employee_salary / $mn_value * 60, 2); ?>
                                        <?php $vmn = round($emp->employee_salary / $mn_value, 5); ?>
                                        <?php $exh = ($vh / 100) * $emp->extra_hours_value; ?>
                                        <?php $exmn = ($vmn / 100) * $emp->extra_hours_value; ?>
                                        <?php $plus_1 = round((int)($presence_xh / 60) * $exmn, 0) ?>
                                        <span class="plus"><?= $plus_1 ?></span>
                                    </td>
                                    <td>
                                        <?php $plus_4 = 0; ?>
                                        <?php if ($emp->job_time == "Full"): ?>
                                            <?php if (!empty(@$allowances_list)): ?>
                                                <?php foreach (@$allowances_list as $al): ?>
                                                    <?php if ($al->employe_id == $emp->employee_id and $al->allowance_id == 1): ?>
                                                        <?php if (dates($month, $year, $al->allowance_date_from, $al->allowance_date_to)): ?>
                                                            <?php $from = strtotime($al->allowance_date_from); ?>
                                                            <?php $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30"); ?>

                                                            <?php if ($al->allowance_type == 'value'): ?>
                                                                <?php $val = ($al->allowance_value / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <?php $plus_4 += $val; ?>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <?php $plus_4 += $val; ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <span class="plus"><?= $plus_4 ?></span>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <span class="plus"><?= $plus_4 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <span class="plus"><?= ($x = $emp->employee_salary + $plus_1 + $plus_2 + $plus_3 + $plus_4) ?></span>
                                    </td>
                                    <td>
                                        <span class="minus"><?= $minus_1 = round($abs / 60 * $vmn, 0) ?></span>
                                    </td>
                                    <td>
                                        <?php $minus_2 = 0; ?>
                                        <?php if ($emp->med_insur != 1): ?>
                                            <span class="minus"><?= $minus_2 ?></span>
                                        <?php else: ?>
                                            <?php $minus_2 = ($emp->employee_salary / 100) * @$emp->m_insurance_percent; ?>
                                            <span class="minus"><?= $minus_2 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php $minus_3 = 0; ?>
                                        <?php if ($emp->social_insurance != 1): ?>
                                            <span class="minus"><?= $minus_3 ?></span>
                                        <?php else: ?>
                                            <?php
                                            $id = 2;
                                            switch ($emp->social_insurance_type) {
                                                case 'saudi1':
                                                    $id = 1;
                                                    break;
                                                case 'saudi2':
                                                    $id = 2;
                                                    break;
                                                case 'non-saudi':
                                                    $id = 3;
                                                    break;
                                            }
                                            ?>
                                            <?php if($socials[$id-1]->habit_insurance==1): ?>
                                                <?php $minus_3 = (($emp->employee_salary+$plus_4) / 100) * @$emp->insurance_percent ?>
                                                <?php else: ?>
                                                <?php $minus_3 = ($emp->employee_salary / 100) * @$emp->insurance_percent ?>
                                            <?php endif; ?>

                                            <span class="minus"><?= $minus_3 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php $minus_4 = 0; ?>
                                        <?php if (!empty(@$deduction_list)): ?>
                                            <?php foreach (@$deduction_list as $d): ?>
                                                <?php if ($d->employd_id == $emp->employee_id): ?>
                                                    <?php if (dates($month, $year, $d->start_date, $d->end_date)): ?>
                                                        <?php
                                                        if ($d->deduction_type == 'percent') {
                                                            $val = ($emp->employee_salary / 100) * @$d->deduction_value;
                                                            $minus_4 += $val;
                                                        } else {
                                                            $val = @$d->deduction_value;
                                                            $minus_4 += $val;
                                                        }
                                                        ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <span class="minus"><?= $minus_4 ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php $minus_5 = 0; ?>
                                        <?php foreach (@$leaves_list as $leave): ?>
                                            <?php if ($leave->employel_id == $emp->employee_id): ?>
                                                <?php $value = ($emp->employee_salary / 100) * $leave->quota ?>
                                                <?php $minus_5 += $value; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <span class="minus"><?= $minus_5 ?></span>
                                    </td>
                                    <td>
                                        <?php $minus_6 = 0; ?>
                                        <?php if (!empty(@$advances_list)): ?>
                                            <?php foreach (@$advances_list as $adv): ?>
                                                <?php if ($adv->employea_id == $emp->employee_id): ?>
                                                    <?php $valuee = $adv->monthly_installement ?><?php $minus_6 += $valuee; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <span class="minus"><?= $minus_6 ?></span>
                                    </td>
                                    <td>
                                        <span class="minus"><?= ($y = $minus_1 + $minus_2 + $minus_3 + $minus_4 + $minus_5 + $minus_6) ?></span>
                                    </td>
                                    <td>
                                        <span style="color: green; font-weight: bold"><?php $total_salary+=round($x - $y, 1);?><?=round($x - $y, 1) ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td colspan="19" class="text-center"><b><strong><?=($lang == 'english')?'Total salaries':'مجموع الرواتب'; ?></strong></b></td>
                                <td>
                                    <span style="color: green; font-weight: bold" class="text-center"><b><strong><?= $total_salary; ?></strong></b></span>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td class="text-center"><h4 class="text-center"><?= lang('nothing_to_display') ?></h4>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function proccessadvance(id) {
        var result = confirm('<?=($lang == "arabic") ? "تنبيه : يجب حساب الرصيد قبل خلاص أي قسط " : "Attention: calculate the salary before paying";?>')
        if (result) {
            $('#spinner_sects' + id).fadeIn('fast');
            $.ajax({
                url: '<?=base_url()?>employee/dashboard/proccessadvance/' + id,
                success: function (data) {
                    if (data = 'true') {
                        $('#spinner_sects' + id).fadeOut('fast');
                        alert('<?=($lang == 'arabic') ? 'تم تثبيت خلاص القسط' : 'advance payement was successfully saved';?>');
                        location.reload();
                    }
                }
            }).fail(function () {
                alert('<?= lang('alert_error') ?>')
            });
        }
    }
</script>


<?php
function dates($month, $year, $start, $end)
{
    if ($end == '')
        $end = date_create_from_format('Y-m-d', "9999-12-30")->format('Y-m-d');
    $flag = false;

    $start_s = date_create_from_format('Y-m-d', $start)->format('Ym');
    $end_e = date_create_from_format('Y-m-d', $end)->format('Ym');
    $date_x = date_create_from_format('Y-m-d', $year . '-' . $month . '-01')->format('Ym');
    if ($date_x <= $end_e and $date_x >= $start_s)
        $flag = true;

    return $flag;
}
?>