<style type="text/css">
    .btno-container {
        width: 100%;
        text-align: center;
        margin: 20px 0px;
    }

    .btno {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        position: relative;
        display: inline-block;
        overflow: hidden;
        height: 53px;
        border-radius: 6px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        line-height: 30px;
        font-size: 16px;
        font-weight: bold;
        text-shadow: 0px 1px 1px #888;
        color: #fff;
    }

    .btno span.icon, .btno span.title {
        display: block;
        position: relative;
        line-height: 50px;
        padding: 0 30px;
        border-radius: 6px;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
    }

    .btno span.left {
        float: left;
        border-radius: 6px 0 0 6px;
        -moz-border-radius: 6px 0 0 6px;
        -webkit-border-radius: 6px 0 0 6px;
    }

    .btno span.right {
        float: right;
        border-radius: 0 6px 6px 0;
        -moz-border-radius: 0 6px 6px 0;
        -webkit-border-radius: 0 6px 6px 0;
    }

    .btno span.icon {
        font-size: 23px;
        background-color: #00967f;
        -webkit-box-shadow: 0 3px 0 0 #007261;
        box-shadow: 0 3px 0 0 #007261;
        text-shadow: 0px 1px 1px #888;
    }

    .btno span.title {
        -webkit-box-shadow: 0 3px 0 0 #00ae94;
        box-shadow: 0 3px 0 0 #00ae94;
        background-color: #00cdae;
    }

    .btno span.slant-left, .btno span.slant-right {
        position: absolute;
        width: 0;
        height: 0;
        border-top: 0 solid transparent;
        border-bottom: 50px solid transparent;
        -webkit-transition: all .15s;
        -transition: all .15s;
        -webkit-transition-property: left, right;
        transition-property: left, right;
    }

    .btno.right span.slant-left {
        right: 0;
        -webkit-box-shadow: 10px 0 0 0 #00967f, 10px 3px 0 0 #007261;
        box-shadow: 10px 0 0 0 #00967f, 10px 3px 0 0 #007261;
        border-right: 10px solid #00967f;
    }

    .btno:active, .btno.active {
        height: 51px;
    }

    .btno:hover span.slant-left {
        right: 10px;
    }

    .btno-small {
        height: 30px;
        font-size: 12px;
        line-height: 10px;
    }

    a.btno-small span.btno {
        height: 30px;
    }

    .btno:hover, .btno:focus, .btno:active, .btno:visited {
        color: #fff !important;
        cursor: pointer;
    }
</style>
<div class="btno-container">
    <a href="<?=base_url().$name?>" class="btno right">
    <span class="left title">
        <span class="slant-left"></span>
        <?= ($lang == 'arabic') ? 'تحميل ملف الإكسل' : 'Download Excel file' ?>
    </span>
        <span class="right icon fa fa-cloud-download"></span>
    </a>
</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="text-center" width="12%"><?= lang("employee_id") ?></th>
        <th class="text-center" width="15%"><?= lang("employee_name") ?></th>
        <th class="text-center" width="8%"><?= lang("job_time") ?></th>
        <th class="text-center" width="13%"><?= lang("total_extra_hours") ?></th>
        <th class="text-center" width="13%"><?= lang("total_attendant_hours") ?></th>
        <th class="text-center" width="13%"><?= lang("total_permissions") ?></th>
        <th class="text-center" width="13%"><?= lang("all_hours_available_in_month") ?></th>
        <th class="text-center" width="13%"><?= lang("total_abscence_in_month") ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($employee_list)): ?>
        <?php foreach ($employee_list as $emp): ?>
            <tr>
                <td class="text-center"><?= $emp->employment_id ?></td>
                <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                <td class="text-center">
                    <?php if ($emp->job_time == 'Part'): ?>
                        <?= lang('job_part') ?>
                    <?php else: ?>
                        <?= lang('job_full') ?>
                    <?php endif; ?>
                </td>
                <? $presence = 0; ?>
                <? $presence_xh = 0; ?>
                <? $presence_pr = 0; ?>
                <?php for ($i = 1; $i <= $num_days; $i++): ?>
                    <? $day = $year . '/' . $month . '/' . sprintf('%02d', $i); ?>
                    <!---------------------------------->
                    <? $total = 0; ?>
                    <? $in = ""; ?>
                    <? $out = ""; ?>
                    <? $blocks = array(); ?>
                    <?php foreach ($attendances as $att): ?>

                        <?php if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day): ?>
                            <?php if ($att->Action == "1"): ?>
                                <?php $in = $att->att_time; ?>
                            <?php else: ?>
                                <?php $out = $att->att_time; ?>
                                <?php if ($in != "" and $out != ""): ?>
                                    <?php $in_time = strtotime($in);
                                    $out_time = strtotime($out);
                                    $diff = round(abs($in_time - $out_time) / 60); ?>
                                    <?php array_push($blocks, [$in, $out, $diff]); ?>
                                    <?php $in = "";
                                    $out = ""; ?>
                                <? endif; ?>
                            <? endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <? if (!empty($blocks)): ?>
                        <? foreach ($blocks as $block): ?>
                            <? $total += $block[2]; ?>
                        <? endforeach; ?>
                    <? endif; ?>
                    <!---------------------------------->
                    <? $total_xh = 0; ?>
                    <? $in_xh = ""; ?>
                    <? $out_xh = ""; ?>
                    <? $blocks_xh = array(); ?>
                    <? foreach ($attendances_xh as $att_xh): ?>
                        <? if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day): ?>
                            <? if ($att_xh->Action == "1"): ?>
                                <? $in_xh = $att_xh->att_time; ?>
                            <? else: ?>
                                <? $out_xh = $att_xh->att_time; ?>
                                <? if ($in_xh != "" and $out_xh != ""): ?>
                                    <? $in_time = strtotime($in_xh);
                                    $out_time = strtotime($out_xh);
                                    $diff_xh = round(abs($in_time - $out_time) / 60); ?>
                                    <? array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]); ?>
                                    <? $in_xh = "";
                                    $out_xh = ""; ?>
                                <? endif; ?>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <? if (!empty($blocks_xh)): ?>
                        <? foreach ($blocks_xh as $block_xh): ?>
                            <? $total_xh += $block_xh[2]; ?>
                        <? endforeach; ?>
                    <? endif; ?>
                    <!---------------------------------->
                    <? $total_pr = 0; ?>
                    <?php foreach ($tbl_permissions as $pr): ?>
                        <?php if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day): ?>
                            <?php $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <!---------------------------------->
                    <? if ($total != 0): ?>
                        <? $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY'); ?>
                    <? endif; ?>
                    <? if ($total_xh != 0): ?>
                        <? $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY'); ?>
                    <? endif; ?>
                    <? if ($total_pr != 0): ?>
                        <? $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY'); ?>
                    <? endif; ?>
                <?php endfor; ?>
                <td class="text-center"><?= ((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00' ?></td>
                <td class="text-center"><?= ((int)($presence / 3600)) . ':' . (int)(($presence % 3600) / 60) . ':00' ?></td>
                <td class="text-center"><?= ((int)($presence_pr / 3600)) . ':' . (int)(($presence_pr % 3600) / 60) . ':00' ?></td>
                <td class="text-center">
                    <?php if($emp->job_time=='Part' and $emp->job_time_hours!=0): ?>
                        <?php $mn = ($num_days - $num_days_off) * $emp->job_time_hours; ?>
                    <?php else: ?>
                        <?php $mn = ($num_days - $num_days_off) * $job_time_hours; ?>
                    <?php endif; ?>
                    <?php $mn = ($mn * 3600); ?>
                    <?= ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00' ?>
                </td>
                <td class="text-center">
                    <?php $abs = $mn - $presence- $presence_pr; ?>
                    <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00' ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="5" class="text-center"><?= lang('nothing_to_display') ?></h4></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>

