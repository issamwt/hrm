<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .spinner_sects {
        display: none;
        font-size: 7px;
    }
    .spinner_deps {
        display: none;
        font-size: 7px;
    }
</style>

<div class="main_content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong><?= lang('calculating_salary') ?> </strong>
                </div>
                <div class="panel-body"><br>
                    <?php
                    $current_year = explode('-', $current_date)[0];
                    $start_year = $current_year - 10;
                    ?>
                    <?php $the_month = explode('-', $current_date)[1]; ?>
                    <?php $the_year = explode('-', $current_date)[0]; ?>

                    <form action="<?= base_url() ?>employee/calculations/calculating_salary1" method="post"
                          target="_blank">

                        <!-- fields -->
                        <div class="col-sm-12">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3"><?= lang('month') ?></div>
                            <div class="col-sm-3">
                                <select name="month" class="form-control  months" required=""></select>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3"><?= lang('year') ?></div>
                            <div class="col-sm-3">
                                <select class="form-control" name="year">
                                    <?php for ($i = $start_year; $i < ($start_year + 20); $i++): ?>
                                        <option
                                            value="<?= $i ?>" <?= ($i == $the_year) ? 'selected=""' : ''; ?> ><?= $i ?></option>
                                    <?php endfor; ?>
                                    <select>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3"><?= lang('branche') ?></div>
                            <div class="col-sm-3">
                                <select name="branche" class="form-control department" required=""
                                        onchange="print_deps(this.value)">
                                    <option value="0"><?= lang("all") ?></option>
                                    <?php foreach (@$branches_list as $brch): ?>
                                        <option value="<?= $brch->branche_id ?>">
                                            <?= ($lang == "arabic") ? $brch->branche_ar : $brch->branche_en; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3"><?= lang('department') ?></div>
                            <div class="col-sm-3">
                                <select  id="x2" name="department" class="form-control department" required=""
                                        onchange="print_sects(this.value)">
                                    <option value="0"><?= lang("all") ?></option>
                                    <!--
                                    <?php foreach ($department_list as $dep): ?>
                                        <option value="<?= $dep->department_id ?>">
                                            <?= ($lang == "arabic") ? $dep->department_name_ar : $dep->department_name; ?>
                                        </option>
                                    <?php endforeach; ?>
                                    -->
                                </select>
                            </div>
                            <div class="col-sm-1 spinner_deps"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3"><?= lang('designation') ?></div>
                            <div class="col-sm-3">
                                <select id="x3" name="designation" class="form-control designation" required="">
                                    <option value="-1"><?= lang("all") ?></option>
                                    <option value="0"><?= lang("without_section") ?></option>
                                </select>
                            </div>
                            <div class="col-sm-1 spinner_sects"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                        </div>
                        <br><br>
                        <div class="col-sm-12" style="margin-bottom: 50px">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6 text-center">
                                <button type="submit" id="sbtn" name="submit" value="one"
                                        class="btn btn-success btn-block"><?= lang('report_by_employee') ?></button>
                                <button type="submit" id="sbtn2" name="submit" value="all"
                                        class="btn btn-success btn-block"><?= lang('report_all') ?></button>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                        <br><br>
                        <!-- fields -->


                        <!-- table -->
                        <table class="table table-bordered" id="dataTables-example">
                            <thead>
                            <tr>
                                <th width="5%" class="text-center">
                                    <input type="checkbox" class="all ids" name="ids[]" value="0"
                                           style="cursor: pointer"/>
                                </th>
                                <th style="width:72px" class="text-center"><?= lang('image') ?></th>
                                <th class="text-center"><?= lang('name') ?></th>
                                <th class="text-center"><?= lang('department') ?></th>
                                <th class="text-center"><?= lang('designation') ?></th>
                                <th class="text-center"><?= lang('job_title') ?></th>
                                <th class="text-center"><?= lang('job_time') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty(@$employee_list)): ?>
                                <?php foreach (@$employee_list as $emp): ?>
                                    <tr>
                                        <td class="text-center" style="vertical-align: middle">
                                            <input type="checkbox" name="ids[]" class="ids"
                                                   value="<?= $emp->employee_id ?>"
                                                   style="cursor: pointer"/>
                                        </td>
                                        <td>
                                            <?php if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager' and $emp->gender == "Female"): ?>
                                                <img src="<?= base_url() ?>img/adminf.png"
                                                     style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                     alt="Please Connect Your Internet"/>
                                            <?php else: ?>
                                                <img src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>"
                                                     style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                     alt="Please Connect Your Internet">
                                            <?php endif; ?>
                                        </td>
                                        <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                        <td>
                                            <?= ($lang == 'english') ? $emp->department_name : $emp->department_name_ar; ?>
                                        </td>
                                        <td>
                                            <?php if ($emp->designations_id != '0'): ?>
                                                <?= ($lang == 'english') ? $emp->designations : $emp->designations_ar; ?>
                                            <?php else: ?>
                                                <?= lang('no-exist') ?>
                                            <?php endif; ?>
                                        </td>
                                        <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                        <td><?= ($emp->job_time == "Full") ? lang('job_full') : lang('job_part'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <!-- table -->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?php if($lang == 'english'):?>
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    <?php else: ?>
    var months = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
    <?php endif; ?>

    $(function () {
        $.each(months, function (i, v) {
            var num = pad(i + 1, 2);
            var selected = '';
            if (num == '<?=$the_month?>')
                selected = ' selected=""';
            $('.months').append('<option value="' + num + '" ' + selected + '>' + v + '</option>');
        });

    });
    function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }
    var lang = "<?= $this->session->userdata('lang'); ?>";
    function print_deps(id) {
        var vurl = "<?php echo base_url() ?>employee/dashboard/get_dep_with_brch/" + id;
        if (id) {
            $('.spinner_deps').fadeIn('fast');
            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x2').empty();
                    $('#x2').append('<option value="0"><?= lang("all") ?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x2').append('<option value="' + data[i].department_id + '" >' + data[i].department_name_ar + '</option>');
                        else
                            $('#x2').append('<option value="' + data[i].department_id + '">' + data[i].department_name + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_deps').fadeOut('fast');
            });
        } else {
            $('#x2').empty();
            $('#x2').append('<option value="0"><?=lang("all")?></option>');
        }
    }
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>employee/dashboard/get_sec_with_dep/" + id;
        if (id) {
            $('.spinner_sects').fadeIn('fast');
            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x3').empty();
                    $('#x3').append('<option value="-1"><?=lang("all")?></option>');
                    $('#x3').append('<option value="0"><?= lang("without_section") ?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '" >' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
            });
        } else {
            $('#x3').empty();
            $('#x3').append('<option value="0"><?=lang("all")?></option>');
        }
    }
</script>
<script type="text/javascript">
    $(function () {
        $('.all').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.ids').map(function () {
                    $(this).prop('checked', true);
                });
            }
            else {
                $('.ids').map(function () {
                    $(this).prop('checked', false);
                });
            }
        });
        $("#sbtn").click(function () {
            var flag = false;
            $('.ids').map(function () {
                if ($(this).is(':checked'))
                    flag = true;
            });
            if (!flag) {
                alert('<?=lang("report_error")?>')
                return false;
            }
        });
    });
</script>