<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>

<style type="text/css">
    .btn {
        margin-bottom: 3px;
    }

    .print {
        border: 1px solid gray;
        min-height: 200px;
        margin-bottom: 100px;
        padding: 80px;
        position: relative;
    }

    .btn-print {
        position: absolute;
        top: 0px;
        left: 0px;
    }

    .print .boxino {
        border: 1px solid black;
        border-radius: 6px;
        float: right;
        min-width: 300px;
        padding: 10px 15px;
    <?php if($lang == 'english'):?> float: left;
    <?php else: ?> float: right;
    <?php endif; ?>
    }

    .btn-pr1 {
        background: #999;
        borde: 1px solid #777;
        color: #fff;
    }

    .btn-pr2 {
        background: #777;
        borde: 1px solid #777;
        color: #fff;
    }

    .print h5 {
        font-weight: bold;
        background: #337AB7;
        color: #fff;
        padding: 10px;
    }

    .print .plus {
        color: #2e6da4;
        font-weight: bold;
    }

    .print .minus {
        color: red;
        font-weight: bold;
    }

    .spinner_sects {
        display: none;
        font-size: 7px;
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0"><br><br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><?= lang('results') ?>
                    <small>(<?= lang('report_by_employee') ?>)</small>
                </strong>
            </div>
            <div class="panel-body">
                <?php $today = strtotime($today) ?>
                <?php if (!empty($employee_list)): ?>
                    <?php foreach ($employee_list as $emp): ?>
                        <div class="print" id="printed_content<?= $emp->employee_id ?>">
                            <style type="text/css">@media print {
                                    .no-print {
                                        display: none;
                                    }
                                }</style>
                            <a class="btn btn-primary btn-xs btn-print no-print"
                               onClick="printdiv('printed_content<?= $emp->employee_id ?>');"><i
                                        class="fa fa-print"></i> <?= lang("print") ?></a>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div class="boxino">
                                            <b><?= lang('name') ?>
                                                : </b><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                            <br>
                                            <b><?= lang('employee_id') ?>
                                                : </b><?= $emp->employment_id ?>
                                            <br>
                                            -----------------------<br>
                                            <?php if ($lang == 'english'): ?>
                                                <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                            <?php else: ?>
                                                <?php $months = array("الكل","جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                            <?php endif; ?>
                                            <b><?= lang('month') ?> : </b> <?= $months[intval($month)] ?><br>
                                            <b><?= lang('year') ?>
                                                : </b> <?= ($year == 0) ? lang('all') : $year; ?>
                                            <br>
                                            <br>
                                            <b><?= lang('branche') ?> : </b>
                                            <?php if ($branches == 0): ?>
                                                <?= lang("all") ?>
                                            <?php else: ?>
                                                <?php foreach ($branches as $brch): ?>
                                                    <?php if ($brch->branche_id == $branche): ?>
                                                        <?= ($lang == 'arabic') ? $brch->branche_ar : $brch->branche_en; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <br>
                                            <b><?= lang('department') ?> : </b>
                                            <?php if ($department == 0): ?>
                                                <?= lang("all") ?>
                                            <?php else: ?>
                                                <?php foreach ($departments as $dep): ?>
                                                    <?php if ($dep->department_id == $department): ?>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <br>
                                            <b><?= lang('designation') ?> : </b>
                                            <?php if ($designation == -1): ?>
                                                <?= lang("all") ?>
                                            <?php elseif ($designation == 0): ?>
                                                <?= lang("without_section") ?>
                                            <?php else: ?>
                                                <?php foreach ($designations as $des): ?>
                                                    <?php if ($des->designations_id == $designation): ?>
                                                        <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <br>
                                        </div>
                                    </td>
                                    <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                    </td>
                                </tr>
                            </table>
                            <br><br>
                            <!---------------------------------->
                            <? $presence = 0; ?>
                            <? $presence_xh = 0; ?>
                            <? $presence_pr = 0; ?>
                            <?php for ($i = 1; $i <= $num_days; $i++): ?>
                                <? $day = $year . '/' . $month . '/' . sprintf('%02d', $i); ?>
                                <!---------------------------------->
                                <? $total = 0; ?>
                                <? $in = ""; ?>
                                <? $out = ""; ?>
                                <? $blocks = array(); ?>
                                <?php foreach ($attendances as $att): ?>
                                    <?php if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day): ?>
                                        <?php if ($att->Action == "1"): ?>
                                            <?php $in = $att->att_time; ?>
                                        <?php else: ?>
                                            <?php $out = $att->att_time; ?>
                                            <?php if ($in != "" and $out != ""): ?>
                                                <?php $in_time = strtotime($in);
                                                $out_time = strtotime($out);
                                                $diff = round(abs($in_time - $out_time) / 60); ?>
                                                <?php array_push($blocks, [$in, $out, $diff]); ?>
                                                <?php $in = "";
                                                $out = ""; ?>
                                            <? endif; ?>
                                        <? endif; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <? if (!empty($blocks)): ?>
                                    <? foreach ($blocks as $block): ?>
                                        <? $total += $block[2]; ?>
                                    <? endforeach; ?>
                                <? endif; ?>
                                <!---------------------------------->
                                <? $total_xh = 0; ?>
                                <? $in_xh = ""; ?>
                                <? $out_xh = ""; ?>
                                <? $blocks_xh = array(); ?>
                                <? foreach ($attendances_xh as $att_xh): ?>
                                    <? if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day): ?>
                                        <? if ($att_xh->Action == "1"): ?>
                                            <? $in_xh = $att_xh->att_time; ?>
                                        <? else: ?>
                                            <? $out_xh = $att_xh->att_time; ?>
                                            <? if ($in_xh != "" and $out_xh != ""): ?>
                                                <? $in_time = strtotime($in_xh);
                                                $out_time = strtotime($out_xh);
                                                $diff_xh = round(abs($in_time - $out_time) / 60); ?>
                                                <? array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]); ?>
                                                <? $in_xh = "";
                                                $out_xh = ""; ?>
                                            <? endif; ?>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endforeach; ?>
                                <? if (!empty($blocks_xh)): ?>
                                    <? foreach ($blocks_xh as $block_xh): ?>
                                        <? $total_xh += $block_xh[2]; ?>
                                    <? endforeach; ?>
                                <? endif; ?>
                                <!---------------------------------->
                                <? $total_pr = 0; ?>
                                <?php foreach ($tbl_permissions as $pr): ?>
                                    <?php if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day): ?>
                                        <?php $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60); ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <!---------------------------------->
                                <? if ($total != 0): ?>
                                    <? $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY'); ?>
                                <? endif; ?>
                                <? if ($total_xh != 0): ?>
                                    <? $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY'); ?>
                                <? endif; ?>
                                <? if ($total_pr != 0): ?>
                                    <? $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY'); ?>
                                <? endif; ?>
                            <?php endfor; ?>
                            <!-----------------BASIC SALARY----------------->
                            <h5><?= lang('finance_basic_salary') ?></h5>
                            <span class="plus"><?= $emp->employee_salary ?></span> <?= lang('rial') ?>
                            <!-----------------BASIC SALARY----------------->

                            <!-----------------ATTENDANCE----------------->
                            <h5><?= lang('calculating_attendance') ?></h5>
                            <?php $plus_1 = 0; ?>
                            <?php $minus_1 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><?= lang("total_extra_hours") ?></td>
                                    <td class="text-center"><?= lang("total_attendant_hours") ?></td>
                                    <td class="text-center"><?= lang("total_permissions") ?></td>
                                    <td class="text-center"><?= lang("all_hours_available_in_month") ?></td>
                                    <td class="text-center"><?= lang("total_abscence_in_month") ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <span><?= ((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00' ?></span>
                                    </td>
                                    <td class="text-center">
                                        <span><?= ((int)($presence / 3600)) . ':' . (int)(($presence % 3600) / 60) . ':00' ?></span>
                                    </td>
                                    <td class="text-center">
                                        <span><?= ((int)($presence_pr / 3600)) . ':' . (int)(($presence_pr % 3600) / 60) . ':00' ?></span>
                                    </td>
                                    <td class="text-center">
                                        <span>
                                            <?php if ($emp->job_time == 'Part' and $emp->job_time_hours != 0): ?>
                                                <?php $mn = ($num_days - $num_days_off) * $emp->job_time_hours; ?>
                                            <?php else: ?>
                                                <?php $mn = ($num_days - $num_days_off) * $job_time_hours; ?>
                                            <?php endif; ?>
                                            <?php $mn = ($mn * 3600); ?>
                                            <?= ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00' ?>
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <span>
                                            <?php $abs = $mn - $presence - $presence_pr; ?>
                                            <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00' ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><?= lang("value_per_hour") ?></td>
                                    <td class="text-center"><?= lang("value_extra_hour") ?></td>
                                    <td class="text-center"><?= lang("value_extra_hours") ?></td>
                                    <td class="text-center"><?= lang("value_abscences") ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <?php $mn_value = (int)($mn / 60); ?>
                                        <?= $emp->employee_salary . ' / ' . ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00' ?>
                                        <?php $vh = round($emp->employee_salary / $mn_value * 60, 2); ?>
                                        <?= ' = ' . $vh . ' ' . lang("rial"); ?>
                                        <?php $vmn = round($emp->employee_salary / $mn_value, 5); ?>
                                    </td>
                                    <td class="text-center">
                                        <?= $emp->extra_hours_value . '(%) * ' . $vh . ' = ' ?>
                                        <?php $exh = ($vh / 100) * $emp->extra_hours_value; ?>
                                        <?= round($exh, 2) . ' ' . lang("rial") ?>
                                        <?php $exmn = ($vmn / 100) * $emp->extra_hours_value; ?>
                                    </td>
                                    <td class="text-center">
                                        <?= gmdate("H:i:s", ($presence_xh)) . ' * ' . $exh . ' = '; ?>
                                        <?php $plus_1 = round((int)($presence_xh / 60) * $exmn, 0) ?>
                                        <span class="plus"><?= $plus_1 ?></span> <?= lang("rial") ?>
                                    </td>
                                    <td class="text-center">
                                        <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00 * ' . $vh . '  = ' ?>
                                        <span class="minus"><?= $minus_1 = round($abs / 60 * $vmn, 0) ?></span> <?= lang("rial") ?>
                                    </td>
                                </tr>
                            </table>
                            <!-----------------ATTENDANCE----------------->

                            <!-----------------ALLOWANCES----------------->
                            <h5><?= lang('allowances') ?></h5>
                            <?php $plus_2 = 0; ?>
                            <table class="table table-bordered">
                                <?php if ($emp->job_time == "Full"): ?>
                                    <tr>
                                        <td class="text-center"><?= lang('finance_bonus') ?></td>
                                        <td class="text-center"><?= lang('finance_value') ?></td>
                                        <td class="text-center"><?= lang('finance_from') ?></td>
                                        <td class="text-center"><?= lang('finance_to') ?></td>
                                        <td class="text-center"><?= lang('status') ?></td>
                                        <td class="text-center"><?= lang('finance_reservation') ?> %</td>
                                        <td width="20%" class="text-center"><?= lang('total') ?></td>
                                    </tr>
                                    <?php if (!empty(@$allowances_list)): ?>
                                        <?php foreach (@$allowances_list as $al): ?>
                                            <?php if ($al->employe_id == $emp->employee_id): ?>
                                                <?php if (dates($month, $year, $al->allowance_date_from, $al->allowance_date_to)): ?>
                                                    <tr>
                                                        <td>
                                                            <?php if ($al->allowance_id == 0): ?>
                                                                <?= lang('other_dues') ?>
                                                            <?php else: ?>
                                                                <?php foreach (@$allowances_cat_list as $acl): ?>
                                                                    <?php if ($acl->allowance_id == $al->allowance_id): ?>
                                                                        <?= ($lang == 'english') ? $acl->allowance_title_en : $acl->allowance_title_ar; ?>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($al->allowance_type == 'value'): ?>
                                                                <span class="pull-left"><?= $al->allowance_value ?></span>
                                                            <?php else: ?>
                                                                <span class="pull-left"><?= $al->allowance_value ?>
                                                                    (%)</span>
                                                                <span class="pull-right"
                                                                      style="border:1px solid gray; padding: 0px 10px"><?= ($emp->employee_salary / 100) * ($al->allowance_value) ?></span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?= $al->allowance_date_from ?></td>
                                                        <td><?= $al->allowance_date_to ?></td>
                                                        <td>
                                                            <?php $from = strtotime($al->allowance_date_from); ?>
                                                            <?php $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30"); ?>

                                                            <?php if ($from <= $today and $today <= $to): ?>
                                                                <span class="label label-success"><?= lang('active') ?></span>
                                                            <?php else: ?>
                                                                <span class="label label-warning"><?= lang('inactive') ?></span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($from <= $today and $today <= $to): ?>
                                                                <span class="label label-success"><?= $al->reservation ?>
                                                                    %</span>
                                                            <?php else: ?>
                                                                <span class="label label-warning"><?= $al->reservation ?>
                                                                    %</span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if ($al->allowance_type == 'value'): ?>
                                                                <?php $val = ($al->allowance_value / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <span class="label label-success"><?= $val ?> <?= lang("rial") ?></span>
                                                                    <?php $plus_2 += $val; ?>
                                                                <?php else: ?>
                                                                    <span class="label label-warning"><?= $val ?> <?= lang("rial") ?></span>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <?php $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation); ?>
                                                                <?php if ($from <= $today and $today <= $to): ?>
                                                                    <span class="label label-success"><?= $val ?> <?= lang("rial") ?></span>
                                                                    <?php $plus_2 += $val; ?>
                                                                <?php else: ?>
                                                                    <span class="label label-warning"><?= $val ?> <?= lang("rial") ?></span>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td colspan="6"></td>
                                            <td class="text-center"><span
                                                        class="plus"><?= $plus_2 ?></span> <?= lang("rial") ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <tr>
                                        <td><?= lang('no-exist') ?></td>
                                        <td width="20%" class="text-center"><span
                                                    class="plus"><?= $plus_2 ?></span> <?= lang("rial") ?></td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                            <!-----------------ALLOWANCES----------------->

                            <!-----------------fieldset_med----------------->
                            <h5><?= lang('fieldset_med') ?></h5>
                            <?php $minus_2 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <?php if ($emp->med_insur != 1): ?>
                                        <td>
                                            <?= lang('no-exist') ?>
                                            <input type="hidden" class="med-insurance"
                                                   value="0">
                                        </td>
                                        <td width="20%" class="text-center">
                                            <span class="minus"><?= $minus_2 ?></span> <?= lang("rial") ?>
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <?= (@$emp->med_insur_type == 1) ? lang('medical_insur_type_all') : lang('medical_insur_type_part'); ?>
                                            ( <?= @$emp->m_insurance_percent ?>% )
                                        </td>
                                        <td width="20%" class="text-center">
                                            <?php $minus_2 = ($emp->employee_salary / 100) * @$emp->m_insurance_percent; ?>
                                            <span class="minus"><?= $minus_2 ?></span> <?= lang("rial") ?>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            </table>
                            <!-----------------fieldset_med----------------->

                            <!-----------------fieldset_social----------------->
                            <h5><?= lang('fieldset_social') ?></h5>
                            <?php $minus_3 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <?php if ($emp->social_insurance != 1): ?>
                                        <td>
                                            <?= lang('no-exist') ?>
                                        </td>
                                        <td width="20%" class="text-center">
                                            <span class="minus"><?= $minus_3 ?></span> <?= lang("rial") ?>
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <?php
                                            if (@$emp->social_insurance_type == "saudi1") echo lang('saudi1');
                                            elseif (@$emp->social_insurance_type == "saudi2") echo lang('saudi2');
                                            elseif (@$emp->social_insurance_type == "non-saudi") echo lang('non-saudi');
                                            else echo lang('no-exist');
                                            ?>
                                            (<?= @$emp->insurance_percent ?> %)
                                        </td>
                                        <td width="20%" class="text-center">
                                            <!-- clculate house allowance -->
                                            <?php $x = 0; ?>
                                            <?php
                                            if ($emp->job_time == "Full"):
                                                if (!empty(@$allowances_list)):
                                                    foreach (@$allowances_list as $al):
                                                        if ($al->employe_id == $emp->employee_id and $al->allowance_id == 1):
                                                            if (dates($month, $year, $al->allowance_date_from, $al->allowance_date_to)):
                                                                $from = strtotime($al->allowance_date_from);
                                                                $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30");
                                                                if ($al->allowance_type == 'value'):
                                                                    $val = ($al->allowance_value / 100) * (100 - $al->reservation);
                                                                    if ($from <= $today and $today <= $to):
                                                                        $x += $val;
                                                                    endif;
                                                                else:
                                                                    $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation);
                                                                    if ($from <= $today and $today <= $to):
                                                                        $x += $val;
                                                                    endif;
                                                                endif;
                                                            endif;
                                                        endif;
                                                    endforeach;
                                                endif;
                                            endif;
                                            ?>
                                            <!--clculate house allowance-->
                                            <?php
                                            $id = 2;
                                            switch ($emp->social_insurance_type) {
                                                case 'saudi1':
                                                    $id = 1;
                                                    break;
                                                case 'saudi2':
                                                    $id = 2;
                                                    break;
                                                case 'non-saudi':
                                                    $id = 3;
                                                    break;
                                            }
                                            ?>
                                            <?php if ($socials[$id - 1]->habit_insurance == 1): ?>
                                                <?php $minus_3 = (($emp->employee_salary + $x) / 100) * @$emp->insurance_percent ?>
                                            <?php else: ?>
                                                <?php $minus_3 = ($emp->employee_salary / 100) * @$emp->insurance_percent ?>
                                            <?php endif; ?>
                                            <span class="minus"><?= $minus_3 ?></span> <?= lang("rial") ?>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            </table>
                            <!-----------------fieldset_social----------------->

                            <!-----------------finance_permanent_deduction----------------->
                            <h5><?= lang('finance_permanent_deduction') ?></h5>
                            <?php $minus_4 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><?= lang('finance_deduction_bonus') ?></td>
                                    <td class="text-center"><?= lang('finance_deduction_type') ?></td>
                                    <td class="text-center"><?= lang('finance_deduction_value') ?></td>
                                    <th class="text-center"><?= lang('start_date') ?></th>
                                    <th class="text-center"><?= lang('end_date') ?></th>
                                    <td width="20%" class="text-center"><?= lang('total') ?></td>
                                </tr>
                                <?php if (!empty(@$deduction_list)): ?>
                                    <?php foreach (@$deduction_list as $d): ?>
                                        <?php if ($d->employd_id == $emp->employee_id): ?>
                                            <?php if (dates($month, $year, $d->start_date, $d->end_date)): ?>
                                                <tr>
                                                    <td>
                                                        <?php foreach (@$deduction_category_list as $dc): ?>
                                                            <?php if ($d->deduction_id == $dc->deduction_id): ?>
                                                                <?= ($lang == 'english') ? $dc->deduction_title_en : $dc->deduction_title_ar; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </td>
                                                    <td><?= ($d->deduction_type == "percent") ? lang('deduction_type_percent') : lang('deduction_type_value'); ?></td>
                                                    <td><?= $d->deduction_value; ?></td>
                                                    <td><?= $d->start_date; ?></td>
                                                    <td><?= $d->end_date; ?></td>
                                                    <td class="text-center">
                                                        <?php
                                                        if ($d->deduction_type == 'percent') {
                                                            $val = ($emp->employee_salary / 100) * @$d->deduction_value;
                                                            $minus_4 += $val;
                                                        } else {
                                                            $val = @$d->deduction_value;
                                                            $minus_4 += $val;
                                                        }
                                                        ?>
                                                        <?= $val ?> <?= lang("rial") ?>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="5"></td>
                                        <td width="20%" class="text-center">
                                            <span class="minus"><?= $minus_4 ?></span> <?= lang("rial") ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                            <!-----------------finance_permanent_deduction----------------->

                            <!-----------------finance_provision----------------->
                            <h5><?= lang('finance_provision') ?></h5>
                            <?php $plus_3 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><?= lang('finance_provision_bonus') ?></td>
                                    <th class="text-center"><?= lang('start_date') ?></th>
                                    <th class="text-center"><?= lang('end_date') ?></th>
                                    <td width="20%" class="text-center"><?= lang('finance_provision_value') ?></td>

                                </tr>
                                <?php if (!empty(@$provision_list)): ?>
                                    <?php foreach (@$provision_list as $p): ?>
                                        <?php if ($p->employp_id == $emp->employee_id): ?>
                                            <?php if (dates($month, $year, $p->start_date, $p->end_date)): ?>
                                                <tr>
                                                    <td>
                                                        <?php foreach (@$provision_cat_list as $pc): ?>
                                                            <?php if ($pc->provision_category_id == $p->provision_category_id): ?>
                                                                <?= ($lang == 'english') ? $pc->provision_title_en : $pc->provision_title_ar; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </td>
                                                    <td><?= $p->start_date ?></td>
                                                    <td><?= $p->end_date ?></td>
                                                    <td class="text-center">
                                                        <?php $plus_3 += $p->provision_value ?>
                                                        <?= $p->provision_value ?> <?= lang("rial") ?>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td width="20%" class="text-center">
                                            <span class="plus"><?= $plus_3 ?></span> <?= lang("rial") ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                            <!-----------------finance_provision----------------->

                            <!-----------------finance_houcing----------------->
                            <!--
                            <h5><?= lang('finance_houcing') ?></h5>
                            <?php $plus_4 = 0; ?>
                            <?php if ($emp->houcing_status == 0): ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="2"><?= lang('no-exist') ?></td>
                                        <td width="20%" class="text-center">
                                            <span class="plus"><?= $plus_4 ?></span> <?= lang("rial") ?>
                                        </td>
                                    </tr>
                                </table>
                            <?php else: ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td><?= lang('finance_status') ?></td>
                                        <td>
                                            <?= lang('finance_count') ?> :
                                            <?= ($emp->houcing_type == 1) ? lang('percent') : lang('value'); ?>
                                            <?= ($emp->houcing_type == 1) ? '(' . $emp->houcing_value . ' %)' : $emp->houcing_value . ' ' . lang('rial'); ?>
                                        </td>
                                        <td width="20%" class="text-center">
                                            <?php if (($emp->houcing_type == 1)) {
                                $plus_4 = ($emp->employee_salary / 100) * @$emp->houcing_value;
                            } else
                                $plus_4 = @$emp->houcing_value;
                                ?>
                                            <span class="plus"><?= $plus_4 ?></span> <?= lang("rial") ?>
                                        </td>
                                    </tr>
                                </table>
                            <?php endif; ?>
                            -->
                            <!-----------------finance_houcing----------------->

                            <!-----------------non_paid_leaves----------------->
                            <h5><?= lang('non_paid_leaves') ?></h5>
                            <?php $minus_5 = 0; ?>
                            <table class="table table-bordered">
                                <?php foreach (@$leaves_list as $leave): ?>
                                    <?php if ($leave->employel_id == $emp->employee_id): ?>
                                        <tr>
                                            <td>
                                                <?php foreach ($leaves_cat as $lc): ?>
                                                    <?php if ($leave->leave_category_id == $lc->leave_category_id): ?>
                                                        <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td>
                                                <?= lang('going_date') ?> : <?= $leave->going_date ?>
                                            </td>
                                            <td>
                                                <?= lang('coming_date') ?> : <?= $leave->coming_date ?>
                                            </td>
                                            <td>
                                                <?= lang('leave_category_duration') ?>
                                                : <?= $leave->duration ?> <?= lang('days') ?>
                                            </td>
                                            <td>
                                                <?= lang('leave_category_quota') ?> : <?= $leave->quota ?> %
                                            </td>
                                            <td width="20%" class="text-center">
                                                <?php $value = ($emp->employee_salary / 100) * $leave->quota ?>
                                                <?= $value; ?>
                                                <?php $minus_5 += $value; ?> <?= lang("rial") ?>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <tr>
                                    <td colspan="5"></td>
                                    <td width="20%" class="text-center">
                                        <span class="minus"><?= $minus_5 ?></span> <?= lang("rial") ?>
                                    </td>
                                </tr>
                            </table>
                            <!-----------------non_paid_leaves----------------->

                            <!-----------------advances_app----------------->
                            <h5><?= lang('advances_app') ?></h5>
                            <?php $minus_6 = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><?= lang('date') ?></td>
                                    <td class="text-center"><?= lang('advance_value') ?></td>
                                    <td class="text-center"><?= lang('rest_advance') ?></td>
                                    <td class="text-center"><?= lang('payement_method') ?></td>
                                    <td class="text-center"><?= lang('monthly_installement') ?></td>
                                    <td class="text-center"><?= lang('payement_months') ?></td>
                                    <td class="text-center"><?= lang('last_advance_date') ?></td>
                                    <? if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager' or $this->session->userdata('emp_type') == 'accountant'): ?>
                                        <td class="text-center"><?= lang('action') ?></td>
                                    <? endif; ?>
                                </tr>
                                <?php if (!empty(@$advances_list)): ?>
                                    <?php foreach (@$advances_list as $adv): ?>
                                        <?php if ($adv->employea_id == $emp->employee_id): ?>
                                            <tr>
                                                <td><?= $adv->advance_date ?></td>
                                                <td><?= $adv->advance_value ?></td>
                                                <td><?= $adv->rest ?></td>
                                                <td><?= lang('payement_method' . $adv->payement_method) ?></td>
                                                <td>
                                                    <?= $valuee = $adv->monthly_installement ?><?php $minus_6 += $valuee; ?>
                                                </td>
                                                <td><?= $adv->payement_months ?></td>
                                                <td><?= (!empty(@$adv->last_date)) ? @$adv->last_date : lang('no-exist'); ?></td>
                                                <? if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager' or $this->session->userdata('emp_type') == 'accountant'): ?>
                                                    <td>
                                                        <a class="no-print btn btn-success btn-xs"
                                                           onclick="proccessadvance(<?= $adv->advances_id ?>)"><?= lang('proccess_advance') ?></a>
                                                        <span class="spinner_sects no-print"
                                                              id="spinner_sects<?= $adv->advances_id ?>"><i
                                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></span>
                                                    </td>
                                                <? endif; ?>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <tr>
                                    <td colspan="2"></td>
                                    <td width="20%" class="text-center">
                                        <span class="minus"><?= $minus_6 ?></span> <?= lang("rial") ?>
                                    </td>
                                    <td colspan="5"></td>
                                </tr>
                            </table>
                            <!-----------------advances_app----------------->

                            <!-----------------results----------------->
                            <h5><?= lang('results') ?></h5>
                            <?php $discounts = 0; ?>
                            <?php $extras = 0; ?>
                            <?php $finance_all_salary = 0; ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td width="33%" class="text-center"><?= lang('discounts') ?></td>
                                    <td width="33%" class="text-center"><?= lang('extras') ?></td>
                                    <td width="33%" class="text-center"><?= lang('finance_all_salary') ?></td>
                                </tr>
                                <tr>
                                    <td width="33%" class="text-center">
                                        <?php $discounts = round(($minus_1 + $minus_2 + $minus_3 + $minus_4 + $minus_5 + $minus_6), 2); ?>
                                        <span class="minus"><?= $discounts . ' ' . lang("rial") ?></span>
                                    </td>
                                    <td width="33%" class="text-center">
                                        <?php $extras = round(($plus_1 + $plus_2 + $plus_3 + $plus_4 + $emp->employee_salary), 2); ?>
                                        <span class="plus"><?= $extras . ' ' . lang("rial") ?></span>
                                    </td>
                                    <td width="33%" class="text-center">
                                        <?php $finance_all_salary = round(($extras - $discounts), 2); ?>
                                        <span style="color: green;font-weight: bold;"><?= $finance_all_salary ?></span>
                                        <span style="color: green;font-weight: bold;"> <?= lang("rial") ?></span>
                                    </td>
                                </tr>
                            </table>
                            <!-----------------results----------------->

                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4 class="text-center"><?= lang('nothing_to_display') ?></h4>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
function dates($month, $year, $start, $end)
{
    if ($end == '')
        $end = date_create_from_format('Y-m-d', "9999-12-30")->format('Y-m-d');
    $flag = false;

    $start_s = date_create_from_format('Y-m-d', $start)->format('Ym');
    $end_e = date_create_from_format('Y-m-d', $end)->format('Ym');
    $date_x = date_create_from_format('Y-m-d', $year . '-' . $month . '-01')->format('Ym');
    if ($date_x <= $end_e and $date_x >= $start_s)
        $flag = true;

    return $flag;
}

?>

<script type="text/javascript">
    function proccessadvance(id) {
        var result = confirm('<?=($lang == "arabic") ? "تنبيه : يجب حساب الرصيد قبل خلاص أي قسط " : "Attention: calculate the salary before paying";?>')
        if (result) {
            $('#spinner_sects' + id).fadeIn('fast');
            $.ajax({
                url: '<?=base_url()?>employee/dashboard/proccessadvance/' + id,
                success: function (data) {
                    if (data = 'true') {
                        $('#spinner_sects' + id).fadeOut('fast');
                        alert('<?=($lang == 'arabic') ? 'تم تثبيت خلاص القسط' : 'advance payement was successfully saved';?>');
                        location.reload();
                    }
                }
            }).fail(function () {
                alert('<?= lang('alert_error') ?>')
            });
        }
    }
</script>
