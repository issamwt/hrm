<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>

<style type="text/css">
    .btn {
        margin-bottom: 3px;
    }

    .print {
        border: 1px solid gray;
        min-height: 200px;
        margin-bottom: 100px;
        padding: 80px;
        position: relative;
    }

    .btn-print {
        position: absolute;
        top: 0px;
        left: 0px;
    }

    .btn-excel {
        position: absolute;
        top: 0px;
        left: 70px;
    }

    .print .boxino {
        border: 1px solid black;
        border-radius: 6px;
        float: right;
        min-width: 300px;
        padding: 10px 15px;
    <?php if($lang == 'english'):?> float: left;
    <?php else: ?> float: right;
    <?php endif; ?>
    }

    .btn-pr1 {
        background: #999;
        borde: 1px solid #777;
        color: #fff;
    }

    .btn-pr2 {
        background: #777;
        borde: 1px solid #777;
        color: #fff;
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0"><br><br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><?= lang('results') ?>
                    <small>(<?= lang('report_all') ?>)</small>
                </strong>
            </div>
            <div class="panel-body">
                <div class="print" id="printed_content">
                    <style type="text/css">@media print {
                            .no-print {
                                display: none;
                            }
                        }</style>
                    <a class="btn btn-primary btn-xs btn-print no-print"
                       onClick="printdiv('printed_content');"><i
                                class="fa fa-print"></i> <?= lang("print") ?></a>
                    <a class="btn btn-success btn-xs btn-excel no-print"
                       href="<?= base_url() ?>employee/calculations/download_attendance/<?= $month ?>/<?= $year ?>/<?= $department ?>/<?= $designation ?>"
                       target="_blank">
                        <i class=" fa fa-file-excel-o"></i> Excel</a>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div class="boxino">
                                    <?php if ($lang == 'english'): ?>
                                        <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                    <?php else: ?>
                                        <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                    <?php endif; ?>
                                    <b><?= lang('month') ?> : </b> <?= $months[intval($month)] ?><br>
                                    <b><?= lang('year') ?>
                                        : </b> <?= ($year == 0) ? lang('all') : $year ; ?>
                                    <br>
                                    <b><?= lang('department') ?> : </b>
                                    <?php if ($department == 0): ?>
                                        <?= lang("all") ?>
                                    <?php else: ?>
                                        <?php foreach ($departments as $dep): ?>
                                            <?php if ($dep->department_id == $department): ?>
                                                <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <br>
                                    <b><?= lang('designation') ?> : </b>
                                    <?php if ($designation == -1): ?>
                                        <?= lang("all") ?>
                                    <?php elseif ($designation == 0): ?>
                                        <?= lang("without_section") ?>
                                    <?php else: ?>
                                        <?php foreach ($designations as $des): ?>
                                            <?php if ($des->designations_id == $designation): ?>
                                                <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <br>
                                </div>
                            </td>
                            <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                            </td>
                        </tr>
                    </table>
                    <br><br><br>

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center" width="12%"><?= lang("employee_id") ?></th>
                            <th class="text-center" width="15%"><?= lang("employee_name") ?></th>
                            <th class="text-center" width="8%"><?= lang("job_time") ?></th>
                            <th class="text-center" width="13%"><?= lang("total_extra_hours") ?></th>
                            <th class="text-center" width="13%"><?= lang("total_attendant_hours") ?></th>
                            <th class="text-center" width="13%"><?= lang("total_permissions") ?></th>
                            <th class="text-center" width="13%"><?= lang("all_hours_available_in_month") ?></th>
                            <th class="text-center" width="13%"><?= lang("total_abscence_in_month") ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($employee_list)): ?>
                            <?php foreach ($employee_list as $emp): ?>
                                <tr>
                                    <td class="text-center"><?= $emp->employment_id ?></td>
                                    <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                                    <td class="text-center">
                                        <?php if ($emp->job_time == 'Part'): ?>
                                            <?= lang('job_part') ?>
                                        <?php else: ?>
                                            <?= lang('job_full') ?>
                                        <?php endif; ?>
                                    </td>
                                    <? $presence = 0; ?>
                                    <? $presence_xh = 0; ?>
                                    <? $presence_pr = 0; ?>
                                    <?php for ($i = 1; $i <= $num_days; $i++): ?>
                                        <? $day = $year . '/' . $month . '/' . sprintf('%02d', $i); ?>
                                        <!---------------------------------->
                                        <? $total = 0; ?>
                                        <? $in = ""; ?>
                                        <? $out = ""; ?>
                                        <? $blocks = array(); ?>
                                        <?php foreach ($attendances as $att): ?>

                                            <?php if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day): ?>
                                                <?php if ($att->Action == "1"): ?>
                                                    <?php $in = $att->att_time; ?>
                                                <?php else: ?>
                                                    <?php $out = $att->att_time; ?>
                                                    <?php if ($in != "" and $out != ""): ?>
                                                        <?php $in_time = strtotime($in);
                                                        $out_time = strtotime($out);
                                                        $diff = round(abs($in_time - $out_time) / 60); ?>
                                                        <?php array_push($blocks, [$in, $out, $diff]); ?>
                                                        <?php $in = "";
                                                        $out = ""; ?>
                                                    <? endif; ?>
                                                <? endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <? if (!empty($blocks)): ?>
                                            <? foreach ($blocks as $block): ?>
                                                <? $total += $block[2]; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                        <!---------------------------------->
                                        <? $total_xh = 0; ?>
                                        <? $in_xh = ""; ?>
                                        <? $out_xh = ""; ?>
                                        <? $blocks_xh = array(); ?>
                                        <? foreach ($attendances_xh as $att_xh): ?>
                                            <? if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day): ?>
                                                <? if ($att_xh->Action == "1"): ?>
                                                    <? $in_xh = $att_xh->att_time; ?>
                                                <? else: ?>
                                                    <? $out_xh = $att_xh->att_time; ?>
                                                    <? if ($in_xh != "" and $out_xh != ""): ?>
                                                        <? $in_time = strtotime($in_xh);
                                                        $out_time = strtotime($out_xh);
                                                        $diff_xh = round(abs($in_time - $out_time) / 60); ?>
                                                        <? array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]); ?>
                                                        <? $in_xh = "";
                                                        $out_xh = ""; ?>
                                                    <? endif; ?>
                                                <? endif; ?>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                        <? if (!empty($blocks_xh)): ?>
                                            <? foreach ($blocks_xh as $block_xh): ?>
                                                <? $total_xh += $block_xh[2]; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                        <!---------------------------------->
                                        <? $total_pr = 0; ?>
                                        <?php foreach ($tbl_permissions as $pr): ?>
                                            <?php if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day): ?>
                                                <?php $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60); ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <!---------------------------------->
                                        <? if ($total != 0): ?>
                                            <? $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY'); ?>
                                        <? endif; ?>
                                        <? if ($total_xh != 0): ?>
                                            <? $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY'); ?>
                                        <? endif; ?>
                                        <? if ($total_pr != 0): ?>
                                            <? $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY'); ?>
                                        <? endif; ?>
                                    <?php endfor; ?>
                                    <td class="text-center"><?= ((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00' ?></td>
                                    <td class="text-center"><?= ((int)($presence / 3600)) . ':' . (int)(($presence % 3600) / 60) . ':00' ?></td>
                                    <td class="text-center"><?= ((int)($presence_pr / 3600)) . ':' . (int)(($presence_pr % 3600) / 60) . ':00' ?></td>
                                    <td class="text-center">
                                        <?php if($emp->job_time=='Part' and $emp->job_time_hours!=0): ?>
                                            <?php $mn = ($num_days - $num_days_off) * $emp->job_time_hours; ?>
                                        <?php else: ?>
                                            <?php $mn = ($num_days - $num_days_off) * $job_time_hours; ?>
                                        <?php endif; ?>
                                        <?php $mn = ($mn * 3600); ?>
                                        <?= ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00' ?>
                                    </td>
                                    <td class="text-center">
                                        <?php $abs = $mn - $presence - $presence_pr ?>
                                        <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00' ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center"><?= lang('nothing_to_display') ?></h4></td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>