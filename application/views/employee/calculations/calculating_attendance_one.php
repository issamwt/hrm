<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>

<style type="text/css">
    .btn {
        margin-bottom: 3px;
    }

    .print {
        border: 1px solid gray;
        min-height: 200px;
        margin-bottom: 100px;
        padding: 80px;
        position: relative;
    }

    .btn-print {
        position: absolute;
        top: 0px;
        left: 0px;
    }

    .print .boxino {
        border: 1px solid black;
        border-radius: 6px;
        float: right;
        min-width: 300px;
        padding: 10px 15px;
    <?php if($lang == 'english'):?> float: left;
    <?php else: ?> float: right;
    <?php endif; ?>
    }

    .btn-pr1 {
        background: #999;
        borde: 1px solid #777;
        color: #fff;
    }

    .btn-pr2 {
        background: #777;
        borde: 1px solid #777;
        color: #fff;
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0"><br><br>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><?= lang('results') ?>
                    <small>(<?= lang('report_by_employee') ?>)</small>
                </strong>
            </div>
            <div class="panel-body">
                <?php if (!empty($employee_list)): ?>
                    <?php foreach ($employee_list as $emp): ?>
                        <div class="print" id="printed_content<?= $emp->employee_id ?>">
                            <style type="text/css">@media print {
                                    .no-print {
                                        display: none;
                                    }
                                }</style>
                            <a class="btn btn-primary btn-xs btn-print no-print"
                               onClick="printdiv('printed_content<?= $emp->employee_id ?>');"><i
                                        class="fa fa-print"></i> <?= lang("print") ?></a>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div class="boxino">
                                            <b><?= lang('name') ?>
                                                : </b><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                            <br>
                                            <b><?= lang('employee_id') ?>
                                                : </b><?= $emp->employment_id ?>
                                            <br>
                                            -----------------------<br>
                                            <?php if ($lang == 'english'): ?>
                                                <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                            <?php else: ?>
                                                <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                            <?php endif; ?>
                                            <b><?= lang('month') ?> : </b> <?= $months[intval($month)] ?><br>
                                            <b><?= lang('year') ?>
                                                : </b> <?= ($year == 0) ? lang('all') : $year ; ?>
                                            <br>
                                            <b><?= lang('department') ?> : </b>
                                            <?php if ($department == 0): ?>
                                                <?= lang("all") ?>
                                            <?php else: ?>
                                                <?php foreach ($departments as $dep): ?>
                                                    <?php if ($dep->department_id == $department): ?>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <br>
                                            <b><?= lang('designation') ?> : </b>
                                            <?php if ($designation == -1): ?>
                                                <?= lang("all") ?>
                                            <?php elseif ($designation == 0): ?>
                                                <?= lang("without_section") ?>
                                            <?php else: ?>
                                                <?php foreach ($designations as $des): ?>
                                                    <?php if ($des->designations_id == $designation): ?>
                                                        <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <br>
                                        </div>
                                    </td>
                                    <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <span class="btn btn-success btn-xs"></span> <?= lang('clock_in') ?><br>
                            <span class="btn btn-danger btn-xs"></span> <?= lang('clock_out') ?><br>
                            <span class="btn btn-primary btn-xs"></span> <?= lang('clock_in') ?>
                            <small>(<?= lang('extra_hours') ?>)</small>
                            <br>
                            <span class="btn btn-primary btn-xs"
                                  style="background:#0d518c"></span> <?= lang('clock_out') ?>
                            <small>(<?= lang('extra_hours') ?>)</small>
                            <br>
                            <span class="btn btn-pr1 btn-xs"></span> <?= lang('start_pr') ?><br>
                            <span class="btn btn-pr2 btn-xs"></span> <?= lang('end_pr') ?>
                            <br><br>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center" width="15%"><?= lang('date') ?></th>
                                    <th class="text-center"><?= lang('attendance_hours') ?></th>
                                    <th class="text-center" width="15%"><?= lang('presence') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <? $presence = 0; ?>
                                <? $presence_xh = 0; ?>
                                <? $presence_pr = 0; ?>
                                <?php for ($i = 1; $i <= $num_days; $i++): ?>
                                    <tr>
                                        <? $day = $year . '/' . $month . '/' . sprintf('%02d', $i); ?>
                                        <td class="text-center"><?= $day ?></td>
                                        <td>
                                            <!---------------------------------->
                                            <? $total = 0; ?>
                                            <? $in = ""; ?>
                                            <? $out = ""; ?>
                                            <? $blocks = array(); ?>
                                            <?php foreach ($attendances as $att): ?>
                                                <?php if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day): ?>
                                                    <?php if ($att->Action == "1"): ?>
                                                        <?php $in = $att->att_time; ?>
                                                        <?= modal($att, $emp_type) ?>
                                                    <?php else: ?>
                                                        <?php $out = $att->att_time; ?>
                                                        <?php if ($in != "" and $out != ""): ?>
                                                            <?php $in_time = strtotime($in);
                                                            $out_time = strtotime($out);
                                                            $diff = round(abs($in_time - $out_time) / 60); ?>
                                                            <?php array_push($blocks, [$in, $out, $diff, $in_id, $att->attendance_id]); ?>
                                                            <?php $in = "";
                                                            $out = ""; ?>
                                                        <? endif; ?>
                                                        <?= modal($att, $emp_type) ?>
                                                    <? endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <? if (!empty($blocks)): ?>
                                                <? foreach ($blocks as $block): ?>
                                                    <!--
                                                    <span class="btn btn-success btn-xs"><?= $block[0] ?></span>
                                                    <span class="btn btn-danger btn-xs"><?= $block[1] ?></span>
                                                    <span></span>
                                                    -->
                                                    <? $total += $block[2]; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <!---------------------------------->
                                            <? $total_xh = 0; ?>
                                            <? $in_xh = ""; ?>
                                            <? $out_xh = ""; ?>
                                            <? $blocks_xh = array(); ?>
                                            <? foreach ($attendances_xh as $att_xh): ?>
                                                <? if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day): ?>
                                                    <? if ($att_xh->Action == "1"): ?>
                                                        <? $in_xh = $att_xh->att_time; ?>
                                                    <? else: ?>
                                                        <? $out_xh = $att_xh->att_time; ?>
                                                        <? if ($in_xh != "" and $out_xh != ""): ?>
                                                            <? $in_time = strtotime($in_xh);
                                                            $out_time = strtotime($out_xh);
                                                            $diff_xh = round(abs($in_time - $out_time) / 60); ?>
                                                            <? array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]); ?>
                                                            <? $in_xh = "";
                                                            $out_xh = ""; ?>
                                                        <? endif; ?>
                                                    <? endif; ?>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                            <? if (!empty($blocks_xh)): ?>
                                                <? foreach ($blocks_xh as $block_xh): ?>
                                                    <span class="btn btn-primary btn-xs"><?= $block_xh[0] ?></span>
                                                    <span class="btn btn-primary btn-xs"
                                                          style="background:#0d518c"><?= $block_xh[1] ?></span>
                                                    <? $total_xh += $block_xh[2]; ?>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <!---------------------------------->
                                            <? $total_pr = 0; ?>
                                            <?php foreach ($tbl_permissions as $pr): ?>
                                                <?php if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day): ?>
                                                    <?php $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60); ?>
                                                    <span class="btn btn-pr1 btn-xs"><?= $pr->permission_star ?></span>
                                                    <span class="btn btn-pr2 btn-xs"><?= $pr->permission_end ?></span>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <!---------------------------------->
                                        </td>
                                        <td class="text-center">
                                            <? if ($total != 0): ?>
                                                <div class="btn btn-success btn-xs"><?= gmdate("H:i:s", ($total * 60)); ?></div>
                                                <? $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                            <? if ($total_xh != 0): ?>
                                                <div class="btn btn-primary btn-xs"><?= gmdate("H:i:s", ($total_xh * 60)); ?></div>
                                                <? $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                            <? if ($total_pr != 0): ?>
                                                <div class="btn btn-pr1 btn-xs"><?= gmdate("H:i:s", ($total_pr * 60)); ?></div>
                                                <? $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY'); ?>
                                            <? endif; ?>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                                <tr>
                                    <td colspan="2" class="text-center"><?= lang("total_extra_hours") ?></td>
                                    <td class="text-center">
                                        <span style="color: #2e6da4"><?= gmdate("H:i:s", ($presence_xh)); ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"><?= lang("total_attendant_hours") ?></td>
                                    <td class="text-center">
                                        <span style="color: green"><?= ((int)($presence / 3600)) . ':' . (int)(($presence % 3600) / 60) . ':00' ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"><?= lang("total_permissions") ?></td>
                                    <td class="text-center">
                                        <span><?= gmdate("H:i:s", ($presence_pr)); ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"><?= lang("all_hours_available_in_month") ?></td>
                                    <td class="text-center">
                                        <?php if ($emp->job_time == 'Part' and $emp->job_time_hours != 0): ?>
                                            <?php $mn = ($num_days - $num_days_off) * $emp->job_time_hours; ?>
                                        <?php else: ?>
                                            <?php $mn = ($num_days - $num_days_off) * $job_time_hours; ?>
                                        <?php endif; ?>
                                        <span>
                                            <?php $mn = ($mn * 3600); ?>
                                            <?= ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00' ?>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center"><?= lang("total_abscence_in_month") ?></td>
                                    <td class="text-center">
                                        <span>
                                            <?php $abs = $mn - $presence - $presence_pr ?>
                                            <?= ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00' ?>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <?php if ($this->session->userdata('emp_type') == 'hr_manager' or $this->session->userdata('emp_type') == 'super_manager'): ?>
                                <!--<button  data-target="#myModalx<?= $emp->employee_id ?>">
                                    <?= ($lang == 'arabic') ? 'أضف دخول/خروج' : 'Add In/Out attendance'; ?>
                                </button>
                                -->
                                <div id="myModalx<?= $emp->employee_id ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <style type="text/css">
                                            .the-table td {
                                                border: 0px !important;
                                            }

                                            .the-table td:first-child {
                                                font-weight: bold;
                                            }

                                            .form {
                                                display: block;
                                                width: 70%;
                                                margin: 0 auto;
                                            }

                                            .form input {
                                                border-radius: 0px !important;
                                            }
                                        </style>
                                        <h3 class="text-center" style="margin-top: 0px"><?= lang('edit') ?></h3>
                                        <form class="form" method="post" id="myForm<?= $emp->employee_id ?>">
                                            <table class="table table-nonbordered  the-table">
                                                <tr>
                                                    <td width="60%">اليوم</td>
                                                    <td>
                                                        <input name="att_date" type="text" value=""
                                                               class="form-control hijri_datepicker" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="60%">الوقت</td>
                                                    <td>
                                                        <input name="att_time" type="text" value="00:00:00"
                                                               class="form-control" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>دخول/خروج</td>
                                                    <td style="direction: ltr;text-align: right;">دخول
                                                        <input type="radio" name="Action" value="1" checked="checked"
                                                               required/>
                                                        <br>
                                                        خروج
                                                        <input type="radio" name="Action" value="2" required/>
                                                        <input type="hidden" name="employee_att_id"
                                                               value="<?= $emp->employee_id ?>">
                                                        <input type="hidden" name="employee_name"
                                                               value="<?= $emp->full_name_en ?>">
                                                        <input type="hidden" name="job_code"
                                                               value="<?= $emp->employment_id ?>">
                                                        <input type="hidden" name="employee_status"
                                                               value="<?= $emp->status ?>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="submit" style="text-align: center">
                                                            <button class="btn btn-block btn-primary"><?= lang('save') ?></button>
                                                        </div>
                                                        <script type="text/javascript">
                                                            $(function () {
                                                                $("#myForm<?= $emp->employee_id ?>").submit(function (e) {
                                                                    $(this).find('.submit').html('<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="color: #337ab7"></i>');
                                                                    var url = "<?= base_url() ?>employee/calculations/save_attendance2";
                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: url,
                                                                        data: $("#myForm<?= $emp->employee_id ?>").serialize(),
                                                                        success: function (data) {
                                                                            $("#myForm<?= $emp->employee_id ?>").find('.submit').html('<i class="fa fa-check fa-5x" style="color: #00CC00"></i>');
                                                                            setTimeout('location.reload()', 2000);
                                                                        }
                                                                    });
                                                                    e.preventDefault();
                                                                });
                                                            });
                                                        </script>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                        <script type="text/javascript">
                                            $(function () {
                                                var calendar = $.calendars.instance('ummalqura');
                                                $('.hijri_datepicker').calendarsPicker({calendar: calendar});
                                            });
                                        </script>
                                    </div>

                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4 class="text-center"><?= lang('nothing_to_display') ?></h4>
                <?php endif; ?>


            </div>
        </div>
    </div>
</div>


<?php
function modal($attendace, $emp_type)
{
    $class = ($attendace->Action == 1) ? 'success' : 'danger';
    ?>
    <span class="btn btn-<?= $class ?> btn-xs" 
          data-target="#myModaly<?= $attendace->attendance_id ?>">
    <?= $attendace->att_time ?>
</span>
    <?php if ($emp_type == 'hr_manager' or $emp_type == 'super_manager'): ?>
        <div id="myModaly<?= $attendace->attendance_id ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <style type="text/css">
                    .the-table td {
                        border: 0px !important;
                    }

                    .the-table td:first-child {
                        font-weight: bold;
                    }

                    .form {
                        display: block;
                        width: 70%;
                        margin: 0 auto;
                    }

                    .form input {
                        border-radius: 0px !important;
                    }
                </style>
                <h3 class="text-center" style="margin-top: 0px"><?= lang('edit') ?></h3>
                <form class="form" method="post" id="the_form<?= $attendace->attendance_id ?>">
                    <table class="table table-nonbordered  the-table">
                        <tr>
                            <td width="60%">اليوم</td>
                            <td>
                                <input name="att_date" type="text" value="<?= $attendace->att_date; ?>"
                                       class="form-control hijri_datepicker" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="60%">الوقت</td>
                            <td>
                                <input name="att_time" type="text" value="<?= $attendace->att_time; ?>"
                                       class="form-control" required>
                            </td>
                        </tr>
                        <tr>
                            <td>دخول/خروج</td>
                            <td style="direction: ltr;text-align: right;">دخول
                                <input type="radio" name="Action"
                                       value="1" <?= ($attendace->Action == 1) ? 'checked="checked"' : ''; ?> required/>
                                <br>
                                خروج
                                <input type="radio" name="Action"
                                       value="2" <?= ($attendace->Action == 2) ? 'checked="checked"' : ''; ?> required/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="submit" style="text-align: center">
                                    <button class="btn btn-block btn-primary"><?= lang('save') ?></button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                <script type="text/javascript">
                    $(function () {
                        $("#the_form<?= $attendace->attendance_id ?>").submit(function (e) {
                            $("#the_form<?= $attendace->attendance_id ?> .submit").html('<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="color: #337ab7"></i>');
                            var url = "<?= base_url() ?>employee/calculations/change_attendance2/<?= $attendace->attendance_id ?>";
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: $("#the_form<?= $attendace->attendance_id ?>").serialize(),
                                success: function (data) {
                                    $("#the_form<?= $attendace->attendance_id ?> .submit").html('<i class="fa fa-check fa-5x" style="color: #00CC00"></i>');
                                    setTimeout('location.reload()', 2000);
                                    console.log(data);
                                }
                            });
                            e.preventDefault();
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(function () {
                        var calendar = $.calendars.instance('ummalqura');
                        $('.hijri_datepicker').calendarsPicker({calendar: calendar});
                    });
                </script>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php
}

?>

