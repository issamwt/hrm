<script language="javascript">
    function printdiv(printpage) {
        var headstr = "<html><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
    $(function () {
        $('.main_content').closest('.container').attr('class', 'container-fluid');
    });
</script>
<div class="main_content">
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">
            <div class="panel panel-primary">
                <div class="panel-heading"><strong><?= lang("results") ?></strong></div>
                <div class="panel-body">
                    <?php if (!empty(@$results)): ?>
                        <?php foreach (@$results as $result): ?>
                            <button class="btn btn-primary btn-xs"
                                    onClick="printdiv('printed_content<?= $result->employee_id ?>');"><i
                                        class="fa fa-print"></i> <?= lang("print") ?></button>
                            <div class="print" id="printed_content<?= $result->employee_id ?>">
                                <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet">
                                <link href="<?php echo base_url(); ?>asset/css/employee.css" rel="stylesheet">
                                <link href="<?php echo base_url() ?>asset/css/bootstrap.min.css" rel="stylesheet"/>
                                <?php if ($lang == "arabic"): ?>
                                    <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css"
                                          rel="stylesheet"/>

                                <?php endif; ?>
                                <style type="text/css">
                                    .btn.btn-primary.btn-xs {
                                        float: left;
                                        margin-bottom: 20px;
                                    }

                                    .print {
                                        border: 1px solid gray;
                                        min-height: 200px;
                                        margin-bottom: 100px;
                                        padding: 100px;
                                    }

                                    .print .boxino {
                                        border: 1px solid black;
                                        border-radius: 6px;
                                        float: right;
                                        min-width: 300px;
                                        padding: 10px 15px;
                                    <?php if($lang == "english"):?> float: left;
                                    <?php else: ?> float: right;
                                    <?php endif; ?>
                                    }

                                    table {
                                        width: 100%;
                                    }
                                </style>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="boxino">
                                                <b><?= lang("name") ?>
                                                    : </b><?= ($lang == "english") ? $result->full_name_en : $result->full_name_ar; ?>
                                                <br>
                                                <b><?= lang("designation") ?>
                                                    :</b><?= (!empty($result->designations)) ? (($lang == "english") ? $result->designations : $result->designations_ar) : lang("no-exist"); ?>
                                                <br>
                                                <b><?= lang("department") ?>
                                                    : </b><?= ($lang == "english") ? $result->department_name : $result->department_name_ar; ?>
                                                <br>
                                                <b><?= lang("direct_manager") ?> : </b>
                                                <?php foreach ($all as $em): ?>
                                                    <?php if ($em->employee_id == $result->direct_manager_id): ?>
                                                        <?= ($lang == "english") ? $em->full_name_en : $em->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <br>
                                                -----------------------
                                                <br>
                                                <?php if ($lang == "english"): ?>
                                                    <?php $months = array("All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"); ?>
                                                <?php else: ?>
                                                    <?php $months = array("الكل", "جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"); ?>
                                                <?php endif; ?>
                                                <b><?= lang("the_report") ?> : </b> <?= lang("accounting") ?><br>
                                                <b><?= lang("month") ?> : </b> <?= $months[intval($month)] ?><br>
                                                <b><?= lang("year") ?>
                                                    : </b> <?= ($year == 0) ? lang("all") : $year; ?>
                                                <br>
                                            </div>
                                        </td>
                                        <td width="120px"><img src="<?= base_url() ?>img/logo.jpg" style="float: left;">
                                        </td>
                                    </tr>
                                </table>


                                <div style="clear:both;">
                                    <div style="height: 60px;"></div>
                                    <h2 class="text-center"><?= lang("accounting") ?></h2><br>
                                </div>
                                <div class="content">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('date') ?></th>
                                            <th class="text-center"><?= lang('sender') ?></th>
                                            <th class="text-center"><?= lang('employee_name') ?></th>
                                            <th class="text-center"><?= lang('accounting_title') ?></th>
                                            <th class="text-center" width="20%"><?= lang('accounting_text') ?></th>
                                            <th class="text-center" width="20%"><?= lang('employee_reply') ?></th>
                                            <th class="text-center" width="20%"><?= lang('final_decision') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? $i = 0; ?>
                                        <?php foreach ($accountings as $acc): ?>
                                            <?php if ($acc->to_employee == $result->employee_id): ?>
                                                <?php if (strpos($acc->created_date, $filter) !== false) : $i++; ?>
                                                    <tr>
                                                        <td><?= $acc->created_date ?></td>
                                                        <td>
                                                            <?php foreach ($all as $emp): ?>
                                                                <?php if ($emp->employee_id == $acc->from_employee): ?>
                                                                    <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td>
                                                            <?php foreach ($all as $emp): ?>
                                                                <?php if ($emp->employee_id == $acc->to_employee): ?>
                                                                    <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td><?= $acc->accounting_title ?></td>
                                                        <td><?= $acc->accounting_text ?></td>
                                                        <td><?= $acc->employee_reply ?></td>
                                                        <td><?= $acc->final_decision ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <? if ($i == 0): ?>
                                            <tr>
                                                <td class="text-center" colspan="7"><?= lang("no-exist") ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <div style="margin: 30px auto; color: red"><?= lang("no-exist") ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>


