<?php $lang = $this->session->userdata('lang'); ?>
<script type="text/javascript" src="<?= base_url() ?>asset/js/jquery-ui.min.js"></script>
<style type="text/css">






</style>
<div class="row">

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <!-- Buttons -->
            <div class="buttons">
                <div class="buttons-container">
                    <button class="btn btn-success" id="show" data-toggle="modal"><i class="fa fa-list-alt"></i>
                    </button>
                    <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                    <button class="btn btn-dropbox" onclick="submit_form()"><i class="fa fa-save"></i></button>
                    <h3><?= (empty(@$approvals_cat_info)) ? lang('new_approval_chain') : (($lang == 'arabic') ? @$approvals_cat_info->approval_cat_ar : @$approvals_cat_info->approval_cat_en); ?></h3>
                </div>
            </div>
            <!-- Buttons -->

            <div id="accordion" class="panel-group">

                <!---------- PANEL 1 ---------->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               onclick="show(1)"><?= lang('approvals_data') ?></a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form role="form" id="form"
                                  action=""
                                  method="post" class="form-horizontal form1">


                                <!-- Description Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="approval_cat_ar" id="name_ar" class="form-control"
                                               id="field-1" required=""
                                               value="">
                                    </div>
                                </div>

                                <!-- Description English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="approval_cat_en" id="name_ar" class="form-control"
                                               id="field-1" required=""
                                               value="">
                                    </div>
                                </div>

                                <!-- Status -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="approval_cat_status" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="1" >
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="approval_cat_status" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="0" >
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"></span>
                                        </label>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!---------- PANEL 1 ---------->
                <?php if (!empty(@$approvals_cat_info)): ?>
                    <!---------- PANEL 2 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                   onclick="show(2)"><?= lang('approvals_scope') ?></a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">

                                    <!-- Job Place -->
                                    <div class="form-group">
                                        <label for="x1" class="control-label col-sm-3"><?= lang('job_place') ?></label>
                                        <div class="col-sm-5">
                                            <select name="jplc" id="x1" class="form-control">
                                                <option value="0"><?= lang('all') ?></option>
                                                <?php foreach ($jplaces as $jplc): ?>
                                                    <option value="<?= $jplc->job_place_id ?>" <?= (@$approvals_cat_info->jplc == $jplc->job_place_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $jplc->place_name_ar : $jplc->place_name_en; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Departement -->
                                    <div class="form-group">
                                        <label for="x2" class="control-label col-sm-3"><?= lang('department') ?></label>
                                        <div class="col-sm-5">
                                            <select name="dep" id="x2" class="form-control"
                                                    onchange="print_sects(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                                <?php foreach ($deps as $dep): ?>
                                                    <option value="<?= $dep->department_id ?>" <?= (@$approvals_cat_info->dep == $dep->department_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Sections -->
                                    <div class="form-group">
                                        <label for="x3"
                                               class="control-label col-sm-3"><?= lang('designation') ?></label>
                                        <div class="col-sm-5">
                                            <select name="sect" id="x3" class="form-control"
                                                    onchange="print_units(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_sects"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                    <!-- Units -->
                                    <div class="form-group">
                                        <label for="x4" class="control-label col-sm-3"><?= lang('aunit') ?></label>
                                        <div class="col-sm-5">
                                            <select name="unit" id="x4" class="form-control"
                                                    onchange="print_divis(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_units"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                    <!-- Divisions -->
                                    <div class="form-group">
                                        <label for="x5" class="control-label col-sm-3"><?= lang('adivision') ?></label>
                                        <div class="col-sm-5">
                                            <select name="divi" id="x5" class="form-control">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_divis"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 2 ---------->

                    <!---------- PANEL 3 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                   onclick="show(3)"><?= lang('approvals_comm_adm') ?></a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval2/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <p><b><?= lang('choose_admin') ?> : </b></p>

                                    <table class="table table-responsive" id="admin_table">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width='10%'><?= lang('action') ?></th>
                                            <th width='40%'><?= lang('responsability') ?></th>
                                            <th width='50%'><?= lang('name') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center"><input type="hidden" name="admins[]"
                                                                           value="<?= @$hradmin->employee_id ?>"></td>
                                            <td><?= lang('hr_admin') ?></td>
                                            <td><?= ($lang == 'arabic') ? @$hradmin->full_name_ar : @$hradmin->full_name_en; ?></td>
                                        </tr>
                                        <?php if (@$approvals_cat_info->admins != ''): ?>
                                            <?php foreach (explode(';', @$approvals_cat_info->admins) as $adm): ?>
                                                <?php if ($adm != @$hradmin->employee_id): ?>
                                                    <tr>
                                                        <td class="text-center" style="color:red">
                                                            <i class="fa fa-times" style="cursor:pointer;"
                                                               onclick="$(this).closest('tr').remove()"></i>
                                                            <input type="hidden" name="admins[]" value="<?= $adm ?>">
                                                        </td>
                                                        <?php foreach ($employees as $emp): ?>
                                                            <?php if ($emp->employee_id == $adm): ?>
                                                                <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                                <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 3 ---------->

                    <!---------- PANEL 4 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
                                   onclick="show(4)"><?= lang('approvals_comm_emp') ?></a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval3/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <p><b><?= lang('choose_employees') ?> : </b></p>
                                    <table class="table table-responsive" id="employee_table">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width='10%'><?= lang('action') ?></th>
                                            <th width='40%'><?= lang('job_title') ?></th>
                                            <th width='50%'><?= lang('name') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center"><input type="hidden" name="employees[]" value="dm">
                                            </td>
                                            <td><?= lang('direct_manager') ?></td>
                                            <td><?= lang('direct_manager') ?></td>
                                        </tr>
                                        <?php foreach (explode(';', @$approvals_cat_info->employees) as $empx): ?>
                                            <?php if ($empx != 'dm'): ?>
                                                <tr>
                                                    <td class="text-center" style="color:red">
                                                        <i class="fa fa-times" style="cursor:pointer;"
                                                           onclick="$(this).closest('tr').remove()"></i>
                                                        <input type="hidden" name="employees[]" value="<?= $empx ?>">
                                                    </td>
                                                    <?php foreach ($employees_non_admins as $emp): ?>
                                                        <?php if ($emp->employee_id == $empx): ?>
                                                            <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                            <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 4 ---------->

                    <!---------- PANEL 5 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
                                   onclick="show(5)"><?= lang('approvals_comm_squence') ?></a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">

                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval4/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <th width="11%" class="text-center"><?= lang('order') ?></th>
                                        <th width="28%" class="text-center"><?= lang('comm_members') ?></th>
                                        <th width="29%" class="text-center"><?= lang('member_type') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_accept') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_refuse') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_delegate') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_consultation') ?></th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $all_list = explode(';', $approvals_cat_info->all_list); ?>
                                        <?php
                                        $n = count($all_list);
                                        $accept = array_fill(0, $n, 0);
                                        $refuse= array_fill(0, $n, 0);
                                        $delegate = array_fill(0, $n, 0);
                                        $consultation = array_fill(0, $n, 0);
                                        ?>
                                        <?php $accept = array_merge(explode(';', $approvals_cat_info->accept), $accept); ?>
                                        <?php $refuse = array_merge(explode(';', $approvals_cat_info->refuse), $refuse); ?>
                                        <?php $delegate = array_merge(explode(';', $approvals_cat_info->delegate), $delegate); ?>
                                        <?php $consultation = array_merge(explode(';', $approvals_cat_info->consultation), $consultation); ?>

                                        <?php for ($i = 0; $i < count($all_list); $i++): ?>
                                            <tr id="<?= $i + 1 ?>">
                                                <td class="text-center order">
                                                    <?= $i + 1 ?>
                                                </td>
                                                <?php if ($all_list[$i] == 'dm'): ?>
                                                    <td><?= lang('direct_manager') ?><input type="hidden"
                                                                                            name="all_list[]"
                                                                                            value="<?= $all_list[$i] ?>">
                                                    </td>
                                                    <td><?= lang('direct_manager') ?></td>
                                                <?php else: ?>
                                                    <?php foreach ($employees as $emp): ?>
                                                        <?php if ($emp->employee_id == $all_list[$i]): ?>
                                                            <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                                <input type="hidden" name="all_list[]"
                                                                       value="<?= $all_list[$i] ?>"></td>
                                                            <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($accept[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $accept[$i] ?>" name="accept[]">
                                                </td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($refuse[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $refuse[$i] ?>" name="refuse[]">
                                                </td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($delegate[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $delegate[$i] ?>"
                                                            name="delegate[]"></td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($consultation[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $consultation[$i] ?>"
                                                            name="consultation[]"></td>
                                            </tr>
                                        <?php endfor; ?>

                                        </tbody>
                                    </table>
                                </form>
                                <label class="control-label" for="checkall">
                                    <input type="checkbox" class="checkall" name="checkall">
                                    <?= lang('all') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 5 ---------->
                <?php endif; ?>
            </div>
        </div>
    </div>





