
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#social_list" data-toggle="tab"><?= lang('social_list') ?></a></li>
                <?php if ($active == 2): ?>
                    <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_social"  data-toggle="tab"><?= lang('add_social') ?></a></li>
                <?php endif; ?>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="social_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th class="text-center"><?= lang('sl') ?></th>
                                        <th class="text-center"><?= lang('description_ar') ?></th>
                                        <th class="text-center"><?= lang('description_en') ?></th>
                                        <th class="text-center"><?= lang('insurance_percent') ?></th>
                                        <th class="text-center"><?= lang('societe_percent') ?></th>
                                        <th class="text-center"><?= lang('habit_insurance') ?></th>
                                        <th class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($sociales_info)): $sl = 1; ?>
                                        <?php foreach ($sociales_info as $soc): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td style="direction: rtl;"><?= $soc->title_ar; ?></td>
                                                <td style="direction: ltr;"><?= $soc->title_en; ?></td>
                                                <td class="text-center"><?= $soc->insurance_percent; ?> %</td>
                                                <td class="text-center"><?= $soc->societe_percent; ?> %</td>
                                                <td class="text-center"><?= ($soc->habit_insurance == 1) ? '<i class="fa fa-check" style="color:#24e624"></i>' : '<i class = "fa fa-times" style="color:red"></i>'; ?></td>

                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/insurances/social_list/<?php echo $soc->social_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <?php if ($active == 2): ?>
                    <!-- Add Section Starts -->
                    <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_social" style="position: relative;">
                        <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                            <div class="box-body">
                                <form role="form" id="form" action="<?php echo base_url(); ?>admin/insurances/save_social/<?= @$social_info->social_id; ?> " method="post" class="form-horizontal">
                                    <!-- ID for update -->
                                    <?php if (!empty($medicale_info->extra_work_id)): ?>
                                        <input type="hidden" name="social_id"  class="form-control" id="field-1" value="<?= @$social_info->social_id; ?>"/>
                                    <?php endif; ?>
                                    <!-- Nom Arabe -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('social_title_ar') ?><span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?= @$social_info->title_ar; ?>" required=""/>
                                        </div>
                                    </div>
                                    <!-- Nom English -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('social_title_en') ?><span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="title_en"  class="form-control" id="field-1" value="<?= @$social_info->title_en; ?>" required=""/>
                                        </div>
                                    </div>
                                    <!-- Nom English -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('insurance_percent') ?> (%)<span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" max="100" name="insurance_percent"  class="form-control" id="field-1" value="<?= @$social_info->insurance_percent; ?>" required=""/>
                                        </div>
                                    </div>
                                    <!-- societe_percent -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('societe_percent') ?> (%)<span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" min="0" max="100" name="societe_percent"  class="form-control" id="field-1" value="<?= @$social_info->societe_percent; ?>" required=""/>
                                        </div>
                                    </div>
                                    <!-- habit_insurance -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('habit_insurance') ?> (%)<span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <select name="habit_insurance" class="form-control">
                                                <option value="1" <?= (@$social_info->habit_insurance == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                <option value="0" <?= (@$social_info->habit_insurance == 0) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                            </select>
                                        </div>
                                    </div>


                                    <!-- Submit Button -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Add Section Ends -->
                <?php endif; ?>


            </div>
        </div>
    </div>
</div>