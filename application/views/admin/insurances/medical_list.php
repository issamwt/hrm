<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#medical_list" data-toggle="tab"><?= lang('medical_list') ?></a></li>
                <?php if ($active == 2): ?>
                    <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_medical"  data-toggle="tab"><?= lang('edit_medical') ?></a></li>
                <?php endif; ?>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="medical_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th  class="text-center"><?= lang('name_ar') ?></th>
                                        <th class="text-center"><?= lang('name_en') ?></th>
                                        <th class="text-center"><?= lang('insurance_percent') ?></th>
                                        <th class="text-center"><?= lang('societe_percent') ?></th>
                                        <th width="5%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($medicales_info)): $sl = 1; ?>
                                        <?php foreach ($medicales_info as $med): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td style="direction: rtl;"><?= $med->title_ar; ?></td>
                                                <td style="direction: ltr;"><?= $med->title_en; ?></td>
                                                <td class="text-center"><?= $med->m_insurance_percent; ?> %</td>
                                                <td class="text-center"><?= $med->m_societe_percent; ?> %</td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/insurances/medical_list/<?php echo $med->medical_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <?php if ($active == 2): ?>
                    <!-- Add Section Starts -->
                    <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_medical" style="position: relative;">
                        <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                            <div class="box-body">
                                <form role="form" id="form" action="<?php echo base_url(); ?>admin/insurances/save_medical/<?= @$medicale_info->medical_id; ?> " method="post" class="form-horizontal">
                                    <!-- Bonuse Id for Update -->
                                    <?php if (!empty($medicale_info->extra_work_id)): ?>
                                        <input type="hidden" name="medical_id"  class="form-control" id="field-1" value="<?= @$medicale_info->medical_id; ?>"/>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?><span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?= @$medicale_info->title_ar; ?>" required=""/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?><span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="title_en"  class="form-control" id="field-1" value="<?= @$medicale_info->title_en; ?>" required=""/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('insurance_percent') ?> (%)<span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" step="0.01" min="0" max="100" name="m_insurance_percent"  class="form-control" id="field-1" required="" value="<?= @$medicale_info->m_insurance_percent; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('societe_percent') ?> (%)<span class="required">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="number" step="0.01" min="0" max="100" name="m_societe_percent"  class="form-control" id="field-1" required="" value="<?= @$medicale_info->m_societe_percent; ?>" />
                                        </div>
                                    </div>
                                    <!-- Submit Button -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Add Section Ends -->
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>