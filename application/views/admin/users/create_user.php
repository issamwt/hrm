<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.default.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/kendo.common.min.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/kendo.all.min.js"></script>
<style type="text/css">
    .depsec{
        display: none;
    }
    .list-group-item.disabled.active{
        font-weight: bold;
        font-size: 1.2em;
        cursor: default !important;
    }
    .list-group-item:hover{
        /*background-color: #fff !important;*/
    }
</style>
<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/users/users_list/" class="btn btn-primary"><i class="fa fa-plus"></i> <?= lang('users_list') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <div class="box-title">
                <h3 class="box-title" style="margin-top: 0px; margin-bottom: 20px"><?= (!empty(@$user_info)) ? lang('edit_user') : lang('add_user'); ?></h3>
            </div>

            <form role="form"  id="form_validation" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/users/save_user/<?= @$user_info->user_id ?>" method="post" class="form-horizontal form-groups-bordered">
                <div class="row">
                    <!-- First Div -->
                    <div class="col-sm-6">
                        <div  class="list-group">
                            <a href="#" class="list-group-item disabled active"><?= lang('user_data') ?></a>
                            <div href="#" class="list-group-item options">
                                <!-- Id for Update -->
                                <?php if (!empty(@$user_info)): ?>
                                    <input type="hidden" id="user_id" value="<?= @$user_info->user_id ?>"><br>
                                <?php endif; ?>
                                <!-- Name in Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?><span class="required"> *</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="user_name_ar" class="form-control" value="<?= @$user_info->user_name_ar ?>" required="">
                                    </div>
                                </div>

                                <!-- Name in English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?><span class="required"> *</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="user_name_en" class="form-control col-sm-8" value="<?= @$user_info->user_name_en ?>" required="">
                                    </div>
                                </div>

                                <!-- Username -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('email') ?><span class="required"> *</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" class="form-control col-sm-8" value="<?= @$user_info->email ?>" required="">
                                    </div>
                                </div>

                                <!-- Username -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('username') ?><span class="required"> *</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="user_name" class="form-control col-sm-8" value="<?= @$user_info->user_name ?>" required="">
                                    </div>
                                </div>

                                <!-- Password -->
                                <?php if (empty(@$user_info)): ?>
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label"><?= lang('password') ?><span class="required"> *</span></label>
                                        <div class="col-sm-9">
                                            <input type="password" name="password" class="form-control col-sm-8" value="" required="">
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <!-- User Type -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('user_type') ?><span class="required"> *</span></label>
                                    <div class="col-sm-9">
                                        <select type="text" id="owner" name="owner" class="form-control col-sm-8" required="">
                                            <option value=""><?= lang('choose_user_type') ?></option>
                                            <option value="1" <?= (@$user_info->owner == 1) ? 'selected' : ''; ?>><?= lang('super_admin') ?></option>
                                            <option value="2" <?= (@$user_info->owner == 2) ? 'selected' : ''; ?>><?= lang('guest_admin') ?></option>
                                        </select>
                                    </div>
                                </div>

                                <!--Submit -->
                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Second Div -->
                    <div class="col-sm-6">
                        <div id="roll" class="list-group">
                            <a href="#" class="list-group-item disabled active">
                                <?= lang('user_permissions') ?>
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="k-header">
                                    <div class="box-col">
                                        <div id="treeview"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $("#treeview").kendoTreeView({
    checkboxes: {
    checkChildren: true,
            template: "<input type='checkbox' #= item.check# name='menu[]' value='#= item.value #'  />"

    },
            check: onCheck,
            dataSource: [
<?php foreach ($result as $parent => $v_parent): ?>
    <?php if (is_array($v_parent)): ?>
        <?php foreach ($v_parent as $parent_id => $v_child): ?>
                        {
                        id: "", text: "<?php echo lang($parent); ?>", value: "<?php
            if (!empty($parent_id)) {
                echo $parent_id;
            }
            ?>", expanded: false, items: [
            <?php foreach ($v_child as $child => $v_sub_child) : ?>
                <?php if (is_array($v_sub_child)): ?>
                    <?php foreach ($v_sub_child as $sub_chld => $v_sub_chld): ?>
                                    {
                                    id: "", text: "<?php echo lang($child); ?>", value: "<?php
                        if (!empty($sub_chld)) {
                            echo $sub_chld;
                        }
                        ?>", expanded: false, items: [
                        <?php foreach ($v_sub_chld as $sub_chld_name => $sub_chld_id): ?>
                                        {
                                        id: "", text: "<?php echo lang($sub_chld_name); ?>",<?php
                            if (!empty(@$roll[$sub_chld_id])) {
                                echo @$roll[$sub_chld_id] ? 'check: "checked",' : '';
                            }
                            ?> value: "<?php
                            if (!empty($sub_chld_id)) {
                                echo $sub_chld_id;
                            }
                            ?>",
                                        },
                        <?php endforeach; ?>
                                    ]
                                    },
                    <?php endforeach; ?>
                <?php else: ?>
                                {
                                id: "", text: "<?php echo lang($child); ?>", <?php
                    if (!is_array($v_sub_child)) {
                        if (!empty(@$roll[$v_sub_child])) {
                            echo @$roll[$v_sub_child] ? 'check: "checked",' : '';
                        }
                    }
                    ?> value: "<?php
                    if (!empty($v_sub_child)) {
                        echo $v_sub_child;
                    }
                    ?>",
                                },
                <?php endif; ?>
            <?php endforeach; ?>
                        ]
                        },
        <?php endforeach; ?>
    <?php else: ?>
                    { <?php if ($parent == 'Dashboard') {
            ?>
                        id: "", text: "<?php echo lang($parent) ?>", <?php echo 'check: "checked",';
            ?>  value: "<?php
            if (!is_array($v_parent)) {
                echo $v_parent;
            }
            ?>"
            <?php
        } else {
            ?>
                        id: "", text: "<?php echo lang($parent) ?>", <?php
            if (!is_array($v_parent)) {
                if (!empty(@$roll[$v_parent])) {
                    echo @$roll[$v_parent] ? 'check: "checked",' : '';
                }
            }
            ?> value: "<?php
            if (!is_array($v_parent)) {
                echo $v_parent;
            }
            ?>"
        <?php }
        ?>
                    },
    <?php endif; ?>
<?php endforeach; ?>
            ]
    });
    // show checked node IDs on datasource change
    function onCheck() {
    var checkedNodes = [],
            treeView = $("#treeview").data("kendoTreeView"),
            message;
    checkedNodeIds(treeView.dataSource.view(), checkedNodes);
    $("#result").html(message);
    }

</script>

<script type="text/javascript">
// hide dashboard checkbox
    $(function () {
    $("#treeview .k-checkbox input").eq(0).hide();
    $('form').submit(function () {
    $('#treeview :checkbox').each(function () {
    if (this.indeterminate) {
    this.checked = true;
    }
    });
    })
    })
</script>

<?php if (!empty(@$user_info)): ?>
    <?php if (@$user_info->owner == 4): ?>
        <script>
                    $(function(){
                    $("#form_validation").find(".depsec.dep select, .depsec.opt input").prop('disabled', false);
                    $("#form_validation").find(".depsec.dep, .depsec.opt").slideDown("slow");
                    });</script>
    <?php elseif (@$user_info->owner == 5 or @ $user_info->owner == 6): ?>
        <script>
            $("#form_validation").find(".depsec.sec select, .depsec.opt input").prop('disabled', false);
            $("#form_validation").find(".depsec.sec, .depsec.opt").slideDown("slow");</script>
    <?php endif; ?>
<?php endif; ?>