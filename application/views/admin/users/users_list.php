<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/users/add_user/" class="btn btn-primary"><i class="fa fa-plus"></i> <?= lang('add_user') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                <th width="3%" class="text-center"><?= lang('sl') ?></th>
                <th width="15%" class="text-center"><?= lang('name') ?></th>
                <th width="16%" class="text-center"><?= lang('username') ?></th>
                <th width="20%" class="text-center"><?= lang('email') ?></th>
                <th width="21%" class="text-center"><?= lang('user_type') ?></th>
                <th width="5%" class="text-center"><?= lang('status') ?></th>
                <th width="20%" class="text-center"><?= lang('action') ?></th>
                </thead>
                <tbody>
                    <?php if (!empty($users_list)): ?>
                        <?php foreach ($users_list as $user): $sl = 1; ?>
                            <tr>
                                <td class="text-center"><?= $sl++; ?></td>
                                <td><?= ($lang == 'arabic') ? $user->user_name_ar : $user->user_name_en; ?></td>
                                <td><?= $user->user_name ?></td>
                                <td><?= $user->email ?></td>
                                <td>
                                    <?php
                                    switch ($user->owner) {
                                        case 1:echo lang('super_admin');
                                            break;
                                        case 2:echo lang('guest_admin');
                                            break;
                                        default:echo '';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?=
                                    ($user->user_status == 1) ? '<span class="label label-success">' . lang("active") . '</span>' : '<span class="label label-danger">' . lang("inactive") . '</span>';
                                    ?>
                                </td>
                                <td>
                                    <a data-original-title="<?= lang('view_details') ?>" href="#"  class="btn btn-success btn-xs"  data-placement="top"  data-toggle="modal" data-target="#myModal<?= $user->user_id ?>"><i class="fa fa-eye"></i> <?= lang('view_details') ?></a>
                                    <?php if ($this->session->userdata('user_type') == 1): ?>
                                        <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/users/add_user/<?= $user->user_id ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" ><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                        <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/users/delete_user/<?= $user->user_id ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');" <?= ($user->user_id == 1) ? 'disabled' : ''; ?>><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    <?php else: ?><!-- prochainement -->
                                        <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/users/add_user/<?= $user->user_id ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                        <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/users/delete_user/<?= $user->user_id ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" disabled><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="6" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- Modal For Users -->
<?php if (!empty($users_list)): foreach ($users_list as $user) : ?>
        <div class="modal fade" id="myModal<?= $user->user_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="font-size: 1.2em;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center" id="myModalLabel"><?= $user->user_name ?></h4>
                    </div>
                    <div class="modal-body">
                        <b><?= lang('name') ?> :</b><br>
                        <?= ($lang == 'arabic') ? $user->user_name_ar : $user->user_name_en; ?><br>
                        <b><?= lang('email') ?> :</b><br>
                        <?= $user->email; ?><br>
                        <b><?= lang('user_type') ?> :</b><br>
                        <?php
                        switch ($user->owner) {
                            case 1:echo lang('super_admin') . '<br>';
                                break;
                            case 2:echo lang('guest_admin') . '<br>';
                                break;
                            default:echo '<br>';
                                break;
                        }
                        ?>
                        <b><?= lang('status') ?> : </b><br>
                        <?= ($user->user_status == 1) ? '<span class="label label-success">' . lang("active") . '</span><br>' : '<span class="label label-danger">' . lang("inactive") . '</span><br>'; ?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
                        <a  href="<?php echo base_url() ?>admin/users/add_user/<?= $user->user_id ?>" class="btn btn-primary"><?= lang('edit') ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<!-- Modal For Users -->