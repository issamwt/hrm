<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    i.fa.fa-check.type {
        color: #24e624;
        font-size: 1.4em;
    }

    .comment {
        background: #eee;
        color: #7d7c7c;
        font-size: 0.8em;
        padding: 10px;
        width: 100%;
        border: 1px solid #d2d2d2;
        margin: 10px auto;

    }

    table.comment td {
        padding: 5px;
        text-align: center;
    }

    h4 {
        cursor: pointer;
    }

    h4:hover {
        background: #2C3B41;
    }

    h3 {
        font-size: 19px;
        background: #337ab7;
        padding: 10px;
        color: #fff;
        margin: 10px 0px;
        cursor: pointer;
    }

    h3:hover {
        background: #1a80b6;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#items_list"
                                                                   data-toggle="tab"><?= lang('all_items') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_item"
                                                                   data-toggle="tab"><?= lang('new_item') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Items List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="items_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <h4 data-toggle="collapse"
                                data-target="#according_to_job_titles"><?= lang("according_to_job_titles") ?></h4>
                            <div id="according_to_job_titles" class="collapse">
                                <?php if (!empty($job_titles)): ?>
                                    <?php foreach ($job_titles as $jt): ?>
                                        <?php $x = 0; ?>
                                        <? foreach ($evaluation_items as $value): ?>
                                            <? if ($value->job_titles_id == $jt->job_titles_id): ?>
                                                <?php $x++; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <h3 data-toggle="collapse" data-target="#atjt<?= $jt->job_titles_id ?>">
                                            <?= ($lang == 'arabic') ? $jt->job_titles_name_ar : $jt->job_titles_name_en; ?> <?= ($x != 0) ? '(' . $x . ')' : ''; ?>
                                        </h3>
                                        <table id="atjt<?= $jt->job_titles_id ?>"
                                               class="table table-bordered collapse">
                                            <thead>
                                            <th width="2%" class="text-center"><?= lang('sl') ?></th>
                                            <th width="23%" class="text-center"><?= lang('name_ev_item') ?></th>
                                            <th width="23%" class="text-center"><?= lang('description') ?></th>
                                            <th width="8%" class="text-center"><?= lang('percentage') ?></th>
                                            <th width="8%" class="text-center"><?= lang('rating') ?></th>
                                            <th width="13%" class="text-center"><?= lang('action') ?></th>
                                            </thead>
                                            <tbody>
                                            <?php $sl = 1; ?>
                                            <? foreach ($evaluation_items as $value): ?>
                                                <? if ($value->job_titles_id == $jt->job_titles_id): ?>
                                                    <tr>
                                                        <td class="text-center"><?= $sl++; ?></td>
                                                        <td><?= ($lang == 'arabic') ? $value->name_ar : $value->name_en; ?></td>
                                                        <td><?= ($lang == 'arabic') ? $value->ei_desc_ar : $value->ei_desc_en; ?></td>
                                                        <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 1) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                        <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 2) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                        <td>
                                                            <a data-original-title="<?= lang('edit') ?>"
                                                               href="<?php echo base_url() ?>admin/settings2/evaluation_items/<?= $value->evaluation_items_id; ?>"
                                                               class="btn btn-primary btn-xs" title=""
                                                               data-toggle="tooltip"
                                                               data-placement="top"><i
                                                                        class="fa fa-pencil-square-o"></i> <?= lang('edit') ?>
                                                            </a>
                                                            <a data-original-title="<?= lang('delete') ?>"
                                                               href="<?php echo base_url() ?>admin/settings2/delete_evaluation_item/<?= $value->evaluation_items_id; ?>"
                                                               class="btn btn-danger btn-xs" title=""
                                                               data-toggle="tooltip"
                                                               data-placement="top"
                                                               onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                        class="fa fa-trash-o"></i> <?= lang('delete') ?>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                            <? if ($sl == 1): ?>
                                                <tr>
                                                    <td class="text-center"
                                                        colspan="7"><?= lang("no-exist"); ?></td>
                                                </tr>
                                            <? endif; ?>
                                            </tbody>
                                        </table>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                            <br>
                            <h4 data-toggle="collapse"
                                data-target="#according_to_departments"><?= lang("according_to_departments") ?></h4>
                            <div id="according_to_departments" class="collapse">
                                <?php foreach (@$all_departs_ev_items as $akey => $dep): ?>
                                    <?php $y = 0; ?>
                                    <?php foreach ($dep as $key => $value): ?>
                                        <?php if (!empty($value->evaluation_items_id)): ?>
                                            <?php $y++; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <h3 data-toggle="collapse"
                                        data-target="#atd<?= $all_departements[$akey]->department_id ?>"><?= ($lang == 'arabic') ? $all_departements[$akey]->department_name_ar : $all_departements[$akey]->department_name; ?> <?= ($y != 0) ? '(' . $y . ')' : ''; ?>
                                    </h3>
                                    <!-- Table -->
                                    <table class="table table-bordered collapse"
                                           id="atd<?= $all_departements[$akey]->department_id ?>">
                                        <thead>
                                        <th width="2%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="30%" class="text-center"><?= lang('name_ev_item') ?></th>
                                        <th width="39%" class="text-center"><?= lang('description') ?></th>
                                        <th width="8%" class="text-center"><?= lang('percentage') ?></th>
                                        <th width="8%" class="text-center"><?= lang('rating') ?></th>
                                        <th width="13%" class="text-center"><?= lang('action') ?></th>
                                        </thead>
                                        <tbody>
                                        <?php $sl = 1; ?>
                                        <?php foreach ($dep as $key => $value): ?>
                                            <?php if (!empty($value->evaluation_items_id)): ?>
                                                <tr>
                                                    <td class="text-center"><?= $sl++; ?></td>
                                                    <td><?= ($lang == 'arabic') ? $value->name_ar : $value->name_en; ?></td>
                                                    <td><?= ($lang == 'arabic') ? $value->ei_desc_ar : $value->ei_desc_en; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 1) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                    <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 2) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                    <td>
                                                        <a data-original-title="<?= lang('edit') ?>"
                                                           href="<?php echo base_url() ?>admin/settings2/evaluation_items/<?= $value->evaluation_items_id; ?>"
                                                           class="btn btn-primary btn-xs" title=""
                                                           data-toggle="tooltip"
                                                           data-placement="top"><i
                                                                    class="fa fa-pencil-square-o"></i> <?= lang('edit') ?>
                                                        </a>
                                                        <a data-original-title="<?= lang('delete') ?>"
                                                           href="<?php echo base_url() ?>admin/settings2/delete_evaluation_item/<?= $value->evaluation_items_id; ?>"
                                                           class="btn btn-danger btn-xs" title=""
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                    class="fa fa-trash-o"></i> <?= lang('delete') ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php else : ?>
                                                <tr>
                                                    <td colspan="6"
                                                        class="text-center"><?= lang('nothing_to_display') ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endforeach; ?>
                            </div>
                            <br>
                            <h4 data-toggle="collapse"
                                data-target="#general_items"><?= lang("general_items") ?></h4>
                            <div id="general_items" class="collapse">
                                <table id="atjt<?= $jt->job_titles_id ?>" class="table table-bordered">
                                    <thead>
                                    <th width="2%" class="text-center"><?= lang('sl') ?></th>
                                    <th width="23%" class="text-center"><?= lang('name_ev_item') ?></th>
                                    <th width="23%" class="text-center"><?= lang('description') ?></th>
                                    <th width="8%" class="text-center"><?= lang('percentage') ?></th>
                                    <th width="8%" class="text-center"><?= lang('rating') ?></th>
                                    <th width="13%" class="text-center"><?= lang('action') ?></th>
                                    </thead>
                                    <tbody>
                                    <?php $sl = 1; ?>
                                    <? foreach ($evaluation_items as $value): ?>
                                        <? if ($value->job_titles_id == 0 and $value->department_id == 0): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl++; ?></td>
                                                <td><?= ($lang == 'arabic') ? $value->name_ar : $value->name_en; ?></td>
                                                <td><?= ($lang == 'arabic') ? $value->ei_desc_ar : $value->ei_desc_en; ?></td>
                                                <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 1) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                <td style="text-align:center;vertical-align:middle;"><?= ($value->type == 2) ? '<i class="fa fa-check type"></i>' : ''; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>"
                                                       href="<?php echo base_url() ?>admin/settings2/evaluation_items/<?= $value->evaluation_items_id; ?>"
                                                       class="btn btn-primary btn-xs" title=""
                                                       data-toggle="tooltip"
                                                       data-placement="top"><i
                                                                class="fa fa-pencil-square-o"></i> <?= lang('edit') ?>
                                                    </a>
                                                    <a data-original-title="<?= lang('delete') ?>"
                                                       href="<?php echo base_url() ?>admin/settings2/delete_evaluation_item/<?= $value->evaluation_items_id; ?>"
                                                       class="btn btn-danger btn-xs" title=""
                                                       data-toggle="tooltip"
                                                       data-placement="top"
                                                       onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                class="fa fa-trash-o"></i> <?= lang('delete') ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                    <? if ($sl == 1): ?>
                                        <tr>
                                            <td class="text-center"
                                                colspan="7"><?= lang("no-exist"); ?></td>
                                        </tr>
                                    <? endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Items List tab Ends -->

                <!-- Add Item tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_item" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <form id="form"
                                  action="<?php echo base_url() ?>admin/settings2/save_evaluation_item/<?= @$evaluation_item_info->evaluation_items_id ?>"
                                  method="post" class="form-horizontal form-groups-bordered">

                                <!-- Leave Category ID For Update -->
                                <input type="hidden" name="evaluation_items_id"
                                       value="<?= @$evaluation_item_info->evaluation_items_id ?>"/>

                                <!-- Name In Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_ar" value="<?= @$evaluation_item_info->name_ar ?>"
                                               class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Name In English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_en" value="<?= @$evaluation_item_info->name_en ?>"
                                               class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Departement -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('departement') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="department_id" class="form-control department_id" required="">
                                            <option value="0"><?= lang('all') ?>.....</option>
                                            <?php if (!empty($all_departements)): ?>
                                                <?php foreach ($all_departements as $dep): ?>
                                                    <option value="<?= $dep->department_id ?>" <?= (@$evaluation_item_info->department_id == $dep->department_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Job_titles -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('job_titles') ?></label>
                                    <div class="col-sm-5">
                                        <select name="job_titles_id" class="form-control job_titles_id" required="">
                                            <option value="0"><?= lang('all') ?>.....</option>
                                            <?php if (!empty($job_titles)): ?>
                                                <?php foreach ($job_titles as $jt): ?>
                                                    <option value="<?= $jt->job_titles_id ?>" <?= (@$evaluation_item_info->job_titles_id == $jt->job_titles_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $jt->job_titles_name_ar : $jt->job_titles_name_en; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Type -->
                                <div class="form-group type">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('ev_type') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block;">
                                            <input id="radioStacked1" name="type" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="1" <?= (@$evaluation_item_info->type == 1) ? 'checked' : ''; ?>>
                                            <?= lang('percentage') ?>
                                            <div class="comment"><?= lang('fromto') ?></div>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block;">
                                            <input id="radioStacked2" name="type" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="2" <?= (@$evaluation_item_info->type == 2) ? 'checked' : ''; ?>>
                                            <?= lang('rating') ?>
                                            <table class="comment" border="1" cellspacing="1">
                                                <tr>
                                                    <td>ممتاز</td>
                                                    <td>جيد جدا</td>
                                                    <td>جيد</td>
                                                    <td>متوسظ</td>
                                                    <td>ضعيف</td>
                                                    <td>ضعيف جدا</td>
                                                </tr>
                                                <tr>
                                                    <td>100%</td>
                                                    <td>80%</td>
                                                    <td>60%</td>
                                                    <td>40%</td>
                                                    <td>20%</td>
                                                    <td>0%</td>
                                                </tr>
                                            </table>
                                        </label>
                                    </div>
                                </div>

                                <!-- Desc In Arabic -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('description_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea rows="5" class="form-control"
                                                  name="ei_desc_ar"><?= @$evaluation_item_info->ei_desc_ar ?></textarea>
                                    </div>
                                </div>

                                <!-- Desc In English -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('description_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea rows="5" class="form-control"
                                                  name="ei_desc_en"><?= @$evaluation_item_info->ei_desc_en ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary"
                                                id="i_submit"><?= lang('save') ?></button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Item tab Ends -->
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('.job_titles_id').change(function () {
            if ($(this).val() != 0) {
                $('.department_id').val('0');
            }
        });
    });
</script>