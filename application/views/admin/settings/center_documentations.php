<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#doc_list"
                                                                   data-toggle="tab"><?= lang('all_documents') ?></a>
                </li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_doc"
                                                                   data-toggle="tab"><?= lang('add_document') ?></a>
                </li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="doc_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center"><?= lang('sl') ?></th>
                                    <th class="text-center"><?= lang('name_ar') ?></th>
                                    <th class="text-center"><?= lang('name_en') ?></th>
                                    <th width="19%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $key = 1 ?>
                                <?php if (!empty(@$document_list)): ?>
                                    <?php foreach (@$document_list as $doc) : ?>
                                        <tr>
                                            <td class="text-center"><?php echo $key ?></td>
                                            <td><?= $doc->doc_name_ar ?></td>
                                            <td><?= $doc->doc_name_en ?></td>
                                            <td><a href="<?= base_url() ?>img/center_documentations/<?= $doc->link ?>"
                                                   class="btn btn-success btn-xs"
                                                   target="_blank"> <i class="fa fa-download"></i>
												   <?= lang('download') ?>
												   </a>
                                                <a data-original-title="<?= lang('edit') ?>"
                                                   href="<?php echo base_url() ?>admin/settings2/center_documentations/<?= $doc->doc_id; ?>"
                                                   class="btn btn-primary btn-xs" title="" data-toggle="tooltip"
                                                   data-placement="top"><i
                                                            class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                <a data-original-title="<?= lang('delete') ?>"
                                                   href="<?php echo base_url() ?>admin/settings2/delete_document/<?= $doc->doc_id; ?>"
                                                   class="btn btn-danger btn-xs"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>

                                        </tr>
                                        <?php $key++; ?>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <td colspan="5">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Leave Category List tab Ends -->


                <!-- Add Category tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_doc" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <form id="form"
                                  action="<?php echo base_url() ?>admin/settings2/save_documents/<?= @$the_doc->doc_id; ?>"
                                  method="post" class="form-horizontal form-groups-bordered"
                                  enctype="multipart/form-data">

                                <?php if (!empty(@$the_doc)): ?>
                                    <!-- Leave Category ID For Update -->
                                    <input type="hidden" name="doc_id" value="<?= @$the_doc->doc_id ?>"/>
                                <?php endif; ?>

                                <!-- Name In Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="doc_name_ar" value="<?= @$the_doc->doc_name_ar; ?>"
                                               class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Name In English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="doc_name_en" value="<?= @$the_doc->doc_name_en; ?>"
                                               class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Link -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('link') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="btn btn-primary btn-file btn-block"><span
                                                        class="fileinput-new"><?= lang('select_file') ?></span>
                                                <span class="fileinput-exists"><?= lang('change') ?></span>
                                                <input type="file" name="link" value="<?= @$the_doc->link; ?>" class="form-control" >
                                            </div>
                                            <div class="fileinput-filename"><?= @$the_doc->link; ?></div>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                               style="float: none; color:red;">&times;</a>
                                        </div>
                                        <div id="msg_pdf" style="color: #e11221"></div>
										
										<div style="margin-top:10px; color:rgb(182, 172, 172)" col="col-sm-12"><small><?=lang('link_note')?></small></div>
                                    </div>
                                </div>

                                <!-- Submit -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary"
                                                id="i_submit"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Category tab Ends -->
            </div>
        </div>
    </div>
</div>
