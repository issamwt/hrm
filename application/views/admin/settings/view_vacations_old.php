<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#vacations_list" data-toggle="tab"><?= lang('vacations_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#grant_a_vacation"  data-toggle="tab"><?= lang('add_vacation') ?></a></li>
            </ul>
            <div class="tab-content no-padding">
                <!-- Vacations List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="vacations_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="20%" class="text-center"><?= lang('name') ?></th>
                                        <th class="text-center"><?= lang('vacation_type') ?></th>
                                        <th class="text-center"><?= lang('vacation_date_assign') ?></th>
                                        <th class="text-center"><?= lang('vacation_duration') ?></th>
                                        <th class="text-center"><?= lang('vacation_date_start') ?></th>
                                        <th class="text-center"><?= lang('vacation_date_end') ?></th>
                                        <th width="2%" class="text-center"><?= lang('notified') ?></th>
                                        <th width="20%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Vacations List Ends -->

                <!-- Grant a Vacation Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="grant_a_vacation" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="panel-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings/save_vacation/<?php if (!empty($vacation_info)) echo $vacation_info->job_titles_id; ?> " method="post" class="form-horizontal">

                                <!-- Vacation Id for Update -->
                                <input type="hidden" name="job_titles_id"  class="form-control" id="field-1" value="<?php if (!empty($vacation_info)) echo $vacation_info->job_titles_id; ?>"/>

                                <!-- Employee Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('employee_name') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="employee_id" class="form-control" id="field-1">
                                            <option value=""><?= lang('select_employee') ?>.....</option>
                                            <option value=""><?= lang('select_employee') ?>.....</option>
                                            <option value=""><?= lang('select_employee') ?>.....</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- Start Date -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('vacation_date_start') ?><span class="required">*</span></label>
                                    <div class="col-sm-5 input-group">
                                        <input type="text" name="date_start" value="" class="form-control hijri_datepicker" data-format="yyy-mm-dd">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="entypo-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Duration -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label"><?= lang('vacation_duration') ?><span class="required">*</span></label>
                                    <div class="col-sm-5 input-group">
                                        <span class="input-group-addon" title="* Days" id="priceLabel"><?= lang('date_days') ?></span>
                                        <?php
                                        $d = 31;
                                        $w = 5;
                                        $m = 7;
                                        ?>
                                        <select name="days" class="form-control days">
                                            <?php
                                            for ($i = 0; $i < 31; $i++)
                                                echo '<option>' . $i . '</option>';
                                            ?>
                                        </select>
                                        <span class="input-group-addon" title="* Weeks" id="priceLabel"><?= lang('date_weeks') ?></span>
                                        <select name="weeks" class="form-control weekss">
                                            <?php
                                            for ($i = 0; $i < 5; $i++)
                                                echo '<option>' . $i . '</option>';
                                            ?>
                                        </select>
                                        <span class="input-group-addon" title="* months" id="priceLabel"><?= lang('date_months') ?></span>
                                        <select name="months" class="form-control months">
                                            <?php
                                            for ($i = 0; $i < 7; $i++)
                                                echo '<option>' . $i . '</option>';
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Start Date -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('vacation_date_end') ?><span class="required">*</span></label>
                                    <div class="col-sm-5 input-group">
                                        <input type="text" name="date_start" value="" class="form-control hijri_datepicker2" data-format="yyy-mm-dd" disabled="">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="entypo-calendar"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- Affect Stock Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('vacation_affect_stock') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="radio-stacked" type="radio" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Toggle this custom radio</span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" name="radio-stacked" type="radio" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Or toggle this other custom radio</span>
                                        </label>
                                    </div>
                                </div>

                                <!-- Job Title Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('employee_name') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="job_titles_name_ar"  class="form-control" id="field-1" value="<?php if (!empty($vacation_info)) echo $vacation_info->job_titles_name_ar; ?>" required=""/>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Grant a Vacation Ends -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var calendar;
    $(function () {
        calendar = $.calendars.instance('islamic');
        $('.hijri_datepicker').calendarsPicker({calendar: calendar});
        var calendar2 = $.calendars.instance('islamic');
        $('.hijri_datepicker2').calendarsPicker({calendar: calendar2});
    });
    function Datepickercalculate() {
        // get value from first datepicker
        var date_array = $('.hijri_datepicker').val().split("/");

        // put the value in a date instance to calculate it
        var date = calendar.newDate(parseInt(date_array[0]), parseInt(date_array[1]), parseInt(date_array[2]));
        console.log(date);

        // calculate the value
        date.add($('.days').val(), 'd').add($('.weekss').val(), 'w').add($('.months').val(), 'm');
        console.log($('.days').val());
        console.log($('.weekss').val());
        console.log($('.months').val());
        console.log(date);

        // take care of the leading zero
        var m = (date.month().toString().length == 1) ? "0" + date.month() : date.month();
        var d = (date.day().toString().length == 1) ? "0" + date.day() : date.day();

        // put the result in the second datepicker
        $('.hijri_datepicker2').val(date.year() + "/" + m + "/" + d);
    }
</script>
