<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .box-body {
        padding-bottom: 20px;
    }

    .box-body-img {
        margin: 30px auto;
        width: 100%;
        border-radius:11px;
        display: block;
    }
</style>
<div class="row">
    <div class="col-sm-8 ">
        <div class="box box-primary" data-collapsed="0" style="border: none">
            <div class="col-sm-3"><img class="box-body-img" src="<?= base_url() ?>img/yamamah.jpg"></div>
            <div class="box-body col-sm-9">
                <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings2/save_sms_config"
                      method="post" class="form-horizontal form-groups-bordered small" style="padding-top: 15px;">

                    <div class="form-group ">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10"><strong><?= lang('sms_config_note1') ?> <a href="http://www.yamamah.com/"
                                                                                          target="_blank">www.yamamah.com</a></strong>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sender_name') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="text" name="sender_name1" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sender_name1 ?>" required/>
                        </div>
                    </div>
                    <div class="form-group" style="padding-top: 0px">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <small><?= lang('sms_config_note2') ?></small>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sms_login') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="text" name="sms_login1" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sms_login1 ?>" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sms_password') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="password" name="sms_password1" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sms_password1 ?>" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="sbtn" class="btn btn-block btn-primary"
                                    id="i_submit"><?= lang('save') ?></button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="box box-primary" data-collapsed="0" style="border: none">
            <div class="col-sm-3"><img class="box-body-img" src="<?= base_url() ?>img/lana.jpg"></div>
            <div class="box-body col-sm-9">
                <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings2/save_sms_config"
                      method="post" class="form-horizontal form-groups-bordered small" style="padding-top: 15px;">

                    <div class="form-group ">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10"><strong><?= lang('sms_config_note1') ?> <a href="http://www.lanasms.net"
                                                                                          target="_blank">www.lanasms.net</a></strong>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sender_name') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="text" name="sender_name2" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sender_name2 ?>" required/>
                        </div>
                    </div>
                    <div class="form-group" style="padding-top:0px">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <small><?= lang('sms_config_note2') ?>... إسم المرسل يجب أن يكون مسجل في الموقع أولا!</small>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sms_login') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="text" name="sms_login2" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sms_login2 ?>" required/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-2 control-label "><?= lang('sms_password') ?><span
                                    class="required">*</span></label>

                        <div class="col-sm-10">
                            <input type="password" name="sms_password2" class="form-control" id="field-1"
                                   value="<?= @$sms_config->sms_password2 ?>" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="sbtn" class="btn btn-block btn-primary"
                                    id="i_submit"><?= lang('save') ?></button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="box box-primary" data-collapsed="0" style="border: none">
            <div class="box-body">
                <div class="form-group" style="padding: 0px !important;">
                    <div class="col-sm-12">
                        <h4 style="background: #fff !important; padding-top: 0px !important; color: #000 !important; text-align: center; margin: 0px !important;">
                            <?= lang('sms_default') ?>
                        </h4>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-sm-offset-2 col-sm-8">
                        <a href="<?php echo base_url(); ?>admin/settings2/default_sms_config/1" class="btn btn-<?=(@$sms_config->default==1)?'primary':'default';?> btn-block"><b>YAMAMAH</b></a>
                    </div>
                </div>
                <br><br><br>
                <div class="form-group ">
                    <div class="col-sm-offset-2 col-sm-8">
                        <a href="<?php echo base_url(); ?>admin/settings2/default_sms_config/2" class="btn btn-<?=(@$sms_config->default==2)?'primary':'default';?> btn-block"><b>LANA</b></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="box box-primary" data-collapsed="0" style="border: none">
            <div class="box-body">
                <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings2/sms_test" target="_blank"
                      method="post" class="form-horizontal form-groups-bordered small" style="padding-top: 15px;">

                    <div class="form-group" style="padding: 0px !important;">
                        <div class="col-sm-12">
                            <h4 style="background: #fff !important; padding-top: 0px !important; color: #000 !important; text-align: center; margin: 0px !important;">
                                <?= lang('sms_test') ?>
                            </h4>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label "><?= lang('phone') ?><span class="required">*</span></label>

                        <div class="col-sm-9">
                            <input type="text" name="phone" class="form-control" id="field-1" required/>
                        </div>
                    </div>

                    <div class="form-group" style="padding-top: 0px !important;; padding-bottom: 20px;">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" id="sbtn" class="btn btn-block btn-success"
                                    id="i_submit"><?= lang('send') ?></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>