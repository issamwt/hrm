<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/jquery.orgchart.css" >
<style type="text/css">
    .orgchart{
        background-image: none !important;
        direction: ltr !important;
        display: block  !important;
        margin: 0px auto !important;
        transform: translateX(-50%);
        cursor: move;
    }
    .orgchart table{
        cursor: move;
    }
    .node .content {
        height: inherit !important;
        min-height: inherit !important;
        padding: 2px;
    }
    .node i.fa.fa-info-circle.chartform-icon {
        position: absolute;
        top: 0px;
        right: 0px;
        color: #2C3B41;
        box-shadow: 0px 0px 2px 1px #000;
        border-radius: 50%;
        padding: 1px;
        background: #fff;
        display: none;
        cursor: pointer;
    }
    .node:hover i.fa.fa-info-circle.chartform-icon {
        display: block;
    }
    .chartform{
        display: none;
        background: #222D32;
        position: absolute;
        top: 12px;
        right: -173px;
        width: 180px;
        border-radius: 2px;
        color: #fff;
        z-index: 100000;
    }
    .chartform hr {
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .chartform input{
        font-size: 0.9em;
        border-radius: 3px;
        padding: 3px;
        height: 22px;
        margin: 5px 0px;
    }
    .chartform{
        direction: <?= ($lang == 'arabic') ? 'rtl' : 'ltr'; ?> !important;
    }

    .chartform *{
        font-size:12px !important;
    }
    .chartform .btn, .chartform input{
        width: 90%;
        display: block;
        margin: 6px auto;

    }
    .chartoverlay{
        position: absolute;
        width: 100%;
        background: rgba(0, 0, 0, 0.31);
        z-index: 10;
        display: none;
    }
    .chartoverlay .fa-spin{
        font-size: 6em;
        display: block;
        margin: 20% auto;
        color: #2C3B41;
    }
    .chartoverlay-result{
        position: absolute;
        width: 100%;
        background: rgba(0, 55, 32, 0.92);
        z-index: 100;

    }
    .alert{
        position: absolute;
        width: 50%;
        top: 50%;
        left: 30%;
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="nav-tabs-custom" style="min-height: 500px; position: relative">
            <div id="chart-container">
                <div class="chartoverlay">
                    <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
                </div>
                <!--
                <div class="chartoverlay-result">
                </div>
                -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.orgchart.js"></script>
<script type="text/javascript">
    var datasource = <?= json_encode($datasource) ?>;
    $('#chart-container').orgchart({
        'data': datasource,
        'parentNodeSymbol': 'fa-th-large',
        'toggleSiblingsResp': true,
        'nodeContent': 'title',
        'pan': true,
        'zoom': true,
        'depth': 7,
        'createNode': function ($node, data) {
            var id = data.id;
            var secondMenuIcon = $('<i>', {
                'class': 'fa fa-info-circle chartform-icon',
                click: function () {
                    $(this).siblings('.chartform').slideToggle();
                }
            });
            if (id.split(';')[0] == 'dep') {
                var secondMenu = '<div class="chartform form">' +
                        '<button class="btn btn-block btn-danger btn-xs"  onclick="return confirm(\'<?= lang('js_confirm_message') ?>\')?orgajax(this,\'delete\',\'department\',\'' + data.id + '\'):\'\';"><i class="fa fa-trash-o"></i> <?= lang("delete") ?></button>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-primary btn-xs" onclick="orgajax(this,\'update\',\'department\',\'' + data.id + '\')"><i class="fa fa-pencil-square-o"></i> <?= lang("edit") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_ar + '"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_en + '" style="direction:ltr;"/>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-success btn-xs" onclick="orgajax(this,\'add\',\'designation\',\'' + data.id + '\')"><i class="fa fa-plus-square-o"></i> <?= lang("add_designation") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '</div>';
            } else if (id.split(';')[0] == 'des') {
                var secondMenu = '<div class="chartform form">' +
                        '<button class="btn btn-block btn-danger btn-xs"  onclick="return confirm(\'<?= lang('js_confirm_message') ?>\')?orgajax(this,\'delete\',\'designation\',\'' + data.id + '\'):\'\';"><i class="fa fa-trash-o"></i> <?= lang("delete") ?></button>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-primary btn-xs" onclick="orgajax(this,\'update\',\'designation\',\'' + data.id + '\')"><i class="fa fa-pencil-square-o"></i> <?= lang("edit") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_ar + '"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_en + '" style="direction:ltr;"/>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-success btn-xs" onclick="orgajax(this,\'add\',\'unit\',\'' + data.id + '\')"><i class="fa fa-plus-square-o"></i> <?= lang("add_unit") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '</div>';
            } else if (id.split(';')[0] == 'unit') {
                var secondMenu = '<div class="chartform form">' +
                        '<button class="btn btn-block btn-danger btn-xs"  onclick="return confirm(\'<?= lang('js_confirm_message') ?>\')?orgajax(this,\'delete\',\'unit\',\'' + data.id + '\'):\'\';"><i class="fa fa-trash-o"></i> <?= lang("delete") ?></button>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-primary btn-xs" onclick="orgajax(this,\'update\',\'unit\',\'' + data.id + '\')"><i class="fa fa-pencil-square-o"></i> <?= lang("edit") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_ar + '"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_en + '" style="direction:ltr;"/>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-success btn-xs" onclick="orgajax(this,\'add\',\'division\',\'' + data.id + '\')"><i class="fa fa-plus-square-o"></i> <?= lang("add_division") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '</div>';
            } else if (id.split(';')[0] == 'div') {
                var secondMenu = '<div class="chartform form">' +
                        '<button class="btn btn-block btn-danger btn-xs"  onclick="return confirm(\'<?= lang('js_confirm_message') ?>\')?orgajax(this,\'delete\',\'division\',\'' + data.id + '\'):\'\';"><i class="fa fa-trash-o"></i> <?= lang("delete") ?></button>' +
                        '<hr>' +
                        '<button class="btn btn-block btn-primary btn-xs" onclick="orgajax(this,\'update\',\'division\',\'' + data.id + '\')"><i class="fa fa-pencil-square-o"></i> <?= lang("edit") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_ar + '"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control" value="' + data.name_en + '" style="direction:ltr;"/>' +
                        '</div>';
            } else if (id.split(';')[0] == 'hrm') {
                var secondMenu = '<div class="chartform form">' +
                        '<button class="btn btn-block btn-success btn-xs" onclick="orgajax(this,\'add\',\'department\',\'' + data.id + '\')"><i class="fa fa-plus-square-o"></i> <?= lang("add_department") ?></button>' +
                        '<span><?= lang('name_ar') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '<span><?= lang('name_en') ?> : </span>' +
                        '<input type="text" class="form-control"/>' +
                        '</div>';
            }
            $node.append(secondMenuIcon).append(secondMenu);
        }
    });
    $('.orgchart').on('click', function (event) {
        if (!$(event.target).is('.chartform-icon') && !$(event.target).is('.chartform *')) {
            $(this).find('.chartform').slideUp();
        }
    });
    function orgajax(source, action, type, theid) {
        //alert(action + ' - ' + type + ' - ' + id);
        var id = theid.split(';')[1];
        var input1 = $(source).next().next().val();
        var input2 = $(source).next().next().next().next().val();
        if (action != 'delete' && (input1 == '' || input2 == '')) {
            alert("<?= lang('alert') ?>");
            return;
        }
        var url = '<?= base_url() ?>admin/settings2/ajaxfuction/';
        ////////////////////////////
        if (type == 'department') {
            url += 'department/';
        } else if (type == 'designation') {
            url += 'designation/';
        } else if (type == 'unit') {
            url += 'unit/';
        } else if (type == 'division') {
            url += 'division/';
        } else {
            alert("<?= lang('alert') ?>");
            return;
        }
        ////////////////////////////
        if (action == 'delete') {
            url += 'delete/';
        } else if (action == 'update') {
            url += 'update/';
        } else if (action == 'add') {
            url += 'add/';
        } else {
            alert("<?= lang('alert') ?>");
            return;
        }
        ////////////////////////////
        url += id;

        $('.chartoverlay').css('display', 'block');
        $('.orgchart').find('.chartform').slideUp();
        $.post(url, {ar: input1, en: input2}, function (data) {
            console.log(data);
            if (action == 'delete') {
                $('#chart-container').orgchart('removeNodes', $(source));
            } else if (action == 'update') {
                $('#chart-container').append('<div class="alert alert-success"><?= lang("saved_successfully") ?></div>');
                setTimeout(function () {
                    $('.alert.alert-success').fadeOut("slow", function () {
                        $('.alert.alert-success').remove();
                    });
                }, 1000);
setTimeout(function () {
                    location.reload();
                }, 2000);
            } else if (action == 'add') {
                $('#chart-container').append('<div class="alert alert-success"><?= lang("saved_successfully") ?></div>');
                setTimeout(function () {
                    $('.alert.alert-success').fadeOut("slow", function () {
                        $('.alert.alert-success').remove();
                    });
                }, 1000);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        }).fail(function (data) {
            console.log(data);
            alert("<?= lang('alert_error') ?>");
        })
                .always(function () {
                    $('.chartoverlay').css('display', 'none');
                });
    }

    $(function () {
        $('.chartoverlay').height($('.chart-container').height());
        $('.chartoverlay-result').height($('.chartoverlay').height());
    });
</script>
