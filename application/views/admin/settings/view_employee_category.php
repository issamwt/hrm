<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#category_list" data-toggle="tab"><?= lang('all_categary_emp') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_category"  data-toggle="tab"><?= lang('add_emp') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="category_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th class="col-sm-1 text-center"><?= lang('sl') ?></th>
                                        <th class="text-center"><?= lang('name_ar') ?></th>
                                        <th class="text-center"><?= lang('name_en') ?></th>
                                        <th width="19%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $key = 1 ?>
                                    <?php if (!empty($all_employee_category)): ?>
                                        <?php foreach ($all_employee_category as $v_category) : ?>
                                            <tr>
                                                <td class="text-center"><?php echo $key ?></td>
                                                <td><?= $v_category->name_ar ?></td>
                                                <td><?= $v_category->name_en ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/settings2/employee_category/<?= $v_category->id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" ><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/settings2/delete_employee_category/<?= $v_category->id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>

                                            </tr>
                                            <?php $key++; ?>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                    <td colspan="4">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Leave Category List tab Ends -->



                <!-- Add Category tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_category" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <form id="form" action="<?php echo base_url() ?>admin/settings2/save_employee_category/<?= @$employee_category_info->id; ?>" method="post" class="form-horizontal form-groups-bordered">

                                <!-- Leave Category ID For Update -->
                                <input type="hidden" name="id" value="<?= @$employee_category_info->id ?>"/>

                                <!-- Name In Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_ar" value="<?= @$employee_category_info->name_ar; ?>" class="form-control" placeholder=""  required=""/>
                                    </div>
                                </div>

                                <!-- Name In English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="name_en" value="<?= @$employee_category_info->name_en; ?>" class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Submit -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary" id="i_submit" ><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Category tab Ends -->
            </div>
        </div>
    </div>
</div>
