<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#category_list"
                                                                   data-toggle="tab"><?= lang('all_categary') ?></a>
                </li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_category"
                                                                   data-toggle="tab"><?= lang('new_category') ?></a>
                </li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Category List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="category_list"
                     style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th class="col-sm-1 text-center"><?= lang('sl') ?></th>
                                    <th class="text-center"><?= lang('category_name') ?></th>
                                    <th width="19%" class="text-center"><?= lang('leave_category_duration') ?></th>
                                    <th width="19%" class="text-center"><?= lang('leave_category_quota') ?></th>
                                    <th width="19%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $key = 1 ?>
                                <?php if (!empty($all_leave_category)): ?>
                                    <?php foreach ($all_leave_category as $v_category) : ?>
                                        <tr>
                                            <td class="text-center"><?php echo $key ?></td>
                                            <td><?php echo ($lang == 'arabic') ? $v_category->category_ar : $v_category->category_en; ?></td>
                                            <?php $val = ($lang == 'arabic') ? ($v_category->leave_duration > 2 && $v_category->leave_duration < 11) ? lang("days") : lang("day") : lang("days"); ?>
                                            <td class="text-center"><?php echo $v_category->leave_duration . ' ' . $val; ?> </td>
                                            <td class="text-center"><?php echo $v_category->leave_quota ?> %</td>
                                            <td>
                                                <a data-original-title="<?= lang('view_details') ?>" href="#"
                                                   class="btn btn-success btn-xs" data-placement="top"
                                                   data-toggle="modal"
                                                   data-target="#myModal<?= $v_category->leave_category_id ?>"><i
                                                        class="fa fa-eye"></i> <?= lang('view_details') ?></a>
                                                <a data-original-title="<?= lang('edit') ?>"
                                                   href="<?php echo base_url() ?>admin/settings/leave_category/<?= $v_category->leave_category_id ?>"
                                                   class="btn btn-primary btn-xs" title="" data-toggle="tooltip"
                                                   data-placement="top"><i
                                                        class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                <?php if ($v_category->leave_category_id != 1): ?>
                                                    <a data-original-title="<?= lang('delete') ?>"
                                                       href="<?php echo base_url() ?>admin/settings/delete_leave_category/<?= $v_category->leave_category_id ?>"
                                                       class="btn btn-danger btn-xs" title="" data-toggle="tooltip"
                                                       data-placement="top"
                                                       onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                    <?php else:?>
                                                    <a class="btn btn-danger btn-xs" disabled=""><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                <?php endif; ?>
                                            </td>

                                        </tr>
                                        <?php $key++; ?>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <td colspan="4">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Leave Category List tab Ends -->


                <!-- Modal For Leave Category Starts -->
                <?php if (!empty($all_leave_category)): ?>
                    <?php foreach ($all_leave_category as $v_category) : ?>
                        <div class="modal fade" id="myModal<?= $v_category->leave_category_id ?>" tabindex="-1"
                             role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style="font-size: 1.2em;">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title text-center"
                                            id="myModalLabel"><?php echo ($lang == 'arabic') ? $v_category->category_ar : $v_category->category_en; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><b><?= lang('name_ar') ?> : </b><br><?= $v_category->category_ar ?></p>
                                        <p><b><?= lang('name_en') ?> : </b><br><?= $v_category->category_en ?></p>
                                        <p>
                                            <b><?= lang('leave_category_duration') ?> : </b><br>
                                            <?php $val = ($lang == 'arabic') ? ($v_category->leave_duration > 2 && $v_category->leave_duration < 11) ? lang("days") : lang("day") : lang("days"); ?>
                                            <?= $v_category->leave_duration . ' ' . $val; ?>
                                        </p>
                                        <p><b><?= lang('leave_category_quota') ?>
                                                : </b><br><?php echo $v_category->leave_quota ?> %</p>
                                        <p><b><?= lang('paid_leave') ?>
                                                : </b><br><?= ($v_category->paid_leave == 1) ? lang('yes') : lang('no') ?>
                                        </p>
                                        <p><b><?= lang('leave_affect_stock') ?>
                                                : </b><br><?= ($v_category->affect_balance == 1) ? lang('yes') : lang('no') ?>
                                        </p>
                                        <p><b><?= lang('leave_calculation_type') ?>
                                                : </b><br><?= ($v_category->calc_type == 1) ? lang('contractual_year') : lang('normal_year') ?>
                                        </p>
                                        <p><b><?= lang('set_replacement') ?>
                                                : </b><br><?= ($v_category->set_replacement == 1) ? lang('yes') : lang('no') ?>
                                        </p>
                                        <p><b><?= lang('leave_tel') ?>
                                                : </b><br><?= ($v_category->leave_tel == 1) ? lang('yes') : lang('no') ?>
                                        </p>
                                        <p><b><?= lang('allowance_cat_ids') ?> : </b><br>
                                        <p>
                                        <ul>
                                            <?php $allowance_cat_ids = explode(';', @$v_category->allowance_cat_ids); ?>
                                            <?php foreach (@$all_allowance_categories as $ac): ?>
                                                <?php if (in_array($ac->allowance_id, $allowance_cat_ids)): ?>
                                                    <li><?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?></li>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </ul>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-secondary"
                                                data-dismiss="modal"><?= lang('close') ?></button>
                                        <a href="<?php echo base_url() ?>admin/settings/leave_category/<?= $v_category->leave_category_id ?>"
                                           class="btn btn-primary"><?= lang('edit') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!-- Modal For Leave Category Ends -->


                <!-- Add Category tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_category" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <form id="form" action="<?php echo base_url() ?>admin/settings/save_leave_category/<?php
                            if (!empty($leave_category->leave_category_id)) {
                                echo $leave_category->leave_category_id;
                            }
                            ?>" method="post" class="form-horizontal form-groups-bordered">

                                <!-- Leave Category ID For Update -->
                                <input type="hidden" name="leave_category_id"
                                       value="<?= @$leave_category->leave_category_id ?>"/>

                                <!-- Name In Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="category_ar" value="<?php
                                        if (!empty($leave_category->category_ar)) {
                                            echo $leave_category->category_ar;
                                        }
                                        ?>" class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Name In English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="category_en" value="<?php
                                        if (!empty($leave_category->category_en)) {
                                            echo $leave_category->category_en;
                                        }
                                        ?>" class="form-control" placeholder="" required=""/>
                                    </div>
                                </div>

                                <!-- Duration -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('leave_category_duration') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" min="1" max="240" name="leave_duration" value="<?php
                                        if (!empty($leave_category->leave_duration)) {
                                            echo $leave_category->leave_duration;
                                        } else {
                                            echo '1';
                                        }
                                        ?>" class="form-control" placeholder="1" required=""/>
                                    </div>
                                </div>

                                <!-- Quota -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('leave_category_quota') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" min="0" max="100" name="leave_quota" value="<?php
                                        if (!empty($leave_category->leave_quota)) {
                                            echo $leave_category->leave_quota;
                                        } else {
                                            echo '0';
                                        }
                                        ?>" class="form-control" placeholder="0" required=""/>
                                    </div>
                                </div>

                                <!-- Paid Leave  -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('paid_leave') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="paid_leave" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->paid_leave) && @$leave_category->paid_leave == 1) echo 'checked="checked"'; ?>
                                                   required="" value="1">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('yes') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" name="paid_leave" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->paid_leave) && @$leave_category->paid_leave == 2) echo 'checked="checked"'; ?>
                                                   required="" value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('no') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <!-- Affect Stock  -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('leave_affect_stock') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="affect_balance" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->affect_balance) && @$leave_category->affect_balance == 1) echo 'checked="checked"'; ?>
                                                   required="" value="1">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('yes') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" name="affect_balance" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->affect_balance) && @$leave_category->affect_balance == 2) echo 'checked="checked"'; ?>
                                                   required="" value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('no') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <!-- Calculation Type  -->
                                <div class="form-group">
                                    <label for="field-1"
                                           class="col-sm-3 control-label"><?= lang('leave_calculation_type') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="calc_type" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->calc_type) && @$leave_category->calc_type == 1) echo 'checked="checked"'; ?>
                                                   required="" value="1">
                                            <span class="custom-control-indicator"></span>
                                            <span
                                                class="custom-control-description"><?= lang('contractual_year') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" disabled="" name="calc_type" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->calc_type) && @$leave_category->calc_type == 2) echo 'checked="checked"'; ?>
                                                   required="" value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('normal_year') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <!-- Set a replacement -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('set_replacement') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="set_replacement" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->set_replacement) && @$leave_category->set_replacement == 1) echo 'checked="checked"'; ?>
                                                   required="" value="1">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('yes') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" name="set_replacement" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->set_replacement) && @$leave_category->set_replacement == 2) echo 'checked="checked"'; ?>
                                                   value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('no') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <!-- telephone number -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('leave_tel') ?> <span
                                            class="required">*</span></label>
                                    <div class="col-sm-5 alwaysltr">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="leave_tel" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->leave_tel) && @$leave_category->leave_tel == 1) echo 'checked="checked"'; ?>
                                                   required="" value="1">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('yes') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked2" name="leave_tel" type="radio"
                                                   class="custom-control-input" <?php if (!empty($leave_category->leave_tel) && @$leave_category->leave_tel == 2) echo 'checked="checked"'; ?>
                                                   required="" value="2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('no') ?></span>
                                        </label>
                                    </div>
                                </div>

                                <!-- allowance_cat_ids -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('allowance_cat_ids') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <?php $allowance_cat_ids = explode(';', @$leave_category->allowance_cat_ids); ?>
                                        <select name="allowance_cat_ids[]" class="form-control" multiple>
                                            <?php foreach (@$all_allowance_categories as $ac): ?>
                                                <option value="<?= $ac->allowance_id ?>"
                                                    <?= (in_array($ac->allowance_id, $allowance_cat_ids)) ? 'selected' : ''; ?>>
                                                    <?= ($lang == 'english') ? $ac->allowance_title_en : $ac->allowance_title_ar; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary"
                                                id="i_submit"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Category tab Ends -->
            </div>
        </div>
    </div>
</div>
