<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    #form2 {
        margin-top: 20px;
        border: 2px solid #000;
        padding: 10px 10px 0px 10px;
        display: none;
    }
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#job_titles_list"
                                                                   data-toggle="tab"><?= lang('job_titles_list') ?></a>
                </li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_job_title"
                                                                   data-toggle="tab"><?= lang('add_job_title') ?></a>
                </li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Job Titles List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="job_titles_list"
                     style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                    <th width="20%" class="text-center"><?= lang('job_title') ?></th>
                                    <th class="text-center"><?= lang('description') ?></th>
                                    <th width="13%" class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($job_titles_details)):$sl = 1; ?>
                                    <?php foreach ($job_titles_details as $jt): ?>
                                        <tr>
                                            <td class="text-center"><?= $sl ?></td>
                                            <td><?= ($lang == 'arabic') ? $jt->job_titles_name_ar : $jt->job_titles_name_en; ?></td>
                                            <td><?= ($lang == 'arabic') ? $jt->job_titles_desc_ar : $jt->job_titles_desc_en; ?></td>
                                            <td>
                                                <a data-original-title="<?= lang('edit') ?>"
                                                   href="<?php echo base_url() ?>admin/settings/job_titles/<?php echo $jt->job_titles_id; ?>"
                                                   class="btn btn-primary btn-xs" title="" data-toggle="tooltip"
                                                   data-placement="top"><i
                                                            class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                <?php $vip = ['1', '2', '3', '4', '5']; ?>
                                                <?php if (!in_array($jt->job_titles_id, $vip)): ?>
                                                    <a data-original-title="<?= lang('delete') ?>"
                                                       href="<?php echo base_url() ?>admin/settings/delete_job_title/<?php echo $jt->job_titles_id; ?>"
                                                       class="btn btn-danger btn-xs" title="" data-toggle="tooltip"
                                                       data-placement="top"
                                                       onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php $sl++; ?>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong><?= lang('nothing_to_display') ?></strong></td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Job Titles List Ends -->

                <!-- Add Job Titles Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_job_title"
                     style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="col-sm-8">
                                    <form role="form" id="form"
                                          action="<?php echo base_url(); ?>admin/settings/save_job_title/<?php if (!empty($job_title_info)) echo $job_title_info->job_titles_id; ?> "
                                          method="post" class="form-horizontal">

                                        <!-- Job Title Id for Update -->
                                        <input type="hidden" name="job_titles_id" class="form-control" id="field-1"
                                               value="<?php if (!empty($job_title_info)) echo @$job_title_info->job_titles_id; ?>"/>

                                        <!-- Job Title Arabic Name -->
                                        <div class="form-group">
                                            <label for="field-1"
                                                   class="col-sm-4 control-label"><?= lang('job_title_ar') ?><span
                                                        class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="job_titles_name_ar" class="form-control"
                                                       id="field-1"
                                                       value="<?php if (!empty($job_title_info)) echo $job_title_info->job_titles_name_ar; ?>"
                                                       required=""/>
                                            </div>
                                        </div>


                                        <!-- Job Title English Name -->
                                        <div class="form-group">
                                            <label for="field-2"
                                                   class="col-sm-4 control-label"><?= lang('job_title_en') ?><span
                                                        class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <input type="text" name="job_titles_name_en" class="form-control"
                                                       id="field-2"
                                                       value="<?php if (!empty($job_title_info)) echo $job_title_info->job_titles_name_en; ?>"
                                                       required=""/>
                                            </div>
                                        </div>

                                        <!-- Job Title Arabic Description -->
                                        <div class="form-group">
                                            <label for="field-3"
                                                   class="col-sm-4 control-label"><?= lang('description_ar') ?></label>
                                            <div class="col-sm-8">
                                                <textarea name="job_titles_desc_ar" class="form-control" id="field-3"
                                                          rows="8"><?php if (!empty($job_title_info)) echo $job_title_info->job_titles_desc_ar; ?></textarea>
                                            </div>
                                        </div>

                                        <!-- Job Title English Description -->
                                        <div class="form-group">
                                            <label for="field-4"
                                                   class="col-sm-4 control-label"><?= lang('description_en') ?></label>
                                            <div class="col-sm-8">
                                                <textarea name="job_titles_desc_en" class="form-control" id="field-4"
                                                          rows="8"><?php if (!empty($job_title_info)) echo $job_title_info->job_titles_desc_en; ?></textarea>
                                            </div>
                                        </div>

                                        <!-- Submit Button -->
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-4">
                                                <button type="submit"
                                                        class="btn btn-primary"><?= lang('save') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-4">
                                    <?php if (!empty($job_title_info)): ?>
                                        <h5 style="font-size: 1.2em"><?= lang('missions_list') ?></h5>
                                        <?php if ($evaluation_items): ?>
                                            <table class="table table-bordered">
                                                <?php foreach ($evaluation_items as $ev): ?>
                                                    <tr>
                                                        <td>
                                                            <?= ($lang == 'english') ? $ev->name_en : $ev->name_ar; ?>
                                                        </td>
                                                        <td width="100px" class="text-center">
                                                            <a class="btn btn-success btn-xs" target="_blank" href="<?=base_url()?>admin/settings2/evaluation_items/<?=$ev->evaluation_items_id?>">
                                                                <?= lang('edit_ev') ?>
                                                            </a>
                                                            <a class="btn btn-danger btn-xs" style="margin-top: 5px" href="<?=base_url()?>admin/settings/delete_mission/<?=$job_title_info->job_titles_id?>/<?=$ev->evaluation_items_id?>"
                                                                onclick="return confirm('<?= lang('js_confirm_message') ?>');">
                                                                <?= lang('delete') ?>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        <?php else: ?>
                                            <?= lang('no-exist') ?>
                                        <?php endif; ?>
                                        <button class="btn btn-primary btn-xs btn-block" onclick="showform()">
                                            <?= lang('add_mission') ?>
                                        </button>
                                        <div class="add-mission">
                                            <form role="form" id="form2"
                                                  action="<?php echo base_url(); ?>admin/settings/save_mission/<?php if (!empty($job_title_info)) echo $job_title_info->job_titles_id; ?> "
                                                  method="post" class="form-horizontal">
                                                <div class="form-group">
                                                    <label for="field-3"
                                                           class="col-sm-4 control-label"><?= lang('name_ar') ?></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="name_ar" class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="field-3"
                                                           class="col-sm-4 control-label"><?= lang('name_en') ?></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="name_en" class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-4">
                                                        <button type="submit"
                                                                class="btn btn-primary btn-xs"><?= lang('save') ?></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Add Job Titles Ends -->
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function showform() {
        $("#form2").slideToggle();
    }
</script>