<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    label{
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-sm-8 ">
        <div class="box box-primary" data-collapsed="0" style="border: none">
            <div class="box-body">
                <form role="form" id="general_settings" enctype="multipart/form-data"
                      action="<?php echo base_url(); ?>admin/settings/save_automatic_reminder/<?php if (!empty($rinfo)) echo $rinfo->reminder_id; ?>"
                      method="post" class="form-horizontal form-groups-bordered small" style="padding-top: 15px;">

                    <!-- reminder_language -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_language') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_lang" required>
                                <option><?= lang('reminder_language_select') ?></option>
                                <?php foreach ($all_langs as $lg):?>
                                    <option value="<?=$lg->code?>" <?=(!empty($rinfo) and $lg->code==$rinfo->reminder_lang)?'selected':'';?>>
                                        <?=($lang=='arabic')?$lg->name_ar:$lg->name;?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <!-- reminder_language -->

                    <!-- reminder_test_period_hrm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_hrm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_test_period_hrm">
                                <option value="0" <?=(@$rinfo->reminder_test_period_hrm==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?>" <?=(@$rinfo->reminder_test_period_hrm==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_test_period') ?></div>
                    </div>
                    <!-- reminder_test_period_hrm -->

                    <!-- reminder_test_period_dm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_dm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_test_period_dm">
                                <option value="0" <?=(@$rinfo->reminder_test_period_dm==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?> "<?=(@$rinfo->reminder_test_period_dm==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_test_period') ?></div>
                    </div>
                    <!-- reminder_test_period_dm -->

                    <!-- reminder_identity_end_hrm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_hrm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_identity_end">
                                <option value="0" <?=(@$rinfo->reminder_identity_end==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?>" <?=(@$rinfo->reminder_identity_end==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_identity_end') ?></div>
                    </div>
                    <!-- reminder_identity_end_hrm -->

                    <!-- reminder_passport_end_hrm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_hrm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_passport_end">
                                <option value="0" <?=(@$rinfo->reminder_passport_end==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?>" <?=(@$rinfo->reminder_passport_end==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_passport_end') ?></div>
                    </div>
                    <!-- reminder_passport_end_hrm -->

                    <!-- reminder_med_insurance_end_hrm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_hrm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_med_insurance_end">
                                <option value="0" <?=(@$rinfo->reminder_med_insurance_end==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?>" <?=(@$rinfo->reminder_med_insurance_end==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_med_insurance_end') ?></div>
                    </div>
                    <!-- reminder_med_insurance_end_hrm -->

                    <!-- reminder_contract_end_hrm -->
                    <div class="form-group ">
                        <label class="col-sm-4 control-label"><?= lang('reminder_hrm_employee') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_contract_end">
                                <option value="0" <?=(@$rinfo->reminder_contract_end==0)?'selected':'';?>><?= lang('reminder_number_select') ?></option>
                                <?php for($i=1; $i<=30; $i++): ?>
                                    <option value="<?=$i?>" <?=(@$rinfo->reminder_contract_end==$i)?'selected':'';?>><?=$i?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-sm-4"><?= lang('reminder_contract_end') ?></div>
                    </div>
                    <!-- reminder_contract_end_hrm -->

                    <!-- reminder_mail -->
                    <div class="form-group ">
                        <label class="col-sm-12 control-label" style="text-align: right;">
                            <input type="checkbox" name="reminder_mail" <?=(@$rinfo->reminder_mail==1)?'checked':'';?>> <?= lang('reminder_mail') ?>
                        </label>
                    </div>
                    <!-- reminder_mail -->

                    <!-- reminder_employee_fixed -->
                    <div class="form-group ">
                        <label class="col-sm-12 control-label" style="text-align: right;">
                            <input type="checkbox" name="reminder_employee_fixed" <?=(@$rinfo->reminder_employee_fixed==1)?'checked':'';?>> <?= lang('reminder_employee_fixed') ?>
                        </label>
                    </div>
                    <!-- reminder_employee_fixed -->

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                            <button type="submit" id="sbtn" class="btn btn-primary btn-block"
                                    id="i_submit"><?= lang('save') ?></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


                    <!--
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label "><?= lang('reminder_language') ?></label>
                        <div class="col-sm-7">
                            <select class="form-control" name="reminder_lang">
                                <option><?= lang('reminder_language_select') ?></option>
                                <?php foreach ($all_langs as $langs) { ?>
                                    <option value="<?= $langs->code ?>"<?php if (!empty($rinfo)) echo $langs->code == $rinfo->reminder_lang ? 'selected' : '' ?>><?= ($lang == 'arabic') ? $langs->name_ar : $langs->name; ?></option>
                                <?php } ?>
                            </select class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?= lang('reminder_med_insur') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_med_insur">
                                <option><?= lang('reminder_number_select') ?></option>
                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                    <option value="<?= $i; ?>"<?php if (!empty($rinfo)) echo $i == $rinfo->reminder_med_insur ? 'selected' : '' ?>><?= $i; ?></option>
                                <?php } ?>
                            </select class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <?= lang('reminder_med_insur_next') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?= lang('reminder_insur_card') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_insur_card">
                                <option><?= lang('reminder_number_select') ?></option>
                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                    <option value="<?= $i; ?>"<?php if (!empty($rinfo)) echo $i == $rinfo->reminder_insur_card ? 'selected' : '' ?>><?= $i; ?></option>
                                <?php } ?>
                            </select class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <?= lang('reminder_insur_card_next') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?= lang('reminder_retrait') ?></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="reminder_retrait">
                                <option><?= lang('reminder_number_select') ?></option>
                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                    <option value="<?= $i; ?>"<?php if (!empty($rinfo)) echo $i == $rinfo->reminder_retrait ? 'selected' : '' ?>><?= $i; ?></option>
                                <?php } ?>
                            </select class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <?= lang('reminder_retrait_next') ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                            <button type="submit" id="sbtn" class="btn btn-primary btn-block"
                                    id="i_submit"><?= lang('save') ?></button>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
-->