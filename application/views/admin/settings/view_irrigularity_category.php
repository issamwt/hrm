<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
<?= ($lang == 'english') ? '' : '.thetable{font-size: 1.1em;}'; ?>
    .thetable thead{background: #ffe000}
    .thetable tbody tr td:first-of-type{background: #ffe000}
    .thetable tbody tr td:nth-child(2){background: #f5f5f5;}
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#category_list" data-toggle="tab"><?= lang('all_categary') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_category"  data-toggle="tab"><?= lang('new_category') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="category_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover thetable" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="2%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('name') ?></th>
                                        <th width="10%" class="text-center"><?= lang('first') ?></th>
                                        <th width="10%" class="text-center"><?= lang('second') ?></th>
                                        <th width="10%" class="text-center"><?= lang('third') ?></th>
                                        <th width="10%" class="text-center"><?= lang('fourth') ?></th>
                                        <th width="13%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($all_irrigularity_categories)): $sl = 1; ?>
                                        <?php foreach ($all_irrigularity_categories as $irgl): ?>
                                            <tr>
                                                <td class="text-center text-aqua"><?= $sl ?></td>
                                                <td><?= ($lang == 'arabic') ? $irgl->name_ar : $irgl->name_en ?></td>
                                                <td><?= ($lang == 'arabic') ? $irgl->first_ar : $irgl->first_en ?></td>
                                                <td><?= ($lang == 'arabic') ? $irgl->second_ar : $irgl->second_en ?></td>
                                                <td><?= ($lang == 'arabic') ? $irgl->third_ar : $irgl->third_en ?></td>
                                                <td><?= ($lang == 'arabic') ? $irgl->fourth_ar : $irgl->fourth_en ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/settings/irrigularity_category/<?= $irgl->irrigularity_category_id ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/settings/delete_irrigularity_category/<?= $irgl->irrigularity_category_id ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--List Ends -->

                <!--Add Starts -->
                <div class = "tab-pane <?= $active == 2 ? 'active' : '' ?>" id = "add_category" style = "position: relative;">
                    <div class = "box" style = "border: none; padding-top: 15px;" data-collapsed = "0">
                        <div class="panel-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings/save_irrigularity_category/<?= @$irrigularity_categories_info->irrigularity_category_id ?>" method="post" class="form-horizontal">

                                <!-- ID For Update -->
                                <input type="hidden" name="irrigularity_category_id" value="<?= @$irrigularity_categories_info->irrigularity_category_id ?>"/>

                                <!-- Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <textarea name="name_ar" rows="3" class="form-control" id="field-1" required=""><?= @$irrigularity_categories_info->name_ar ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"></label>
                                    <div class="col-sm-5 text-center">
                                        <h6><b><?= lang('penalty') ?></b></h6>
                                    </div>
                                </div>

                                <!-- First Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('first_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="first_ar" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->first_ar ?></textarea>
                                    </div>
                                </div>

                                <!-- Second Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('second_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="second_ar" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->second_ar ?></textarea>
                                    </div>
                                </div>

                                <!-- Third Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('third_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="third_ar" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->third_ar ?></textarea>
                                    </div>
                                </div>

                                <!-- Fourth Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('fourth_ar') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="fourth_ar" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->fourth_ar ?></textarea>
                                    </div>
                                </div>

                                <br>
                                <hr>
                                <br>
                                <br>

                                <!-- English Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_en') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <textarea name="name_en" rows="3" class="form-control" id="field-1" required=""><?= @$irrigularity_categories_info->name_en ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"></label>
                                    <div class="col-sm-5 text-center">
                                        <h6><b><?= lang('penalty') ?></b></h6>
                                    </div>
                                </div>

                                <!-- First English Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('first_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="first_en" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->first_en ?></textarea>
                                    </div>
                                </div>

                                <!-- Second English Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('second_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="second_en" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->second_en ?></textarea>
                                    </div>
                                </div>

                                <!-- Third English Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('third_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="third_en" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->third_en ?></textarea>
                                    </div>
                                </div>

                                <!-- Fourth English Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('fourth_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="fourth_en" rows="2" class="form-control" id="field-1"><?= @$irrigularity_categories_info->fourth_en ?></textarea>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!--Add Ends -->


            </div>
        </div>
    </div>
</div>