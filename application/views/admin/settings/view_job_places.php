<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#job_places" data-toggle="tab"><?= lang('job_places') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_job_places"  data-toggle="tab"><?= lang('add_job_places') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Job Titles List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="job_places" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">

                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="20%" class="text-center"><?= lang('name_ar') ?></th>
                                        <th width="20%" class="text-center"><?= lang('name_en') ?></th>
                                        <th width="10%" width="13%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($all_job_places)):$sl = 1; ?>
                                        <?php foreach ($all_job_places as $jp): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= $jp->place_name_ar ?></td>
                                                <td><?= $jp->place_name_en ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/settings2/job_places/<?= $jp->job_place_id ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/settings2/delete_job_places/<?= $jp->job_place_id ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="4" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Job Titles List Ends -->

                <!-- Add Job Titles Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_job_places" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="panel-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings2/save_job_place/<?= @$job_place_info->job_place_id ?>" method="post" class="form-horizontal">

                                <!-- Job Title Id for Update -->
                                <?php if (!empty($job_title_info)): ?>
                                    <input type="hidden" name="job_place_id"  class="form-control" id="field-1" value="<?= @$job_place_info->job_place_id ?>"/>
                                <?php endif; ?>


                                <!-- Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="place_name_ar"  class="form-control" id="field-1" value="<?= @$job_place_info->place_name_ar ?>" required=""/>
                                    </div>
                                </div>


                                <!-- Job Title English Name -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label"><?= lang('name_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="place_name_en"  class="form-control" id="field-2" value="<?= @$job_place_info->place_name_en ?>" required=""/>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div></div>

                </div>
                <!-- Add Job Titles Ends -->
            </div>
        </div>
    </div>
</div>

