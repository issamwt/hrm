<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#designations_list" data-toggle="tab"><?= lang('designations_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_designation"  data-toggle="tab"><?= lang('add_designation') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="designations_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th  class="text-center"><?= lang('designation_name') ?></th>
                                        <th   class="text-center"><?= lang('departement') ?></th>
                                        <th  class="text-center"><?= lang('designation_emp') ?></th>
                                        <th  class="text-center"><?= lang('description') ?></th>
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($designations_details)): $sl = 1; ?>
                                        <?php foreach ($designations_details as $des): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?></td>
                                                <td><?= ($lang == 'arabic') ? $des->department_name_ar : $des->department_name; ?></td>
                                                <td><?= ($lang == 'arabic') ? $des->full_name_ar : $des->full_name_en; ?></td>
                                                <td><?= ($lang == 'arabic') ? $des->designations_desc_ar : $des->designations_desc_en; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/designation/designation_list/<?php echo $des->designations_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/designation/delete_designation/<?php echo $des->designations_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_designation" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/designation/save_designation/<?= @$designation_info->designations_id; ?> " method="post" class="form-horizontal">

                                <!-- Designation Id for Update -->
                                <?php if (!empty($designation_info->designations_id)): ?>
                                    <input type="hidden" name="designations_id"  class="form-control" id="field-1" value="<?= @$designation_info->designations_id; ?>"/>
                                <?php endif; ?>

                                <!-- Designation Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="designations_ar"  class="form-control" id="field-1" value="<?= @$designation_info->designations_ar; ?>" required=""/>
                                    </div>
                                </div>


                                <!-- Designation English Name -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label"><?= lang('name_en') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="designations"  class="form-control" id="field-2" value="<?= @$designation_info->designations; ?>" required=""/>
                                    </div>
                                </div>

                                <!-- The Departement -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('departement') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="department_id" class="form-control" id="field-1">
                                            <option value=""><?= lang('select_departement') ?>.....</option>
                                            <?php if (!empty($departement_info)): ?>
                                                <?php foreach ($departement_info as $dep): ?>
                                                    <option value="<?= $dep->department_id ?>" <?php if (@$designation_info->department_id == $dep->department_id) echo 'selected'; ?>>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>


                                <!-- Employee Name -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <select name="employee_id_non_required" class="form-control">
                                            <option value=""><?= lang('select_employee') ?>.....</option>
                                            <?php if (!empty($employee_info)): ?>
                                                <?php foreach ($employee_info as $emp): ?>
                                                    <option value="<?= $emp->employee_id ?>" <?php if (@$designation_info->employee_id == $emp->employee_id) echo 'selected'; ?>>
                                                        <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Description in Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="designations_desc_ar" class="form-control" rows="10"><?php if (!empty($designation_info->designations_desc_ar)) echo $designation_info->designations_desc_ar; ?></textarea>
                                    </div>
                                </div>

                                <!-- Description in english -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_en') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="designations_desc_en" class="form-control" rows="10"><?php if (!empty($designation_info->designations_desc_en)) echo $designation_info->designations_desc_en; ?></textarea>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>