<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" type="image/png" href="<?= base_url() ?>img/favicon.png">


        <link href="<?php echo base_url(); ?>asset/css/normalize.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>asset/js/css3-mediaqueries.js" type="text/javascript"></script>
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>asset/css/main.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>asset/css/admin.css" rel="stylesheet" type="text/css" />
        <!-- Date and Time Picker CSS -->
        <link href="<?php echo base_url(); ?>asset/css/datepicker.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>asset/css/timepicker.css" rel="stylesheet" type="text/css" />
        <!-- All Icon  CSS -->
        <link href="<?php echo base_url(); ?>asset/css/font-icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/font-icons/entypo/css/entypo.css" >
        <!-- Data Table  CSS -->
        <link href="<?php echo base_url(); ?>asset/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>asset/css/plugins/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!--Select 2 -->
        <link href="<?php echo base_url() ?>asset/css/select2.css" rel="stylesheet"/>
        <link href="<?php echo base_url() ?>asset/css/bootstrap-wysihtml5.css" rel="stylesheet"/>
        <!-- bootstrap slider -->
        <link rel="stylesheet" href="<?= base_url(); ?>asset/js/plugins/bootstrap-slider/slider.css">
        <!-- bootstrap slider -->
        <link rel="stylesheet" href="<?= base_url(); ?>asset/css/new.css">
        <?php if (@$hijri_calendar): ?>
            <!-- Hijri Calendar -->
            <link rel="stylesheet" href="<?= base_url(); ?>asset/css/plugins/Hijri/jquery.calendars.islamic.css">
        <?php endif; ?>




        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="asset/js/html5shiv.js" type="text/javascript"></script>
        <script src="asset/js/respond.min.js" type="text/javascript"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>asset/js/jquery-1.10.2.min.js"></script>
        <!-- ALl Custom Scripts -->
        <script src="<?php echo base_url(); ?>asset/js/custom.js"></script>
        <script>

            $(document).ready(function () {

                $(window).resize(function () {
                    ellipses1 = $("#bc1 :nth-child(2)")
                    if ($("#bc1 a:hidden").length > 0) {
                        ellipses1.show()
                    } else {
                        ellipses1.hide()
                    }
                    ellipses2 = $("#bc2 :nth-child(2)")
                    if ($("#bc2 a:hidden").length > 0) {
                        ellipses2.show()
                    } else {
                        ellipses2.hide()
                    }
                })
            });
        </script>
        <style type="text/css">
            input[type="file"]{width: 40px;}
            input[type="file"]::-webkit-file-upload-button {display: none;}
            input[type="file"]::before {content: 'Upload';}
        </style>
        <?php if ($this->session->userdata('lang') == 'arabic'): ?>
            <link href="<?php echo base_url() ?>asset/css/bootstrap.ar.min.css" rel="stylesheet"/>
            <style type="text/css">
                input[type="file"]{width: 50px;}
                input[type="file"]::-webkit-file-upload-button {display: none;}
                input[type="file"]::before {content: 'إختر ملف';}
                .input-group-addon {
                    border-left: 1px solid #ccc !important;
                    border-radius: 4px 0px 0px 4px !important;
                }
                .content-header>.breadcrumb{
                    float: initial !important;
                    right: initial !important;
                    left: 10px;
                }
                .sidebar-menu li>a>.pull-right{
                    right: initial !important;
                    left: 10px;
                }
                .sidebar-menu>li>a {
                    padding: 12px 15px 12px 15px;}
                .sidebar-menu .treeview-menu>li>a {
                    padding: 10px 30px 5px 5px;}

                @media (min-width:768px) {
                    .main-sidebar, .left-side {
                        left:initial !important;
                        right: 0;
                    }
                    .content-wrapper
                    {
                        margin-right: 230px;
                        margin-left: initial !important;
                    }
                    .main-header>.navbar{    margin-left: initial !important;
                                             margin-right: 230px !important;}
                    .main-header .logo{
                        float: right !important;
                    }
                    .sidebar-mini.sidebar-collapse .content-wrapper, .sidebar-mini.sidebar-collapse .right-side, .sidebar-mini.sidebar-collapse .main-footer{
                        margin-right: 50px !important;
                        margin-left: initial !important;
                    }
                    .sidebar-mini.sidebar-collapse .main-header .navbar {
                        margin-left: initial !important;
                        margin-right: 50px !important;
                    }
                }
                .main-footer{margin-left: initial !important;
                             margin-right: 230px !important;}
                .main-header .sidebar-toggle {
                    float: right;}
                .main-header .navbar-custom-menu, .main-header .navbar-right {
                    float: left;
                }
                .navbar-custom-menu>.navbar-nav>li>.dropdown-menu {
                    position: absolute;
                    right: 0;
                    left: 0px !important;
                    right: auto;
                }
                .control-sidebar.control-sidebar-open, .control-sidebar.control-sidebar-open+.control-sidebar-bg {
                    right: initial !important;
                    left: 0;
                }
                .control-sidebar-bg, .control-sidebar{
                    right: initial !important;
                    left: -268px;
                }
                .nav-tabs-custom>.nav-tabs>li{
                    margin-right: 0px !important;
                }
                .nav-tabs-custom>.nav-tabs>li.active>a{
                    border-left-color: #f4f4f4 !important;
                    border-right-color: #f4f4f4 !important;
                }
                #dataTables-example_filter {float: left;}
                #dataTables-example_length {float: right;}
                #dataTables-example_paginate {float: left;}
                #dataTables-example_info {float: right;}

                .k-treeview .k-in {
                    margin: 1px .16666em 1px 0 !important;
                }

                .k-treeview .k-item {
                    padding: 0 16px 0 0 !important;
                }

                .k-treeview .k-minus, .k-treeview .k-plus {
                    margin-left: 0px !important;
                    margin-right: -16px !important;
                }
                /*
                .content *{direction: rtl !important;}
                .content .alwaysltr {direction: ltr !important;}
                .content .alwaysltr *{direction: ltr !important;}
                .content .alwaysltr input[type='text']{direction: rtl !important;}
                .pull-right{float: left !important;}
                .content-header > h1 , .input-group-addon, .input-group-btn{direction: ltr !important;}


                .sidebar-menu > li > a{padding: 12px 20px 12px 9px !important;}
                .skin-red .sidebar-menu > li > a{border-left: initial; border-right: 4px solid transparent;}
                .skin-red .sidebar-menu > li:hover > a, .skin-red .sidebar-menu > li.active > a{border-right-color: #dd4b39;}
                .sidebar-menu .treeview-menu > li > a{padding: 10px 30px 5px 5px !important;}
                .sidebar-menu li > a > .pull-right{right: initial;left: 10px;transform: rotate(180deg);}
                .nav-tabs > li{float: right !important;}
                .nav-tabs-custom > .nav-tabs > li{margin-right: initial;margin-left: 5px;}
                .nav-tabs-custom > .nav-tabs > li.active > a{border-left-color: #f4f4f4 !important;}
                */
            </style>
        <?php endif; ?>
        <style type="text/css">
            .calendars-month-year {
                width: 47% !important;
                height: 24px !important;
                border: 1px solid #fff !important;
                border-radius: 0px !important;
            }
            .calendars-month-header {
                border-radius: 0px;
            }
        </style>
    </head>


