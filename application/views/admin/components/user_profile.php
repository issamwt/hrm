<header class="main-header">

    <!-- Logo -->
    <a href="<?= base_url() ?>" class="logo">
        <?php
        $genaral_info = $this->session->userdata('genaral_info');
        if (!empty($genaral_info)) {
            foreach ($genaral_info as $info) {
                ?>
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"> <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/minilogo.jpg" alt="" class="img-circle"/></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><?php echo $info->name ?></span>

                <?php
            }
        } else {
            ?>
            <span class="logo-mini">
                <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/minilogo.png" alt="Logo" class="img-circle"/>
            </span>
            <span class="logo-lg">HR - Lite</span>

        <?php }
        ?>

    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag"></i> <?= lang('languages') ?>
                    </a>
                    <ul class="dropdown-menu">

                        <?php
                        $languages = $this->db->order_by('name', 'ASC')->get('tbl_languages')->result();

                        foreach ($languages as $lang) : if ($lang->active == 1) :
                                ?>
                                <li>
                                    <a href="<?= base_url() ?>admin/dashboard/set_language/<?= $lang->name ?>" title="<?= ucwords(str_replace("_", " ", $lang->name)) ?>">
                                        <img src="<?= base_url() ?>asset/images/flags/<?= $lang->icon ?>.gif" alt="<?= ucwords(str_replace("_", " ", $lang->name)) ?>"  /> <?= ucwords(str_replace("_", " ", ($this->session->userdata('lang') == 'arabic') ? $lang->name_ar : $lang->name)) ?>
                                    </a>
                                </li>
                                <?php
                            endif;
                        endforeach;
                        ?>

                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="hidden-xs"><b><?= ($this->session->userdata('lang') == 'arabic') ? $this->session->userdata('user_name_ar') : $this->session->userdata('user_name_en'); ?> :

                                <?php
                                switch ($this->session->userdata('user_type')) {
                                    case 1:echo lang('super_admin');
                                        break;
                                    case 2:echo lang('guest_admin');
                                        break;
                                    default:echo '<br>';
                                        break;
                                }
                                ?>


                            </b></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url() ?>img/admin.png" class="img-circle" alt="User Image" />
                            <p>
                                <?= lang('username') ?> : <?php echo $this->session->userdata('user_name') ?>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url() ?>admin/settings/update_profile" class="btn btn-default btn-flat"><?= lang('profile') ?></a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url() ?>login/logout" class="btn btn-danger btn-flat"><?= lang('sign_out') ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>

    </nav>
</header>
<!-- Control Sidebar -->
<?php
$opened = $this->session->userdata('opened');
$this->session->unset_userdata('opened');
?>
<aside class="control-sidebar control-sidebar-dark <?php
if (!empty($opened)) {
    echo 'control-sidebar-open';
}
?>">
    <style>
        .active{
            background:none;
        }
    </style>

</aside><!-- /.control-sidebar -->
<div class="control-sidebar-bg"></div>