
    <div class="header-left">
        <div class="topnav">
            <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>
            <ul class="nav nav-icons">
                <li><a href="#" class="toggle-sidebar-top"><span class="icon-user-following"></span></a></li>
            </ul>
        </div>
    </div>
    <div class="header-right">
        <ul class="header-menu nav navbar-nav">
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="language-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-globe"></i>
                    <span> <?=lang('languages')?> </span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?=base_url()?>admin/dashboard/set_language/arabic" data-lang="ar"><img src="<?=base_url()?>/assets/images/flags/Saudi-Arabia.png" alt=""> <span> عربى </span></a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>admin/dashboard/set_language/english" data-lang="en"><img src="<?=base_url()?>/assets/images/flags/United-Kingdom.png" alt=""> <span>English</span></a>
                    </li>
                </ul>
            </li>


            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <img src="<?=base_url()?>assets/images/avatars/user-5.png" alt="user image">
                    <span class="username">
                        <?= ($this->session->userdata('lang') == 'arabic') ? $this->session->userdata('user_name_ar') : $this->session->userdata('user_name_en'); ?> :
                        <?php
                        switch ($this->session->userdata('user_type')) {
                            case 1:echo lang('super_admin');
                                break;
                            case 2:echo lang('guest_admin');
                                break;
                            default:echo '<br>';
                                break;
                        }
                        ?>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?php echo base_url() ?>admin/settings/update_profile"><i class="icon-user"></i><span> <?= lang('profile') ?> </span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>admin/settings/general_settings"><i class="icon-settings"></i><span><?=lang('general_settings')?></span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>login/logout"><i class="icon-logout"></i><span> <?= lang('sign_out') ?> </span></a>
                    </li>
                </ul>
            </li>
            <!-- END USER DROPDOWN -->

        </ul>
    </div>
    <!-- header-right -->
