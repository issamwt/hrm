<?php

$user_permission = $_SESSION["user_roll"];

foreach ($user_permission as $v_permission) {
    $user_roll[$v_permission->menu_id] = $v_permission->menu_id;
}

?>

<div class="sidebar">
    <div class="logopanel">
        <h1>
            <a href="#"></a>
        </h1>
    </div>
    <div class="sidebar-inner">
        <!--
        <div class="sidebar-top big-img">
            <div class="user-image">
                <img src="<?= base_url() ?>assets/images/avatars/user-5.png" class="img-responsive img-circle">
            </div>
            <h4><?= ($this->session->userdata('lang') == 'arabic') ? $this->session->userdata('user_name_ar') : $this->session->userdata('user_name_en'); ?></h4>
            <div class="dropdown user-login">
                <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">
                    <i class="online"></i><span> موجود </span> <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#"><i class="busy"></i><span> مشغول </span></a></li>
                    <li><a href="#"><i class="turquoise"></i><span> غير موجود </span></a></li>
                    <li><a href="#"><i class="away"></i><span> بعيد </span></a></li>
                </ul>
            </div>
        </div>
        <div class="menu-title">
            <span> <?= ($lang == 'arabic') ? 'القائمة' : 'Menu'; ?> </span>
        </div>
        -->
        <ul class="nav nav-sidebar" style="padding-top: 20px">
            <?php echo $this->menu->dynamicMenu(); ?>
        </ul>
        <div class="sidebar-widgets"></div>
        <div class="sidebar-footer clearfix" style="">
            <a class="pull-left footer-settings" href="<?= base_url() ?>admin/settings/general_settings"
               data-rel="tooltip" data-placement="top"
               data-original-title="<?= lang('general_settings') ?>">
                <i class="icon-settings"></i></a>
            <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top"
               data-original-title="full screen">
                <i class="icon-size-fullscreen"></i></a>
            <!--<a class="pull-left" href="#" data-rel="tooltip" data-placement="top" data-original-title="غلق الشاشة">
                <i class="icon-lock"></i></a>-->
            <a class="pull-left btn-effect" href="<?= base_url() ?>login/logout" data-modal="modal-1" data-rel="tooltip"
               data-placement="top"
               data-original-title="<?= lang('logout'); ?>">
                <i class="icon-power"></i></a>
        </div>
    </div>
</div>





