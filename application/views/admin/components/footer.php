<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/menu.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/custom-validation.js" type="text/javascript"></script>
<?php if ($this->session->userdata('lang') == 'arabic'): ?>
    <script src="<?php echo base_url(); ?>asset/js/jquery.validate.ar.js" type="text/javascript"></script>
<?php else: ?>
    <script src="<?php echo base_url(); ?>asset/js/jquery.validate.js" type="text/javascript"></script>
<?php endif; ?>
<!-- Jasny Bootstrap for NIce Image Change -->
<script src="<?php echo base_url() ?>asset/js/jasny-bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
<style>.datepicker-dropdown {max-width: 203px;}.datepicker {float: right}.datepicker.dropdown-menu {right:auto}.datepicker-dropdown:after,.datepicker-dropdown:before{display: none}</style>
<script src="<?php echo base_url() ?>asset/js/timepicker.js" ></script>

<!-- Data Table -->
<script src="<?php echo base_url(); ?>asset/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/dataTables.bootstrap.js" type="text/javascript"></script>
<!--select 2 -->
<script src="<?php echo base_url() ?>asset/js/select2.js"></script>
<script src="<?= base_url() ?>asset/js/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() ?>asset/js/bootstrap-wysihtml5.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url() ?>asset/js/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<?php if (@$hijri_calendar): ?>
    <!-- Hijri Calendar -->
    <script src="<?php echo base_url() ?>asset/js/plugins/Hijri/jquery.plugin.js" type="text/javascript"></script>
    <?php if ($lang == 'arabic'): ?>
        <script src="<?php echo base_url() ?>asset/js/plugins/Hijri/jquery.calendars.islamic.ar.js" type="text/javascript"></script>
    <?php else: ?>
        <script src="<?php echo base_url() ?>asset/js/plugins/Hijri/jquery.calendars.islamic.en.js" type="text/javascript"></script>
    <?php endif; ?>
<?php endif; ?>


<script src="<?php echo base_url() ?>asset/js/functions.js?v=<?= uniqid(true) ?>"></script>

<script>
    $(document).ready(function () {
        $("[id^=dataTables-example]").dataTable(
            <?php if (@$lang == 'arabic'): ?>
            {language: {
                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر _MENU_ مدخلات",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix": "",
                    "sSearch": "ابحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"
                    }
                }
                ,
                "pageLength": 25}
        <?php else: ?>
        {"pageLength": 25}
        <?php endif; ?>
    );
    });

</script>

</body>
</html>
