<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php $lang = $this->session->userdata('lang'); ?>
<script type="text/javascript" src="<?= base_url() ?>asset/js/jquery-ui.min.js"></script>
<style type="text/css">
    .buttons {
        text-align: center;
        padding: 0px;
        display: block;
        margin: 0 auto;
        margin-bottom: 20px;
        margin-top: -20px;
        /*background:#D2D6DE;*/
    }

    .buttons .buttons-container {
        border: 1px solid #ddd;
        width: auto;
        background-color: #f5f5f5;
        padding: 2px;
    }

    .panel, .panel-heading, .panel-title {
        border-radius: 0px !important;
    }

    .panel-default > .panel-heading {
        color: #333;
        background-color: #f5f5f5;
        border-color: #ddd;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
        border-top-color: #ddd;
    }

    h4.panel-title {
        text-align: center;
        background-color: transparent !important;
        padding: 0px !important;
        color: initial !important;
    }

    h4.panel-title a, h4.panel-title a:hover {
        width: 100%;
        display: block;
        text-decoration: none;
        font-size: 1em;
        font-weight: bold;
        color: initial !important;
    }

    .spinner_deps, .spinner_sects, .spinner_units, .spinner_divis {
        display: none;
    }

    .spinner_deps .fa, .spinner_sects .fa, .spinner_units .fa, .spinner_divis .fa {
        font-size: 15px;
    }

    input[type='checkbox'] {
        width: initial !important;
        height: initial !important;
        display: inline;
        margin: <?= ($lang == 'arabic') ? '0px 0px 0px 10px' : '0px 10px 0px 0px'; ?>;
    }

    #sortable tr td:nth-child(1), #sortable tr td:nth-child(2), #sortable tr td:nth-child(3) {
        cursor: move;
        transition: all 2s ease;
    }

    .ui-sortable-helper {
        display: none;
    }

    .ui-sortable-placeholder {
        visibility: visible !important;
        cursor: move;
        background: gray;
    }

</style>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/approvals/approvals_list" class="btn btn-primary"><i
                        class="fa fa-list-alt"></i> <?= lang('approvals_list') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <!-- Buttons -->
            <div class="buttons">
                <div class="buttons-container">
                    <button class="btn btn-success" id="show" data-toggle="modal"><i class="fa fa-list-alt"></i>
                    </button>
                    <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                    <button class="btn btn-dropbox" onclick="submit_form()"><i class="fa fa-save"></i></button>
                    <h3><?= (empty(@$approvals_cat_info)) ? lang('new_approval_chain') : (($lang == 'arabic') ? @$approvals_cat_info->approval_cat_ar : @$approvals_cat_info->approval_cat_en); ?></h3>
                </div>
            </div>
            <!-- Buttons -->

            <div id="accordion" class="panel-group">

                <!---------- PANEL 1 ---------->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                               onclick="show(1)"><?= lang('approvals_data') ?></a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form role="form" id="form"
                                  action="<?php echo base_url() ?>admin/approvals/save_approval/<?= @$approvals_cat_info->approval_cat_id ?>"
                                  method="post" class="form-horizontal form1">
                                <!-- Description Arabic -->
                                <?php if (!empty(@$approvals_cat_info->approval_cat_id)): ?>
                                    <input type="hidden" name="approval_cat_id"
                                           value="<?= @$approvals_cat_info->approval_cat_id ?>"/>
                                <?php endif; ?>

                                <!-- Description Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_ar') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="approval_cat_ar" id="name_ar" class="form-control"
                                               id="field-1" required=""
                                               value="<?= @$approvals_cat_info->approval_cat_ar ?>">
                                    </div>
                                </div>

                                <!-- Description English -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_en') ?>
                                        <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="approval_cat_en" id="name_ar" class="form-control"
                                               id="field-1" required=""
                                               value="<?= @$approvals_cat_info->approval_cat_en ?>">
                                    </div>
                                </div>

                                <!-- Status -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('active') ?> <span
                                                class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="approval_cat_status" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="1" <?= (@$approvals_cat_info->approval_cat_status == 1) ? 'checked' : ''; ?>>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('yes') ?></span>
                                        </label>
                                        <label class="custom-control custom-radio" style="display: block">
                                            <input id="radioStacked1" name="approval_cat_status" type="radio"
                                                   class="custom-control-input" required=""
                                                   value="0" <?= (!empty(@$approvals_cat_info) and @ $approvals_cat_info->approval_cat_status == 0) ? 'checked' : ''; ?>>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"><?= lang('no') ?></span>
                                        </label>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!---------- PANEL 1 ---------->
                <?php if (!empty(@$approvals_cat_info)): ?>
                    <!---------- PANEL 2 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                                   onclick="show(2)"><?= lang('approvals_scope') ?></a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">

                                    <!-- Job Place -->
                                    <div class="form-group">
                                        <label for="x1" class="control-label col-sm-3"><?= lang('job_place') ?></label>
                                        <div class="col-sm-5">
                                            <select name="jplc" id="x1" class="form-control">
                                                <option value="0"><?= lang('all') ?></option>
                                                <?php foreach ($jplaces as $jplc): ?>
                                                    <option value="<?= $jplc->job_place_id ?>" <?= (@$approvals_cat_info->jplc == $jplc->job_place_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $jplc->place_name_ar : $jplc->place_name_en; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Departement -->
                                    <div class="form-group">
                                        <label for="x2" class="control-label col-sm-3"><?= lang('department') ?></label>
                                        <div class="col-sm-5">
                                            <select name="dep" id="x2" class="form-control"
                                                    onchange="print_sects(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                                <?php foreach ($deps as $dep): ?>
                                                    <option value="<?= $dep->department_id ?>" <?= (@$approvals_cat_info->dep == $dep->department_id) ? 'selected' : ''; ?>>
                                                        <?= ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Sections -->
                                    <div class="form-group">
                                        <label for="x3"
                                               class="control-label col-sm-3"><?= lang('designation') ?></label>
                                        <div class="col-sm-5">
                                            <select name="sect" id="x3" class="form-control"
                                                    onchange="print_units(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_sects"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                    <!-- Units -->
                                    <div class="form-group">
                                        <label for="x4" class="control-label col-sm-3"><?= lang('aunit') ?></label>
                                        <div class="col-sm-5">
                                            <select name="unit" id="x4" class="form-control"
                                                    onchange="print_divis(this.value)">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_units"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                    <!-- Divisions -->
                                    <div class="form-group">
                                        <label for="x5" class="control-label col-sm-3"><?= lang('adivision') ?></label>
                                        <div class="col-sm-5">
                                            <select name="divi" id="x5" class="form-control">
                                                <option value="0"><?= lang('all') ?></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 spinner_divis"><i
                                                    class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 2 ---------->

                    <!---------- PANEL 3 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                   onclick="show(3)"><?= lang('approvals_comm_adm') ?></a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval2/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <p><b><?= lang('choose_admin') ?> : </b></p>

                                    <table class="table table-responsive" id="admin_table">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width='10%'><?= lang('action') ?></th>
                                            <th width='40%'><?= lang('responsability') ?></th>
                                            <th width='50%'><?= lang('name') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (@$approvals_cat_info->admins != ''): ?>
                                            <?php foreach (explode(';', @$approvals_cat_info->admins) as $adm): ?>
                                                <?php if ($adm != @$hradmin->employee_id): ?>
                                                    <tr>
                                                        <td class="text-center" style="color:red">
                                                            <i class="fa fa-times" style="cursor:pointer;"
                                                               onclick="$(this).closest('tr').remove()"></i>
                                                            <input type="hidden" name="admins[]" value="<?= $adm ?>">
                                                        </td>
                                                        <?php foreach ($employees as $emp): ?>
                                                            <?php if ($emp->employee_id == $adm): ?>
                                                                <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                                <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </tr>
                                                <?php else: ?>
                                                    <tr>
                                                        <td class="text-center" style="color:red">
                                                            <i class="fa fa-times" style="cursor:pointer;"
                                                               onclick="$(this).closest('tr').remove()"></i>
                                                            <input type="hidden" name="admins[]"
                                                                   value="<?= @$hradmin->employee_id ?>"></td>
                                                        <td><?= lang('hr_admin') ?></td>
                                                        <td><?= ($lang == 'arabic') ? @$hradmin->full_name_ar : @$hradmin->full_name_en; ?></td>
                                                    </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 3 ---------->

                    <!---------- PANEL 4 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
                                   onclick="show(4)"><?= lang('approvals_comm_emp') ?></a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval3/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <p><b><?= lang('choose_employees') ?> : </b></p>
                                    <table class="table table-responsive" id="employee_table">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width='10%'><?= lang('action') ?></th>
                                            <th width='40%'><?= lang('job_title') ?></th>
                                            <th width='50%'><?= lang('name') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center"><input type="hidden" name="employees[]" value="dm">
                                            </td>
                                            <td><?= lang('direct_manager') ?></td>
                                            <td><?= lang('direct_manager') ?></td>
                                        </tr>
                                        <?php foreach (explode(';', @$approvals_cat_info->employees) as $empx): ?>
                                            <?php if ($empx != 'dm'): ?>
                                                <tr>
                                                    <td class="text-center" style="color:red">
                                                        <i class="fa fa-times" style="cursor:pointer;"
                                                           onclick="$(this).closest('tr').remove()"></i>
                                                        <input type="hidden" name="employees[]" value="<?= $empx ?>">
                                                    </td>
                                                    <?php foreach ($employees_non_admins as $emp): ?>
                                                        <?php if ($emp->employee_id == $empx): ?>
                                                            <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                            <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?></td>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 4 ---------->

                    <!---------- PANEL 5 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
                                   onclick="show(5)"><?= lang('approvals_comm_squence') ?></a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">

                                <form role="form" id="form"
                                      action="<?php echo base_url() ?>admin/approvals/save_configure_approval4/<?= @$approvals_cat_info->approval_cat_id ?>"
                                      method="post" class="form-horizontal  form2">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <th width="11%" class="text-center"><?= lang('order') ?></th>
                                        <th width="28%" class="text-center"><?= lang('comm_members') ?></th>
                                        <th width="29%" class="text-center"><?= lang('member_type') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_accept') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_refuse') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_delegate') ?></th>
                                        <th width="8%" class="text-center"><?= lang('appro_consultation') ?></th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $all_list = explode(';', $approvals_cat_info->all_list); ?>
                                        <?php
                                            $n = count($all_list);
                                            $accept = array_fill(0, $n, 0);
                                            $refuse= array_fill(0, $n, 0);
                                            $delegate = array_fill(0, $n, 0);
                                            $consultation = array_fill(0, $n, 0);
                                        ?>
                                        <?php $accept = array_merge(explode(';', $approvals_cat_info->accept), $accept); ?>
                                        <?php $refuse = array_merge(explode(';', $approvals_cat_info->refuse), $refuse); ?>
                                        <?php $delegate = array_merge(explode(';', $approvals_cat_info->delegate), $delegate); ?>
                                        <?php $consultation = array_merge(explode(';', $approvals_cat_info->consultation), $consultation); ?>
                                        
                                        <?php for ($i = 0; $i < count($all_list); $i++): ?>
                                            <tr id="<?= $i + 1 ?>">
                                                <td class="text-center order">
                                                    <?= $i + 1 ?>
                                                </td>
                                                <?php if ($all_list[$i] == 'dm'): ?>
                                                    <td><?= lang('direct_manager') ?><input type="hidden"
                                                                                            name="all_list[]"
                                                                                            value="<?= $all_list[$i] ?>">
                                                    </td>
                                                    <td><?= lang('direct_manager') ?></td>
                                                <?php else: ?>
                                                    <?php foreach ($employees as $emp): ?>
                                                        <?php if ($emp->employee_id == $all_list[$i]): ?>
                                                            <td><?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                                <input type="hidden" name="all_list[]"
                                                                       value="<?= $all_list[$i] ?>"></td>
                                                            <td><?= ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?></td>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($accept[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $accept[$i] ?>" name="accept[]">
                                                </td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($refuse[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $refuse[$i] ?>" name="refuse[]">
                                                </td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($delegate[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $delegate[$i] ?>"
                                                            name="delegate[]"></td>
                                                <td class="text-center"><input
                                                            type="checkbox" <?= ($consultation[$i] == 1) ? 'checked' : ''; ?>><input
                                                            type="hidden" value="<?= $consultation[$i] ?>"
                                                            name="consultation[]"></td>
                                            </tr>
                                        <?php endfor; ?>

                                        </tbody>
                                    </table>
                                </form>
                                <label class="control-label" for="checkall">
                                    <input type="checkbox" class="checkall" name="checkall">
                                    <?= lang('all') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 5 ---------->
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!-- MODAL ADMIN -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="font-size: 1.2em;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-center" id="myModalLabel"><?= lang('choose_admin') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="text-center"><b><?= lang('get_dep_admins'); ?></b></div>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th width='20%'></th>
                            <th width='40%'><?= lang('responsability') ?></th>
                            <th width='40%'><?= lang('name') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($get_dep_admins as $adm): ?>
                            <tr>
                                <?php $jobtitle = ($lang == 'arabic') ? $adm->job_titles_name_ar : $adm->job_titles_name_en; ?>
                                <?php $name = ($lang == 'arabic') ? $adm->full_name_ar : $adm->full_name_en; ?>
                                <td>
                                    <button class="btn btn-primary btn-xs"
                                            onclick="add_admin(<?= $adm->employee_id ?>, '<?= $jobtitle; ?>', '<?= $name; ?>')"><?= lang('add') ?></button>
                                </td>
                                <td><?= $jobtitle; ?></td>
                                <td><?= $name; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="text-center"><b><?= lang('get_des_admins'); ?></b></div>
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th width='20%'></th>
                            <th width='40%'><?= lang('responsability') ?></th>
                            <th width='40%'><?= lang('name') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($get_des_admins as $adm): ?>
                            <tr>
                                <?php $jobtitle = ($lang == 'arabic') ? $adm->job_titles_name_ar : $adm->job_titles_name_en; ?>
                                <?php $name = ($lang == 'arabic') ? $adm->full_name_ar : $adm->full_name_en ?>
                                <td>
                                    <button class="btn btn-primary btn-xs"
                                            onclick="add_admin(<?= $adm->employee_id ?>, '<?= $jobtitle; ?>', '<?= $name; ?>')"><?= lang('add') ?></button>
                                </td>
                                <td><?= $jobtitle; ?></td>
                                <td><?= $name; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL ADMIN -->

    <!-- MODAL EMPLOYEE -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="font-size: 1.2em;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-center" id="myModalLabel"><?= lang('choose_employees') ?></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th width='20%'></th>
                            <th width='40%'><?= lang('job_title') ?></th>
                            <th width='40%'><?= lang('name') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($employees_non_admins as $emp): ?>
                            <tr>
                                <?php $jobtitle = ($lang == 'arabic') ? $emp->job_titles_name_ar : $emp->job_titles_name_en; ?>
                                <?php $name = ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en ?>
                                <td>
                                    <button class="btn btn-primary btn-xs"
                                            onclick="add_employee(<?= $emp->employee_id ?>, '<?= $jobtitle; ?>', '<?= $name; ?>')"><?= lang('add') ?></button>
                                </td>
                                <td><?= $jobtitle; ?></td>
                                <td><?= $name; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL EMPLOYEE -->

    <script type="text/javascript">
        var lang = "<?= $this->session->userdata('lang'); ?>";
        $(function () {
            $('#show').prop('disabled', true);
            $("#sortable").sortable({
                beforeStop: function (event, ui) {
                    $('#sortable tr td:first-child.order').map(function (index) {
                        $(this).html(index + 1);
                    });
                }
            });
            $('#sortable').find(':checkbox').map(function () {
                $(this).change(function () {
                    if (this.checked)
                        $(this).next(':hidden').val('1');
                    else
                        $(this).next(':hidden').val(2);
                });
            });
        });
        function submit_form() {
            $('.panel-collapse.collapse.in').find('form').map(function () {
                $(this).submit();
            });
        }
        function show(x) {
            if (x == 3) {
                $('#show').prop('disabled', false);
                $('#show').attr('data-target', '#myModal1');
            } else if (x == 4) {
                $('#show').prop('disabled', false);
                $('#show').attr('data-target', '#myModal2');
            } else {
                $('#show').prop('disabled', true);
                $('#show').attr('data-target', '');
            }
        }
        function add_admin(id, jt, name) {
            $('#admin_table').append('<tr>' +
                '<td class="text-center" style="color:red"><i class="fa fa-times" style="cursor:pointer;" onclick="$(this).closest(\'tr\').remove()"></i>' +
                '<input type="hidden" name="admins[]" value="' + id + '">' +
                '</td>' +
                '<td>' + jt + '</td>' +
                '<td>' + name + '</td>' +
                '</tr>');
        }
        function add_employee(id, jt, name) {
            $('#employee_table').append('<tr>' +
                '<td class="text-center" style="color:red"><i class="fa fa-times" style="cursor:pointer;" onclick="$(this).closest(\'tr\').remove()"></i>' +
                '<input type="hidden" name="employees[]" value="' + id + '">' +
                '</td>' +
                '<td>' + jt + '</td>' +
                '<td>' + name + '</td>' +
                '</tr>');
        }
        ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
        ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
        ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
        function print_sects(id) {
            var vurl = "<?php echo base_url() ?>admin/approvals/get_sec_with_dep/" + id
            if (id) {
                $('.spinner_sects').fadeIn('fast');
                $.ajax({
                    url: vurl,
                    success: function (data) {
                        reset_sect();
                        reset_unit();
                        reset_divi();
                        for (var i = 0; i < data.length; i++) {
                            if (lang == 'arabic')
                                $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations_ar + '</option>');
                            else
                                $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                        }
                    },
                    dataType: 'json'
                }).fail(function () {
                    alert('<?= lang('alert_error') ?>');
                }).always(function () {
                    $('.spinner_sects').fadeOut('fast');
                    $('#x3').find('option').map(function () {
                        if ($(this).val() ==<?= (!empty(@$approvals_cat_info)) ? @$approvals_cat_info->sect : 0 ?>)
                            $(this).prop("selected", true);
                    });
                });
            } else {
                reset_sect();
                reset_unit();
                reset_divi();
            }
        }
        ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
        ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
        ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
        function print_units(id) {
            var vurl = "<?php echo base_url() ?>admin/approvals/get_unit_with_sec/" + id
            if (id) {
                $('.spinner_units').fadeIn('fast');
                $.ajax({
                    url: vurl,
                    success: function (data) {
                        reset_unit();
                        reset_divi();
                        for (var i = 0; i < data.length; i++) {
                            if (lang == 'arabic')
                                $('#x4').append('<option value="' + data[i].unit_id + '">' + data[i].unit_ar + '</option>');
                            else
                                $('#x4').append('<option value="' + data[i].unit_id + '">' + data[i].unit_en + '</option>');
                        }
                    },
                    dataType: 'json'
                }).fail(function () {
                    alert('<?= lang('alert_error') ?>');
                }).always(function () {
                    $('.spinner_units').fadeOut('fast');
                    $('#x4').find('option').map(function () {
                        if ($(this).val() ==<?= (!empty(@$approvals_cat_info)) ? @$approvals_cat_info->unit : 0 ?>)
                            $(this).prop("selected", true);
                    });
                });
            } else {
                reset_unit();
                reset_divi();
            }
        }
        ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
        ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
        ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
        function print_divis(id) {
            var vurl = "<?php echo base_url() ?>admin/approvals/get_division_with_unit/" + id;
            if (id) {
                $('.spinner_divis').fadeIn('fast');
                $.ajax({
                    url: vurl,
                    success: function (data) {
                        reset_divi();
                        for (var i = 0; i < data.length; i++) {
                            if (lang == 'arabic')
                                $('#x5').append('<option value="' + data[i].division_id + '">' + data[i].division_ar + '</option>');
                            else
                                $('#x5').append('<option value="' + data[i].division_id + '">' + data[i].division_en + '</option>');
                        }
                    },
                    dataType: 'json'
                }).fail(function () {
                    alert('<?= lang('alert_error') ?>');
                }).always(function () {
                    $('.spinner_divis').fadeOut('fast');
                    $('#x5').find('option').map(function () {
                        if ($(this).val() ==<?= (!empty(@$approvals_cat_info)) ? @$approvals_cat_info->divi : 0 ?>)
                            $(this).prop("selected", true);
                    });
                });
            } else {
                reset_divi();
            }
        }
        ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
        ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
        ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
        function reset_sect() {
            $('#x3').empty();
            $('#x3').append('<option value="0"><?= lang('all') ?></option>');
        }
        function reset_unit() {
            $('#x4').empty();
            $('#x4').append('<option value="0"><?= lang('all') ?></option>');
        }
        function reset_divi() {
            $('#x5').empty();
            $('#x5').append('<option value="0"><?= lang('all') ?></option>');
        }
        ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
        ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
        ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
        <?php if (!empty(@$approvals_cat_info)): ?>
        $(function () {
            $.ajax({
                url: print_sects(<?= @$approvals_cat_info->dep ?>),
                success: function () {
                    $.ajax({
                        url: print_units(<?= @$approvals_cat_info->sect ?>),
                        success: function () {
                            print_divis(<?= @$approvals_cat_info->unit ?>);
                        }
                    });
                }
            });
        });
        <?php endif; ?>
        $(function () {
           $('.checkall').change(function () {
               if($(this).is(":checked")){
                   $('#sortable :checkbox').map(function () {
                       $(this).prop('checked', true);
                       $(this).next(':hidden').val('1');
                   })
               }
               else{
                   $('#sortable :checkbox').map(function () {
                       $(this).prop('checked', false);
                       $(this).next(':hidden').val('0');
                   })
               }
           });
        });
    </script>