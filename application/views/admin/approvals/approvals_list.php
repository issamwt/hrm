<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    i.fa.type{
        font-size: 1.4em;
    }
    i.fa-check{
        color: #24e624;
    }
    i.fa-times{
        color: #ff192d;
    }
    i.fa-question{
        color: #1949ff;
    }
</style>
<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/approvals/add_approval" class="btn btn-primary"><i class="fa fa-plus"></i> <?= lang('add_approval') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none; ">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th class="text-center" width="6%"><?= lang('sl') ?></th>
                        <th class="text-center" width="22%"><?= lang('description_ar') ?></th>
                        <th class="text-center" width="22%"><?= lang('description_en') ?></th>
                        <th class="text-center" width="5%"><?= lang('status') ?></th>
                        <th class="text-center" width="19%"><?= lang('action') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($approvals_cat_list)): $sl = 1; ?>
                        <?php foreach ($approvals_cat_list as $app_cat): ?>
                            <tr>
                                <td class="text-center"><?= $sl++; ?></td>

                                <td><?= $app_cat->approval_cat_ar ?></td>
                                <td style="direction: ltr !important;"><?= $app_cat->approval_cat_en ?></td>

                                <td class="text-center">
                                    <?php
                                    switch ($app_cat->approval_cat_status) {
                                        case 1:echo '<i class="fa fa-check type"></i>';
                                            break;
                                        case 0:echo '<i class="fa fa-times type"></i>';
                                            break;
                                        default:echo '<i class="fa fa-question type"></i>';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <?php $link = base_url() . 'admin/approvals/'; ?>
                                    <a  href="<?= $link . 'add_approval/' . $app_cat->approval_cat_id ?>" class="btn btn-primary btn-xs"><i class="fa fa-gears"></i> <?= lang('config') ?></a>
                                    <!--<a  href="<?= $link . 'follow_up/' . $app_cat->approval_cat_id ?>" class="btn btn-success btn-xs"><i class="fa fa-eye"></i> <?= lang('follow_up') ?></a>-->
                                    <a  href="<?= $link . 'delete_approval/' . $app_cat->approval_cat_id ?>" class="btn btn-danger btn-xs"onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
