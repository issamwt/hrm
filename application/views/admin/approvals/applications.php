<br><br>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="pull-right col-sm-12">

    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none; ">
            <table class="table table-bordered" id="dataTables-example">
                <thead>
                <tr>
                    <th class="text-center"><?= lang('id_app') ?></th>
                    <th class="text-center"><?= lang('date_app') ?></th>
                    <th class="text-center"><?= lang('employee_id') ?></th>
                    <th class="text-center"><?= lang('employee_name') ?></th>
                    <th class="text-center"><?= lang('app_type') ?></th>
                    <th class="text-center"><?= lang('configure_approval') ?></th>
                    <th class="text-center"><?= lang('status') ?></th>
                    <th class="text-center"><?= lang('action') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($applications_list)): ?>
                    <?php foreach ($applications_list as $app): ?>
                        <tr>
                            <td class="text-center"><?= $app->application_id ?></td>
                            <td><?= $app->date ?></td>
                            <td>
                                <?php foreach ($all as $emp): ?>
                                    <?php if ($emp->employee_id == $app->employee_id): ?>
                                        <?= $emp->employment_id ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <?php foreach ($all as $emp): ?>
                                    <?php if ($emp->employee_id == $app->employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td><?= lang($app->type_app) ?></td>
                            <td>
                                <?php foreach ($approval_cats as $ac): ?>
                                    <?php if ($ac->approval_cat_id == $app->approval_cat_id): ?>
                                        <?= ($lang == 'english') ? $ac->approval_cat_en : $ac->approval_cat_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td class="text-center"><?= status($app->status) ?></td>
                            <td class="text-center">
                                <?php echo btn_view_modal('admin/approvals/view_application/' . $app->application_id) ?>
                                <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/approvals/delete_application/<?= $app->application_id ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<?php
function status($x)
{
    switch ($x) {
        case '0':
            echo '<span class="label label-warning">' . lang('pending') . '</span>';
            break;
        case '1':
            echo '<span class="label label-success">' . lang('accepted') . '</span>';
            break;
        case '2':
            echo '<span class="label label-danger">' . lang('rejected') . '</span>';
            break;
        default:
            echo '?';
            break;
    }
}

?>