<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    .follow_up_header{
        background: lightgray;
        width: 100%;
        font-weight: bold;
        text-align: center;
        padding:4px;
        margin-bottom: 20px;
    }
    .form{
        font-size: 1.1em;
        font-weight: bold;
    }
    .form .btn{
        border-radius: 0px;
        font-weight: bold;
    }
    .btn-default {
        background-color: #f4f4f4 !important;
    }
    .part1 select{
        min-height: 250px;
    }
    .span{padding: 7px;
          width: 100%;
          text-align: center;
          display: block;
    }
    .modal-body{
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .modal-body img{
        width: 90%;
        display: block;
        margin: 20px auto;
        border: 1px solid gray;
    }
</style>

<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/approvals/approvals_list" class="btn btn-primary"><i class="fa fa-list-alt"></i> <?= lang('approvals_list') ?></a>
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-question-circle"></i></a>
        </div>
    </div>

    <!-- Start Modal Help -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="font-size: 1.2em;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-center" id="myModalLabel"><?= lang('help') ?></h4>
                </div>
                <div class="modal-body">
                    <ol>
                        <li><?= lang('help1') ?></li>
                        <li>
                            <?= lang('help2') ?>
                            <img src="<?= base_url() ?>asset/images/help/Capture1_<?= ($lang == 'arabic') ? 'ar' : 'en'; ?>.PNG">
                        </li>
                        <li><?= lang('help3') ?></li>
                        <li>
                            <?= lang('help4') ?>
                            <img src="<?= base_url() ?>asset/images/help/Capture2_<?= ($lang == 'arabic') ? 'ar' : 'en'; ?>.PNG">
                        </li>
                        <li>
                            <?= lang('help5') ?>
                            <img src="<?= base_url() ?>asset/images/help/Capture3_<?= ($lang == 'arabic') ? 'ar' : 'en'; ?>.PNG">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Help -->

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <form class="form" id="form" action="<?php echo base_url() ?>admin/approvals/save_link_approvals" method="post">

                <!-- PART 2 -->
                <div class="follow_up_header"><?= lang('link_approvals') ?></div>
                <div class="row part1">
                    <div class="col-sm-5">
                        <span class="span"><?= lang('configure_approvals') ?></span>
                        <select multiple="" class="form-control select3">
                            <?php foreach ($approvals_cat_list as $value): ?>
                                <option value="<?= $value->approval_cat_id ?>" data-linked-apps="<?= $value->linked_apps ?>">
                                    <?= ($lang == 'arabic') ? $value->approval_cat_ar : $value->approval_cat_en; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <span class="span" style="visibility: hidden;"><?= lang('action') ?></span>
                        <button onclick="move_all2()" type="button" class="btn btn-primary btn-block">>></button>
                        <button onclick="move_one2()" type="button" class="btn btn-primary btn-block">></button>
                        <button onclick="back_one2()" type="button" class="btn btn-primary btn-block"><</button>
                        <button onclick="back_all2()" type="button" class="btn btn-primary btn-block"><<</button>
                    </div>
                    <div class="col-sm-5">
                        <span class="span"><?= lang('link_approval_expression') ?></span>
                        <select multiple="" class="form-control select4">

                        </select>
                    </div>
                </div>
                <!-- PART 2 -->

                <!-- PART 1 -->
                <div class="follow_up_header" style="margin-top: 20px;"><?= lang('link_approval_list') ?></div>
                <div class="row part1">
                    <div class="col-sm-5">
                        <select multiple="" class="form-control select1">
                            <optgroup label="<?= lang('la_advance') ?>">
                                <option value="la_advance/0"><?= lang('all') ?> (<?= lang('la_advance') ?>)</option>
                                <?php foreach ($la_advance as $value): ?>
                                    <option value="la_advance/<?= $value->advance_id ?>">
                                        <?= ($lang == 'arabic') ? $value->title_ar : $value->title_en; ?>
                                    </option>
                                <?php endforeach; ?>
                            </optgroup>
                            <optgroup label="<?= lang('la_vacations') ?>">
                                <option value="la_vacations/0"><?= lang('all') ?> (<?= lang('la_vacations') ?>)</option>
                                <?php foreach ($la_vacations as $value): ?>
                                    <option value="la_vacations/<?= $value->leave_category_id ?>"><?= ($lang == 'arabic') ? $value->category_ar : $value->category_en ?></option>
                                <?php endforeach; ?>
                            </optgroup>
                            <option value="la_caching"><?= lang('la_caching') ?></option>
                            <option value="la_filter_dues"><?= lang('la_filter_dues') ?></option>
                            <option value="la_custody"><?= lang('la_custody') ?></option>
                            <option value="la_resignation"><?= lang('la_resignation') ?></option>
                            <option value="la_transfer"><?= lang('la_transfer') ?></option>
                            <option value="la_training"><?= lang('la_training') ?></option>
                            <option value="la_overtime"><?= lang('la_overtime') ?></option>
                            <option value="la_purchase"><?= lang('la_purchase') ?></option>
                            <option value="la_maintenance"><?= lang('la_maintenance') ?></option>
                            <option value="la_annual_assessment"><?= lang('la_annual_assessment') ?></option>
                            <option value="la_permission"><?= lang('la_permission') ?></option>
                            <option value="la_def_salary"><?= lang('la_def_salary') ?></option>
                            <option value="la_recrutement"><?= lang('la_recrutement') ?></option>
                            <option value="la_job_application"><?= lang('la_job_application') ?></option>
                            <option value="la_embarkation"><?= lang('la_embarkation') ?></option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button onclick="move_all1()" type="button" class="btn btn-primary btn-block"><i class="fa fa-check"></i></button>
                        <button onclick="move_one1()" type="button" class="btn btn-primary btn-block">></button>
                        <button onclick="back_all1()" type="button" class="btn btn-primary btn-block"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="col-sm-5">
                        <select multiple="" class="form-control select2">


                        </select>
                    </div>
                </div>
                <!-- PART 1 -->

                <div >
                    <input type="hidden" class="form-control" name="approvals" required="">
                    <input type="hidden" class="form-control" name="applications" required="">
                </div>

                <!-- Submit Button -->
                <div class="form-group row" style="margin-top: 40px">
                    <div class="col-sm-offset-5 col-sm-2">
                        <button type="submit" class="btn btn-block btn-primary"><?= lang('save') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function move_all1() {
        $('.select1').blur();
        $('.select1').find('option').map(function () {
            var clone = $(this).clone();
            $('.select2').append(clone);
        });
        get_applications_id();
    }
    function move_one1() {
        var array = $('.select1').val();
        $('.select1').find(':selected').map(function () {

            var clone = $(this).clone();
            $('.select2').append(clone);

        });
        get_applications_id();
    }

    function back_all1() {
        $('.select2').empty();
        get_applications_id();
    }
    /////////////////////////////////////////////////////////////////////////////////////

    function move_all2() {
        $('.select3').blur();
        $('.select3').find('option').map(function () {
            $('.select4').append($(this));
        });
        get_approvals_id();
        show_linked_apps();
    }
    function move_one2() {
        //$('.select3').blur();
        var array = $('.select3').val();
        $('.select3').find('option').map(function () {
            if ($(this).attr("value") == $('.select3 :selected').val() || $.inArray($(this).attr("value"), array) == 1) {
                $('.select4').append($(this));
            }
        });
        get_approvals_id();
        show_linked_apps();
    }
    function back_one2() {
        //$('.select4').blur();
        var array = $('.select4').val();
        $('.select4').find('option').map(function () {
            if ($(this).attr("value") == $('.select4 :selected').val() || $.inArray($(this).attr("value"), array) == 1) {
                $('.select3').append($(this));
            }
        });
        get_approvals_id();
        show_linked_apps();
    }
    function back_all2() {
        $('.select4').blur();
        $('.select4').find('option').map(function () {
            $('.select3').append($(this));
        });
        get_approvals_id();
        show_linked_apps();
    }

    function get_applications_id() {
        var array1 = [];
        $('.select2').find('option').map(function () {
            array1.push($(this).attr("value"));
        });
        $('input[name=applications]').val(array1);

    }

    function get_approvals_id() {
        var array2 = [];
        $('.select4').find('option').map(function () {
            array2.push($(this).attr("value"));
        });
        $('input[name=approvals]').val(array2);

    }

    function show_linked_apps() {
        $('.select2').empty();
        $('.select4').find('option').map(function () {
            var linked_apps = $(this).attr("data-linked-apps").split(',');
            for (var i = 0; i < linked_apps.length; i++) {
                console.log(linked_apps[i]);
                $('.select1').find('option').map(function () {
                    if ($(this).attr('value') == linked_apps[i]) {
                        var clone = $(this).clone();
                        $('.select2').append(clone);
                    }
                });
            }
        });
        get_applications_id();
    }

    $('form').submit(function () {
        if ($('.select4').find('option').length == 0)
            return false;
    });
</script>