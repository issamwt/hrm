<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<style type="text/css">
    .follow_up_header{
        background: lightgray;
        width: 100%;
        font-weight: bold;
        text-align: center;
        padding:4px;
        margin-bottom: 20px;
    }
    .form{
        font-size: 1.1em;
        font-weight: bold;
    }
    .btn-default {
        background-color: #f4f4f4 !important;
    }
</style>

<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/approvals/approvals_list" class="btn btn-primary"><i class="fa fa-list-alt"></i> <?= lang('approvals_list') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <div class="follow_up_header"><?= ($lang == 'arabic') ? $approvals_cat_info->approval_cat_ar : $approvals_cat_info->approval_cat_en; ?></div>

            <form role="form" id="form" action="<?php echo base_url() ?>admin/approvals/save_follow_up/<?= $approvals_cat_info->approval_cat_id ?>" method="post" class="form-horizontal">

                <input type="checkbox" name="automatic" <?= ($approvals_cat_info->automatic) ? 'checked' : ''; ?>> <?= lang('follow_up1') ?>
                <br><br>
                <?= lang('follow_up2') ?>
                <select style="direction: ltr" name="approval_cat_delay">
                    <option value="5" <?= ($approvals_cat_info->approval_cat_delay == 5) ? 'selected' : ''; ?>>5</option>
                    <option value="10" <?= ($approvals_cat_info->approval_cat_delay == 10) ? 'selected' : ''; ?>>10</option>
                    <option value="15" <?= ($approvals_cat_info->approval_cat_delay == 15) ? 'selected' : ''; ?>>15</option>
                </select>
                <?= lang('follow_up3') ?>
                <select style="direction: ltr" name="approval_cat_times">
                    <option value="1" <?= ($approvals_cat_info->approval_cat_times == 1) ? 'selected' : ''; ?>>1</option>
                    <option value="2" <?= ($approvals_cat_info->approval_cat_times == 2) ? 'selected' : ''; ?>>2</option>
                    <option value="3" <?= ($approvals_cat_info->approval_cat_times == 3) ? 'selected' : ''; ?>>3</option>
                </select>
                <?= lang('follow_up4') ?>
                <?= lang('follow_up5') ?>
                <br><br>
                <ul style="list-style-type: none">
                    <li>
                        <input type="radio" name="approval_cat_action" value="1" required="" <?= ($approvals_cat_info->approval_cat_action == 1) ? 'checked' : ''; ?>> <?= lang('follow_up6') ?>
                    </li>
                    <li>
                        <input type="radio" name="approval_cat_action" value="2" required="" <?= ($approvals_cat_info->approval_cat_action == 2) ? 'checked' : ''; ?>> <?= lang('follow_up7') ?>
                    </li>
                </ul>

                <br><br>
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-6">
                        <button type="submit" class="btn btn-primary" style="border-radius: 0px;"><?= lang('save') ?></button>
                        <a href="<?php echo base_url() ?>admin/approvals/approvals_list" class="btn btn-default" style="border-radius: 0px;"><?= lang('exit') ?></a>
                    </div>
                </div>
            </form>
            </form>
        </div>
    </div>
</div>