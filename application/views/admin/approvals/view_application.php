<style type="text/css">
    .ids {
        border: 2px solid darkslategray;
    }

    .ids.current {
        border: 2px solid red;
        border-style: double;
    }

    .table-container {
        padding: 20px;
        padding-top: 10px;
    }

    h4 {
        color: #fff;
        background: #337AB7;
        padding: 5px 0px;
    }
</style>
<div class="col-md-12">
    <div class="box box-primary">
        <!-- Default panel contents -->

        <div class="table-container">

            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only"><?= lang('close') ?></span></button>
                <div class="panel-title">
                    <strong><?= lang('view_application') ?></strong>
                </div>
            </div>
            <!--  /////////////////  la_vacations //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_vacations"): ?>
                <h4 class="text-center"><?= lang('la_vacations') ?></h4>
                <table class="table table-non-bordered">
                    <tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <!--////////////////////////////////-->
                    <?php if(@$view_application->type==1):?>
                        <?php foreach ($employees_list as $emp): ?>
                            <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                <?php
                                @$employee_detail->joining_date = $emp->joining_date;
                                @$employee_detail->holiday_no=$emp->holiday_no;
                                @$employee_detail->old_rest = $emp->old_rest;
                                @$employee_detail->new_balance = $emp->new_balance;
                                ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if ($lang == 'english')
                            $months = array("Muharram", "Safar", "Rabi' al-awwal", "Rabi' al-thani", "Jumada al-awwal", "Jumada al-thani", "Rajab", "Sha'aban", "Ramadan", "Shawwal", "Dhu al-Qi'dah", "Dhu al-Hijjah");
                        else
                            $months = array("محرّم", "صفر", "ربيع الأول", "ربيع الثاني", "جمادى الاول", "جمادى الآخر أو جمادى الثاني", "رجب", "شعبان", "رمضان", "شوّال", "ذو القعدة", "ذو الحجة");
                        $d = explode('-', @$employee_detail->joining_date);
                        $d2 = explode('-',@$today);
                        // employee join the work in this year (or employee join the work before this year and joining date has passed)
                        if(($d2[0]==$d[0]) or (($d2[1]*30+$d2[2])>($d[1]*30+$d[2]))){
                            $days = abs(($d2[1]*30+$d2[2])-($d[1]*30+$d[2]));
                        }
                        // employee join the work before this year
                        else{
                            $date1 = new DateTime(($d2[0]-1).'-'.$d[1].'-'.$d[2]);
                            $date2 = new DateTime($today);
                            $days = $date2->diff($date1)->format("%a");
                        }
                        // $d[2].' '.$months[ltrim($d[1],'0')]
                        ?>
                        <tr>
                            <td width="30%"><?= lang('notes_emp') ?></td>
                            <td>
                                <?php $rest = (ceil(((@$employee_detail->holiday_no-@$employee_detail->new_balance) * $days) / 354)<=(@$employee_detail->holiday_no-@$employee_detail->new_balance))?ceil(((@$employee_detail->holiday_no-@$employee_detail->new_balance) * $days) / 354)+@$employee_detail->old_rest:(@$employee_detail->holiday_no-@$employee_detail->new_balance)+@$employee_detail->old_rest; ?>
                                <?= lang('holiday_no') ?> : <?= (@$employee_detail->holiday_no-@$employee_detail->new_balance) ?><br>
                                <?= lang('old_rest') ?> : <?= @$employee_detail->old_rest ?><br>
                                <?= lang('start_contractual_year') ?> : <?= $d[2].' '.$months[ltrim($d[1],'0')-1] ?><br>
                                <?= lang('days_of_work_this_yeas') ?> : <?=$days?><br>
                                <?= lang('yearly_holiday_no') ?> : <?=$rest?><br>
                            </td>
                        </tr>
                    <?php endif;?>
                    <!--////////////////////////////////-->
                    <tr>
                        <td width="30%"><?= lang('leave_type') ?></td>
                        <td>
                            <?php foreach (@$all_leave_category as $lc): ?>
                                <?php if ($lc->leave_category_id == @$view_application->type): ?>
                                    <?= ($lang == 'english') ? $lc->category_en : $lc->category_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('start_date') ?></td>
                        <td><?= @$view_application->start_date ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('end_date') ?></td>
                        <td><?= @$view_application->end_date ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('leave_duration') ?></td>
                        <td><?= @$view_application->leave_duration ?> <?= lang('days') ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('address_in_leave') ?></td>
                        <td><?= @$view_application->address_in_leave ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('telephone') ?></td>
                        <td><?= @$view_application->tel_in_leave ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('replacement') ?></td>
                        <td>
                            <?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == @$view_application->replacement): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('tikets') ?></td>
                        <td>
                            <?php
                            switch (@$view_application->tikets) {
                                case '1':
                                    echo lang('tikets_opt1');
                                    break;
                                case '2':
                                    echo lang('tikets_opt2');
                                    break;
                                case '3':
                                    echo lang('tikets_opt3');
                                    break;
                                default:
                                    echo '?';
                                    break;
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('going_date') ?></td>
                        <td><?= @$view_application->going_date ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('coming_date') ?></td>
                        <td><?= @$view_application->coming_date ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('going_date_2') ?></td>
                        <td><?= @$view_application->going_date_2 ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('coming_date_2') ?></td>
                        <td><?= @$view_application->coming_date_2 ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_advance //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_advance"): ?>
                <h4 class="text-center"><?= lang('la_advance') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('advance_type') ?></td>
                        <td>
                            <?php foreach (@$advances_list as $al): ?>
                                <?php if ($al->advance_id == @$view_application->type): ?>
                                    <?= ($lang == 'english') ? $al->title_en : $al->title_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('advance_value') ?></td>
                        <td><?= @$view_application->value ?> <?= lang('rial') ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('payement_method') ?></td>
                        <td><?= lang('payement_method'.@$view_application->payement_method) ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('monthly_installement') ?></td>
                        <td><?= @$view_application->monthly_installement ?> <?= lang('rial') ?></td>
                    </tr>

                    <tr>
                        <td><?= lang('payement_months') ?></td>
                        <td><?= @$view_application->payement_months ?> <?= lang('months') ?></td>
                    </tr>
                    <tr>
                        <td><?= lang('voucher') ?></td>
                        <td>
                            <?php if(@$view_application->other_employee_id==0): ?>
                                <?=lang('no-exist')?>
                            <?php else: ?>
                                <?php foreach (@$employees_list as $emp): ?>
                                    <?php if ($emp->employee_id == @$view_application->other_employee_id): ?>
                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('phone') ?></td>
                        <td><?= @$view_application->phone ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_caching //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_caching"): ?>
                <h4 class="text-center"><?= lang('la_caching') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('cahing_type') ?></td>
                        <td><?= lang('cahing_type'.$view_application->type) ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('beneficiary_name') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('beneficiary_address') ?></td>
                        <td><?= @$view_application->address_in_leave ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('phone') ?></td>
                        <td><?= @$view_application->phone ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('cahing_value') ?></td>
                        <td><?= @$view_application->value ?> <?=lang('rial')?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('bank_name') ?></td>
                        <td><?= @$view_application->bank_name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('branch_name') ?></td>
                        <td><?= @$view_application->branch_name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('account_name') ?></td>
                        <td><?= @$view_application->account_name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('account_number') ?></td>
                        <td><?= @$view_application->account_number ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('caching_reason') ?></td>
                        <td><?= @$view_application->caching_reason ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('description') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_custody //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_custody"): ?>
                <h4 class="text-center"><?= lang('la_custody') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>

                    <tr>
                        <td width="30%"><?= lang('name') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_transfer //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_transfer"): ?>
                <h4 class="text-center"><?= lang('la_transfer') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('job_title') ?></td>
                        <td>
                            <?php foreach (@$job_titles_list as $jt): ?>
                                <?php if ($jt->job_titles_id == @$view_application->new_jt): ?>
                                    <?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('department') ?></td>
                        <td>
                            <?php foreach (@$department_list as $dep): ?>
                                <?php if ($dep->department_id == @$view_application->new_dep): ?>
                                    <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('designation') ?></td>
                        <td>
                            <?php foreach (@$designations_list as $des): ?>
                                <?php if ($des->designations_id == @$view_application->new_sect): ?>
                                    <?= ($lang == 'english') ? $des->designations : $des->designations_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('employee_cat') ?></td>
                        <td>
                            <?php foreach (@$emp_cat_list as $ec): ?>
                                <?php if ($ec->id == @$view_application->new_emp_cat): ?>
                                    <?= ($lang == 'english') ? $ec->name_en : $ec->name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('job_time') ?></td>
                        <td><?= lang(@$view_application->new_jtme) ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_training //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_training"): ?>
                <h4 class="text-center"><?= lang('la_training') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_name') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_institute') ?></td>
                        <td><?= @$view_application->course_institute ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_date') ?></td>
                        <td><?= @$view_application->start_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_price') ?></td>
                        <td><?= @$view_application->value ?> <?= lang('rial') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_note') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_overtime //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_overtime"): ?>
                <h4 class="text-center"><?= lang('la_overtime') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('extra_work_type') ?></td>
                        <td>
                            <?php foreach (@$extra_work_list as $exw): ?>
                                <?php if ($exw->extra_work_id == @$view_application->type): ?>
                                    <?= ($lang == 'english') ? $exw->title_en : $exw->title_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('num_hours') ?></td>
                        <td><?= @$view_application->other_employee_id ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('permission_star') ?></td>
                        <td style="direction: ltr !important;text-align: right;"><?= @$view_application->start_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('details') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_purchase //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_purchase"): ?>


                <h4 class="text-center"><?= lang('la_purchase') ?></h4>
                <table class="table table-non-bordered">
                    <tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
                <?php $prod_name = explode(';', @$view_application->prod_name); ?>
                <?php $prod_desc = explode(';', @$view_application->prod_desc); ?>
                <?php $prod_num = explode(';', @$view_application->prod_num); ?>
                <?php $prod_note = explode(';', @$view_application->prod_note); ?>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="5%"></th>
                        <th width="27%" class="text-center"><?= lang('prod_name') ?></th>
                        <th width="30%" class="text-center"><?= lang('prod_desc') ?></th>
                        <th width="10%" class="text-center"><?= lang('prod_num') ?></th>
                        <th width="28%" class="text-center"><?= lang('prod_note') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < 5; $i++): ?>
                        <?php if (!empty($prod_name[$i])): ?>
                            <tr>
                                <td class="text-center"><?= $i + 1 ?></td>
                                <td><?= $prod_name[$i] ?></td>
                                <td><?= $prod_desc[$i] ?></td>
                                <td class="text-center"><?= $prod_num[$i] ?></td>
                                <td><?= $prod_note[$i] ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endfor; ?>
                    </tbody>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_maintenance //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_maintenance"): ?>
                <h4 class="text-center"><?= lang('la_maintenance') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('app_title') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('msg_body') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_annual_assessment //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_annual_assessment"): ?>
                <h4 class="text-center"><?= lang('la_annual_assessment') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('app_name') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_permission //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_permission"): ?>
                <h4 class="text-center"><?= lang('la_permission') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('permission_type') ?></td>
                        <td><?= lang('permission_opt' . @$view_application->type) ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('permission_star') ?></td>
                        <td><?= @$view_application->start_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('permission_end') ?></td>
                        <td><?= @$view_application->end_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_def_salary //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_def_salary"): ?>
                <h4 class="text-center"><?= lang('la_def_salary') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('app_title') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_filter_dues //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_filter_dues"): ?>
                <h4 class="text-center"><?= lang('la_filter_dues') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('allowances') ?></td>
                        <td>
                            <?php foreach (@$allowances_list as $al): ?>
                                <?php if ($al->allowance_id == @$view_application->type): ?>
                                    <?= ($lang == 'english') ? $al->allowance_title_en : $al->allowance_title_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_resignation //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_resignation"): ?>
                <h4 class="text-center"><?= lang('la_resignation') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('reason') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>


            <!--  /////////////////  la_job_application //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_job_application"): ?>
                <h4 class="text-center"><?= lang('la_job_application') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('full_name_ar') ?></td>
                        <td><?= @$view_application->full_name_ar ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('full_name_en') ?></td>
                        <td><?= @$view_application->full_name_en ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('identity_no') ?></td>
                        <td><?= @$view_application->id_no ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('identity_end') ?></td>
                        <td><?= @$view_application->id_no_end ?></td>
                    </tr>
                    <tr>
                        <?php $countries = $this->db->select('*')->from('countries')->get()->result(); ?>
                        <td width="30%"><?= lang('nationality') ?></td>
                        <td>
                            <?php foreach ($countries as $country): ?>
                                <?php if ($country->idCountry == @$view_application->nationality): ?>
                                    <?= ($lang == 'english') ? $country->countryName : $country->countryName_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('gender') ?></td>
                        <td><?= (@$view_application->sex == "male") ? lang('male') : lang('female'); ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('date_of_birth') ?></td>
                        <td><?= @$view_application->birth_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('phone') ?></td>
                        <td><?= @$view_application->phone ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('maratial_status') ?></td>
                        <td><?php
                            if (@$view_application->maratial_status == "married") echo lang('married');
                            elseif (@$view_application->maratial_status == "un_married") echo lang('un-married');
                            elseif (@$view_application->maratial_status == "widowed") echo lang('widowed');
                            elseif (@$view_application->maratial_status == "divorced") echo lang('married');
                            ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('job_time') ?></td>
                        <td><?php
                            if (@$view_application->job_time == "full") echo lang('job_full');
                            elseif (@$view_application->job_time == "part") echo lang('job_part');
                            ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('photo') ?></td>
                        <td>
                            <?php $src = ($view_application->photo) ? base_url() . @$view_application->photo : base_url() . 'img/admin.png'; ?>
                            <img src="<?= $src ?>" style="width:130px; border:1px solid black; border-radius: 1px;">
                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_recrutement //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_recrutement"): ?>
                <h4 class="text-center"><?= lang('la_recrutement') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('course_institute') ?></td>
                        <td><?= @$view_application->course_institute ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('recrutement_task') ?></td>
                        <td><?= @$view_application->name ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('recrutement_type') ?></td>
                        <td><?= (@$view_application->type==1)?lang('internal'):lang('external'); ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('from_institute') ?></td>
                        <td><?= @$view_application->address_in_leave ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('going_date') ?></td>
                        <td><?= @$view_application->going_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('coming_date') ?></td>
                        <td><?= @$view_application->coming_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('duration') ?></td>
                        <td><?= @$view_application->leave_duration ?> <?= lang('day') ?></td>
                    </tr>

                    <tr>
                        <td width="30%"><?= lang('details') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>

            <!--  /////////////////  la_embarkation //////////////////////////////////////////////////////////////////////// -->
            <?php if (@$view_application->type_app == "la_embarkation"): ?>
                <h4 class="text-center"><?= lang('la_embarkation') ?></h4>
                <table class="table  table-non-bordered"><tr>
                        <td width="30%"><?= lang('employee_name') ?></td>
                        <td><?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $view_application->employee_id): ?>
                                    <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('coming_date_after_vacation') ?></td>
                        <td><?= @$view_application->coming_date ?></td>
                    </tr>
                    <tr>
                        <td width="30%"></td>
                        <td><?= lang('embarkation_note1') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('date') ?></td>
                        <td><?= @$view_application->coming_date_2 ?> </td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('embarkation_note2') ?></td>
                        <td><?= @$view_application->leave_duration ?> <?= lang('day') ?> / <?= lang('days') ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('notes') ?></td>
                        <td><?= @$view_application->note ?></td>
                    </tr>
                    <tr>
                        <td width="30%"><?= lang('attachment') ?></td>
                        <td><?= (@$view_application->file!=null)?'<a target="_blank" href="'.base_url().'img/uploads/'.@$view_application->file.'">'.lang("download").'</a>':lang('no-exist');  ?></td>
                    </tr>
                </table>
            <?php endif; ?>


            <!--  /////////////////  ids //////////////////////////////////////////////////////////////////////// -->
            <table class="table  table-non-bordered">
                <tr>
                    <td width="30%"><?= lang('approval') ?></td>
                    <td>
                        <?php $ids = explode(';', $view_application->ids); ?>
                        <?php $results = explode(';', $view_application->results); ?>
                        <?php $current = $view_application->current; ?>

                        <?php for ($i = 0; $i < count($ids); $i++): ?>
                            <?php foreach ($employees_list as $emp): ?>
                                <?php if ($emp->employee_id == $ids[$i]): ?>
                                    <?php
                                    $class = 'btn-primary';
                                    switch ($results[$i]) {
                                        case 0:
                                            $class = 'btn-primary';
                                            break;
                                        case 1:
                                            $class = 'btn-success';
                                            break;
                                        case 2:
                                            $class = 'btn-danger';
                                            break;
                                        default:
                                            $class = 'btn-primary';
                                            break;
                                    }
                                    ?>
                                    <?php $class .= ($i == $current) ? ' current' : ' '; ?>
                                    <span
                                        class="ids btn <?= $class ?> btn-xs margin"><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></span>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endfor; ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <ul style="list-style-type: none; font-size: 0.8em">
                            <li>
                                <button class="ids btn btn-success"></button> <?= lang('employee_accepted') ?></li>
                            <li>
                                <button class="ids btn btn-danger"></button> <?= lang('employee_refused') ?></li>
                            <li>
                                <button class="ids btn btn-primary current"></button> <?= lang('current_employee') ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>


        </div>


        <div class="table-container">
            <?php if(!empty(@$app_notes)):?>
                <div class="panel-title">
                    <h4 class="text-center"><?= lang('notes') ?></h4>
                    <table class="table table-bordered">
                        <tbody>
                        <?php foreach (@$app_notes as $an):?>
                            <tr>
                                <td>
                                    <?php foreach ($employees_list as $emp): ?>
                                        <?php if ($emp->employee_id == $an->employee_id): ?>
                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td>
                                    <?php get_action($an->action)?>
                                </td>
                                <td width="70%">
                                    <?=$an->note_reason;?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endif; ?>
        </div>

    </div>
</div>



<?php
function get_action($x){
    switch ($x) {
        case '1':
            echo '<span class="label label-success">' . lang('appro_accept') . '</span>';
            break;
        case '2':
            echo '<span class="label label-danger">' . lang('appro_refuse') . '</span>';
            break;
        case '3':
            echo '<span class="label label-warning">' . lang('appro_delegate') . '</span>';
            break;
        case '4':
            echo '<span class="label label-info">' . lang('appro_consultation') . '</span>';
            break;
        case '5':
            echo '<span class="label label-info">' . lang('appro_pass') . '</span>';
            break;
        default:
            echo '?';
            break;
    }
}
?>


