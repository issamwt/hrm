<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php $lang = $this->session->userdata('lang'); ?>

<style type="text/css">
    .spinner_deps, .spinner_sects, .spinner_units, .spinner_divis{
        display: none;
    }
    .spinner_deps .fa, .spinner_sects .fa, .spinner_units .fa, .spinner_divis .fa{
        font-size: 15px;
    }

    .panel,.panel-heading, .panel-title{
        border-radius: 0px !important;
    }
    .panel-default>.panel-heading {
        color: #333;
        background-color: #f5f5f5;
        border-color: #ddd;}
    .panel-default > .panel-heading + .panel-collapse > .panel-body{
        border-top-color: #ddd;
    }
    h4.panel-title{
        text-align: center;
        background-color: transparent !important;
        padding: 0px !important;
        color: initial !important;
    }
    h4.panel-title a, h4.panel-title a:hover{
        width: 100%;
        display: block;
        text-decoration: none;
        font-size: 1em;
        font-weight: bold;
        color: initial !important;
    }
    input[type='checkbox']{
        width: initial !important;
        height: initial !important;
        display: inline;
        margin: <?= ($lang == 'arabic') ? '0px 0px 0px 10px' : '0px 10px 0px 0px'; ?> ;
    }
    .admin{
        background: #f5f5f5;
        margin-bottom: 10px;
        height: 34px;
        padding: 7px;

    }
    .myalert{
        padding: 10px;
        background: rgba(255, 165, 0, 0.38);
        color: #ff8300;
        border: 1px solid rgba(255, 165, 0, 0.35);
        border-radius: 3px;
        margin-bottom: 20px;
    }
    .myalert_button{
        background: transparent;
        border: none;
        float: left;
        color: #ff8300;
        font-weight: bold;
        cursor: pointer;
    }
    .hi{
        cursor: pointer;
    }
    .hi:hover{
        box-shadow: 0px 0px 1px 1px #00b8ff;
    }
</style>
<br><br>
<div class="row">
    <div class="pull-right col-sm-12">
        <div class="form-group" style="background: #fff; padding: 10px; float: left">
            <a href="<?php echo base_url() ?>admin/approvals/approvals_list" class="btn btn-primary"><i class="fa fa-list-alt"></i> <?= lang('approvals_list') ?></a>
        </div>
    </div>

    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="box" style="padding: 20px; border: none">
            <form role="form" id="form" action="<?php echo base_url() ?>admin/approvals/save_configure_approval/<?= $approvals_cat_info->approval_cat_id ?>" method="post" class="form-horizontal">
                <p><b><?= ($lang == 'arabic') ? $approvals_cat_info->approval_cat_ar : $approvals_cat_info->approval_cat_en; ?> :</b></p><br>
                <!-- Start Accordian -->
                <div id="accordion" class="panel-group">
                    <!---------- PANEL 1 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?= lang('approvals_scope') ?></a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <!-- Job Place -->
                                <div class="form-group">
                                    <label for="x1" class="control-label col-sm-3"><?= lang('job_place') ?></label>
                                    <div class="col-sm-5">
                                        <select name="jplc" id="x1" class="form-control"  onchange="print_deps(this.value)">
                                            <option value="0"><?= lang('all') ?></option>
                                            <?php foreach ($job_places_list as $jplc): ?>
                                                <option value="<?= $jplc->job_place_id ?>" <?= (@$approvals_cat_info->jplc == $jplc->job_place_id) ? 'selected' : ''; ?>>
                                                    <?= ($lang == 'arabic') ? $jplc->place_name_ar : $jplc->place_name_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Departement -->
                                <div class="form-group">
                                    <label for="x2" class="control-label col-sm-3"><?= lang('department') ?></label>
                                    <div class="col-sm-5">
                                        <select name="dep" id="x2" class="form-control" onchange="print_sects(this.value)">
                                            <option value="0"><?= lang('all') ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1 spinner_deps"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                </div>

                                <!-- Sections -->
                                <div class="form-group">
                                    <label for="x3" class="control-label col-sm-3"><?= lang('designation') ?></label>
                                    <div class="col-sm-5">
                                        <select name="sect" id="x3" class="form-control" onchange="print_units(this.value)">
                                            <option value="0"><?= lang('all') ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1 spinner_sects"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                </div>

                                <!-- Units -->
                                <div class="form-group">
                                    <label for="x4" class="control-label col-sm-3"><?= lang('aunit') ?></label>
                                    <div class="col-sm-5">
                                        <select name="unit" id="x4" class="form-control" onchange="print_divis(this.value)">
                                            <option value="0"><?= lang('all') ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1 spinner_units"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                </div>

                                <!-- Divisions -->
                                <div class="form-group">
                                    <label for="x5" class="control-label col-sm-3"><?= lang('adivision') ?></label>
                                    <div class="col-sm-5">
                                        <select name="divi" id="x5" class="form-control" >
                                            <option value="0"><?= lang('all') ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1 spinner_divis"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                </div>

                                <!-- Notify -->
                                <div class="form-group">
                                    <label for="x6" class="control-label col-sm-3"><?= lang('option') ?></label>
                                    <div class="col-sm-5">
                                        <input style="display: inline" type="checkbox" name="notify" class="form-control" <?= ($approvals_cat_info->notify == 1) ? 'checked' : ''; ?>>
                                        <?= lang('approval_notify_emp') ?>
                                    </div>
                                </div>

                                <!-- admin_must_participate -->
                                <div class="form-group">
                                    <label for="x7" class="control-label col-sm-3"><?= lang('option') ?></label>
                                    <div class="col-sm-5">
                                        <input type="checkbox" name="adm_participate" class="form-control" <?= ($approvals_cat_info->adm_participate == 1) ? 'checked' : ''; ?>>
                                        <?= lang('admin_must_participate') ?>
                                    </div>
                                </div>

                                <!-- can_add_replacement -->
                                <div class="form-group">
                                    <label for="x8" class="control-label col-sm-3"><?= lang('option') ?></label>
                                    <div class="col-sm-5">
                                        <input type="checkbox" name="add_replacement" class="form-control" <?= ($approvals_cat_info->add_replacement == 1) ? 'checked' : ''; ?>>
                                        <?= lang('can_add_replacement') ?>
                                    </div>
                                </div>

                                <!-- can_consult -->
                                <div class="form-group">
                                    <label for="x9" class="control-label col-sm-3"><?= lang('option') ?></label>
                                    <div class="col-sm-5">
                                        <input type="checkbox" name="can_consult" class="form-control" <?= ($approvals_cat_info->can_consult == 1) ? 'checked' : ''; ?>>
                                        <?= lang('can_consult') ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 1 ---------->
                    <!---------- PANEL 2 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?= lang('approvals_comm_adm') ?></a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><b><?= lang('choose_admin') ?> :</b></p>
                                <!-- Admins -->
                                <div class="form-group">
                                    <label for="x10" class="control-label col-sm-3"><?= lang('the_list') ?></label>
                                    <div class="col-sm-5">
                                        <select name="admins[]" class="form-control" multiple="" style="min-height: 300px;">
                                            <?php $array = explode(';', $approvals_cat_info->admins); ?>
                                            <?php foreach ($admin_list as $adm): ?>
                                                <option value="<?= $adm->user_id ?>" <?= (in_array($adm->user_id, $array)) ? 'selected' : ''; ?>>
                                                <b><?= ($lang == 'arabic') ? $adm->user_name_ar : $adm->user_name_en; ?> : </b>
                                                <?php get_admin($adm, $department_list, $designations_list, $lang); ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 2 ---------->
                    <!---------- PANEL 3 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?= lang('approvals_comm_emp') ?></a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><b><?= lang('choose_employees') ?> :</b></p>
                                <!-- Employees -->
                                <div class="form-group">
                                    <label for="x10" class="control-label col-sm-3"><?= lang('the_list') ?></label>
                                    <div class="col-sm-5">
                                        <select name="employees[]" class="form-control" multiple="" style="min-height: 300px;">
                                            <?php $array = explode(';', $approvals_cat_info->employees); ?>
                                            <?php foreach ($employee_list as $emp): ?>
                                                <option value="<?= $emp->employee_id ?>" <?= (in_array($emp->employee_id, $array)) ? 'selected' : ''; ?>>
                                                    - <?= $emp->employment_id ?> : <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 3 ---------->
                    <!---------- PANEL 4 ---------->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFoor"><?= lang('approvals_comm_squence') ?></a>
                            </h4>
                        </div>
                        <div id="collapseFoor" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="myalert">
                                    <?= lang('approvals_comm_squence_note') ?>
                                    <button type="button" class="myalert_button" onclick="$(this).parent().fadeOut()">&times;</button>
                                </div>
                                <table class="table table-bordered checkbox_table">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?= lang('order') ?></th>
                                            <th class="text-center"><?= lang('comm_members') ?></th>
                                            <th class="text-center"><?= lang('member_type') ?></th>
                                            <th class="text-center"><?= lang('appro_accept') ?></th>
                                            <th class="text-center"><?= lang('appro_refuse') ?></th>
                                            <th class="text-center"><?= lang('appro_view') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $order = 1; ?>
                                        <!--admins-->
                                        <?php $array = explode(';', $approvals_cat_info->admins); ?>
                                        <?php $accept = explode(';', $approvals_cat_info->accept); ?><?php $i = 0; ?>
                                        <?php $refuse = explode(';', $approvals_cat_info->refuse); ?>
                                        <?php $view = explode(';', $approvals_cat_info->view); ?>

                                        <?php foreach ($array as $adm_id): ?>
                                            <tr>
                                                <td class="text-center"><?= $order++; ?></td>
                                                <td>
                                                    <?php foreach ($admin_list as $adm): ?>
                                                        <?= ($adm->user_id == $adm_id) ? ($lang == 'arabic') ? $adm->user_name_ar : $adm->user_name_en : ''; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td>
                                                    <?php foreach ($admin_list as $adm): ?>
                                                        <?php if ($adm->user_id == $adm_id): ?>
                                                            <?php get_admin($adm, $department_list, $designations_list, $lang); ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="hi" <?= ($accept[$i] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="accept[]" value="<?= $accept[$i] ?>">
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="hi" <?= ($refuse[$i] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="refuse[]" value="<?= $refuse[$i] ?>">
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="hi"<?= ($view[$i] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="view[]" value="<?= $view[$i] ?>">
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                        <!--employees-->
                                        <?php $array = explode(';', $approvals_cat_info->employees); ?>
                                        <?php $accept_emp = explode(';', $approvals_cat_info->accept_emp); ?><?php $j = 0; ?>
                                        <?php $refuse_emp = explode(';', $approvals_cat_info->refuse_emp); ?>
                                        <?php $view_emp = explode(';', $approvals_cat_info->view_emp); ?>

                                        <?php foreach ($array as $emp_id): ?>
                                            <tr>
                                                <td class="text-center"><?= $order++; ?></td>
                                                <td>
                                                    <?php foreach ($employee_list as $emp): ?>
                                                        <?= ($emp->employee_id == $emp_id) ? ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en : ''; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td>
                                                    <?= lang('employee') ?>
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="hi" <?= ($accept_emp[$j] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="accept_emp[]" value="<?= $accept_emp[$j] ?>">
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox" class="hi" <?= ($refuse_emp[$j] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="refuse_emp[]" value="<?= $refuse_emp[$j] ?>">
                                                </td>
                                                <td class="text-center">
                                                    <input type="checkbox"  class="hi"<?= ($view_emp[$j] == 1) ? 'checked' : ''; ?>>
                                                    <input type="hidden" class="hidden" name="view_emp[]" value="<?= $view_emp[$j] ?>">
                                                </td>
                                            </tr>
                                            <?php $j++; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!---------- PANEL 4 ---------->
                </div>
                <!-- Ends Accordian -->

        </div>

        <br>
        <!-- Submit Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-block btn-primary" style="border-radius: 0px;"><?= lang('save') ?></button>
            </div>
        </div>

        </form>
    </div>
</div>
</div>

<?php

function get_admin($user, $department_list, $designations_list, $lang) {
    switch ($user->owner) {
        case 1:echo lang('super_admin') . '<br>';
            break;
        case 2:echo lang('hr_admin') . '<br>';
            break;
        case 3:echo lang('compt_admin') . '<br>';
            break;
        case 4:echo lang('dep_admin');
            foreach ($department_list as $dep) {
                if ($dep->department_id == $user->depsec_id) {
                    echo ' : ';
                    echo ($lang == 'arabic') ? $dep->department_name_ar : $dep->department_name;
                }
            }
            echo '<br>';
            break;
        case 5:echo lang('section_admin');
            foreach ($designations_list as $des) {
                if ($des->designations_id == $user->depsec_id) {
                    echo ' : ';
                    echo ($lang == 'arabic') ? $des->designations_ar : $des->designations;
                }
            }
            echo '<br>';
            break;
        case 6:echo lang('guest_admin');
            foreach ($designations_list as $des) {
                if ($des->designations_id == $user->depsec_id) {
                    echo ' : ';
                    echo ($lang == 'arabic') ? $des->designations_ar : $des->designations;
                }
            }
            echo '<br>';
            break;
        default:echo '';
            break;
    }
}
?>
<script type="text/javascript">
    var lang = "<?= $this->session->userdata('lang'); ?>";
    ////////////////////////////////////////////////////DEPARTMENT////////////////////////////////////////////////////
    ////////////////////////////////////////////////////DEPARTMENT////////////////////////////////////////////////////
    ////////////////////////////////////////////////////DEPARTMENT////////////////////////////////////////////////////
    function print_deps(id) {
        var vurl = "<?php echo base_url() ?>admin/approvals/get_dep_with_jobplace/" + id
        if (id)
        {
            $('.spinner_deps').fadeIn('fast');
            $.ajax({url: vurl,
                success: function (data) {
                    reset_dep();
                    reset_sect();
                    reset_unit();
                    reset_divi();
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x2').append('<option value="' + data[i].department_id + '" >' + data[i].department_name_ar + '</option>');
                        else
                            $('#x2').append('<option value="' + data[i].department_id + '" >' + data[i].department_name + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_deps').fadeOut('fast');
                $('#x2').find('option').map(function () {
                    if ($(this).val() ==<?= @$approvals_cat_info->dep ?>)
                        $(this).prop("selected", true);
                });
            });

        } else {
            reset_dep();
            reset_sect();
            reset_unit();
            reset_divi();
        }
    }
    ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
    ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
    ////////////////////////////////////////////////////SECTION////////////////////////////////////////////////////
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>admin/approvals/get_sec_with_dep/" + id
        if (id)
        {
            $('.spinner_sects').fadeIn('fast');
            $.ajax({url: vurl,
                success: function (data) {
                    reset_sect();
                    reset_unit();
                    reset_divi();
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
                $('#x3').find('option').map(function () {
                    if ($(this).val() ==<?= @$approvals_cat_info->sect ?>)
                        $(this).prop("selected", true);
                });
            });
        } else {
            reset_sect();
            reset_unit();
            reset_divi();
        }
    }
    ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
    ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
    ////////////////////////////////////////////////////UNITS////////////////////////////////////////////////////
    function print_units(id) {
        var vurl = "<?php echo base_url() ?>admin/approvals/get_unit_with_sec/" + id
        if (id)
        {
            $('.spinner_units').fadeIn('fast');
            $.ajax({url: vurl,
                success: function (data) {
                    reset_unit();
                    reset_divi();
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x4').append('<option value="' + data[i].unit_id + '">' + data[i].unit_ar + '</option>');
                        else
                            $('#x4').append('<option value="' + data[i].unit_id + '">' + data[i].unit_en + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_units').fadeOut('fast');
                $('#x4').find('option').map(function () {
                    if ($(this).val() ==<?= @$approvals_cat_info->unit ?>)
                        $(this).prop("selected", true);
                });
            });
        } else {
            reset_unit();
            reset_divi();
        }
    }
    ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
    ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
    ////////////////////////////////////////////////////DIVISION////////////////////////////////////////////////////
    function print_divis(id) {
        var vurl = "<?php echo base_url() ?>admin/approvals/get_division_with_unit/" + id;
        if (id)
        {
            $('.spinner_divis').fadeIn('fast');
            $.ajax({url: vurl,
                success: function (data) {
                    reset_divi();
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x5').append('<option value="' + data[i].division_id + '">' + data[i].division_ar + '</option>');
                        else
                            $('#x5').append('<option value="' + data[i].division_id + '">' + data[i].division_en + '</option>');
                    }
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_divis').fadeOut('fast');
                $('#x5').find('option').map(function () {
                    if ($(this).val() ==<?= @$approvals_cat_info->divi ?>)
                        $(this).prop("selected", true);
                });
            });
        } else {
            reset_divi();
        }
    }
    ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
    ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
    ////////////////////////////////////////////////////RESET ALL////////////////////////////////////////////////////
    function reset_dep() {
        $('#x2').empty();
        $('#x2').append('<option value="0"><?= lang('all') ?></option>');
    }
    function reset_sect() {
        $('#x3').empty();
        $('#x3').append('<option value="0"><?= lang('all') ?></option>');
    }
    function reset_unit() {
        $('#x4').empty();
        $('#x4').append('<option value="0"><?= lang('all') ?></option>');
    }
    function reset_divi() {
        $('#x5').empty();
        $('#x5').append('<option value="0"><?= lang('all') ?></option>');
    }
    ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
    ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
    ////////////////////////////////////////////////////POPULATE VARIABLES////////////////////////////////////////////////////
<?php if (!empty(@$approvals_cat_info)): ?>
        $(function () {
            $.ajax({
                url: print_deps(<?= @$approvals_cat_info->jplc ?>),
                success: function () {
                    $.ajax({
                        url: print_sects(<?= @$approvals_cat_info->dep ?>),
                        success: function () {
                            $.ajax({
                                url: print_units(<?= @$approvals_cat_info->sect ?>),
                                success: function () {
                                    print_divis(<?= @$approvals_cat_info->unit ?>);
                                }
                            });
                        }
                    });
                }
            });
        });
<?php endif; ?>
    $(function () {
        $('.checkbox_table').find(':checkbox.hi').map(function () {
            $(this).change(function () {
                if (this.checked)
                    $(this).next('.hidden').val(1);
                else
                    $(this).next('.hidden').val(0);
            });
        });
    });
</script>