<?php echo message_box('success'); ?>

<style type="text/css">
    .social_header{
        background: lightgray;
        width: 100%;
        font-weight: bold;
        text-align: left;
        padding:4px;
        margin-bottom: 20px;
    }
    .form{
        font-size: 1.1em;
        font-weight: bold;
    }
    .btn-default {
        background-color: #f4f4f4 !important;
    }
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#finances_info" data-toggle="tab"><?= lang('finance_employee') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#finances_option"  data-toggle="tab"><?= lang('finance_option') ?></a></li>
				<li class="<?= $active == 3 ? 'active' : '' ?>"><a href="#finances_deduction"  data-toggle="tab"><?= lang('finance_permanent_deduction') ?></a></li>
				<li class="<?= $active == 4 ? 'active' : '' ?>"><a href="#finances_other"  data-toggle="tab"><?= lang('finance_provision') ?></a></li>
				<li class="<?= $active == 5 ? 'active' : '' ?>"><a href="#finances_houcing"  data-toggle="tab"><?= lang('finance_houcing') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="finances_info" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
						    <div class="form-group col-sm-3">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('reference') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->reference; ?>" disabled/>
                                    </div>
                                </div>
						   <div class="form-group col-sm-3">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->full_name_en; ?>" disabled/>
                                    </div>
                                </div>
                            <div class="form-group col-sm-3">
                                    <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('finance_basic_salary') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="salary" value="<?= $employee_info->employee_salary; ?>" disabled />
                                    </div>
                                </div>
                            <div class="form-group col-sm-3">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('finance_all_salary') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="" disabled/>
                                    </div>
                                </div>
								 <table class="table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center"><?= lang('finance_bonus')?></th>
                                        <th width="20%" class="text-center"><?= lang('finance_value')?></th>
                                        <th width="11%" class="text-center"><?= lang('finance_from')?></th>
										<th width="11%" class="text-center"><?= lang('finance_to')?></th>
										<th width="8%" class="text-center"><?= lang('finance_reservation')?></th>
										<th width="8%" class="text-center"><?= lang('action')?></th>
										
                                    </tr>
                                </thead>
                                <tbody><?php if (!empty($allowances_employee)){ ?> <?php foreach($allowances_employee as $allows){ ?>
                                            <tr>
                                                <td>
												<?= ($lang == 'arabic') ? $allows->allowance_title_ar : $allows->allowance_title_en; ?> 
												</td>
			
												<td>
												<?php if($allows->allowance_id == 1){echo "(".$allows->allowance_value."&nbsp;%)&nbsp;".($allows->allowance_value * $employee_info->employee_salary)/100 ." <strong>".lang('device')."</strong>";}else { echo $allows->allowance_value." <strong>". lang('device')."</strong>";} ?>
												</td>
												
												<td> 
												<?= $allows->allowance_date_from; ?>												
													</td>
												<td>  
                                                    <?= $allows->allowance_date_to; ?>	
													</td>
													<td><?= $allows->reservation; ?>	</td>
													<td> 
													<a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/employee/delete_allowance/<?php echo $allows->allowance_type_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
</td>
                                            </tr>
								<?php }}else{ ?>
								<td colspan="3">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
								<?php } ?>
											 
											</tbody>
                            </table>
							<br>
                <br>
                <br>
                <br>
                <h4><?= ($active == 1) ? lang('finance_add_allowance') : lang('finance_add_allowance'); ?></h4>
                <br>
                <br>
							<form role="form" id="employee-form"  action="<?php echo base_url() ?>admin/employee/save_allowance" method="post" class="form-horizontal form-groups-bordered">
   <input type="hidden" name="employee_id"  value="<?= $this->uri->segment(4) ?>"  class="form-control"  />
								 <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_type') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
									<select class="form-control" name="allowance_id" id="allowance_type">
												<option><?= lang('finance_select') ?></option>
												<?php foreach($allowances_info as $allow=>$key){ ?>
												<option value="<?php echo $key->allowance_id;?>"><?= ($lang=='arabic')? $key->allowance_title_ar:$key->allowance_title_en; ?></option>
												<?php } ?>
												</select>
												</div>
                                </div>
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_value') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="allowance_value" id="allow_total_value"  class="form-control" disabled />
                                    </div>
                                </div>
								<div class="form-group">
                                                        <label class="control-label col-sm-3" ><?= lang('finance_from') ?> <span class="required"> *</span></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="allowane_date_from" class="form-control hijri_datepicker" data-format="yyy-mm-dd">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="form-group">
                                                        <label class="control-label col-sm-3" ><?= lang('finance_to') ?> <span class="required"> *</span></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="allowane_date_to"  class="form-control hijri_datepicker" data-format="yyy-mm-dd">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
													
													 <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="finances_option" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
							<form class="form-horizontal form-groups-bordered" action="<?php echo base_url() ?>admin/employee/save_finance_info/<?php
                            if (!empty($employee_info->finance_info_id)) {
                                echo $employee_info->finance_info_id;
                            }
                            ?>" method="post">
                           <input type="hidden" name="employee_id"  value="<?= $this->uri->segment(4) ?>"  class="form-control"  />
						
							 <fieldset>
							 <legend><?= lang('fieldset_bank') ?></legend>
							<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('payment_method'); ?></label>
                                    <div class="col-sm-5">
									<select class="form-control" name="payment_method">
									<option value="1"><?= ($lang=='arabic')? 'اختر طريقة دفع':'Select a payment method';?></option>
										<option value="1" <?php if (!empty($employee_info)) echo $employee_info->payment_method == 1 ? 'selected' : '' ?>><?= ($lang=='arabic')? 'بنك':'Bank';?></option>
										<option value="2" <?php if (!empty($employee_info)) echo $employee_info->payment_method == 2 ? 'selected' : '' ?>><?= ($lang=='arabic')? 'شيكات':'Checks';?></option>
										<option value="3" <?php if (!empty($employee_info)) echo $employee_info->payment_method == 3 ? 'selected' : '' ?>><?= ($lang=='arabic')? 'نقدا':'Cash money';?></option>
									</select>
                                    </div>
                                </div>
							<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('payment_account'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= $employee_info->account_number?>"  class="form-control" />
                                    </div>
                                </div>
								<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('payment_bank'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= $employee_info->bank_name?>"  class="form-control" />
                                    </div>
                                </div>
								<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('payment_branch'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= $employee_info->branch_name?>"  class="form-control" />
                                    </div>
                                </div>
								</fieldset>
								<fieldset>
							 <legend><?= lang('fieldset_med') ?></legend>
							 
							 <div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_yes'); ?></label>
                                    <div class="col-sm-5">
									<?php
                            if ($employee_info->med_insur == 1) {  ?>
                                        <input type="checkbox" name="deduction_value" id="deduction_total_value" checked />
							<?php }else { ?>
							<input type="checkbox" name="deduction_value" id="deduction_total_value"  />
							<?php } ?>
                                    </div>
                                </div>
								<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_type'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= ($employee_info->med_insur_type == 1) ? lang('medical_insur_type_all') : lang('medical_insur_type_part'); ?> "  class="form-control" />
                                    </div>
                                </div>
								
									<div class="form-group col-sm-6">
                                                        <label class="control-label col-sm-3" ><?= lang('medical_date'); ?> <span class="required"> *</span></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="med_insur_start_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->med_insur_start_date)) {
                                echo $employee_info->med_insur_start_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
								<div class="form-group col-sm-6">
                                                        <label class="control-label col-sm-3" ><?= lang('medical_end_date'); ?> <span class="required"> *</span></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="med_insur_end_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->med_insur_end_date)) {
                                echo $employee_info->med_insur_end_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
							 </fieldset>
							 <fieldset>
							 <legend><?= lang('fieldset_social') ?></legend>
							 <div class="form-group col-sm-3">
                                    <label for="field-1" class="col-sm-4 control-label"><?= lang('social_yes'); ?></label>
                                    <div class="col-sm-5">
									<?php
                            if ($employee_info->social_insurance == 1) {  ?>
                                        <input type="checkbox" name="deduction_value" id="deduction_total_value" checked />
							<?php }else { ?>
							<input type="checkbox" name="deduction_value" id="deduction_total_value"  />
							<?php } ?>
                                    </div>
                                </div>
							 <div class="form-group col-sm-5">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('social_type'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= ($employee_info->social_insurance_type == 1) ? lang('saudi_social_insur') : lang('no_saudi_social_insur'); ?> "  class="form-control" />
                                    </div>
                                </div>
								<div class="form-group col-sm-4">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('social_salary'); ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value" value="<?= $employee_info->employee_salary?>"  class="form-control" />
                                    </div>
                                </div>
									<div class="form-group col-sm-8">
                                                        <label class="control-label col-sm-3" ><?= lang('social_date'); ?> </label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="soc_insur_start_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->soc_insur_start_date)) {
                                echo $employee_info->soc_insur_start_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
								
							 </fieldset>
							 <fieldset>
							 <legend><?= lang('fieldset_other') ?></legend>
							 <div class="form-group col-sm-6">                               
                                    <div class="col-sm-1">
<?php if ($employee_info->service_benefits_yes == 1) {
                                
								?>										
										<input type="checkbox" name="service_benefits_yes" id="deduction_total_value" value="1" checked/>
<?php } else { ?>
<input type="checkbox" name="service_benefits_yes" id="deduction_total_value" value="1"/>
<?php } ?>   
                                    </div>
									 <label for="field-1" class="col-sm-4 control-label"><?= lang('option_end_service')?></label>
                                </div>
								
								
								<div class="form-group col-sm-6">
                                                        <label class="control-label col-sm-3" ><?= lang('option_date')?> </label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="service_benefits_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->service_benefits_date)) {
                                echo $employee_info->service_benefits_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
								</div>
								<div class="form-group col-sm-6">
								<div class="col-sm-1">
							<?php
                            if ($employee_info->service_fixed == 1) {
                                
								?>								
										<input type="checkbox" name="service_fixed" id="deduction_total_value" value="1" checked/>
							<?php }else { ?>
							<input type="checkbox" name="service_fixed" id="deduction_total_value" value="1" />
							<?php } ?>
								   </div>
                                    <label for="field-1" class="col-sm-4 control-label"><?= lang('option_fix')?></label>
                                    
                                </div>
								<div class="form-group col-sm-6">
                                                        <label class="control-label col-sm-3" ><?= lang('option_date')?> </label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="service_fix_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->service_fix_date)) {
                                echo $employee_info->service_fix_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
								</div>
								<div class="form-group col-sm-2">
								<div class="col-sm-1">	
<?php
                            if ($employee_info->service_suspend == 1) {
                                
								?>									
										<input type="checkbox" name="service_suspend" id="deduction_total_value"  value="1" checked/>
							<?php } else { ?>
							<input type="checkbox" name="service_suspend" id="deduction_total_value"  value="1" />
							<?php } ?>
                                    </div>
                                    <label for="field-1" class="col-sm-4 control-label"><?= lang('option_suspend')?></label>
                                    
                                </div>
								<div class="form-group col-sm-5">
                                                        <label class="control-label col-sm-3" ><?= lang('option_date')?> </label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="stopping_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->stopping_date)) {
                                echo $employee_info->stopping_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
								</div>
								<div class="form-group col-sm-5">
                                                        <label class="control-label col-sm-3" ><?= lang('option_stop_salary')?></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="stopping_salary_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->stopping_salary_date)) {
                                echo $employee_info->stopping_salary_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
								</div>
								<div class="form-group col-sm-8">
                                                        <label class="control-label col-sm-3" ><?= lang('retrait_date')?> </label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="retrait_date"  class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->retrait_date)) {
                                echo $employee_info->retrait_date;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
								</div>
								<div class="form-group col-sm-6">
								<label for="field-1" class="col-sm-3 control-label"><?= lang('option_accounting_1')?></label>
								<div class="col-sm-5">		
										<input type="text" name="figure_account_1" id="deduction_total_value" class="form-control" value="<?php
                            if (!empty($employee_info->figure_account_1)) {
                                echo $employee_info->figure_account_1;
                            }
                            ?>"/>
                                    </div>
                                    
                                    
                                </div>
								<div class="form-group col-sm-6">
								<label for="field-1" class="col-sm-3 control-label"><?= lang('option_accounting_2')?></label>
								<div class="col-sm-5">		
										<input type="text" name="figure_account_2" id="figure_account_2" class="form-control" value="<?php
                            if (!empty($employee_info->figure_account_2)) {
                                echo $employee_info->figure_account_2;
                            }
                            ?>" />
                                    </div>
                                    
                                    
                                </div>
								
								
								<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('option_description_ar')?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="option_desc_ar" class="form-control" rows="10"><?php
                            if (!empty($employee_info->option_desc_ar)) {
                                echo $employee_info->option_desc_ar;
                            }
                            ?></textarea>
                                    </div>
                                </div>	
								<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('option_description_en')?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="option_desc_en" class="form-control" rows="10"><?php
                            if (!empty($employee_info->option_desc_en)) {
                                echo $employee_info->option_desc_en;
                            }
                            ?></textarea>
                                    </div>
                                </div>	
							 </fieldset>
							 <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-7">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
								</form>
							
							
                        </div>
                    </div>
                </div>
				 <div class="tab-pane <?= $active == 3 ? 'active' : '' ?>" id="finances_deduction" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <div class="box-body">
							<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('reference') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->reference; ?>" disabled/>
                                    </div>
                                </div>
						   <div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->full_name_en; ?>" disabled/>
                                    </div>
                                </div>
						<table class="table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center"><?= lang('finance_deduction_bonus')?></th>
                                        <th width="20%" class="text-center"><?= lang('finance_deduction_value')?></th>
										<th width="8%" class="text-center"><?= lang('finance_deduction_description')?></th>
										<th width="8%" class="text-center"><?= lang('action')?></th>
										
                                    </tr>
                                </thead>
                                <tbody><?php if(!empty($deductions_employee)){ ?><?php foreach($deductions_employee as $deds){ ?>
                                            <tr>
                                                <td>
												<?= ($lang == 'arabic') ? $deds->deduction_title_ar : $deds->deduction_title_en; ?> 
												</td>
												<td>
												<?php if($deds->deduction_type == "percent"){echo "(".$deds->deduction_value."&nbsp;%)&nbsp;".($deds->deduction_value * $employee_info->employee_salary)/100 ." <strong>".lang('device')."</strong>";}else { echo $deds->deduction_value." <strong>". lang('device')."</strong>";} ?>

												</td>
												
												
												<td>  
                                                   <?= ($lang == 'arabic') ? $deds->deduction_description_ar : $deds->deduction_description_en; ?> 	
													</td>
													
													<td> 
													<a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/employee/delete_deduction/<?php echo $deds->deduction_type_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
</td>
                                            </tr>
								<?php }}else{ ?>
								<td colspan="3">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
								<?php } ?>
											 
											</tbody>
                            </table>
                           <br>
                <br>
                <br>
                <h4><?= ($active == 1) ? lang('finance_add_deduction') : lang('finance_add_deduction'); ?></h4>
                <br>
                <br>
							<form role="form" id="employee-form"  action="<?php echo base_url() ?>admin/employee/save_deduction" method="post" class="form-horizontal form-groups-bordered">
   <input type="hidden" name="employee_id"  value="<?= $this->uri->segment(4) ?>"  class="form-control"  />
								 <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_deduction_type') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
									<select class="form-control" name="deduction_id" id="deduction_type">
												<option><?= lang('finance_deduction_type') ?></option>
												<?php foreach($deductions_info as $allow=>$key){ ?>
												<option value="<?php echo $key->deduction_id;?>"><?= ($lang = 'arabic') ? $key->deduction_title_ar : $key->deduction_title_en; ?> </option>
												<?php } ?>
												</select>
												</div>
                                </div>
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_deduction_value') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="deduction_value" id="deduction_total_value"  class="form-control" />
                                    </div>
                                </div>
								
								  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_deduction_description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="desc_ar" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
									  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_deduction_description_en') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="desc_en" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>				
													 <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
				</div>
				 <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="finances_other" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
						<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('reference') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->reference; ?>" disabled/>
                                    </div>
                                </div>
						   <div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->full_name_en; ?>" disabled/>
                                    </div>
                                </div>
						<table class="table table-bordered table-hover">

                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center"><?= lang('finance_provision_bonus')?></th>
                                        <th width="20%" class="text-center"><?= lang('finance_provision_value')?></th>
										<th width="8%" class="text-center"><?= lang('finance_provision_description')?></th>
										<th width="8%" class="text-center"><?= lang('action')?></th>
										
                                    </tr>
                                </thead>
                                <tbody><?php if(!empty($provisions_employee)){ ?><?php foreach($provisions_employee as $provs){ ?>
                                            <tr>
                                                <td>
												<?= ($lang == 'english') ? $provs->provision_title_en : $provs->provision_title_ar; ?> 
												</td>
												<td>
												<?= $provs->provision_value.'&nbsp;<strong>'.lang('device').'</strong>'; ?>
												</td>
												
												
												<td>  
                                                   <?= ($lang == 'english') ? $provs->provision_description_en : $provs->provision_description_ar ?> 	
													</td>
													
													<td> 
													<a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/employee/delete_provision/<?php echo $provs->provision_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
</td>
                                            </tr>
								<?php }}else{ ?>
								<td colspan="3">
                                        <strong><?= lang('nothing_to_display') ?></strong>
                                    </td>
								<?php } ?>
											 
											</tbody>
                            </table>
                           <br>
                <br>
                <br>
                <h4><?= ($active == 1) ? lang('finance_add_provision') : lang('finance_add_provision'); ?></h4>
                <br>
                <br>
							<form role="form" id="employee-form"  action="<?php echo base_url() ?>admin/employee/save_provision" method="post" class="form-horizontal form-groups-bordered">
   <input type="hidden" name="employee_id"  value="<?= $employee_info->employee_id?>"  class="form-control"  />
								 <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_provision_type') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
									<select class="form-control" name="provision_category_id" id="allowance_type">
												<option><?= lang('finance_provision_type') ?></option>
												<?php foreach($provisions_info as $allow=>$key){ ?>
												<option value="<?php echo $key->provision_category_id;?>"><?= ($lang=='arabic')? $key->provision_title_ar : $key->provision_title_en; ?></option>
												<?php } ?>
												</select>
												</div>
                                </div>
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_provision_value') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="provision_value" id="allow_total_value"  class="form-control" />
                                    </div>
                                </div>
								
								  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_provision_description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="desc_ar" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
									  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_provision_description_en') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="desc_en" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>				
													 <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
				<div class="tab-pane <?= $active == 5 ? 'active' : '' ?>" id="finances_houcing" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
						<div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('reference') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->reference; ?>" disabled/>
                                    </div>
                                </div>
								
						   <div class="form-group col-sm-6">
                                    <label for="field-1" class="col-sm-3 control-label"  style="color:red;"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="total_salary" value="<?= $employee_info->full_name_en; ?>" disabled/>
                                    </div>
                                </div>
								<br>
                           <form role="form" id="employee-form"  action="<?php echo base_url() ?>admin/employee/save_houcing/ <?php
                            if (!empty($employee_info->houcing_id)) {
                                echo $employee_info->houcing_id;
                            }
                            ?>" method="post" class="form-horizontal form-groups-bordered">
   <input type="hidden" name="employee_id"  value="<?= $this->uri->segment(4);?>"  class="form-control"  />
								 
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_status') ?></label>
                                    <div class="col-sm-5">
									<?php
                            if ($employee_info->houcing_status == 1) {
        
                            ?>
                                        <input type="checkbox" name="houcing_status" id="allow_total_value" value="1" checked/>
							<?php } else { ?>
							  <input type="checkbox" name="houcing_status" id="allow_total_value" value="1" />
							<?php } ?>
                                    </div>
                                </div>
								
								  <div class="form-group">
                                                        <label class="control-label col-sm-3" ><?= lang('start_date') ?> <span class="required"> *</span></label>
                                                        <div class="input-group col-sm-5">
                                                            <input type="text" name="start_day" class="form-control hijri_datepicker" data-format="yyy-mm-dd" value="<?php
                            if (!empty($employee_info->start_day)) {
                                echo $employee_info->start_day;
                            }
                            ?>">
                                                            <div class="input-group-addon">
                                                                <a href="#"><i class="entypo-calendar"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>	
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_count') ?></label>
                                    <div class="col-sm-5">
									<?php
                            if ($employee_info->houcing_type == 'value') {
        
                            ?> <div class="col-sm-6"> <input type="radio" name="provision_value" id="count" value="1" checked /> <?= lang('finance_fixed') ?></div>
							<?php } else { ?>
                                       <div class="col-sm-6"> <input type="radio" name="provision_value" id="count" value="1" /> <?= lang('finance_fixed') ?></div>
							 <?php } ?>
							<?php
                            if ($employee_info->houcing_type == 'percent') {
        
                            ?> 
									   <div class="col-sm-6"> <input type="radio" name="provision_value" id="count"  value="2" checked /> <?= lang('finance_percent') ?></div>
									   <?php } else { ?>
									   <div class="col-sm-6"> <input type="radio" name="provision_value" id="count"  value="2" /> <?= lang('finance_percent') ?></div>
									   <?php } ?>
                                    </div>
                                </div>
								<input type="hidden" name="houcing_type" id="houcing_type" value="<?php
                            if (!empty($employee_info->houcing_type)) {
                                echo $employee_info->houcing_type;
                            }
                            ?>">
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_value') ?></label>
                                    <div class="col-sm-5">
                                       <div class="col-sm-10"> <input type="text" name="houcing_value" id="deduction_total_value"  class="form-control" value="<?php
                            if (!empty($employee_info->houcing_value)) {
                                echo $employee_info->houcing_value;
                            }
                            ?>"/></div> <div class="col-sm-2"><span id="percent"><strong>%</strong></span><span id="fixed"><strong><?= lang('device') ?></strong></span></div>
                                    </div>
                                </div>	

								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('finance_repetition') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
									<div class="col-sm-8">
									<select class="form-control" name="houcing_repetition" id="">
												<option><?= lang('month') ?></option>
													<option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option91</option><option>10</option><option>11</option><option>12</option>
												</select>
												</div>
												<div class="col-sm-3"><?= lang('month') ?></div>
												</div>
                                </div>
								
													 <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>
</div>
<script>
$('#allowance_details_percent').hide();
$('#allowance_details_value').hide();
$('#allowance_type').on('change',function(){
	//alert($('#allowance_type').val());
	var type = $('#allowance_type').val();
	var salary = $('#salary').val();	
		$.ajax({
			type : 'POST',
			url : 'http://smartlives.top/hrm_lite/admin/employee/getAllowancesByType/'+type,
			success : function(data){
				var datas = JSON.parse(data);
				for(var i in datas){
					if (datas[i]['allowance_type']=='percent'){
					 $('#allow_total_value').val((salary * datas[i]['allowance_value'])/100);
					 $('#origin_input').hide();
				}else{
					 $('#allow_total_value').val(datas[i]['allowance_value']);

				}
				}
				
			}
		})
	
});
$('#deduction_type').on('change',function(){
	//alert($('#allowance_type').val());
	var type = $('#deduction_type').val();
	var sal =$('#salary').val();	
	//alert(type)
		$.ajax({
			type : 'POST',
			url : 'http://smartlives.top/hrm_lite/admin/employee/getDeductionsByType/'+type,
			success : function(data){
				//alert(data)
				var datas = JSON.parse(data);
				for(var i in datas){
					//alert(datas[i]['deduction_type'])
					if (datas[i]['deduction_type']=='percent'){
					 $('#deduction_total_value').val((sal * datas[i]['deduction_value'])/100);
				}else{
					 $('#deduction_total_value').val(datas[i]['deduction_value']);

				}
				}
				
			}
		})
	
})
$('#percent').hide();
$('#fixed').hide();
$("input[type='radio']") // select the radio by its id
    .change(function(){ // bind a function to the change event
        if( $("input[type='radio']:checked") ){ // check if the radio is checked
            var val = $(this).val(); // retrieve the value
			if(val==1){
				$('#fixed').show();
				$('#percent').hide();
				$('#houcing_type').val('value');
			}else{
				$('#percent').show();
				$('#fixed').hide();
				$('#houcing_type').val('percent');
			}
        }
    });
</script>
<script type="text/javascript">

            $(function () {
                var calendar = $.calendars.instance('islamic');
                $('.hijri_datepicker').calendarsPicker({calendar: calendar});
            });
        </script>