<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">

    .btn-file {
        overflow: hidden;
        position: relative;
        vertical-align: middle;


    input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        filter: alpha(opacity=0);
        transform: translate(-300px, 0) scale(4);
        -webkit-transform: translate(-300px, 0) scale(4);
        font-size: 23px;
        height: 100%;
        width: 100%;
        direction: ltr;
        cursor: pointer;
    }

    }

    @-moz-document url-prefix() {
        .btn-file > input {
            transform: none;
        }
    }

    .fileinput {
        margin-bottom: 9px;
        display: inline-block;

    .form-control {
        padding-top: 7px;
        padding-bottom: 5px;
        display: inline-block;
        margin-bottom: 0px;
        vertical-align: middle;
        cursor: text;
    }

    .thumbnail {
        overflow: hidden;
        display: inline-block;
        margin-bottom: 5px;
        vertical-align: middle;
        text-align: center;

    >
    img {
        max-height: 100%;
    }

    }
    .btn {
        vertical-align: middle;
    }

    }
    .fileinput-exists .fileinput-new,
    .fileinput-new .fileinput-exists {
        display: none;
    }

    .fileinput-inline .fileinput-controls {
        display: inline;
    }

    .fileinput-filename {
        vertical-align: middle;
        display: inline-block;
        overflow: hidden;
    }

    .form-control .fileinput-filename {
        vertical-align: bottom;
    }

    .fileinput.input-group {
        display: table;
    }

    /
    /
    Not

    100
    %
    correct, but helps in typical use case
    .fileinput-new.input-group .btn-file,
    .fileinput-new .input-group .btn-file {
        border-radius: 0 @border-radius-base @border-radius-base 0;

    &
    .btn-xs,

    &
    .btn-sm {
        border-radius: 0 @border-radius-small @border-radius-small 0;
    }

    &
    .btn-lg {
        border-radius: 0 @border-radius-large @border-radius-large 0;
    }

    }

    .form-group.has-warning .fileinput {

    .fileinput-preview {
        color: @state-warning-text;
    }

    .thumbnail {
        border-color: @state-warning-border;
    }

    }
    .form-group.has-error .fileinput {

    .fileinput-preview {
        color: @state-danger-text;
    }

    .thumbnail {
        border-color: @state-danger-border;
    }

    }
    .form-group.has-success .fileinput {

    .fileinput-preview {
        color: @state-success-text;
    }

    .thumbnail {
        border-color: @state-success-border;
    }

    }

    /
    /
    Input group fixes
    .input-group-addon:not(:first-child) {
        border-left: 0;
    }

    .fileinput .btn {
        vertical-align: middle;
    }

    .btn.btn-file {
        position: relative;
        overflow: hidden;
    }

    .family, .wife, .children {
        display: none;
    }

    .spinner_sects, .spinner_manager {
        display: none;
        font-size: 7px;
    }

    h3.title {
        text-align: center;
        padding: 10px;
        display: block;
        background: #222D32 !important;
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#employee_list"
                                                                   data-toggle="tab"><?= lang('employee_list') ?></a>
                </li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_employee"
                                                                   data-toggle="tab"><?= lang('add_employee') ?></a>
                </li>
            </ul>
            <div class="tab-content no-padding">
                <!-- Employee List tab Starts -->

                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="employee_list"
                     style="position: relative;">
                    <div class="box" style="border: none;" data-collapsed="0">
                        <div class="box-body">
                            <div class="pull-right hidden-print" style="padding-top: 0px;padding-bottom: 8px">
                                <!--<span><?php echo btn_pdf('admin/employee/employee_list_pdf'); ?></span>-->
                            </div>
                            <table class="table table-bordered" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th width="10%" class="text-center"><?= lang('employee_id') ?></th>
                                    <th style="width:72px" class="text-center"><?= lang('image') ?></th>
                                    <th class="text-center"><?= lang('name') ?></th>
                                    <th class="text-center"><?= lang('job_title') ?></th>
                                    <th class="text-center"><?= lang('dept_desingation') ?></th>
                                    <th class="text-center"><?= lang('job_time') ?></th>
                                    <th class="text-center"><?= lang('status') ?></th>
                                    <th class="text-center"><?= lang('action') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty(@$employee_list)): ?>
                                    <?php foreach (@$employee_list as $emp): ?>
                                        <?php if ($this->session->userdata('user_type') == 2): ?>
                                            <?php if ($emp->nationality == 193): ?>
                                                <tr>
                                                    <td><?= $emp->employment_id ?></td>
                                                    <td><img
                                                                src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>"
                                                                style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                                alt="Please Connect Your Internet"></td>
                                                    <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                                    <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                                    <td>
                                                        <?php if ($lang == 'english'): ?>
                                                            <?= $emp->department_name . ' > '; ?>
                                                            <?php if ($emp->designations_id != '0'): ?>
                                                                <?= $emp->designations ?>
                                                            <?php else: ?>
                                                                <?= lang('no-exist') ?>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <?= $emp->department_name_ar . ' > '; ?>
                                                            <?php if ($emp->designations_id != '0'): ?>
                                                                <?= $emp->designations_ar ?>
                                                            <?php else: ?>
                                                                <?= lang('no-exist') ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if ($emp->job_time == 'Part'): ?>
                                                            <?= lang('job_part') ?>
                                                        <?php else: ?>
                                                            <?= lang('job_full') ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if ($emp->status == 1):
                                                            echo '<span class="label label-success">' . lang("active") . '</span>';
                                                        elseif ($emp->status == 2):
                                                            echo '<span class="label label-danger">' . lang("inactive") . '</span>';
                                                        else:
                                                            echo '<span class="label label-primary">' . lang("in_test") . '</span>';
                                                        endif; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <!-- view -->
                                                        <a href="<?= base_url() ?>admin/employee/profile/<?= @$emp->employee_id ?>"
                                                           class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
                                                        <!-- view -->

                                                        <!-- personal_details -->
                                                        <a href="<?= base_url() ?>admin/employee/employees/<?= @$emp->employee_id ?>"
                                                           class="btn btn-primary btn-xs"><i
                                                                    class="fa fa-edit"></i> <?= lang('personal_details') ?>
                                                        </a>
                                                        <!-- personal_details -->

                                                        <!-- delete -->
                                                        <?php if (@$emp->job_title != 4): ?>
                                                            <a href="<?= base_url() ?>admin/employee/delete_employee/<?= @$emp->employee_id ?>"
                                                               class="btn btn-danger btn-xs"
                                                               onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                        class="fa fa-trash"></i> <?= lang('delete') ?>
                                                            </a>
                                                        <?php else: ?>
                                                            <a href="#" class="btn btn-danger btn-xs disabled"><i
                                                                        class="fa fa-trash"></i> <?= lang('delete') ?>
                                                            </a>
                                                        <?php endif; ?>
                                                        <!-- delete -->
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td><?= $emp->employment_id ?></td>
                                                <td><img
                                                            src="<?= (!empty(@$emp->photo)) ? base_url() . @$emp->photo : base_url() . 'img/admin.png'; ?>"
                                                            style="width:70px; height: 70px; border: 2px solid gray; border-radius:5px; "
                                                            alt="Please Connect Your Internet"></td>
                                                <td><?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?></td>
                                                <td><?= ($lang == 'english') ? $emp->job_titles_name_en : $emp->job_titles_name_ar; ?></td>
                                                <td>
                                                    <?php if ($lang == 'english'): ?>
                                                        <?= $emp->department_name . ' > '; ?>
                                                        <?php if ($emp->designations_id != '0'): ?>
                                                            <?= $emp->designations ?>
                                                        <?php else: ?>
                                                            <?= lang('no-exist') ?>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <?= $emp->department_name_ar . ' > '; ?>
                                                        <?php if ($emp->designations_id != '0'): ?>
                                                            <?= $emp->designations_ar ?>
                                                        <?php else: ?>
                                                            <?= lang('no-exist') ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($emp->job_time == 'Part'): ?>
                                                        <?= lang('job_part') ?>
                                                    <?php else: ?>
                                                        <?= lang('job_full') ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($emp->status == 1):
                                                        echo '<span class="label label-success">' . lang("active") . '</span>';
                                                    elseif ($emp->status == 2):
                                                        echo '<span class="label label-danger">' . lang("inactive") . '</span>';
                                                    else:
                                                        echo '<span class="label label-primary">' . lang("in_test") . '</span>';
                                                    endif; ?>
                                                </td>
                                                <td class="text-center">
                                                    <!-- view -->
                                                    <a href="<?= base_url() ?>admin/employee/profile/<?= @$emp->employee_id ?>"
                                                       class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
                                                    <!-- view -->

                                                    <!-- personal_details -->
                                                    <a href="<?= base_url() ?>admin/employee/employees/<?= @$emp->employee_id ?>"
                                                       class="btn btn-primary btn-xs"><i
                                                                class="fa fa-edit"></i> <?= lang('personal_details') ?>
                                                    </a>
                                                    <!-- personal_details -->

                                                    <!-- delete -->
                                                    <?php if (@$emp->job_title != 4): ?>
                                                        <a href="<?= base_url() ?>admin/employee/delete_employee/<?= @$emp->employee_id ?>"
                                                           class="btn btn-danger btn-xs"
                                                           onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                                    class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                                    <?php else: ?>
                                                        <a href="#" class="btn btn-danger btn-xs disabled"><i
                                                                    class="fa fa-trash"></i> <?= lang('delete') ?></a>
                                                    <?php endif; ?>
                                                    <!-- delete -->
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Employee List tab Ends -->


                <!-- Add Employee tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_employee" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <div class="row">

                                <form
                                        action="<?= base_url() ?>admin/employee/save_employee/<?= @$employee_info->employee_id ?>"
                                        method="post" class="form form-horizontal form-groups-bordered"
                                        enctype="multipart/form-data" id="form">


                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('personal_details') ?></h3>
                                            </label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- full_name_ar -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('full_name_ar') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input name="full_name_ar" value="<?= @$employee_info->full_name_ar ?>"
                                                       type="text" class="form-control" required="">
                                            </div>
                                        </div>

                                        <!-- full_name_en -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('full_name_en') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input name="full_name_en" value="<?= @$employee_info->full_name_en ?>"
                                                       type="text" class="form-control" required="">
                                            </div>
                                        </div>

                                        <!-- date_of_birth -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('date_of_birth') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-6">
                                                <input name="date_of_birth"
                                                       value="<?= @$employee_info->date_of_birth ?>" id="date_of_birth"
                                                       type="text" class="form-control hijri_datepicker" required="">
                                            </div>
                                            <div class="col-sm-1"><a href="#date_of_birth" class="btn btn-primary"><i
                                                            class="entypo-calendar"></i></a></div>
                                        </div>

                                        <!-- gender -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('gender') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <select name="gender" class="form-control" required="">
                                                    <option value=""><?= lang('gender') ?> ...</option>
                                                    <option
                                                            value="Male" <?= (@$employee_info->gender == 'Male') ? 'selected' : ''; ?>><?= lang('male') ?></option>
                                                    <option
                                                            value="Female" <?= (@$employee_info->gender == 'Female') ? 'selected' : ''; ?>><?= lang('female') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- phone -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('phone') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input name="phone" value="<?= @$employee_info->phone ?>" type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <!-- email -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('email') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input name="email" value="<?= @$employee_info->email ?>" type="text"
                                                       class="form-control" required="">
                                            </div>
                                        </div>

                                        <!-- nationality -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('nationality') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <select name="nationality" class="form-control" required="">
                                                    <option value=""><?= lang('select_country') ?>...</option>
                                                    <?php foreach (@$all_countries as $country): ?>
                                                        <option
                                                                value="<?= $country->idCountry ?>" <?= ($country->idCountry == @$employee_info->nationality) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $country->countryName : $country->countryName_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- present_address -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('present_address') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" value="<?= @$employee_info->present_address ?>"
                                                       name="present_address" class="form-control" required=""/>
                                            </div>
                                        </div>

                                        <!-- education -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('education') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-6">
                                                <input name="education" value="<?= @$employee_info->education ?>"
                                                       type="text" class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-3">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-primary btn-file"><span
                                                    class="fileinput-new"><?= lang('select_file') ?></span>
                                            <span class="fileinput-exists"><?= lang('change') ?></span>
                                            <input type="file" name="id_proff"></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                            </div>
                                            <?php if (!empty(@$employee_info->id_proff) and file_exists(FCPATH . 'img/uploads/' . @$employee_info->id_proff)): ?>
                                                <div class="col-sm-6 btn btn-default"><a target="_blank"
                                                                                         href="<?= base_url() ?>img/uploads/<?= urlencode(@$employee_info->id_proff) ?>"><?= urldecode(@$employee_info->id_proff) ?></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <!-- passport_number -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('passport_no') ?></label>
                                            <div class="col-sm-9">
                                                <input name="passport_number"
                                                       value="<?= @$employee_info->passport_number ?>" type="number"
                                                       class="form-control passport_number"/>
                                            </div>
                                        </div>

                                        <!-- passport_end -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('passport_end') ?></label>
                                            <div class="col-sm-6">
                                                <input name="passport_end" value="<?= @$employee_info->passport_end ?>"
                                                       id="passport_end" type="text"
                                                       class="form-control hijri_datepicker2">
                                            </div>
                                            <div class="col-sm-1"><a href="#passport_end" class="btn btn-primary"><i
                                                            class="entypo-calendar"></i></a></div>
                                        </div>

                                        <!-- identity_no -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('identity_no') ?></label>
                                            <div class="col-sm-9">
                                                <input name="identity_no" value="<?= @$employee_info->identity_no ?>"
                                                       type="number" class="form-control identity_no"/>
                                            </div>
                                        </div>

                                        <!-- identity_end -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('identity_end') ?></label>
                                            <div class="col-sm-6">
                                                <input name="identity_end" value="<?= @$employee_info->identity_end ?>"
                                                       id="identity_end" type="text"
                                                       class="form-control hijri_datepicker2"/>
                                            </div>
                                            <div class="col-sm-1"><a href="#identity_end" class="btn btn-primary"><i
                                                            class="entypo-calendar"></i></a></div>
                                        </div>

                                        <!-- education -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('photo') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-3">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail"
                                                         style="width: 200px; height: 200px;">
                                                        <img src="<?= (!empty(@$employee_info->photo)) ? base_url() . @$employee_info->photo : base_url() . 'img/admin.png'; ?>"
                                                             alt="Please Connect Your Internet">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 200px;"></div>
                                                    <div>
                                                        <span class="btn btn-primary btn-file"><span
                                                                    class="fileinput-new"><?= lang('select_file') ?></span><span
                                                                    class="fileinput-exists"><?= lang('change') ?></span><input
                                                                    type="file" name="photo"></span>
                                                        <a href="#" class="btn btn-primary fileinput-exists"
                                                           data-dismiss="fileinput"><?= lang('remove') ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- maratial_status -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('maratial_status') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <select name="maratial_status" class="form-control" required="">
                                                    <option value=""><?= lang('maratial_status') ?>...</option>
                                                    <option
                                                            value="Married" <?= (@$employee_info->maratial_status == "Married") ? 'selected' : ''; ?>><?= lang('married') ?></option>
                                                    <option
                                                            value="Un-Married" <?= (@$employee_info->maratial_status == "Un-Married") ? 'selected' : ''; ?>><?= lang('un-married') ?></option>
                                                    <option
                                                            value="Widowed" <?= (@$employee_info->maratial_status == "Widowed") ? 'selected' : ''; ?>><?= lang('widowed') ?></option>
                                                    <option
                                                            value="Divorced" <?= (@$employee_info->maratial_status == "Divorced") ? 'selected' : ''; ?>><?= lang('divorced') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('family') ?></h3></label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- dependent -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('dependent') ?></label>
                                            <div class="col-sm-9">
                                                <a class="btn btn-primary"
                                                   onclick="$('.family').fadeToggle()"><?= lang('show_hide') ?></a>
                                            </div>
                                        </div>

                                        <!-- wife -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('wife') ?></label>
                                            <div class="col-sm-9">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang('wife_name') ?></th>
                                                        <th><?= lang('wife_name_ar') ?></th>
                                                        <th><?= lang('wife_birth') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><input type="text" name="wife_name"
                                                                   value="<?= @$employee_info->wife_name ?>"
                                                                   class="form-control"></td>
                                                        <td><input type="text" name="wife_name_ar"
                                                                   value="<?= @$employee_info->wife_name_ar ?>"
                                                                   class="form-control"></td>
                                                        <td><input type="text" name="wife_birth"
                                                                   value="<?= @$employee_info->wife_birth ?>"
                                                                   class="form-control hijri_datepicker"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!-- children -->
                                        <div class="form-group family"
                                             style="<?= (!empty(@$employee_info->employee_id)) ? 'display:block' : ''; ?>">
                                            <label class="col-sm-3 control-label"><?= lang('children') ?></label>
                                            <div class="col-sm-9">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang('fils_name') ?></th>
                                                        <th><?= lang('fils_name_ar') ?></th>
                                                        <th><?= lang('fils_birth') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $fils_name = explode(';', @$employee_info->fils_name); ?>
                                                    <?php $fils_name_ar = explode(';', @$employee_info->fils_name_ar); ?>
                                                    <?php $fils_birth = explode(';', @$employee_info->fils_birth); ?>
                                                    <?php for ($i = 0; $i < 5; $i++): ?>
                                                        <tr>
                                                            <td><input type="text" name="fils_name[]"
                                                                       value="<?= @$fils_name[$i] ?>"
                                                                       class="form-control"></td>
                                                            <td><input type="text" name="fils_name_ar[]"
                                                                       value="<?= @$fils_name_ar[$i] ?>"
                                                                       class="form-control"></td>
                                                            <td><input type="text" name="fils_birth[]"
                                                                       value="<?= @$fils_birth[$i] ?>"
                                                                       class="form-control hijri_datepicker"></td>
                                                        </tr>
                                                    <?php endfor; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('insurance') ?></h3></label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- med_insur -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('medical_insur') ?></label>
                                            <div class="col-sm-9">
                                                <select name="med_insur" id="medical_insur" class="form-control">
                                                    <option
                                                            value="0" <?= (@$employee_info->med_insur == 0) ? 'selected' : ''; ?>><?= lang('medical_insur_select') ?></option>
                                                    <option
                                                            value="1" <?= (@$employee_info->med_insur == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                    <option
                                                            value="2"<?= (@$employee_info->med_insur == 2) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- med_insur_type -->
                                        <div class="form-group">
                                            <label
                                                    class="col-sm-3 control-label"><?= lang('medical_insur_type') ?></label>
                                            <div class="col-sm-9">
                                                <select name="med_insur_type" id="med_insur_type" class="form-control"
                                                        disabled="">
                                                    <option
                                                            value="0" <?= (@$employee_info->med_insur_type == 0) ? 'selected' : ''; ?>><?= lang('medical_insur_type_select') ?></option>
                                                    <option
                                                            value="1" <?= (@$employee_info->med_insur_type == 1) ? 'selected' : ''; ?>><?= lang('medical_insur_type_all') ?></option>
                                                    <option
                                                            value="2" <?= (@$employee_info->med_insur_type == 2) ? 'selected' : ''; ?>><?= lang('medical_insur_type_part') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- social_insurance -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('social_insur') ?></label>
                                            <div class="col-sm-9">
                                                <select name="social_insurance" id="soc_insur" class="form-control">
                                                    <option
                                                            value="0" <?= (@$employee_info->social_insurance == 0) ? 'selected' : ''; ?>><?= lang('social_insur_select') ?></option>
                                                    <option
                                                            value="1" <?= (@$employee_info->social_insurance == 1) ? 'selected' : ''; ?>><?= lang('yes') ?></option>
                                                    <option
                                                            value="2" <?= (@$employee_info->social_insurance == 2) ? 'selected' : ''; ?>><?= lang('no') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- social_insurance_type -->
                                        <div class="form-group">
                                            <label
                                                    class="col-sm-3 control-label"><?= lang('social_insurance_type') ?></label>
                                            <div class="col-sm-9">
                                                <select name="social_insurance_type" id="social_insurance_type"
                                                        class="form-control" disabled="">
                                                    <option
                                                            value="0" <?= (@$employee_info->social_insurance_type == "0") ? 'selected' : ''; ?>><?= lang('social_insur_select') ?></option>
                                                    <option
                                                            value="saudi1" <?= (@$employee_info->social_insurance_type == "saudi1") ? 'selected' : ''; ?>><?= lang('saudi1') ?></option>
                                                    <option
                                                            value="saudi2" <?= (@$employee_info->social_insurance_type == "saudi2") ? 'selected' : ''; ?>><?= lang('saudi2') ?></option>
                                                    <option
                                                            value="non-saudi" <?= (@$employee_info->social_insurance_type == "non-saudi") ? 'selected' : ''; ?>><?= lang('non-saudi') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('official_status') ?></h3>
                                            </label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- status -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('status') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <select name="status" class="form-control status" required="">
                                                    <option value=""></option>
                                                    <option
                                                            value="1" <?= (@$employee_info->status == 1) ? 'selected' : ''; ?>><?= lang('active') ?></option>
                                                    <option
                                                            value="2" <?= (@$employee_info->status == 2) ? 'selected' : ''; ?>><?= lang('inactive') ?></option>
                                                    <option
                                                            value="3" <?= (@$employee_info->status == 3) ? 'selected' : ''; ?>><?= lang('in_test') ?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- test_period -->
                                        <style type="text/css">
                                            .test_period-form-group {
                                            <?php if(!empty(@$employee_info) and @$employee_info->test_period!=0):?> display: block;
                                            <?php else:?> display: none;
                                            <?php endif;?>
                                            }
                                        </style>
                                        <div class="form-group test_period-form-group">
                                            <label class="col-sm-3 control-label"><?= lang('test_period') ?>
                                                (<?= lang("days") ?>)<span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <input type="number" min="0" name="test_period"
                                                    <?= (!empty(@$employee_info) and @$employee_info->test_period != 0) ? 'required' : 'disabled'; ?>
                                                       class="form-control test_period"
                                                       value="<?= @$employee_info->test_period ?>">

                                            </div>
                                        </div>

                                        <!-- department -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('department') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-9">
                                                <select name="departement_id" class="form-control" required=""
                                                        onchange="print_sects(this.value)">
                                                    <option value=""></option>
                                                    <?php foreach (@$department_list as $dep): ?>
                                                        <option
                                                                value="<?= $dep->department_id ?>" <?= ($dep->department_id == @$employee_info->departement_id) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $dep->department_name : $dep->department_name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- designations_id -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('designation') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-8">
                                                <select id="x3" name="designations_id" class="form-control" required="">
                                                    <option value=""></option>
                                                    <option value="0"><?= lang('without_section') ?></option>
                                                </select>
                                            </div>
                                            <div class="col-sm-1 spinner_sects"><i
                                                        class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
                                        </div>

                                        <!-- direct_manager_id -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('direct_manager') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <select name="direct_manager_id" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php foreach (@$employees_list as $emp): ?>
                                                        <option
                                                                value="<?= $emp->employee_id ?>" <?= ($emp->employee_id == @$employee_info->direct_manager_id) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- employee_category_id -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('employee_type') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <select name="employee_category_id" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php foreach (@$emp_cat_list as $emp_cat): ?>
                                                        <option
                                                                value="<?= $emp_cat->id ?>"<?= ($emp_cat->id == @$employee_info->employee_category_id) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $emp_cat->name_en : $emp_cat->name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- joining_date -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('joining_date') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-6">
                                                <input name="joining_date" value="<?= @$employee_info->joining_date ?>"
                                                       id="joining_date" type="text"
                                                       class="form-control hijri_datepicker" required="">
                                            </div>
                                            <div class="col-sm-1"><a href="#joining_date" class="btn btn-primary"><i
                                                            class="entypo-calendar"></i></a></div>
                                        </div>

                                        <!-- retirement_date -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('retirement_date') ?></label>
                                            <div class="col-sm-6">
                                                <input name="retirement_date"
                                                       value="<?= @$employee_info->retirement_date ?>"
                                                       id="retirement_date" type="text"
                                                       class="form-control hijri_datepicker">
                                            </div>
                                            <div class="col-sm-1"><a href="#retirement_date" class="btn btn-primary"><i
                                                            class="entypo-calendar"></i></a></div>
                                        </div>

                                        <!-- job_time -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('job_time') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <select name="job_time" class="form-control job_time" required="">
                                                    <option value=""></option>
                                                    <option value="Full" <?= (@$employee_info->job_time == "Full") ? 'selected' : ''; ?>><?= lang('job_full') ?></option>
                                                    <option value="Part" <?= (@$employee_info->job_time == "Part") ? 'selected' : ''; ?>><?= lang('job_part') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('.job_time').change(function () {
                                                if ($(this).val() == 'Part') {
                                                    $('.job_time_hours').fadeIn();
                                                }
                                                else {
                                                    $('.job_time_hours').fadeOut();
                                                }
                                            });
                                        </script>

                                        <!-- job_time_hours -->
                                        <div class="form-group job_time_hours"
                                             style="display: <?= (@$employee_info->job_time == "Part") ? 'block' : 'none'; ?>">
                                            <label class="col-sm-3 control-label"><?= lang('job_time_hours') ?></label>
                                            <div class="col-md-9">
                                                <input type="number" min="0" max="24" name="job_time_hours" class="form-control" value="<?=@$employee_info->job_time_hours;?>">
                                            </div>
                                        </div>

                                        <!-- job_place_id -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('job_place') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <select name="job_place_id" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php foreach (@$job_place_list as $jp): ?>
                                                        <option
                                                                value="<?= $jp->job_place_id ?>" <?= ($jp->job_place_id == @$employee_info->job_place_id) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $jp->place_name_en : $jp->place_name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- job_title -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('job_title') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <select name="job_title" class="form-control" required="">
                                                    <option value=""></option>
                                                    <?php foreach (@$job_titles_list as $jt): ?>
                                                        <option
                                                                value="<?= $jt->job_titles_id ?>" <?= ($jt->job_titles_id == @$employee_info->job_title) ? 'selected' : ''; ?>>
                                                            <?= ($lang == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- holiday_no -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('holiday_no') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <input type="number" value="<?= @$employee_info->holiday_no ?>"
                                                       name="holiday_no" class="form-control" required=""/>
                                            </div>
                                        </div>

                                        <!-- employee_salary -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('employee_salary') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-md-9">
                                                <input type="number" value="<?= @$employee_info->employee_salary ?>"
                                                       min="0" name="employee_salary" class="form-control" required=""/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('employee_document') ?></h3>
                                            </label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- cin_photo -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('cin_photo') ?></label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                                    <div class="btn btn-primary btn-file btn-block"><span
                                                                class="fileinput-new"><?= lang('select_file') ?></span>
                                                        <span class="fileinput-exists"><?= lang('change') ?></span>
                                                        <input type="file" name="cin_photo_path" class="form-control">
                                                    </div>
                                                    <div class="fileinput-filename"></div>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                                <div id="msg_pdf" style="color: #e11221"></div>
                                            </div>
                                        </div>

                                        <!-- passport_photo -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('passport_photo') ?></label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                                    <div class="btn btn-primary btn-file btn-block"><span
                                                                class="fileinput-new"><?= lang('select_file') ?></span>
                                                        <span class="fileinput-exists"><?= lang('change') ?></span>
                                                        <input type="file" name="passport_photo_path"
                                                               class="form-control">
                                                    </div>
                                                    <div class="fileinput-filename"></div>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                                <div id="msg_pdf" style="color: #e11221"></div>
                                            </div>
                                        </div>

                                        <!-- resume_path -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('resume') ?></label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                                    <div class="btn btn-primary btn-file btn-block"><span
                                                                class="fileinput-new"><?= lang('select_file') ?></span>
                                                        <span class="fileinput-exists"><?= lang('change') ?></span>
                                                        <input type="file" name="resume_path" class="form-control">
                                                    </div>
                                                    <div class="fileinput-filename"></div>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                                <div id="msg_pdf" style="color: #e11221"></div>
                                            </div>
                                        </div>

                                        <!-- contract_paper_path -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('contract_paper') ?></label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                                    <div class="btn btn-primary btn-file btn-block"><span
                                                                class="fileinput-new"><?= lang('select_file') ?></span>
                                                        <span class="fileinput-exists"><?= lang('change') ?></span>
                                                        <input type="file" name="contract_paper_path"
                                                               class="form-control">
                                                    </div>
                                                    <div class="fileinput-filename"></div>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                                <div id="msg_pdf" style="color: #e11221"></div>
                                            </div>
                                        </div>

                                        <!-- other_document_path -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('other_documents') ?></label>
                                            <div class="col-sm-7">
                                                <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                                    <div class="btn btn-primary btn-file btn-block"><span
                                                                class="fileinput-new"><?= lang('select_file') ?></span>
                                                        <span class="fileinput-exists"><?= lang('change') ?></span>
                                                        <input type="file" name="other_document_path"
                                                               class="form-control">
                                                    </div>
                                                    <div class="fileinput-filename"></div>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput"
                                                       style="float: none; color:red;">&times;</a>
                                                </div>
                                                <div id="msg_pdf" style="color: #e11221"></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('bank_information') ?></h3>
                                            </label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- bank_name -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('bank_name') ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="bank_name"
                                                       value="<?= @$employee_info->bank_name ?>" class="form-control"/>
                                            </div>
                                        </div>

                                        <!-- branch_name -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('branch_name') ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="branch_name"
                                                       value="<?= @$employee_info->branch_name ?>"
                                                       class="form-control"/>

                                            </div>
                                        </div>

                                        <!-- account_name -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('account_name') ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="account_name"
                                                       value="<?= @$employee_info->account_name ?>"
                                                       class="form-control"/>

                                            </div>
                                        </div>

                                        <!-- account_number -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('account_number') ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="account_number"
                                                       value="<?= @$employee_info->account_number ?>"
                                                       class="form-control"/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->
                                        <div class="form-group">
                                            <label class="col-sm-12"><h3
                                                        class="title btn-primary"><?= lang('employee_account') ?></h3>
                                            </label>
                                        </div>
                                        <!---------------------------------------------------------------->
                                        <!---------------------------------------------------------------->

                                        <!-- employment_id -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('employee_id') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-6">
                                                <input name="employment_id"
                                                       value="<?= @$employee_info->employment_id ?>" maxlength="12"
                                                       type="text" class="form-control employment_id" required="">
                                            </div>
                                            <div class="col-sm-3"><a class="btn btn-primary"
                                                                     onclick="generate()"><?= lang('generate_auto') ?></a>
                                            </div>
                                        </div>

                                        <!-- password -->
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?= lang('password') ?> <span
                                                        class="required"> *</span></label>
                                            <div class="col-sm-6">
                                                <input name="password" value="<?= @$employee_info->password ?>"
                                                       type="password" id="password" class="form-control" required="">
                                            </div>
                                            <div class="col-sm-3"><a class="btn btn-primary"
                                                                     onclick="generate2()"><?= lang('generate_auto') ?></a>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group margin">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" id="sbtn"
                                                    class="btn btn-primary btn-block"><?= lang('save') ?></button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        var calendar = $.calendars.instance('ummalqura');
        $('.hijri_datepicker').datepicker({language: "ar", rtl: true, format:"yyyy-mm-dd", "autoclose": true});
        $('.hijri_datepicker2').datepicker({language: "ar", rtl: true, startDate: "today" , format:"yyyy-mm-dd", "autoclose": true});

        $('#medical_insur').on('change', function () {
            console.log($(this).val());
            if ($(this).val() == '1')
                $('#med_insur_type').prop('disabled', false);
            else
                $('#med_insur_type').prop('disabled', 'true');
        });
        $('#soc_insur').on('change', function () {
            if ($(this).val() == '1')
                $('#social_insurance_type').prop('disabled', false);
            else
                $('#social_insurance_type').prop('disabled', 'true');
        });
        <?php if (!empty(@$employee_info->departement_id)): ?>
        print_sects(<?= $employee_info->departement_id ?>);
        <?php endif; ?>

        $('.status').change(function () {
            var val = $(this).val();
            if (val == 3) {
                $('.test_period-form-group').fadeIn();
                $('.test_period-form-group .test_period').prop('disabled', false);
                $('.test_period-form-group .test_period').prop('required', true);
            }
            else {
                $('.test_period-form-group').fadeOut();
                $('.test_period-form-group .test_period').prop('disabled', true);
                $('.test_period-form-group .test_period').prop('required', false);
            }
        });
    });
    function generate2() {
        var val = makeid();
        $('#password').val(val);
    }
    function generate() {
        var val = makeid();
        var employment_ids = <?= json_encode($employment_ids) ?>;
        $.each(employment_ids, function (i, v) {
            if (v.employment_id == val)
                generate();
            return;
        });
        $('.employment_id').val(val);
        return;
    }
    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    function family(s) {
        var val = s.val();
        if (val == 'Married') {

            $('.family').fadeIn('fast');
            $('.wife').fadeIn('fast');
            $('.children').fadeIn('fast');
        } else if (val == 'Widowed' || val == 'Divorced') {
            $('.family').fadeIn('fast');
            $('.wife').fadeOut('fast');
            $('.children').fadeIn('fast');
        } else if (val == 'Divorced') {
            $('.family').fadeIn('fast');
            $('.wife').fadeOut('fast');
            $('.children').fadeIn('fast');
        } else {
            $('.family').fadeOut('fast');
            $('.wife').fadeOut('fast');
            $('.children').fadeOut('fast');
        }
    }
</script>
<script type="text/javascript">
    var lang = "<?= $this->session->userdata('lang'); ?>";
    function print_sects(id) {
        var vurl = "<?php echo base_url() ?>admin/employee/get_sec_with_dep/" + id;
        if (id) {
            $('.spinner_sects').fadeIn('fast');

            $.ajax({
                url: vurl,
                success: function (data) {
                    $('#x3').empty();
                    $('#x3').append('<option value=""></option>');
                    $('#x3').append('<option value="0"><?= lang("without_section") ?></option>');
                    for (var i = 0; i < data.length; i++) {
                        if (lang == 'arabic')
                            $('#x3').append('<option value="' + data[i].designations_id + '" >' + data[i].designations_ar + '</option>');
                        else
                            $('#x3').append('<option value="' + data[i].designations_id + '">' + data[i].designations + '</option>');
                    }
                    <?php if (!empty(@$employee_info->departement_id)): ?>
                    $('#x3').val('<?=$employee_info->designations_id?>');
                    <?php endif; ?>
                },
                dataType: 'json'
            }).fail(function () {
                alert('<?= lang('alert_error') ?>');
            }).always(function () {
                $('.spinner_sects').fadeOut('fast');
                <?php if (!empty(@$employee_info->designations_id)): ?>
                var select = <?= @$employee_info->designations_id ?>;
                console.log(select + '-----');
                $('#x3').find('option').map(function () {
                    if ($(this).attr('value') == select)
                        $(this).prop('selected', 'true');
                });
                <?php endif; ?>
            });
        } else {
            $('#x3').empty();
            $('#x3').append('<option value=""></option>');
            $('#x3').append('<option value="0"><?= lang("without_section") ?></option>');
        }


    }
</script>


<script type="text/javascript">
    $("#form").submit(function(e){
        var  passport_number= $(".passport_number").val();
        var identity_no = $(".identity_no").val();
        var passport_end = $("#passport_end").val();
        var identity_end = $("#identity_end").val();
        if(!passport_number && !identity_no){
            alert("<?=($this->session->userdata('lang')=='arabic')?'يجب تعمير رقم جواز السفر أو رقم الهوية':'You have to type Passport number or Identity card number !';?>")
            e.preventDefault();
            return;
        }
        else{
            if(passport_number && !passport_end){
                alert("<?=($this->session->userdata('lang')=='arabic')?'يجب تعمير تاريخ نهاية جواز السفر':'You have to type Passport end date !';?>")
                e.preventDefault();
                return;
            }
            if(identity_no && !identity_end){
                alert("<?=($this->session->userdata('lang')=='arabic')?'يجب تعمير تاريخ نهاية بطاقة الهوية':'You have to type CIN end date !';?>")
                e.preventDefault();
                return;
            }
        }
        var today="<?=$today?>";
        today=parseInt(today.replace(/-/g, ''));
        passport_end=parseInt(passport_end.replace(/-/g, ''));
        identity_end= parseInt(identity_end.replace(/-/g, ''));
        if(passport_end<today && passport_end){
            alert("<?=($this->session->userdata('lang')=='arabic')?'تاريخ نهاية جواز السفر غير صحيح':'Passport end date incorrect !';?>")
            e.preventDefault();
            return;
        }
        if(identity_end<today && identity_end){
            alert("<?=($this->session->userdata('lang')=='arabic')?'تاريخ نهاية بطاقة الهوية غير صحيح':'CIN end date incorrect !';?>")
            e.preventDefault();
            return;
        }

        $('#sbtn').prop('disabled','true');

    });
</script>
