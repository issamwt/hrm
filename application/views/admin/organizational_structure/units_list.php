<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .link, .link:hover{
        text-decoration: none !important;
        font-weight: bold !important;
    }
</style>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#units_list" data-toggle="tab"><?= lang('units_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_unit"  data-toggle="tab"><?= lang('add_unit') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="units_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="18%"  class="text-center"><?= lang('unit_name') ?></th>
                                        <th width="18%"   class="text-center"><?= lang('designation_name') ?></th>
                                        <th width="18%"  class="text-center"><?= lang('designation_emp') ?></th>
                                        <th width="28%"  class="text-center"><?= lang('description') ?></th>
                                        <th width="13%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($units_list)): $sl = 1; ?>
                                        <?php foreach ($units_list as $unit): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'arabic') ? $unit->unit_ar : $unit->unit_en; ?></td>
                                                <td>
                                                    <?php
                                                    if ($lang == 'arabic')
                                                        echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/designation_list/' . $unit->designations_id . '">' . $unit->designations_ar . '</a>';
                                                    else
                                                        echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/designation_list/' . $unit->designations_id . '">' . $unit->designations . '</a>';
                                                    ?>
                                                </td>
                                                <td><?= ($lang == 'arabic') ? $unit->full_name_ar : $unit->full_name_en; ?></td>
                                                <td><?= ($lang == 'arabic') ? $unit->unit_desc_ar : $unit->unit_desc_en; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/organizational_structure/units_list/<?php echo $unit->unit_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/organizational_structure/delete_unit/<?php echo $unit->unit_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_unit" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/organizational_structure/save_unit/<?= @$unit_info->unit_id; ?> " method="post" class="form-horizontal">

                                <!-- Designation Id for Update -->
                                <?php if (!empty($unit_info->unit_id)): ?>
                                    <input type="hidden" name="unit_id"  class="form-control" id="field-1" value="<?= @$unit_info->unit_id; ?>"/>
                                <?php endif; ?>

                                <!-- Unit Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="unit_ar"  class="form-control" id="field-1" value="<?= @$unit_info->unit_ar; ?>" required=""/>
                                    </div>
                                </div>


                                <!-- Unit English Name -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label"><?= lang('name_en') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="unit_en"  class="form-control" id="field-2" value="<?= @$unit_info->unit_en; ?>" required=""/>
                                    </div>
                                </div>

                                <!-- The Designation -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('designation_name') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select name="designations_id" class="form-control" id="field-1">
                                            <option value=""><?= lang('select_designation') ?>.....</option>
                                            <?php if (!empty($designations_info)): ?>
                                                <?php foreach ($designations_info as $des): ?>
                                                    <option value="<?= $des->designations_id ?>" <?php if (@$unit_info->designations_id == $des->designations_id) echo 'selected'; ?>>
                                                        <?= ($lang == 'arabic') ? $des->designations_ar : $des->designations; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>


                                <!-- Employee Name -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><?= lang('employee_name') ?></label>
                                    <div class="col-sm-5">
                                        <select name="employee_id_non_required" class="form-control">
                                            <option value=""><?= lang('select_employee') ?>.....</option>
                                            <?php if (!empty($employee_info)): ?>
                                                <?php foreach ($employee_info as $emp): ?>
                                                    <option value="<?= $emp->employee_id ?>" <?php if (@$unit_info->unit_employee_id == $emp->employee_id) echo 'selected'; ?>>
                                                        <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Description in Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="unit_desc_ar" class="form-control" rows="10"><?php if (!empty($unit_info->unit_desc_ar)) echo $unit_info->unit_desc_ar; ?></textarea>
                                    </div>
                                </div>

                                <!-- Description in english -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_en') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="unit_desc_en" class="form-control" rows="10"><?php if (!empty($unit_info->unit_desc_en)) echo $unit_info->unit_desc_en; ?></textarea>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>