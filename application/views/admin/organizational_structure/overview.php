<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset/css/jquery.orgchart.css" >
<style type="text/css">
    .orgchart{
        background-image: none !important;
        direction: ltr !important;
        display: block  !important;
        margin: 0px auto !important;
        transform: translateX(-50%);
        cursor: move;
    }
    .orgchart table{
        cursor: move;
    }
    .node .content {
        height: inherit !important;
        min-height: inherit !important;
        padding: 2px;
    }
    .node i.fa.fa-info-circle.chartform-icon {
        position: absolute;
        top: 0px;
        right: 0px;
        color: #2C3B41;
        box-shadow: 0px 0px 2px 1px #000;
        border-radius: 50%;
        padding: 1px;
        background: #fff;
        display: none;
        cursor: pointer;
    }
    .node:hover i.fa.fa-info-circle.chartform-icon {
        display: block;
    }
    .chartform{
        display: none;
        background: #222D32;
        position: absolute;
        top: 12px;
        right: -173px;
        width: 180px;
        border-radius: 2px;
        color: #fff;
        z-index: 100000;
        direction: <?= ($lang == 'arabic') ? 'rtl' : 'ltr'; ?> !important;
    }
    .chartform *{
        font-size:12px !important;
    }
    .chartform .btn{
        display: inline-block;
        margin: 5px 5px;
    }
    .orgchart {
        background: #fff;
    }
    .orgchart td.left, .orgchart td.right, .orgchart td.top {
        border-color: #aaa;
    }
    .orgchart td>.down {
        background-color: #aaa;
    }
    .orgchart .division .title {
        background-color: #996633 !important;
    }
    .orgchart .division .content {
        border-color: #996633;
    }
    .orgchart .unit .title {
        background-color: #009933 !important;
    }
    .orgchart .unit .content {
        border-color: #009933;
    }
    .orgchart .designation .title {
        background-color: #993366 !important;
    }
    .orgchart .designation .content {
        border-color: #993366;
    }
    .orgchart .department .title {
        background-color: #006699 !important;
    }
    .orgchart .department .content {
        border-color: #006699;
    }
    .orgchart .branche .title {
        background-color: #cc0066 !important;
    }
    .orgchart .branche .content {
        border-color: #cc0066;
    }
</style>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="nav-tabs-custom" style="min-height: 500px; position: relative">
            <div id="chart-container">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.orgchart.js"></script>
<script type="text/javascript">
    var datasource = <?= json_encode($datasource) ?>;
    $('#chart-container').orgchart({
        'data': datasource,
        'parentNodeSymbol': 'fa-th-large',
        'toggleSiblingsResp': true,
        'nodeContent': 'title',
        'pan': true,
        'zoom': true,
        'depth': 7,
        'createNode': function ($node, data) {
            var id = data.id;
            var secondMenuIcon = $('<i>', {
                'class': 'fa fa-info-circle chartform-icon',
                click: function () {
                    $(this).siblings('.chartform').slideToggle();
                    //alert('sdg');
                }
            });
            if (id != 'hrm;0') {
                var secondMenu = '<div class="chartform form">' +
                        '<a target="_blank" href="' + data.add + '" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= lang("add") ?></a>' +
                        '<a target="_blank" href="' + data.update + '" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i> <?= lang("update") ?></a>' +
                        '<a target="_blank" href="' + data.delete + '" onclick="return confirm(\'<?= lang("js_confirm_message") ?>\');" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> <?= lang("delete") ?></a>' +
                        '</div>';
                $node.append(secondMenuIcon).append(secondMenu);
            }
        }
    });
    $('.orgchart').on('click', function (event) {
        if (!$(event.target).is('.chartform-icon') && !$(event.target).is('.chartform *')) {
            $(this).find('.chartform').slideUp();
        }
    });

    $(function () {
        $('.chartoverlay').height($('.chart-container').height());
        $('.chartoverlay-result').height($('.chartoverlay').height());
    });
</script>
