<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<style type="text/css">
    .link, .link:hover{
        text-decoration: none !important;
        font-weight: bold !important;
    }
</style>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#department_list" data-toggle="tab"><?= lang('department_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_department"  data-toggle="tab"><?= lang('add_department') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Departement List tab Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="department_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered " id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th  width="2%" class="text-center"><?= lang('sl') ?></th>
                                        <th  width="15%" class="text-center"><?= lang('department_name') ?></th>
                                        <th  width="15%" class="text-center"><?= lang('branche') ?></th>
                                        <th  width="14%" class="text-center"><?= lang('administrator') ?></th>
                                        <th  width="14%" class="text-center"><?= lang('rep') ?></th>
                                        <th  width="22%" class="text-center"><?= lang('description') ?></th>
                                        <th  width="18%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($all_department_info)):$sl = 1; ?>
                                        <?php foreach ($all_department_info as $akey => $v_department_info) : ?>
                                            <?php if (!empty($v_department_info)): ?>
                                                <tr>
                                                    <td class="text-center"><?= $sl++; ?></td>
                                                    <td><?= ($lang == 'arabic') ? $all_dept_info[$akey]->department_name_ar : $all_dept_info[$akey]->department_name; ?></td>
                                                    <td>
                                                        <?php foreach ($braches_list as $bra): ?>
                                                            <?php
                                                            if ($bra->branche_id == $all_dept_info[$akey]->branche_id)
                                                                if ($lang == 'arabic')
                                                                    echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/branches/' . $bra->branche_id . '">' . $bra->branche_ar . '</a>';
                                                                else
                                                                    echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/branches/' . $bra->branche_id . '">' . $bra->branche_en . '</a>';
                                                            ?>
                                                        <?php endforeach; ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        foreach ($employee_list as $emp) {
                                                            if ($emp->employee_id == $all_dept_info[$akey]->employee_id)
                                                                echo ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        foreach ($employee_list as $emp) {
                                                            if ($emp->employee_id == $all_dept_info[$akey]->employee_rep_id)
                                                                echo ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en;
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?= ($lang == 'arabic') ? $all_dept_info[$akey]->department_desc_ar : $all_dept_info[$akey]->department_desc_en; ?></td>
                                                    <td>
                                                        <a data-original-title="<?= lang('view_details') ?>" href="#"  class="btn btn-success btn-xs"  data-placement="top"  data-toggle="modal" data-target="#myModal<?= $all_dept_info[$akey]->department_id ?>"><i class="fa fa-eye"></i> <?= lang('view_details') ?></a>
                                                        <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/organizational_structure/department_list/<?php echo $all_dept_info[$akey]->department_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" ><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                        <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/organizational_structure/delete_department/<?php echo $all_dept_info[$akey]->department_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Departement List tab Ends -->

                <!-- Modal For Departements Starts -->
                <?php if (!empty($all_department_info)): foreach ($all_department_info as $akey => $v_department_info) : ?>
                        <?php if (!empty($v_department_info)): ?>
                            <div class="modal fade" id="myModal<?= $all_dept_info[$akey]->department_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="font-size: 1.2em;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title text-center" id="myModalLabel"><?php echo($lang == 'arabic') ? $all_dept_info[$akey]->department_name_ar : $all_dept_info[$akey]->department_name; ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <!-- Job Place -->
                                            <b><?= lang('branche') ?> :</b>
                                            <p>
                                                <?php foreach ($braches_list as $bra): ?>
                                                    <?php
                                                    if ($bra->branche_id == $all_dept_info[$akey]->branche_id)
                                                        if ($lang == 'arabic')
                                                            echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/branches/' . $bra->branche_id . '">' . $bra->branche_ar . '</a>';
                                                        else
                                                            echo '<a target="_blank" class="link" href="' . base_url() . 'admin/organizational_structure/branches/' . $bra->branche_id . '">' . $bra->branche_en . '</a>';
                                                    ?>
                                                <?php endforeach; ?>
                                            </p>

                                            <!-- Administrator -->
                                            <b><?= lang('add_department_admin') ?> :</b>
                                            <p>
                                                <?php
                                                if (!empty($all_dept_info[$akey]->employee_id))
                                                    foreach ($employee_list as $emp) {
                                                        if ($emp->employee_id == $all_dept_info[$akey]->employee_id)
                                                            echo ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en;
                                                    } else
                                                    echo ($lang == 'arabic') ? 'لا يوجد' : 'no replacement';
                                                ?>
                                            </p>
                                            <!-- Administrator_rep -->
                                            <b><?= lang('add_department_admin_rep') ?> :</b>
                                            <p>
                                                <?php
                                                if (!empty($all_dept_info[$akey]->employee_rep_id))
                                                    foreach ($employee_list as $emp) {
                                                        if ($emp->employee_id == $all_dept_info[$akey]->employee_rep_id)
                                                            echo ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en;
                                                    } else
                                                    echo ($lang == 'arabic') ? 'لا يوجد' : 'no replacement';
                                                ?>
                                            </p>
                                            <b><?= lang('description') ?> :</b>
                                            <p>
                                                <?= ($lang == 'arabic') ? $all_dept_info[$akey]->department_desc_ar : $all_dept_info[$akey]->department_desc_en; ?>
                                            </p>
                                            <b><?= lang('designation') ?> :</b>
                                            <ul>
                                                <?php foreach ($v_department_info as $key => $v_department) : ?>
                                                    <?php if (!empty($v_department->designations)): ?>
                                                        <li>
                                                            <h5><?= ($lang == 'arabic') ? $v_department->designations_ar : $v_department->designations; ?></h5>
                                                            <p><?php echo ($lang == 'arabic') ? $v_department->designations_desc_ar : $v_department->designations_desc_en; ?></p>
                                                        </li>
                                                    <?php else : ?>
                                                        <li><?= lang('no_record_found') ?></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
                                            <a  href="<?php echo base_url() ?>admin/organizational_structure/department_list/<?php echo $all_dept_info[$akey]->department_id; ?>" class="btn btn-primary"><?= lang('edit') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <!-- Modal For Departements Ends -->


                <!-- Add Employee tab Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_department" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="panel-body">
                            <form  id="form_validation" action="<?php echo base_url() ?>admin/organizational_structure/save_department/<?php
                            if (!empty($department_info->department_id)) {
                                echo $department_info->department_id;
                            }
                            ?>" method="post" class="form-horizontal form-groups-bordered">

                                <!-- Action -->
                                <?php if (!empty($department_info->department_id)): ?>
                                    <input type="hidden" name="department_id" value="<?= $department_info->department_id ?>"/>
                                <?php endif; ?>

                                <!-- Name of Departement in arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('department_name_ar') ?> <span class="required"> *</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="department_name_ar" value="<?php
                                        if (!empty($department_info->department_name_ar)) {
                                            echo $department_info->department_name_ar;
                                        }
                                        ?>" class="form-control" required/>
                                    </div>
                                </div>

                                <!-- Name of Departement in english -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('department_name_en') ?> <span class="required"> *</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="department_name" value="<?php
                                        if (!empty($department_info->department_name)) {
                                            echo $department_info->department_name;
                                        }
                                        ?>" class="form-control" required/>
                                    </div>
                                </div>

                                <!-- Branches List -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('branche') ?><span class="required"> *</span></label>
                                    <div class="col-sm-5">
                                        <select name="branche_id" class="form-control" id="field-1" required="">
                                            <option value=""><?= lang('select_branche') ?> .....</option>
                                            <?php foreach ($braches_list as $bra): ?>
                                                <option value="<?= $bra->branche_id ?>"
                                                        <?php if (!empty($braches_list) && $bra->branche_id == @$department_info->branche_id) echo 'selected'; ?>>
                                                            <?= ($lang == 'arabic') ? $bra->branche_ar : $bra->branche_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Employee List -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('add_department_admin') ?></label>
                                    <div class="col-sm-5">
                                        <select name="employee_id" class="form-control" id="field-1">
                                            <option value=""><?= lang('select_employee') ?> .....</option>
                                            <?php foreach ($employee_list as $emp): ?>
                                                <option value="<?= $emp->employee_id ?>"
                                                        <?php if (!empty($department_info) && $emp->employee_id == $department_info->employee_id) echo 'selected'; ?>>
                                                            <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Employee List -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('add_department_admin_rep') ?></label>
                                    <div class="col-sm-5">
                                        <select name="employee_rep_id" class="form-control" id="field-1">
                                            <option value=""><?= lang('select_employee') ?> .....</option>
                                            <?php foreach ($employee_list as $emp): ?>
                                                <option value="<?= $emp->employee_id ?>"
                                                        <?php if (!empty($department_info) && $emp->employee_id == $department_info->employee_rep_id) echo 'selected'; ?>>
                                                            <?= ($lang == 'arabic') ? $emp->full_name_ar : $emp->full_name_en; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- Description in Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="department_desc_ar" class="form-control" rows="10"><?php if (!empty($department_info->department_desc_ar)) echo $department_info->department_desc_ar; ?></textarea>
                                    </div>
                                </div>

                                <!-- Description in english -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_en') ?></label>
                                    <div class="col-sm-5">
                                        <textarea name="department_desc_en" class="form-control" rows="10"><?php if (!empty($department_info->department_desc_en)) echo $department_info->department_desc_en; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group margin">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="sbtn" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var maxAppend = 0;
        $("#add_more").click(function () {
            if (maxAppend >= 9)
            {
                alert("Maximum 10 File is allowed");
            } else {
                var add_new = $('<div class="form-group">\n\
                <label for="field-1" class="col-sm-3 control-label">Add Designations <span class="required"> *</span></label>\n\
                    <div class="col-sm-5">\n\<input type="text" name="designations[]" value="<?php ?>" class="form-control" placeholder="Enter Your Designations"/>\n\
    </div>\n\
    <div class="col-sm-2">\n\
    <strong><a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;<?= lang('remove') ?></a></strong>\n\
    </div>');
                maxAppend++;
                $("#add_new").append(add_new);
            }
        });

        $("#add_new").on('click', '.remCF', function () {
            $(this).parent().parent().parent().remove();
        });
    });
</script>



