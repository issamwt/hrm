<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#branches_list" data-toggle="tab"><?= lang('branches_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_branche"  data-toggle="tab"><?= lang('add_branche') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="branches_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="10%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="25%" class="text-center"><?= lang('name') ?></th>
                                        <th width="50%" class="text-center"><?= lang('description') ?></th>
                                        <th width="15%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty(@$branches_list)):$sl = 1; ?>
                                        <?php foreach ($branches_list as $bra): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl++; ?></td>
                                                <td>
                                                    <?= ($lang == 'arabic') ? $bra->branche_ar : $bra->branche_en; ?>
                                                </td>
                                                <td>
                                                    <?= ($lang == 'arabic') ? $bra->branche_desc_ar : $bra->branche_desc_en; ?>
                                                </td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/organizational_structure/branches/<?= $bra->branche_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/organizational_structure/delete_branche/<?= $bra->branche_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_branche" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/organizational_structure/save_branche/<?= @$branche_info->branche_id; ?> " method="post" class="form-horizontal">

                                <!-- Id for Update -->
                                <?php if (!empty($branche_info->branche_id)): ?>
                                    <input type="hidden" name="branche_id"  class="form-control" id="field-1" value="<?= @$branche_info->branche_id; ?>"/>
                                <?php endif; ?>

                                <!-- Arabic Name -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('name_ar') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="branche_ar"  class="form-control" id="field-1" value="<?= @$branche_info->branche_ar; ?>" required=""/>
                                    </div>
                                </div>


                                <!-- English Name -->
                                <div class="form-group">
                                    <label for="field-2" class="col-sm-3 control-label"><?= lang('name_en') ?> <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="branche_en"  class="form-control" id="field-2" value="<?= @$branche_info->branche_en; ?>" required=""/>
                                    </div>
                                </div>


                                <!-- Description in Arabic -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_ar') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="branche_desc_ar" class="form-control" rows="10"><?= @$branche_info->branche_desc_ar; ?></textarea>
                                    </div>
                                </div>

                                <!-- Description in english -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('description_en') ?> </label>
                                    <div class="col-sm-5">
                                        <textarea name="branche_desc_en" class="form-control" rows="10"><?= @$branche_info->branche_desc_en; ?></textarea>
                                    </div>
                                </div>

                                <!-- lat -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('lat') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="lat" id="lat" class="form-control" id="field-1" readonly="" value="<?= @@$branche_info->lat ?>" />
                                    </div>
                                </div>

                                <!-- lng -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('lng') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="lng" id="lng" class="form-control" id="field-1" readonly="" value="<?= @@$branche_info->lng ?>" />
                                    </div>
                                </div>

                                <!-- zoom -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('zoom') ?></label>
                                    <div class="col-sm-5">
                                        <input type="number" min="2" name="zoom" id="zoom" class="form-control" id="field-1" readonly="" value="<?= (!empty(@$branche_info->zoom)) ? @$branche_info->zoom : 2; ?>" />
                                    </div>
                                </div>

                                <!-- map -->
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('map') ?></label>
                                    <div class="col-sm-5">
                                        <div id="map" style="width: 100%; height: 300px; border:1px solid #e9e9e9;" ></div>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>


<script>
    var geocoder;
    var map;
    var marker;
    var lat = <?= (!empty($branche_info->lat)) ? $branche_info->lat : '0.0'; ?>;
    var lng = <?= (!empty($branche_info->lng)) ? $branche_info->lng : '0.0'; ?>;
    var zoom = <?= (!empty($branche_info->zoom)) ? $branche_info->zoom : '2'; ?>;
    var init = {lat: lat, lng: lng};
    function initMap() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById("map"), {center: init, zoom: zoom, mapTypeId: google.maps.MapTypeId.ROADMAP});
        marker = new google.maps.Marker({position: init, map: map, animation: google.maps.Animation.DROP, });
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
        });
        google.maps.event.addListener(map, 'zoom_changed', function (event) {
            document.getElementById("zoom").value = map.zoom
        });

        function placeMarker(latLng) {
            document.getElementById('lat').value = latLng.lat();
            document.getElementById('lng').value = latLng.lng();
            if (marker) {
                marker.setPosition(latLng);
            } else {
                marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }
            map.panTo(latLng);
        }

    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq3C6MH2XjShVLm07FYuisLUV59LHyzNU&signed_in=true&language=<?= ($lang == 'english') ? 'en' : 'ar'; ?>&callback=initMap"></script>