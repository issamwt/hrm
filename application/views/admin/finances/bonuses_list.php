<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#bonuses_list" data-toggle="tab"><?= lang('bonuse_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_bonuse"  data-toggle="tab"><?= lang('add_bonuse') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="bonuses_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('description') ?></th>
										<th width="11%" class="text-center"><?= lang('bonuse_assurance') ?></th>
										<th width="11%" class="text-center"><?= lang('bonuse_guarantee') ?></th>
										<th width="11%" class="text-center"><?= lang('bonuse_departure') ?></th>
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($bonuses_info)): $sl = 1; ?>
                                        <?php foreach ($bonuses_info as $fin): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $fin->title_en : $fin->title_ar; ?></td>
												<td><?php if($fin->assurance == 1) {?> <input type="checkbox" checked disabled> <?php }else { ?><input type="checkbox" disabled> <?php } ?></td>
												<td><?php if($fin->guarantee == 1) {?> <input type="checkbox" checked disabled> <?php }else { ?><input type="checkbox" disabled> <?php } ?></td>
												<td><?php if($fin->departure == 1) {?> <input type="checkbox" checked disabled> <?php }else { ?><input type="checkbox" disabled> <?php } ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/finances/bonuses_list/<?php echo $fin->bonuse_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/finances/delete_bonuse/<?php echo $fin->bonuse_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_bonuse" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/finances/save_bonuse/<?php
                            if (!empty($finance_info->bonuse_id)) {
                                echo $finance_info->bonuse_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($finance_info->bonuse_id)): ?>
                                    <input type="hidden" name="bonuse_id"  class="form-control" id="field-1" value="<?=@$finance_info->bonuse_id;?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('bonuse_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?=@$finance_info->title_ar;?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('bonuse_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_en"  class="form-control" id="field-1" value="<?=@$finance_info->title_en;?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" ><?= lang('bonuse_assurance') ?></label>
                                    <div class="col-sm-5">
                                        <select class="form-control" name="assurance" id="soc_insur">
                                            <option><?= lang('select_bonus') ?>...</option>
                                            <option value="1" <?php
                                            if (!empty($finance_info->assurance)) {
                                                echo $finance_info->assurance == 1 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_yes') ?></option>
                                            <option value="0" <?php
                                            if (!empty($finance_info->assurance)) {
                                                echo $finance_info->assurance == 0 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_no') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" ><?= lang('bonuse_guarantee') ?></label>
                                    <div class="col-sm-5">
                                        <select class="form-control" name="guarantee" id="soc_insur">
                                            <option><?= lang('select_bonus') ?>...</option>
                                            <option value="1" <?php
                                            if (!empty($finance_info->guarantee)) {
                                                echo $finance_info->guarantee == 1 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_yes') ?></option>
                                            <option value="0" <?php
                                            if (!empty($finance_info->guarantee)) {
                                                echo $finance_info->guarantee == 0 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_no') ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" ><?= lang('bonuse_departure') ?></label>
                                    <div class="col-sm-5">
                                        <select class="form-control" name="departure" id="soc_insur">
                                            <option><?= lang('select_bonus') ?>...</option>
                                            <option value="1" <?php
                                            if (!empty($finance_info->departure)) {
                                                echo $finance_info->departure == 1 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_yes') ?></option>
                                            <option value="0" <?php
                                            if (!empty($finance_info->departure)) {
                                                echo $finance_info->departure == 0 ? 'selected' : '';
                                            }
                                            ?>><?= lang('bonuse_no') ?></option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('brief_ar') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($finance_info->brief_ar)) {
                                            echo $finance_info->brief_ar;
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('brief_en') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($finance_info->brief_en)) {
                                            echo $finance_info->brief_en;
                                        }
                                        ?>" />
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>