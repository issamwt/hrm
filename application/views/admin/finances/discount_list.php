<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#discount_list" data-toggle="tab"><?= lang('discount_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_discount"  data-toggle="tab"><?= lang('add_discount') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="discount_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('description') ?></th>
										<th width="23%" class="text-center"><?= lang('discount_value') ?></th>
      
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($discounts_info)): $sl = 1; ?>
                                        <?php foreach ($discounts_info as $ded): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $ded->discount_title_en : $ded->discount_title_ar; ?></td>
												<td><?php if($ded->discount_type == "percent") { echo $ded->discount_value.'<b>%</b>';}else {echo $ded->discount_value.'<b>&nbsp;'.lang('device').'</b>';}?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/finances/discount_list/<?php echo $ded->discount_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/finances/delete_discount/<?php echo $ded->discount_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_discount" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/finances/save_discount/<?php
                            if (!empty($discount_info->discount_id)) {
                                echo $discount_info->discount_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($discount_info->extra_work_id)): ?>
                                    <input type="hidden" name="discount_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($discount_info->discount_id)) {
                                        echo $discount_info->discount_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('discount_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="discount_title_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($discount_info->discount_title_ar)) {
                                            echo $discount_info->discount_title_ar;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('discount_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="discount_title_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($discount_info->discount_title_en)) {
                                            echo $discount_info->discount_title_en;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('discount_type') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                                                <select class="form-control" name="discount_type" id="discount_type">
                                                                <option><?= lang('discount_type') ?>...</option>
                                                                <option value="percent" <?php
                                                                if (!empty($discount_info->discount_type)) {
                                                                    echo $discount_info->discount_type == "percent" ? 'selected' : '';
                                                                }
                                                                ?>><?= lang('discount_type_percent') ?></option>
                                                                <option value="value" <?php
                                                                if (!empty($discount_info->discount_type)) {
                                                                    echo $discount_info->discount_type == "value" ? 'selected' : '';
                                                                }
                                                                ?>><?= lang('discount_type_value') ?></option>
                                                            </select>
                                                            </div>
                                </div>
					
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('discount_value') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="discount_value"  class="form-control" id="field-1" value="<?php
                                        if (!empty($discount_info->discount_value)) {
                                            echo $discount_info->discount_value;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                </div>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>