<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#advance_list" data-toggle="tab"><?= lang('advance_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_advance"  data-toggle="tab"><?= lang('add_advance') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="advance_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('description') ?></th>
      
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($advances_info)): $sl = 1; ?>
                                        <?php foreach ($advances_info as $ad): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $ad->title_en : $ad->title_ar; ?></td>
        
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/finances/advance_list/<?php echo $ad->advance_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/finances/delete_advance/<?php echo $ad->advance_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_advance" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/finances/save_advance/<?php
                            if (!empty($advance_info->advance_id)) {
                                echo $advance_info->advance_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($advance_info->extra_work_id)): ?>
                                    <input type="hidden" name="advance_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($advance_info->advance_id)) {
                                        echo $advance_info->advance_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('advance_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($advance_info->title_ar)) {
                                            echo $advance_info->title_ar;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('advance_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($advance_info->title_en)) {
                                            echo $advance_info->title_en;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>