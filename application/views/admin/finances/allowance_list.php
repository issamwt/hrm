<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#allowance_list" data-toggle="tab"><?= lang('allowance_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_allowance"  data-toggle="tab"><?= lang('add_allowance') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="allowance_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="27%" class="text-center"><?= lang('allowance_title_ar') ?></th>
                                        <th width="27%" class="text-center"><?= lang('allowance_title_en') ?></th>
										<th width="25%" class="text-center"><?= lang('allowance_value') ?></th>
                                        <th width="13%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($allowances_info)): $sl = 1; ?>
                                        <?php foreach ($allowances_info as $ded): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td style="direction: rtl"><?= $ded->allowance_title_ar; ?></td>
                                                <td style="direction: ltr"><?= $ded->allowance_title_en; ?></td>
												<td><?php if($ded->allowance_type == "percent") { echo $ded->allowance_value.'<b>%</b>';}else {echo $ded->allowance_value.'<b>&nbsp;'.lang('rial').'</b>';}?></td>

                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/finances/allowance_list/<?php echo $ded->allowance_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/finances/delete_allowance/<?php echo $ded->allowance_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_allowance" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/finances/save_allowance/<?php
                            if (!empty($allowance_info->allowance_id)) {
                                echo $allowance_info->allowance_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($allowance_info->extra_work_id)): ?>
                                    <input type="hidden" name="allowance_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($allowance_info->allowance_id)) {
                                        echo $allowance_info->allowance_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('allowance_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="allowance_title_ar"  class="form-control" id="field-1" value="<?=@$allowance_info->allowance_title_ar; ?>" required=""/>
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('allowance_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="allowance_title_en"  class="form-control" id="field-1" value="<?=@$allowance_info->allowance_title_en;?>" required=""/>
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('allowance_type') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <select class="form-control" name="allowance_type" required>
                                            <option value=""><?= lang('allowance_type') ?>...</option>
                                            <option value="percent" <?=(@$allowance_info->allowance_type == "percent" )? 'selected' : '';?>><?= lang('percent') ?></option>
                                            <option value="value" <?=(@$allowance_info->allowance_type == "value") ? 'selected' : ''; ?>><?= lang('value') ?></option>
                                        </select>
                                    </div>
                                </div>
					
								<div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('allowance_value') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="allowance_value"  class="form-control" id="field-1" value="<?=@$allowance_info->allowance_value; ?>" required=""/>
                                    </div>
                                </div>

                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                </div>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(function () {
        $('form').map(function () {
            $(this).validate({rules: {name: "required"}});
        });
    });
</script>