<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#extra_list" data-toggle="tab"><?= lang('extra_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_extra"  data-toggle="tab"><?= lang('add_extra') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="extra_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('description') ?></th>
                                        <th width="23%" class="text-center"><?= lang('work_value') ?> (%)</th>
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($works_info)): $sl = 1; ?>
                                        <?php foreach ($works_info as $work): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $work->title_en : $work->title_ar; ?></td>
                                                <td><?= $work->work_value; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/finances/extra_work_list/<?php echo $work->extra_work_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/finances/delete_extra_work/<?php echo $work->extra_work_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_extra" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/finances/save_extra_work/<?php
                            if (!empty($work_info->extra_work_id)) {
                                echo $work_info->extra_work_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($work_info->extra_work_id)): ?>
                                    <input type="hidden" name="extra_work_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($work_info->extra_work_id)) {
                                        echo $work_info->extra_work_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('extra_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($work_info->title_ar)) {
                                            echo $work_info->title_ar;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('extra_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($work_info->title_en)) {
                                            echo $work_info->title_en;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('work_value') ?> (%) <span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="number" step="any" name="work_value"  class="form-control" id="field-1" value="<?php
                                        if (!empty($work_info->work_value)) {
                                            echo $work_info->work_value;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>