<link href="<?php echo base_url() ?>asset/css/fullcalendar.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .d-box {
        height: 90px;
        background-color: #fff;
        padding: 10px;
        position: relative;
        border-radius: 2px;
    }

    .d-box-number {
        font-weight: bold;
        font-size: 1.4em;
    }

    .d-box-icon {
        width: 90px;
        height: 90px;
        background-color: #2980b9;
        position: absolute;
        top: 0px;
    <?=($lang=='english')?'right:0px;':'left:0px;';?> border-radius: 2px;
        text-align: center;
        color: #FFF !important;
        line-height: 90px;
        font-size: 45px;
    }

    .d-box-icon .fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
    }
</style>

<?php echo message_box('success'); ?>

<div class="dashboard row">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('total_employee') ?></div>
                        <div class="d-box-number"><?= count($all) ?></div>
                        <a href="<?= base_url() ?>admin/employee/employees"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon"><i class="fa fa-users"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('total_branches') ?></div>
                        <div class="d-box-number"><?= $branches ?></div>
                        <a href="<?= base_url() ?>admin/organizational_structure/branches"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #16a085"><i class="fa fa-globe"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('total_department') ?></div>
                        <div class="d-box-number"><?= $departments ?></div>
                        <a href="<?= base_url() ?>admin/organizational_structure/department_list"><?= lang('more_info') ?>
                            <i class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #9b59b6"><i class="fa fa-university"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('total_sections') ?></div>
                        <div class="d-box-number"><?= $sections ?></div>
                        <a href="<?= base_url() ?>admin/organizational_structure/designation_list"><?= lang('more_info') ?>
                            <i class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #e67e22"><i class="fa fa-building"></i></div>
                    </div>
                </div>
            </div>
            <br><br><br><br><br><br>

            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('job_titles') ?></div>
                        <div class="d-box-number"><?= $job_titles ?></div>
                        <a href="<?= base_url() ?>admin/settings/job_titles"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #f1c40f"><i class="fa fa-user"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('leave_category') ?></div>
                        <div class="d-box-number"><?= $leave_category ?></div>
                        <a href="<?= base_url() ?>admin/settings/leave_category"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #27ae60;"><i class="fa fa-child"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('irrigularity_category') ?></div>
                        <div class="d-box-number"><?= $irrigularity_category ?></div>
                        <a href="<?= base_url() ?>admin/settings/irrigularity_category"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #e74c3c"><i class="fa fa-bolt"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('job_places') ?></div>
                        <div class="d-box-number"><?= $job_places ?></div>
                        <a href="<?= base_url() ?>admin/settings2/job_places"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #3498db"><i class="fa fa-map-marker"></i></div>
                    </div>
                </div>
            </div>
            <br><br><br><br><br><br><br>

            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('employee_category') ?></div>
                        <div class="d-box-number"><?= $employee_category ?></div>
                        <a href="<?= base_url() ?>admin/settings2/employee_category"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #1abc9c"><i class="fa fa-star"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('evaluation_items') ?></div>
                        <div class="d-box-number"><?= $evaluation_items ?></div>
                        <a href="<?= base_url() ?>admin/settings2/evaluation_items"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #8e44ad;"><i class="fa fa-magic"></i></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-box">
                        <div class="d-box-text"><?= lang('approvals') ?></div>
                        <div class="d-box-number"><?= $approvals_cats ?></div>
                        <a href="<?= base_url() ?>admin/approvals/approvals_list"><?= lang('more_info') ?> <i
                                    class="fa fa-arrow-circle-right"></i></a>
                        <div class="d-box-icon" style="background-color: #34495e"><i class="fa fa-thumbs-up"></i></div>
                    </div>
                </div>
            </div>
            <br><br><br><br><br><br><br>
        </div>


        <div class="row">
            <div class="col-md-12">


                <!-- Main content -->
                <div class="content">
                    <div class="box box-primary">

                        <div class="box-body no-padding">
                            <p><b><?= lang('last_application') ?></b></p>
                            <div class="table-responsive mailbox-messages">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th class="text-center"><?= lang('id_app') ?></th>
                                        <th class="text-center"><?= lang('date_app') ?></th>
                                        <th class="text-center"><?= lang('employee_id') ?></th>
                                        <th class="text-center"><?= lang('employee_name') ?></th>
                                        <th class="text-center"><?= lang('app_type') ?></th>
                                        <th class="text-center"><?= lang('configure_approval') ?></th>
                                        <th class="text-center"><?= lang('status') ?></th>
                                        <th class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody style="font-size: 13px">
                                    <?php if (!empty($applications_list)):foreach ($applications_list as $app): ?>
                                        <tr>
                                            <td class="text-center"><?= $app->application_id ?></td>
                                            <td><?= $app->date ?></td>
                                            <td>
                                                <?php foreach ($all as $emp): ?>
                                                    <?php if ($emp->employee_id == $app->employee_id): ?>
                                                        <?= $emp->employment_id ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td>
                                                <?php foreach ($all as $emp): ?>
                                                    <?php if ($emp->employee_id == $app->employee_id): ?>
                                                        <?= ($lang == 'english') ? $emp->full_name_en : $emp->full_name_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td><?= lang($app->type_app) ?></td>
                                            <td>
                                                <?php foreach ($approval_cats as $ac): ?>
                                                    <?php if ($ac->approval_cat_id == $app->approval_cat_id): ?>
                                                        <?= ($lang == 'english') ? $ac->approval_cat_en : $ac->approval_cat_ar; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td class="text-center"><?= status($app->status) ?></td>
                                            <td class="text-center">
                                                <?php echo btn_view_modal('admin/approvals/view_application/' . $app->application_id) ?>
                                                <a data-original-title="<?= lang('delete') ?>"
                                                   href="<?php echo base_url() ?>admin/approvals/delete_application/<?= $app->application_id ?>"
                                                   class="btn btn-danger btn-xs" title="" data-toggle="tooltip"
                                                   data-placement="top"
                                                   onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i
                                                            class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                    </tbody>
                                </table><!-- /.table -->
                                <h5><a href="<?= base_url() ?>admin/approvals/applications"><?= lang('more_info') ?> <i
                                                class="fa fa-arrow-circle-right"></i></a></h5>
                            </div><!-- /.mail-box-messages -->
                        </div><!-- /.box-body -->
                    </div><!-- /. box -->
                </div>

            </div><!-- /.content-wrapper -->
        </div>


        <div class="row">
            <div class="col-md-12">

                <form method="post" action="<?php echo base_url() ?>admin/mailbox/delete_mail/inbox">
                    <!-- Main content -->
                    <div class="content">
                        <div class="box box-primary">

                            <div class="box-body no-padding">
                                <div class="mailbox-controls">
                                    <b><?= lang('recent_email') ?></b>
                                    <br><br>
                                    <!-- Check all button -->
                                    <div class="mail_checkbox">
                                        <input type="checkbox" id="parent_present">
                                    </div>
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                    </div><!-- /.btn-group -->
                                    <a href="#" onClick="history.go(0)" class="btn btn-default btn-sm"><i
                                                class="fa fa-refresh"></i></a>
                                    <a href="<?php echo base_url() ?>admin/mailbox/compose"
                                       class="btn btn-danger"><?= lang('compose') ?> +</a>
                                </div>
                                <br/>
                                <div class="table-responsive mailbox-messages">
                                    <table class="table table-hover table-striped" id="dataTables-example">
                                        <tbody style="font-size: 13px">
                                        <?php if (!empty($get_inbox_message)):foreach ($get_inbox_message as $v_inbox_msg): ?>
                                            <tr>
                                                <td><input class="child_present" type="checkbox" name="selected_id[]"
                                                           value="<?php echo $v_inbox_msg->inbox_id; ?>"/></td>

                                                <td class="mailbox-name"><a
                                                            href="<?php echo base_url() ?>admin/mailbox/read_inbox_mail/<?php echo $v_inbox_msg->inbox_id ?>">
                                                        <?= $v_inbox_msg->from; ?></a>
                                                </td>
                                                <td class="mailbox-subject" style="font-size:13px"><b
                                                            class="pull-left"><?php
                                                        $subject = (strlen($v_inbox_msg->subject) > 20) ? substr($v_inbox_msg->subject, 0, 15) . '...' : $v_inbox_msg->subject;
                                                        echo $subject;
                                                        ?> - &nbsp;</b> <span class="pull-left "> <?php
                                                        $body = (strlen($v_inbox_msg->message_body) > 40) ? substr($v_inbox_msg->message_body, 0, 40) . '...' : $v_inbox_msg->message_body;
                                                        echo $body;
                                                        ?></span></td>
                                                <td style="font-size:13px">
                                                    <?= $v_inbox_msg->message_time ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                            <tr>
                                                <td><strong><?= lang('nothing_to_display') ?></strong></td>
                                            </tr>
                                        <?php endif; ?>
                                        </tbody>
                                    </table><!-- /.table -->
                                    <h5><a href="<?= base_url() ?>admin/mailbox/inbox"><?= lang('more_info') ?> <i
                                                    class="fa fa-arrow-circle-right"></i></a></h5>
                                </div><!-- /.mail-box-messages -->
                            </div><!-- /.box-body -->
                        </div><!-- /. box -->
                    </div>
                </form>
            </div><!-- /.content-wrapper -->
        </div>


    </div>
</div>


<?php
function status($x)
{
    switch ($x) {
        case '0':
            echo '<span class="label label-warning">' . lang('pending') . '</span>';
            break;
        case '1':
            echo '<span class="label label-success">' . lang('accepted') . '</span>';
            break;
        case '2':
            echo '<span class="label label-danger">' . lang('rejected') . '</span>';
            break;
        default:
            echo '?';
            break;
    }
}

?>