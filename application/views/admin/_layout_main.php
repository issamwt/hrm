<?php $this->load->view('admin/components/header'); ?>
<body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('admin/components/user_profile'); ?>

        <?php $this->load->view('admin/components/navigation'); ?>
        <!-- Right side column. Contains the navbar and content of the page -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <h1>
                    <?php echo $page_header; ?>
                </h1>
                <ol class="breadcrumb">
                    <?php echo $this->breadcrumbs->build_breadcrumbs(); ?>
                </ol>
            </section>
            <section class="content">

                <?php echo $subview ?>
            </section>


        </div><!-- /.right-side -->
        <div class="control-sidebar-bg"></div>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">

            </div>
            جميع الحقوق محفوظة &copy; <a href="http://smartlives.ws/" style="color: #000" target="_blank">Smart Life</a>
        </footer>
    </div><!-- ./wrapper -->
    <?php $this->load->view('admin/_layout_modal'); ?>
    <?php $this->load->view('admin/_layout_modal_lg'); ?>
    <?php $this->load->view('admin/components/footer'); ?>
