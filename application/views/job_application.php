<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HR Lite | Rokn Al Hiwar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/font-icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/plugins/Hijri/jquery.calendars.islamic.css">
    <link rel="stylesheet" href="<?= base_url(); ?>asset/css/employee.css">
    <style type="text/css">
        body {
            background-image: url(<?=base_url()?>img/simple-gray-texture.jpg);
        }

        .container {
            border: 1px solid lightslategray;
            border-radius: 2px;
            margin: 100px auto;
            padding: 0px;
            background: #fff;
        }

        .container-header {
            padding: 10px;
            background: #398ed3;
            color: #fff;;
        }

        .rtl, .rtl * {
            direction: rtl;
        }

        .ltr, .ltr * {
            direction: ltr;
        }

        h3 {
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .container-body {
            padding: 60px 20px;
        }

        .col-sm-12 {
            margin: 10px auto;
        }
        .container-footer{
            padding: 20px;
            border-top: 1px solid lightslategray;
        }
        .help-block{
            color: red !important;
        }
    </style>
</head>
<body>
<!--<img src="img/logo2.jpg" style="margin: 10px">-->
<div class="container" style="">
    <form action="<?=base_url()?>job_application/save_application" class="form" id="form" method="post" enctype="multipart/form-data">
        <div class="container-header">
            <div class="row">
                <div class="col-xs-6 ltr"><h3>Job Application</h3></div>
                <div class="col-xs-6 rtl"><h3>طلب وظيفة</h3></div>
            </div>
        </div>
        <div class="container-body">
            <?php echo message_box('success'); ?>
            <?php echo message_box('error'); ?>
            <center><img src="" alt="" class="img-responsive"/></center>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Full name in Arabic</b></div>
                    <div class="col-sm-6"><input required type="text" name="full_name_ar" class="form-control rtl"></div>
                    <div class="col-sm-3 rtl"><b>الإسم كاملا بالعربي</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Full name in English</b></div>
                    <div class="col-sm-6"><input required type="text" name="full_name_en" class="form-control"></div>
                    <div class="col-sm-3 rtl"><b>الإسم كاملا بالإنجليزي</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Card ID Number</b></div>
                    <div class="col-sm-6"><input required type="text" name="id_no" class="form-control"></div>
                    <div class="col-sm-3 rtl"><b>رقم الهوية</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Card ID End Date</b></div>
                    <div class="col-sm-6"><input required type="text" name="id_no_end" class="form-control hijri_datepicker">
                    </div>
                    <div class="col-sm-3 rtl"><b>تاريخ نهاية الهوية</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Nationality</b></div>
                    <div class="col-sm-6">
                        <select class="form-control" name="nationality" required>
                            <option value=""></option>
                            <?php foreach ($countries as $country): ?>
                                <option value="<?= $country->idCountry ?>">
                                    <?= $country->countryName ?> | <?= $country->countryName_ar ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm-3 rtl"><b>الجنسية</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Gender</b></div>
                    <div class="col-sm-6">
                        <input required type="radio" name="sex" value="male"> Male | ذكر<br>
                        <input required type="radio" name="sex" value="female"> Female | أنثى
                        </select>
                    </div>
                    <div class="col-sm-3 rtl"><b>الجنس</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Date of Birth</b></div>
                    <div class="col-sm-6"><input required type="text" name="birth_date" class="form-control hijri_datepicker">
                    </div>
                    <div class="col-sm-3 rtl"><b>تاريخ الميلاد</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Mobile number</b></div>
                    <div class="col-sm-6"><input required type="text" name="phone" class="form-control"></div>
                    <div class="col-sm-3 rtl"><b>رقم الجوال</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Mobile number</b></div>
                    <div class="col-sm-6">
                        <input required type="radio" name="maratial_status" value="married"> Married | متزوج<br>
                        <input required type="radio" name="maratial_status" value="un_married"> Single | أعزب<br>
                        <input required type="radio" name="maratial_status" value="widowed"> Widowed | مطلق<br>
                        <input required type="radio" name="maratial_status" value="divorced"> Divorced | أرمل<br>
                    </div>
                    <div class="col-sm-3 rtl"><b>رقم الجوال</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Job Time</b></div>
                    <div class="col-sm-6">
                        <input required type="radio" name="job_time" value="full"> Full time | دوام كامل<br>
                        <input required type="radio" name="job_time" value="part"> part time | دوام جزئي<br>
                    </div>
                    <div class="col-sm-3 rtl"><b>نوع الدوام</b></div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-3"><b>Personal photo</b></div>
                    <div class="col-sm-6">
                        <div class="col-sm-3">
                            <div class="fileinput fileinput-new col-sm-9" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px">
                                    <img src="<?=base_url() . 'img/admin.png'; ?>" alt="Please Connect Your Internet">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px;">
                                </div>
                                <div class="btn btn-primary btn-file"><span class="fileinput-new" ><?= lang('select_file') ?></span>
                                    <div class="fileinput-exists"><?= lang('change') ?></div>
                                    <input type="file" name="photo" size="20" class="form-control" required>
                                </div>
                                <div class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput">
                                    <?= lang('remove') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 rtl"><b>صورة شخصية</b></div>
                </div>
            </div>
        </div>
        <div class="container-footer">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-2"><button type="submit" class="form-control btn btn-primary btn-block">SUBMIT\ تقديم</button></div>
                <div class="col-sm-2"><input type="reset" value="RESET\ إعادة" class="form-control btn btn-primary btn-block"></div>
                <div class="col-sm-4"></div>
            </div>
        </div>
    </form>
</div>


<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url(); ?>asset/js/jquery-1.10.2.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url() ?>asset/js/plugins/Hijri/jquery.plugin.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>asset/js/plugins/Hijri/jquery.calendars.islamic.en.js"
        type="text/javascript"></script>
<script src="<?php echo base_url() ?>asset/js/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>asset/js/jasny-bootstrap.min.js" type="text/javascript"></script>
<script src="http://localhost:8080/hrm_lite_local/asset/js/custom-validation.js" type="text/javascript"></script>
<script src="http://localhost:8080/hrm_lite_local/asset/js/jquery.validate.js" type="text/javascript"></script>



<script>
    $(function () {
        var calendar = $.calendars.instance('islamic');
        $('.hijri_datepicker').calendarsPicker({calendar: calendar});
    });
</script>
</body>
</html>
