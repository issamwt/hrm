<?php

session_start();

/**
 * Description of MY_Controller
 *
 * @author Trescoder
 */
class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('login_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('admin_model');
        $this->load->model('global_model');


        $lang = $this->session->userdata("lang") == null ? "english" : $this->session->userdata("lang");
        $this->lang->load($lang, $lang);
        $this->lang->load('file', $lang);
        $this->lang->load('employee', $lang);
        $uri1 = $this->uri->segment(1);
        $uri2 = $this->uri->segment(2);
        $uri3 = $this->uri->segment(3);

        if ($uri3) {
            $uri3 = '/' . $uri3;
        }
        $uriSegment = $uri1 . '/' . $uri2 . $uri3;
        $menu_uri['menu_active_id'] = $this->admin_model->select_menu_by_uri($uriSegment);
        $menu_uri['menu_active_id'] == false || $this->session->set_userdata($menu_uri);

        // Login check
        $exception_uris = array(
            'reminder_cronjob/reminder_test_period_hrm',
            'reminder_cronjob/reminder_test_period_dm',
            'reminder_cronjob/reminder_identity_end',
            'reminder_cronjob/reminder_passport_end',
            'reminder_cronjob/reminder_med_insurance_end',
            'reminder_cronjob/reminder_contract_end',
            'reminder_cronjob/reminder_employee_fixed',
            'job_application',
            'job_application/save_application',
            'cronjob2/aaa',
            'login',
            'login/index/1',
            'login/index/2',
            'login/logout',
            'edialogue'
        );
        if (in_array(uri_string(), $exception_uris) == FALSE) {
            if ($this->login_model->loggedin() == FALSE) {
                redirect('login');
            }
        }

        // check notififation status by where
        $where = array('notify_me' => '1', 'view_status' => '2');
        // check email notification status
        $this->admin_model->_table_name = 'tbl_inbox';
        $this->admin_model->_order_by = 'inbox_id';
        $data['total_email_notification'] = count($this->admin_model->get_by($where, FALSE));
        $data['email_notification'] = $this->admin_model->get_by($where, FALSE);

        // check notice notification status
        $this->admin_model->_table_name = 'tbl_notice';
        $this->admin_model->_order_by = 'notice_id';
        $data['total_notice_notification'] = count($this->admin_model->get_by($where, FALSE));

        $data['notice_notification'] = $this->admin_model->get_by($where, FALSE);





        // get all general settings info
        $this->admin_model->_table_name = "tbl_gsettings"; //table name
        $this->admin_model->_order_by = "id_gsettings";
        $info['genaral_info'] = $this->admin_model->get();

        date_default_timezone_set($info['genaral_info'][0]->timezone_name);

        $this->session->set_userdata($info);


    }

}
