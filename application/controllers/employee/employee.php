<?php

class Employee extends Employee_Controller
{

    public function __construct()
    {

        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        $this->emp_type = $this->session->userdata('emp_type');

        if ($this->emp_type == 'dep_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $this->employee_list = $this->emp_model->all_emplyee_info();
        }
    }

    public function edit_balance($id){
        $data = $this->input->post();
        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/vacation_balances');
    }

    public function changeimage($id){
        $data = array();
        if (!empty($_FILES['photo']['name'])) {
            $_FILES['photo']['name'] = $_FILES['photo']['name'];
            $val = $this->emp_model->uploadImage('photo');
            $data['photo'] = (!empty($val['path'])) ? $val['path'] : '';
        }


        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/profile');
    }

    public  function changepassword($id)
    {
        $data = $this->input->post();
        print_r($data);
        $this->emp_model->_table_name = "tbl_employee_login";
        $this->emp_model->_primary_key = "employee_login_id";
        $this->emp_model->_order_by = "employee_login_id";
        $login = $this->emp_model->get_by(array('employee_id'=>$id), true);
        $this->emp_model->save($data, $login->employee_login_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/profile');
    }

    public function check_employment_id($employment_id){

        $test = $this->db->select('*')->from('tbl_employee')->where('employment_id', $employment_id)->get()->result();

        if (count($test) > 0) {
            echo "error";
        }
        else{
            echo 'success';
        }
    }

    public function save_employee($id = NULL)
    {
        if ($this->emp_type != 'super_manager' and $this->emp_type != 'hr_manager')
            redirect('employee/dashboard');
        ///////////////////////////////////////////////////////////
        /////////////////////// prepare data /////////////////////
        ///////////////////////////////////////////////////////////
        $data = $this->input->post();

        $today = date('Y-m-d');
        $today = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $today = str_replace('-', '', $today);
        $passport_end = str_replace('-', '', $data['passport_end']);
        $identity_end = str_replace('-', '', $data['identity_end']);


        if ($id) {
            echo 'update<br>';
            $test = $this->db->select('*')->from('tbl_employee')->where('employment_id', $data['employment_id'])->where(array('employee_id !=' => $id))->get()->result();
        } else {
            echo 'add<br>';
            $test = $this->db->select('*')->from('tbl_employee')->where('employment_id', $data['employment_id'])->get()->result();
        }
        if (count($test) > 0) {
            $type = "error";
            $message = ($this->session->userdata('lang') == 'arabic') ? 'هوية الموظف مستخدمة. الرجاء إدخال رقم هوية آخر' : 'Employee ID is used. Please use another one';
            set_message($type, $message);
            redirect('employee/dashboard/employee_list');

        }

        if (!empty($data['fils_name'])) {
            $data['fils_name'] = implode(';', $data['fils_name']);
        }
        if (!empty($data['fils_name_ar'])) {
            $data['fils_name_ar'] = implode(';', $data['fils_name_ar']);
        }
        if (!empty($data['fils_birth'])) {
            $data['fils_birth'] = implode(';', $data['fils_birth']);
        }
        $data['reference'] = rand(1000, 9999);
        $data_login['user_name'] = $data['employment_id'];
        $data_login['password'] = $data['password'];
        $data_login['activate'] = $data['status'];
        unset($data['password']);
        ///////////////////////////////////////////////////////////
        /////////////////////// prepare files /////////////////////
        ///////////////////////////////////////////////////////////

        if (!empty($_FILES['id_proff']['name'])) {
            $_FILES['id_proff']['name'] = $_FILES['id_proff']['name'];
            $val = $this->emp_model->uploadAllType('id_proff');
            $data['id_proff'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['photo']['name'])) {
            $_FILES['photo']['name'] = $_FILES['photo']['name'];
            $val = $this->emp_model->uploadImage('photo');
            $data['photo'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        if (!empty($_FILES['cin_photo_path']['name'])) {
            $_FILES['cin_photo_path']['name'] = $_FILES['cin_photo_path']['name'];
            $val = $this->emp_model->uploadImage('cin_photo_path');
            $data['cin_photo_path'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        if (!empty($_FILES['passport_photo_path']['name'])) {
            $_FILES['passport_photo_path']['name'] = $_FILES['passport_photo_path']['name'];
            $val = $this->emp_model->uploadImage('passport_photo_path');
            $data['passport_photo_path'] = (!empty($val['path'])) ? $val['path'] : '';

        }
        if (!empty($_FILES['resume_path']['name'])) {
            $_FILES['resume_path']['name'] = $_FILES['resume_path']['name'];
            $val = $this->emp_model->uploadFile('resume_path');
            $data['resume_path'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['contract_paper_path']['name'])) {
            $_FILES['contract_paper_path']['name'] = $_FILES['contract_paper_path']['name'];
            $val = $this->emp_model->uploadFile('contract_paper_path');
            $data['contract_paper_path'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['other_document_path']['name'][0]) and $id) {
            $vals = $this->emp_model->multi_uploadAllType('other_document_path');
            $this->emp_model->_table_name = "tbl_other_document_path"; // table name
            $this->emp_model->_primary_key = "odp_id";
            if (!empty($vals)) {
                foreach ($vals as $val) {
                    $dop = array("employee_idodp" => $id, "filename" => $val['fileName']);
                    $this->emp_model->save($dop);
                }
            }
        }

        ///////////////////////////////////////////////////////////
        /////////////////////// save employee /////////////////////
        ///////////////////////////////////////////////////////////
        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_primary_key = "employee_id"; // $id
        if ($id) {
            $saved_id = $this->emp_model->save($data, $id);
        } else {
            $data['old_balance'] = 0;
            $data['new_balance'] = 0;
            $saved_id = $this->emp_model->save($data);
        }

        if ($saved_id) {
            $this->emp_model->_table_name = "tbl_employee_login";
            $this->emp_model->_primary_key = "employee_login_id";
            $this->emp_model->_order_by = "employee_login_id";
            $data_login['employee_id'] = $saved_id;
            /////////////////
            if ($id) {
            } else {
                $saved_id2 = $this->emp_model->save($data_login);
            }
            /////////////////
            if ($saved_id2) {
                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                redirect('employee/dashboard/employee_list');
            } else {
                $type = "error";
                $message = $this->db->_error_message();
                set_message($type, $message);
                redirect('employee/dashboard/employee_list');
            }
        } else {
            $type = "error";
            $message = $this->db->_error_message();
            set_message($type, $message);
            redirect('employee/dashboard/employee_list');
        }
    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function delete_other_doc($odp_id)
    {
        $this->emp_model->_table_name = "tbl_other_document_path"; // table name
        $this->emp_model->_primary_key = "odp_id";
        $this->emp_model->_order_by = "odp_id";
        $odp = $this->emp_model->get_by(array('odp_id' => $odp_id), true);
        $this->emp_model->delete($odp_id);

        if (file_exists(FCPATH . 'img/uploads/' . $odp->filename)) {
            unlink(FCPATH . 'img/uploads/' . $odp->filename);
        }

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/profile/' . $odp->employee_idodp);
    }

    public function finance_employee($id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('finance_employee');
        $data['menu'] = array("employee_list" => 1);
        $data['hijri_calendar'] = 'TRUE';

        $data['employee_info'] = $this->emp_model->all_emplyee_info($id);
        $data['managers'] = $this->get_direct_manager($data['employee_info']);

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance';
        $this->emp_model->_order_by = "allowance_type_id";
        $data['allowances_list'] = $this->emp_model->get_by(array('employe_id' => $id));
        $data['today'] = date('Y-m-d');

        $this->emp_model->_table_name = 'tbl_deduction_category';
        $this->emp_model->_order_by = "deduction_id";
        $data['deduction_category_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_deduction';
        $this->emp_model->_primary_key = "deduction_type_id";
        $data['deduction_list'] = $this->emp_model->get_by(array('employd_id' => $id));

        $this->emp_model->_table_name = 'tbl_provision_category';
        $this->emp_model->_order_by = "provision_category_id";
        $data['provision_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_provision';
        $this->emp_model->_primary_key = "provision_id";
        $data['provision_list'] = $this->emp_model->get_by(array('employp_id' => $id));


        $this->emp_model->_table_name = 'tbl_social_insurane';
        $this->emp_model->_order_by = "social_id";
        $data['social_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_order_by = "custody_id";
        $data['custodies_list'] = $this->emp_model->get_by(array('employee_id' => $id));

        $data['subview'] = $this->load->view('employee/finance_employee', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_allowance($id, $aid = NULL)
    {
        $data = $this->input->post();
        $data['employe_id'] = $id;

        $this->emp_model->_table_name = 'tbl_allowance';
        $this->emp_model->_order_by = "allowance_type_id";
        $this->emp_model->_primary_key = "allowance_type_id";
        if ($aid)
            $this->emp_model->save($data, $aid);
        else {
            $this->emp_model->save($data);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $id);
    }

    public function delete_allowance($aid, $id)
    {
        $this->emp_model->_table_name = 'tbl_allowance';
        $this->emp_model->_primary_key = "allowance_type_id";
        $this->emp_model->delete($aid);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $id);
    }

    public function save_finance_option($emp_id, $fo_id = NULL)
    {

        $data = $this->input->post();
        $data_emp['account_number'] = $data['account_number'];
        unset($data['account_number']);
        $data_emp['bank_name'] = $data['bank_name'];
        unset($data['bank_name']);
        $data_emp['branch_name'] = $data['branch_name'];
        unset($data['branch_name']);
        $data_emp['med_insur'] = $data['med_insur'];
        unset($data['med_insur']);
        if (!empty($data['med_insur_type'])) {
            $data_emp['med_insur_type'] = $data['med_insur_type'];
            unset($data['med_insur_type']);
        }
        $data_emp['social_insurance'] = $data['social_insurance'];
        unset($data['social_insurance']);
        if (!empty($data['social_insurance_type'])) {
            $data_emp['social_insurance_type'] = $data['social_insurance_type'];
            unset($data['social_insurance_type']);
        }
        $data_emp['retirement_date'] = $data['retirement_date'];
        unset($data['retirement_date']);
        $data['employef_id'] = $emp_id;

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->save($data_emp, $emp_id);

        $this->emp_model->_table_name = 'tbl_finance_info';
        $this->emp_model->_primary_key = "finance_info_id";
        if ($fo_id) {
            $this->emp_model->save($data, $fo_id);
        } else {
            $this->emp_model->save($data);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function save_deduction($emp_id)
    {
        $data = $this->input->post();
        $data['employd_id'] = $emp_id;

        $this->emp_model->_table_name = 'tbl_deduction';
        $this->emp_model->_primary_key = "deduction_type_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function delete_deduction($d_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_deduction';
        $this->emp_model->_primary_key = "deduction_type_id";
        $this->emp_model->delete($d_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function save_provisions($emp_id)
    {
        $data = $this->input->post();
        $data['employp_id'] = $emp_id;


        $this->emp_model->_table_name = 'tbl_provision';
        $this->emp_model->_primary_key = "provision_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function delete_provision($p_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_provision';
        $this->emp_model->_primary_key = "provision_id";
        $this->emp_model->delete($p_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function save_houcing($emp_id)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->save($data, $emp_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function save_custody($emp_id, $cust_id = NULL)
    {
        $data = $this->input->post();
        $data['employee_id'] = $emp_id;


        if (!empty($_FILES['document']['name'])) {
            $_FILES['document']['name'] = urlencode($_FILES['document']['name']);
            $val = $this->emp_model->uploadAllType('document');
            $data['document'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $this->emp_model->save($data, $cust_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function delete_custody($c_ic, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $this->emp_model->delete($c_ic);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function receipt_confirmation($c_ic, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $data['received'] = 1;
        $this->emp_model->save($data, $c_ic);

        $type = "success";
        $message = lang('receipt_confirmed_successfully');
        set_message($type, $message);
        redirect('employee/employee/finance_employee/' . $emp_id);
    }

    public function receipt_confirmation2($c_ic, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $data['received'] = 1;
        $this->emp_model->save($data, $c_ic);

        $type = "success";
        $message = lang('receipt_confirmed_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    public function get_direct_manager($data)
    {
        $all = $this->db->get('tbl_employee')->result();
        //get  direct_manager_id
        $datax['direct_manager_id'] = $data->direct_manager_id;

        foreach ($all as $emp) {
            if ($emp->employee_id == $datax['direct_manager_id']) {
                $datax['direct_manager_name_ar'] = $emp->full_name_ar;
                $datax['direct_manager_name_en'] = $emp->full_name_en;
            }
        }
        //get  human_resource_id
        $datax['human_resource_name_ar'] = lang('hr_admin');
        $datax['human_resourcen_name_en'] = lang('hr_admin');
        $datax['human_resource_id'] = 1;

        $flag=false;
        foreach ($all as $emp) {
            if ($emp->job_title == 4 and $flag==false) {
                $datax['human_resource_name_ar'] = $emp->full_name_ar;
                $datax['human_resourcen_name_en'] = $emp->full_name_en;
                $datax['human_resource_id'] = $emp->employee_id;
                $flag=true;
            }
        }
        return $datax;
    }

    //////////////////////////////////// MOUVEMENTS ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////


    public function mouvements($emp_id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('mouvements');
        $data['menu'] = array("employee_list" => 1);
        $data['hijri_calendar'] = 'TRUE';

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['employee_info'] = $this->emp_model->all_emplyee_info($emp_id);
        $data['managers'] = $this->get_direct_manager($data['employee_info']);

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['employee_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_embarkation';
        $this->emp_model->_order_by = "embarkation_id";
        $data['embarkations_list'] = $this->emp_model->get_by(array("employermb_id"=>$emp_id));

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->emp_model->get_by(array('employel_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_order_by = "advances_id";
        $data['advances'] = $this->emp_model->get_by(array('employea_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_caching';
        $this->emp_model->_order_by = "caching_id";
        $data['cachings'] = $this->emp_model->get_by(array('employec_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_recrutements';
        $this->emp_model->_order_by = "recrutement_id";
        $data['recrutements'] = $this->emp_model->get_by(array('employerc_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_extra_hours';
        $this->emp_model->_order_by = "extra_hours_id";
        $data['extra_hours_list'] = $this->emp_model->get_by(array('employexh_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_order_by = "custody_id";
        $data['custodies_list'] = $this->emp_model->get_by(array('employee_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_courses';
        $this->emp_model->_order_by = "course_id";
        $data['courses_list'] = $this->emp_model->get_by(array('employeetr_id' => $emp_id));

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_purchases';
        $this->emp_model->_order_by = "purchase_id";
        $data['purchase_list'] = $this->emp_model->get_by(array('employeepr_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_maintenaces';
        $this->emp_model->_order_by = "maintenance_id";
        $data['maintenace_list'] = $this->emp_model->get_by(array('employeemn_id' => $emp_id));

        $this->emp_model->_table_name = 'tbl_permissions';
        $this->emp_model->_order_by = "permission_id";
        $data['permissions_list'] = $this->emp_model->get_by(array('employeeprm_id' => $emp_id));

        $employee_info = $data['employee_info'];


        $data['subview'] = $this->load->view('employee/mouvements', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function leave_detail($leave_id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = "";

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['employee_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leave'] = $this->emp_model->get_by(array('leave_id' => $leave_id), TRUE);

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->emp_model->get();

        $data['modal_subview'] = $this->load->view('employee/leave_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function save_leave($emp_id)
    {
        $data = $this->input->post();
        $data['employel_id'] = $emp_id;

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }


        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $saved_id = $this->emp_model->save($data);

        if ($saved_id and $data['affect_stock'] == 1) {
            $this->emp_model->_table_name = 'tbl_employee';
            $this->emp_model->_order_by = "employee_id";
            $this->emp_model->_primary_key = "employee_id";
            $res = $this->emp_model->get_by(array('employee_id' => $emp_id), TRUE);

            $datax['new_balance'] = $res->new_balance + $data['duration'];
            $this->emp_model->save($datax, $emp_id);
        }
        /*
        echo $this->db->last_query();
        echo '<br>';
        echo $this->db->_error_message();
        */

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_leave($l_id, $emp_id)
    {

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_primary_key = "leave_id";
        $this->emp_model->delete($l_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_advance($emp_id)
    {
        $data = $this->input->post();
        $data['employea_id'] = $emp_id;
        $data['rest'] = $data['advance_value'];

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_order_by = "advances_id";
        $this->emp_model->save($data);

        echo '<hr>';
        echo $this->db->last_query();
        echo '<hr>';
        echo $this->db->_error_message();

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_advance($adv_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_primary_key = "advances_id";
        $this->emp_model->delete($adv_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function update_advance_page($adv_id, $emp_id)
    {
        $data['adv_id'] = $adv_id;
        $data['emp_id'] = $emp_id;
        $data['lang'] = $this->session->userdata('lang');

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_order_by = "advances_id";
        $data['advance'] = $this->emp_model->get_by(array('advances_id' => $adv_id), true);

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['employee_detail'] = $this->emp_model->get_by(array('employee_id' => $emp_id), true);

        $data['subview'] = $this->load->view('employee/update_advance_page', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function update_advance($adv_id)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_primary_key = "advances_id";
        $this->emp_model->save($data, $adv_id);

        $this->load->library('user_agent');
        $type = "success";
        $message = lang('updated_successfully');
        set_message($type, $message);

        redirect($this->agent->referrer());
    }

    public function save_caching($emp_id)
    {
        $data = $this->input->post();
        $data['employec_id'] = $emp_id;
        $today = date('Y-m-d');
        $data['caching_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_caching';
        $this->emp_model->_order_by = "caching_id";
        $this->emp_model->save($data);


        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_caching($c_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_caching';
        $this->emp_model->_primary_key = "caching_id";
        $this->emp_model->delete($c_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_custody2($emp_id, $cust_id = NULL)
    {
        $data = $this->input->post();
        $data['employee_id'] = $emp_id;


        if (!empty($_FILES['document']['name'])) {
            $_FILES['document']['name'] = urlencode($_FILES['document']['name']);
            $val = $this->emp_model->uploadAllType('document');
            $data['document'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $this->emp_model->save($data, $cust_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_custody2($c_ic, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_primary_key = "custody_id";
        $this->emp_model->delete($c_ic);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function transfer($emp_id)
    {
        $data = $this->input->post();
        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->save($data, $emp_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);

    }

    public function savetraining($emp_id, $tr_id = NULL)
    {
        $data = $this->input->post();
        $data['employeetr_id'] = $emp_id;

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_courses';
        $this->emp_model->_primary_key = "course_id";
        $this->emp_model->save($data, $tr_id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_training($tr_id, $emp_id)
    {

        $this->emp_model->_table_name = 'tbl_courses';
        $this->emp_model->_primary_key = "course_id";
        $this->emp_model->delete($tr_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_extra_hours($emp_id)
    {
        $data = $this->input->post();
        $data['employexh_id'] = $emp_id;

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_extra_hours';
        $this->emp_model->_primary_key = "extra_hours_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_extra_hours($xh_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_extra_hours';
        $this->emp_model->_primary_key = "extra_hours_id";
        $this->emp_model->delete($xh_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_purchase($emp_id)
    {
        $data = $this->input->post();
        $data['employeepr_id'] = $emp_id;
        $today = date('Y-m-d');
        $data['purchase_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_purchases';
        $this->emp_model->_primary_key = "purchase_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_purchase($pr_id, $emp_id)
    {

        $this->emp_model->_table_name = 'tbl_purchases';
        $this->emp_model->_primary_key = "purchase_id";
        $this->emp_model->delete($pr_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_maintenance($emp_id)
    {
        $data = $this->input->post();
        $data['employeemn_id'] = $emp_id;
        $today = date('Y-m-d');
        $data['maintenance_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_maintenaces';
        $this->emp_model->_primary_key = "maintenance_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_maintenance($mn_id, $emp_id)
    {

        $this->emp_model->_table_name = 'tbl_maintenaces';
        $this->emp_model->_primary_key = "maintenance_id";
        $this->emp_model->delete($mn_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_permission($emp_id)
    {
        $data = $this->input->post();
        $data['employeeprm_id'] = $emp_id;
        $today = date('Y-m-d');
        $data['permission_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_permissions';
        $this->emp_model->_primary_key = "permission_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_permission($prm_id, $emp_id)
    {

        $this->emp_model->_table_name = 'tbl_permissions';
        $this->emp_model->_primary_key = "permission_id";
        $this->emp_model->delete($prm_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_recrutement($emp_id)
    {
        $data = $this->input->post();
        $data['employerc_id'] = $emp_id;

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_recrutements';
        $this->emp_model->_primary_key = "recrutement_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_recrutement($rc_id, $emp_id)
    {
        $this->emp_model->_table_name = 'tbl_recrutements';
        $this->emp_model->_primary_key = "recrutement_id";
        $this->emp_model->delete($rc_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function save_embarkation($emp_id)
    {
        $data = $this->input->post();
        $data['employermb_id'] = $emp_id;

        if (!empty($_FILES['file']['name'])) {
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->emp_model->_table_name = 'tbl_embarkation';
        $this->emp_model->_primary_key = "embarkation_id";
        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_embarkation($emb_id, $emp_id){
        $this->emp_model->_table_name = 'tbl_embarkation';
        $this->emp_model->_primary_key = "embarkation_id";
        $this->emp_model->delete($emb_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/employee/mouvements/' . $emp_id);
    }

    public function delete_employee($emp_id)
    {
        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->delete($emp_id);

        $this->emp_model->_table_name = 'tbl_employee_login';
        $this->emp_model->_primary_key = "employee_login_id";
        $emp = $this->emp_model->get_by(array('employee_id'=>$emp_id), TRUE);
        $this->emp_model->delete($emp->employee_login_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/employee_list');
    }

}