
<?php

class Dashboard extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->load->model('mailbox_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        $this->direct_manager = $this->get_direct_manager($this->employee_details);
        $this->load->helper('ckeditor');
        $this->emp_type = $this->session->userdata('emp_type');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "100%",
                'height' => "350px",
                'language'=>($this->session->userdata('lang')=='arabic')?"ar":"en"
            )
        );
        date_default_timezone_set('Asia/Riyadh');
    }

    public function send_sms($receiver, $message)
    {
        $this->emp_model->_table_name = "tbl_sms_config"; //table name
        $this->emp_model->_primary_key = "sms_config_id";
        $this->emp_model->_order_by = "sms_config_id";

        $data = $this->emp_model->get(array('sms_config_id'=>1), TRUE);

        if($data->default==1)
        {
            $login = $data->sms_login1;
            $pass = $data->sms_password1;
            $tagname = $data->sender_name1;
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $tagname,
                'RecepientNumber' => $receiver,
                'Message' => $message."\n\n\n".base_url(),
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data);

            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
        }else{
            $login = $data->sms_login2;
            $pass = $data->sms_password2;
            $sender = $data->sender_name2;
            $message =  $message." \n\n\n".base_url();
            $numbers = $receiver;

            $_url = 'https://www.lanasms.net/api/sendsms.php';
            $postData = 'username='.$login.'&password='.$pass.'&message='.$message.'&numbers='.$numbers.'&sender='.$sender.'&unicode=E&return=Json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

            $output=curl_exec($ch);

            curl_close($ch);
        }
    }

    public function send_email($to, $message, $subject="HUMAN RESOURCES")
    {
        if($to){
            $this->load->library('email');
            //$send_email = $this->mail->sendEmail($from, $to, $subject, $message.'<br><a href="'.base_url().'">'.base_url().'</a>');
            $this->email->from('hr@edialogue.org', 'hr');
            $this->email->to($to);

            $this->email->subject($subject);
            $msg = ($this->session->userdata('lang')=='arabic')?'الموارد البشرية':'human resources';
            $this->email->message('<br>'.$message.'<br><br><br><br><br><a href="'.base_url().'">'.$msg.'</a><br><br>');
            $this->email->set_mailtype("html");

            $this->email->send();
        }
    }

    public function index()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 1);
        $data['title'] = lang('employee_panel');


        $data['employee_details'] = $this->employee_details;
        $data['managers'] = $this->direct_manager;

        $data['total_days'] = 0;
        $data['total_leave_applied'] = 0;
        $data['total_award_received'] = 0;

        //get all notice by flag
        $data['notice_info'] = $this->emp_model->get_all_notice(TRUE);

        $data['get_inbox_message'] = $this->emp_model->get_inbox_message($this->session->userdata('email'), $flag = NULL, $del_info = NULL, TRUE);

        $this->emp_model->_table_name = "tbl_extra_hours";
        $this->emp_model->_order_by = "extra_hours_id";
        $data['extra_hours'] = $this->emp_model->get_by(array('employexh_id' => $this->employee_id));

        $data['subview'] = $this->load->view('employee/main_content', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function get_direct_manager($data)
    {
        $all = $this->db->get('tbl_employee')->result();
        //get  direct_manager_id
        $datax['direct_manager_id'] = $data->direct_manager_id;

        foreach ($all as $emp) {
            if ($emp->employee_id == $datax['direct_manager_id']) {
                $datax['direct_manager_name_ar'] = $emp->full_name_ar;
                $datax['direct_manager_name_en'] = $emp->full_name_en;
            }
        }
        //get  human_resource_id
        $datax['human_resource_name_ar'] = lang('hr_admin');
        $datax['human_resourcen_name_en'] = lang('hr_admin');
        $datax['human_resource_id'] = 1;

        $flag=false;
        foreach ($all as $emp) {
            if ($emp->job_title == 4 and $flag==false) {
                $datax['human_resource_name_ar'] = $emp->full_name_ar;
                $datax['human_resourcen_name_en'] = $emp->full_name_en;
                $datax['human_resource_id'] = $emp->employee_id;
                $flag=true;
            }
        }

        return $datax;
    }

    public function administrative_affairs()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("administrative_affairs" => 1);
        $data['title'] = lang('administrative_affairs');

        $this->emp_model->_table_name = "tbl_allowance_category";
        $this->emp_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_categories'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_irrigularity_category';
        $this->emp_model->_order_by = "irrigularity_category_id";
        $data['irrigularity_categories'] = $this->emp_model->get();
        $today = date('Y-m-d');
        $today = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $filter =  '-'.explode('-', $today)[1].'-';

        $data['eva'] = $this->db->select('evaluation_id, emp_id')->from('tbl_evaluations')->where("created_date like '%".$filter."%'")->order_by('total', 'DESC')->limit(1)->get()->row();


        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['ideal_employee'] = $this->emp_model->ideal_employee(@$data['eva']->emp_id);


        $this->emp_model->_table_name = "tbl_center_documentations";
        $this->emp_model->_order_by = "doc_id";
        $data['document_list'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/administrative_affairs', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function contact_admin($send = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');

        if ($this->emp_type == 'dep_manager') {
            $employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $employee_list = $this->emp_model->all_emplyee_info();
        }

        if ($send) {
            if ($this->input->post('long_description') == '') {
                $type = "error";
                $message = lang('contact_admin_error');
                set_message($type, $message);
                redirect('employee/dashboard/contact_admin');
            } else {
                $this->emp_model->_table_name = 'tbl_notice';
                $this->emp_model->_order_by = "notice_id";
                $data = $this->input->post();
                $data['employee_id'] = $this->session->userdata('employee_id');
                $data['flag'] = 1;
                $today = date('Y-m-d');
                $data['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
                $data['view_status'] = 2;

                if($data['send_to']==0){
                    foreach ($employee_list as $emp){
                        if($emp->employee_id!=$this->session->userdata('employee_id')){
                            $data['to_all']=1;
                            $data['send_to'] = $emp->employee_id;
                            $this->emp_model->save($data);
                        }
                    }
                }
                else{
                    $this->emp_model->save($data);
                }

                $type = "success";
                $message = lang('contact_admin_success');
                set_message($type, $message);
                redirect('employee/dashboard/contact_admin');
            }
            return;
        }
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("contact_admin" => 1);
        $data['title'] = lang('contact_admin');
        $data['editor'] = $this->data;
        $data['managers'] = $this->direct_manager;

        if ($this->emp_type == 'dep_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $data['employee_list'] = $this->emp_model->all_emplyee_info();
        }

        $datax = $this->input->post();
        if(!empty($datax)){
            $data['reply_employee'] = $this->db->select('employee_id, full_name_en, full_name_ar')->from('tbl_employee')->where('employee_id',$datax['employee_id'])->get()->row();
        }


        $data['subview'] = $this->load->view('employee/contact_admin', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function all_notice()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("notices" => 1);
        $data['title'] = lang('notices');

        //$this->emp_model->_table_name = 'tbl_notice';
        //$this->emp_model->_order_by = "notice_id";
        //$data['notice_info'] = $this->emp_model->get_by(array('send_to' => $this->session->userdata('employee_id')));
        $data['notice_info'] = $this->db->select("*")->from("tbl_notice")->order_by("notice_id")->where('send_to', $this->session->userdata('employee_id'))->or_where('send_to', '0')->get()->result();

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['emps'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['emps'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/all_notice', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function notice_detail($id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = "Notice Details";

        $this->emp_model->_table_name = "tbl_notice"; // table name
        $this->emp_model->_order_by = "notice_id"; // $id
        $data['full_notice_details'] = $this->emp_model->get_by(array('notice_id' => $id,), TRUE);

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['emps'] = $this->emp_model->get();

        $data['modal_subview'] = $this->load->view('employee/notice_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function notice_readed($id)
    {
        $this->emp_model->_table_name = "tbl_notice"; // table name
        $this->emp_model->_order_by = "notice_id"; // $id
        $data['full_notice_details'] = $this->emp_model->get_by(array('notice_id' => $id,), TRUE);

        $newdata['view_status'] = 1;
        $this->emp_model->_table_name = "tbl_notice";
        $this->emp_model->_primary_key = "notice_id";
        echo $this->emp_model->save($newdata, $id);
    }

    public function delete_notice($id){
        $this->emp_model->_table_name = 'tbl_notice';
        $this->emp_model->_primary_key = "notice_id";
        $this->emp_model->delete($id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/all_notice/' . $id);
    }

    public function employee_list()
    {
        if ($this->emp_type == 'employee')
            redirect('employee/dashboard');

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('employee_list');
        $data['menu'] = array("employee_list" => 1);

        if ($this->emp_type == 'dep_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'), true);
        } elseif ($this->emp_type == 'sec_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'), true);
        } else {
            $data['employee_list'] = $this->emp_model->all_emplyee_info(NULL,true);
        }

        $data['subview'] = $this->load->view('employee/employee_list', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function employee_list2()
    {
        if ($this->emp_type == 'employee')
            redirect('employee/dashboard');

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('employee_list');
        $data['menu'] = array("employee_list" => 1);

        if ($this->emp_type == 'dep_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'), true);
        } elseif ($this->emp_type == 'sec_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'), true);
        } else {
            $data['employee_list'] = $this->emp_model->all_emplyee_info(NULL,true);
        }

        $data['subview'] = $this->load->view('employee/employee_list2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function add_employee($id = NULL)
    {
        if ($this->emp_type == 'employee')
            redirect('employee/dashboard');

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('add_employee');
        if ($id)
            $data['title'] = lang('edit_employee');
        $data['menu'] = array("employee_list" => 1);
        $data['hijri_calendar'] = 'TRUE';

        $this->db->select("employment_id");
        $this->db->from("tbl_employee");
        $data['employment_ids'] = $this->db->get()->result();

        $this->emp_model->_table_name = "countries";
        $this->emp_model->_order_by = "countryName";
        $data['all_countries'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_order_by = "employee_id";
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_places';
        $this->emp_model->_order_by = "job_place_id";
        $data['job_place_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        if ($id) {
            $data['employee_info'] = $this->emp_model->all_emplyee_info($id);
        }

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['subview'] = $this->load->view('employee/add_employee', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function get_all_emp()
    {
        $this->db->select("employee_id");
        $this->db->from("tbl_employee");
        $result = $this->db->get()->result();
        echo json_encode($result);
    }

    public function profile($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('view_profile');
        $employee_id = $this->session->userdata('employee_id');

        if ($id) {
            $data['employee_details'] = $this->emp_model->all_emplyee_info($id);
            $this->emp_model->_table_name = 'tbl_advances';
            $this->emp_model->_order_by = "advances_id";
            $data['advances'] = $this->emp_model->get_by(array('employea_id'=>$id));
            $this->emp_model->_table_name = 'tbl_courses';
            $this->emp_model->_order_by = "course_id";
            $data['courses_list'] = $this->emp_model->get_by(array('employeetr_id' =>$id));
            $this->emp_model->_table_name = "tbl_other_document_path";
            $this->emp_model->_order_by = "odp_id";
            $data['other_documents'] = $this->emp_model->get_by(array('employee_idodp' => $id));

        } else {
            $data['employee_details'] = $this->emp_model->all_emplyee_info($employee_id);
            $this->emp_model->_table_name = 'tbl_advances';
            $this->emp_model->_order_by = "advances_id";
            $data['advances'] = $this->emp_model->get_by(array('employea_id' => $employee_id));
            $this->emp_model->_table_name = 'tbl_courses';
            $this->emp_model->_order_by = "course_id";
            $data['courses_list'] = $this->emp_model->get_by(array('employeetr_id' =>$employee_id));
            $this->emp_model->_table_name = "tbl_other_document_path";
            $this->emp_model->_order_by = "odp_id";
            $data['other_documents'] = $this->emp_model->get_by(array('employee_idodp' => $employee_id));
        }

        $this->emp_model->_table_name = "tpl_evaluation_items";
        $this->emp_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->emp_model->get_by(array('department_id' => 0,'job_titles_id'=>$data['employee_details']->job_title));

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_order_by = "custody_id";
        $data['custodies_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance';
        $this->emp_model->_order_by = "allowance_type_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_deduction_category';
        $this->emp_model->_order_by = "deduction_id";
        $data['deduction_category_list'] = $this->emp_model->get();


        $this->emp_model->_table_name = 'tbl_deduction';
        $this->emp_model->_primary_key = "deduction_type_id";
        $data['deduction_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_provision_category';
        $this->emp_model->_order_by = "provision_category_id";
        $data['provision_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_provision';
        $this->emp_model->_primary_key = "provision_id";
        $data['provision_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $data['all'] = $this->db->get('tbl_employee')->result();

        $data['subview'] = $this->load->view('employee/user_profile', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function inbox()
    {
        $data['title'] = lang('inbox');
        $data['menu'] = array("mailbox" => 1, "inbox" => 1);
        $email = $this->session->userdata('email');

        $data['unread_mail'] = count($this->emp_model->get_inbox_message($email, TRUE));
        $data['get_inbox_message'] = $this->emp_model->get_inbox_message($email);
        $data['subview'] = $this->load->view('employee/inbox', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function read_inbox_mail($id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("mailbox" => 1, "inbox" => 1);
        $data['title'] = "Inbox Details";
        $this->emp_model->_table_name = 'tbl_inbox';
        $this->emp_model->_order_by = 'inbox_id';
        $data['read_mail'] = $this->emp_model->get_by(array('inbox_id' => $id), true);
        $this->emp_model->_primary_key = 'inbox_id';
        $updata['view_status'] = '1';
        $data['reply'] = 1;
        $this->emp_model->save($updata, $id);
        $data['subview'] = $this->load->view('employee/read_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function sent()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("mailbox" => 1, "sent" => 1);
        $data['title'] = lang('sent');
        $employee_id = $this->session->userdata('employee_id');
        $data['get_sent_message'] = $this->emp_model->get_sent_message($employee_id);
        $data['subview'] = $this->load->view('employee/sent', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function read_sent_mail($id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("mailbox" => 1, "sent" => 1);
        $data['title'] = lang('inbox');
        $this->emp_model->_table_name = 'tbl_sent';
        $this->emp_model->_order_by = 'sent_id';
        $data['read_mail'] = $this->emp_model->get_by(array('sent_id' => $id), true);
        $data['subview'] = $this->load->view('employee/read_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function draft()
    {
        $data['menu'] = array("mailbox" => 1, "draft" => 1);
        $data['title'] = lang('draft');
        $employee_id = $this->session->userdata('employee_id');
        $data['draft_message'] = $this->emp_model->get_draft_message($employee_id);
        $data['subview'] = $this->load->view('employee/draft', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function trash($action = NULL)
    {
        $data['menu'] = array("mailbox" => 1, "trash" => 1);
        $employee_id = $this->session->userdata('employee_id');
        if ($action == 'sent') {
            $data['title'] = lang('trash_sent_item');
            $data['trash_view'] = 'sent';
            $data['get_sent_message'] = $this->emp_model->get_sent_message($employee_id, TRUE);
        } elseif ($action == 'draft') {
            $data['title'] = lang('trash_draft_item');
            $data['trash_view'] = 'draft';
            $data['draft_message'] = $this->emp_model->get_draft_message($employee_id, TRUE);
        } else {
            $data['title'] = lang('trash_inbox_item');
            $data['trash_view'] = 'inbox';
            $data['get_inbox_message'] = $this->emp_model->get_inbox_message($employee_id, '', TRUE);
        }
        $data['subview'] = $this->load->view('employee/trash', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function compose($id = NULL, $reply = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('compose_new');
        $data['menu'] = array("mailbox" => 1, "inbox" => 1);
        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = 'employee_id';
        $data['get_employee_email'] = $this->emp_model->get_by(array('status' => '1'), FALSE);

        $data['editor'] = $this->data;
        if (!empty($reply)) {
            $data['inbox_info'] = $this->emp_model->check_by(array('inbox_id' => $id), 'tbl_inbox');
        } elseif (!empty($id)) {
            $this->emp_model->_table_name = 'tbl_draft';
            $this->emp_model->_order_by = 'draft_id';
            $data['get_draft_info'] = $this->emp_model->get_by(array('draft_id' => $id), TRUE);
        }
        $data['subview'] = $this->load->view('employee/compose_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function delete_mail($action, $from_trash = NULL, $v_id = NULL)
    {
        // get sellected id into inbox email page
        $selected_id = $this->input->post('selected_id', TRUE);
        if (!empty($selected_id)) { // check selected message is empty or not
            foreach ($selected_id as $v_id) {
                if (!empty($from_trash)) {
                    if ($action == 'inbox') {
                        $this->emp_model->_table_name = 'tbl_inbox';
                        $this->emp_model->delete_multiple(array('inbox_id' => $v_id));
                    } elseif ($action == 'sent') {
                        $this->emp_model->_table_name = 'tbl_sent';
                        $this->emp_model->delete_multiple(array('sent_id' => $v_id));
                    } else {

                        $this->emp_model->_table_name = 'tbl_draft';
                        $this->emp_model->delete_multiple(array('draft_id' => $v_id));
                    }
                } else {
                    $value = array('deleted' => 'Yes');
                    if ($action == 'inbox') {
                        $this->emp_model->set_action(array('inbox_id' => $v_id), $value, 'tbl_inbox');
                    } elseif ($action == 'sent') {
                        $this->emp_model->set_action(array('sent_id' => $v_id), $value, 'tbl_sent');
                    } else {
                        $this->emp_model->set_action(array('draft_id' => $v_id), $value, 'tbl_draft');
                    }
                }
            }
            $type = "success";
            $message = lang('deleted_successfully');
        } elseif (!empty($v_id)) {
            if ($action == 'inbox') {
                $this->emp_model->_table_name = 'tbl_inbox';
                $this->emp_model->delete_multiple(array('inbox_id' => $v_id));
            } elseif ($action == 'sent') {
                $this->emp_model->_table_name = 'tbl_sent';
                $this->emp_model->delete_multiple(array('sent_id' => $v_id));
            } else {
                $this->emp_model->_table_name = 'tbl_draft';
                $this->emp_model->delete_multiple(array('draft_id' => $v_id));
            }
            if ($action == 'inbox') {
                redirect('employee/dashboard/trash/inbox');
            } elseif ($action == 'sent') {
                redirect('employee/dashboard/trash/sent');
            } else {
                redirect('employee/dashboard/trash/draft');
            }
            $type = "success";
            $message = lang('deleted_successfully');
        } else {
            $type = "error";
            $message = lang('please_select_message');
        }
        set_message($type, $message);
        if ($action == 'inbox') {
            redirect('employee/dashboard/inbox');
        } elseif ($action == 'sent') {
            redirect('employee/dashboard/sent');
        } else {
            redirect('employee/dashboard/draft');
        }
    }

    public function delete_inbox_mail($id)
    {
        $value = array('deleted' => 'Yes');
        $this->emp_model->set_action(array('inbox_id' => $id), $value, 'tbl_inbox');
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/inbox');
    }

    public function send_mail($id = NULL){
        $data['lang'] = $this->session->userdata('lang');
        if(empty($this->input->post('message_body'))){
            $type = "error";
            $message = ($data['lang']=="english")?"Cant send empty message":"لا يمكن إرسال رسالة فارغة";
            set_message($type, $message);
            redirect('employee/dashboard/inbox');
        }
        $discard = $this->input->post('discard', TRUE);

        if (!empty($discard)) {
            redirect('employee/dashboard/inbox');
        }
        $all_email = $this->input->post('to', TRUE);
// get all email address
        foreach ($all_email as $v_email) {
            $data = $this->emp_model->array_from_post(array('subject', 'message_body'));
            if (!empty($_FILES['attach_file']['name'])) {
                $old_path = $this->input->post('attach_file_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->emp_model->uploadAllType('attach_file');
                $val == TRUE || redirect('employee/dashboard/compose');
// save into send table
                $data['attach_filename'] = $val['fileName'];
                $data['attach_file'] = $val['path'];
                $data['attach_file_path'] = $val['fullPath'];
// save into inbox table
                $idata['attach_filename'] = $val['fileName'];
                $idata['attach_file'] = $val['path'];
                $idata['attach_file_path'] = $val['fullPath'];
            }
            $data['to'] = $v_email;
            /*
             * Email Configuaration
             */
// get company name
            $name = $this->session->userdata('email');
            $info = $data['subject'];
// set from email
            $from = array($name, $info);
// set sender email
            $to = $v_email;
//set subject
            $subject = $data['subject'];
            $data['employee_id'] = $this->session->userdata('employee_id');
            $today = date('Y-m-d');
            $data['message_time'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $draf = $this->input->post('draf', TRUE);
            if (!empty($draf)) {
                $data['to'] = serialize($all_email);

// save into send
                $this->emp_model->_table_name = 'tbl_draft';
                $this->emp_model->_primary_key = 'draft_id';
                $this->emp_model->save($data, $id);
                redirect('employee/dashboard/inbox');
            } else {
// save into send
                $this->emp_model->_table_name = 'tbl_sent';
                $this->emp_model->_primary_key = 'sent_id';
                $send_id = $this->emp_model->save($data);
// get mail info by send id to send
                $this->emp_model->_order_by = 'sent_id';
                $data['read_mail'] = $this->emp_model->get_by(array('sent_id' => $send_id), true);
// set view page
                $view_page = $this->load->view('employee/read_mail', $data, TRUE);
                $this->load->library('mail');
                $send_email = $this->mail->sendEmail($from, $to, $subject, $view_page);

// save into inbox table procees
                $idata['to'] = $data['to'];
                $idata['from'] = $this->session->userdata('email');
                $idata['employee_id'] = $this->session->userdata('employee_id');
                $idata['subject'] = $data['subject'];
                $idata['message_body'] = $data['message_body'];
                $idata['message_time'] = date('Y-m-d H:i:s');

// save into inbox
                $this->emp_model->_table_name = 'tbl_inbox';
                $this->emp_model->_primary_key = 'inbox_id';
                $this->emp_model->save($idata);
            }
        }
        if ($send_email) {
            $type = "success";
            $message = lang('contact_admin_success');
            set_message($type, $message);
            redirect('employee/dashboard/sent');
        } else {
            show_error($this->email->print_debugger());
        }
    }

    public function restore($action, $id)
    {
        $value = array('deleted' => 'No');
        if ($action == 'inbox') {
            $this->emp_model->set_action(array('inbox_id' => $id), $value, 'tbl_inbox');
        } elseif ($action == 'sent') {
            $this->emp_model->set_action(array('sent_id' => $id), $value, 'tbl_sent');
        } else {
            $this->emp_model->set_action(array('draft_id' => $id), $value, 'tbl_draft');
        }
        if ($action == 'inbox') {
            redirect('employee/dashboard/inbox');
        } elseif ($action == 'sent') {
            redirect('employee/dashboard/sent');
        } else {
            redirect('employee/dashboard/draft');
        }
    }

    public function set_language($lang)
    {
        $this->session->set_userdata('lang', $lang);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function get_sec_with_dep($dep_id)
    {
        $this->emp_model->_table_name = "tbl_designations"; //table name
        $this->emp_model->_primary_key = "designations_id";
        $this->emp_model->_order_by = "designations_id";
        $result = $this->emp_model->get_by(array('department_id' => $dep_id));
        echo json_encode($result);
    }

    public function get_dep_with_brch($brch_id)
    {
        $this->emp_model->_table_name = "tbl_department"; //table name
        $this->emp_model->_primary_key = "department_id";
        $this->emp_model->_order_by = "department_id";
        $result = $this->emp_model->get_by(array('branche_id' => $brch_id));
        echo json_encode($result);
    }

    public function set_attendance()
    {
        $data = $this->input->post();
        $datax['action'] = $data['action'];
        $datax['employee_att_id'] = $this->employee_details->employee_id;
        $datax['employee_name'] = $this->employee_details->full_name_en;
        $datax['employee_status'] = $this->employee_details->status;
        $datax['job_code'] = $this->employee_details->employment_id;
        date_default_timezone_set('Asia/Riyadh');
        $today = date('Y-m-d');
        $datax['att_date'] = date_create_from_format('Y-m-d', $today)->format('Y/m/d');
        $datax['att_time'] = date('H:i:s', time());

        $this->emp_model->_table_name = "tbl_attendance";
        $this->emp_model->_primary_key = "attendance_id";
        $saved_id = $this->emp_model->save($datax);
        if ($saved_id) {
            $message = "";
            $type = "success";
            if ($data['action'] == 1)
                $message = lang('clock_in_message').' '.$datax['att_date'] .' '.$datax['att_time'];
            else
                $message = lang('clock_out_message').' '.$datax['att_date'] .' '.$datax['att_time'];
            set_message($type, $message);
            redirect('employee/dashboard');
        } else {
            $type = "error";
            $message = lang('clock_error_message').'<br>'.$this->db->_error_message();
            set_message($type, $message);
            redirect('employee/dashboard');
        }
    }

    public function set_extra_hours()
    {
        $data = $this->input->post();
        $datax['action'] = $data['action'];
        $datax['employee_xh_id'] = $this->employee_details->employee_id;
        $datax['employee_name'] = $this->employee_details->full_name_en;
        $datax['employee_status'] = $this->employee_details->status;
        $datax['job_code'] = $this->employee_details->employment_id;
        date_default_timezone_set('Asia/Riyadh');
        $today = date('Y-m-d');
        $datax['att_date'] = date_create_from_format('Y-m-d', $today)->format('Y/m/d');
        $datax['att_time'] = date('H:i:s', time());

        $this->emp_model->_table_name = "tbl_attendance_xh";
        $this->emp_model->_primary_key = "attendance_xh_id";
        $saved_id = $this->emp_model->save($datax);
        if ($saved_id) {
            $message = "";
            $type = "success";
            if ($data['action'] == 1)
                $message = lang('clock_in_message2').' '.$datax['att_date'] .' '.$datax['att_time'];
            else
                $message = lang('clock_out_message2').' '.$datax['att_date'] .' '.$datax['att_time'];
            set_message($type, $message);
            redirect('employee/dashboard');
        } else {
            $type = "error";
            $message = lang('clock_error_message').'<br>'.$this->db->_error_message();
            set_message($type, $message);
            redirect('employee/dashboard');
        }
    }

    public function vacation_balances(){
        if($this->emp_type =='employee')
            redirect('employee/dashboard');
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('vacation_balances');
        $data['menu'] = array("employee_list" => 1);


        if ($this->emp_type == 'dep_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $data['employee_list'] = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $data['employee_list'] = $this->emp_model->all_emplyee_info();
        }


        $data['subview'] = $this->load->view('employee/vacation_balances', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function reset_balance($emp_id, $rest=0){
        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->_order_by = "employee_id";
        $emp = $this->emp_model->get_by(array('employee_id'=>$emp_id), TRUE);

        $data['reset_old_rest']= $emp->old_rest;
        $data['reset_new_balance']= $emp->new_balance;
        $data['reset_old_balance'] = $emp->old_balance;

        $data['old_rest']=$rest;
        $data['new_balance']=0;
        $data['old_balance'] =$emp->new_balance;

        $this->emp_model->save($data, $emp_id);
        redirect('employee/dashboard/vacation_balances');
    }

    public function rereset_balance($emp_id){
        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_primary_key = "employee_id";
        $this->emp_model->_order_by = "employee_id";
        $emp = $this->emp_model->get_by(array('employee_id'=>$emp_id), TRUE);

        $data['old_rest']= $emp->reset_old_rest;
        $data['new_balance']= $emp->reset_new_balance;
        $data['old_balance'] = $emp->reset_old_balance;

        $this->emp_model->save($data, $emp_id);
        redirect('employee/dashboard/vacation_balances');
    }

    public function proccessadvance($id){
        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_order_by = "advances_id";
        $this->emp_model->_primary_key = "advances_id";
        $adv = $this->emp_model->get_by(array('advances_id' => $id), true);
        $data['rest'] = ($adv->rest)-($adv->monthly_installement);
        $data['payement_months'] = $adv->payement_months-1;
        if($data['rest']<$adv->monthly_installement)
            $data['monthly_installement'] = $data['rest'];
        if($data['rest']<=0)
        {
            $data['rest']=0;
            $data['payement_months']=0;
            $data['monthly_installement']=0;
        }
        $today = date('Y-m-d');
        $data['last_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $saved_id = $this->emp_model->save($data, $id);
        if($saved_id==$id){
            echo 'true';
        }
        else{
            echo 0;
        }
    }


    ////////////////////////////////////////////////////////////

    public function save_job_application($id)
    {
        $data = $this->emp_model->array_from_post(array('name', 'email', 'mobile', 'cover_letter'));
        // Resume File upload
        if (!empty($_FILES['resume']['name'])) {
            $val = $this->emp_model->uploadFile('resume');
            $val == TRUE || redirect('employee/dashboard/job_circular');
            $data['resume'] = $val['path'];
        }
        $data['job_circular_id'] = $id;
        $data['employee_id'] = $this->session->userdata('employee_id');
        $where = array('job_circular_id' => $id, 'employee_id' => $data['employee_id']);
        $check_existing_data = $this->emp_model->check_by($where, 'tbl_job_appliactions');
        if (!empty($check_existing_data)) {
            $type = "error";
            $message = "You Already Send This Application !";
        } else {
            $this->emp_model->_table_name = 'tbl_job_appliactions';
            $this->emp_model->_primary_key = 'job_appliactions_id';
            $this->emp_model->save($data);
// messages for user
            $type = "success";
            $message = "Application Information Successfully Submitted !";
        }
        set_message($type, $message);
        redirect('employee/dashboard');
    }

}