
<?php

class Applications extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        $this->direct_manager = $this->get_direct_manager($this->employee_details);
        $this->emp_type = $this->session->userdata('emp_type');
    }

    ///////// BEGIN HELPERS /////////

    public function send_sms($receiver, $message)
    {
        $this->emp_model->_table_name = "tbl_sms_config";
        $this->emp_model->_primary_key = "sms_config_id";
        $this->emp_model->_order_by = "sms_config_id";

        $data = $this->emp_model->get(array('sms_config_id'=>1), TRUE);

        if($data->default==1)
        {
            $login = $data->sms_login1;
            $pass = $data->sms_password1;
            $tagname = $data->sender_name1;
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $tagname,
                'RecepientNumber' => $receiver,
                'Message' => $message."\n\n\n".base_url(),
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data);

            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
        }else{
            $login = $data->sms_login2;
            $pass = $data->sms_password2;
            $sender = $data->sender_name2;
            $message =  $message." \n\n\n".base_url();
            $numbers = $receiver;

            $_url = 'https://www.lanasms.net/api/sendsms.php';
            $postData = 'username='.$login.'&password='.$pass.'&message='.$message.'&numbers='.$numbers.'&sender='.$sender.'&unicode=E&return=Json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

            $output=curl_exec($ch);

            curl_close($ch);
        }
    }

    public function send_email($to, $message, $subject="HUMAN RESOURCES")
    {
        if($to){
            $this->load->library('email');
            $this->email->from('hr@edialogue.org', 'hr');
            $this->email->to($to);

            $this->email->subject($subject);
            $msg = ($this->session->userdata('lang')=='arabic')?'الموارد البشرية':'human resources';
            $this->email->message('<br>'.$message.'<br><br><br><br><br><a href="'.base_url().'">'.$msg.'</a><br><br>');
            $this->email->set_mailtype("html");

            $this->email->send();
        }
    }

    public function get_direct_manager($data)
    {
        $all = $this->db->get('tbl_employee')->result();
        //get  direct_manager_id
        $datax['direct_manager_id'] = $data->direct_manager_id;

        foreach ($all as $emp) {
            if ($emp->employee_id == $datax['direct_manager_id']) {
                $datax['direct_manager_name_ar'] = $emp->full_name_ar;
                $datax['direct_manager_name_en'] = $emp->full_name_en;
            }
        }
        //get  human_resource_id
        $datax['human_resource_name_ar'] = lang('hr_admin');
        $datax['human_resourcen_name_en'] = lang('hr_admin');
        $datax['human_resource_id'] = 1;

        $flag=false;
        foreach ($all as $emp) {
            if ($emp->job_title == 4 and $flag==false) {
                $datax['human_resource_name_ar'] = $emp->full_name_ar;
                $datax['human_resourcen_name_en'] = $emp->full_name_en;
                $datax['human_resource_id'] = $emp->employee_id;
                $flag=true;
            }
        }

        return $datax;
    }

    public function get_sec_with_dep($dep_id)
    {
        $this->emp_model->_table_name = "tbl_designations";
        $this->emp_model->_primary_key = "designations_id";
        $this->emp_model->_order_by = "designations_id";
        $result = $this->emp_model->get_by(array('department_id' => $dep_id));
        echo json_encode($result);
    }

    ///////// END HELPERS /////////

    public function send_applications($id = NULL)
    {
        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("send_applications" => 1);
        $data['menu'] = array("applications" => 1);
        $data['title'] = lang('send_applications');
        $data['hijri_calendar'] = 'TRUE';

        if ($id == NULL) {
            $id = $this->session->userdata('employee_id');
        }

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->emp_model->get_by(array('employel_id' => $id));

        $data['employees_list'] = $this->db->where("status !=",2)->get("tbl_employee")->result();

        $data['employee_detail'] = $this->emp_model->all_emplyee_info($id);
        $data['managers'] = $this->get_direct_manager($data['employee_detail']);

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee_custody";
        $this->emp_model->_order_by = "custody_id";
        $data['custodies_list'] = $this->emp_model->get_by(array('employee_id' => $id), FALSE);

        $this->emp_model->_table_name = 'tbl_courses';
        $this->emp_model->_order_by = "course_id";
        $data['courses_list'] = $this->emp_model->get_by(array('employeetr_id' =>$id));

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $data['approvals_list'] = $this->emp_model->get_all_approvals();

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['subview'] = $this->load->view('employee/applications/send_applications', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_application($type)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_order_by = "sms_template_id";
        $sms = $this->emp_model->get_by(array('sms_template_id' => 1), TRUE);
        $email = $this->emp_model->get_by(array('sms_template_id' => 2), TRUE);

        $lang = $this->session->userdata('lang');
        if($lang=='arabic'){
            foreach ($email as $k=>$v){
                $email->{$k} = '<div style="text-align: right">'.$v.'</div>';
            }
        }

        if (!empty($_FILES['file']['name'])) {
            $_FILES['file']['name'] = $_FILES['file']['name'];
            $val = $this->emp_model->uploadAllType('file');
            $data['file'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        $data['type_app'] = $type;


        $today = date('Y-m-d');
        $data['date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        if (!empty($data['prod_name']))
            $data['prod_name'] = implode(';', $data['prod_name']);
        if (!empty($data['prod_desc']))
            $data['prod_desc'] = implode(';', $data['prod_desc']);
        if (!empty($data['prod_num']))
            $data['prod_num'] = implode(';', $data['prod_num']);
        if (!empty($data['prod_note']))
            $data['prod_note'] = implode(';', $data['prod_note']);

        $approval_cat = $this->db->select('all_list, accept, refuse, delegate, consultation')->from('tbl_approvals_cat')->where('approval_cat_id', $data['approval_cat_id'])->get()->row();

        $data['accept'] = $approval_cat->accept;
        $data['refuse'] = $approval_cat->refuse;
        $data['delegate'] = $approval_cat->delegate;
        $data['consultation'] = $approval_cat->consultation;

        $ids = explode(';', $approval_cat->all_list);
        $results = array_fill(0, count($ids), 0);
        $dm = $this->db->select('direct_manager_id')->from('tbl_employee')->where('employee_id', $data['employee_id'])->get()->row();

        $repeated=false;
        for ($i = 0; $i < count($ids); $i++) {
            if ($ids[$i] == 'dm'){
                if(in_array($dm->direct_manager_id, $ids)){
                    $repeated = ($repeated===false)?array_search($dm->direct_manager_id, $ids):$repeated;
                }
                $ids[$i] = $dm->direct_manager_id;
            }
        }

        if(is_numeric($repeated)){
            for ($i = 0; $i < count($ids); $i++){
                if($i==$repeated){
                    unset($ids[$i]);
                    $data['accept'] = explode(';', $data['accept']);unset($data['accept'][$i]);$data['accept'] = implode(';', $data['accept']);
                    $data['refuse'] = explode(';', $data['refuse']);unset($data['refuse'][$i]);$data['refuse'] = implode(';', $data['refuse']);
                    $data['delegate'] = explode(';', $data['delegate']);unset($data['delegate'][$i]);$data['delegate'] = implode(';', $data['delegate']);
                    $data['consultation'] = explode(';', $data['consultation']);unset($data['consultation'][$i]);$data['consultation'] = implode(';', $data['consultation']);
                    unset($results[$i]);
                }
            }
        }
        if($type=='la_advance' and $data['other_employee_id']!=0){
            array_unshift($ids,$data['other_employee_id']);
            array_unshift($results,0);
            $data['accept'] = '1;'.$data['accept'];
            $data['refuse'] = '1;'.$data['refuse'];
            $data['delegate'] = '2;'.$data['delegate'];
            $data['consultation'] = '2;'.$data['consultation'];
        }

        if($type=='la_vacations' and $data['replacement']!=0){
            array_unshift($ids,$data['replacement']);
            array_unshift($results,0);
            $data['accept'] = '1;'.$data['accept'];
            $data['refuse'] = '1;'.$data['refuse'];
            $data['delegate'] = '2;'.$data['delegate'];
            $data['consultation'] = '2;'.$data['consultation'];
        }

        $data['ids'] = implode(';', $ids);
        $data['results'] = implode(';', $results);
        $data['current'] = 0;

        $this->emp_model->_table_name = "tbl_applications";
        $this->emp_model->_primary_key = "application_id";
        $saved_id = $this->emp_model->save($data);

        if ($saved_id) {
            $message ="";
            try{
                error_reporting(0);
                $lang = $this->session->userdata('lang');
                $next_emp = $this->db->select('phone, email')->from('tbl_employee')->where('employee_id', explode(";",$data['ids'])[0])->get()->row();
                $this->send_sms(@$next_emp->phone, $sms->{$lang.'_new_app'});
                $this->send_email(@$next_emp->email, $email->{$lang.'_new_app'});
            }
            catch(Exception $e){
                $message .= $e->getMessage();
            }
            $type = "success";
            $message .= lang('app_sended');
            set_message($type, $message);
            redirect('employee/dashboard');
        } else {
            $type = "error";
            $message = $this->db->_error_message();
            set_message($type, $message);
            redirect('employee/dashboard');
        }
    }

    public function list_applications($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("applications" => 1);
        $data['title'] = lang('list_applications');

        if ($id == NULL) {
            $id = $this->session->userdata('employee_id');
        }

        $data['applications_list'] = $this->emp_model->get_applications_by_emp($id);

        $data['subview'] = $this->load->view('employee/applications/list_applications', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function received_applications()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("applications" => 1);
        $data['title'] = lang('received_applications');

        $result = $this->db->select("*")->from('tbl_applications')->get()->result();
        $array = [];

        if($this->emp_type!='hr_manager' and $this->emp_type!='super_manager'){
            foreach ($result as $r) {
                $ids = explode(';', $r->ids);
                if (in_array($this->session->userdata('employee_id'), $ids))
                    array_push($array, $r);
            }
            $data['applications_list'] = $array;
        }else{
            $data['applications_list'] = $result;
        }
        
        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_order_by = "employee_id";
        $data['all'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_approvals_cat";
        $this->emp_model->_order_by = "approval_cat_id";
        $data['approval_cats'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/applications/received_applications', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_application($id)
    {
        $data['view_application'] = $this->emp_model->get_applications_by_id($id);

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("list_applications" => 1);

        $this->emp_model->_table_name = "tbl_leave_category";
        $this->emp_model->_order_by = "leave_category_id";
        $data['all_leave_category'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee";
        $this->emp_model->_order_by = "employee_id";
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_designations';
        $this->emp_model->_order_by = "designations_id";
        $data['designations_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_application_notes';
        $this->emp_model->_order_by = "note_id";
        $data['app_notes'] = $this->emp_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('employee/applications/view_application', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function view_application2($id)
    {
        $data['view_application'] = $this->emp_model->get_applications_by_id($id);

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("list_applications" => 1);

        $this->emp_model->_table_name = "tbl_leave_category"; // table name
        $this->emp_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_designations';
        $this->emp_model->_order_by = "designations_id";
        $data['designations_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_application_notes';
        $this->emp_model->_order_by = "note_id";
        $data['app_notes'] = $this->emp_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('employee/applications/view_application2', $data, FALSE);
        $this->load->view('admin/_layout_main2', $data);
    }

    public function view_application3($id)
    {
        $data['view_application'] = $this->emp_model->get_applications_by_id($id);

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("list_applications" => 1);

        $this->emp_model->_table_name = "tbl_leave_category"; // table name
        $this->emp_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_designations';
        $this->emp_model->_order_by = "designations_id";
        $data['designations_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_application_notes';
        $this->emp_model->_order_by = "note_id";
        $data['app_notes'] = $this->emp_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('employee/applications/view_application3', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function view_application4($id)
    {
        $data['view_application'] = $this->emp_model->get_applications_by_id($id);

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("list_applications" => 1);


        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_application_notes';
        $this->emp_model->_order_by = "note_id";
        $data['app_notes'] = $this->emp_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('employee/applications/view_application4', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function view_application5($id)
    {
        $data['view_application'] = $this->emp_model->get_applications_by_id($id);

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("list_applications" => 1);

        $this->emp_model->_table_name = "tbl_leave_category"; // table name
        $this->emp_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_job_titles';
        $this->emp_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_designations';
        $this->emp_model->_order_by = "designations_id";
        $data['designations_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_employee_category';
        $this->emp_model->_order_by = "id";
        $data['emp_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_extra_work';
        $this->emp_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_application_notes';
        $this->emp_model->_order_by = "note_id";
        $data['app_notes'] = $this->emp_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('employee/applications/view_application5', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function update_advance_app($app_id)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = 'tbl_applications';
        $this->emp_model->_primary_key = "application_id";
        $saved_id= $this->emp_model->save($data, $app_id);
        if($saved_id){
            $type = "success";
            $message = lang('saved_successfully');
            set_message($type, $message);
            redirect('employee/applications/received_applications');
        }
        redirect('employee/applications/received_applications');
    }

    public function delete_application($id){
        $this->emp_model->_table_name = 'tbl_applications';
        $this->emp_model->_primary_key = "application_id";
        $this->emp_model->delete($id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/applications/received_applications/' . $id);
    }

    public function delete_application2($id){
        $this->emp_model->_table_name = 'tbl_applications';
        $this->emp_model->_primary_key = "application_id";
        $this->emp_model->delete($id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/applications/list_applications');
    }

    public function app_handling($app_id)
    {
        $lang = $this->session->userdata('lang');
        $datax = $this->input->get();

        //sms_templates
        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_order_by = "sms_template_id";
        $sms = $this->emp_model->get_by(array('sms_template_id' => 1), TRUE);
        $email = $this->emp_model->get_by(array('sms_template_id' => 2), TRUE);

        if($lang=='arabic'){
            foreach ($email as $k=>$v){
                $email->{$k} = '<div style="text-align: right">'.$v.'</div>';
            }
        }

        // application_notes
        $datan['application_id'] = $app_id;
        $datan['employee_id'] = $this->session->userdata('employee_id');
        $datan['action'] = $datax['action'];
        $datan['note_reason'] = $datax['note_reason'];
        $this->emp_model->_table_name = "tbl_application_notes";
        $this->emp_model->_primary_key = "note_id";
        $this->emp_model->save($datan);
        ///////////////
        $this->emp_model->_table_name = "tbl_applications";
        $this->emp_model->_order_by = "application_id";
        $app = $this->emp_model->get_by(array('application_id' => $app_id), TRUE);
        $current = $app->current;
        $results = explode(';', $app->results);

        if (($datax['action'] == 1 or $datax['action'] == 2 or $datax['action'] == 5) and (count($results) - 1 != $current)) {
            $next_id = explode(';', $app->ids)[$current + 1];
            //get next employee
            $next_emp = $this->db->select('phone, email, employee_id')->from('tbl_employee')->where('employee_id', $next_id)->get()->row();
        }

        // ACCEPT ////////////////////////////////////////
        if ($datax['action'] == 1) {
            $results[$app->current] = 1;
            $dataa['results'] = implode(';', $results);
            $dataa['current'] = $current + 1;

            $the_emp = $this->db->select('full_name_ar,full_name_en, phone, email')->from('tbl_employee')->where('employee_id', $app->employee_id)->get()->row();
            $the_hr_admin = $this->db->select('full_name_ar,full_name_en, phone, email')->from('tbl_employee')->where('job_title', 4)->get()->row();

            if (count($results) - 1 != $current) {
                $this->send_sms(@$next_emp->phone, $sms->{$lang.'_new_app'});
                $this->send_email(@$next_emp->email, $email->{$lang.'_new_app'}, lang('application').lang($app->type_app));

                $current_emp = $this->emp_model->emplyee_name($this->session->userdata('employee_id'));

                $this->send_sms(@$the_emp->phone, sprintf($sms->{$lang.'_accepted_app_from'},$current_emp));
                $this->send_email(@$the_emp->email, sprintf($email->{$lang.'_accepted_app_from'},$current_emp), lang('application').lang($app->type_app).' - '.lang('note_accept'));

                $this->send_sms(@$the_hr_admin->phone, sprintf($sms->{$lang.'_accepted_app_from'},$current_emp));
                $this->send_email(@$the_hr_admin->email, sprintf($email->{$lang.'_accepted_app_from'},$current_emp), lang('application').lang($app->type_app).' - '.lang('note_accept'));
            }


            if (count($results) - 1 == $current){
                $this->send_sms(@$the_emp->phone, $sms->{$lang.'_final_acception'});
                $this->send_email(@$the_emp->email, $email->{$lang.'_final_acception'}, lang('application').lang($app->type_app).' - '.lang('note_accept'));

                $this->send_sms(@$the_hr_admin->phone, sprintf($sms->{$lang.'_final_acception_of'}, ($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en));
                $this->send_email(@$the_hr_admin->email, sprintf($email->{$lang.'_final_acception_of'}, ($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en), lang('application').lang($app->type_app).' - '.lang('note_accept'));
                $dataa['status'] = 1;

            }


            $this->emp_model->_table_name = "tbl_applications";
            $this->emp_model->_primary_key = "application_id";
            $saved_id = $this->emp_model->save($dataa, $app_id);

            if ($saved_id) {
                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                if($dataa['status'] == 1)
                {$this->execute_app($app_id);}
                redirect('employee/applications/received_applications');
            } else {
                show_error($this->email->print_debugger());
            }
        }
        // REFUSE ////////////////////////////////////////
        if ($datax['action'] == 2) {
            $results[$app->current] = 2;
            $dataa['results'] = implode(';', $results);
            $dataa['current'] = $current + 1;
            $dataa['status'] = 2;

            $current_emp = $this->emp_model->emplyee_name($this->session->userdata('employee_id'));

            $the_emp = $this->db->select('full_name_en, full_name_ar, phone, email')->from('tbl_employee')->where('employee_id', $app->employee_id)->get()->row();

            $this->send_sms(@$the_emp->phone, sprintf($sms->{$lang.'_refuse_app_from'}, $current_emp));
            $this->send_email(@$the_emp->email, sprintf($email->{$lang.'_refuse_app_from'}, $current_emp), lang('application').lang($app->type_app).' - '.lang('note_refuse'));
            $the_hr_admin = $this->db->select('full_name_ar, phone, email')->from('tbl_employee')->where('job_title', 4)->get()->row();

            $msgfa = sprintf($sms->{$lang.'_refuse_of_from'},($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en,$current_emp);
            $msgfae = sprintf($email->{$lang.'_refuse_of_from'},($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en,$current_emp);
            $this->send_sms(@$the_hr_admin->phone, $msgfa);
            $this->send_email(@$the_hr_admin->email, $msgfae, lang('application').lang($app->type_app).' - '.lang('note_refuse'));


            $this->emp_model->_table_name = "tbl_applications";
            $this->emp_model->_primary_key = "application_id";
            $saved_id = $this->emp_model->save($dataa, $app_id);

            if ($saved_id) {
                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                redirect('employee/applications/received_applications');
            } else {
                show_error($this->email->print_debugger());
            }
        }
        // DELEGETE ////////////////////////////////////////
        if ($datax['action'] == 3) {
            $ids = explode(';', $app->ids);
            $ids[$current] = $datax['emp_id'];
            $dataa['ids'] = implode(';', $ids);
            $this->emp_model->_table_name = "tbl_applications";
            $this->emp_model->_primary_key = "application_id";
            $saved_id = $this->emp_model->save($dataa, $app_id);

            if ($saved_id) {
                $next_emp = $this->db->select('phone, email, employee_id, full_name_ar, full_name_en')->from('tbl_employee')->where('employee_id', $datax['emp_id'])->get()->row();

                if (count($results) - 1 != $current) {
                    $this->send_sms(@$next_emp->phone,$sms->{$lang.'_new_app'});
                    $this->send_email(@$next_emp->email,$email->{$lang.'_new_app'}, lang('application').lang($app->type_app));
                }

                $current_emp = $this->emp_model->emplyee_name($this->session->userdata('employee_id'));
                $the_emp = $this->db->select('phone, email')->from('tbl_employee')->where('employee_id', $app->employee_id)->get()->row();
                $the_hr_admin = $this->db->select('full_name_ar, phone, email')->from('tbl_employee')->where('job_title', 4)->get()->row();

                $msgd = sprintf($sms->{$lang.'_delegate_of_from'}, $current_emp, ($lang=="arabic")?$next_emp->full_name_ar:$next_emp->full_name_en);
                $msgde = sprintf($email->{$lang.'_delegate_of_from'}, $current_emp, ($lang=="arabic")?$next_emp->full_name_ar:$next_emp->full_name_en);
                $this->send_sms(@$the_emp->phone, $msgd);
                $this->send_email(@$the_emp->email, $msgde, lang('application').lang($app->type_app).' - '.lang('note_delegate'));
                $this->send_sms(@$the_hr_admin->phone, $msgd);
                $this->send_email(@$the_hr_admin->email, $msgde, lang('application').lang($app->type_app).' - '.lang('note_delegate'));

                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                redirect('employee/applications/received_applications');
            } else {
                show_error($this->email->print_debugger());
            }
        }
        // CONSULTATION ////////////////////////////////////////
        if ($datax['action'] == 4) {
            $dataa['title'] = ($lang == 'english') ? 'Consultation on employee application' : 'إستشارة بخصوص طلب موظف';
            $link = base_url() . 'employee/applications/view_application2/' . $app_id;
            $link_name = ($lang == 'english') ? 'FOLLOW THIS THE LINK' : 'رابط الطلب';
            $dataa['long_description'] = '<a href="' . $link . '" target="_blank" style=" width: 200px;background: #337AB7;padding: 6px 20px;text-decoration: none;font-weight: bold;color: #fff;border-radius: 2px;">' . $link_name . '</a><br><br>';
            $dataa['long_description'] .= ($lang == 'english') ? 'Please reply as soon as possible' : 'الرجاء الرد في أقرب وقت';
            $dataa['employee_id'] = $this->employee_id;
            $dataa['send_to'] = $datax['emp2_id'];
            $dataa['sugg_or_compl'] = 4;
            $dataa['flag'] = 1;
            $today = date('Y-m-d');
            $dataa['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

            $this->emp_model->_table_name = "tbl_notice";
            $this->emp_model->_primary_key = "notice_id";
            $saved_id = $this->emp_model->save($dataa);

            if ($saved_id) {
                $next_emp = $this->db->select('phone, email, employee_id, full_name_ar, full_name_en')->from('tbl_employee')->where('employee_id', $datax['emp2_id'])->get()->row();
                if (count($results) - 1 != $current) {
                    $this->send_sms(@$next_emp->phone, $sms->{$lang.'_new_app'});
                    $this->send_email(@$next_emp->email, $email->{$lang.'_new_app'}, lang('application').lang($app->type_app).' - '.lang('note_consultation'));
                }

                $current_emp = $this->emp_model->emplyee_name($this->session->userdata('employee_id'));
                $the_emp = $this->db->select('phone, email')->from('tbl_employee')->where('employee_id', $app->employee_id)->get()->row();
                $the_hr_admin = $this->db->select('full_name_ar, phone, email')->from('tbl_employee')->where('job_title', 4)->get()->row();

                $msgc = sprintf($sms->{$lang.'_consultation_of_from'}, $current_emp, ($lang=="arabic")?$next_emp->full_name_ar:$next_emp->full_name_en);
                $msgce = sprintf($email->{$lang.'_consultation_of_from'}, $current_emp, ($lang=="arabic")?$next_emp->full_name_ar:$next_emp->full_name_en);

                $this->send_sms(@$the_emp->phone, $msgc);
                $this->send_email(@$the_emp->email, $msgce, lang('application').lang($app->type_app).' - '.lang('note_consultation'));
                $this->send_sms(@$the_hr_admin->phone, $msgc);
                $this->send_email(@$the_hr_admin->email, $msgce, lang('application').lang($app->type_app).' - '.lang('note_consultation'));

                $type = "success";
                $message = lang('sended_successfully');
                set_message($type, $message);
                redirect('employee/applications/received_applications');
            } else {
                show_error($this->email->print_debugger());
            }
        }
        // PASS ////////////////////////////////////////
        if ($datax['action'] == 5) {

            $results[$app->current] = 1;
            $dataa['results'] = implode(';', $results);
            $dataa['current'] = $current + 1;

            $current_emp = $this->emp_model->emplyee_name($this->session->userdata('employee_id'));
            $the_emp = $this->db->select('full_name_ar, full_name_en, phone, email')->from('tbl_employee')->where('employee_id', $app->employee_id)->get()->row();
            $the_hr_admin = $this->db->select('full_name_ar, phone, email')->from('tbl_employee')->where('job_title', 4)->get()->row();

            $msgp = sprintf($sms->{$lang.'_pass_from'}, $current_emp);
            $msgpe = sprintf($email->{$lang.'_pass_from'}, $current_emp);

            $this->send_sms(@$the_emp->phone, $msgp);
            $this->send_email(@$the_emp->email, $msgpe, lang('application').lang($app->type_app));

            $this->send_sms(@$the_hr_admin->phone, $msgp);
            $this->send_email(@$the_hr_admin->email, $msgpe, lang('application').lang($app->type_app));

            if (count($results) - 1 != $current) {
                $this->send_sms(@$next_emp->phone, $sms->{$lang.'_new_app'});
                $this->send_email(@$next_emp->email, $email->{$lang.'_new_app'});
            }

            if (count($results) - 1 == $current){

                $x8 = 'Your Application  was Accepted';
                $y8 = 'السلام عليكم ورحمة الله وبركاته. تمت الموافقة على طلبك';
                $this->send_sms(@$the_emp->phone, $sms->{$lang.'_final_acception'});
                $this->send_email(@$the_emp->email, $email->{$lang.'_final_acception'}, lang('application').lang($app->type_app).' - '.lang('note_accept'));

                $msgfa = sprintf($sms->{$lang.'_final_acception_of'}, ($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en);
                $msgfae = sprintf($email->{$lang.'_final_acception_of'}, ($lang=='arabic')?$the_emp->full_name_ar:$the_emp->full_name_en);

                $this->send_sms(@$the_hr_admin->phone, $msgfa);
                $this->send_email(@$the_hr_admin->email, $msgfae, lang('application').lang($app->type_app).' - '.lang('note_accept'));
                $dataa['status'] = 1;
            }

            $this->emp_model->_table_name = "tbl_applications";
            $this->emp_model->_primary_key = "application_id";
            $saved_id = $this->emp_model->save($dataa, $app_id);

            if ($saved_id) {
                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                if($dataa['status'] == 1)
                {$this->execute_app($app_id);}
                redirect('employee/applications/received_applications');
            } else {
                show_error($this->email->print_debugger());
            }
        }
    }

    public function execute_app($app_id){
        $this->emp_model->_table_name = "tbl_applications";
        $this->emp_model->_order_by = "application_id";
        $app = $this->emp_model->get_by(array('application_id'=>$app_id), TRUE);

        $id = $app->employee_id;
        $data['file'] = (@$app->file)?@$app->file:null;
        $data['app_id'] = $app_id;

        // la_vacations
        if ($app->type_app=='la_vacations'){
            $this->emp_model->_table_name = "tbl_leave_category";
            $this->emp_model->_order_by = "leave_category_id";
            $leave_category = $this->emp_model->get_by(array('leave_category_id'=>$app->type), TRUE);

            $data['leave_category_id'] = $app->type;
            $data['employel_id'] = $app->employee_id;
            $data['duration'] = $app->leave_duration;
            $data['quota'] = $leave_category->leave_quota;
            $data['affect_stock'] = $leave_category->affect_balance;
            $data['calc_type'] = $leave_category->calc_type;
            $data['replacement'] = $app->replacement;
            $data['leave_tel'] = $app->tel_in_leave;
            $data['address_in_leave'] = $app->address_in_leave;
            $data['going_date'] = $app->start_date;
            $data['coming_date'] = $app->end_date;
            $this->emp_model->_table_name = "tbl_leaves";
            $this->emp_model->_primary_key = "leave_id";
            $app = $this->emp_model->save($data);

            $this->emp_model->_table_name = 'tbl_employee';
            $this->emp_model->_order_by = "employee_id";
            $this->emp_model->_primary_key = "employee_id";
            $res = $this->emp_model->get_by(array('employee_id'=>$id), TRUE);

            if($data['affect_stock']==1)
                $datax['new_balance'] = $res->new_balance+$data['duration'];
            $this->emp_model->save($datax , $id);
        }

        //la_advance
        elseif($app->type_app=='la_advance'){
            $today = date('Y-m-d');
            $data['advance_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $data['employea_id'] = $app->employee_id;
            $data['advance_type_id'] = $app->type;
            $data['advance_voucher'] = ($app->other_employee_id!=0)?$app->other_employee_id:Null;
            $data['advance_value'] = $app->value;
            $data['rest'] = $app->value;
            $data['payement_method'] = $app->payement_method;
            $data['monthly_installement'] = $app->monthly_installement;
            $data['payement_months'] = $app->payement_months;
            $this->emp_model->_table_name = 'tbl_advances';
            $this->emp_model->_primary_key = "advances_id";
            $this->emp_model->save($data);
        }


        //la_caching
        elseif($app->type_app=='la_caching'){
            $today = date('Y-m-d');
            $data['caching_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $data['employec_id'] = $app->employee_id;

            $data['name'] = $app->name;
            $data['cahing_type'] = $app->type;
            $data['bill_number'] = $app->bill_number;
            $data['item_no'] = $app->item_no;
            $data['value'] = $app->value;
            $data['bank_name'] = $app->bank_name;
            $data['account_holder_name'] = $app->account_holder_name;
            $data['phone'] = $app->phone;
            $data['country'] = $app->country;
            $data['account_number'] = $app->account_number;
            $data['address_in_leave'] = $app->address_in_leave;
            $data['swift_code'] = $app->swift_code;
            $data['city'] = $app->city;
            $data['caching_reason'] = $app->caching_reason;

            $this->emp_model->_table_name = 'tbl_caching';
            $this->emp_model->_primary_key = "caching_id";
            $this->emp_model->save($data);
        }

        //la_transfer
        elseif($app->type_app=='la_transfer'){
            unset($data['file']);
            unset($data['app_id']);
            $id = $app->employee_id;
            $data['job_title'] = $app->new_jt;
            $data['departement_id'] = $app->new_dep;
            $data['designations_id'] = $app->new_sect;
            $data['employee_category_id'] = $app->new_emp_cat;
            $data['job_time'] = $app->new_jtme;
            $this->emp_model->_table_name = 'tbl_employee';
            $this->emp_model->_primary_key = "employee_id";
            $this->emp_model->save($data, $id);
        }

        //la_training
        elseif($app->type_app=='la_training'){
            $data['employeetr_id'] = $app->employee_id;
            $data['course_name'] = $app->name;
            $data['course_institute_name'] = $app->course_institute;
            $data['start_date'] = $app->start_date;
            $data['course_price'] = $app->value;
            $data['course_note'] = $app->note;
            $this->emp_model->_table_name = 'tbl_courses';
            $this->emp_model->_primary_key = "course_id";
            $this->emp_model->save($data);
        }

        // la_overtime
        elseif($app->type_app=='la_overtime'){
            $data['extra_work_id'] = $app->type;
            $data['num_hours'] = $app->other_employee_id;
            $data['start_time'] = $app->start_date;
            $data['employexh_id'] = $app->employee_id;
            $this->emp_model->_table_name = "tbl_extra_hours";
            $this->emp_model->_primary_key = "extra_hours_id";
            $this->emp_model->save($data);
        }

        //la_purchase
        elseif($app->type_app=='la_purchase'){
            echo '<h1>ksjdfh</h1>';
            $prod_name = explode(';', @$app->prod_name);
            $prod_desc = explode(';', @$app->prod_desc);
            $prod_num = explode(';', @$app->prod_num);
            $prod_note = explode(';', @$app->prod_note);
            for($i=0;$i<count($prod_name);$i++){
                if(!empty($prod_name[$i])){
                    $data['employeepr_id'] = $app->employee_id;
                    $data['prod_name'] = $prod_name[$i];
                    $data['prod_desc'] = $prod_desc[$i];
                    $data['prod_num'] = $prod_num[$i];
                    $data['prod_note'] = $prod_note[$i];
                    $today = date('Y-m-d');
                    $data['purchase_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

                    $this->emp_model->_table_name = "tbl_purchases";
                    $this->emp_model->_primary_key = "purchase_id";
                    $this->emp_model->save($data);

                }
            }
        }

        // la_maintenance
        elseif($app->type_app=='la_maintenance'){
            $data['employeemn_id'] = $app->employee_id;
            $data['maintenance_title'] = $app->name;
            $data['maintenance_notes'] = $app->note;
            $today = date('Y-m-d');
            $data['maintenance_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

            $this->emp_model->_table_name = "tbl_maintenaces";
            $this->emp_model->_primary_key = "maintenance_id";
            $this->emp_model->save($data);
        }

        // la_permission
        elseif($app->type_app=='la_permission'){
            $data['employeeprm_id'] = $app->employee_id;
            $data['permission_type'] = $app->type;
            $today = date('Y-m-d');
            $data['permission_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $data['permission_star'] = $app->start_date;
            $data['permission_end'] = $app->end_date;
            $data['reason'] = $app->note;

            $this->emp_model->_table_name = "tbl_permissions";
            $this->emp_model->_primary_key = "permission_id";
            $this->emp_model->save($data);
        }

        // la_recrutement
        elseif($app->type_app=='la_recrutement'){
            $data['employerc_id'] = $app->employee_id;
            $data['course_institute'] = $app->course_institute;
            $data['name'] = $app->name;
            $data['type'] = $app->type;
            $data['address_in_leave'] = $app->address_in_leave;
            $data['going_date'] = $app->going_date;
            $data['coming_date'] = $app->coming_date;
            $data['leave_duration'] = $app->leave_duration;
            $data['note'] = $app->note;

            $this->emp_model->_table_name = "tbl_recrutements";
            $this->emp_model->_primary_key = "recrutement_id";
            $this->emp_model->save($data);
        }

        // la_embarkation
        if ($app->type_app=='la_embarkation'){
            $data['employermb_id'] = $app->employee_id;
            $data['date1'] = $app->coming_date;
            $data['date2'] = $app->coming_date_2;
            $data['rest'] = $app->leave_duration;
            $data['note'] = $app->note;

            $this->emp_model->_table_name = "tbl_embarkation";
            $this->emp_model->_primary_key = "embarkation_id";
            $this->emp_model->save($data);
        }
    }

}
?>