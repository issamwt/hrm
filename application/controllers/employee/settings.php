<?php

class Settings extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        if ($this->session->userdata('emp_type') != 'hr_manager' and $this->session->userdata('emp_type') != 'super_manager')
            redirect('employee/dashboard');
        $this->emp_type = $this->session->userdata('emp_type');
        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "100%",
                'height' => "500px",
                'language'=>($this->session->userdata('lang')=='arabic')?"ar":"en"
            )
        );

    }

    public function index()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('settings');
        $data['editor'] = $this->data;

        $this->emp_model->_table_name = "tbl_gsettings";
        $this->emp_model->_order_by = "id_gsettings";
        $data['settings'] = $this->emp_model->get_by(array('id_gsettings' => 1), true);

        $this->emp_model->_table_name = "tbl_department";
        $this->emp_model->_order_by = "department_id";
        $data['deps'] = $this->emp_model->get();

        foreach ($data['deps'] as $key => $value) {
            $this->emp_model->_table_name = "tbl_employee";
            $this->emp_model->_order_by = "employee_id";
            $data['deps'][$key]->employees = $this->emp_model->get_by(array('departement_id'=>$value->department_id,'status !='=>2));
        }

        $data['all'] = $this->db->where("status !=",2)->get("tbl_employee")->result();

        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_order_by = "sms_template_id";
        $data['sms'] = $this->emp_model->get_by(array('sms_template_id' => 1), TRUE);

        $data['subview'] = $this->load->view('employee/settings', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_settings()
    {
        $data = $this->input->post();
        $this->emp_model->_table_name = "tbl_gsettings";
        $this->emp_model->_primary_key = "id_gsettings";
        $saved_id = $this->emp_model->save($data, 1);
        if ($saved_id == 1) {
            $type = "success";
            $message = lang('saved_successfully');
            set_message($type, $message);
            redirect('employee/settings');
        } else {
            $type = "error";
            $message = 'Error !';
            set_message($type, $message);
            redirect('employee/settings');
        }
    }

    public function save_attendance()
    {
        echo '<h1>FILES:</h1><pre>';
        print_r($_FILES);
        echo '</pre>';
        $file = '';
        if (!empty($_FILES['excel']['name'])) {
            $_FILES['excel']['name'] = urlencode($_FILES['excel']['name']);
            $val = $this->emp_model->uploadExcel('excel');
            $file = (!empty($val['path'])) ? $val['path'] : '';
        }

        echo '<h1>' . $file . '</h1><pre>';
        print_r($val);
        echo '</pre>';
        if (!empty($file)) {
            $this->load->library('excel');
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            foreach ($cell_collection as $cell) {
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                $arr_data[$row][$column] = $data_value;
            }

            if (!empty($arr_data)) {
                foreach ($arr_data as $att) {
                    $this->emp_model->_table_name = 'tbl_attendance';
                    $this->emp_model->_primary_key = "attendance_id";
                    $data['employee_att_id'] = $att['A'];
                    $data['employee_name'] = $att['B'];
                    $data['employee_status'] = $att['C'];
                    $data['Action'] = $att['D'];
                    $data['job_code'] = $att['E'];
                    $data['att_date'] = explode(' ', $att['F'])[0];
                    $data['att_time'] = explode(' ', $att['F'])[1];
                    $this->emp_model->save($data);
                }
            }
            unlink($val['fullPath']);
        }
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/settings');
    }

    public function send_account_info()
    {
        $ids = $this->input->post('ids');

        if(in_array(0,$ids)){
            $this->emp_model->_table_name = "tbl_employee";
            $this->emp_model->_order_by = "employee_id";
            $emps = $this->emp_model->get();
            foreach ($emps as $emp){
                $this->emp_model->_table_name = "tbl_employee_login";
                $this->emp_model->_order_by = "employee_login_id";
                $account_info = $this->emp_model->get_by(array('employee_id' => $emp->employee_id), TRUE);

                $email = $emp->email;
                $username = $account_info->user_name;
                $password = $account_info->password;

                $message = '<b>username : </b>' . $username . '<br><b>password : </b> : ' . $password . '<br>';
                $subject = ($this->session->userdata('lang') == 'arabic') ? 'معلومات الحساب' : 'Account Inforamtions';
                $this->send_email($email, $message, $subject);
            }
        }else{
            foreach ($ids as $id){
                $this->emp_model->_table_name = "tbl_employee";
                $this->emp_model->_order_by = "employee_id";
                $emp = $this->emp_model->get_by(array('employee_id' => $id), TRUE);

                $this->emp_model->_table_name = "tbl_employee_login";
                $this->emp_model->_order_by = "employee_login_id";
                $account_info = $this->emp_model->get_by(array('employee_id' => $id), TRUE);

                $email = $emp->email;
                $username = $account_info->user_name;
                $password = $account_info->password;

                $message = '<b>username : </b>' . $username . '<br><b>password : </b> : ' . $password . '<br>';
                $subject = ($this->session->userdata('lang') == 'arabic') ? 'معلومات الحساب' : 'Account Inforamtions';
                $this->send_email($email, $message, $subject);

            }
        }

        $type = "success";
        $message = lang('contact_admin_success');
        set_message($type, $message);
        redirect('employee/settings');


    }

    public function send_email($to, $message, $subject = "HUMAN RESOURCES")
    {
        if ($to) {
            $this->load->library('email');
            //$send_email = $this->mail->sendEmail($from, $to, $subject, $message.'<br><a href="'.base_url().'">'.base_url().'</a>');
            $this->email->from('hr@edialogue.org', 'hr');
            $this->email->to($to);

            $this->email->subject($subject);
            $msg = ($this->session->userdata('lang') == 'arabic') ? 'الموارد البشرية' : 'human resources';
            $this->email->message('<br>' . $message . '<br><br><br><br><br><a href="' . base_url() . '">' . $msg . '</a><br><br>');
            $this->email->set_mailtype("html");

            $this->email->send();
        }
    }

    public function sms_templates(){
        $data = $this->input->post();
        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_primary_key = "sms_template_id";
        $this->emp_model->save($data,1);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/settings');
    }

    public function ajax1($type){
        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_order_by = "sms_template_id";
        $email = $this->emp_model->get_by(array('sms_template_id' => 2), TRUE);
        echo $email->{$type};
    }

    public function email_templates(){
        $data = $this->input->post();

        $datax[$data['type']] =$data['text'];
        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_primary_key = "sms_template_id";
        $this->emp_model->save($datax,2);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/settings');
    }

    public function test(){
        $this->emp_model->_table_name = "sms_templates";
        $this->emp_model->_order_by = "sms_template_id";
        $email = $this->emp_model->get_by(array('sms_template_id' => 2), TRUE);
        echo '<pre>';
        print_r($email);
            echo '<pre>';
    }
}
