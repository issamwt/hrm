<?php

class Reports extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->load->model('reports_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        if ($this->session->userdata('emp_type') == 'employee')
            redirect('employee/dashboard');
        $this->emp_type = $this->session->userdata('emp_type');
        if ($this->emp_type == 'dep_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $this->employee_list = $this->emp_model->all_emplyee_info();
        }
    }

    public function reports_section()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');

        $data['employee_list'] = $this->employee_list;
        $data['leaves_list'] = $this->db->select("leave_id, going_date, coming_date, employel_id")->from("tbl_leaves")->get()->result();

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/reports_section', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function accounting()
    {

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();

        $this->emp_model->_table_name = 'tbl_accountings';
        $this->emp_model->_order_by = "accounting_id";
        $data['accountings'] = $this->emp_model->get();
        //

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }


        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/accounting', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/accounting', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }
    
    public function la_training()
    {

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();

        $this->emp_model->_table_name = 'tbl_courses';
        $this->emp_model->_order_by = "course_id";
        $data['training_list'] = $this->emp_model->get();
        //

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }


        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/la_training', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/la_training', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function cutodies()
    {

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();

        $this->emp_model->_table_name = 'tbl_employee_custody';
        $this->emp_model->_order_by = "employee_id";
        $data['custodies_list'] = $this->emp_model->get();
        //

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }


        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        if ($data['inputs']['submit'] == 'all') {
            $data['custodies_list'] = array();
            foreach ($data['results'] as $key => $emp) {
                $custodies = $this->db->select("*")->from("tbl_employee_custody")->order_by("employee_id")->where("employee_id", $emp->employee_id)->get()->result();
                foreach ($custodies as $key => $cst) {
                    $cst->name_emp_ar = $emp->full_name_ar;
                    $cst->name_emp_en = $emp->full_name_en;
                    $cst->employment_id = $emp->employment_id;
                    array_push($data['custodies_list'], $cst);
                }
            }
        }


        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/cutodies', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/cutodies', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function caching()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();


        $this->emp_model->_table_name = 'tbl_caching';
        $this->emp_model->_order_by = "caching_id";
        $data['cachings'] = $this->emp_model->get();

        foreach ($data['inputs']['ids'] as $id) {
            if ($id != 0) {
                $data['results'][] = $this->emp_model->all_emplyee_info($id);
            }
        }
        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        if ($data['inputs']['submit'] == 'all') {
            $data['ids'] = array();
            foreach ($data['results'] as $id) {
                array_push($data['ids'], $id->employee_id);
            }
        } else {
            $data['ids'] = $data['inputs']['ids'];
            $help = array();
            foreach ($data['results'] as $emp) {
                array_push($help, $emp->employee_id);
            }
            foreach ($data['ids'] as $key => $id) {
                if (!in_array($id, $help)) {
                    unset($data['ids'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/caching', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/caching', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function finance_details()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['allowances_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance';
        $this->emp_model->_order_by = "allowance_type_id";
        $data['allowances_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_deduction_category';
        $this->emp_model->_order_by = "deduction_id";
        $data['deduction_category_list'] = $this->emp_model->get();


        $this->emp_model->_table_name = 'tbl_deduction';
        $this->emp_model->_primary_key = "deduction_type_id";
        $data['deduction_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_provision_category';
        $this->emp_model->_order_by = "provision_category_id";
        $data['provision_cat_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_provision';
        $this->emp_model->_primary_key = "provision_id";
        $data['provision_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_caching';
        $this->emp_model->_order_by = "caching_id";
        $data['cachings'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advances';
        $this->emp_model->_order_by = "advances_id";
        $data['advances'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_advance';
        $this->emp_model->_order_by = "advance_id";
        $data['advances_list'] = $this->emp_model->get();

        foreach ($data['inputs']['ids'] as $id) {
            if ($id != 0) {
                $data['results'][] = $this->emp_model->all_emplyee_info($id);
            }
        }
        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/finance_details', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/finance_details', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function personal_details()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['filter'] = $filter;

        $data['all'] = $this->db->get('tbl_employee')->result();

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/personal_details', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/personal_details', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function evaluations()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->employee_list;

        $this->emp_model->_table_name = 'tbl_evaluations';
        $this->emp_model->_order_by = "evaluation_id";
        $data['evaluations'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tpl_evaluation_items';
        $this->emp_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->emp_model->get();

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/evaluations', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/evaluations', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function la_embarkation()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->employee_list;

        $this->emp_model->_table_name = 'tbl_embarkation';
        $this->emp_model->_order_by = "embarkation_id";
        $data['embarkations_list'] = $this->emp_model->get();

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        if ($data['inputs']['submit'] == 'all') {
            $data['embarkations_list'] = array();
            foreach ($data['results'] as $key => $emp) {
                $embarkations = $this->db->select("*")->from("tbl_embarkation")->order_by("employermb_id")->where("employermb_id", $emp->employee_id)->get()->result();
                foreach ($embarkations as $key => $emb) {
                    $emb->name_emp_ar = $emp->full_name_ar;
                    $emb->name_emp_en = $emp->full_name_en;
                    $emb->employment_id = $emp->employment_id;
                    array_push($data['embarkations_list'], $emb);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/embarkations', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/embarkations', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function vacations()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->employee_list;

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->emp_model->get();

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// submit ////<
        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/vacations', $data, TRUE);
        } else
            $data['subview'] = $this->load->view('employee/vacations', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function attendances()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');
        $data['employee_list'] = $this->emp_model->get_all('tbl_employee');;

        $data['today'] = date('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;


        $this->emp_model->_table_name = 'tbl_attendance';
        $this->emp_model->_order_by = "attendance_id";
        $data['attendances'] = $this->emp_model->get();


        $data['all'] = $this->employee_list;
        $data['results'] = $this->employee_list;

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        if ($data['inputs']['submit'] == 'all') {
            $data['ids'] = array();
            foreach ($data['results'] as $id) {
                array_push($data['ids'], $id->employee_id);
            }
        } else {
            $data['ids'] = $data['inputs']['ids'];
            $help = array();
            foreach ($data['results'] as $emp) {
                array_push($help, $emp->employee_id);
            }
            foreach ($data['ids'] as $key => $id) {
                if (!in_array($id, $help)) {
                    unset($data['ids'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'all') {
            $data['name'] = $this->toexcel($data);
            $data['subview'] = $this->load->view('employee/reports/attendances', $data, TRUE);
        } else {
            $data['name'] = $this->toexcel($data);
            $data['subview'] = $this->load->view('employee/attendances', $data, TRUE);
        }

        $this->load->view('employee/_layout_main', $data);
    }

    public function vacations1()
    {
        $data = $this->input->post();
        $month = $data['month'];
        $year = $data['year'];
        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }
        $data['filter'] = $filter;

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $data['today'] = date('Y-m-d');

        //// submit ////
        $data['results'] = array();
        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($this->employee_list as $emp) {
                if (in_array($emp->employee_id, $data['inputs']['ids'])) {
                    array_push($data['results'], $emp);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }

        $this->emp_model->_table_name = 'tbl_leave_category';
        $this->emp_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_leaves';
        $this->emp_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_allowance_category';
        $this->emp_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/reports/vacations1', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);
    }

    public function toexcel($data)
    {
        date_default_timezone_set('Asia/Riyadh');
        $this->load->library('Excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setCreator("Rokn Al-Hiwar HRM")
            ->setLastModifiedBy("Rokn Al-Hiwar HRM")
            ->setTitle("Rokn Al-Hiwar HRM")
            ->setSubject("Rokn Al-Hiwar HRM")
            ->setDescription("Rokn Al-Hiwar HRM")
            ->setKeywords("Rokn Al-Hiwar HRM")
            ->setCategory("Rokn Al-Hiwar HRM");
        /////////////////////////////////////////////
        $obj = $objPHPExcel->setActiveSheetIndex(0);
        $obj->setCellValue('A1', 'UID')
            ->setCellValue('B1', 'Name')
            ->setCellValue('C1', 'Status')
            ->setCellValue('D1', 'Action')
            ->setCellValue('E1', 'JobCode')
            ->setCellValue('F1', 'DateTime');
        $counter = 2;

        foreach ($data['attendances'] as $att) {
            if (in_array($att->employee_att_id, $data['ids'])) {
                if (strpos(implode('-', explode('/', $att->att_date)), $data['filter']) !== false) {
                    $obj->setCellValue('A' . $counter, $att->employee_att_id);
                    foreach ($data['all'] as $emp) {
                        if ($emp->employee_id == $att->employee_att_id)
                            $obj->setCellValue('B' . $counter, $emp->full_name_en);
                    }
                    $obj->setCellValue('C' . $counter, $att->employee_status);
                    $obj->setCellValue('D' . $counter, $att->Action);
                    foreach (@$data['employee_list'] as $emp):
                        if ($emp->employee_id == $att->employee_att_id):
                            $x = $emp->employment_id;
                        endif;
                    endforeach;
                    $obj->setCellValue('E' . $counter, @$x);
                    $obj->setCellValue('F' . $counter, $att->att_date . ' ' . $att->att_time);
                    $counter++;
                }
            }
        }


        $obj->getColumnDimension('C')->setWidth(25);
        $obj->getColumnDimension('G')->setWidth(20);


        ////////////////////////////////////////////
        $today = date('Y-m-d');
        $name = $today . '_' . date('H-i-s');
        $objPHPExcel->getActiveSheet()->setTitle($name);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('excel/' . $name . '.xlsx');

        return $name;
    }

    public function recrutements()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('reports_section');
        $data['inputs'] = $this->input->post();

        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $month = $data['inputs']['month'];
        $year = $data['inputs']['year'];

        $filter = "-";
        if ($month == '0' and $year == '0') {
            $filter = "-";
        } elseif ($month == '0' and $year != '0') {
            $filter = $year . "-";
        } elseif ($month != '0' and $year == '0') {
            $filter = "-" . $month . "-";
        } else {
            $filter = $year . "-" . $month . "-";
        }

        $data['month'] = $month;
        $data['year'] = $year;
        $data['filter'] = $filter;

        $data['all'] = $this->employee_list;

        $this->emp_model->_table_name = 'tbl_recrutements';
        $this->emp_model->_order_by = "employerc_id";
        $data['recrutements_list'] = $this->emp_model->get();

        if ($data['inputs']['submit'] == 'all') {
            $data['results'] = $this->employee_list;
        } else {
            foreach ($data['inputs']['ids'] as $id) {
                if ($id != 0) {
                    $data['results'][] = $this->emp_model->all_emplyee_info($id);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['results'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['results'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['results'][$key]);
                }
            }
        }


        if ($data['inputs']['submit'] == 'all') {
            $data['subview'] = $this->load->view('employee/reports/recrutements', $data, TRUE);
        } else {
            $data['subview'] = $this->load->view('employee/recrutements', $data, TRUE);
        }

        $this->load->view('employee/_layout_main', $data);
    }
    
}