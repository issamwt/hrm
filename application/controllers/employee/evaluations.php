<?php

class Evaluations extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        $this->emp_type = $this->session->userdata('emp_type');

        if ($this->emp_type == 'dep_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $this->employee_list = $this->emp_model->all_emplyee_info();
        }
    }

    public function evaluations_section()
    {
        if ($this->emp_type == 'employee')
            redirect('employee/dashboard');
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('evaluations_section');
        $data['menu'] = array("employee_list" => 1);
        $data['hijri_calendar'] = 'TRUE';

        $data['employee_list'] = $this->employee_list;

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_evaluations';
        $this->emp_model->_order_by = "evaluation_id";
        if ($this->session->userdata('emp_type') != 'dep_manager') {
            $data['evaluations'] = $this->emp_model->get();
        } else {
            $data['evaluations'] = $this->emp_model->get_by(array('departmnt_id' => $this->session->userdata('my_department_id')));
        }


        $this->emp_model->_table_name = 'tpl_evaluation_items';
        $this->emp_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->emp_model->get();

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['subview'] = $this->load->view('employee/evaluations/evaluations_section', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function delete_evaluation($ev_id)
    {
        $this->emp_model->_table_name = 'tbl_evaluations';
        $this->emp_model->_primary_key = "evaluation_id";
        $this->emp_model->delete($ev_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/evaluations/evaluations_section');
    }

    public function put_an_evaluation()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('put_an_evaluation');
        $data['menu'] = array("employee_list" => 1);

        $datax = $this->input->post();

        $this->emp_model->_table_name = 'tbl_employee';
        $this->emp_model->_order_by = "employee_id";
        $data['employee_list'] = $this->emp_model->get_by(array('employee_id' => $datax['emp_id']), TRUE);

        $this->emp_model->_table_name = 'tpl_evaluation_items';
        $this->emp_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->emp_model->get();

        $data['subview'] = $this->load->view('employee/evaluations/put_an_evaluation', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_evaluation()
    {
        $data = $this->input->post();
        $data['valuess'] = implode(';', $data['valuess']);
        $data['ids'] = implode(';', $data['ids']);
        $today = date('Y-m-d');
        $data['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');


        $this->emp_model->_table_name = 'tbl_evaluations';
        $this->emp_model->_order_by = "evaluation_id";
        $saved_id = $this->emp_model->save($data);

        ////////////////////
        $this->emp_model->_table_name = 'tbl_notice';
        $this->emp_model->_order_by = "notice_id";
        $data = $this->input->post();
        $data2['employee_id'] = $this->session->userdata('employee_id');
        $data2['flag'] = 1;
        $today = date('Y-m-d');
        $data2['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $data2['view_status'] = 2;
        $lang = $this->session->userdata('lang');
        $data2['title'] = ($lang == 'english') ? 'new evaluation' : 'تقييم جديد';
        $data2['long_description'] = ($lang == 'english') ? 'You have a new evaluation from your manager : <br><a href="' . base_url() . 'employee/evaluations/evaluation_detail/' . $saved_id . '" target="_blank" style=" width: 200px;background: #337AB7;padding: 6px 20px;text-decoration: none;font-weight: bold;color: #fff;border-radius: 2px;">evaluation link</a>' : 'لديك تقييم جديد من طرف المدير :<br><br><a href="' . base_url() . 'employee/evaluations/evaluation_detail/' . $saved_id . '" target="_blank" style=" width: 200px;background: #337AB7;padding: 6px 20px;text-decoration: none;font-weight: bold;color: #fff;border-radius: 2px;">رابط التقييم</a>';
        $data2['sugg_or_compl'] = 3;
        $data2['send_to'] = $data['emp_id'];

        $this->emp_model->save($data2);
        ///////////////////

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/evaluations/evaluations_section');
    }

    public function evaluation_detail($ev_id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('evaluations_section');
        $data['menu'] = array("employee_list" => 1);

        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $data['employee_list'] = $this->db->where('status !=',2)->get("tbl_employee")->result();

        $this->emp_model->_table_name = 'tpl_evaluation_items';
        $this->emp_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->emp_model->get();

        $this->emp_model->_table_name = 'tbl_evaluations';
        $this->emp_model->_order_by = "evaluation_id";
        $data['ev'] = $this->emp_model->get_by(array('evaluation_id' => $ev_id), TRUE);

        $data['subview'] = $this->load->view('employee/evaluations/evaluation_detail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

}