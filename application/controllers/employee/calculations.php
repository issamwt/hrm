<?php

class Calculations extends Employee_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');

        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);
        $this->emp_type = $this->session->userdata('emp_type');

        if ($this->emp_type == 'dep_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $this->employee_list = $this->emp_model->all_emplyee_info();
        }

    }

    public function change_attendance($att_id){
        $this->emp_model->_table_name = 'tbl_attendance';
        $this->emp_model->_order_by = "attendance_id";

        $data['hijri_calendar'] = 'TRUE';
        $data['lang'] = $this->session->userdata('lang');

        $data['attendace'] = $this->emp_model->get_by(array('attendance_id'=>$att_id), true);

        $data['subview'] = $this->load->view('employee/change_attendance', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function change_attendance2($att_id=NULL){
        $this->emp_model->_table_name = 'tbl_attendance';
        $this->emp_model->_primary_key = "attendance_id";
        $data=$this->input->post();

        $this->emp_model->save($data, $att_id);
    }

    public function save_attendance2($emp_id=NULL){
        $this->emp_model->_table_name = 'tbl_attendance';
        $this->emp_model->_primary_key = "attendance_id";
        $data=$this->input->post();
        $data['att_date'] = str_replace('-', '/', $data['att_date']);


        $this->emp_model->save($data);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/calculations/calculating_attendance');
    }



    public function calculating_attendance(){
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_attendance');
        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $this->emp_model->_table_name = 'tbl_department';
        $this->emp_model->_order_by = "department_id";
        $data['department_list'] = $this->emp_model->get();

        $data['employee_list'] = $this->employee_list;

        $data['subview'] = $this->load->view('employee/calculations/calculating_attendance', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function calculating_attendance1()
    {
        $data['inputs'] = $this->input->post();
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_attendance');
        $data['hijri_calendar'] = 'TRUE';
        $data['emp_type']=$this->session->userdata('emp_type');

        $data['employee_list'] = $this->employee_list;
        $data['month'] = $data['inputs']['month'];
        $data['year'] = $data['inputs']['year'];
        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];

        $settings = $this->emp_model->get_all('tbl_gsettings');
        $data['num_days_off'] = $settings[0]->num_days_off;
        $data['job_time_hours'] = $settings[0]->job_time_hours;

        //// database records ////
        $data['num_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']);
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');
        $data['attendances'] = $this->db->select('*')->from('tbl_attendance')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('att_time', 'ASC')->get()->result();
        $data['attendances_xh'] = $this->db->select('*')->from('tbl_attendance_xh')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_xh_id', 'ASC')->get()->result();
        $data['tbl_permissions'] = $this->db->select('*')->from('tbl_permissions')->where("permission_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'")->order_by('permission_id', 'ASC')->get()->result();

        foreach ($data['tbl_permissions'] as $k => $p) {
            $date = $data['tbl_permissions'][$k]->permission_date;
            $data['tbl_permissions'][$k]->permission_date = str_replace('-', '/', $date);
            $permission_star = $data['tbl_permissions'][$k]->permission_star;
            if (strpos($permission_star, 'ص') !== false or strpos($permission_star, 'AM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            if (strpos($permission_star, 'م') !== false or strpos($permission_star, 'PM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $permission_star = (intval(explode(':', $permission_star)[0]) + 12) . ':' . explode(':', $permission_star)[1];
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            $permission_end = $data['tbl_permissions'][$k]->permission_end;
            if (strpos($permission_end, 'ص') !== false or strpos($permission_end, 'AM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
            if (strpos($permission_end, 'م') !== false or strpos($permission_end, 'PM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $permission_end = (intval(explode(':', $permission_end)[0]) + 12) . ':' . explode(':', $permission_end)[1];
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'one') {
            $data['title'] .= ' : ' . lang('report_by_employee');
            $arr = array();
            foreach ($data['inputs']['ids'] as $id) {
                array_push($arr, $id);
            }
            $data['inputs']['ids'] = $arr;
        } else {
            $data['title'] .= ' : ' . lang('report_all');
            $arr = array();
            foreach ($data['employee_list'] as $emp) {
                array_push($arr, $emp->employee_id);
            }
            $data['inputs']['ids'] = $arr;
        }

        //// ids ////
        foreach ($data['employee_list'] as $key => $emp) {
            if (!in_array($emp->employee_id, $data['inputs']['ids'])) {
                unset($data['employee_list'][$key]);
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'one') {
            $data['subview'] = $this->load->view('employee/calculations/calculating_attendance_one', $data, TRUE);
        } else {
            $data['subview'] = $this->load->view('employee/calculations/calculating_attendance_all', $data, TRUE);
        }

        $this->load->view('employee/_layout_main', $data);
    }

    public function calculating_salary()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_salary');

        $today = date('Y-m-d');
        $data['current_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $this->emp_model->_table_name = 'tbl_branches';
        $this->emp_model->_order_by = "branche_id";
        $data['branches_list'] = $this->emp_model->get();

        $data['employee_list'] = $this->employee_list;

        $data['subview'] = $this->load->view('employee/calculations/calculating_salary', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function calculating_salary1()
    {
        $data['inputs'] = $this->input->post();
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_salary');

        $data['employee_list'] = $this->employee_list;
        $data['month'] = $data['inputs']['month'];
        $data['year'] = $data['inputs']['year'];
        $data['branche'] = $data['inputs']['branche'];
        $data['department'] = $data['inputs']['department'];
        $data['designation'] = $data['inputs']['designation'];

        $settings = $this->emp_model->get_all('tbl_gsettings');
        $data['num_days_off'] = $settings[0]->num_days_off;
        $data['job_time_hours'] = $settings[0]->job_time_hours;

        $data['today'] = date('Y-m-d');

        //// database records ////
        $data['num_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']);
        $data['branches'] = $this->emp_model->get_all('tbl_branches');
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['job_titles'] = $this->emp_model->get_all('tbl_job_titles');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');
        $data['allowances_list'] = $this->emp_model->get_all('tbl_allowance');
        $data['allowances_cat_list'] = $this->emp_model->get_all('tbl_allowance_category');
        $data['deduction_list'] = $this->emp_model->get_all('tbl_deduction');
        $data['deduction_category_list'] = $this->emp_model->get_all('tbl_deduction_category');
        $data['provision_list'] = $this->emp_model->get_all('tbl_provision');
        $data['provision_cat_list'] = $this->emp_model->get_all('tbl_provision_category');
        $data['leaves_list'] = $this->emp_model->check_all_by("going_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'", 'tbl_leaves');
        $data['leaves_cat'] = $this->emp_model->get_all('tbl_leave_category');
        $data['advances_list'] = $this->emp_model->get_all('tbl_advances');
        $data['socials'] = $this->emp_model->get_all('tbl_social_insurane');

        $data['attendances'] = $this->db->select('*')->from('tbl_attendance')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_id', 'ASC')->get()->result();
        $data['attendances_xh'] = $this->db->select('*')->from('tbl_attendance_xh')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_xh_id', 'ASC')->get()->result();
        $data['tbl_permissions'] = $this->db->select('*')->from('tbl_permissions')->where("permission_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'")->order_by('permission_id', 'ASC')->get()->result();
        foreach ($data['tbl_permissions'] as $k => $p) {
            $date = $data['tbl_permissions'][$k]->permission_date;
            $data['tbl_permissions'][$k]->permission_date = str_replace('-', '/', $date);
            $permission_star = $data['tbl_permissions'][$k]->permission_star;
            if (strpos($permission_star, 'ص') !== false or strpos($permission_star, 'AM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            if (strpos($permission_star, 'م') !== false or strpos($permission_star, 'PM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $permission_star = (intval(explode(':', $permission_star)[0]) + 12) . ':' . explode(':', $permission_star)[1];
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            $permission_end = $data['tbl_permissions'][$k]->permission_end;
            if (strpos($permission_end, 'ص') !== false or strpos($permission_end, 'AM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
            if (strpos($permission_end, 'م') !== false or strpos($permission_end, 'PM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $permission_end = (intval(explode(':', $permission_end)[0]) + 12) . ':' . explode(':', $permission_end)[1];
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
        }
        // extra_hours_value
        foreach ($data['employee_list'] as $key => $emp) {
            $extra = $this->emp_model->check_by(array('employexh_id' => $emp->employee_id), 'tbl_extra_hours');
            if (!empty($extra)) {
                $extra__value = $this->emp_model->check_by(array('extra_work_id' => $extra->extra_work_id), 'tbl_extra_work');
                if (!empty($extra__value)) {
                    $data['employee_list'][$key]->extra_hours_value = $extra__value->work_value;
                } else
                    $data['employee_list'][$key]->extra_hours_value = 0;
            } else
                $data['employee_list'][$key]->extra_hours_value = 0;
        }


        //// submit ////
        if ($data['inputs']['submit'] == 'one') {
            $data['title'] .= ' : ' . lang('report_by_employee');
            $arr = array();
            foreach ($data['inputs']['ids'] as $id) {
                array_push($arr, $id);
            }
            $data['inputs']['ids'] = $arr;
        } else {
            $data['title'] .= ' : ' . lang('report_all');
            $arr = array();
            foreach ($data['employee_list'] as $emp) {
                array_push($arr, $emp->employee_id);
            }
            $data['inputs']['ids'] = $arr;
        }

        //// ids ////
        foreach ($data['employee_list'] as $key => $emp) {
            if (!in_array($emp->employee_id, $data['inputs']['ids'])) {
                unset($data['employee_list'][$key]);
            }
        }

        $dep_arr = array();
        //// branche ////
        if ($data['inputs']['branche'] != 0) {
            foreach ($data['departments'] as $dep){
                if($dep->branche_id==$data['inputs']['branche'])
                    array_push($dep_arr, $dep->department_id);
            }
            foreach ($data['employee_list'] as $key => $emp) {
                if (!in_array($emp->departement_id, $dep_arr)) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// department ////
        if ($data['inputs']['department'] != 0) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->departement_id != $data['inputs']['department']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }


        //// designation ////
        if ($data['inputs']['designation'] != -1) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->designations_id != $data['inputs']['designation']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// submit ////
        if ($data['inputs']['submit'] == 'one') {
            $data['subview'] = $this->load->view('employee/calculations/calculating_salary_one', $data, TRUE);
        } else {
            $html = $data['subview'] = $this->load->view('employee/calculations/calculating_salary_all2', $data, TRUE);
        }

        $this->load->view('employee/_layout_main', $data);
    }

    public function download_attendance($month, $year, $department, $designation)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_attendance');

        $data['employee_list'] = $this->employee_list;
        $data['month'] = $month;
        $data['year'] = intval($year);
        $data['department'] = intval($department);
        $data['designation'] = intval($designation);

        $settings = $this->emp_model->get_all('tbl_gsettings');
        $data['num_days_off'] = $settings[0]->num_days_off;
        $data['job_time_hours'] = $settings[0]->job_time_hours;

        //// database records ////
        $data['num_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']);
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');
        $data['attendances'] = $this->db->select('*')->from('tbl_attendance')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_id', 'ASC')->get()->result();
        $data['attendances_xh'] = $this->db->select('*')->from('tbl_attendance_xh')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_xh_id', 'ASC')->get()->result();
        $data['tbl_permissions'] = $this->db->select('*')->from('tbl_permissions')->where("permission_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'")->order_by('permission_id', 'ASC')->get()->result();
        foreach ($data['tbl_permissions'] as $k => $p) {
            $date = $data['tbl_permissions'][$k]->permission_date;
            $data['tbl_permissions'][$k]->permission_date = str_replace('-', '/', $date);
            $permission_star = $data['tbl_permissions'][$k]->permission_star;
            if (strpos($permission_star, 'ص') !== false or strpos($permission_star, 'AM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            if (strpos($permission_star, 'م') !== false or strpos($permission_star, 'PM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $permission_star = (intval(explode(':', $permission_star)[0]) + 12) . ':' . explode(':', $permission_star)[1];
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            $permission_end = $data['tbl_permissions'][$k]->permission_end;
            if (strpos($permission_end, 'ص') !== false or strpos($permission_end, 'AM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
            if (strpos($permission_end, 'م') !== false or strpos($permission_end, 'PM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $permission_end = (intval(explode(':', $permission_end)[0]) + 12) . ':' . explode(':', $permission_end)[1];
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
        }

        //// submit ////

        $data['title'] .= ' : ' . lang('report_all');
        $arr = array();
        foreach ($data['employee_list'] as $emp) {
            array_push($arr, $emp->employee_id);
        }

        //// ids ////
        foreach ($data['employee_list'] as $key => $emp) {
            if (!in_array($emp->employee_id, $arr)) {
                unset($data['employee_list'][$key]);
            }
        }

        //// department ////
        if ($data['department'] != 0) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->departement_id != $data['department']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// designation ////
        if ($data['designation'] != -1) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->designations_id != $data['designation']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// submit ////
        $data['name'] = $this->toexcel($data);
        $data['subview'] = $this->load->view('employee/calculations/calculating_attendance_download', $data, TRUE);

        $this->load->view('employee/_layout_main', $data);


    }

    public function toexcel($data)
    {
        $files = glob('img/temp/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }

        date_default_timezone_set('Asia/Riyadh');
        $this->load->library('Excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setCreator("Rokn Al-Hiwar HRM")
            ->setLastModifiedBy("Rokn Al-Hiwar HRM")
            ->setTitle("Rokn Al-Hiwar HRM")
            ->setSubject("Rokn Al-Hiwar HRM")
            ->setDescription("Rokn Al-Hiwar HRM")
            ->setKeywords("Rokn Al-Hiwar HRM")
            ->setCategory("Rokn Al-Hiwar HRM");
        /////////////////////////////////////////////
        $obj = $objPHPExcel->setActiveSheetIndex(0);
        $obj->setCellValue('A1', lang("employee_id"));
        $obj->setCellValue('B1', lang("employee_name"))
            ->setCellValue('C1', lang("job_time"))
            ->setCellValue('D1', lang("total_extra_hours"))
            ->setCellValue('E1', lang("total_attendant_hours"))
            ->setCellValue('F1', lang("total_permissions"))
            ->setCellValue('G1', lang("all_hours_available_in_month"))
            ->setCellValue('H1', lang("total_abscence_in_month"));
        $counter = 2;

        if (!empty($data['employee_list'])):
            foreach ($data['employee_list'] as $emp):

                $obj->setCellValue('A' . $counter, $emp->employment_id);
                $obj->getStyle('A' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('A' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $obj->setCellValue('B' . $counter, ($data['lang'] == 'arabic') ? $emp->full_name_ar : $emp->full_name_en);
                $obj->getStyle('B' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('B' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $presence = 0;
                $presence_xh = 0;
                $presence_pr = 0;
                for ($i = 1; $i <= $data['num_days']; $i++):
                    $day = $data['year'] . '/' . $data['month'] . '/' . sprintf('%02d', $i);
                    $total = 0;
                    $in = "";
                    $out = "";
                    $blocks = array();
                    foreach ($data['attendances'] as $att):

                        if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day):
                            if ($att->Action == "1"):
                                $in = $att->att_time;
                            else:
                                $out = $att->att_time;
                                if ($in != "" and $out != ""):
                                    $in_time = strtotime($in);
                                    $out_time = strtotime($out);
                                    $diff = round(abs($in_time - $out_time) / 60);
                                    array_push($blocks, [$in, $out, $diff]);
                                    $in = "";
                                    $out = "";
                                endif;
                            endif;
                        endif;
                    endforeach;
                    if (!empty($blocks)):
                        foreach ($blocks as $block):
                            $total += $block[2];
                        endforeach;
                    endif;
                    $total_xh = 0;
                    $in_xh = "";
                    $out_xh = "";
                    $blocks_xh = array();
                    foreach ($data['attendances_xh'] as $att_xh):
                        if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day):
                            if ($att_xh->Action == "1"):
                                $in_xh = $att_xh->att_time;
                            else:
                                $out_xh = $att_xh->att_time;
                                if ($in_xh != "" and $out_xh != ""):
                                    $in_time = strtotime($in_xh);
                                    $out_time = strtotime($out_xh);
                                    $diff_xh = round(abs($in_time - $out_time) / 60);
                                    array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]);
                                    $in_xh = "";
                                    $out_xh = "";
                                endif;
                            endif;
                        endif;
                    endforeach;
                    if (!empty($blocks_xh)):
                        foreach ($blocks_xh as $block_xh):
                            $total_xh += $block_xh[2];
                        endforeach;
                    endif;

                    $total_pr = 0;
                    foreach ($data['tbl_permissions'] as $pr):
                        if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day):
                            $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60);
                        endif;
                    endforeach;

                    if ($total != 0):
                        $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY');
                    endif;
                    if ($total_xh != 0):
                        $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY');
                    endif;
                    if ($total_pr != 0):
                        $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY');
                    endif;
                endfor;

                if ($emp->job_time == 'Part'):
                    $obj->setCellValue('C' . $counter, lang('job_part'));
                else:
                    $obj->setCellValue('C' . $counter, lang('job_full'));
                endif;
                $obj->getStyle('C' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('C' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


                $obj->setCellValue('D' . $counter, (((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00'));
                $obj->getStyle('D' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('D' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $obj->setCellValue('E' . $counter, (((int)($presence / 3600)) . ':' . (int)(($presence % 3600) / 60) . ':00'));
                $obj->getStyle('E' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('E' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $obj->setCellValue('F' . $counter, (((int)($presence_pr / 3600)) . ':' . (int)(($presence_pr % 3600) / 60) . ':00'));
                $obj->getStyle('F' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('F' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                if ($emp->job_time == 'Part' and $emp->job_time_hours != 0):
                    $mn = ($data['num_days'] - $data['num_days_off']) * $emp->job_time_hours;
                else:
                    $mn = ($data['num_days'] - $data['num_days_off']) * $data['job_time_hours'];
                endif;
                $mn = ($mn * 3600);
                $obj->setCellValue('G' . $counter, ((int)($mn / 3600)) . ':' . (int)(($mn % 3600) / 60) . ':00');
                $obj->getStyle('G' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('G' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $abs = $mn - $presence - $presence_pr;
                $obj->setCellValue('H' . $counter, ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00');
                $obj->getStyle('H' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('H' . $counter)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $counter++;
            endforeach;
        endif;


        $obj->getRowDimension('1')->setRowHeight(20);
        $obj->getColumnDimension('A')->setWidth(15);
        $obj->getColumnDimension('B')->setWidth(15);
        $obj->getColumnDimension('C')->setWidth(10);
        $obj->getColumnDimension('D')->setWidth(15);
        $obj->getColumnDimension('E')->setWidth(15);
        $obj->getColumnDimension('F')->setWidth(15);
        $obj->getColumnDimension('G')->setWidth(20);
        $obj->getColumnDimension('H')->setWidth(20);

        $obj->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $obj->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('H1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        ////////////////////////////////////////////
        $today = date('Y-m-d');
        $name = $today . '_' . date('H-i-s');
        $objPHPExcel->getActiveSheet()->setTitle($name);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('img/temp/' . $name . '.xlsx');
        return 'img/temp/' . $name . '.xlsx';
    }

    public function download_salary($month, $year, $branche, $department, $designation)
    {

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("index" => 2);
        $data['title'] = lang('calculating_salary');

        $data['employee_list'] = $this->employee_list;
        $data['month'] = $month;
        $data['year'] = intval($year);
        $data['branche'] = intval($branche);
        $data['department'] = intval($department);
        $data['designation'] = intval($designation);

        $settings = $this->emp_model->get_all('tbl_gsettings');
        $data['num_days_off'] = $settings[0]->num_days_off;
        $data['job_time_hours'] = $settings[0]->job_time_hours;

        $data['today'] = date('Y-m-d');

        //// database records ////
        $data['num_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']);
        $data['departments'] = $this->emp_model->get_all('tbl_department');
        $data['job_titles'] = $this->emp_model->get_all('tbl_job_titles');
        $data['designations'] = $this->emp_model->get_all('tbl_designations');
        $data['allowances_list'] = $this->emp_model->get_all('tbl_allowance');
        $data['allowances_cat_list'] = $this->emp_model->get_all('tbl_allowance_category');
        $data['deduction_list'] = $this->emp_model->get_all('tbl_deduction');
        $data['deduction_category_list'] = $this->emp_model->get_all('tbl_deduction_category');
        $data['provision_list'] = $this->emp_model->get_all('tbl_provision');
        $data['provision_cat_list'] = $this->emp_model->get_all('tbl_provision_category');
        $data['leaves_list'] = $this->emp_model->check_all_by("going_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'", 'tbl_leaves');
        $data['leaves_cat'] = $this->emp_model->get_all('tbl_leave_category');
        $data['advances_list'] = $this->emp_model->get_all('tbl_advances');
        $data['socials'] = $this->emp_model->get_all('tbl_social_insurane');

        $data['attendances'] = $this->db->select('*')->from('tbl_attendance')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_id', 'ASC')->get()->result();
        $data['attendances_xh'] = $this->db->select('*')->from('tbl_attendance_xh')->where("att_date LIKE '" . $data['year'] . "/" . $data['month'] . "%'")->order_by('attendance_xh_id', 'ASC')->get()->result();
        $data['tbl_permissions'] = $this->db->select('*')->from('tbl_permissions')->where("permission_date LIKE '" . $data['year'] . "-" . $data['month'] . "%'")->order_by('permission_id', 'ASC')->get()->result();
        foreach ($data['tbl_permissions'] as $k => $p) {
            $date = $data['tbl_permissions'][$k]->permission_date;
            $data['tbl_permissions'][$k]->permission_date = str_replace('-', '/', $date);
            $permission_star = $data['tbl_permissions'][$k]->permission_star;
            if (strpos($permission_star, 'ص') !== false or strpos($permission_star, 'AM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            if (strpos($permission_star, 'م') !== false or strpos($permission_star, 'PM') !== false) {
                $permission_star = str_replace(' ', '', $permission_star);
                $permission_star = preg_replace("/[^0-9,:]/", "", $permission_star);
                $permission_star = (intval(explode(':', $permission_star)[0]) + 12) . ':' . explode(':', $permission_star)[1];
                $data['tbl_permissions'][$k]->permission_star = $permission_star . ':00';
            }
            $permission_end = $data['tbl_permissions'][$k]->permission_end;
            if (strpos($permission_end, 'ص') !== false or strpos($permission_end, 'AM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
            if (strpos($permission_end, 'م') !== false or strpos($permission_end, 'PM') !== false) {
                $permission_end = str_replace(' ', '', $permission_end);
                $permission_end = preg_replace("/[^0-9,:]/", "", $permission_end);
                $permission_end = (intval(explode(':', $permission_end)[0]) + 12) . ':' . explode(':', $permission_end)[1];
                $data['tbl_permissions'][$k]->permission_end = $permission_end . ':00';
            }
        }
        // extra_hours_value
        foreach ($data['employee_list'] as $key => $emp) {
            $extra = $this->emp_model->check_by(array('employexh_id' => $emp->employee_id), 'tbl_extra_hours');
            if (!empty($extra)) {
                $extra__value = $this->emp_model->check_by(array('extra_work_id' => $extra->extra_work_id), 'tbl_extra_work');
                if (!empty($extra__value)) {
                    $data['employee_list'][$key]->extra_hours_value = $extra__value->work_value;
                } else
                    $data['employee_list'][$key]->extra_hours_value = 0;
            } else
                $data['employee_list'][$key]->extra_hours_value = 0;
        }


        //// submit ////
        $data['title'] .= ' : ' . lang('report_all');
        $arr = array();
        foreach ($data['employee_list'] as $emp) {
            array_push($arr, $emp->employee_id);
        }

        //// ids ////
        foreach ($data['employee_list'] as $key => $emp) {
            if (!in_array($emp->employee_id, $arr)) {
                unset($data['employee_list'][$key]);
            }
        }

        $dep_arr = array();
        //// branche ////
        if ($data['branche'] != 0) {
            foreach ($data['departments'] as $dep){
                if($dep->branche_id==$data['branche'])
                    array_push($dep_arr, $dep->department_id);
            }
            foreach ($data['employee_list'] as $key => $emp) {
                if (!in_array($emp->departement_id, $dep_arr)) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// department ////
        if ($data['department'] != 0) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->departement_id != $data['department']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }


        //// designation ////
        if ($data['designation'] != -1) {
            foreach ($data['employee_list'] as $key => $emp) {
                if ($emp->designations_id != $data['designation']) {
                    unset($data['employee_list'][$key]);
                }
            }
        }

        //// submit ////
        $data['name'] = $this->toexcel2($data);
        $html = $data['subview'] = $this->load->view('employee/calculations/calculating_salary_download', $data, TRUE);


        $this->load->view('employee/_layout_main', $data);
    }

    public function toexcel2($data)
    {
        $files = glob('img/temp/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }

        date_default_timezone_set('Asia/Riyadh');
        $this->load->library('Excel');
        $objPHPExcel = new Excel();
        $objPHPExcel->getProperties()->setCreator("Rokn Al-Hiwar HRM")
            ->setLastModifiedBy("Rokn Al-Hiwar HRM")
            ->setTitle("Rokn Al-Hiwar HRM")
            ->setSubject("Rokn Al-Hiwar HRM")
            ->setDescription("Rokn Al-Hiwar HRM")
            ->setKeywords("Rokn Al-Hiwar HRM")
            ->setCategory("Rokn Al-Hiwar HRM");
        /////////////////////////////////////////////
        $obj = $objPHPExcel->setActiveSheetIndex(0);
        $obj->setCellValue('A1', lang("sl"));
        $obj->setCellValue('B1', lang("name"))
            ->setCellValue('C1', lang("job_title"))
            ->setCellValue('D1', lang("job_time"))
            ->setCellValue('E1', lang("abscence_days"))
            ->setCellValue('F1', lang("total_extra_hours"))
            ->setCellValue('G1', lang("merits"))
            ->setCellValue('G2', lang("finance_basic_salary"))
            ->setCellValue('H2', lang("transportation_allowance"))
            ->setCellValue('I2', lang("finance_provision"))
            ->setCellValue('J2', lang("value_extra_hours"))
            ->setCellValue('K2', lang("house_allowance"))
            ->setCellValue('L1', lang("total_merits"))
            ->setCellValue('M1', lang("deductions"))
            ->setCellValue('M2', lang("abscence"))
            ->setCellValue('N2', lang("fieldset_med"))
            ->setCellValue('O2', lang("fieldset_social"))
            ->setCellValue('P2', lang("finance_permanent_deduction"))
            ->setCellValue('Q2', lang("leaves"))
            ->setCellValue('R2', lang("advances_app"))
            ->setCellValue('S1', lang("total_deductions"))
            ->setCellValue('T1', lang("net_merits"));


        $obj->mergeCells('A1:A2');
        $obj->mergeCells('B1:B2');
        $obj->mergeCells('C1:C2');
        $obj->mergeCells('D1:D2');
        $obj->mergeCells('E1:E2');
        $obj->mergeCells('F1:F2');
        $obj->mergeCells('G1:H1');
        $obj->mergeCells('G1:I1');
        $obj->mergeCells('G1:J1');
        $obj->mergeCells('G1:K1');
        $obj->mergeCells('L1:L2');
        $obj->mergeCells('M1:N1');
        $obj->mergeCells('M1:O1');
        $obj->mergeCells('M1:P1');
        $obj->mergeCells('M1:Q1');
        $obj->mergeCells('M1:R1');
        $obj->mergeCells('S1:S2');
        $obj->mergeCells('T1:T2');

        $obj->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('B1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('C1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('E1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('F1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('G1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('F2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('G2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('H2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('I2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('J2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('K2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('L1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('L2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('M1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('M2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('N2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('N2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('O2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('P2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('Q2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('Q2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('R2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('S1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('S1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $obj->getStyle('T1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $obj->getStyle('T1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


        $counter = 3;

        $today = strtotime($data['today']);
        $n = 0;
        if (!empty($data['employee_list'])):
            foreach ($data['employee_list'] as $emp):$n++;
                $obj->setCellValue('A' . $counter, $n);
                $obj->setCellValue('B' . $counter, ($data['lang'] == 'english') ? $emp->full_name_en : $emp->full_name_ar);

                foreach ($data['job_titles'] as $jt):
                    if ($jt->job_titles_id == $emp->job_title):
                        $obj->setCellValue('C' . $counter, ($data['lang'] == 'english') ? $jt->job_titles_name_en : $jt->job_titles_name_ar);
                    endif;
                endforeach;

                if ($emp->job_time == 'Part'):
                    $obj->setCellValue('D' . $counter, lang('job_part'));
                else:
                    $obj->setCellValue('D' . $counter, lang('job_full'));
                endif;

                $presence = 0;
                $presence_xh = 0;
                $presence_pr = 0;
                for ($i = 1; $i <= $data['num_days']; $i++):
                    $day = $data['year'] . '/' . $data['month'] . '/' . sprintf('%02d', $i);
                    $total = 0;
                    $in = "";
                    $out = "";
                    $blocks = array();
                    foreach ($data['attendances'] as $att):
                        if ($att->employee_att_id == $emp->employee_id and $att->att_date == $day):
                            if ($att->Action == "1"):
                                $in = $att->att_time;
                            else:
                                $out = $att->att_time;
                                if ($in != "" and $out != ""):
                                    $in_time = strtotime($in);
                                    $out_time = strtotime($out);
                                    $diff = round(abs($in_time - $out_time) / 60);
                                    array_push($blocks, [$in, $out, $diff]);
                                    $in = "";
                                    $out = "";
                                endif;
                            endif;
                        endif;
                    endforeach;
                    if (!empty($blocks)):
                        foreach ($blocks as $block):
                            $total += $block[2];
                        endforeach;
                    endif;
                    $total_xh = 0;
                    $in_xh = "";
                    $out_xh = "";
                    $blocks_xh = array();
                    foreach ($data['attendances_xh'] as $att_xh):
                        if ($att_xh->employee_xh_id == $emp->employee_id and $att_xh->att_date == $day):
                            if ($att_xh->Action == "1"):
                                $in_xh = $att_xh->att_time;
                            else:
                                $out_xh = $att_xh->att_time;
                                if ($in_xh != "" and $out_xh != ""):
                                    $in_time = strtotime($in_xh);
                                    $out_time = strtotime($out_xh);
                                    $diff_xh = round(abs($in_time - $out_time) / 60);
                                    array_push($blocks_xh, [$in_xh, $out_xh, $diff_xh]);
                                    $in_xh = "";
                                    $out_xh = "";
                                endif;
                            endif;
                        endif;
                    endforeach;
                    if (!empty($blocks_xh)):
                        foreach ($blocks_xh as $block_xh):
                            $total_xh += $block_xh[2];
                        endforeach;
                    endif;
                    $total_pr = 0;
                    foreach ($data['tbl_permissions'] as $pr):
                        if ($pr->employeeprm_id == $emp->employee_id and $pr->permission_date == $day):
                            $total_pr += round(abs(strtotime($pr->permission_star) - strtotime($pr->permission_end)) / 60);
                        endif;
                    endforeach;
                    if ($total != 0):
                        $presence += strtotime(gmdate("H:i:s", ($total * 60))) - strtotime('TODAY');
                    endif;
                    if ($total_xh != 0):
                        $presence_xh += strtotime(gmdate("H:i:s", ($total_xh * 60))) - strtotime('TODAY');
                    endif;
                    if ($total_pr != 0):
                        $presence_pr += strtotime(gmdate("H:i:s", ($total_pr * 60))) - strtotime('TODAY');
                    endif;
                endfor;
                $plus_1 = 0;
                $minus_1 = 0;

                if ($emp->job_time == 'Part' and $emp->job_time_hours != 0):
                    $mn = ($data['num_days'] - $data['num_days_off']) * $emp->job_time_hours;
                else:
                    $mn = ($data['num_days'] - $data['num_days_off']) * $data['job_time_hours'];
                endif;

                $mn = ($mn * 3600);
                $abs = $mn - $presence - $presence_pr;

                $obj->setCellValue('E' . $counter, ((int)($abs / 3600)) . ':' . (int)(($abs % 3600) / 60) . ':00');
                $obj->setCellValue('F' . $counter, ((int)($presence_xh / 3600)) . ':' . (int)(($presence_xh % 3600) / 60) . ':00');
                $obj->setCellValue('G' . $counter, $emp->employee_salary);

                $plus_2 = 0;
                if ($emp->job_time == "Full"):
                    if (!empty(@$data['allowances_list'])):
                        foreach (@$data['allowances_list'] as $al):
                            if ($al->employe_id == $emp->employee_id and $al->allowance_id==2):
                                if ($this->dates($data['month'], $data['year'], $al->allowance_date_from, $al->allowance_date_to)):
                                    $from = strtotime($al->allowance_date_from);
                                    $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30");
                                    if ($al->allowance_type == 'value'):
                                        $val = ($al->allowance_value / 100) * (100 - $al->reservation);
                                        if ($from <= $today and $today <= $to):
                                            $plus_2 += $val;
                                        endif;
                                    else:
                                        $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation);
                                        if ($from <= $today and $today <= $to):
                                            $plus_2 += $val;
                                        endif;
                                    endif;
                                endif;
                            endif;
                        endforeach;
                    endif;
                endif;
                $obj->setCellValue('H' . $counter, $plus_2);


                $plus_3 = 0;
                if (!empty(@$data['provision_list'])):
                    foreach (@$data['provision_list'] as $p):
                        if ($p->employp_id == $emp->employee_id):
                            if ($this->dates($data['month'], $data['year'], $p->start_date, $p->end_date)):
                                $plus_3 += $p->provision_value;
                            endif;
                        endif;
                    endforeach;
                    $obj->setCellValue('I' . $counter, $plus_3);
                endif;
                $mn_value = (int)($mn / 60);
                $vh = round($emp->employee_salary / $mn_value * 60, 2);
                $vmn = round($emp->employee_salary / $mn_value, 5);
                $exh = ($vh / 100) * $emp->extra_hours_value;
                $exmn = ($vmn / 100) * $emp->extra_hours_value;
                $plus_1 = round((int)($presence_xh / 60) * $exmn, 0);
                $obj->setCellValue('J' . $counter, $plus_1);

                $plus_4 = 0;
                if ($emp->job_time == "Full"):
                    if (!empty(@$data['allowances_list'])):
                        foreach (@$data['allowances_list'] as $al):
                            if ($al->employe_id == $emp->employee_id and $al->allowance_id==1):
                                if ($this->dates($data['month'], $data['year'], $al->allowance_date_from, $al->allowance_date_to)):
                                    $from = strtotime($al->allowance_date_from);
                                    $to = ($al->allowance_date_to != '') ? strtotime($al->allowance_date_to) : strtotime("9999-12-30");
                                    if ($al->allowance_type == 'value'):
                                        $val = ($al->allowance_value / 100) * (100 - $al->reservation);
                                        if ($from <= $today and $today <= $to):
                                            $plus_4 += $val;
                                        endif;
                                    else:
                                        $val = (($emp->employee_salary / 100) * ($al->allowance_value) / 100) * (100 - $al->reservation);
                                        if ($from <= $today and $today <= $to):
                                            $plus_4 += $val;
                                        endif;
                                    endif;
                                endif;
                            endif;
                        endforeach;
                    endif;
                endif;
                $obj->setCellValue('K' . $counter, $plus_4);

                $obj->setCellValue('L' . $counter, ($x = $emp->employee_salary + $plus_1 + $plus_2 + $plus_3 + $plus_4));
                $minus_1 = round($abs / 60 * $vmn, 0);
                $obj->setCellValue('M' . $counter, $minus_1);

                $minus_2 = 0;
                if ($emp->med_insur != 1):
                    $obj->setCellValue('N' . $counter, $minus_2);
                else:
                    $minus_2 = ($emp->employee_salary / 100) * @$emp->m_insurance_percent;
                    $obj->setCellValue('N' . $counter, $minus_2);
                endif;


                $minus_3 = 0;
                if ($emp->social_insurance != 1):
                    $obj->setCellValue('O' . $counter, $minus_3);
                else:
                    $id = 2;
                    switch ($emp->social_insurance_type) {
                        case 'saudi1':
                            $id = 1;
                            break;
                        case 'saudi2':
                            $id = 2;
                            break;
                        case 'non-saudi':
                            $id = 3;
                            break;
                    }
                    if($data['socials'][$id-1]->habit_insurance==1):
                        $minus_3 = (($emp->employee_salary+$plus_4) / 100) * @$emp->insurance_percent;
                    else:
                        $minus_3 = ($emp->employee_salary / 100) * @$emp->insurance_percent;
                    endif;
                    $obj->setCellValue('O' . $counter, $minus_3);
                endif;

                $minus_4 = 0;
                if (!empty(@$data['deduction_list'])):
                    foreach (@$data['deduction_list'] as $d):
                        if ($d->employd_id == $emp->employee_id):
                            if ($this->dates($data['month'], $data['year'], $d->start_date, $d->end_date)):
                                if ($d->deduction_type == 'percent') {
                                    $val = ($emp->employee_salary / 100) * @$d->deduction_value;
                                    $minus_4 += $val;
                                } else {
                                    $val = @$d->deduction_value;
                                    $minus_4 += $val;
                                }
                            endif;
                        endif;
                    endforeach;
                    $obj->setCellValue('P' . $counter, $minus_4);
                endif;
                $minus_5 = 0;
                foreach (@$data['leaves_list'] as $leave):
                    if ($leave->employel_id == $emp->employee_id):
                        $value = ($emp->employee_salary / 100) * $leave->quota;
                        $minus_5 += $value;
                    endif;
                endforeach;
                $obj->setCellValue('Q' . $counter, $minus_5);
                $minus_6 = 0;
                if (!empty(@$data['advances_list'])):
                    foreach (@$data['advances_list'] as $adv):
                        if ($adv->employea_id == $emp->employee_id):
                            $valuee = $adv->monthly_installement ?><?php $minus_6 += $valuee;
                        endif;
                    endforeach;
                endif;
                $obj->setCellValue('R' . $counter, $minus_6);
                $y = $minus_1 + $minus_2 + $minus_3 + $minus_4 + $minus_5 + $minus_6;
                $obj->setCellValue('S' . $counter, $y);
                $obj->setCellValue('T' . $counter, round($x - $y, 1));

                $obj->getStyle('A' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('B' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('C' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('D' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('E' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('F' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('G' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('H' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('I' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('J' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('K' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('L' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('M' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('N' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('O' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('P' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('Q' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('R' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('S' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $obj->getStyle('T' . $counter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $obj->getStyle('G' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
                $obj->getStyle('H' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
                $obj->getStyle('I' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
                $obj->getStyle('J' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
                $obj->getStyle('K' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
                $obj->getStyle('L' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);

                $obj->getStyle('M' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('N' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('O' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('P' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('Q' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('R' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                $obj->getStyle('S' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);

                $obj->getStyle('T' . $counter)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_GREEN);

                $counter++;
            endforeach;
        endif;


        $obj->getRowDimension('1')->setRowHeight(20);
        $obj->getRowDimension('2')->setRowHeight(20);
        $obj->getColumnDimension('A')->setWidth(5);
        $obj->getColumnDimension('B')->setWidth(15);
        $obj->getColumnDimension('C')->setWidth(15);
        $obj->getColumnDimension('D')->setWidth(15);
        $obj->getColumnDimension('E')->setWidth(15);
        $obj->getColumnDimension('F')->setWidth(15);
        $obj->getColumnDimension('G')->setWidth(15);
        $obj->getColumnDimension('H')->setWidth(15);
        $obj->getColumnDimension('I')->setWidth(15);
        $obj->getColumnDimension('J')->setWidth(15);
        $obj->getColumnDimension('K')->setWidth(15);
        $obj->getColumnDimension('L')->setWidth(15);
        $obj->getColumnDimension('M')->setWidth(15);
        $obj->getColumnDimension('N')->setWidth(15);
        $obj->getColumnDimension('O')->setWidth(15);
        $obj->getColumnDimension('P')->setWidth(15);
        $obj->getColumnDimension('Q')->setWidth(15);
        $obj->getColumnDimension('R')->setWidth(15);
        $obj->getColumnDimension('S')->setWidth(15);
        $obj->getColumnDimension('T')->setWidth(15);
        ////////////////////////////////////////////
        $today = date('Y-m-d');
        $name = $today . '_' . date('H-i-s');
        $objPHPExcel->getActiveSheet()->setTitle($name);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('img/temp/' . $name . '.xlsx');
        return 'img/temp/' . $name . '.xlsx';
    }

    public function dates($month, $year, $start, $end)
    {
        if ($end == '')
            $end = date_create_from_format('Y-m-d', "9999-12-30")->format('Y-m-d');
        $flag = false;

        $start_s = date_create_from_format('Y-m-d', $start)->format('Ym');
        $end_e = date_create_from_format('Y-m-d', $end)->format('Ym');
        $date_x = date_create_from_format('Y-m-d', $year . '-' . $month . '-01')->format('Ym');
        if ($date_x <= $end_e and $date_x >= $start_s)
            $flag = true;

        return $flag;
    }
}