<?php

class Accounting extends Employee_Controller
{

    public function __construct()
    {

        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->employee_id = $this->session->userdata('employee_id');
        $this->employee_details = $this->emp_model->all_emplyee_info($this->employee_id);

        $this->emp_type = $this->session->userdata('emp_type');
        if ($this->emp_type == 'dep_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_dep(NULL, $this->session->userdata('my_department_id'));
        } elseif ($this->emp_type == 'sec_manager') {
            $this->employee_list = $this->emp_model->all_emplyee_info_by_sec(NULL, $this->session->userdata('my_designation_id'));
        } else {
            $this->employee_list = $this->emp_model->all_emplyee_info();
        }
    }

    public function send_accounting($emp_id)
    {
        if ($this->session->userdata('emp_type') == 'employee')
            redirect('employee/dashboard');

        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('send_accounting');

        $data['employee_info'] = $this->emp_model->all_emplyee_info($emp_id);
        $data['managers'] = $this->get_direct_manager($data['employee_info']);

        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_order_by = "accounting_id";
        $data['accounting_list'] = $this->emp_model->get_by(array("from_employee" => $this->employee_id));
        $data['all'] = $this->employee_list;

        $data['subview'] = $this->load->view('employee/accounting/send_accounting', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_accounting($emp_id)
    {
        $today = date('Y-m-d');

        if ($this->session->userdata('emp_type') == 'employee')
            redirect('employee/dashboard');

        $data = $this->input->post();
        $data['from_employee'] = $this->employee_id;
        $data['to_employee'] = $emp_id;
        $data['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_primary_key = "accounting_id";
        $saved_id = $this->emp_model->save($data);

        if ($saved_id) {
            $name = ($this->session->userdata('lang') == 'arabic') ? $this->employee_details->full_name_ar : $this->employee_details->full_name_en;
            $job_titles_name = ($this->session->userdata('lang') == 'arabic') ? $this->employee_details->job_titles_name_ar : $this->employee_details->job_titles_name_en;

            $datax['title'] = lang('accounting_from') .$job_titles_name.' '. $name;
            $datax['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $datax['view_status'] = 2;
            $datax['flag'] = 1;
            $datax['send_to'] = $emp_id;
            $datax['employee_id'] = $this->employee_id;
            $datax['to_all'] = 0;
            $datax['sugg_or_compl'] = 5;
            $datax['long_description'] = lang('accounting_long_description_1');
            $datax['long_description'] .= '<br><a href="' . base_url() . 'employee/accounting/accounting_detail1/' . $saved_id . '" class="btn btn-primary" target="_blank">' . lang('accounting_long_description_2') . '</a>';

            $this->emp_model->_table_name = "tbl_notice";
            $this->emp_model->_primary_key = "notice_id";
            $this->emp_model->save($datax);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/accounting/send_accounting/' . $emp_id);
    }

    public function accounting_detail1($acc_id)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['menu'] = array("employee_list" => 1);
        $data['title'] = lang('employee_reply');

        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_order_by = "accounting_id";
        $data['accounting_detail'] = $this->emp_model->get_by(array("accounting_id" => $acc_id), TRUE);

        $data['subview'] = $this->load->view('employee/accounting/accounting_detail1', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function employee_reply($acc_id)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_primary_key = "accounting_id";
        $saved_id = $this->emp_model->save($data, $acc_id);

        if ($saved_id) {
            $this->emp_model->_table_name = "tbl_accountings";
            $this->emp_model->_order_by = "accounting_id";
            $accounting_detail = $this->emp_model->get_by(array("accounting_id" => $acc_id), TRUE);

            $today = date('Y-m-d');
            $name = ($this->session->userdata('lang') == 'arabic') ? $this->employee_details->full_name_ar : $this->employee_details->full_name_en;
            $datax['title'] = lang('accounting_long_description_3');
            $datax['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $datax['view_status'] = 2;
            $datax['flag'] = 1;
            $datax['send_to'] = $accounting_detail->from_employee;
            $datax['employee_id'] = $this->employee_id;
            $datax['to_all'] = 0;
            $datax['sugg_or_compl'] = 3;
            $datax['long_description'] = lang('accounting_long_description_4');
            $datax['long_description'] .= $name;

            $this->emp_model->_table_name = "tbl_notice";
            $this->emp_model->_primary_key = "notice_id";
            $this->emp_model->save($datax);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/accounting/accounting_detail1/' . $acc_id);
    }

    public function final_decision($emp_id, $acc_id)
    {
        $data = $this->input->post();

        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_primary_key = "accounting_id";
        $saved_id = $this->emp_model->save($data, $acc_id);

        if ($saved_id) {
            $name = ($this->session->userdata('lang') == 'arabic') ? $this->employee_details->full_name_ar : $this->employee_details->full_name_en;
            $datax['title'] = lang('accounting_from') . $name;
            $today = date('Y-m-d');
            $datax['created_date'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
            $datax['view_status'] = 2;
            $datax['flag'] = 1;
            $datax['send_to'] = $emp_id;
            $datax['employee_id'] = $this->employee_id;
            $datax['to_all'] = 0;
            $datax['sugg_or_compl'] = 3;
            $datax['long_description'] .= '<b>'.lang("final_decision").' : </b><br>'.$data['final_decision'];

            $this->emp_model->_table_name = "tbl_notice";
            $this->emp_model->_primary_key = "notice_id";
            $this->emp_model->save($datax);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('employee/accounting/send_accounting/' . $emp_id);
    }

    public function delete_accounting($emp_id, $acc_id){
        $this->emp_model->_table_name = "tbl_accountings";
        $this->emp_model->_primary_key = "accounting_id";
        $this->emp_model->delete($acc_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/accounting/send_accounting/' . $emp_id);
    }


    public function get_direct_manager($data)
    {
        $all = $this->db->get('tbl_employee')->result();
        //get  direct_manager_id
        $datax['direct_manager_id'] = $data->direct_manager_id;

        foreach ($all as $emp) {
            if ($emp->employee_id == $datax['direct_manager_id']) {
                $datax['direct_manager_name_ar'] = $emp->full_name_ar;
                $datax['direct_manager_name_en'] = $emp->full_name_en;
            }
        }
        //get  human_resource_id
        $datax['human_resource_name_ar'] = lang('hr_admin');
        $datax['human_resourcen_name_en'] = lang('hr_admin');
        $datax['human_resource_id'] = 1;

        foreach ($all as $emp) {
            if ($emp->job_title == 4) {
                $datax['human_resource_name_ar'] = $emp->full_name_ar;
                $datax['human_resourcen_name_en'] = $emp->full_name_en;
                $datax['human_resource_id'] = $emp->employee_id;
            }
        }
        return $datax;
    }
}