<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of department
 *
 * @author Ashraf
 */
class Department extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('department_model');
    }

    public function department_list($id = NULL) {

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('department_list');
        $data['page_header'] = lang('department_page_header');

        $data['active'] = 1;

        //department table initials
        $this->department_model->_table_name = "tbl_department"; //table name
        $this->department_model->_order_by = "department_id";

        if ($id) { // retrive data from db by id
            $data['active'] = 2;
            // get all department by id
            $data['department_info'] = $this->department_model->get_by(array('department_id' => $id), TRUE);
            // get all designation by department id
            $this->department_model->_table_name = "tbl_designations"; //table name
            $data['designation_info'] = $this->department_model->get_by(array('department_id' => $id), FALSE);

            if (empty($data['department_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/department/department_list');
            }
        }


        $this->department_model->_table_name = "tbl_employee"; //table name
        $this->department_model->_order_by = "employee_id";
        $data['employee_list'] = $this->department_model->get();

        $this->department_model->_table_name = "tbl_department"; //table name
        $this->department_model->_order_by = "department_id";
        $data['all_dept_info'] = $this->department_model->get();
        // get all department info and designation info
        foreach ($data['all_dept_info'] as $v_dept_info) {
            $data['all_department_info'][] = $this->department_model->get_add_department_by_id($v_dept_info->department_id);
        }
        $data['subview'] = $this->load->view('admin/department/department_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_department($id = NULL) {

        $this->department_model->_table_name = "tbl_department"; // table name
        $this->department_model->_primary_key = "department_id"; // $id

        $data = $this->input->post(); //input post

        $this->department_model->save($data, $id);
        $type = "success";
        $message = lang('department_info_saved');
        set_message($type, $message);
        redirect('admin/department/department_list'); //redirect page
    }

    public function delete_department($id) {
        // if trying to delete the default departeemt
        if ($id == 1)
            redirect('errors/notallawoed');

        // delete the departement
        $this->department_model->_table_name = "tbl_department"; // table name
        $this->department_model->_primary_key = "department_id"; // $id
        $this->department_model->delete($id);

        // get all the designations that belong to the deleted departement
        $where = array('department_id' => $id);
        $this->department_model->_table_name = "tbl_designations"; // table name
        $this->department_model->_order_by = "designations_id";
        $get_designations = $this->department_model->get_by($where);

        //prepare the update_batch $update_data array
        $update_data = array();
        if (count($get_designations)) {
            foreach ($get_designations as $desig) {
                $array = array(
                    'designations_id' => $desig->designations_id,
                    'department_id' => 1
                );
                array_push($update_data, $array);
            }
        }

        // reset the designation belonging to the deleted departement
        $res = $this->department_model->reset_designation($update_data);
        // redirect
        set_message('success', lang('department_info_deleted'));
        redirect('admin/department/department_list');
    }

    public function delete_designation($dept_id, $id) {
        // check into designation table by id
        // if data exist do not delete the department
        // else delete the department

        $or_where = array('designations_id' => $id);
        $get_existing_id = $this->department_model->check_by($or_where, 'tbl_employee');
        if (!empty($get_existing_id)) {
            $type = "error";
            $message = lang('designation_info_used');
        } else {
            // delete all designations by id
            $this->department_model->_table_name = "tbl_designations"; // table name
            $this->department_model->_primary_key = "designations_id"; // $id
            $this->department_model->delete($id);
            $type = "success";
            $message = lang('designation_info_deleted');
        }
        set_message($type, $message);
        redirect('admin/department/department_list/' . $dept_id); //redirect page
    }

    public function check_designations($department_id, $v_designations) { // check_designations by id and designation
        $where = array('department_id' => $department_id, 'designations' => $v_designations);
        return $this->department_model->check_by($where, 'tbl_designations');
    }

}
