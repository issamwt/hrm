<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Insurances extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('insurance_model');
    }

    //Start Medical insurance
    public function medical_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('medical_list');
        $data['page_header'] = lang('medical_page_header');

        $this->insurance_model->_table_name = "tbl_medical_insurane"; //table name
        $this->insurance_model->_order_by = "medical_id";
        $data['medicales_info'] = $this->insurance_model->get();

        if (!empty($id)) {// retrive data from db by id
            $data['active'] = 2;
            $this->insurance_model->_table_name = "tbl_medical_insurane"; //table name
            $this->insurance_model->_order_by = "medical_id";

            $data['medicale_info'] = $this->insurance_model->get_by(array('medical_id' => $id), TRUE);
            if (empty($data['medicale_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/insurances/add_medical');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/insurances/medical_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_medical($id = NULL) {

        $data = $this->input->post();
        $this->insurance_model->_table_name = "tbl_medical_insurane"; //table name
        $this->insurance_model->_order_by = "medical_id";
        $this->insurance_model->_primary_key = "medical_id";

        $this->insurance_model->save($data, $id);
        echo $this->db->last_query();
echo $this->db->_error_message();


        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/insurances/medical_list');
    }

    public function delete_medical($id) {
        $this->insurance_model->_table_name = "tbl_medical_insurane"; // table name
        $this->insurance_model->_primary_key = "medical_id";
        $this->insurance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/insurances/medical_list');
    }

    //End social insurance

    public function social_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('social_list');
        $data['page_header'] = lang('social_page_header');

        $this->insurance_model->_table_name = "tbl_social_insurane"; //table name
        $this->insurance_model->_order_by = "social_id";
        $data['sociales_info'] = $this->insurance_model->get();

        if (!empty($id)) {// retrive data from db by id
            $data['active'] = 2;
            $this->insurance_model->_table_name = "tbl_social_insurane"; //table name
            $this->insurance_model->_order_by = "social_id";

            $data['social_info'] = $this->insurance_model->get_by(array('social_id' => $id), TRUE);
            if (empty($data['social_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/insurances/add_medical');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/insurances/social_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_social($id = NULL) {

        $data = $this->input->post();
        $this->insurance_model->_table_name = "tbl_social_insurane"; //table name
        $this->insurance_model->_order_by = "social_id";
        $this->insurance_model->_primary_key = "social_id";

        $this->insurance_model->save($data, $id);
        echo $this->db->last_query();

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/insurances/social_list');
    }

    public function save_social_details($id = NULL) {

        $data = $this->insurance_model->array_from_post(array('social_id', 'insurance_percent', 'societe_percent', 'nb_days', 'age_male', 'age_female', 'salary_insurance',
            'habit_insurance', 'insurance_value', 'max_salary', 'min_salary'));
        $this->insurance_model->_table_name = "tbl_social_details"; //table name
        $this->insurance_model->_order_by = "social_details_id";
        $this->insurance_model->_primary_key = "social_details_id";

        $this->insurance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/insurances/social_list');
    }

    //End Social insurance
}
