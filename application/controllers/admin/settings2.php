<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings2 extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('settings_model');

        $this->load->helper('ckeditor');
        $this->lang->load('date', $this->session->userdata('lang'));

        $var = $this->session->userdata('lang');
        $this->lang->load('file', $var);
    }

    public function sms_config(){
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('sms_config'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title

        $this->settings_model->_table_name = "tbl_sms_config"; //table name
        $this->settings_model->_primary_key = "sms_config_id";
        $this->settings_model->_order_by = "sms_config_id";

        $data['sms_config'] = $this->settings_model->get(array('sms_config_id'=>1), TRUE);

        $data['subview'] = $this->load->view('admin/settings/sms_config', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_sms_config(){
        $data =$this->input->post();

        $this->settings_model->_table_name = "tbl_sms_config"; //table name
        $this->settings_model->_primary_key = "sms_config_id";
        $this->settings_model->_order_by = "sms_config_id";
        $this->settings_model->save($data, 1);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings2/sms_config');
    }

    public function sms_test(){
        $datas =$this->input->post();

        $this->settings_model->_table_name = "tbl_sms_config";
        $this->settings_model->_primary_key = "sms_config_id";
        $this->settings_model->_order_by = "sms_config_id";
        $data = $this->settings_model->get(array('sms_config_id'=>1), TRUE);
        if($data->default==1)
        {
            $login = $data->sms_login1;
            $pass = $data->sms_password1;
            $tagname = $data->sender_name1;
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $tagname,
                'RecepientNumber' => $datas['phone'],
                'Message' => "Test message from ".$tagname."\n\n\n".base_url(),
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data);

            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result);
            if($response->StatusDescription=='Success')
                echo '<div style="text-align: center;font-size: 200%;background: #a3ff38;padding: 20px;color: green;border: 1px solid;">'.$response->StatusDescription.'</div>';
            else{
                echo '<div style="text-align: center;font-size: 200%;background: #ffd1ce;padding: 20px;color: #F44336;border: 1px solid;">ERROR !<br>'.$response->StatusDescription.'</div>';
            }
        }else{
            $login = $data->sms_login2;
            $pass = $data->sms_password2;
            $sender = $data->sender_name2;
            $message =  "Test message from ".$sender." ".base_url();
            $numbers = $datas['phone'];

            $_url = 'https://www.lanasms.net/api/sendsms.php';
            $postData = 'username='.$login.'&password='.$pass.'&message='.$message.'&numbers='.$numbers.'&sender='.$sender.'&unicode=E&return=Json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

            $output=curl_exec($ch);

            curl_close($ch);
            if($output==100){
                echo '<div style="text-align: center;font-size: 200%;background: #a3ff38;padding: 20px;color: green;border: 1px solid;">SUCCESS</div>';
            } else{
                echo '<div style="text-align: center;font-size: 200%;background: #ffd1ce;padding: 20px;color: #F44336;border: 1px solid;">ERROR !<br>'.$output.'</div>';
            }
        }
    }

    public function default_sms_config($default){
        $data['default'] = $default;
        $this->settings_model->_table_name = "tbl_sms_config";
        $this->settings_model->_primary_key = "sms_config_id";
        $this->settings_model->_order_by = "sms_config_id";
        $x = $this->settings_model->save($data, 1);


        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings2/sms_config');

    }

    public function job_places($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('job_places'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['active'] = 1;

        $this->settings_model->_table_name = "tbl_job_places"; //table name
        $this->settings_model->_primary_key = "job_place_id";
        $this->settings_model->_order_by = "job_place_id";

        if ($id) {
            $data['active'] = 2;
            $data['job_place_info'] = $this->settings_model->get_by(array('job_place_id' => $id,), TRUE);
        }

        $data['all_job_places'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/view_job_places', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_job_place($id = NULL)
    {
        $this->settings_model->_table_name = "tbl_job_places"; //table name
        $this->settings_model->_primary_key = "job_place_id";
        $this->settings_model->_order_by = "job_place_id";

        $data = $this->input->post();

        $this->settings_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings2/job_places'); //redirect page
    }

    public function delete_job_places($id = NULL)
    {
        // check into application list
        $where = array('job_place_id' => $id);
        // check existing leave category into tbl_leaves
        $check_existing_ctgry = $this->settings_model->check_by($where, 'tbl_employee');
        if (!empty($check_existing_ctgry)) { // if not empty do not delete this else delete
            // messages for user
            $type = "error";
            $message = lang('job_place_used');
            set_message($type, $message);
        } else {
            $this->settings_model->_table_name = "tbl_job_places"; //table name
            $this->settings_model->_primary_key = "job_place_id";    //id
            $this->settings_model->delete($id);
            $type = "success";
            $message = lang('deleted_successfully');
            set_message($type, $message);
        }
        redirect('admin/settings2/job_places'); //redirect page
    }

    public function employee_category($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('employee_category'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['active'] = 1;

        $this->settings_model->_table_name = "tpl_employee_category"; //table name
        $this->settings_model->_primary_key = "id";
        $this->settings_model->_order_by = "id";

        if ($id) {
            $data['active'] = 2;
            $data['employee_category_info'] = $this->settings_model->get_by(array('id' => $id,), TRUE);
        }

        $data['all_employee_category'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/view_employee_category', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_employee_category($id = Null)
    {
        $this->settings_model->_table_name = "tpl_employee_category"; //table name
        $this->settings_model->_primary_key = "id";
        $this->settings_model->_order_by = "id";

        $data = $this->input->post();

        $this->settings_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings2/employee_category'); //redirect page
    }

    public function delete_employee_category($id = Null)
    {
        // check into application list
        $where = array('employee_category_id' => $id);
        // check existing leave category into tbl_leaves
        $check_existing_ctgry = $this->settings_model->check_by($where, 'tbl_employee');
        if (!empty($check_existing_ctgry)) { // if not empty do not delete this else delete
            // messages for user
            $type = "error";
            $message = lang('employee_category_used');
            set_message($type, $message);
        } else {
            $this->settings_model->_table_name = "tpl_employee_category"; //table name
            $this->settings_model->_primary_key = "id";    //id
            $this->settings_model->delete($id);
            $type = "success";
            $message = lang('deleted_successfully');
            set_message($type, $message);
        }
        redirect('admin/settings2/employee_category'); //redirect page
    }


    public function evaluation_items($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('evaluation_items'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['active'] = 1;

        if ($id) {
            $data['active'] = 2;
            $this->settings_model->_table_name = "tpl_evaluation_items"; //table name
            $this->settings_model->_primary_key = "evaluation_items_id";
            $this->settings_model->_order_by = "evaluation_items_id";
            $data['evaluation_item_info'] = $this->settings_model->get_by(array('evaluation_items_id' => $id,), TRUE);
        }

        $this->settings_model->_table_name = "tbl_department"; //table name
        $this->settings_model->_primary_key = "department_id";
        $this->settings_model->_order_by = "department_id";

        //evaluation_items_by_departements_id
        $data['all_departements'] = $this->settings_model->get();
        foreach ($data['all_departements'] as $dep) {
            $data['all_departs_ev_items'][] = $this->settings_model->evaluation_items_by_departements_id($dep->department_id);
        }

        $this->settings_model->_table_name = "tpl_evaluation_items";
        $this->settings_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->settings_model->get();

        $this->settings_model->_table_name = "tbl_job_titles";
        $this->settings_model->_order_by = "job_titles_id";
        $data['job_titles'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/view_evaluation_items', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_evaluation_item($id = NULL)
    {
        $data = $this->input->post();
        $this->settings_model->_table_name = "tpl_evaluation_items"; //table name
        $this->settings_model->_primary_key = "evaluation_items_id";
        $this->settings_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings2/evaluation_items'); //redirect page
    }

    public function delete_evaluation_item($id = NULL)
    {
        $this->settings_model->_table_name = "tpl_evaluation_items"; //table name
        $this->settings_model->_primary_key = "evaluation_items_id";

        $this->settings_model->delete($id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/settings2/evaluation_items'); //redirect page
    }

    public function organizational_chart()
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('organizational_chart'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title

        $deps = $this->settings_model->get_deps();

        for ($i = 0; $i < count($deps); $i++) {
            $desg = $this->settings_model->get_desg($deps[$i]['department_id']);
            if (!empty($desg)) {
                for ($j = 0; $j < count($desg); $j++) {
                    $units = $this->settings_model->get_units($desg[$j]['designations_id']);
                    if (!empty($units)) {
                        for ($k = 0; $k < count($units); $k++) {
                            $divs = $this->settings_model->get_divis($units[$k]['unit_id']);
                            if (!empty($divs)) {
                                for ($l = 0; $l < count($divs); $l++) {
                                    $divs[$l]['id'] = 'div;' . $divs[$l]['division_id'];
                                    $divs[$l]['name'] = lang('adivision');
                                    $divs[$l]['title'] = ($data['lang'] == 'arabic') ? $divs[$l]['division_ar'] : $divs[$l]['division_en'];
                                    $divs[$l]['name_ar'] = $divs[$l]['division_ar'];
                                    $divs[$l]['name_en'] = $divs[$l]['division_en'];
                                    unset($divs[$l]['division_id']);
                                    unset($divs[$l]['division_ar']);
                                    unset($divs[$l]['division_en']);
                                }
                                $units[$k]['children'] = $divs;
                            }
                            $units[$k]['id'] = 'unit;' . $units[$k]['unit_id'];
                            $units[$k]['name'] = lang('aunit');
                            $units[$k]['title'] = ($data['lang'] == 'arabic') ? $units[$k]['unit_ar'] : $units[$k]['unit_en'];
                            $units[$k]['name_ar'] = $units[$k]['unit_ar'];
                            $units[$k]['name_en'] = $units[$k]['unit_en'];
                            unset($units[$k]['unit_id']);
                            unset($units[$k]['unit_ar']);
                            unset($units[$k]['unit_en']);
                        }
                        $desg[$j]['children'] = $units;
                    }
                    $desg[$j]['id'] = 'des;' . $desg[$j]['designations_id'];
                    $desg[$j]['name'] = lang('adseignation');
                    $desg[$j]['title'] = ($data['lang'] == 'arabic') ? $desg[$j]['designations_ar'] : $desg[$j]['designations'];
                    $desg[$j]['name_ar'] = $desg[$j]['designations_ar'];
                    $desg[$j]['name_en'] = $desg[$j]['designations'];
                    unset($desg[$j]['designations_id']);
                    unset($desg[$j]['designations']);
                    unset($desg[$j]['designations_ar']);
                }
                $deps[$i]['children'] = $desg;
            }

            $deps[$i]['id'] = 'dep;' . $deps[$i]['department_id'];
            $deps[$i]['name'] = lang('adeprtment');
            $deps[$i]['title'] = ($data['lang'] == 'arabic') ? $deps[$i]['department_name_ar'] : $deps[$i]['department_name'];
            $deps[$i]['name_ar'] = $deps[$i]['department_name_ar'];
            $deps[$i]['name_en'] = $deps[$i]['department_name'];
            unset($deps[$i]['department_id']);
            unset($deps[$i]['department_name']);
            unset($deps[$i]['department_name_ar']);
        }

        $datasource['id'] = 'hrm;0';
        $datasource['name'] = lang('acompany');
        $datasource['title'] = 'HRM_LITE';
        $datasource['children'] = $deps;

        $data['datasource'] = $datasource;

        $data['subview'] = $this->load->view('admin/settings/view_organizational_chart', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function ajaxfuction($type, $action, $id)
    {
        $data = $this->input->post();
        $ar = $data['ar'];
        $en = $data['en'];
        //echo $type . ' - ' . $action . ' - ' . $id . ' - ' . @$ar . ' - ' . @$en;
        if ($type == 'department') {
            $this->settings_model->_table_name = "tbl_department"; //table name
            $this->settings_model->_primary_key = "department_id";
            $this->settings_model->_order_by = "department_id";
            $updata['department_name_ar'] = $ar;
            $updata['department_name'] = $en;
            /////////
            $addata['department_name_ar'] = $ar;
            $addata['department_name'] = $en;
        } elseif ($type == 'designation') {
            $this->settings_model->_table_name = "tbl_designations"; //table name
            $this->settings_model->_primary_key = "designations_id";
            $this->settings_model->_order_by = "designations_id";
            $updata['designations_ar'] = $ar;
            $updata['designations'] = $en;
            ///////////
            $addata['designations_ar'] = $ar;
            $addata['designations'] = $en;
            $addata['department_id'] = $id;
        } elseif ($type == 'unit') {
            $this->settings_model->_table_name = "tbl_units"; //table name
            $this->settings_model->_primary_key = "unit_id";
            $this->settings_model->_order_by = "unit_id";
            $updata['unit_ar'] = $ar;
            $updata['unit_en'] = $en;
            ///////////
            $addata['unit_ar'] = $ar;
            $addata['unit_en'] = $en;
            $addata['designations_id'] = $id;
        } elseif ($type == 'division') {
            $this->settings_model->_table_name = "tbl_divisions"; //table name
            $this->settings_model->_primary_key = "division_id";
            $this->settings_model->_order_by = "division_id";
            $updata['division_ar'] = $ar;
            $updata['division_en'] = $en;
            ///////////
            $addata['division_ar'] = $ar;
            $addata['division_en'] = $en;
            $addata['unit_id'] = $id;
        }
        //////////////////////////////////////////////////////
        if ($action == 'delete') {
            $this->settings_model->delete($id);
        } elseif ($action == 'update') {
            $this->settings_model->save($updata, $id);
        } elseif ($action == 'add') {
            $this->settings_model->save($addata);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function center_documentations($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('center_documentations'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['active'] = 1;

        $this->settings_model->_table_name = "tbl_center_documentations"; //table name
        $this->settings_model->_primary_key = "doc_id";
        $this->settings_model->_order_by = "doc_id";

        if ($id) {
            $data['active'] = 2;
            $data['the_doc'] = $this->settings_model->get_by(array('doc_id' => $id), TRUE);
        }
        $data['document_list'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/center_documentations', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_documents($id = NULL)
    {
        $data = $this->input->post();

        if (!empty($_FILES['link']['name'])) {
            $_FILES['id_proff']['name'] = $_FILES['link']['name'];
            $val = $this->upload_document('link');
            $data['link'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }

        $this->settings_model->_table_name = "tbl_center_documentations"; //table name
        $this->settings_model->_primary_key = "doc_id";
        $this->settings_model->_order_by = "doc_id";
        $this->settings_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/settings2/center_documentations');
    }

    public function delete_document($id)
    {
        $this->settings_model->_table_name = "tbl_center_documentations"; //table name
        $this->settings_model->_primary_key = "doc_id";
        $this->settings_model->_order_by = "doc_id";
        $doc = $this->settings_model->get_by(array('doc_id' => $id), TRUE);
        $path = base_url() . 'img/center_documentations/' . $doc->link;
        $this->settings_model->delete($id);

        unlink($path);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/settings2/center_documentations');


    }

    function upload_document($field)
    {
        $config['upload_path'] = 'img/center_documentations/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '12048';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($field)) {
            $error = $this->upload->display_errors();
            $type = "error";
            $message = $error;
            set_message($type, $message);
            return FALSE;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $this->upload->data();
            $file_data ['fileName'] = $fdata['file_name'];
            $file_data ['path'] = $config['upload_path'] . $fdata['file_name'];
            $file_data ['fullPath'] = $fdata['full_path'];
            return $file_data;
            // uploading successfull, now do your further actions
        }
    }
}
