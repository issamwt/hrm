<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('settings_model');

        $this->load->helper('ckeditor');
        $this->lang->load('date', $this->session->userdata('lang'));
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "90%",
                'height' => "400px"
            )
        );
    }

    public function general_settings($val = NULL)
    {
        $data['title'] = lang('general_settings'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['lang'] = $this->session->userdata('lang');

        //Query from DB
        $this->settings_model->_table_name = "tbl_gsettings"; //table name
        $this->settings_model->_order_by = "name";
        $val = $this->settings_model->get_by(array('id_gsettings' => 1,), TRUE);

        $this->settings_model->_table_name = "countries"; //table name
        $this->settings_model->_order_by = "countryName";
        $data['all_country'] = $this->settings_model->get();
        if ($val) { // get general info by id
            $data['ginfo'] = $val; // assign value from db
        }


        $data['subview'] = $this->load->view('admin/settings/general_settings', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_ginfo($id = NULL)
    {

        $this->settings_model->_table_name = "tbl_gsettings"; // table name
        $this->settings_model->_primary_key = "id_gsettings"; // $id

        $data = $this->settings_model->array_from_post(array('name', 'email', 'address', 'country_id', 'city', 'phone', 'mobile', 'website'));

        //image Process
        if (!empty($_FILES['logo']['name'])) {
            $old_path = $this->input->post('old_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->settings_model->uploadImage('logo');
            $val == TRUE || redirect('admin/dashboard');
            $data['logo'] = $val['path'];
            $data['full_path'] = $val['fullPath'];
        }

        $this->settings_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('general_settings_saved');
        set_message($type, $message);
        redirect('admin/dashboard');
    }

    public function set_working_days()
    {

        $data['title'] = lang('set_working_days');
        $data['page_header'] = lang('settings_management'); //Page header title
        // get all days
        $this->settings_model->_table_name = "tbl_days"; //table name
        $this->settings_model->_order_by = "day_id";
        $data['days'] = $this->settings_model->get();
        // get all working days
        $data['working_days'] = $this->settings_model->get_working_days();

        // get all days
        $this->settings_model->_table_name = "tbl_working_hours"; //table name
        $this->settings_model->_order_by = "working_hours_id";
        $data['working_hours'] = $this->settings_model->get_by(array('working_hours_id' => 1), TRUE);

        $data['subview'] = $this->load->view('admin/settings/set_working_days', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_working_days()
    {
        // delete all working days after save and again save
        $this->settings_model->delete_all('tbl_working_days');
        $day_id = $this->input->post('day', TRUE);
        $day = $this->input->post('day_id', TRUE);
        $this->settings_model->_table_name = "tbl_working_days"; // table name
        $this->settings_model->_primary_key = "working_days_id"; // $id
        if (!empty($day)) {
            foreach ($day as $value) {
                $data['flag'] = 0;
                $data['day_id'] = $value;
                if (!empty($day_id)) {
                    foreach ($day_id as $days) {
                        if ($value == $days) {
                            $data['flag'] = 1;
                        }
                    }
                }
                $this->settings_model->save($data);
            }
        }
        //To display confimation message.
        $type = "success";
        $message = lang('working_dasy_saved');
        set_message($type, $message);
        redirect('admin/settings/set_working_days');
    }

    public function save_working_hours($id = NULL)
    {
        $adata = $this->settings_model->array_from_post(array('start_hours', 'end_hours')); //input post

        $data['start_hours'] = date('H:i', strtotime($adata['start_hours']));
        $data['end_hours'] = date('H:i', strtotime($adata['end_hours']));

        $this->settings_model->_table_name = "tbl_working_hours";
        $this->settings_model->_primary_key = "working_hours_id";
        $this->settings_model->save($data, $id);
        $type = "success";
        $message = lang('working_hour_saved');
        set_message($type, $message);
        redirect('admin/settings/set_working_days');
    }

    public function holiday_list($flag = NULL, $id = NULL)
    {
        $data['title'] = lang('holiday_list');
        $data['page_header'] = lang('settings_management');

        $this->settings_model->_table_name = "tbl_holiday"; //table name
        $this->settings_model->_order_by = "holiday_id";
        // get holiday list by id
        if (!empty($id)) {
            $data['holiday_list'] = $this->settings_model->get_by(array('holiday_id' => $id,), TRUE);

            if (empty($data['holiday_list'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/settings/holiday_list');
            }
        }// click add holiday theb show
        if (!empty($flag)) {
            $data['active_add_holiday'] = $flag;
        }
        // active check with current month
        $data['current_month'] = date('m');
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        // get all holiday list by year and month
        $data['all_holiday_list'] = $this->get_holiday_list($data['year']);  // get current year
        // retrive all data from db
        $data['subview'] = $this->load->view('admin/settings/holiday_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function add_holiday_modal()
    {
        // active check with current month
        $data['current_month'] = date('m');
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        $data['modal_subview'] = $this->load->view('admin/settings/_modal_add_holiday', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function get_holiday_list($year)
    {// this function is to create get monthy recap report
        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_holiday_list[$i] = $this->settings_model->get_holiday_list_by_date($start_date, $end_date); // get all report by start date and in date
        }
        return $get_holiday_list; // return the result
    }

    public function save_holiday($id = NULL)
    {

        $this->settings_model->_table_name = "tbl_holiday"; //table name
        $this->settings_model->_primary_key = "holiday_id";    //id
        // input data
        $data = $this->settings_model->array_from_post(array('event_name', 'description', 'start_date', 'end_date')); //input post
        // dublicacy check into database
        if (!empty($id)) {
            $holiday_id = array('holiday_id !=' => $id);
        } else {
            $holiday_id = null;
        }
        $where = array('event_name' => $data['event_name'], 'start_date' => $data['start_date']); // where
        // check holiday by where
        // if not empty show alert message else save data
        $check_holiday = $this->settings_model->check_update('tbl_holiday', $where, $holiday_id);

        if (!empty($check_holiday)) {
            $type = "error";
            $message = lang('holiday_information_exist');
            set_message($type, $message);
        } else {
            $this->settings_model->save($data, $id);
            // messages for user
            $type = "success";
            $message = lang('holiday_information_saved');
            set_message($type, $message);
        }

        redirect('admin/settings/holiday_list'); //redirect page
    }

    public function delete_holiday_list($id)
    { // delete holiday list by id
        $this->settings_model->_table_name = "tbl_holiday"; //table name
        $this->settings_model->_primary_key = "holiday_id";    //id
        $this->settings_model->delete($id);
        $type = "success";
        $message = lang('holoday_information_delete');
        set_message($type, $message);
        redirect('admin/settings/holiday_list'); //redirect page
    }


    public function update_profile($action = NULL)
    {
        $data['title'] = lang('update_profile');
        $data['page_header'] = lang('update_profile');
        if (!empty($action)) {
            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/settings/update_profile', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function profile_updated()
    {
        $employee_id = $this->session->userdata('employee_id');
        $data['user_name_ar'] = $this->input->post('user_name_ar');
        $data['user_name_en'] = $this->input->post('user_name_en');
        $data['user_name'] = $this->input->post('user_name');
        $this->settings_model->_table_name = 'tbl_user';
        $this->settings_model->_primary_key = 'user_id';
        $this->settings_model->save($data, $employee_id);
        $type = "success";
        $message = lang('profile_information_updated');
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    public function set_password()
    {
        $employee_id = $this->session->userdata('employee_id');
        $data['password'] = $this->hash($this->input->post('new_password'));
        $this->settings_model->_table_name = 'tbl_user';
        $this->settings_model->_primary_key = 'user_id';
        $this->settings_model->save($data, $employee_id);
        $type = "success";
        $message = lang('password_updated');
        set_message($type, $message);
        redirect('admin/settings/update_profile'); //redirect page
    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function job_titles($id = NULL)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('job_titles');
        $data['page_header'] = lang('settings_management');
        $data['active'] = 1;

        $this->settings_model->_table_name = "tbl_job_titles"; // table name
        $this->settings_model->_order_by = "job_titles_id";
        $data['job_titles_details'] = $this->settings_model->get();
        ////////////////
        //echo "<br><pre>";
        //print_r($data['job_titles_details']);
        //echo "</pre><br>";
        ////////////////
        if (!empty($id)) {
            $data['active'] = 2;
            $this->settings_model->_table_name = "tbl_job_titles"; // table name
            $this->settings_model->_order_by = "job_titles_id";
            $data['job_title_info'] = $this->settings_model->get_by(array('job_titles_id' => $id), TRUE);

            $this->settings_model->_table_name = "tpl_evaluation_items";
            $this->settings_model->_order_by = "evaluation_items_id";
            $data['evaluation_items'] = $this->settings_model->get_by(array('department_id' => 0,'job_titles_id'=>$id));
        }

        $data['subview'] = $this->load->view('admin/settings/view_job_titles', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_job_title($id = NULL)
    {
        if (empty($id)) {
            $jtar = $this->input->post('job_titles_name_ar');
            $jten = $this->input->post('job_titles_name_en');
            $this->db->where("job_titles_name_ar = '$jtar' OR job_titles_name_en = '$jten'");
            $check = $this->db->get('tbl_job_titles')->result();
            if (!empty($check)) {
                $type = "error";
                $message = lang('job_title_used');
                set_message($type, $message);
                redirect('admin/settings/job_titles');
            }
        }

        $this->settings_model->_table_name = "tbl_job_titles"; // table name
        $this->settings_model->_order_by = "job_titles_id";
        $this->settings_model->_primary_key = "job_titles_id";

        $data = $this->input->post();

        $this->settings_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/settings/job_titles');
    }

    public function save_mission($jt){
        $data = $this->input->post();
        $data['department_id']=0;
        $data['type']=1;
        $data['job_titles_id']=$jt;

        $this->settings_model->_table_name = "tpl_evaluation_items"; //table name
        $this->settings_model->_primary_key = "evaluation_items_id";
        $this->settings_model->save($data);
        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/settings/job_titles/'.$jt);
    }

    public function delete_mission($jt, $ev){
        $this->settings_model->_table_name = "tpl_evaluation_items"; //table name
        $this->settings_model->_primary_key = "evaluation_items_id";
        $this->settings_model->delete($ev);
        // messages for user
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/settings/job_titles/'.$jt);
    }

    public function delete_job_title($id)
    {
        $this->settings_model->_table_name = "tbl_job_titles"; // table name
        $this->settings_model->_primary_key = "job_titles_id"; // $id
        $this->settings_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/settings/job_titles');
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function database_backup()
    {
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => 'HR-lite_db_backup.sql'
        );
        $backup = &$this->dbutil->backup($prefs);
        $db_name = 'HR-lite_backup_' . date("d-m-Y") . '.zip';
        //$save = 'img/uploads/' . $db_name;
        $this->load->helper('file');
        //write_file($save, $backup);
        $this->load->helper('download');
        force_download($db_name, $backup);
    }

    public function save_leave_category($id = NULL)
    {

        $this->settings_model->_table_name = "tbl_leave_category"; //table name
        $this->settings_model->_primary_key = "leave_category_id";    //id
        $this->settings_model->_order_by = "leave_category_id";

        // input data
        $data = $this->input->post();

        $data['allowance_cat_ids'] = (!empty($data['allowance_cat_ids'])) ? (implode(';', $data['allowance_cat_ids'])) : '0';

        echo '<pre>';
        print_r($data);
        echo '</pre>';


        $this->settings_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = lang('leave_category_saved');
        set_message($type, $message);

        redirect('admin/settings/leave_category'); //redirect page
    }

    public function delete_leave_category($id)
    {
        // check into application list
        $where = array('leave_category_id' => $id);
        // check existing leave category into tbl_leaves
        $check_existing_ctgry = $this->settings_model->check_by($where, 'tbl_leaves');
        if (!empty($check_existing_ctgry)) { // if not empty do not delete this else delete
            // messages for user
            $type = "error";
            $message = lang('leave_category_used');
            set_message($type, $message);
        } else {
            $this->settings_model->_table_name = "tbl_leave_category"; //table name
            $this->settings_model->_primary_key = "leave_category_id";    //id
            $this->settings_model->delete($id);
            $type = "success";
            $message = lang('leave_category_deleted');
            set_message($type, $message);
        }
        redirect('admin/settings/leave_category'); //redirect page
    }

    public function leave_category($id = NULL)
    {
        $data['title'] = lang('leave_category');
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['lang'] = $this->session->userdata('lang');
        $data['active'] = 1;

        $this->settings_model->_table_name = "tbl_leave_category"; //table name
        $this->settings_model->_order_by = "leave_category_id";

        // retrive data from db by id
        if (!empty($id)) {
            $data['active'] = 2;
            $data['leave_category'] = $this->settings_model->get_by(array('leave_category_id' => $id,), TRUE);

            if (empty($data['leave_category'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/settings/leave_category');
            }
        }
        // retrive all data from db
        $data['all_leave_category'] = $this->settings_model->get();

        $this->settings_model->_table_name = "tbl_allowance_category";
        $this->settings_model->_order_by = "allowance_id";
        $data['all_allowance_categories'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/view_leave_category', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function irrigularity_category($id = null)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('irrigularity_category');
        $data['page_header'] = lang('settings_management'); //Page header title
        $data['active'] = 1;

        $this->settings_model->_table_name = "tbl_irrigularity_category"; //table name
        $this->settings_model->_order_by = "irrigularity_category_id";

        if ($id) {
            $data['active'] = 2;
            $data['irrigularity_categories_info'] = $this->settings_model->get_by(array('irrigularity_category_id' => $id,), TRUE);
        }

        $data['all_irrigularity_categories'] = $this->settings_model->get();

        $data['subview'] = $this->load->view('admin/settings/view_irrigularity_category', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_irrigularity_category($id = null)
    {
        $this->settings_model->_table_name = "tbl_irrigularity_category"; //table name
        $this->settings_model->_primary_key = "irrigularity_category_id";    //id
        $this->settings_model->_order_by = "irrigularity_category_id";

        // input data
        $data = $this->input->post();

        $this->settings_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings/irrigularity_category'); //redirect page
    }

    public function delete_irrigularity_category($id = null)
    {
        // check into application list
        $where = array('irrigularity_category_id' => $id);
        // check existing leave category into tbl_leaves
        $check_existing_ctgry = $this->settings_model->check_by($where, 'tbl_irrigularities');
        if (!empty($check_existing_ctgry)) { // if not empty do not delete this else delete
            // messages for user
            $type = "error";
            $message = lang('irrigularity_category_used');
            set_message($type, $message);
        } else {
            $this->settings_model->_table_name = "tbl_irrigularity_category"; //table name
            $this->settings_model->_primary_key = "irrigularity_category_id";    //id
            $this->settings_model->delete($id);
            $type = "success";
            $message = lang('irrigularity_category_deleted');
            set_message($type, $message);
        }
        redirect('admin/settings/irrigularity_category'); //redirect page
    }

    public function automatic_reminder($id = null)
    {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('automatic_reminder');
        $data['page_header'] = lang('settings_management'); //Page header title

        $this->settings_model->_table_name = "tbl_languages"; //table name
        $this->settings_model->_order_by = "code";
        $data['all_langs'] = $this->settings_model->get();

        $this->settings_model->_table_name = "tbl_reminder"; //table name
        $this->settings_model->_order_by = "reminder_id";

        $val = $this->settings_model->get_by(array('reminder_id' => 1,), TRUE);
        if ($val) {
            $data['rinfo'] = $val;
        }

        $data['subview'] = $this->load->view('admin/settings/automatic_reminder', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_automatic_reminder($id = null)
    {
        $this->settings_model->_table_name = "tbl_reminder";
        $this->settings_model->_primary_key = "reminder_id";

        $data = $this->input->post();
        $data['reminder_mail'] = (@$data['reminder_mail']) ? 1 : 0;
        $data['reminder_employee_fixed'] = (@$data['reminder_employee_fixed']) ? 1 : 0;

        $this->settings_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/settings/automatic_reminder');
    }
}
