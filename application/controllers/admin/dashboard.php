<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author pc mart ltd
 */
class Dashboard extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('settings_model');
        $this->lang->load('date', $this->session->userdata('lang'));
        $this->lang->load('calendar', $this->session->userdata('lang'));
    }

    public function index() {

        $data['title'] = lang('dashboard'); //Page title
        $data['page_header'] = lang('dashboard'); //Page header title
        $data['lang'] = $this->session->userdata('lang');
        // branches
        $data['branches'] = $this->db->from('tbl_branches')->get()->num_rows();
        $data['departments_ids'] = $this->db->select('department_id')->from('tbl_department')->get()->result();
        $data['departments'] =  $this->db->select('department_id')->from('tbl_department')->get()->num_rows();
        $data['sections'] = $this->db->from('tbl_designations')->get()->num_rows();
        $data['job_titles'] = $this->db->from('tbl_job_titles')->get()->num_rows();
        $data['leave_category'] = $this->db->from('tbl_leave_category')->get()->num_rows();
        $data['irrigularity_category'] = $this->db->from('tbl_irrigularity_category')->get()->num_rows();
        $data['job_places'] = $this->db->from('tbl_job_places')->get()->num_rows();
        $data['employee_category'] = $this->db->from('tpl_employee_category')->get()->num_rows();
        $ids = array();
        foreach ($data['departments_ids'] as $id){
            array_push($ids, $id->department_id);
        }
        $data['evaluation_items'] = $this->db->from('tpl_evaluation_items')->where_in('department_id', $ids)->get()->num_rows();

        $data['approvals_cats'] = $this->db->from('tbl_approvals_cat')->get()->num_rows();







        $data['get_inbox_message'] = $this->db->select('*')->from('tbl_inbox')->order_by("inbox_id", "desc")->limit(5)->get()->result();

        $data['applications_list'] =  $this->db->select('*')->from('tbl_applications')->order_by("application_id", "desc")->limit(5)->get()->result();

        $this->admin_model->_table_name = "tbl_employee"; //table name
        $this->admin_model->_order_by = "employee_id";
        $data['all'] = $this->admin_model->get();

        $this->admin_model->_table_name = "tbl_approvals_cat"; //table name
        $this->admin_model->_order_by = "approval_cat_id";
        $data['approval_cats'] = $this->admin_model->get();

        $data['subview'] = $this->load->view('admin/main_content', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function get_expense_list($year) {// this function is to create get monthy recap report
        for ($i = 1; $i <= 12; $i++) { // query for months
            if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $i . '-' . '01';
                $end_date = $year . "-" . '0' . $i . '-' . '31';
            } else {
                $start_date = $year . "-" . $i . '-' . '01';
                $end_date = $year . "-" . $i . '-' . '31';
            }
            $get_expense_list[$i] = $this->admin_model->get_expense_list_by_date($start_date, $end_date); // get all report by start date and in date
        }
        return $get_expense_list; // return the result
    }

    public function set_language($lang) {
        $this->session->set_userdata('lang', $lang);
        redirect($_SERVER["HTTP_REFERER"]);
    }

}
