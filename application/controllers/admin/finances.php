<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Finances extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('finance_model');
    }
    
    
     public function bonuses_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('bonuses_list');
        $data['page_header'] = lang('bonuses_page_header');
        
        $this->finance_model->_table_name = "tbl_bonuses"; //table name
        $this->finance_model->_order_by = "bonuse_id";
         $data['bonuses_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_bonuses"; //table name
             $this->finance_model->_order_by = "bonuse_id";

            $data['finance_info'] = $this->finance_model->get_by(array('bonuse_id' => $id), TRUE);
            if (empty($data['finance_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_finance');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/bonuses_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    
    public function save_bonuse($id = NULL) {
       
        $data = $this->finance_model->array_from_post(array('title_ar','title_en','assurance','guarantee','departure','brief_ar','brief_en'));
        $this->finance_model->_table_name = "tbl_bonuses"; //table name
        $this->finance_model->_order_by = "bonuse_id";
        $this->finance_model->_primary_key = "bonuse_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/bonuses_list');
    }
    
     public function delete_bonuse($id) {
        $this->finance_model->_table_name = "tbl_bonuses"; // table name
        $this->finance_model->_primary_key = "bonuse_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/bonuses_list');
    }
    //End Bonuses
    
    //Extra work
    public function extra_work_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('extra_work');
        $data['page_header'] = lang('extra_page_header');
        
        $this->finance_model->_table_name = "tbl_extra_work"; //table name
        $this->finance_model->_order_by = "extra_work_id";
         $data['works_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_extra_work"; //table name
             $this->finance_model->_order_by = "extra_work_id";

            $data['work_info'] = $this->finance_model->get_by(array('extra_work_id' => $id), TRUE);
            if (empty($data['work_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_extra');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/extra_work_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    
    public function save_extra_work($id = NULL) {
       
        $data = $this->finance_model->array_from_post(array('title_ar','title_en','work_value'));
        $this->finance_model->_table_name = "tbl_extra_work"; //table name
        $this->finance_model->_order_by = "extra_work_id";
        $this->finance_model->_primary_key = "extra_work_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/extra_work_list');
    }
    
     public function delete_extra_work($id) {
        $this->finance_model->_table_name = "tbl_extra_work"; // table name
        $this->finance_model->_primary_key = "extra_work_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/extra_work_list');
    }
    
 //End Extra work
    //Start Financial Deduction
    public function deduction_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('deduction_title');
        $data['page_header'] = lang('deduction_page_header');
        
        $this->finance_model->_table_name = "tbl_deduction_category"; //table name
        $this->finance_model->_order_by = "deduction_id";
         $data['deductions_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_deduction_category"; //table name
             $this->finance_model->_order_by = "deduction_id";

            $data['deduction_info'] = $this->finance_model->get_by(array('deduction_id' => $id), TRUE);
            if (empty($data['deduction_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_deduction');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/deduction_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   
    public function save_deduction($id = NULL) {
       
        $data = $this->finance_model->array_from_post(array('deduction_title_ar','deduction_title_en','deduction_type','deduction_value'));
        $this->finance_model->_table_name = "tbl_deduction_category"; //table name
        $this->finance_model->_order_by = "deduction_id";
        $this->finance_model->_primary_key = "deduction_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/deduction_list');
    }
    
     public function delete_deduction($id) {
        $this->finance_model->_table_name = "tbl_deduction_category"; // table name
        $this->finance_model->_primary_key = "deduction_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/deduction_list');
    }
    // End Financial Deduction
    
     //Start Other provisions
    public function provision_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('provision_title');
        $data['page_header'] = lang('provision_page_header');
        
        $this->finance_model->_table_name = "tbl_provision_category"; //table name
        $this->finance_model->_order_by = "provision_category_id";
         $data['provisions_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_provision_category"; //table name
             $this->finance_model->_order_by = "provision_category_id";

            $data['provision_info'] = $this->finance_model->get_by(array('provision_category_id' => $id), TRUE);
            if (empty($data['provision_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_provision');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/provision_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   
    public function save_provision($id = NULL) {
       
        $data = $this->input->post();
        $this->finance_model->_table_name = "tbl_provision_category"; //table name
        $this->finance_model->_order_by = "provision_category_id";
        $this->finance_model->_primary_key = "provision_category_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/provision_list');
    }
    
     public function delete_provision($id) {
        $this->finance_model->_table_name = "tbl_provision_category"; // table name
        $this->finance_model->_primary_key = "provision_category_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/provision_list');
    }
    // End Other provision
    // 
      //Start Types of advances
    public function advance_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('advance_title');
        $data['page_header'] = lang('advance_page_header');
        
        $this->finance_model->_table_name = "tbl_advance"; //table name
        $this->finance_model->_order_by = "advance_id";
         $data['advances_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_advance"; //table name
             $this->finance_model->_order_by = "advance_id";

            $data['advance_info'] = $this->finance_model->get_by(array('advance_id' => $id), TRUE);
            if (empty($data['advance_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_provision');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/advance_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   
    public function save_advance($id = NULL) {
       
        $data = $this->finance_model->array_from_post(array('title_ar','title_en'));
        $this->finance_model->_table_name = "tbl_advance"; //table name
        $this->finance_model->_order_by = "advance_id";
        $this->finance_model->_primary_key = "advance_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/advance_list');
    }
    
     public function delete_advance($id) {
        $this->finance_model->_table_name = "tbl_advance"; // table name
        $this->finance_model->_primary_key = "advance_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/advance_list');
    }
    // End Other provision
	
	 //Start Allowance 
    public function allowance_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('bonuses_page_header');
        $data['page_header'] = lang('bonuses_page_header');
        
        $this->finance_model->_table_name = "tbl_allowance_category"; //table name
        $this->finance_model->_order_by = "allowance_id";
         $data['allowances_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_allowance_category"; //table name
             $this->finance_model->_order_by = "allowance_id";

            $data['allowance_info'] = $this->finance_model->get_by(array('allowance_id' => $id), TRUE);
            if (empty($data['allowance_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/allowance_list');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/allowance_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   
    public function save_allowance($id = NULL) {
       
        $data = $this->input->post();
        $this->finance_model->_table_name = "tbl_allowance_category"; //table name
        $this->finance_model->_order_by = "allowance_id";
        $this->finance_model->_primary_key = "allowance_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/allowance_list');
    }
    
     public function delete_allowance($id) {
        $this->finance_model->_table_name = "tbl_allowance_category"; // table name
        $this->finance_model->_primary_key = "allowance_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/allowance_list');
    }
    // End Financial allowance
	 //Start discount 
    public function discount_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('discount_title');
        $data['page_header'] = lang('discount_page_header');
        
        $this->finance_model->_table_name = "tbl_discount_category"; //table name
        $this->finance_model->_order_by = "discount_id";
         $data['discounts_info'] = $this->finance_model->get();
        
        if (!empty($id)) {// retrive data from db by id 
            $data['active'] = 2;
             $this->finance_model->_table_name = "tbl_discount_category"; //table name
             $this->finance_model->_order_by = "discount_id";

            $data['discount_info'] = $this->finance_model->get_by(array('discount_id' => $id), TRUE);
            if (empty($data['discount_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/finance/add_discount');
            }
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/finances/discount_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   
    public function save_discount($id = NULL) {
       
        $data = $this->finance_model->array_from_post(array('discount_title_ar','discount_title_en','discount_type','discount_value'));
        $this->finance_model->_table_name = "tbl_discount_category"; //table name
        $this->finance_model->_order_by = "discount_id";
        $this->finance_model->_primary_key = "discount_id";
        
        $this->finance_model->save($data, $id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/finances/discount_list');
    }
    
     public function delete_discount($id) {
        $this->finance_model->_table_name = "tbl_discount_category"; // table name
        $this->finance_model->_primary_key = "discount_id";
        $this->finance_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/finances/discount_list');
    }
    // End Financial discount
}