<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Global_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('global_model');
        $this->load->model('admin_model');
    }

    public function get_employee_by_designations_id($designation_id) {
        $HTML = NULL;
        $this->admin_model->_table_name = 'tbl_employee';
        $this->admin_model->_order_by = 'designations_id';
        $employee_info = $this->admin_model->get_by(array('designations_id' => $designation_id, 'status' => '1'), FALSE);
        if (!empty($employee_info)) {
            foreach ($employee_info as $v_employee_info) {
                $HTML.="<option value='" . $v_employee_info->employee_id . "'>" . $v_employee_info->first_name . ' ' . $v_employee_info->last_name . "</option>";
            }
        }
        echo $HTML;
    }

    public function check_duplicate_emp_id($val) {
        $check_dupliaction_id = $this->admin_model->check_by(array('employment_id' => $val), 'tbl_employee');

        if (!empty($check_dupliaction_id)) {
            $result = '<small style="padding-left:10px;color:red;font-size:10px">Employee ID Already Exist !<small>';
        } else {
            $result = NULL;
        }
        echo $result;
    }

    public function check_current_password($val) {
        $password = $this->hash($val);
        $check_dupliaction_id = $this->admin_model->check_by(array('password' => $password), 'tbl_user');
        if (empty($check_dupliaction_id)) {
            $result = '<small style="padding-left:10px;color:red;font-size:10px">Your Entered Password Do Not Match !<small>';
        } else {
            $result = NULL;
        }
        echo $result;
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function get_item_name_by_id($stock_sub_category_id) {
        $HTML = NULL;
        $this->admin_model->_table_name = 'tbl_stock';
        $this->admin_model->_order_by = 'stock_sub_category_id';
        $stock_info = $this->admin_model->get_by(array('stock_sub_category_id' => $stock_sub_category_id, 'total_stock >=' => '1'), FALSE);
        if (!empty($stock_info)) {
            foreach ($stock_info as $v_stock_info) {
                $HTML.="<option value='" . $v_stock_info->stock_id . "'>" . $v_stock_info->item_name . "</option>";
            }
        }
        echo $HTML;
    }

    public function check_existing_user_name($user_name, $user_id = null) {
        $result = $this->global_model->check_user_name($user_name, $user_id);
        if ($result) {
            echo 'This User Name is Exist!';
        }
    }


}
