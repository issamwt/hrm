<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        // $this->load->library('encryption');
    }

    public function users_list() {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('users_list');
        $data['page_header'] = lang('users_permissions_system');

        // Get Users
        $this->users_model->_table_name = "tbl_user"; //table name
        $this->users_model->_order_by = "user_id";
        $data['users_list'] = $this->users_model->get();

        $data['subview'] = $this->load->view('admin/users/users_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function add_user($id = NULL) {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('add_user');
        $data['page_header'] = lang('users_permissions_system');
        $data['user_info'] = array();

        if (!empty($id)) {
            $data['title'] = lang('edit_user');
            $this->users_model->_table_name = "tbl_user"; //table name
            $this->users_model->_order_by = "user_id";
            $data['user_info'] = $this->users_model->get_by(array('user_id' => $id), TRUE);
        }

        // Get Users
        $this->users_model->_table_name = "tbl_menu"; //table name
        $this->users_model->_order_by = "menu_id";
        $menu_info = $this->users_model->get();

        foreach ($menu_info as $items) {
            $menu['parents'][$items->parent][] = $items;
        }

        $data['result'] = $this->buildChild(0, $menu);

        if ($data['user_info']) {
            $role = $this->users_model->select_user_roll_by_employee_id($id);
            if ($role) {
                foreach ($role as $value) {
                    $result[$value->menu_id] = $value->menu_id;
                }
                $data['roll'] = $result;
            }
        }


        $data['subview'] = $this->load->view('admin/users/create_user', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function buildChild($parent, $menu) {

        if (isset($menu['parents'][$parent])) {

            foreach ($menu['parents'][$parent] as $ItemID) {

                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    public function save_user($id = null) {
        $data = $this->input->post();
        $data['flag'] = 1;


        if (empty($id)) {
            $data['password'] = $this->encryption->hash($data['password']);
        }
        //delete existing userroll by login id
        if (!empty($id)) {
            $this->users_model->_table_name = 'tbl_user_role'; //table name
            $this->users_model->_order_by = 'user_id';
            $this->users_model->_primary_key = 'user_role_id';
            $roll = $this->users_model->get_by(array('user_id' => $id), false);
            foreach ($roll as $v_roll) {
                $this->users_model->delete($v_roll->user_role_id);
            }
        }

        if (!empty($data['menu'])) {
            $menu['menu'] = $data['menu'];
            unset($data['menu']);
        }

        // save the user
        $this->users_model->_table_name = "tbl_user"; // table name
        $this->users_model->_primary_key = "user_id"; // $id
        if (!empty($id)) {
            $new_id = $this->users_model->save($data, $id);
        } else {
            $new_id = $this->users_model->save($data);
        }

        $this->users_model->_table_name = "tbl_user_role"; // table name
        $this->users_model->_primary_key = "user_role_id";
        if (!empty($menu['menu'])) {
            foreach ($menu as $v_menu) {
                foreach ($v_menu as $value) {
                    $mdata['menu_id'] = $value;
                    $mdata['user_id'] = $new_id;
                    $this->users_model->save($mdata);
                }
            }
        }

        $type = "success";
        if (!empty($id)) {
            $message = lang('user_updated');
        } else {
            $message = lang('user_created');
        }

        set_message($type, $message);
        redirect('admin/users/users_list');
    }

    public function delete_user($id = NULL) {
        $this->users_model->_table_name = 'tbl_user_role'; //table name
        $this->users_model->_order_by = 'user_id';
        $this->users_model->_primary_key = 'user_role_id';
        $roll = $this->users_model->get_by(array('user_id' => $id), false);
        foreach ($roll as $v_roll) {
            $this->users_model->delete($v_roll->user_role_id);
        }


        $this->users_model->_table_name = 'tbl_user'; //table name
        $this->users_model->_order_by = 'user_id';
        $this->users_model->_primary_key = 'user_id';

        $this->users_model->delete($id);
        $type = "success";
        $message = lang('stock_deleted');


        set_message($type, $message);
        redirect('admin/users/users_list');
    }

}
