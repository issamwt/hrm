<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of department
 *
 * @author Ashraf
 */
class Designation extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('designation_model');
    }

    public function designation_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('designations_list');
        $data['page_header'] = lang('department_page_header');

        if ($id) {
            $data['active'] = 2;
            $data['designation_info'] = $this->designation_model->get_designation_with_departments($id);
        }

        $this->designation_model->_table_name = "tbl_employee"; //table name
        $this->designation_model->_order_by = "employee_id";
        $data['employee_info'] = $this->designation_model->get();

        $this->designation_model->_table_name = "tbl_department"; //table name
        $this->designation_model->_order_by = "department_id";
        $data['departement_info'] = $this->designation_model->get();

        $data['designations_details'] = $this->designation_model->get_designation_with_departments();

        $data['subview'] = $this->load->view('admin/designation/designation_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_designation($id = NULL) {
        $this->designation_model->_table_name = "tbl_designations"; // table name
        $this->designation_model->_order_by = "designations_id";
        $this->designation_model->_primary_key = "designations_id";
        $data = $this->input->post();
        $data['employee_id'] = $data['employee_id_non_required'];
        unset($data['employee_id_non_required']);

        $this->designation_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/designation/designation_list');
    }

    public function delete_designation($id) {
        $this->designation_model->_table_name = "tbl_designations"; // table name
        $this->designation_model->_primary_key = "designations_id"; // $id
        $this->designation_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/designation/designation_list');
    }

}
