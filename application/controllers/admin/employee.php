<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Ashraf
 */
class Employee extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('employee_model');
        $this->load->model('approvals_model');
    }

    public function employees($id = NULL) {
        $data['hijri_calendar'] = 'TRUE';
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('employee_list');
        $data['page_header'] = lang('employee_page_header'); //Page header title
        $data['active'] = 1;

        if($id){
            $data['active'] = 2;
            $data['employee_info'] = $this->employee_model->all_emplyee_info($id);
        }

        $data['employee_list'] = $this->employee_model->all_emplyee_info();

        $this->db->select("employment_id");
        $this->db->from("tbl_employee");
        $data['employment_ids'] = $this->db->get()->result();

        $this->employee_model->_table_name = "countries";
        $this->employee_model->_order_by = "countryName";
        $data['all_countries'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_department';
        $this->employee_model->_order_by = "department_id";
        $data['department_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tpl_employee_category';
        $this->employee_model->_order_by = "id";
        $data['emp_cat_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_job_places';
        $this->employee_model->_order_by = "job_place_id";
        $data['job_place_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_job_titles';
        $this->employee_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = "tbl_employee"; // table name
        $this->employee_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->employee_model->get();

        $today = date('Y-m-d');
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $data['subview'] = $this->load->view('admin/employee/employee_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function get_sec_with_dep($dep_id)
    {
        $this->employee_model->_table_name = "tbl_designations"; //table name
        $this->employee_model->_primary_key = "designations_id";
        $this->employee_model->_order_by = "designations_id";
        $result = $this->employee_model->get_by(array('department_id' => $dep_id));
        echo json_encode($result);
    }

    public function delete_employee($emp_id) {
        $this->employee_model->_table_name = 'tbl_employee';
        $this->employee_model->_primary_key = "employee_id";
        $this->employee_model->delete($emp_id);

        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('employee/dashboard/employee_list');
    }

    public function profile($id = NULL)
    {
        $data['title'] = lang('employee_list');
        $data['page_header'] = lang('employee_page_header');
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('view_profile');
        $employee_id = $this->session->userdata('employee_id');

        if ($id) {
            $data['employee_details'] = $this->employee_model->all_emplyee_info($id);
            $this->employee_model->_table_name = 'tbl_courses';
            $this->employee_model->_order_by = "course_id";
            $data['courses_list'] = $this->employee_model->get_by(array('employeetr_id' =>$id));
        } else {
            $data['employee_details'] = $this->employee_model->all_emplyee_info($employee_id);
            $this->employee_model->_table_name = 'tbl_courses';
            $this->employee_model->_order_by = "course_id";
            $data['courses_list'] = $this->employee_model->get_by(array('employeetr_id' =>$id));
        }

        $this->employee_model->_table_name = "tpl_evaluation_items";
        $this->employee_model->_order_by = "evaluation_items_id";
        $data['evaluation_items'] = $this->employee_model->get_by(array('department_id' => 0,'job_titles_id'=>$data['employee_details']->job_title));


        $this->employee_model->_table_name = 'tbl_leave_category';
        $this->employee_model->_order_by = "leave_category_id";
        $data['leaves_cat'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_leaves';
        $this->employee_model->_order_by = "leave_id";
        $data['leaves_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_employee_custody';
        $this->employee_model->_order_by = "custody_id";
        $data['custodies_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_allowance_category';
        $this->employee_model->_order_by = "allowance_id";
        $data['allowances_cat_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_allowance';
        $this->employee_model->_order_by = "allowance_type_id";
        $data['allowances_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_deduction_category';
        $this->employee_model->_order_by = "deduction_id";
        $data['deduction_category_list'] = $this->employee_model->get();


        $this->employee_model->_table_name = 'tbl_deduction';
        $this->employee_model->_primary_key = "deduction_type_id";
        $data['deduction_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_provision_category';
        $this->employee_model->_order_by = "provision_category_id";
        $data['provision_cat_list'] = $this->employee_model->get();

        $this->employee_model->_table_name = 'tbl_provision';
        $this->employee_model->_primary_key = "provision_id";
        $data['provision_list'] = $this->employee_model->get();

        $data['all'] = $this->db->get('tbl_employee')->result();

        $data['subview'] = $this->load->view('admin/employee/user_profile', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_employee($id = NULL) {
        ///////////////////////////////////////////////////////////
        /////////////////////// prepare data /////////////////////
        ///////////////////////////////////////////////////////////
        $data = $this->input->post();
        echo '<pre>';
        print_r($data);
        echo '</pre>';

        $today = date('Y-m-d');
        $today = date_create_from_format('Y-m-d', $today)->format('Y-m-d');
        $today =  str_replace('-','',$today);
        $passport_end =  str_replace('-','',$data['passport_end']);
        $identity_end =  str_replace('-','',$data['identity_end']);


        //$test = $this->employee_model->get_by(array('employment_id' => $data['employment_id']));
        if($id){
            echo 'update<br>';
            $test = $this->db->select('*')->from('tbl_employee')->where('employment_id',$data['employment_id'])->where(array('employee_id !='=> $id))->get()->result();
        }
        else{
            echo 'add<br>';
            $test = $this->db->select('*')->from('tbl_employee')->where('employment_id',$data['employment_id'])->get()->result();
        }
        if(count($test)>0)
        {
            $type = "error";
            $message = ($this->session->userdata('lang')=='arabic')?'هوية الموظف مستخدمة. الرجاء إدخال رقم هوية آخر':'Employee ID is used. Please use another one';
            set_message($type, $message);
            redirect('admin/employee/employees');

        }


        if (!empty($data['fils_name'])) {
            $data['fils_name'] = implode(';', $data['fils_name']);
        }
        if (!empty($data['fils_name_ar'])) {
            $data['fils_name_ar'] = implode(';', $data['fils_name_ar']);
        }
        if (!empty($data['fils_birth'])) {
            $data['fils_birth'] = implode(';', $data['fils_birth']);
        }
        $data['reference'] = rand(1000, 9999);
        $data_login['user_name'] = $data['employment_id'];
        $data_login['password'] = $data['password'];
        $data_login['activate'] = $data['status'];
        unset($data['password']);
        ///////////////////////////////////////////////////////////
        /////////////////////// prepare files /////////////////////
        ///////////////////////////////////////////////////////////

        if (!empty($_FILES['id_proff']['name'])) {
            $_FILES['id_proff']['name'] = urlencode($_FILES['id_proff']['name']);
            $val = $this->employee_model->uploadAllType('id_proff');
            $data['id_proff'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['photo']['name'])) {
            $_FILES['photo']['name'] = urlencode($_FILES['photo']['name']);
            $val = $this->employee_model->uploadImage('photo');
            $data['photo'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        if (!empty($_FILES['cin_photo_path']['name'])) {
            $_FILES['cin_photo_path']['name'] = urlencode($_FILES['cin_photo_path']['name']);
            $val = $this->employee_model->uploadImage('cin_photo_path');
            $data['cin_photo_path'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        if (!empty($_FILES['passport_photo_path']['name'])) {
            $_FILES['passport_photo_path']['name'] = urlencode($_FILES['passport_photo_path']['name']);
            $val = $this->employee_model->uploadImage('passport_photo_path');
            $data['passport_photo_path'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        if (!empty($_FILES['resume_path']['name'])) {
            $_FILES['resume_path']['name'] = urlencode($_FILES['resume_path']['name']);
            $val = $this->employee_model->uploadFile('resume_path');
            $data['resume_path'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['contract_paper_path']['name'])) {
            $_FILES['contract_paper_path']['name'] = urlencode($_FILES['contract_paper_path']['name']);
            $val = $this->employee_model->uploadFile('contract_paper_path');
            $data['contract_paper_path'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        if (!empty($_FILES['other_document_path']['name'])) {
            $_FILES['other_document_path']['name'] = urlencode($_FILES['other_document_path']['name']);
            $val = $this->employee_model->uploadFile('other_document_path');
            $data['other_document_path'] = (!empty($val['fileName'])) ? $val['fileName'] : '';
        }
        ///////////////////////////////////////////////////////////
        /////////////////////// save employee /////////////////////
        ///////////////////////////////////////////////////////////
        $this->employee_model->_table_name = "tbl_employee"; // table name
        $this->employee_model->_primary_key = "employee_id"; // $id
        if ($id)
            $saved_id = $this->employee_model->save($data, $id);
        else {
            $saved_id = $this->employee_model->save($data);
        }

        if ($saved_id) {
            $this->employee_model->_table_name = "tbl_employee_login";
            $this->employee_model->_primary_key = "employee_login_id";
            $this->employee_model->_order_by = "employee_login_id";
            $data_login['employee_id'] = $saved_id;
            /////////////////
            if ($id) {
                $old_id = $this->employee_model->get_by(array('employee_id' => $id), TRUE)->employee_login_id;
                $saved_id2 = $this->employee_model->save($data_login, $old_id);
            } else {
                $saved_id2 = $this->employee_model->save($data_login);
            }
            /////////////////
            if ($saved_id2) {
                $type = "success";
                $message = lang('saved_successfully');
                set_message($type, $message);
                redirect('admin/employee/employees');
            } else {
                $type = "error";
                $message = $this->db->_error_message();
                set_message($type, $message);
                redirect('admin/employee/employees');
            }
        } else {
            $type = "error";
            $message = $this->db->_error_message();
            set_message($type, $message);
            redirect('admin/employee/employees');
        }
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function view_employee($id = NULL) {
        $data['title'] = lang('view_employee');
        $data['page_header'] = lang('employee_page_header'); //Page header title


        if (!empty($id)) {// retrive data from db by id
            $data['employee_info'] = $this->employee_model->all_emplyee_info($id);
            if (empty($data['employee_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/employee/employee_list');
            }
        }
        $data['subview'] = $this->load->view('admin/employee/view_employee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function employee_list_pdf() {
        $data['title'] = "Employee List";
        $data['all_employee_info'] = $this->employee_model->all_emplyee_info();
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/employee/employee_list_pdf', $data, true);
        pdf_create($view_file, 'Employee List');
    }

    public function make_pdf($employee_id) {
        $data['title'] = "Employee List";
        $data['employee_info'] = $this->employee_model->all_emplyee_info($employee_id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/employee/employee_view_pdf', $data, true);
        pdf_create($view_file, $data['employee_info']->first_name . ' ' . $data['employee_info']->last_name);
    }

    public function finance_employee($id = NULL) {
        $data['lang'] = $this->session->userdata('lang');
        $data['hijri_calendar'] = TRUE;
        $data['title'] = lang('finance_employee');
        $employee_info = $this->employee_model->all_emplyee_info($id);
        $data['page_header'] = $employee_info->full_name_en; //Page header title
        $data['active'] = 1;

        if (!empty($id)) {// retrive data from db by id
            $data['employee_info'] = $this->employee_model->all_emplyee_info($id);
            $data['allowances_info'] = $this->employee_model->all_allowances();
            $data['allowances_employee'] = $this->employee_model->get_allowances_by_employee($id);

            $data['provisions_info'] = $this->employee_model->all_provisions();
            $data['provisions_employee'] = $this->employee_model->get_provisions_by_employee($id);

            $data['deductions_info'] = $this->employee_model->all_deductions();
            $data['deductions_employee'] = $this->employee_model->get_deductions_by_employee($id);

            if (empty($data['employee_info'])) {
                $data['active'] = 2;
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/employee/employee_list');
            }
        }else{

        }
        $data['subview'] = $this->load->view('admin/employee/finance_employee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
    public function save_allowance() {

        $this->employee_model->_table_name = "tbl_allowance"; // table name
        $this->employee_model->_primary_key = "allowance_type_id";
        $data['allowance_id'] = $this->input->post('allowance_id');
        $data['employee_id'] = $this->input->post('employee_id');
        $data['allowance_value'] = $this->input->post('allowance_value');
        $data['allowance_date_from'] = $this->input->post('allowane_date_from');
        $data['allowance_date_to'] = $this->input->post('allowane_date_to');
        $data['reservation'] = '0.000';
        $this->employee_model->save($data);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }
    public function save_provision() {

        $this->employee_model->_table_name = "tbl_provision"; // table name
        $this->employee_model->_primary_key = "provision_id";
        $data['provision_category_id'] = $this->input->post('provision_category_id');
        $data['employee_id'] = $this->input->post('employee_id');
        $data['provision_value'] = $this->input->post('provision_value');
        $data['provision_description_ar'] = $this->input->post('desc_ar');
        $data['provision_description_en'] = $this->input->post('desc_en');
        $this->employee_model->save($data);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }
    public function save_deduction() {

        $this->employee_model->_table_name = "tbl_deduction"; // table name
        $this->employee_model->_primary_key = "deduction_type_id";
        $data['deduction_id'] = $this->input->post('deduction_id');
        $data['employee_id'] = $this->input->post('employee_id');
        $data['deduction_value'] = $this->input->post('deduction_value');
        $data['deduction_description_ar'] = $this->input->post('desc_ar');
        $data['deduction_description_en'] = $this->input->post('desc_en');
        $this->employee_model->save($data);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }

    public function save_houcing($id = NULL) {

        $this->employee_model->_table_name = "tbl_houcing"; // table name
        $this->employee_model->_primary_key = "houcing_id";
        $data['employee_id'] = $this->input->post('employee_id');
        $data['houcing_status'] = $this->input->post('houcing_status');
        $data['houcing_type'] = $this->input->post('houcing_type');
        $data['houcing_value'] = $this->input->post('houcing_value');
        $data['houcing_repetition'] = $this->input->post('houcing_repetition');
        $data['start_day'] = $this->input->post('start_day');
        $this->employee_model->save($data,$id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }

    public function save_finance_info($id = NULL) {

        $this->employee_model->_table_name = "tbl_finance_info"; // table name
        $this->employee_model->_primary_key = "finance_info_id";

        $data['employee_id'] = $this->input->post('employee_id');
        $data['payment_method'] = $this->input->post('payment_method');
        $data['med_insur_start_date'] = $this->input->post('med_insur_start_date');
        $data['med_insur_end_date'] = $this->input->post('med_insur_end_date');
        $data['soc_insur_start_date'] = $this->input->post('soc_insur_start_date');
        $data['service_fixed'] = $this->input->post('service_fixed');
        $data['service_benefits'] = $this->input->post('service_benefits');
        $data['service_fix_date'] = $this->input->post('service_fix_date');
        $data['stopping_date'] = $this->input->post('stopping_date');
        $data['service_suspend'] = $this->input->post('service_suspend');
        $data['figure_account_1'] = $this->input->post('figure_account_1');
        $data['figure_account_2'] = $this->input->post('figure_account_2');
        $data['service_benefits_date'] = $this->input->post('service_benefits_date');
        $data['service_benefits_yes'] = $this->input->post('service_benefits_yes');
        $data['stopping_salary_date'] = $this->input->post('stopping_salary_date');
        $data['option_desc_ar'] = $this->input->post('option_desc_ar');
        $data['option_desc_en'] = $this->input->post('option_desc_en');
        $data['retrait_date'] = $this->input->post('retrait_date');

        $this->employee_model->save($data,$id);
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }


    public function delete_allowance($id) {
        $this->employee_model->_table_name = "tbl_allowance"; // table name
        $this->employee_model->_primary_key = "allowance_type_id";
        $this->employee_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }
    public function delete_provision($id) {
        $this->employee_model->_table_name = "tbl_provision"; // table name
        $this->employee_model->_primary_key = "provision_id";
        $this->employee_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }
    public function delete_deduction($id) {
        $this->employee_model->_table_name = "tbl_deduction"; // table name
        $this->employee_model->_primary_key = "deduction_type_id";
        $this->employee_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/employee/employees');
    }
    public function getAllowancesByType($type){
        echo json_encode($this->employee_model->all_allowances_bytype($type));
    }
    public function getDeductionsByType($type){
        echo json_encode($this->employee_model->all_deduction_bytype($type));
    }
    public function getDesignations($id){
        echo json_encode($this->employee_model->all_designations_by_department($id));
    }
    public function getDepartmentByID($id){
        echo json_encode($this->employee_model->all_department_byId($id));
    }
    public function getDesignationByID($id){
        echo json_encode($this->employee_model->all_designation_byId($id));
    }


}
