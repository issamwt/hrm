<?php

class Approvals extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('approvals_model');
    }

    public function approvals_list($id = Null) {

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('approvals_list');
        $data['page_header'] = lang('approvals');

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $data['approvals_cat_list'] = $this->approvals_model->get();

        $data['subview'] = $this->load->view('admin/approvals/approvals_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function add_approval($id = Null) {

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('add_approval');
        $data['page_header'] = lang('approvals');
        if ($id) {
            $data['title'] = lang('edit_approval');
            $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
            $this->approvals_model->_order_by = "approval_cat_id";
            $data['approvals_cat_info'] = $this->approvals_model->get_by(array('approval_cat_id' => $id), TRUE);
            $data['all'] = array_merge(explode(';', $data['approvals_cat_info']->admins), explode(';', $data['approvals_cat_info']->employees));

        }

        // Employees
        $data['employees'] = $this->approvals_model->get_emp_with_jib_titles();
        // DEPARTMENT
        $this->approvals_model->_table_name = "tbl_department"; //table name
        $this->approvals_model->_order_by = "department_id";
        $data['deps'] = $this->approvals_model->get();
        // Job Place
        $this->approvals_model->_table_name = "tbl_job_places"; //table name
        $this->approvals_model->_order_by = "job_place_id";
        $data['jplaces'] = $this->approvals_model->get();
        // HR ADMIN
        $this->approvals_model->_table_name = "tbl_employee"; //table name
        $this->approvals_model->_order_by = "employee_id";
        $data['hradmin'] = $this->approvals_model->get_by(array('job_title' => '4'), TRUE);
        //employees_non_admins
        $data['employees_non_admins'] = $this->approvals_model->employees_non_admins();
        // Admins
        $data['get_dep_admins'] = $this->approvals_model->get_dep_admins();
        $data['get_des_admins'] = $this->approvals_model->get_des_admins();

        $data['subview'] = $this->load->view('admin/approvals/add_approval', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_approval($id = NULL) {
        $data = $this->input->post();
        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $new_id = $this->approvals_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/approvals/add_approval/' . $new_id);
    }

    public function delete_approval($id) {
        $this->approvals_model->_table_name = "tbl_approvals_cat"; // table name
        $this->approvals_model->_primary_key = "approval_cat_id"; // $id
        $this->approvals_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/approvals/approvals_list');
    }

    public function save_configure_approval($id) {
        $data = $this->input->post();

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $this->approvals_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/approvals/add_approval/' . $id);
    }

    public function save_configure_approval2($id) {
        $data = $this->input->post();
        $data['admins'] = $this->input->post("admins") ? implode(';', $data['admins']) : '0;';
        $datax = $data;
        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $this->approvals_model->save($datax, $id);
        echo $this->db->last_query();
        echo '<hr>';
        $this->helperfct($id);
        echo $this->db->last_query();

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/approvals/add_approval/' . $id);
    }

    public function save_configure_approval3($id) {
        $data = $this->input->post();
        $data['employees'] = implode(';', $data['employees']);
        $datax = $data;
        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $this->approvals_model->save($datax, $id);
        echo $this->db->last_query();
        echo '<hr>';
        $this->helperfct($id);
        echo $this->db->last_query();

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/approvals/add_approval/' . $id);
    }

    public function save_configure_approval4($id) {
        $data = $this->input->post();

        $data['accept'] = implode(';', $data['accept']);
        $data['refuse'] = implode(';', $data['refuse']);
        $data['delegate'] = implode(';', $data['delegate']);
        $data['consultation'] = implode(';', $data['consultation']);
        $data['all_list'] = implode(';', $data['all_list']);

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";

        $this->approvals_model->save($data, $id);


        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/approvals/add_approval/' . $id);
    }

    public function helperfct($id) {
        // put the final list of admins+employees
        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $apprvl_info = $this->approvals_model->get_by(array('approval_cat_id' => $id), TRUE);
        $admins = explode(';', $apprvl_info->admins);
        $employees = explode(';', $apprvl_info->employees);
        $order_list = array();
        for ($i = 1; $i < count($admins) + count($employees) + 1; $i++)
            array_push($order_list, $i);
        $data['order_list'] = implode(';', $order_list);
        $data['all_list'] = implode(';', array_merge($admins, $employees));
        $this->approvals_model->save($data, $id);
    }

    public function follow_up($id) {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('follow_up');
        $data['page_header'] = lang('approvals');

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_order_by = "approval_cat_id";
        $data['approvals_cat_info'] = $this->approvals_model->get_by(array('approval_cat_id' => $id,), TRUE);

        $data['subview'] = $this->load->view('admin/approvals/follow_up', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_follow_up($id) {
        $data = $this->input->post();
        $data['automatic'] = $this->input->post("automatic") ? 1 : 0;
        $datax = $data;

        echo '<pre>';
        print_r($datax);
        echo '</pre>';
        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_primary_key = "approval_cat_id";
        $this->approvals_model->_order_by = "approval_cat_id";
        $this->approvals_model->save($datax, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        echo '<br>';
        echo $this->db->last_query();
        echo '<br>';
        echo $this->db->_error_message();


        redirect('admin/approvals/approvals_list');
    }

    public function get_dep_with_jobplace($jplc_id) {
        $this->approvals_model->_table_name = "tbl_department"; //table name
        $this->approvals_model->_primary_key = "department_id";
        $this->approvals_model->_order_by = "department_id";
        $result = $this->approvals_model->get_by(array('job_place_id' => $jplc_id));
        echo json_encode($result);
    }

    public function get_sec_with_dep($dep_id) {
        $this->approvals_model->_table_name = "tbl_designations"; //table name
        $this->approvals_model->_primary_key = "designations_id";
        $this->approvals_model->_order_by = "designations_id";
        $result = $this->approvals_model->get_by(array('department_id' => $dep_id));
        echo json_encode($result);
    }

    public function get_unit_with_sec($sect_id) {
        $this->approvals_model->_table_name = "tbl_units"; //table name
        $this->approvals_model->_primary_key = "unit_id";
        $this->approvals_model->_order_by = "unit_id";
        $result = $this->approvals_model->get_by(array('designations_id' => $sect_id));
        echo json_encode($result);
    }

    public function get_division_with_unit($unit_id) {
        $this->approvals_model->_table_name = "tbl_divisions"; //table name
        $this->approvals_model->_primary_key = "division_id";
        $this->approvals_model->_order_by = "division_id";
        $result = $this->approvals_model->get_by(array('unit_id' => $unit_id));
        echo json_encode($result);
    }

    public function link_approvals() {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('link_approvals');
        $data['page_header'] = lang('approvals');

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_order_by = "approval_cat_id";
        $data['approvals_cat_list'] = $this->approvals_model->get();

        ///////////////////
        $this->approvals_model->_table_name = "tbl_advance"; //table name
        $this->approvals_model->_order_by = "advance_id";
        $data['la_advance'] = $this->approvals_model->get();
        //////////////////
        $this->approvals_model->_table_name = "tbl_leave_category"; //table name
        $this->approvals_model->_order_by = "leave_category_id";
        $data['la_vacations'] = $this->approvals_model->get();
        //////////////////
        // $this->approvals_model->_table_name = "tbl_extra_work"; //table name
        // $this->approvals_model->_order_by = "extra_work_id";
        // $data['la_overtime'] = $this->approvals_model->get();
        //////////////////

        $data['subview'] = $this->load->view('admin/approvals/link_approvals', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_link_approvals() {
        $approvals = $this->input->post('approvals');
        $data['linked_apps'] = $this->input->post('applications');

        echo '<pre>';
        print_r($data);
        echo '</pre><br>';

        foreach (explode(',', $approvals) as $app) {
            echo '<h1>' . $app . '</h1>';
            $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
            $this->approvals_model->_primary_key = "approval_cat_id";
            $this->approvals_model->_order_by = "approval_cat_id";
            $this->approvals_model->save($data, $app);
        }

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        echo '<br>';
        echo $this->db->last_query();
        echo '<br>';
        echo $this->db->_error_message();

        redirect('admin/approvals/link_approvals');
    }

    public function applications(){
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('applications');
        $data['page_header'] = lang('approvals');

        $this->approvals_model->_table_name = "tbl_applications"; //table name
        $this->approvals_model->_order_by = "application_id";
        $data['applications_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = "tbl_employee"; //table name
        $this->approvals_model->_order_by = "employee_id";
        $data['all'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = "tbl_approvals_cat"; //table name
        $this->approvals_model->_order_by = "approval_cat_id";
        $data['approval_cats'] = $this->approvals_model->get();

        $data['subview'] = $this->load->view('admin/approvals/applications', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_application($id)
    {
        $data['view_application'] = $this->approvals_model->get_applications_by_id($id);
        $data['lang'] = $this->session->userdata('lang');


        $today = $this->approvals_model->Greg2Hijri(date('Y-m-d'), TRUE);
        $data['today'] = date_create_from_format('Y-m-d', $today)->format('Y-m-d');

        $this->approvals_model->_table_name = "tbl_leave_category"; // table name
        $this->approvals_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_advance';
        $this->approvals_model->_order_by = "advance_id";
        $data['advances_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = "tbl_employee"; // table name
        $this->approvals_model->_order_by = "employee_id"; // $id
        $data['employees_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_job_titles';
        $this->approvals_model->_order_by = "job_titles_id";
        $data['job_titles_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_department';
        $this->approvals_model->_order_by = "department_id";
        $data['department_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_designations';
        $this->approvals_model->_order_by = "designations_id";
        $data['designations_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tpl_employee_category';
        $this->approvals_model->_order_by = "id";
        $data['emp_cat_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_extra_work';
        $this->approvals_model->_order_by = "extra_work_id";
        $data['extra_work_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_allowance_category';
        $this->approvals_model->_order_by = "allowance_id";
        $data['allowances_list'] = $this->approvals_model->get();

        $this->approvals_model->_table_name = 'tbl_application_notes';
        $this->approvals_model->_order_by = "note_id";
        $data['app_notes'] = $this->approvals_model->get_by(array('application_id'=>$id));

        $data['subview'] = $this->load->view('admin/approvals/view_application', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function delete_application($id){
        $this->approvals_model->_table_name = "tbl_applications"; // table name
        $this->approvals_model->_primary_key = "application_id"; // $id
        $this->approvals_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/approvals/applications');
    }

}
