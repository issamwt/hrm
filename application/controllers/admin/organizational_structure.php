<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Organizational_Structure extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('organizational_structure_model');
    }

    public function branches($id = NULL) {
        $data['lang'] = $this->session->userdata('lang');

        $data['title'] = lang('branches'); //Page title
        $data['page_header'] = lang('organizational_structure'); //Page header title
        $data['active'] = 1;

        $this->organizational_structure_model->_table_name = "tbl_branches"; //table name
        $this->organizational_structure_model->_primary_key = "branche_id";
        $this->organizational_structure_model->_order_by = "branche_id";

        if ($id) {
            $data['active'] = 2;
            $data['branche_info'] = $this->organizational_structure_model->get_by(array('branche_id' => $id,), TRUE);
        }

        $data['branches_list'] = $this->organizational_structure_model->get();


        $data['subview'] = $this->load->view('admin/organizational_structure/branches_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function save_branche($id = NULL) {
        $this->organizational_structure_model->_table_name = "tbl_branches"; //table name
        $this->organizational_structure_model->_primary_key = "branche_id";
        $this->organizational_structure_model->_order_by = "branche_id";

        $data = $this->input->post();
        $all = $this->organizational_structure_model->get();

        foreach ($all as $one){
            if($one->branche_ar==$data['branche_ar'])
            {
                $type = "error";
                $message = $data['branche_ar'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/branches');
            }
            if($one->branche_en==$data['branche_en'])
            {
                $type = "error";
                $message = $data['branche_en'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/branches');
            }
        }

        $this->organizational_structure_model->save($data, $id);

        // messages for user
        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);

        redirect('admin/organizational_structure/branches'); //redirect page
    }

    public function delete_branche($id = NULL) {
        if ($id == 1)
            redirect('admin/organizational_structure/branches');

        $this->organizational_structure_model->_table_name = "tbl_branches"; //table name
        $this->organizational_structure_model->_primary_key = "branche_id";    //id
        $this->organizational_structure_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);

        redirect('admin/organizational_structure/branches'); //redirect page
    }

    //////////////////////////////////////////////////////////////
    // DEPERTMENT
    //////////////////////////////////////////////////////////////

    public function department_list($id = NULL) {

        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('department_list');
        $data['page_header'] = lang('organizational_structure');

        $data['active'] = 1;

        //department table initials
        $this->organizational_structure_model->_table_name = "tbl_department"; //table name
        $this->organizational_structure_model->_order_by = "department_id";

        if ($id) { // retrive data from db by id
            $data['active'] = 2;
            // get all department by id
            $data['department_info'] = $this->organizational_structure_model->get_by(array('department_id' => $id), TRUE);
            // get all designation by department id
            $this->organizational_structure_model->_table_name = "tbl_designations"; //table name
            $data['designation_info'] = $this->organizational_structure_model->get_by(array('department_id' => $id), FALSE);

            if (empty($data['department_info'])) {
                $type = "error";
                $message = lang('no_record_found');
                set_message($type, $message);
                redirect('admin/organizational_structure/department_list');
            }
        }

        $this->organizational_structure_model->_table_name = "tbl_branches"; //table name
        $this->organizational_structure_model->_order_by = "branche_id";
        $data['braches_list'] = $this->organizational_structure_model->get();

        $this->organizational_structure_model->_table_name = "tbl_employee"; //table name
        $this->organizational_structure_model->_order_by = "employee_id";
        $data['employee_list'] = $this->organizational_structure_model->get();

        $this->organizational_structure_model->_table_name = "tbl_department"; //table name
        $this->organizational_structure_model->_order_by = "department_id";
        $data['all_dept_info'] = $this->organizational_structure_model->get();
        // get all department info and designation info
        foreach ($data['all_dept_info'] as $v_dept_info) {
            $data['all_department_info'][] = $this->organizational_structure_model->get_add_department_by_id($v_dept_info->department_id);
        }
        $data['subview'] = $this->load->view('admin/organizational_structure/department_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_department($id = NULL) {

        $this->organizational_structure_model->_table_name = "tbl_department"; // table name
        $this->organizational_structure_model->_primary_key = "department_id"; // $id
        $this->organizational_structure_model->_order_by = "department_id";

        $data = $this->input->post(); //input post
        $all = $this->organizational_structure_model->get();

        foreach ($all as $one){
            if($one->department_name_ar==$data['department_name_ar'] and $one->branche_id==$data['branche_id'] and $id==NULL)
            {
                $type = "error";
                $message = $data['department_name_ar'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/department_list');
            }
            if($one->department_name==$data['department_name'] and $one->branche_id==$data['branche_id'] and $id==NULL)
            {
                $type = "error";
                $message = $data['department_name'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/department_list');
            }
        }

        $this->organizational_structure_model->save($data, $id);

        $type = "success";
        $message = lang('department_info_saved');
        set_message($type, $message);
        redirect('admin/organizational_structure/department_list'); //redirect page
    }

    public function delete_department($id) {
        // if trying to delete the default departeemt
        /*
          if ($id == 1)
          redirect('errors/notallawoed');
         */

        // delete the departement
        $this->organizational_structure_model->_table_name = "tbl_department"; // table name
        $this->organizational_structure_model->_primary_key = "department_id"; // $id
        $this->organizational_structure_model->delete($id);

        // get all the designations that belong to the deleted departement
        /*
          $where = array('department_id' => $id);
          $this->organizational_structure_model->_table_name = "tbl_designations"; // table name
          $this->organizational_structure_model->_order_by = "designations_id";
          $get_designations = $this->organizational_structure_model->get_by($where);

          //prepare the update_batch $update_data array
          $update_data = array();
          if (count($get_designations)) {
          foreach ($get_designations as $desig) {
          $array = array(
          'designations_id' => $desig->designations_id,
          'department_id' => 1
          );
          array_push($update_data, $array);
          }
          }

          // reset the designation belonging to the deleted departement
          $res = $this->organizational_structure_model->reset_designation($update_data);
         */

        // redirect
        set_message('success', lang('department_info_deleted'));
        redirect('admin/organizational_structure/department_list');
    }

    //////////////////////////////////////////////////////////////
    // DESIGNATIONS
    //////////////////////////////////////////////////////////////

    public function designation_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('designations_list');
        $data['page_header'] = lang('organizational_structure');

        if ($id) {
            $data['active'] = 2;
            $data['designation_info'] = $this->organizational_structure_model->get_designation_with_departments($id);
        }

        $this->organizational_structure_model->_table_name = "tbl_employee"; //table name
        $this->organizational_structure_model->_order_by = "employee_id";
        $data['employee_info'] = $this->organizational_structure_model->get();

        $this->organizational_structure_model->_table_name = "tbl_department"; //table name
        $this->organizational_structure_model->_order_by = "department_id";
        $data['departement_info'] = $this->organizational_structure_model->get();

        $data['designations_details'] = $this->organizational_structure_model->get_designation_with_departments();

        $data['subview'] = $this->load->view('admin/organizational_structure/designation_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_designation($id = NULL) {
        $this->organizational_structure_model->_table_name = "tbl_designations"; // table name
        $this->organizational_structure_model->_order_by = "designations_id";
        $this->organizational_structure_model->_primary_key = "designations_id";
        $data = $this->input->post();
        $data['employee_id'] = $data['employee_id_non_required'];
        unset($data['employee_id_non_required']);

        $all = $this->organizational_structure_model->get();

        foreach ($all as $one){
            if($one->designations_ar==$data['designations_ar'] and $one->department_id==$data['department_id'] and $id==NULL)
            {
                $type = "error";
                $message = $data['designations_ar'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
            if($one->designations==$data['designations'] and $one->department_id==$data['department_id'] and $id==NULL)
            {
                $type = "error";
                $message = $data['designations'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
        }

        $this->organizational_structure_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/designation_list');
    }

    public function delete_designation($id) {
        $this->organizational_structure_model->_table_name = "tbl_designations"; // table name
        $this->organizational_structure_model->_primary_key = "designations_id"; // $id
        $this->organizational_structure_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/designation_list');
    }

    //////////////////////////////////////////////////////////////
    // UNITS
    //////////////////////////////////////////////////////////////

    public function units_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('units_list');
        $data['page_header'] = lang('organizational_structure');

        if ($id) {
            $data['active'] = 2;
            $data['unit_info'] = $this->organizational_structure_model->get_unit_with_designations($id);
        }

        $this->organizational_structure_model->_table_name = "tbl_employee"; //table name
        $this->organizational_structure_model->_order_by = "employee_id";
        $data['employee_info'] = $this->organizational_structure_model->get();

        $this->organizational_structure_model->_table_name = "tbl_designations"; //table name
        $this->organizational_structure_model->_order_by = "designations_id";
        $data['designations_info'] = $this->organizational_structure_model->get();

        $data['units_list'] = $this->organizational_structure_model->get_unit_with_designations();

        $data['subview'] = $this->load->view('admin/organizational_structure/units_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_unit($id = NULL) {
        $this->organizational_structure_model->_table_name = "tbl_units"; // table name
        $this->organizational_structure_model->_order_by = "unit_id";
        $this->organizational_structure_model->_primary_key = "unit_id";
        $data = $this->input->post();
        $data['unit_employee_id'] = $data['employee_id_non_required'];
        unset($data['employee_id_non_required']);

        $all = $this->organizational_structure_model->get();

        foreach ($all as $one){
            if($one->unit_ar==$data['unit_ar'] and $one->designations_id==$data['designations_id'])
            {
                $type = "error";
                $message = $data['unit_ar'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
            if($one->unit_en==$data['unit_en'] and $one->designations_id==$data['designations_id'])
            {
                $type = "error";
                $message = $data['unit_en'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
        }

        $this->organizational_structure_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/units_list');
    }

    public function delete_unit($id) {
        $this->organizational_structure_model->_table_name = "tbl_units"; // table name
        $this->organizational_structure_model->_primary_key = "unit_id"; // $id
        $this->organizational_structure_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/units_list');
    }

    //////////////////////////////////////////////////////////////
    // DIVISION
    //////////////////////////////////////////////////////////////

    public function divisions_list($id = NULL) {
        $data['active'] = 1;
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('divisions_list');
        $data['page_header'] = lang('organizational_structure');

        if ($id) {
            $data['active'] = 2;
            $data['division_info'] = $this->organizational_structure_model->get_division_with_units($id);
        }

        $this->organizational_structure_model->_table_name = "tbl_employee"; //table name
        $this->organizational_structure_model->_order_by = "employee_id";
        $data['employee_info'] = $this->organizational_structure_model->get();

        $this->organizational_structure_model->_table_name = "tbl_units"; //table name
        $this->organizational_structure_model->_order_by = "unit_id";
        $data['unit_info'] = $this->organizational_structure_model->get();

        $data['division_list'] = $this->organizational_structure_model->get_division_with_units();

        $data['subview'] = $this->load->view('admin/organizational_structure/divisions_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_division($id = NULL) {
        $this->organizational_structure_model->_table_name = "tbl_divisions"; // table name
        $this->organizational_structure_model->_order_by = "division_id";
        $this->organizational_structure_model->_primary_key = "division_id";
        $data = $this->input->post();
        $data['div_employee_id'] = $data['employee_id_non_required'];
        unset($data['employee_id_non_required']);

        $all = $this->organizational_structure_model->get();

        foreach ($all as $one){
            if($one->division_ar==$data['division_ar'] and $one->unit_id==$data['unit_id'])
            {
                $type = "error";
                $message = $data['division_ar'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
            if($one->division_en==$data['division_en'] and $one->unit_id==$data['unit_id'])
            {
                $type = "error";
                $message = $data['division_en'].' : '.lang('name_exist');
                set_message($type, $message);
                redirect('admin/organizational_structure/designation_list');
            }
        }

        $this->organizational_structure_model->save($data, $id);

        $type = "success";
        $message = lang('saved_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/divisions_list');
    }

    public function delete_division($id) {
        $this->organizational_structure_model->_table_name = "tbl_divisions"; // table name
        $this->organizational_structure_model->_primary_key = "division_id"; // $id
        $this->organizational_structure_model->delete($id);
        $type = "success";
        $message = lang('deleted_successfully');
        set_message($type, $message);
        redirect('admin/organizational_structure/divisions_list');
    }

    //////////////////////////////////////////////////////////////
    // OVERVIEW
    //////////////////////////////////////////////////////////////

    public function overview() {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('overview');
        $data['page_header'] = lang('organizational_structure');

        $data['datasource'] = $this->organizational_chart();
        $data['subview'] = $this->load->view('admin/organizational_structure/overview', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function organizational_chart() {
        $data['lang'] = $this->session->userdata('lang');
        $data['title'] = lang('organizational_chart'); //Page title
        $data['page_header'] = lang('settings_management'); //Page header title

        $bras = $this->organizational_structure_model->get_branches();
        for ($h = 0; $h < count($bras); $h++) {
            $deps = $this->organizational_structure_model->get_deps($bras[$h]['branche_id']);
            if (!empty($deps)) {
                for ($i = 0; $i < count($deps); $i++) {
                    $desg = $this->organizational_structure_model->get_desg($deps[$i]['department_id']);
                    if (!empty($desg)) {
                        for ($j = 0; $j < count($desg); $j++) {
                            $units = $this->organizational_structure_model->get_units($desg[$j]['designations_id']);
                            if (!empty($units)) {
                                for ($k = 0; $k < count($units); $k++) {
                                    $divs = $this->organizational_structure_model->get_divis($units[$k]['unit_id']);
                                    if (!empty($divs)) {
                                        for ($l = 0; $l < count($divs); $l++) {
                                            $divs[$l]['id'] = 'div;' . $divs[$l]['division_id'];
                                            $divs[$l]['name'] = lang('adivision');
                                            $divs[$l]['title'] = ($data['lang'] == 'arabic') ? $divs[$l]['division_ar'] : $divs[$l]['division_en'];
                                            $divs[$l]['delete'] = base_url() . 'admin/organizational_structure/delete_division/' . $divs[$l]['division_id'];
                                            $divs[$l]['add'] = base_url() . 'admin/organizational_structure/divisions_list';
                                            $divs[$l]['update'] = base_url() . 'admin/organizational_structure/divisions_list/' . $divs[$l]['division_id'];
                                            $divs[$l]['className'] = 'division';
                                            unset($divs[$l]['division_id']);
                                            unset($divs[$l]['division_ar']);
                                            unset($divs[$l]['division_en']);
                                        }
                                        $units[$k]['children'] = $divs;
                                    }
                                    $units[$k]['id'] = 'unit;' . $units[$k]['unit_id'];
                                    $units[$k]['name'] = lang('aunit');
                                    $units[$k]['title'] = ($data['lang'] == 'arabic') ? $units[$k]['unit_ar'] : $units[$k]['unit_en'];
                                    $units[$k]['delete'] = base_url() . 'admin/organizational_structure/delete_unit/' . $units[$k]['unit_id'];
                                    $units[$k]['add'] = base_url() . 'admin/organizational_structure/units_list';
                                    $units[$k]['update'] = base_url() . 'admin/organizational_structure/units_list/' . $units[$k]['unit_id'];
                                    $units[$k]['className'] = 'unit';
                                    unset($units[$k]['unit_id']);
                                    unset($units[$k]['unit_ar']);
                                    unset($units[$k]['unit_en']);
                                }
                                $desg[$j]['children'] = $units;
                            }
                            $desg[$j]['id'] = 'des;' . $desg[$j]['designations_id'];
                            $desg[$j]['name'] = lang('adseignation');
                            $desg[$j]['title'] = ($data['lang'] == 'arabic') ? $desg[$j]['designations_ar'] : $desg[$j]['designations'];
                            $desg[$j]['delete'] = base_url() . 'admin/organizational_structure/delete_designation/' . $desg[$j]['designations_id'];
                            $desg[$j]['add'] = base_url() . 'admin/organizational_structure/designation_list';
                            $desg[$j]['update'] = base_url() . 'admin/organizational_structure/designation_list/' . $desg[$j]['designations_id'];
                            $desg[$j]['className'] = 'designation';
                            unset($desg[$j]['designations_id']);
                            unset($desg[$j]['designations']);
                            unset($desg[$j]['designations_ar']);
                        }
                        $deps[$i]['children'] = $desg;
                    }

                    $deps[$i]['id'] = 'dep;' . $deps[$i]['department_id'];
                    $deps[$i]['name'] = lang('adeprtment');
                    $deps[$i]['title'] = ($data['lang'] == 'arabic') ? $deps[$i]['department_name_ar'] : $deps[$i]['department_name'];
                    $deps[$i]['delete'] = base_url() . 'admin/organizational_structure/delete_department/' . $deps[$i]['department_id'];
                    $deps[$i]['add'] = base_url() . 'admin/organizational_structure/department_list';
                    $deps[$i]['update'] = base_url() . 'admin/organizational_structure/department_list/' . $deps[$i]['department_id'];
                    $deps[$i]['className'] = 'department';
                    unset($deps[$i]['department_id']);
                    unset($deps[$i]['department_name']);
                    unset($deps[$i]['department_name_ar']);
                }
                $bras[$h]['children'] = $deps;
            }
            $bras[$h]['id'] = 'bras;' . $bras[$h]['branche_id'];
            $bras[$h]['name'] = lang('abranche');
            $bras[$h]['title'] = ($data['lang'] == 'arabic') ? $bras[$h]['branche_ar'] : $bras[$h]['branche_en'];
            $bras[$h]['delete'] = base_url() . 'admin/organizational_structure/delete_branche/' . $bras[$h]['branche_id'];
            $bras[$h]['add'] = base_url() . 'admin/organizational_structure/branches';
            $bras[$h]['update'] = base_url() . 'admin/organizational_structure/branches/' . $bras[$h]['branche_id'];
            $bras[$h]['className'] = 'branche';
            unset($bras[$h]['branche_id']);
            unset($bras[$h]['branche_ar']);
            unset($bras[$h]['branche_en']);
        }
        $datasource['id'] = 'hrm;0';
        $datasource['name'] = lang('acompany');
        $datasource['title'] = 'HRM_LITE';
        $datasource['children'] = $bras;

        return $datasource;
    }

}
