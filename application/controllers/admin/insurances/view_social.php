<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-8 ">
        <div class="box box-primary" data-collapsed="0" style="border: none">            
            <div class="box-body">
                <h3><?= lang('social_insurance') ?>:&nbsp;<?= ($lang == 'english') ? $details_info->title_en : $details_info->title_ar; ?></h3>
                <form role="form" id="general_settings"  action="<?php echo base_url(); ?>admin/insurances/save_social_details/<?php
                            if (!empty($details_info->social_details_id)) {
                                echo $details_info->social_details_id;
                            }
                            ?>" method="post" class="form-horizontal form-groups-bordered small" style="padding-top: 15px;">
<?php if (!empty($details_info->social_details_id)): ?>
                                    <input type="text" name="social_details_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($details_info->social_details_id)) {
                                        echo $details_info->social_details_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                            <input type="text" name="social_id"  class="form-control" id="field-1" value="<?php echo $social; ?>"/>

                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('insurance_percent') ?>&nbsp;<span><b>(%)</b></span></label>

                        <div class="col-sm-7">
                            <input type="number" name="insurance_percent"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->insurance_percent)) {
                                            echo $details_info->insurance_percent;
                                        }
                                        ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('societe_percent') ?>&nbsp;<span>(%)</span></label>

                        <div class="col-sm-7">
                            <input type="number" name="societe_percent"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->societe_percent)) {
                                            echo $details_info->societe_percent;
                                        }
                                        ?>"/>
                        </div>
                    </div>  
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('nb_days') ?>&nbsp;<span>(<?= lang('days') ?>)</span></label>

                        <div class="col-sm-7">
                            <input type="number" name="nb_days"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->nb_days)) {
                                            echo $details_info->nb_days;
                                        }
                                        ?>"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('age_male') ?>&nbsp;<span>(<?= lang('year') ?>)</label>

                        <div class="col-sm-7">
                            <input type="number" name="age_male"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->age_male)) {
                                            echo $details_info->age_male;
                                        }
                                        ?>"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label" style="color:red;"><?= lang('age_female') ?>&nbsp;<span>(<?= lang('year') ?>)</label>

                        <div class="col-sm-7">
                            <input type="number" name="age_female"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->age_female)) {
                                            echo $details_info->age_female;
                                        }
                                        ?>"/>
                        </div>  
                    </div>
                    <div class=" ">
                        <label for="field-1" class="col-sm-4 control-label "><?= lang('salary_insurance') ?></label>
                        <div class="col-sm-6">
                            <select class="form-control" name="salary_insurance" id="">
                                <option><?= lang('social_insur_select') ?></option>
                                <option value="1" <?php
                                if (!empty($details_info->salary_insurance)) {
                                    echo $details_info->salary_insurance == 1 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_yes') ?></option>
                                <option value="0" <?php
                                if (!empty($details_info->salary_insurance)) {
                                    echo $details_info->salary_insurance == 0 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_no') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="">
                        <label for="field-1" class="col-sm-4 control-label "><?= lang('habit_insurance') ?></label>
                        <div class="col-sm-6">
                            <select class="form-control" name="habit_insurance" id="">
                                <option><?= lang('social_insur_select') ?></option>
                                 <option value="1" <?php
                                if (!empty($details_info->habit_insurance)) {
                                    echo $details_info->habit_insurance == 1 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_yes') ?></option>
                                <option value="0" <?php
                                if (!empty($details_info->habit_insurance)) {
                                    echo $details_info->habit_insurance == 0 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_no') ?></option>
                            </select> 
                        </div>
                    </div>
                    <div class="">
                        <label for="field-1" class="col-sm-4 control-label "><?= lang('insurance_value') ?></label>
                        <div class="col-sm-6">
                            <select class="form-control" name="insurance_value" >
                                <option><?= lang('social_insur_select') ?></option>
                                  <option value="1" <?php
                                if (!empty($details_info->insurance_value)) {
                                    echo $details_info->insurance_value == 1 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_yes') ?></option>
                                <option value="0" <?php
                                if (!empty($details_info->insurance_value)) {
                                    echo $details_info->insurance_value == 0 ? 'selected' : '';
                                }
                                ?>><?= lang('social_insurance_no') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label "><?= lang('max_salary') ?></label>

                        <div class="col-sm-7">
                            <input type="number" name="max_salary"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->max_salary)) {
                                            echo $details_info->max_salary;
                                        }
                                        ?>"/>
                        </div>  
                    </div>
                    <div class="form-group ">
                        <label for="field-1" class="col-sm-3 control-label "><?= lang('min_salary') ?></label>

                        <div class="col-sm-7">
                            <input type="number" name="min_salary"  class="form-control" id="field-1" value="<?php
                                        if (!empty($details_info->min_salary)) {
                                            echo $details_info->min_salary;
                                        }
                                        ?>"/>
                        </div>  
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-12">
                            <button type="submit"  class="btn btn-primary"  ><?= lang('save') ?></button>
                        </div>
                    </div>  

                </form>
            </div>
        </div>
    </div>
</div>