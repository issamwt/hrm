
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#medical_list" data-toggle="tab"><?= lang('medical_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_medical"  data-toggle="tab"><?= lang('add_medical') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="medical_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('medical_title') ?></th>
                                        <th width="23%" class="text-center"><?= lang('medical_brief') ?></th>
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($medicales_info)): $sl = 1; ?>
                                        <?php foreach ($medicales_info as $med): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $med->title_en : $med->title_ar; ?></td>
        <td><?= ($lang == 'english') ? $med->brief_en : $med->brief_ar; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/insurances/medical_list/<?php echo $med->medical_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/insurances/delete_medical/<?php echo $med->medical_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_medical" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/insurances/save_medical/<?php
                            if (!empty($medicale_info->medical_id)) {
                                echo $medicale_info->medical_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($medicale_info->extra_work_id)): ?>
                                    <input type="hidden" name="medical_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($medicale_info->medical_id)) {
                                        echo $medicale_info->medical_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($medicale_info->title_ar)) {
                                            echo $medicale_info->title_ar;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($medicale_info->title_en)) {
                                            echo $medicale_info->title_en;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                       <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_brief_ar') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($medicale_info->brief_ar)) {
                                            echo $medicale_info->brief_ar;
                                        }
                                        ?>" />
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_brief_en') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($medicale_info->brief_en)) {
                                            echo $medicale_info->brief_en;
                                        }
                                        ?>" />
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>