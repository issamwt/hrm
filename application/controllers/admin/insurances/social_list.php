
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">

        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs">
                <li class="<?= $active == 1 ? 'active' : '' ?>"><a href="#social_list" data-toggle="tab"><?= lang('social_list') ?></a></li>
                <li class="<?= $active == 2 ? 'active' : '' ?>"><a href="#add_social"  data-toggle="tab"><?= lang('add_social') ?></a></li>
            </ul>
            <div class="tab-content no-padding">

                <!-- Sections List Starts -->
                <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="social_list" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <!-- Table -->
                            <table class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th width="5%" class="text-center"><?= lang('sl') ?></th>
                                        <th width="23%" class="text-center"><?= lang('social_title') ?></th>
                                        <th width="23%" class="text-center"><?= lang('social_brief') ?></th>
                                        <th width="11%" class="text-center"><?= lang('action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (!empty($sociales_info)): $sl = 1; ?>
                                        <?php foreach ($sociales_info as $soc): ?>
                                            <tr>
                                                <td class="text-center"><?= $sl ?></td>
                                                <td><?= ($lang == 'english') ? $soc->title_en : $soc->title_ar; ?></td>
        <td><?= ($lang == 'english') ? $soc->brief_en : $soc->brief_ar; ?></td>
                                                <td>
                                                    <a data-original-title="<?= lang('view') ?>" href="<?php echo base_url() ?>admin/insurances/view_social/<?php echo $soc->social_id; ?>" class="btn btn-success btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-eye"></i> <?= lang('view') ?></a>
                                                    <a data-original-title="<?= lang('edit') ?>" href="<?php echo base_url() ?>admin/insurances/social_list/<?php echo $soc->social_id; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top"><i class="fa fa-pencil-square-o"></i> <?= lang('edit') ?></a>
                                                    <a data-original-title="<?= lang('delete') ?>" href="<?php echo base_url() ?>admin/insurances/delete_social/<?php echo $soc->social_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('<?= lang('js_confirm_message') ?>');"><i class="fa fa-trash-o"></i> <?= lang('delete') ?></a>
                                                
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><strong><?= lang('nothing_to_display') ?></strong></td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Sections List Ends -->

                <!-- Add Section Starts -->
                <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="add_social" style="position: relative;">
                    <div class="box" style="border: none; padding-top: 15px;" data-collapsed="0">
                        <div class="box-body">
                            <form role="form" id="form" action="<?php echo base_url(); ?>admin/insurances/save_social/<?php
                            if (!empty($social_info->social_id)) {
                                echo $social_info->social_id;
                            }
                            ?> " method="post" class="form-horizontal">
                                <!-- Bonuse Id for Update -->
                                <?php if (!empty($medicale_info->extra_work_id)): ?>
                                    <input type="hidden" name="social_id"  class="form-control" id="field-1" value="<?php
                                    if (!empty($social_info->social_id)) {
                                        echo $social_info->social_id;
                                    }
                                    ?>"/>
                                       <?php endif; ?>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('social_title_ar') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($social_info->title_ar)) {
                                            echo $social_info->title_ar;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('social_title_en') ?><span class="required">*</span></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="title_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($social_info->title_en)) {
                                            echo $social_info->title_en;
                                        }
                                        ?>" required=""/>
                                    </div>
                                </div>
                                       <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('social_brief_ar') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_ar"  class="form-control" id="field-1" value="<?php
                                        if (!empty($social_info->brief_ar)) {
                                            echo $social_info->brief_ar;
                                        }
                                        ?>" />
                                    </div>
                                </div>
                             
                                  <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label"><?= lang('medical_brief_en') ?></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="brief_en"  class="form-control" id="field-1" value="<?php
                                        if (!empty($medicale_info->brief_en)) {
                                            echo $medicale_info->brief_en;
                                        }
                                        ?>" />
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary"><?= lang('save') ?></button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Add Section Ends -->

            </div>
        </div>
    </div>
</div>