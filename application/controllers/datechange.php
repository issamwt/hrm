<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class datechange extends CI_Controller{

    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Riyadh');
        $this->load->database();
        $this->load->model('emp_model');
    }

    public function index(){
        try{
            $employees = $this->db->get('tbl_employee')->result();
            foreach ($employees as $employee){
                $data = [];

                if($employee->date_of_birth and $employee->date_of_birth!="")
                    $data["date_of_birth"] = $this->emp_model->Hijri2Greg($employee->date_of_birth, TRUE);

                if($employee->passport_end and $employee->passport_end!="")
                    $data["passport_end"] = $this->emp_model->Hijri2Greg($employee->passport_end, TRUE);

                if($employee->identity_end and $employee->identity_end!="")
                    $data["identity_end"] = $this->emp_model->Hijri2Greg($employee->identity_end, TRUE);

                if($employee->joining_date and $employee->joining_date!="")
                    $data["joining_date"] = $this->emp_model->Hijri2Greg($employee->joining_date, TRUE);

                if($employee->retirement_date and $employee->retirement_date!="")
                    $data["retirement_date"] = $this->emp_model->Hijri2Greg($employee->retirement_date, TRUE);

                if($employee->wife_birth and $employee->wife_birth!="")
                    $data["wife_birth"] = $this->emp_model->Hijri2Greg($employee->wife_birth, TRUE);


                if($employee->fils_birth and $employee->fils_birth!=""){
                    $xx = explode(";", $employee->fils_birth);
                    $var = "";
                        foreach($xx as $x){
                            if($x and $x!="")
                                $var .= $this->emp_model->Hijri2Greg($x, TRUE).";";
                        }
                    $data["fils_birth"] = $var;
                }

                //$this->db->where("employee_id",$employee->employee_id)->update("tbl_employee", $data);
            }

            $attendances = $this->db->get('tbl_attendance')->result();
            foreach ($attendances as $attendance){
                if($attendance->att_date and $attendance->att_date!=""){
                    $att_date = date_create_from_format('Y/m/d', $attendance->att_date)->format('Y-m-d');
                    $att_date = $this->emp_model->Hijri2Greg($att_date , TRUE);
                    $att_date = date_create_from_format('Y-m-d', $att_date)->format('Y/m/d');
                    //$this->db->where("attendance_id",$attendance->attendance_id)->update("tbl_attendance", ["att_date"=>$att_date]);
                }
            }

            $attendance_xhs = $this->db->get('tbl_attendance_xh')->result();
            foreach ($attendance_xhs as $attendance){
                if($attendance->att_date and $attendance->att_date!=""){
                    $att_date = date_create_from_format('Y/m/d', $attendance->att_date)->format('Y-m-d');
                    $att_date = $this->emp_model->Hijri2Greg($att_date , TRUE);
                    $att_date = date_create_from_format('Y-m-d', $att_date)->format('Y/m/d');
                    //$this->db->where("attendance_xh_id",$attendance->attendance_xh_id)->update("tbl_attendance_xh", ["att_date"=>$att_date]);
                }
            }

            $notices = $this->db->get('tbl_notice')->result();
            foreach ($notices as $notice){
                if($notice->created_date and $notice->created_date!=""){
                    $created_date = $this->emp_model->Hijri2Greg($notice->created_date , TRUE);
                    //$this->db->where("notice_id",$notice->notice_id)->update("tbl_notice", ["created_date"=>$created_date]);
                }
            }

            $allowances = $this->db->get('tbl_allowance')->result();
            foreach ($allowances as $allowance){
                $data = [];
                if($allowance->allowance_date_from and $allowance->allowance_date_from!=""){
                    $data["allowance_date_from"] = $this->emp_model->Hijri2Greg($allowance->allowance_date_from , TRUE);
                }
                if($allowance->allowance_date_to and $allowance->allowance_date_to!=""){
                    $data["allowance_date_to"] = $this->emp_model->Hijri2Greg($allowance->allowance_date_to , TRUE);
                }
                //$this->db->where("allowance_type_id",$allowance->allowance_type_id)->update("tbl_allowance", $data);
            }

            $deductions = $this->db->get('tbl_deduction')->result();
            foreach ($deductions as $deduction){
                $data = [];
                if($deduction->start_date and $deduction->start_date!=""){
                    $data["start_date"] = $this->emp_model->Hijri2Greg($deduction->start_date , TRUE);
                }
                if($deduction->end_date and $deduction->end_date!=""){
                    $data["end_date"] = $this->emp_model->Hijri2Greg($deduction->end_date , TRUE);
                }
                //$this->db->where("deduction_type_id",$deduction->deduction_type_id)->update("tbl_deduction", $data);
            }

            $provisions = $this->db->get('tbl_provision')->result();
            foreach ($provisions as $provision){
                $data = [];
                if($provision->start_date and $provision->start_date!=""){
                    $data["start_date"] = $this->emp_model->Hijri2Greg($provision->start_date , TRUE);
                }
                if($provision->end_date and $provision->end_date!=""){
                    $data["end_date"] = $this->emp_model->Hijri2Greg($provision->end_date , TRUE);
                }
                //$this->db->where("provision_id",$provision->provision_id)->update("tbl_provision", $data);
            }

            $custodies = $this->db->get('tbl_employee_custody')->result();
            foreach ($custodies as $custody){
                if($custody->delivery_date and $custody->delivery_date!=""){
                    //$this->db->where("custody_id",$custody->custody_id)->update("tbl_employee_custody", ["delivery_date"=>$this->emp_model->Hijri2Greg($custody->delivery_date , TRUE)]);
                }
            }

            $leaves = $this->db->get('tbl_leaves')->result();
            foreach ($leaves as $leave){
                $data = [];
                if($leave->going_date and $leave->going_date!=""){
                    $data["going_date"] = $this->emp_model->Hijri2Greg($leave->going_date , TRUE);
                }
                if($leave->coming_date and $leave->coming_date!=""){
                    $data["coming_date"] = $this->emp_model->Hijri2Greg($leave->coming_date , TRUE);
                }
                //$this->db->where("leave_id",$leave->leave_id)->update("tbl_leaves", $data);
            }

            $advances = $this->db->get('tbl_advances')->result();
            foreach ($advances as $advance){
                $data = [];
                if($advance->advance_date and $advance->advance_date!=""){
                    $data["advance_date"] = $this->emp_model->Hijri2Greg($advance->advance_date , TRUE);
                }
                if($advance->last_date and $advance->last_date!=""){
                    $data["last_date"] = $this->emp_model->Hijri2Greg($advance->last_date , TRUE);
                }
                //$this->db->where("advances_id",$advance->advances_id)->update("tbl_advances", $data);
            }

            $cachings = $this->db->get('tbl_caching')->result();
            foreach ($cachings as $caching){
                if($caching->caching_date and $caching->caching_date!=""){
                    //$this->db->where("caching_id",$caching->caching_id)->update("tbl_caching", ["caching_date"=>$this->emp_model->Hijri2Greg($caching->caching_date , TRUE)]);
                }
            }

            $courses = $this->db->get('tbl_courses')->result();
            foreach ($courses as $course){
                $data = [];
                if($course->start_date and $course->start_date!=""){
                    $data["start_date"] = $this->emp_model->Hijri2Greg($course->start_date , TRUE);
                }
                if($course->end_date and $course->end_date!=""){
                    $data["end_date"] = $this->emp_model->Hijri2Greg($course->end_date , TRUE);
                }
                //$this->db->where("course_id",$course->course_id)->update("tbl_courses", $data);
            }

            $purchases = $this->db->get('tbl_purchases')->result();
            foreach ($purchases as $purchase){
                if($purchase->purchase_date and $purchase->purchase_date!=""){
                    //$this->db->where("purchase_id",$purchase->purchase_id)->update("tbl_purchases", ["purchase_date"=>$this->emp_model->Hijri2Greg($purchase->purchase_date , TRUE)]);
                }
            }

            $maintenaces = $this->db->get('tbl_maintenaces')->result();
            foreach ($maintenaces as $maintenace){
                if($maintenace->maintenance_date and $maintenace->maintenance_date!=""){
                    //$this->db->where("maintenance_id",$maintenace->maintenance_id)->update("tbl_maintenaces", ["maintenance_date"=>$this->emp_model->Hijri2Greg($maintenace->maintenance_date , TRUE)]);
                }
            }

            $permissions = $this->db->get('tbl_permissions')->result();
            foreach ($permissions as $perm){
                if($perm->permission_date and $perm->permission_date!=""){
                    //$this->db->where("permission_id",$perm->permission_id)->update("tbl_permissions", ["permission_date"=>$this->emp_model->Hijri2Greg($perm->permission_date , TRUE)]);
                }
            }

            $recrutements = $this->db->get('tbl_recrutements')->result();
            foreach ($recrutements as $recrutement){
                $data = [];
                if($recrutement->going_date and $recrutement->going_date!=""){
                    $data["going_date"] = $this->emp_model->Hijri2Greg($recrutement->going_date , TRUE);
                }
                if($recrutement->coming_date and $recrutement->coming_date!=""){
                    $data["coming_date"] = $this->emp_model->Hijri2Greg($recrutement->coming_date , TRUE);
                }
                //$this->db->where("recrutement_id",$recrutement->recrutement_id)->update("tbl_recrutements", $data);
            }

            $embarkations = $this->db->get('tbl_embarkation')->result();
            foreach ($embarkations as $embarkation){
                $data = [];
                if($embarkation->date1 and $embarkation->date1!=""){
                    $data["date1"] = $this->emp_model->Hijri2Greg($embarkation->date1 , TRUE);
                }
                if($embarkation->date2 and $embarkation->date2!=""){
                    $data["date2"] = $this->emp_model->Hijri2Greg($embarkation->date2 , TRUE);
                }
                //$this->db->where("embarkation_id",$embarkation->embarkation_id)->update("tbl_embarkation", $data);
            }

            $accountings = $this->db->get('tbl_accountings')->result();
            foreach ($accountings as $accounting){
                if($accounting->created_date and $accounting->created_date!=""){
                    //$this->db->where("accounting_id",$accounting->accounting_id)->update("tbl_accountings", ["created_date"=>$this->emp_model->Hijri2Greg($accounting->created_date , TRUE)]);
                }
            }

            $evaluations = $this->db->get('tbl_evaluations')->result();
            foreach ($evaluations as $evaluation){
                if($evaluation->created_date and $evaluation->created_date!=""){
                    //$this->db->where("evaluation_id",$evaluation->evaluation_id)->update("tbl_evaluations", ["created_date"=>$this->emp_model->Hijri2Greg($evaluation->created_date , TRUE)]);
                }
            }

            $applications = $this->db->get('tbl_applications')->result();
            foreach ($applications as $application){
                $data = [];
                if($application->date and $application->date!=""){
                    $data["date"] = $this->emp_model->Hijri2Greg($application->date , TRUE);
                }
                if($application->start_date and $application->start_date!=""){
                    $data["start_date"] = $this->emp_model->Hijri2Greg($application->start_date , TRUE);
                }
                if($application->end_date and $application->end_date!=""){
                    $data["end_date"] = $this->emp_model->Hijri2Greg($application->end_date , TRUE);
                }
                if($application->going_date and $application->going_date!=""){
                    $data["going_date"] = $this->emp_model->Hijri2Greg($application->going_date , TRUE);
                }
                if($application->coming_date and $application->coming_date!=""){
                    $data["coming_date"] = $this->emp_model->Hijri2Greg($application->coming_date , TRUE);
                }
                if($application->going_date_2 and $application->going_date_2!=""){
                    $data["going_date_2"] = $this->emp_model->Hijri2Greg($application->going_date_2 , TRUE);
                }
                if($application->coming_date_2 and $application->coming_date_2!=""){
                    $data["coming_date_2"] = $this->emp_model->Hijri2Greg($application->coming_date_2 , TRUE);
                }
                if($application->birth_date and $application->birth_date!=""){
                    $data["birth_date"] = $this->emp_model->Hijri2Greg($application->birth_date , TRUE);
                }

                //$this->db->where("application_id",$application->application_id)->update("tbl_applications", $data);
            }

            echo "<hr><hr><pre>";
            print_r($applications);
            echo "</pre>";

        }catch (\Exception $e){
            echo $e->getMessage();
        }
    }

}