<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class Job_application extends MY_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('emp_model');
    }

    public function index(){
        $this->emp_model->_table_name = "countries";
        $this->emp_model->_order_by = "countryName";
        $data['countries'] = $this->emp_model->get();

        $this->load->view('job_application', $data);
    }
    public function save_application(){
        $data=$this->input->post();

        $approval_cat_id=$this->db->select('approval_cat_id,all_list, accept, refuse, delegate, consultation')->from('tbl_approvals_cat')->where("linked_apps like '%la_job_application%'")->get()->result();

        $ids = explode(';',$approval_cat_id[0]->all_list);
        $data['accept'] = $approval_cat_id[0]->accept;
        $data['refuse'] = $approval_cat_id[0]->refuse;
        $data['delegate'] = $approval_cat_id[0]->delegate;
        $data['consultation'] = $approval_cat_id[0]->consultation;
        $ids2=array();
        $results=array();
        foreach ($ids as $id)
        {
            if($id!="dm"){
                array_push($results,'0');
                array_push($ids2,$id);
            }

        }
        $data['ids']=implode(';',$ids2);
        $data['results']=implode(';',$results);

        if (!empty($_FILES['photo']['name'])) {
            $_FILES['photo']['name'] = urlencode($_FILES['photo']['name']);
            $val = $this->emp_model->uploadImage('photo');
            $data['photo'] = (!empty($val['path'])) ? $val['path'] : '';
        }
        $data['type_app'] = 'la_job_application';
        $data['employee_id']=0;
        $data['date'] = $this->emp_model->Greg2Hijri(date('Y-m-d'), TRUE);
        $data['approval_cat_id'] = $approval_cat_id[0]->approval_cat_id;


        $this->emp_model->_table_name = "tbl_applications";
        $this->emp_model->_primary_key = "application_id";
        $saved_id = $this->emp_model->save($data);
        echo $this->db->last_query();
        echo '<hr>';
        echo $this->db->_error_message();
        echo '<hr>';
            if($saved_id){
                $type = "success";
                $message = "<div class='rtl'>تم إرسال طلبك بنجاح</div>";
                $message.= "<br>Your job application was sent successfully";
                set_message($type, $message);
                redirect('job_application');
            }
            else
            {
                $ddd = $this->db->last_query().'<br>'.$this->db->_error_message().'<br>';
                $type = "error";
                $message = "<div class='rtl'>لقد حصل خطأ ما أثناء التسجيل الرجاء المحاولة مجددا</div>";
                $message.= "<br>Something went wrong during the process. Please retry again";
                set_message($type, $message);
                redirect('job_application');
            }

    }

}


?>