<?php

class Reminder_cronjob extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_model');
        $this->load->model('emp_model');
        $today = date("Y-m-d");
        $this->hijri = $today;//$this->emp_model->Greg2Hijri($today, TRUE);
        $this->reminders = $this->employee_model->check_by(array('reminder_id' => 1), 'tbl_reminder');
    }

    public function reset_balance()
    {

        $hijri = explode("-", $this->hijri);
        /*if ($hijri[2] == 30 or $hijri[2] == 29)
            return;*/
        $emps = $this->db->get("tbl_employee")->result();
        foreach ($emps as $emp) {
            $joining_date = explode("-", $emp->joining_date);
            if ($joining_date[2] == 30 or $joining_date[2] == 29) {
                //$joining_date[2] = "28";
            }
            if ($joining_date[2] == $hijri[2] and $joining_date[1] == $hijri[1]) {
                $rest = ($emp->holiday_no + $emp->old_rest) - $emp->new_balance;

                $data['reset_old_rest']= $emp->old_rest;
                $data['reset_new_balance']= $emp->new_balance;
                $data['reset_old_balance'] = $emp->old_balance;

                $data['old_rest']=$rest;
                $data['new_balance']=0;
                $data['old_balance'] =$emp->new_balance;
                echo "<pre>";
                print_r($data);
                echo "</pre>";

                $this->emp_model->_table_name = "tbl_employee";
                $this->emp_model->_primary_key = "employee_id";
                $this->emp_model->_order_by = "employee_id";
                //$this->emp_model->save($data, $emp->employee_id);
            }
        }
    }

    public function send_sms($sender, $receiver, $message)
    {
        $this->emp_model->_table_name = "tbl_sms_config"; //table name
        $this->emp_model->_primary_key = "sms_config_id";
        $this->emp_model->_order_by = "sms_config_id";

        $data = $this->emp_model->get(array('sms_config_id' => 1), TRUE);

        if ($data->default == 1) {
            $login = $data->sms_login1;
            $pass = $data->sms_password1;
            $tagname = $data->sender_name1;
            $data = array(
                'Username' => $login,
                'Password' => $pass,
                'Tagname' => $tagname,
                'RecepientNumber' => $receiver,
                'Message' => $message . "\n\n\n" . base_url(),
                'SendDateTime' => 0,
                'EnableDR' => false
            );
            $data_string = json_encode($data);

            $ch = curl_init('http://api.yamamah.com/SendSMS');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            $login = $data->sms_login2;
            $pass = $data->sms_password2;
            $sender = $data->sender_name2;
            $message = $message . " \n\n\n" . base_url();
            $numbers = $receiver;

            $_url = 'https://www.lanasms.net/api/sendsms.php';
            $postData = 'username=' . $login . '&password=' . $pass . '&message=' . $message . '&numbers=' . $numbers . '&sender=' . $sender . '&unicode=E&return=Json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, count($postData));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

            $output = curl_exec($ch);

            curl_close($ch);
        }
    }

    function Greg2Hijri($date, $string = false)
    {
        $date = explode(' ', $date);
        $dt = $date[0];
        $d = explode('-', $dt);

        $day = (int)$d[2];
        $month = (int)$d[1];
        $year = (int)$d[0];

        if (($year > 1582) or (($year == 1582) and ($month > 10)) or (($year == 1582) and ($month == 10) and ($day > 14))) {
            $jd = $this->intPart((1461 * ($year + 4800 + $this->intPart(($month - 14) / 12))) / 4) + $this->intPart((367 * ($month - 2 - 12 * ($this->intPart(($month - 14) / 12)))) / 12) -
                $this->intPart((3 * ($this->intPart(($year + 4900 + $this->intPart(($month - 14) / 12)) / 100))) / 4) + $day - 32075;
        } else {
            $jd = 367 * $year - $this->intPart((7 * ($year + 5001 + $this->intPart(($month - 9) / 7))) / 4) + $this->intPart((275 * $month) / 9) + $day + 1729777;
        }

        $l = $jd - 1948440 + 10632;
        $n = $this->intPart(($l - 1) / 10631);
        $l = $l - 10631 * $n + 354;
        $j = ($this->intPart((10985 - $l) / 5316)) * ($this->intPart((50 * $l) / 17719)) + ($this->intPart($l / 5670)) * ($this->intPart((43 * $l) / 15238));
        $l = $l - ($this->intPart((30 - $j) / 15)) * ($this->intPart((17719 * $j) / 50)) - ($this->intPart($j / 16)) * ($this->intPart((15238 * $j) / 43)) + 29;

        $month = $this->intPart((24 * $l) / 709);
        $day = $l - $this->intPart((709 * $month) / 24);
        $year = 30 * $n + $j - 30;

        $date = array();
        $date['year'] = $year;
        $date['month'] = $month - 1;
        $date['day'] = $day;
        $hijriMonth = array("محرم", "صفر", "ربيع الاول", "ربيع الثاني", "جمادى الاولى", "جمادى الثانية", "رجب", "شعبان", "رمضان", "شوال", "ذو القعدة", "ذو الحجة");

        if (!$string)
            return $date;
        else
            return $year . '-' . $month . '-' . $day;
    }

    function intPart($float)
    {
        if ($float < -0.0000001)
            return ceil($float - 0.0000001);
        else
            return floor($float + 0.0000001);
    }

    public function reminder_test_period_hrm()
    {
        if ($this->reminders->reminder_test_period_hrm == 0)
            return;
        echo 'reminder_test_period_hrm';
        $message_info = $this->employee_model->check_by(array('message_id' => 1), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            /*employee in test period*/
            if ($emp->status == 3) {
                /*end test date*/
                $end_test_date = strtotime($emp->joining_date) - ($emp->test_period * 24 * 60 * 60);
                /*comparison*/
                if ($end_test_date - ($this->reminders->reminder_test_period_hrm * 24 * 60 * 60) == strtotime($this->hijri)) {
                    /* if hr has phone */
                    if ($phone_hr) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->send_sms('0549344855', $phone_hr, $message_info->subject_ar . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->send_sms('0549344855', $phone_hr, $message_info->subject_en . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if hr has email */
                    if ($email_hr) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                }
            }
        }
    }

    public function reminder_test_period_dm()
    {
        if ($this->reminders->reminder_test_period_dm == 0)
            return;
        echo 'reminder_test_period_dm';
        $message_info = $this->employee_model->check_by(array('message_id' => 2), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();

        foreach ($employee_info as $emp) {
            /*employee in test period*/
            if ($emp->status == 3) {
                /*end test date*/
                $end_test_date = strtotime($emp->joining_date) - ($emp->test_period * 24 * 60 * 60);
                /*comparison*/
                if ($end_test_date - ($this->reminders->reminder_test_period_hrm * 24 * 60 * 60) == strtotime($this->hijri)) {
                    $dm = $this->employee_model->check_by(array('employee_id' => $emp->direct_manager_id), 'tbl_employee');
                    $phone_dm = $dm->phone;
                    $email_dm = $dm->email;
                    /* if hr has phone */
                    if ($phone_dm) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->send_sms('0549344855', $phone_dm, $message_info->subject_ar . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->send_sms('0549344855', $phone_dm, $message_info->subject_en . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if hr has email */
                    if ($email_dm) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_dm, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_dm, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_test_period_hrm . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                }
            }
        }
    }

    public function reminder_identity_end()
    {
        if ($this->reminders->reminder_identity_end == 0)
            return;
        echo 'reminder_identity_end';
        $message_info = $this->employee_model->check_by(array('message_id' => 3), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            /* comparison*/
            if (strtotime($emp->identity_end) - ($this->reminders->reminder_identity_end * 24 * 60 * 60) == strtotime($this->hijri)) {
                /* if hr has phone */
                if ($phone_hr) {
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_ar . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_en . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
                /* if hr has email */
                if ($email_hr) {
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
                /* if send to employee also */
                if ($this->reminders->reminder_mail == 1) {
                    $phone = $emp->phone;
                    $email = $emp->email;
                    /* if employee has phone */
                    if ($phone) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->send_sms('0549344855', $phone, $message_info->subject_ar . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->send_sms('0549344855', $phone, $message_info->subject_en . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if employee has email */
                    if ($email) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_identity_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                }
            }
        }
    }

    public function reminder_passport_end()
    {
        if ($this->reminders->reminder_passport_end == 0)
            return;
        echo 'reminder_passport_end';
        $message_info = $this->employee_model->check_by(array('message_id' => 4), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            /* comparison*/
            if (strtotime($emp->passport_end) - ($this->reminders->reminder_passport_end * 24 * 60 * 60) == strtotime($this->hijri)) {
                /* if hr has phone */
                if ($phone_hr) {
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_ar . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_en . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
                /* if hr has email */
                if ($email_hr) {
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
                /* if send to employee also */
                if ($this->reminders->reminder_mail == 1) {
                    $phone = $emp->phone;
                    $email = $emp->email;
                    /* if employee has phone */
                    if ($phone) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->send_sms('0549344855', $phone, $message_info->subject_ar . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->send_sms('0549344855', $phone, $message_info->subject_en . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if employee has email */
                    if ($email) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_passport_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                }
            }
        }
    }

    public function reminder_med_insurance_end()
    {
        if ($this->reminders->reminder_med_insurance_end == 0)
            return;
        echo 'reminder_med_insurance_end';
        $message_info = $this->employee_model->check_by(array('message_id' => 5), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            /* if med_insur_end_date is set */
            if ($emp->med_insur_end_date) {
                /* comparison */
                if (strtotime($emp->med_insur_end_date) - ($this->reminders->reminder_med_insurance_end * 24 * 60 * 60) == strtotime($this->hijri)) {
                    /* if hr has phone */
                    if ($phone_hr) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->send_sms('0549344855', $phone_hr, $message_info->subject_ar . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->send_sms('0549344855', $phone_hr, $message_info->subject_en . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if hr has email */
                    if ($email_hr) {
                        if ($this->reminders->reminder_lang == 'ar')
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                        else
                            $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                    }
                    /* if send to employee also */
                    if ($this->reminders->reminder_mail == 1) {
                        $phone = $emp->phone;
                        $email = $emp->email;
                        /* if employee has phone */
                        if ($phone) {
                            if ($this->reminders->reminder_lang == 'ar')
                                $this->send_sms('0549344855', $phone, $message_info->subject_ar . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                            else
                                $this->send_sms('0549344855', $phone, $message_info->subject_en . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                        }
                        /* if employee has email */
                        if ($email) {
                            if ($this->reminders->reminder_lang == 'ar')
                                $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                            else
                                $this->mail->sendEmail('hrm_lite@gmail.com', $email, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_med_insurance_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                        }
                    }
                }
            }
        }
    }

    public function reminder_contract_end()
    {
        if ($this->reminders->reminder_contract_end == 0)
            return;
        echo 'reminder_contract_end';
        $message_info = $this->employee_model->check_by(array('message_id' => 6), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            /*comparison*/
            if (strtotime($emp->retirement_date) - ($this->reminders->reminder_contract_end * 24 * 60 * 60) == strtotime($this->hijri)) {
                /* if hr has phone */
                if ($phone_hr) {
                    $this->send_sms('0549344855', $phone_hr, "kjsdgjkdbfks");
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_ar . ' : (' . $this->reminders->reminder_contract_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->send_sms('0549344855', $phone_hr, $message_info->subject_en . ' : (' . $this->reminders->reminder_contract_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
                /* if hr has email */
                if ($email_hr) {
                    if ($this->reminders->reminder_lang == 'ar')
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_ar, $message_info->subject_ar . ' : (' . $this->reminders->reminder_contract_end . ') ' . $message_info->message_ar . ' ' . $emp->full_name_ar);
                    else
                        $this->mail->sendEmail('hrm_lite@gmail.com', $email_hr, $message_info->subject_en, $message_info->subject_en . ' : (' . $this->reminders->reminder_contract_end . ') ' . $message_info->message_en . ' ' . $emp->full_name_en);
                }
            }
        }
    }

    public function reminder_employee_fixed()
    {
        if ($this->reminders->reminder_employee_fixed == 0)
            return;
        echo 'reminder_employee_fixed';
        $message_info = $this->employee_model->check_by(array('message_id' => 6), 'tbl_messages');
        $employee_info = $this->emp_model->all_emplyee_info();
        $hr = $this->employee_model->getHr();
        $email_hr = $hr->email;
        $phone_hr = $hr->phone;

        foreach ($employee_info as $emp) {
            if ($emp->status == 3) {
                /*end test date*/
                $end_test_date = strtotime($emp->joining_date) - ($emp->test_period * 24 * 60 * 60);
                /*comparison*/
                if ($end_test_date <= strtotime($this->hijri)) {
                    $this->employee_model->_table_name = "tbl_employee";
                    $this->employee_model->_primary_key = "employee_id";
                    $data['status'] = 1;
                    $data['test_period'] = 0;
                    $idd = $this->employee_model->save($data, $emp->employee_id);
                    if ($idd)
                        echo "<br>the test period of the employee $emp->full_name_en has ended and now he is activated<br>";
                }
            }
        }
    }

}